<?php
class ivtWht{
static $serie='ivtWht';
static $tbk='ivt_owht';
static $tbk1='ivt_wht1';
static $tbk99='ivt_doc99';
static public function revDoc($_J=array()){
	if(_js::iseErr($_J['cardId'],'Se debe definir un contacto.','numeric>0')){}
	else if(_js::iseErr($_J['cardName'],'Se debe definir un contacto.')){}
	else if(_js::iseErr($_J['docDate'],'La fecha del documento debe estar definida.')){}
	else if(_js::iseErr($_J['whsIdFrom'],'Se debe definir bodega origen','numeric>0')){}
	else if(_js::iseErr($_J['whsId'],'Se debe definir bodega destino','numeric>0')){}
	else if($js=_js::textMax($_J['ref1'],20,'Ref. 1: ')){ _err::err($js); }
	else if($js=_js::textMax($_J['lineMemo'],200,'Detalles ')){ _err::err($js); }
	else if(!is_array($_J['L'])){ _err::err('No se han enviado lineas para el documento.',3); }
}
static public function post($_J=array()){
	self::revDoc($_J);
	if(_err::$err){}
	else{
		$_J['docStatus']='C';
		$errs=0;
		a_sql::transaction(); $cmt=false;
		_ADMS::_app('Ivt');
		$nDoc=new ivtDoc(array('tt'=>self::$serie,
		'tbk'=>self::$tbk,'tbk1'=>self::$tbk1,
		'uniLine'=>'Y',
		'qtyMov'=>'inQty','revWhsFrom'=>'Y','revWhs'=>'Y','handInv'=>'Y',
		'priceCan0'=>'Y',
		'priceLineFromAvg'=>'Y','priceFromLine'=>'N','sbPriceFrom'=>'avg',
		'ori'=>'ivtWht::post()'));
		$reqAcc=array('accIvt');
		$nl=1;
		foreach($_J['L'] as $nx=>$L){
			$L['whsIdFrom']=$_J['whsIdFrom'];
			$L['whsId']=$_J['whsId'];
			$nDoc->itmRev($L,array('reqAcc'=>$reqAcc,'ln'=>'Linea '.$nl.': ')); $nl++;
			if(_err::$err){ break; }
				$L['outQty']=$L['quantity'];
			$nDoc->handSet($L,array('whsId'=>$L['whsIdFrom']),$nDoc->La2); unset($L['outQty']);
			$cost=$nDoc->La2['cost'];
			$L['inQty']=$L['quantity'];
			$nDoc->handSet($L,array('costFromLa2'=>'Y')); unset($L['inQty']);
			$L['price']=$cost;
			$L['priceLine']=$cost*$L['quantity'];
			//L1 Doc
			$L[0]='i'; $L[1]=self::$tbk1;
			$nDoc->L1[]=$L;
			$costT=$cost*$L['quantity'];
			//L1 Dac
			$nDoc->Ld[]=array('accId'=>$nDoc->La['accIvt'],'creBal'=>$costT,'accCode'=>'14xx');
			$nDoc->Ld[]=array('accId'=>$nDoc->La['accIvt'],'debBal'=>$costT,'accCode'=>'14xx');
		}
		//die(print_r($nDoc->Livt));
		if(!_err::$err){ $nDoc->post($_J); $docEntry=$nDoc->docEntry; }
		
		if(_err::$err){ $errs++;  }
		if($errs==0){/* Contabilizar */
			self::dacPost($nDoc);
			if(_err::$err){ $errs++; }
		}
		if($errs==0){/* Mover Inventario */
			$nDoc->handPost();
			if(_err::$err){ $errs++; }
		}
		if($errs==0){ $cmt=true;
			$js=_js::r('Documento guardado correctamente.','"docEntry":"'.$docEntry.'"');
			Doc::logPost(array('tbk'=>self::$tbk99,'serieType'=>self::$serie,'docEntry'=>$docEntry,'dateC'=>1));
		}
		a_sql::transaction($cmt);
	}
	return $js;
}

static public function putCancel($D=array()){
	a_sql::transaction(); $cmt=false; $ori=' on[ivtWht::putCancel()]';
	Doc::putStatus(array('closeOmit'=>'Y','t'=>'N','tbk'=>self::$tbk,'docEntry'=>$D['docEntry'],'serieType'=>self::$serie,'log'=>self::$tbk99,'reqMemo'=>'Y','lineMemo'=>$D['lineMemo']));
	if(_err::$err){ $js=_err::$errText; }
	else{ $errs=0;
		_ADMS::mApps('gfi/Dac');
		gfiDac::putCancel(array('tt'=>self::$serie,'tr'=>$D['docEntry']));
		if(_err::$err){ $js=_err::$errText; }
		else{
			_ADMS::_app('Ivt'); 
			IvtDoc::rever(array('tt'=>self::$serie,'tr'=>$D['docEntry'],'docDate'=>date('Y-m-d')));
			if(_err::$err){ $js=_err::$errText;}
			else{ $cmt=true;
				$js=_js::r('Documento anulado correctamente.');
			}
		}
	}
	a_sql::transaction($cmt);
	return $js;
}

static public function dacPost($nDoc){
	$ori =' on[ivtWht::dacPost()]';
	if($js=_js::ise($nDoc->Doc['docEntry'],'No se ha definido el número de documento.'.$ori,'numeric>0')){ _err::err($js); return array(); }
	$n=0; $errs=0;
		_ADMS::mApps('gfi/Dac');
	$nDac=new gfiDac(array('tt'=>self::$serie,'tr'=>$nDoc->Doc['docEntry']));
	$nDac->Doc=$nDoc->Doc;
	$nDac->setLine($nDoc->Ld); if(_err::$err){ return false; }
	$nDac->post(); if(_err::$err){ return false; }
}
static public function logGet($D=array()){
	return Doc::logGet(array('tbk'=>self::$tbk99,'serieType'=>self::$serie,'docEntry'=>$D['docEntry']));
}
}
?>