<?php
class Packi{
public $tbk='mdl_oxxx';
public $tbk1='mdl_xxx1';
public $tbk8='mdl_xxx8';
public $L=array();
function __construct($P=array(),$Doc=array()){
	$this->tbk=$P['tbk'];
	$this->tbk1=$P['tbk1'];
	$this->tbk8=$P['tbk8'];
	$this->docEntry=$P['docEntry'];
}
public function get($P=array()){
	$ori=' on[Packi::get()]'; $M=array('L'=>array());
	$fie ='A.docEntry,A.docStatus,A.docDate';
	$fie.=($P['fie'])?','.$P['fie']:'';
	$M=a_sql::fetch('SELECT '.$fie.' 
	FROM '.$this->tbk.' A
	WHERE A.docEntry=\''.$this->docEntry.'\' ',array(1=>'Error obteniendo información de documento: '.$ori));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); }
	else{
		$M['Lb']=array();
		$q=a_sql::query('SELECT B.itemId,B.itemSzId,B.quantity openQty ,I.itemCode,I.itemName
		FROM '.$this->tbk1.' B
		LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
		WHERE B.docEntry=\''.$this->docEntry.'\' ',array(1=>'Error obteniendo lista de empaque: '.$ori));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else if(a_sql::$errNo==-1){
			while($L=$q->fetch_assoc()){
				$M['Lb'][]=$L;
			}
		}
		$M['L']=array();
		$q=a_sql::query('SELECT BC.id,BC.boxNum,BC.itemId,BC.itemSzId,BC.quantity,BC.breads,I.itemCode,I.itemName 
		FROM '.$this->tbk8.' BC
		LEFT JOIN itm_oitm I ON (I.itemId=BC.itemId)
		WHERE BC.docEntry=\''.$this->docEntry.'\' ',array(1=>'Error obteniendo lista de empaque: '.$ori));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else if(a_sql::$errNo==-1){
			while($L=$q->fetch_assoc()){
				$M['L'][]=$L;
			}
		}
		return $M;
	}
}
public function rev($Lx=array(),$P=array()){
	$ori=' on[Packi::rev()]';
	$qA=array(); $errs=0; 
	$putt=($P['put']!='N');
	if(_js::iseErr($this->docEntry,'Se debe definir el número de documento.'.$ori,'numeric>0')){ return false; }
	foreach($Lx as $n=>$L){
		if($L['quantity']==0 || $L['delete']=='Y'){}
		else{
			$q=a_sql::fetch('SELECT B.id,I.itemCode, G1.itemSize,B.quantity 
			FROM itm_oitm I
			LEFT JOIN itm_grs1 G1 ON (G1.itemSzId=\''.$L['itemSzId'].'\')
			LEFT JOIN '.$this->tbk1.' B ON (B.docEntry=\''.$this->docEntry.'\' AND I.itemId=B.itemId AND B.itemSzId=G1.itemSzId)
			WHERE I.itemId=\''.$L['itemId'].'\' LIMIT 1',array(1=>'Error revisando articulo de lista de empaque en consolidado.'.$ori,2=>'El articulo no existe.'.$ori));
			if(a_sql::$err){ _err::err(a_sql::$errNoText); $errs++; break; }
			else if(_js::iseErr($q['id'],'Articulo '.$q['itemCode'].'-'.$q['itemSize'].' no está definido en el documento.'.$ori,'numeric>0')){  $errs++; break; }
			else if($q['quantity']==0){ _err::err('Articulo '.$q['itemCode'].'-'.$q['itemSize'].': no tiene cantidades definidas para empacar.'.$ori,3); $errs++; break; }
			else if($L['quantity']>$q['quantity']){ _err::err('Articulo '.$q['itemCode'].'-'.$q['itemSize'].': solo se pueden empacar ('.($q['quantity']*1).') para el documento. Actual: '.$L['quantity'].$ori,3); $errs++; break; }
		}
	}
}

public function rev2Put($Lx=array(),$P=array()){
	$ori=' on[Packi::rev2Put()]';
	$qA=array(); $errs=0; 
	$putt=($P['put']!='N');
	if(_js::iseErr($this->docEntry,'Se debe definir el número de documento.'.$ori,'numeric>0')){ return false; }
	foreach($Lx as $n=>$L){
		if($L['quantity']==0 || $L['delete']=='Y'){}
		else{
			$q=a_sql::fetch('SELECT B.id,I.itemCode, G1.itemSize,B.quantity 
			FROM itm_oitm I
			LEFT JOIN itm_grs1 G1 ON (G1.itemSzId=\''.$L['itemSzId'].'\')
			LEFT JOIN '.$this->tbk1.' B ON (B.docEntry=\''.$this->docEntry.'\' AND I.itemId=B.itemId AND B.itemSzId=G1.itemSzId)
			WHERE I.itemId=\''.$L['itemId'].'\' LIMIT 1',array(1=>'Error revisando articulo de lista de empaque en consolidado.'.$ori,2=>'El articulo no existe.'.$ori));
			if(a_sql::$err){ _err::err(a_sql::$errNoText); $errs++; break; }
			else if(_js::iseErr($q['id'],'Articulo '.$q['itemCode'].'-'.$q['itemSize'].' no está definido en el documento.'.$ori,'numeric>0')){  $errs++; break; }
			else if($q['quantity']==0){ _err::err('Articulo '.$q['itemCode'].'-'.$q['itemSize'].': no tiene cantidades definidas para empacar.'.$q['id'].$ori,3); $errs++; break; }
			else if($L['quantity']>$q['quantity']){ _err::err('Articulo '.$q['itemCode'].'-'.$q['itemSize'].': solo se pueden empacar ('.($q['quantity']*1).') para este documento.'.$ori,3); $errs++; break; }
		}
		if($errs==0 && $putt){
			$L[0]='i'; $L[1]=$this->tbk8;
			if($L['quantity']==0){ $L['delete']='Y'; }
			$L['_unik']='id'; $L['docEntry']=$this->docEntry;
			if(!$L['id'] && $L['quantity']==0){ continue; }
			$qA[]=$L;
		}
	}
	if($errs==0 && $putt){
		a_sql::multiQuery($qA);
		if(_err::$err){}
	}
}
public function put($Lx=array()){
	if(_js::iseErr($this->docEntry,'Se debe definir el número de documento.','numeric>0')){ return false; }
	$qA=array(); $errs=0;
	foreach($Lx as $k=>$L){
		if(_js::iseErr($L['itemId'],$ln.'Id de artículo no definido.','numeric>0')){ $errs++; break; }
		else if(_js::iseErr($L['itemSzId'],$ln.'Id de talla no definido.','numeric>0')){ $errs++; break; }
		else if($L['quantity']!=0 && _js::iseErr($L['quantity'],$ln.'La cantidad debe ser un número','numeric')){ $errs++; break; }
		else{
			$L[0]='i'; $L[1]=$this->tbk8;
			if($L['quantity']==0 && !$L['id']){ continue; }
			if($L['quantity']==0){ $L['delete']='Y'; }
			$L['_unik']='id'; $L['docEntry']=$this->docEntry;
			$qA[]=$L;
		}
	}
	if($errs==0){
		a_sql::multiQuery($qA);
		if(_err::$err){}
	}
}
}
?>