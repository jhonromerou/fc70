<?php
class wmaPepAwh{
	static public function post($D=[],$trans=true){
		if(_js::iseErr($D['docDate'],'La fecha del documento debe estar definida.')){}
		else if(_js::iseErr($D['docType'],'Se debe definir el tipo de documento.')){}
		else if(_js::iseErr($D['whsId'],'Se debe la bodega.','numeric>0')){}
		else if(_js::iseErr($D['wfaId'],'Se debe la fase.','numeric>0')){}
		else if(!_js::textLimit($D['lineMemo'],200)){ $js= _js::e(3,'Los detalles no pueden exceder los 200 caracteres.'); }
		else if(_js::isArray($D['L'])){ $js=_js::e(3,'No se han enviado lineas para el documento.'); }
		else{
			$Ld=$D['L']; unset($D['L']); $n=1;
			$rows=0;
			if($trans!='N'){ a_sql::transaction(); $c=false; }
			_ADMS::mApps('wma/PeP'); $rows=0;
			$Di=array(); $Liv=array(); $nl=1;
			#Revisar lineas
			foreach($Ld as $nk=>$L){
				$L['whsId']=$D['whsId'];
				$L['wfaId']=$D['wfaId'];
				unset($L['priceList']);
				$totaln=0; $ln ='Linea '.$nl.': '; $nl++;
				if($L['quantity']==''){ $Ld[$nk]['delete']='Y'; continue; }
				if(_js::iseErr($L['itemId'],$ln.'No se ha encontrado el ID del artículo.','numeric>0')){ break; }
				else if(_js::iseErr($L['itemSzId'],$ln.'No se ha definido el ID del subproducto.','numeric>0')){ break; }
				else if(_js::iseErr($L['quantity'],$ln.'La cantidad debe ser un numero y no puede ser negativa.','numeric')){ break; }
				else if(_js::iseErr($L['whsId'],$ln.'Se debe definir la bodega de salida.','numeric>0')){ break; }
				else if(_js::iseErr($L['wfaId'],$ln.'Se debe definir la fase de salida.','numeric>0')){ break; }
				else{
					PeP::onHand_get($L,array('whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'lnt'=>$ln));
					if(PeP::$err){ _err::err(Pep::$errText); break; }
					else{
						unset($L['handInv']);
						$L['onHand']=PeP::$D['onHand']*1;
						$L['diff']=PeP::$D['diff']*1;
						$L['lineNum']=$n;
						$L[0]='i'; $L[1]='pep_awh1';
						$Ld[$nk]=$L;
						$diff=PeP::$D['diff'];
						if($diff>0){ $rows++;
							$Liv[]=array('whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$diff,'inQty'=>'Y');
						}
						else if($diff<0){ $rows++;
							$Liv[]=array('whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>abs($diff),'outQty'=>'Y');
						}
					}
				}
				$n++;
			}
			if(!_err::$err && ($rows==0)){ _err::err('No se encontraron diferentes para ajustar inventario. ('.$rows.')',3); }
			if(!_err::$err){ _ADMS::lib('docSeries'); $D=docSeries::nextNum($D,$D); }
			if(!_err::$err){
				#crear doc
				$docEntry=a_sql::qInsert($D,array('ku'=>'ud','tbk'=>'pep_oawh'));
				if(a_sql::$err){ _err::err('Error guardando documento pep.egr: '.a_sql::$errText,3); }
				else{
					#registrar lineas
					a_sql::multiQuery($Ld,0,['docEntry'=>$docEntry]);
					if(!_err::$err){ #mover inventario
						$D['docEntry']=$docEntry;
						if(count($Liv)>0){
							PeP::onHand_put($Liv,array('docDate'=>$D['docDate'],'tt'=>'pepAwh','tr'=>$docEntry));
						}
						if(PeP::$err){ _err::err(PeP::$errText); }
						if(!_err::$err){ $c=true;
							$js=_js::r('Información guardada correctamente.','"docEntry":"'.$docEntry.'"');
						}
					}
				}
			}
			if($trans!='N'){a_sql::transaction($c); }
		}
		_err::errDie();
		return $js;
	}
}
?>