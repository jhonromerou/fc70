<?php
/*proviene de /ac70/lib/cost.php julio 2019 */
class wmaCost {

static public function fie2Upd($B=array(),$P=array()){
	$r=array();
	$cost=$P['cost'];
	if(is_array($P)) foreach($P as $k=>$v){ $B[$k]=$v;}
	unset($B['cost']);
	//$B['cost=']='costMP+costMO+costMA+cif';
	$B['cost']=$B['costMP']+$B['costMO']+$B['costSV']+$B['costMA']+$B['cif'];
	$B['dateUpd']=date('Y-m-d H:i:s');
	return $B;
}
static public function getBase($P=array(),$fie='ta'){
	$itemId=$P['itemId'];
	$gb='IT.lineType,IT.itemSzId,IT.variType,IT.isVari';
	$gb .=($fie=='wfa')?',IT.wfaId':'';
	/* B= sumar a +T
	O: valores comunes sin talla
	T: valores espec tallas
	*/
	$IT=array();
	$BS=array('cost'=>0,'costMP'=>0,'costMO'=>0,'costSV'=>0,'costMA'=>0,'cif'=>0);
	if($fie=='wfa'){ $IT['B']=array(); $IT['O']=array(); $IT['S']=$BS; }
	else{ $IT['B']=array(); $IT['O']=array(); }
	$IT['T']=array();
	$q=a_sql::query('SELECT '.$gb.',SUM(IT.lineTotal) lineTotal FROM itm_itt1 IT WHERE itemId=\''.$itemId.'\' GROUP BY '.$gb,array(1=>'Error obteniendo escandallos del producto ID '.$itemId.': '));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else if(a_sql::$errNo==2 && $P['err2']=='Y'){ die(_js::e(3,'No existen componentes para el escandallo ID '.$itemId)); } 
	else if(a_sql::$errNo==-1){
		while($L=$q->fetch_assoc()){
			$k=$L['itemSzId'];
			$k1=$k; $ka='B';
			if($fie=='wfa'){
				$ka = $L['wfaId'];
				$k1 =$k.'_'.$L['wfaId'];
			}
			$BS['wfaId']=($L['wfaId'])?$L['wfaId']:0;
			if(!array_key_exists($ka,$IT['B'])){ $IT['B'][$ka]=$BS; }
			if(!array_key_exists($ka,$IT['O'])){ $IT['O'][$ka]=$BS; }
			$kType='cost'.$L['lineType'];//costMP,MO,SV,MA
			if(strtolower($L['lineType'])=='cif'){ $kType='cif'; }
			else if($L['lineType']=='SE'){ $kType='costMP'; }
			if($k1 && !array_key_exists($k1,$IT)){ $IT['T'][$k1]=$BS; }
			//standard
			if($k==0 && $L['isVari']=='N'){
				$IT['S']['cost']+=$L['lineTotal'];
				$IT['S'][$kType]+=$L['lineTotal'];
			}
			/*base para todas las tallas */
			if($L['variType']=='N' && $L['isVari']=='N'){
				$IT['B'][$ka]['cost']+=$L['lineTotal'];
				$IT['B'][$ka][$kType]+=$L['lineTotal'];
			}
			//Si aplica para todas las tallas
			if($k==0 && $L['variType']=='N'){
				$IT['O'][$ka]['cost']+=$L['lineTotal'];
				$IT['O'][$ka][$kType]+=$L['lineTotal'];
			}
			if($k!=0){/* Talla especifica */
				$IT['T'][$k1]['cost']+=$L['lineTotal'];
				$IT['T'][$k1][$kType]+=$L['lineTotal'];
			}
		}
		/* extructurar todos */
		foreach($IT['T'] as $k=>$Lx){
			$ka=($fie=='wfa')?$Lx['wfaId']:'B';
			foreach($Lx as $k2=>$L2){
				$IT['T'][$k][$k2]=$IT['T'][$k][$k2]+$IT['B'][$ka][$k2];
			}
			unset($IT['T'][$k]['wfaId']);
		}
	}
	return $IT;
}

static public function sumTo($D=array()){ /* sumar a fases */
	$q=a_sql::query('SELECT M1.itemId,G1.uniqSize,G2.itemSzId,M1.wfaId,M1.lineNum
	FROM wma_mpg1 M1
	LEFT JOIN itm_oitm I ON (I.itemId=M1.itemId)
	LEFT JOIN itm_grs2 G2 ON (G2.grsId=I.grsId)
	LEFT JOIN itm_grs1 G1 ON (G1.itemSzId=G2.itemSzId)
	WHERE M1.itemId=\''.$D['itemId'].'\' ',array(1=>'Error calculando acumulado de fases.'));
	if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
	else if(a_sql::$errNo==-1){
		$wF=array();
		$L0=array('invPrice'=>0);// solo si es uniqSize=Y
		while($L=$q->fetch_assoc()){
			$k2=$L['itemId'].'_'.$L['wfaId'];
			if(!array_key_exists($k2,$wF)){
				$qx=a_sql::query('SELECT wfaId FROM wma_mpg1 WHERE itemId=\''.$L['itemId'].'\' AND lineNum<='.$L['lineNum'].' ');
				$wF[$k2]='';
				if(a_sql::$errNo==-1){
					while($L2=$qx->fetch_assoc()){
						$wF[$k2] .='\''.$L2['wfaId'].'\',';
					}
					$wF[$k2]=substr($wF[$k2],0,-1);
				}
			}
			$wIn=($wF[$k2]!='')?' IN ('.$wF[$k2].') ': 'wfaId=\'XXXX\' ';
			$qc=a_sql::fetch('SELECT SUM(cost) cost,sum(costMP) costMP,sum(costMO) costMO, sum(costSV) costSV, sum(costMA) costMA,sum(cif) cif
			FROM itm_oipc WHERE itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' AND wfaId '.$wIn.' ',array(1=>'Error calculando acumulado de fases (2).'));
			if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
			$L0['uniqSize']=$L['uniqSize'];
			$tc=a_sql::query('UPDATE itm_oipc 
			SET sumCost=\''.$qc['cost'].'\', sumCostMP=\''.$qc['costMP'].'\',sumCostMO=\''.$qc['costMO'].'\',sumCostSV=\''.$qc['costSV'].'\',sumCostMA=\''.$qc['costMA'].'\',sumCif=\''.$qc['cif'].'\'
			WHERE itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' AND wfaId=\''.$L['wfaId'].'\' LIMIT 1',array(1=>'Error calculando acumulado de fases (3).'));
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		}
		if($L0['uniqSize']=='Y'){
			$qc=a_sql::fetch('SELECT cost
			FROM itm_oipc WHERE itemId=\''.$D['itemId'].'\' AND isModel=\'Y\' LIMIT 1',array(1=>'Error calculando acumulado de fases (2).'));
			if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
			$tc=a_sql::query('UPDATE itm_oitm 
			SET invPrice=\''.$qc['cost'].'\'
			WHERE itemId=\''.$D['itemId'].'\' LIMIT 1',array(1=>'Error actualizando costo articulo.'));
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		}
	}
}
static public function defineModel($D=array()){
	$IT=self::getBase($D); $ori=' on[wmaCost:defineModel]';
	/* definir */
	$k=0;
	$Di=array('isModel'=>'Y','itemId'=>$D['itemId'],'itemSzId'=>$k);
	$Di=self::fie2Upd($Di,$IT['S']);
	$ins3=a_sql::uniRow($Di,array('tbk'=>'itm_oipc','wh_change'=>'isModel=\'Y\' AND itemId=\''.$D['itemId'].'\' AND itemSzId=\''.$k.'\' LIMIT 1'));
	if(a_sql::$err){ _err::err('Error actualizando costo del artículo en la talla. ('.$D['itemId'].'-'.$k.'): '.$ori.a_sql::$errText,3); $errs=1; return false; }
	/* Obtener todas las tallas para actualizar */
	$ql=a_sql::query('SELECT B.itemSzId
	FROM itm_oitm I 
	LEFT JOIN itm_grs2 B ON (B.grsId=I.grsId)
	WHERE I.itemId=\''.$D['itemId'].'\' ORDER BY B.itemSzId ASC',array(1=>'Error obteniendo tallas del escandallo: '.$ori));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else if(a_sql::$errNo==2 && $D['err2']=='Y'){ _err::err(_js::e(3,'El escandallo no tiene definida ninguna talla.'.$ori)); }
	else if(a_sql::$errNo==-1){
		$dateUpd=date('Y-m-d H:i:s');
		$errs=0;
		$BS=array('cost'=>0,'costMP'=>0,'costMO'=>0,'costSV'=>0,'costMA'=>0,'cif'=>0);
		while($L=$ql->fetch_assoc()){
			$k=$L['itemSzId'];
			$Di=array('isModel'=>'Y','itemId'=>$D['itemId'],'itemSzId'=>$k);
			if($IT['T'][$k]){ $Di=self::fie2Upd($Di,$IT['T'][$k]); }
			else { $Di=self::fie2Upd($Di,$IT['O']['B']); }
			$ins3=a_sql::uniRow($Di,array('tbk'=>'itm_oipc','wh_change'=>'isModel=\'Y\' AND itemId=\''.$D['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' LIMIT 1'));
			if(a_sql::$err){ _err::err('Error actualizando costo del artículo en la talla. ('.$D['itemId'].'-'.$k.'): '.$ori.a_sql::$errText,3); $errs=1; break; }
		}
		if($errs==0){
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else{ $cmt=true; $js=_js::r('Costo de subproductos actualizados.'); }
		}
	}
	return $js;
}
static public function defineModelWfa($D=array()){
	$IT=self::getBase($D,'wfa'); $ori=' on[wmaCost:defineModelWfa]';
	/* Obtener todas las tallas para actualizar */
	$ql=a_sql::query('SELECT B.itemSzId,M.wfaId 
	FROM itm_oitm I 
	LEFT JOIN itm_grs2 B ON (B.grsId=I.grsId)
	LEFT JOIN wma_mpg1 M ON (M.itemId=\''.$D['itemId'].'\')
	WHERE I.itemId=\''.$D['itemId'].'\'',array(1=>'Error obteniendo tallas del escandallo: '.$ori));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else if(a_sql::$errNo==2 && $D['err2']=='Y'){ _err::err(_js::e(3,'El escandallo no tiene definida ninguna talla.'.$ori)); }
	else if(a_sql::$errNo==-1){
		$dateUpd=date('Y-m-d H:i:s');
		$errs=0;
		$BS=array('cost'=>0,'costMP'=>0,'costMO'=>0,'costSV'=>0,'costMA'=>0,'cif'=>0);
		while($L=$ql->fetch_assoc()){
			$k=$L['itemSzId'].'_'.$L['wfaId'];
			$ko=$L['wfaId'];
			$Di=array('isModel'=>'N','itemId'=>$D['itemId'],'itemSzId'=>$L['itemSzId'],'wfaId'=>$L['wfaId']);
			if($IT['T'][$k]){ $Di=self::fie2Upd($Di,$IT['T'][$k]); }
			else if($IT['O'][$ko]){ $Di=self::fie2Upd($Di,$IT['O'][$ko]); }
			else { $Di=self::fie2Upd($Di,$BS); }
			$ins3=a_sql::uniRow($Di,array('tbk'=>'itm_oipc','wh_change'=>'isModel=\'N\' AND itemId=\''.$D['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' AND wfaId=\''.$L['wfaId'].'\' LIMIT 1'));
			if(a_sql::$err){ _err::err('Error actualizando costo del artículo en la talla. ('.$D['itemId'].'-'.$k.'): '.$ori.a_sql::$errText,3); $errs=1; break; }
		}
		if($errs==0){ 
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else{ $js=_js::r('Costo de subproductos actualizados.'); }
		}
	}
	return $js;
}
}
?>