<?php
class wma3 extends _err{
static $D=array();
static $Ds=array();
static $V=array(
'wfaFS'=>false,'wfaFSName'=>'Fase 0',
'wfaFS-RC'=>false,'wfaFSName-RC'=>'Reproceso',
'wfaFS-NC'=>false,'wfaFSName-NC'=>'Desperdicio'
);
static public function i_wfaFS($P=array()){
	$ori=' on[wma3::i_wfaFS()]';
	$q=a_sql::query('SELECT limitFase,wfaClass,wfaId,wfaName FROM wma_owfa WHERE sysF=\'Y\' LIMIT 4',array(1=>'Error obteniendo fases iniciales del sistema.'.$ori,12=>'No se ha definido las fases iniciales sistema.'.$ori));
	if(a_sql::$err){
		self::$V['wfaFS']=false; die(a_sql::$errNoText); return false;
	}
	else if(a_sql::$errNo==-1){
		while($L=$q->fetch_assoc()){
			if($L['limitFase']=='I' || $L['wfaClass']=='F0'){
				self::$V['wfaFS']=$L['wfaId'];
				self::$V['wfaFSName']=$L['wfaName'];
			}
			else if($L['wfaClass']=='RC'){
				self::$V['wfaFS-RC']=$L['wfaId'];
				self::$V['wfaFSName-RC']=$L['wfaName'];
			}
			else if($L['wfaClass']=='NC'){
				self::$V['wfaFS-NC']=$L['wfaId'];
				self::$V['wfaFSName-NC']=$L['wfaName'];
			}
		}
		if(self::$V['wfaFS']==false){ die(_js::e(3,'Fase inicial de sistema no definida.'.$ori)); }
		if(self::$V['wfaFS-RC']==false){ die(_js::e(3,'Fase -Reproceso- de sistema no definida.'.$ori)); }
		if(self::$V['wfaFS-NC']==false){ die(_js::e(3,'Fase -Desperdicio- de sistema no definida.'.$ori)); }
		return $q;
	}
}
/*pdp */
static public function pdp_getLine($P=array()){
	$ql=a_sql::fetch('SELECT quantity,progQty,openQty FROM '._0s::$Tb['wma3_pdp1'].' WHERE docEntry=\''.$P['docEntry'].'\' AND itemId=\''.$P['itemId'].'\' AND itemSzId=\''.$P['itemSzId'].'\' LIMIT 1',array(1=>$ln.'Error obteniendo cantidades pendientes de la planificación: ',2=>$ln.'El artículo a producir no forma parte del documento de planificación.'));
	if(a_sql::$err){ self::err(a_sql::$errNoText); return false; }
	else{ return $ql; }
}
static public function pdp_putLine($L=array(),$ln=''){
	if($js=_js::ise($L['qSet'],$ln.'qSet is undefined on wma3::pdp_putLine().')){ _err::err($js); return false; }
	$upd=a_sql::query('UPDATE  wma3_pdp1 SET '.$L['qSet'].'
	WHERE docEntry=\''.$L['docEntry'].'\' AND itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' LIMIT 1',array(1=>$ln.'Error actualizando cantidades pendientes de la planificación: '));
	//if(a_ses::$userId==1){ print_r($upd); }
	if(a_sql::$err){ _err::err(a_sql::$errNoText); return false; }
	else{ return true; }
}
/* Ord. Prod */
static public function odp_getInf($P=array()){
	$fie=($P['fie'])?','.$P['fie']:'';
	$q=a_sql::fetch('SELECT A.docEntry'.$fie.'
	FROM '._0s::$Tb['wma3_oodp'].' A
	WHERE docEntry=\''.$P['docEntry'].'\' LIMIT 1',array(1=>'Error consultando orden de producción: ',2=>'La orden de producción '.$P['docEntry'].', no existe.'));
	if(a_sql::$err){ _err:err(a_sql::$errNoText); return false; }
	return $q;
}
/* estado de fase */
static public function odp_fase($L=array(),$ln=''){
	$qf=a_sql::fetch('SELECT A.tr,B20.wfaIdBef,B20.wfaIdNext,B20.openQty,B20.canExcess,B20.cost,B20.costMP,B20.costMO,B20.costMA,B20.costMOF,B20.costMAF,B20.cif
	FROM wma3_oodp A
	LEFT JOIN wma3_odp20 B20 ON (B20.docEntry=A.docEntry)
	WHERE B20.lineType=\'F\' AND A.docEntry=\''.$L['docEntry'].'\' AND B20.wfaId=\''.$L['wfaId'].'\' AND B20.itemId=\''.$L['itemId'].'\' AND B20.itemSzId=\''.$L['itemSzId'].'\' LIMIT 1',array(1=>$ln.'Error obteniendo información del estado de la fase en la orden de producción.',2=>'No se encontraron resultados para la fase en la orden de producción.'));
	if(a_sql::$err){ self::err(a_sql::$errNoText); return false; }
	if($qf['wfaIdBef']==0){ $qf['wfaIdBef']=self::$V['wfaFS']; }
	/* Si la fase  no permite excedentes =N */
	if(array_key_exists('quantity',$L) && $qf['canExcess']=='N' && $qf['openQty']<$L['quantity']){
	//if(array_key_exists('quantity',$L) && $qf['openQty']<$L['quantity']){
		self::err(_js::e(3,$ln.'La cantidad realizada ('.($L['quantity']*1).') es mayor a la cantidad pendiente ('.($qf['openQty']*1).') en la fase. La fase no permite excedentes.'));
		return $qf;
	}
	return $qf;
}
static public function odp_faseAnt($L=array(),$ln=''){
	$ori=' on[wma3::odp_faseAnt]';
	$qfA=a_sql::fetch('SELECT B20.wfaIdBef,B20.compQty
	FROM wma3_oodp A
	LEFT JOIN wma3_odp20 B20 ON (B20.docEntry=A.docEntry)
	WHERE B20.lineType=\'F\' AND A.docEntry=\''.$L['docEntry'].'\' AND B20.wfaId=\''.$L['wfaId'].'\' AND B20.itemId=\''.$L['itemId'].'\' AND B20.itemSzId=\''.$L['itemSzId'].'\' LIMIT 1',array(1=>$ln.'Error obteniendo información del estado de la fase en la orden de producción.'.$ori,2=>'No se encontraron resultados para la fase en la orden de producción.'.$ori));
	if(a_sql::$err){ self::err(a_sql::$errNoText); }
	if($qfA['wfaIdBef']==0){ return array(); }
	$qf=a_sql::fetch('SELECT B20.compQty
	FROM wma3_oodp A
	LEFT JOIN wma3_odp20 B20 ON (B20.docEntry=A.docEntry)
	WHERE B20.lineType=\'F\' AND A.docEntry=\''.$L['docEntry'].'\' AND B20.wfaIdNext=\''.$L['wfaId'].'\' AND B20.itemId=\''.$L['itemId'].'\' AND B20.itemSzId=\''.$L['itemSzId'].'\' LIMIT 1',array(1=>$ln.'Error obteniendo información del estado de la fase anterior en la orden de producción.'.$ori,2=>'No se encontraron resultados para la fase anterior en la orden de producción.'.$ori));
	/* Si la fase  no permite excedentes =N */
	if($L['quantity']>$qf['compQty']){
		$ori .=' --[b:'.$qfA['wfaIdBef'].',d:'.$L['wfaId'].'';
		self::err(_js::e(3,$ln.'La cantidad digitada ('.($L['quantity']*1).') es mayor a la cantidad completa en la fase anterior ('.($qf['compQty']*1).') de la orden de producción '.$L['docEntry'].$ori));
		return $qf;
	}
	return $qf;
}

static public function odp_fasePut($L=array(),$P=array()){
	$ln=($P['ln'])?$P['ln']:'';
	if($js=_js::ise($L['wfaId'],$ln.'Se debe definir wfaId para actualizar on wma3::odp_fasePut().')){ _err::err($js); return false; }
	else if($js=_js::ise($L['qSet'],$ln.'qSet is undefined on wma3::odp_fasePut().')){ _err::err($js); return false; }
	if($P['revOdp']=='Y'){/*Revisar estado de orden */
		$qc=a_sql::fetch('SELECT docStatus,tt,tr FROM wma3_oodp WHERE docEntry=\''.$L['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo información de la orden de producción',2=>'La orden de producción  '.$L['docEntry'].', no existe'));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); return false; }
		else if($qc['docStatus']=='C'){ _err::err(_js::e(3,$ln.'La orden de producción está cerrada, no se puede actualizar la fase.')); return false; }
		else if($qc['docStatus']=='N'){ _err::err(_js::e(3,$ln.'La orden de producción está anulada, no se puede actualizar la fase.')); return false; }
		if($qc['tt']=='wmaPdp'){ $qc['pdp_docEntry']=$qc['tr']; }
		self::$D=$qc;
	}
	/* openQty nunca puede ser mayor a lo programado, manejo excedentes */
	$qf=a_sql::query('UPDATE wma3_odp20
	SET '.$L['qSet'].',
	openQty=IF(openQty>quantity,quantity,openQty),
	qtyExcess=(compQty + rejQty - quantity)
	WHERE lineType=\'F\' AND docEntry=\''.$L['docEntry'].'\' AND wfaId=\''.$L['wfaId'].'\' AND itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' LIMIT 1',array(1=>$ln.'Error actualizado la fase en la orden de producción: '));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); return false; }
}
/* documento de fase */
static public function ddf_post($___D=array(),$P2=array()){
	$series=($P2['tt'])?$P2['tt']:'wmaDdf';
	$oTy=($P2['tr'])?$P2['tr']:0;
	if($P2['def_tt']){ $series=$P2['def_tr']; }
	if($P2['def_tr']){ $oTy=$P2['def_tr']; }
	unset($___D['serieId']);
	$systemFase=(array_key_exists('systemFase',$P2));
	if($systemFase){ $P2['def_wfaBefId']=$P2['systemFase']; }
	$isE=($___D['docType']=='E'); unset($___D['cardName']);
	$whs0=($___D['whsIdFrom']!='N999'); $js=false; $jsOk=false;
	if($js=_js::ise($___D['docDate'],'Se debe definir la fecha del documento')){}
	else if($isE && $js=_js::ise($___D['cardId'],'Se debe definir el tercero cuando es de tipo externo.','numeric>0')){}
	else if($js=_js::ise($___D['wfaId'],'Se debe definir la fase para el documento.','numeric>0')){}
	else if($js=_js::ise($___D['whsId'],'Se debe definir el almacen para proceso.','numeric>0')){}
	else if($whs0 && $js=_js::ise($___D['whsIdFrom'],'Se debe definir el almacen anterior.','numeric>0')){}
	else if(!is_array($___D['L']) || count($___D['L'])==0){ $js=_js::e(3,'No se enviaron lineas para el documento.'); }
	else{ $errs=0;
		$L2=$___D['L']; unset($___D['L']);
			_ADMS::mApps('wma/PeP'); $Liv=array();
			_ADMS::libC('wma','ivtPep');
			$docEntryN=$___D['docEntry']=a_sql::nextID('wma3_oddf');
			if(_err::$err){ $errs=1; }
			$ivtPep=new ivtPep(['hands'=>true,'tt'=>'wmaDdf','tr'=>$docEntryN,'docDate'=>$___D['docDate']]);
			$n=1; $odpNum=0;
		/* Revisar campos, inventario de fase, y estado de fase en odp */
		if(!_err::$err){ foreach($L2 as $k =>$L){
		$ln='Linea '.$n.': '; $n++;
			/* Definir fase anterior por defecto */
			if($P2['def_wfaBefId']){ $L['wfaIdBef']=$P2['def_wfaBefId']; }
			if($P2['def_tt']){ $L['tt']=$series; }
			if($P2['def_tr']){ $L['tr']=$oTy; }
			if($L['wfaIdBef']==0){ $L['wfaIdBef']=self::$V['wfaFS']; }
			$L2[$k]=$L;
			$wfaId0=($L['wfaIdBef']!=0);
			$diffWhs=($___D['whsIdFrom']!=$___D['whsId']);
			$revInv=($___D['wfaId']!=$L['wfaIdBef']);
			$movBef=$revInv;
			$L2[$k]['wfaIdBef']=$L['wfaIdBef'];
			$isFase0=($L['wfaId']==self::$V['wfaFS']);
			$isOdp=($L['tt']=='wmaOdp');/* Si tiene origen en odp, revisar */
			if($isOdp && $js=_js::ise($L['tr'],$ln.'Se debe definir el número de la orden de producción.')){ $errs++; break; }
			else if($js=_js::ise($L['wfaIdBef'],$ln.'Se debe definir la fase anterior del proceso. ('.$L['wfaIdBef'].')','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'La cantidad debe ser mayor a 0.','numeric>0')){ $errs++; break; }
			else{
				/* si odp -revisar estado fase a realizar y de la anterior */
				if($isOdp){
					if(!$isFase0){
						wma3::odp_faseAnt(array('docEntry'=>$L['tr'],'wfaId'=>$___D['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']),$ln);
						if(wma3::$err){ $errs++; $js=wma3::$errText; break; }
					}
					$qf=wma3::odp_fase(array('docEntry'=>$L['tr'],'wfaId'=>$___D['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']),$ln);
					if(wma3::$err){ $errs++; $js=wma3::$errText; break; }
					$odpNum++;
				}
				/* revisar inventario si es necesario y no es fase0 */
				if($L['wfaIdBef']!=self::$V['wfaFS'] && $revInv){
					$Lr=array('whsId'=>$___D['whsIdFrom'],'wfaId'=>$L['wfaIdBef'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']);
					$ivtPep->getInfo($Lr); if(_err::$err){ break; }
					$ivtPep->handRevi(['outQty'=>$L['quantity']]); if(_err::$err){ break; }
					$ivtPep->handSett(['outQty'=>$L['quantity']]); if(_err::$err){ break; }
					//PeP::onHand($L,array('revD'=>true, 'whsId'=>$___D['whsIdFrom'],'wfaId'=>$L['wfaIdBef'],'lnt'=>$ln));
					//if(PeP::$err){ $js=PeP::$errText; $errs++; break; }
					//$Liv[]=array('outQty'=>'Y','whsId'=>$___D['whsIdFrom'],'wfaId'=>$L['wfaIdBef'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']);
				}
				/* ingresar si es diferente o si es fase0 */
				if($revInv || $L['wfaIdBef']==wma3::$V['wfaFS']){
					$Lr=array('whsId'=>$___D['whsId'],'wfaId'=>$L['wfaIdBef'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']);
					$ivtPep->getInfo($Lr); if(_err::$err){ break; }
					$ivtPep->handRevi(['inQty'=>$L['quantity']]); if(_err::$err){ break; }
					$ivtPep->handSett(['inQty'=>$L['quantity']]); if(_err::$err){ break; }
					//$Liv[]=array('inQty'=>'Y','whsId'=>$___D['whsId'],'wfaId'=>$L['wfaIdBef'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']);
				}
			}
		}
		if(_err::$err && $errs==0){ $errs=1; $js=_err::$errText; }
		/* Actualizar estado de fase si es odp y liberar programado en pdp si fase0 */
		if($errs==0 && $odpNum>0){
			foreach($L2 as $k =>$L){ $ln='Linea '.$n.': '; $n++;
				if($L['tt']=='wmaOdp'){
					wma3::odp_fasePut(array('docEntry'=>$L['tr'],'wfaId'=>$___D['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'qSet'=>'openQty=openQty-'.$L['quantity'].', onProQty=onProQty+'.$L['quantity'].' '),array('revOdp'=>'Y'));
					if(wma3::$err){ $js=wma3::$errText; $errs++; break; }
					/*Actualizar pdp -prog si es fase0 */
					if(wma3::$D['pdp_docEntry'] && $L['wfaIdBef']==wma3::$V['wfaFS']){
						wma3::pdp_putLine(array('docEntry'=>wma3::$D['pdp_docEntry'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'qSet'=>'progQty=progQty-'.$L['quantity']));
							if(_err::$err){ $js=_err::$errText; $errs++; break; }
						}
				}
			}
		}
		/*mover inventario */
		if($errs==0 && count($ivtPep->Livt)>0){// actualizar inventario pep 
			a_sql::multiQuery($ivtPep->Livt);
			if(_err::$err){ $errs=1; $js=_err::$errText; }
		}
		/*Generar documento de fase*/
		if($errs==0){
			$ins=a_sql::insert($___D,array('table'=>'wma3_oddf','qDo'=>'insert','kui'=>'uid_dateC'));
			if($ins['err']){ $js=_js::e(3,'Error generando documento de fase: '.$ins['text']); $errs++; }
			else{
				$docEntryN=$ins['insertId']; $oTy=$docEntryN;
				foreach($L2 as $k =>$L){ $ln='Linea '.$n.': '; $n++;
					$L['docEntry']=$docEntryN;
					$L['openQty']=$L['quantity'];
					$ins2=a_sql::insert($L,array('table'=>'wma3_ddf1','qDo'=>'insert'));
					if($ins2['err']){ $js=_js::e(3,'Error generando linea de documento de fase: '.$ins2['text']); $errs++; break; }
				}
			}
		}
		if(a_ses::$userId==1){
		}
		if($errs==0){ $jsOk=_js::r('Documento generado correctamente.','"docEntry":"'.$docEntryN.'"');
			self::$Ds=array('docEntry'=>$docEntryN);
		}
		}
	}
	_err::errDie();
	if($js){ _err::err($js); }
	return $jsOk;
}
static public function ddf_put($L=array(),$ln=''){
	$ori =' on[wma3::ddf_put()]';
	if($js=_js::ise($L['nfId'],$ln.'Se debe definir ID de nota de fase.'.$ori,'numeric>0')){ _err::err($js); return false; }
	else if($js=_js::ise($L['qSet'],$ln.'qSet is undefined.'.$ori)){ _err::err($js); return false; }
	$qf=a_sql::query('UPDATE '._0s::$Tb['wma3_ddf1'].'
	SET '.$L['qSet'].'
	WHERE nfId=\''.$L['nfId'].'\' LIMIT 1',array(1=>$ln.'Error actualizado nota de fase.'.$ori));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); return false; }
	else{ return true; }
}
static public function ddf_getLine($L=array(),$P=array()){
	$ln=$P['ln']; $errs=0;
	$qf=a_sql::fetch('SELECT A.docStatus,A.canceled,A.cardId,A.whsId,A.whsIdFrom,A.wfaId,WF.wfaName,WF1.wfaName wfaNameBef,B.*
	FROM wma3_ddf1 B
	JOIN wma3_oddf A ON (A.docEntry=B.docEntry)
	LEFT JOIN wma_owfa WF ON (WF.wfaId=A.wfaId)
	LEFT JOIN wma_owfa WF1 ON (WF1.wfaId=B.wfaIdBef)
	WHERE B.nfId=\''.$L['nfId'].'\' LIMIT 1',array(1=>$ln.'Error obteniendo información de la nota.',2=>$ln.'La nota de fase no existe.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; }
	//else if($qf['docEntry']!=$docEntry){ $js=_js::e(3,$ln.'La nota de fabricación no corresponde con el documento de fase #'.$qf['docEntry']); $errs++;  }
	else if($qf['canceled']=='Y'){ $js=_js::e(3,$ln.'El documento está anulado, no se puede recibir la linea.'); $errs++; }
	else if($qf['docStatus']=='C'){ $js=_js::e(3,$ln.'El documento está cerrado, no se puede recibir la linea.'); $errs++;  }
	else if($L['quantity']>$qf['openQty']){ $js=_js::e(3,$ln.'La cantidad a recibir ('.$L['quantity'].') es mayor a la cantidad pendiente ('.($qf['openQty']*1).') del documento.'); $errs++; }
	if($errs){ _err::err($js); }
	$qf['wfaIdBef']=($qf['wfaIdBef']==self::$V['wfaFS'])?self::$V['wfaFS']:$qf['wfaIdBef'];
	return $qf;
}
/* Doc. cierre fase */
static public function dcf_post($___D=array(),$L2=array()){
	$errs=0; $ori=' on[wma3::dcf_post()]'; unset($___D['serieType']);
	$ins=a_sql::insert($___D,array('table'=>'wma3_odcf','qDo'=>'insert','kui'=>'uid_dateC'));
	if($ins['err']){ $js=_js::e(3,'Error guardando documento de cierre: '.$ins['text'].'.'.$ori); _err::err($js); $errs=1; }
	else{
		$docEntryN=$ins['insertId']; $n=1; /* guardar doc lines */
		foreach($L2 as $nk=>$L){ $ln='Linea '.$n.': '; $n++;
			$quantity=$L['quantity'];
			$L['docEntry']=$docEntryN;

			$L=self::dcf_lineTotalDef($L,$L,'lineTotal');
			unset($L['pdp_docEntry']);
			$ins2=a_sql::insert($L,array('table'=>'wma3_dcf1','qDo'=>'insert'));
			if($ins2['err']){ $js=_js::e(3,$ln.'Error guardando linea de documento: '.$ins2['text'].'.'.$ori); _err::err($js); $errs=1; break; }
		}
	}
	if($errs){ return false; }
	return array('docEntry'=>$docEntryN);
}
static public function dcf_lineTotalDef($L=array(),$qf=array(),$type='lineTotal'){
	if($type=='lineTotal'){
		$L['price']=$qf['cost'];
		$L['lineTotal']=$L['price']*$L['quantity'];
		$L['lineTotalMP']=$qf['costMP']*$L['quantity'];
		$L['lineTotalMO']=($qf['costMO']*$L['quantity'])+$qf['costMOF'];
		$L['lineTotalMA']=($qf['costMA']*$L['quantity'])+$qf['costMAF'];
		$L['lineTotalcif']=$qf['cif']*$L['quantity'];
		unset($L['cost'],$L['costMP'],$L['costMO'],$L['costMA'],$L['cif'],$L['costMOF'],$L['costMAF']);
	}
	else if($type=='define'){
		$L['cost']=$qf['cost'];
		$L['costMP']=$qf['costMP']; $L['cif']=$qf['cif'];
		$L['costMO']=$qf['costMO']; $L['costMOF']=$qf['costMOF'];
		$L['costMA']=$qf['costMA']; $L['costMAF']=$qf['costMAF'];
	}
	return $L;
}
/* fases y operaciones */
static public function mpg_fasePosi($P=array()){
	$qf=a_sql::fetch('SELECT M1.wfaId,M1.wfaIdBef,M1.wfaIdNext,M1.cost,M1.costMP,M1.costMO,M1.costMA,M1.costMOF,M1.costMAF,M1.cif
	FROM wma_mpg1 M1 
	WHERE M1.itemId=\''.$P['itemId'].'\' AND M1.wfaId=\''.$P['wfaId'].'\' LIMIT 1',array(1=>'Error obteniendo información on wma::mpg_fasePosi(): ',2=>'El artículo no tiene asignada la fase on wma::mpg_fasePosi().'));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); return false; }
	/*ojo! ejecuta wfaFS antes de esto */
	if($qf['wfaIdBef']==0){ $qf['wfaIdBef']=self::$V['wfaFS']; }
	return $qf;
}
static public function mpg_faseOrder($WFA1=array()){
	/*
	pasar todo
	*/
	$wfaLast=9999; $wfaBef=0; $nl=1;
	foreach($WFA1 as $n =>$L){
		//if($L['teoricTime']){ $L['teoricTime']=$L['timeBase']; }
		$ve=(($L['wfaId'].$L['teoricTime'].$L['costMP'].$L['costMO'].$L['costMA'].$L['cif']) != ''); /* wfa_mpg1 */
		if(!$ve){ unset($WFA1[$n]); }
		else{
			if(!$L['costMP']){ $WFA1[$n]['costMP']=0; }
			if(!$L['costMO']){ $WFA1[$n]['costMO']=0; }
			if(!$L['costMA']){ $WFA1[$n]['costMA']=0; }
			if(!$L['cif']){ $WFA1[$n]['cif']=0; }
			if(!$L['teoricTime']){ $WFA1[$n]['teoricTime']=0; }
		}
	}
	$nLen=count($WFA1);
	foreach($WFA1 as $n =>$L){
		$lastLine=($nl==$nLen);
		if($nl==1 && $L['delete']=='Y'){ $nl=1; $nLen=$nLen-1; continue; }
		if(!$lastLine && $L['delete']=='Y'){ $nl++; continue; }
		if($nl==1){ $WFA1[$n]['wfaIdBef']=0; }
		else{ $WFA1[$n]['wfaIdBef']=$WFA1[$nAnt]['wfaId']; }
		if($lastLine){ $WFA1[$n]['wfaIdNext']=$wfaLast; }
		if($nl>1){ $WFA1[$nAnt]['wfaIdNext']=$L['wfaId']; }
		if($lastLine && $L['delete']=='Y'){
			$WFA1[$nAnt]['wfaIdNext']=$wfaLast;
		}
		$nAnt=$n;
		$nl++;
	}
	return $WFA1;
}
static public function mpg_get($P=array(),$IL=array()){
	/* $q2=a_sql::query('SELECT B.lineNum,B.lineType,
	B.wfaId, WF.wfaName,
	B.wopId, WO.wopName,
	B.teoricTime,B.cost,B.lineMemo
	FROM '._0s::$Tb['wma_mpg1'].' B
	JOIN '._0s::$Tb['wma_mpg2'].' B2 ON (B2.mpgId=B.mpgId)
	LEFT JOIN '._0s::$Tb['wma_owfa'].' WF ON (WF.wfaId=B.wfaId)
	LEFT JOIN '._0s::$Tb['wma_owop'].' WO ON (WO.wopId=B.wopId)
	WHERE B2.itemId=\''.$P['itemId'].'\'
	ORDER BY B.lineNum ASC,CASE WHEN B.lineType=\'F\' THEN 1 END DESC',array(1=>'Error obteniendo fases y operaciones del artículo: ',2=>'El artículo no tiene definido fases y operaciones.')); */
	$q2=a_sql::query('SELECT B1.lineNum,B1.lineType,B1.canExcess,
	B1.wfaId, WF.wfaName,
	B1.wopId, WO.wopName,B1.teoricTime,B1.cost,B1.costMP,B1.costMO,B1.costMA,B1.cif,B1.whsId,B1.whsIdBef,B1.lineMemo
	FROM wma_mpg1 B1
	LEFT JOIN wma_owfa WF ON (WF.wfaId=B1.wfaId)
	LEFT JOIN wma_owop WO ON (WO.wopId=B1.wopId)
	WHERE B1.itemId=\''.$P['itemId'].'\'
	ORDER BY B1.lineNum ASC,CASE WHEN B1.lineType=\'F\' THEN 1 END DESC',array(1=>'Error obteniendo fases y operaciones del artículo: ',2=>'El artículo no tiene definido fases y operaciones.'));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); }
	else{
		if($P['r']=='query'){ return $q2; }
		else{
			$RL=array(); $rows=count($IL);
			while($L=$q2->fetch_assoc()){
				if($L['lineType']=='F'){ $L['wfaOrder']=$L['lineNum']; }
				else if($L['lineType']=='O'){ $L['wopOrder']=$L['lineNum']; }
				if($rows==0){ $RL[]=$L; }
				else{
					unset($L['lineNum']);
					foreach($IL as $iks => $Ld){
						$L['itemId']=$Ld['itemId'];
						$L['itemSzId']=$Ld['itemSzId'];
						$L['quantity']=$Ld['quantity'];
						$L['openQty']=$Ld['quantity'];
						$RL[]=$L;
					}
				}
			}
			return $RL;
		}
	}
}
}
?>