<?php
class PeP{
static $err=false;
static $errText='';
static $D=array();
static public function _reset(){
	self::$err=false; self::$errText='';
}
static public function onHand_get($D=array(),$P=array()){
	$P['ivtGet']='Y';
	return self::onHand($D,$P);
}
static public function onHand($D=array(),$P=array()){
	$lnt=$P['lnt']; self::_reset(); $ori=' on[Pep::onHand]';
	$ivtGet=($P['ivtGet']); /*obtener solo */
	$revI=($P['revD']);
	$tb_oitw='pep_oitw'; $tb_wtr1='pep_wtr1';
	if(!array_key_exists('whsId',$P) && $D['whsId']){ $P['whsId']=$D['whsId']; }
	if(!array_key_exists('wfaId',$P) && $D['wfaId']){ $P['wfaId']=$D['wfaId']; }
	if($revI){ $ert=$lnt.'Error en consulta inventario en proceso. ';
		if($js=_js::ise($D['itemId'],$ert.'Se debe definir el artículo.')){
			self::$err=true; self::$errText=$js; return self::$err;
		}
		else if($js=_js::ise($D['itemSzId'],$ert.'Se debe definir la talla.'.$ori)){
			self::$err=true; self::$errText=$js; return self::$err;
		}
		else if($js=_js::ise($P['whsId'],$ert.'Se debe definir la bodega.'.$ori)){
			self::$err=true; self::$errText=$js; return self::$err;
		}
		else if($js=_js::ise($P['wfaId'],$ert.'Se debe definir la fase. ('.$P['wfaId'].')'.$ori)){
			self::$err=true; self::$errText=$js; return self::$err;
		}
	}
	$qul='SELECT I.itemCode,G1.itemSize,W1.onHand, W.whsName whsCode,WF.wfaName 
	FROM ivt_owhs W
	JOIN itm_oitm I ON (I.itemId=\''.$D['itemId'].'\')
	LEFT JOIN itm_grs1 G1 ON (G1.itemSzId=\''.$D['itemSzId'].'\')
	LEFT JOIN '.$tb_oitw.' W1 ON (W1.whsId=W.whsId AND W1.wfaId=\''.$P['wfaId'].'\' AND W1.itemId=\''.$D['itemId'].'\' AND W1.itemSzId=\''.$D['itemSzId'].'\' )
	LEFT JOIN wma_owfa WF ON (WF.wfaId=\''.$P['wfaId'].'\')
	WHERE W.whsId=\''.$P['whsId'].'\' LIMIT 1';
	$qS = a_sql::fetch($qul,array(1=>'Error obteniendo inventario de producto en proceso: '));
	$enS='whs:'.$P['whsId'].', wfa:'.$P['wfaId'].'';
	$errType=0;
	if(a_sql::$err){ self::$err=true; self::$errText=a_sql::$errNoText; return false; }
	else if(a_sql::$errNo==2){ self::$err=true; $errType='onHand'; 
		$errText= $lnt.'La bodega ('.$P['whsId'].') para el movimiento no existe.'.$enS.$ori; 
	}
	else if($ivtGet){ /*omitir revision */ }
	else if($qS['onHand']<$D['quantity']){ $errType='onHand'; 
		$errText=$lnt.'La cantidad a transferir del articulo '.$qS['itemCode'].'-'.$qS['itemSize'].' ('.($D['quantity']*1).') del almacen '.$qS['whsCode'].' en fase '.$qS['wfaName'].' es mayor a la disponible ('.($qS['onHand']*1).'). '.$enS.$ori; self::$err=true; }
	if($errType=='onHand'){
		if($P['errOnHand']){ self::$errText=_js::e(3,$P['errOnHand'].': '.$errText); }
		else{ self::$errText=_js::e(3,$errText); }
	}
	if($ivtGet && a_sql::$errNo==-1){
		/* -5 ajusta sistema a menos */
		$qS['diff']=$D['quantity']-$qS['onHand']*1;
	}
	self::$D=$qS;
	return self::$err;
}
static public function onHand_put($D=array(),$P=array()){
	self::_reset(); $ori=' on[PeP.onHand_put()]';
	//_ADMS::_app('Mate');
	$tb_oitw='pep_oitw'; $tb_wtr1='pep_wtr1';
	if(self::$errText=_js::ise($P['docDate'],'Se debe definir fecha para movimiento.'.$ori)){ self::$err=true; }
	else if(self::$errText=_js::ise($P['tt'],'TT ('.$P['tt'].') debe estar definido para movimiento.'.$ori)){ self::$err=true; }
		else if(self::$errText=_js::ise($P['tr'],'TR ('.$P['tr'].') debe estar definido para movimiento.'.$ori)){ self::$err=true; }
	else if(!is_array($D)){ self::$err=true; self::$errText=_js::e(3,'No se recibieron cantidades para actualizar inventario en proceso.'.$ori); }
	else{ $nl=1;
	foreach($D as $n => $L){ $ln='Linea '.$nl.': '; $nl++;
		if(self::$errText=_js::ise($L['whsId'],$ln.'La bodega debe estar definida.'.$ori)){ self::$err=true; break; }
		else if(self::$errText=_js::ise($L['itemId'],$ln.'ID artículo debe estar definido.'.$ori)){ self::$err=true; break; }
		else if(self::$errText=_js::ise($L['itemSzId'],$ln.'ID talla de artículo debe estar definido.'.$ori)){ self::$err=true; break; }
		else if(self::$errText=_js::ise($L['wfaId'],$ln.'ID fase debe estar definida.'.$ori)){ self::$err=true; break; }
		else if(self::$errText=_js::ise($L['quantity'],$ln.'La cantidad debe estar definida.'.$ori)){ self::$err=true; break; }
		$inQty=$outQty=0;
		if($L['inQty']=='Y'){ $inQty=$L['quantity']; }
		if($L['outQty']=='Y'){ $outQty=$L['quantity']; }
		unset($L['stockValue'],$L['inQty'],$L['outQty']);
		$Dm = array('docDate'=>$P['docDate'],'tt'=>$P['tt'],'tr'=>$P['tr'],'whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'], 'itemSzId'=>$L['itemSzId'],'inQty'=>$inQty,'outQty'=>$outQty,'quantity'=>$L['quantity'],'stockValue'=>$L['lineTotal']);
		$ins = a_sql::insert($Dm,array('qDo'=>'insert','table'=>$tb_wtr1));
		if($ins['err']){ self::$errText= _js::e(3,$ln.'Error registrando movimientos wtr1: '.$ins['text'].$ori); self::$err=true; break; }
		$wh = 'WHERE whsId=\''.$L['whsId'].'\' AND wfaId=\''.$L['wfaId'].'\' AND itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' LIMIT 1';
		$L['._.onHand']='+-+onHand+'.$inQty.'-'.$outQty;##pasar + o -
		$L=Mate::stockValue($L,array('in'=>$inQty,'out'=>$outQty));
		/* insert */
		$L['avgPrice']=$L['lineTotal']/$L['quantity'];
		$L['stockValue']=$L['lineTotal'];
		$L['onHand']=$inQty-$outQty;
		unset($L['quantity'],$L['delete'],$L['lineTotal'],$L['price']);
		$ins2=a_sql::insert($L,array('table'=>$tb_oitw,'wh_change'=>$wh));
		if($ins2['err']){ self::$errText= _js::e(3,$ln.'Error actualizando cantidades: '.$ins2['text'].$ori); self::$err=true; break; }
	}
	}
	return self::$err;
}
static public function onHand_rever($P=array()){
	$js=false;
	if($js=_js::ise($P['tt'],'Se debe definir TT on Pep::onHand_rever().')){}
	else if($js=_js::ise($P['tr'],'Se debe definir TT on Pep::onHand_rever().')){}
	else if($js=_js::ise($P['docDate'],'Se debe definir la fecha de la reversión Pep::onHand_rever().')){}
	else{
		_ADMS::libC('wma','ivtPep');
		$ivtPep=new ivtPep(['hands'=>true,'tt'=>$P['tt'],'tr'=>$P['tr'],'docDate'=>$P['docDate']]);
		/*primero salidas */
		$q=a_sql::query('SELECT whsId,wfaId,itemId,itemSzId,inQty,outQty,stockValue lineTotal
		FROM pep_wtr1 WHERE tt=\''.$P['tt'].'\' AND tr=\''.$P['tr'].'\' ORDER BY outQty DESC',array(1=>'Error obteniendor registros para revertir movimientos de inventario de producto en proceso: '));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			while($L=$q->fetch_assoc()){
				$ivtPep->getInfo($L); if(_err::$err){ break; }
				if($L['inQty']>0){
					$ivtPep->handRevi(['outQty'=>$L['inQty']]); if(_err::$err){ break; }
					$ivtPep->handSett(['outQty'=>$L['inQty']]); if(_err::$err){ break; }
					$L['quantity']=$L['inQty']; $L['outQty']='Y'; unset($L['inQty']);
				}
				else if($L['outQty']>0){ 
					$ivtPep->handRevi(['inQty'=>$L['outQty']]); if(_err::$err){ break; }
					$ivtPep->handSett(['inQty'=>$L['quantity']]);
					$L['quantity']=$L['outQty']; $L['inQty']='Y'; unset($L['outQty']);
				}
			}
			if(!_err::$err){ a_sql::multiQuery($ivtPep->Livt); }
		}
	}
	if($js){ _err::err($js); return false; }
}

static public function egrDoc_post($_J=array(),$P=array()){
	$oriB=' on[PeP::egrDoc_post()]';
	$rev=($P['rev']=='N')?false:true;
	if($rev && $js=_js::ise($_J['docDate'],'La fecha del documento debe estar definida.'.$oriB,'Y-m-d')){ _err::err($js); }
	else if($rev && $js=_js::ise($_J['docType'],'Se debe definir el tipo de documento.'.$oriB)){ _err::err($js); }
	else if(!_js::textLimit($_J['lineMemo'],200)){ $js= _js::e(3,'Los detalles no pueden exceder los 100 caracteres.'.$oriB);  _err::err($js);  }
	else if(!is_array($_J['L'])){ $js=_js::e(3,'No se han enviado lineas para el documento.'.$oriB);  _err::err($js);  }
	else{
		$Ld=$_J['L']; unset($_J['L'],$_J['serieId']); $errs=0; $n=1;
		$rows=0;
		$Di=array(); $Liv=array(); $nl=1;
		foreach($Ld as $nk=>$L){
			$ori =($P['kText'])?' '.$P['kText'].'='.$L[$P['kText']].'.'.$oriB:$oriB;
			$totaln=0; $ln =($P['ln']=='N')?'':'Linea '.$nl.': '; $nl++;
			if($L['quantity']==''){ $Ld[$nk]['delete']='Y'; continue; }
			if($js=_js::ise($L['itemId'],$ln.'No se ha encontrado el ID del artículo.'.$ori,'numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['itemSzId'],$ln.'No se ha definido el ID de la talla.'.$ori)){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'La cantidad debe ser mayor a 0.'.$ori,'numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['whsId'],$ln.'Se debe definir la bodega de salida.'.$ori,'numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['wfaId'],$ln.'Se debe definir la fase de salida.'.$ori,'numeric>0')){ $errs++; break; }
			else{
				self::onHand($L,array('whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'lnt'=>$ln));
				if(self::$err){ $js=self::$errText; $errs++; break; }
				else{
					unset($L['itemCode'], $L['handInv']);
					$Di[$n]=$L;
					$Di[$n]['lineNum']=$n;
					$Liv[]=array('whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity'],'outQty'=>'Y');
				}
			}
			$n++;
		}
		$rows=count($Di);
		if($errs==0 && ($rows==0)){ $js=_js::e(3,'No se ha definido ninguna cantidad para dar salida. ('.$rows.')'.$oriB); }
		else if($errs==0){
		$ins=a_sql::insert($_J,array('kui'=>'ud','table'=>'pep_oegr','qDo'=>'insert'));
		if($ins['err']){ $js=_js::e(1,'Error guardando documento pep.egr: '.$ins['text'].$oriB); }
		else{
			$docEntry=$ins['insertId'];
			$jsAdd='"docEntry":"'.$docEntry.'"';
			foreach($Di as $nk=>$L2){
				$n=$L2['lineNum']; $L2['docEntry']=$docEntry;
				$ins2=a_sql::insert($L2,array('table'=>'pep_egr1','qDo'=>'insert'));
				if($ins2['err']){
					$js=_js::e(1,'Linea '.$n.': '.$ins2['text'].$oriB,$jsAdd);
					$errs++; break;
				}
			}
			if($errs==0){
				self::onHand_put($Liv,array('docDate'=>$_J['docDate'],'tt'=>'pepEgr','tr'=>$docEntry));
				if(self::$err){ $js=self::$errText; $errs++;; }
				else if($errs==0){
					$js=_js::r('Información guardada correctamente.',$jsAdd);
				}
			}
		}
		}
	}
	return $js;
}
}
?>