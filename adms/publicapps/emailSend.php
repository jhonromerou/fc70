<?php
/* Utilizar curl para enviar esto
req: templateCode, ocarCode
HB[
variables a reemplazar en plantilla
]
*/
_ADMS::lib('a_mdl');
a_sql::dbase(c::$Sql2);
a_mdl::login($_POST['ocardCode']);
$fromTem=(array_key_exists('templateCode',$_POST));
if($fromTem && $js=_js::ise($_POST['templateCode'],'templateCode no definida.')){ die($js); }
else if(!$fromTem && $js=_js::ise($_POST['body'],'El cuerpo del mensaje no ha sido definido.')){ die($js); }

function getMailF($D=array(),$P=array()){
	$P=(!is_array($P))?json_decode($P,1):$P;
	if(!is_array($P)){ $P=array(); }
	foreach($P as $k=>$v){ $D[$k]=$v; }
	return $D;
}

function file2D($filex=''){
	$g=@json_decode(file_get_contents($filex),1);
	if(!is_array($g)){ return array(); }
	return $g;
}
$q0=a_mdl::oaut($_POST);
$ocardId=$q0['ocardId'];
_ADMS::lib('sql/filter');
/* Obtener datos desde archivo js +*/
if(array_key_exists('file_jsData',$_FILES)){
	$_POST['C']=file2D($_FILES['file_jsData']['tmp_name'][0]);
	unset($_FILES['file_jsData']);
}
$fiSend=($_FILES['file'])?$_FILES['file']:$_FILES;
/* Definir cuerpo */
$Ds=array();
if($fromTem){
	_ADMS::lib('Mailing');
	Mailing::$tbTemp='z_ema_otmp';
	$_POST['C']['ocardName']=$q0['ocardName'];
	$Ds['html']=Mailing::fromTemp(array('tCode'=>$_POST['templateCode']),$_POST['C'],array('encodeHTML'=>'N'));
}
else{
	$Ds['html']=$_POST['body']; unset($_POST['body']);
}
/* Enviar */
if(_err::$err){ echo _err::$errText; }
else{
		_ADMS::lib('_2d,_Mailer,xCurl');
	$Ds['subject'] = 'Notificación';
	$Ds['fromName'] = 'Notificación';
	$Ds['mailTo']=$_POST['mailTo'];
	$s=explode(',',$Ds['mailTo']);
	$sN=false;
	if($Ds['mailToName']){
		$sN=explode(',',$Ds['mailToName']);
	}
	$sends=0;
	$Ds=getMailF($Ds,$q0['jsPars']);
	$fromNameAnt=$Ds['fromName'];
	foreach($s as $n=>$to){
		$Ds['mailTo']=$to;
		if($sN && $sN[$n]){ $Ds['fromName']=$sN[$n]; }
		else{ $Ds['fromName']=$fromNameAnt; }
		//$s = _Mail::send($Ds,array('F'=>$fiSend));
		$s = _Mail::curl($Ds);
		if(is_array($s) && $s['errNo']){}
		else{ $sends++; }
	}
	if($sends>0){
		a_mdl::ctrl(array('ocardId'=>$ocardId,'variName'=>'emailSends','qty'=>$sends));
		echo _js::r('Corres enviados correctamente.');
	}
	else{ echo _js::e(3,'No se envió el correo a los destinatarios ('.$L['email'].'). '.$s['text']); }
}
?>
