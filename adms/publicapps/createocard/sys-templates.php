<?php
/* Crear tablas base del sistema */
c::$Sql2=array(
 'sql_h'=>'sql1.admsistems.com',
 'sql_u'=>'root',
 'sql_p'=>'Pepa1992--',
 'sql_db'=>'adms'
);
header('content-type:text/html');
//a_sql::dbase(c::$Sql2);
$charsett='ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci';
{
$X=array(
	['k'=>'doc1','t'=>'Documento Basico','sql'=>'CREATE TABLE . (
`docEntry` mediumint unsigned auto_increment, primary key(`docEntry`),
`userId` smallint unsigned,
`dateC` datetime,
`serieId` smallint unsigned default 0,
`docNum` int unsigned default 0,
`ott` varchar(16) default  \'\',
`otr` int unsigned default 0,
`docType` varchar(1) default  \'\',
`docClass` mediumint unsigned,
`docStatus` enum(\'O\',\'C\',\'N\') default \'O\',
`docDate` date,
`dueDate` date,
`closeDate` date,
`canceled` enum(\'N\',\'Y\') default \'N\',
`cancelDate` date,
`userUpd` smallint unsigned,
`dateUpd` datetime,
`docTitle` varchar(200) not null default  \'\',
`lineMemo` varchar(1000) not null default  \'\',
#posibles
`lineNums` smallint unsigned not null default 0,
`cmtsNum` smallint unsigned not null default 0,
`filesNum` smallint unsigned not null default 0,
`acmNum` smallint unsigned not null default 0,
`lineMemoClose` varchar(1000) not null default  \'\'
) '.$charsett.';

CREATE TABLE . (
 `id` int unsigned auto_increment, primary key(`id`),
 `docEntry` mediumint unsigned,
 `lineNum` smallint unsigned default 0,
 `lineStatus` enum(\'O\',\'C\') default \'O\',
 `lineType` smallint unsigned,
 `lineDate` date,
 `lineAssg` varchar(50) not null default \'\'
 `lineText` varchar(500) not null default \'\'
) '.$charsett.';
'],
	['k'=>'app_ofrm','t'=>'Estructura de form en otras tablas basado en plantilla','d'=>' usar _GET[tbk y tbk1] para dibujar bien','sql'=>' #Plantillas
 create table `'.$_GET['tbk'].'` like pr_adms.app_ofrm;
 create table `'.$_GET['tbk1'].'` like pr_adms.app_frm1;
 #Respuestas
 create table `'.$_GET['tbk'].'r` like pr_adms.app_ofrmr;
 create table `'.$_GET['tbk1'].'r` like pr_adms.app_frmr1;
 #Planificacion/Participantes
 create table `'.$_GET['tbk'].'p` like pr_adms.app_ofrmp;
 create table `'.$_GET['tbk1'].'p` like pr_adms.app_frmp1;
']
);
}
{ /* buscar para crear truncate */
echo '<style>
table{ border-collapse:collapse; }
td{  border:1px solid #000; }
</style>
<table>
<tr>
<td>k</td><td>Titulo</td> <td>Descripción</td>
</tr>';
$sqlTxt='';
foreach($X as $n=>$L){
	$css='';
	if($_GET['print'] && $L['k']==$_GET['print']){
		$sqlTxt = 'use DBN;'."\n".$L['sql'];
	}
	$css1=$css;
	echo '<tr>
	<td'.$css1.'><a href="?print='.$L['k'].'">'.$L['k'].'</td>
	<td'.$css1.'>'.$L['t'].'</td>
	<td'.$css1.'>'.$L['d'].'</td>
	</tr>';
}
echo '</table>
<textarea style="width:100%; height:500px;">'.$sqlTxt.'</textarea>
';
}
?>
