<?php
/* Crear tablas base del sistema */
$mdlCnf='';
$Mdls=array(
	'crd'=>'Y',
	'itm'=>'Y', 'ivt'=>'Y',
	'gvt'=>'Y','dfe'=>'N',
	'gvtPur'=>'N',
	'gfi'=>'Y',
	'gvp'=>'Y',
	'wma'=>'N',
	'crm'=>'N',
	'gfp'=>'N',
	'gda'=>'N',
	'geDoc'=>'N',
	'mpa'=>'N',
	'gsn'=>'N'
);
$MdlEspecial=[
	'itmSrc'=>'N',
	'ivtMWhs'=>'N',
	'ivtSubItems'=>'N',
	'gfiAcc'=>'N',
];
$mdlCnfEspecial= '';
foreach($MdlEspecial as $k=>$v){
	$mdlCnfEspecial .='"'.$k.'":"'.$v.'",';
};
a_sql::dbase(c::$Sql2);
$dbA='adms'; /* root tables */
$dbIni='pr_adms';
$ocardcode='domicpollo';
$ocardName='Domicpollo';
$ocardId=30;
$cnfPlan='unclicc.p29';
$dbName='mapp_c'.$ocardId; /* nombre nuevo */
$logo='https://firebasestorage.googleapis.com/v0/b/unclicc4.appspot.com/o/'.$ocardcode.'%2F'.$ocardcode.'.png?alt=media';
$DatosCard=['i',1=>$dbName.'.a0_mecrd',
'ocardName'=>$ocardName,
'licTradType'=>'dni','licTradNum'=>'56062140',
'tipEnt'=>'PN','regTrib'=>'RS',
'pbx'=>'310 8912757','mail'=>'@',
'countyCode'=>'44','cityCode'=>'44430','address'=>'#',
'hacienda'=>'','web'=>'www','logo'=>$logo];
$_J=array(
 'dbName'=>$dbName,
	//adms.ocard
	'OC'=>array(
		'ocardcode'=>$ocardcode,'ocardName'=>$ocardName,'ocardType'=>'C',
		/**
		  DIGITAR */
		'soffName'=>'unClicc', 'softPlan'=>'P29',
		'sqlBackup'=>'Y','cartDays'=>10,'cardId'=>$ocardId,
		'sqldb'=>$dbName,'sqlh'=>'sql1.admsistems.com','sqlu'=>'root','sqlp'=>'Pepa1992--',
		'errMsg'=>'N',
		'mailConf'=>'{}',
	),
	/* login */
	'LOGIN'=>['ocardcode'=>$ocardcode,'wDomain'=>$ocardcode.'.unclicc.com','srcLogo'=>$logo,'css'=>'body{ background-color:gray !important; }'
	],
	/* Storage */
	'STOR'=>array('storMnt'=>0,'storOpen'=>0,'storUsaged'=>0,'fileMaxSize'=>1,'svr'=>'FB','filePath'=>'/mnt/vol2/'.$ocardcode,'storBucket'=>'','httpAu'=>'unclicc.com',
	'fireBase'=>'{
"fpath":"'.$ocardcode.'",
"user":"firebase@unclicc.com", "pass":"Alpaca8914",
"Cnf":{
 "apiKey": "AIzaSyD3mSIVvXIyspsIZh00Q4gKS84ilXFQSzI",
 "projectId": "unclicc4",
 "storageBucket": "unclicc4.appspot.com"
}
}'
	)
);
$_J['OC']['ocardId']=$ocardId;
$tbs_tables='information_schema.tables';
$tbs_cols='information_schema.columns';
$fies='';
$sqlExec=false;
$quBa=array('_sys_'=>[],'TB'=>array());
foreach($Mdls as $k=>$status){
	if($status!='N'){ //basic, all
		include('sys-mdl-'.$k.'.php');
	}
}
/* especiales */
$mdlCnf .=$mdlCnfEspecial;
$qC='';
a_sql::query('SET @@SQL_MODE=\'ONLY_FULL_GROUP_BY,NO_ENGINE_SUBSTITUTION,,NO_UNSIGNED_SUBTRACTION\' ');

/** 1- Registrar cliente en ADMS con modulos activos, etc */
if($_GET['q']=='1-card'){
 $errs=0;
 $tDa=array('ocardId'=>$ocardId,'permsXUser'=>'N',
	'fileFromCache'=>'N',
	'jsLibType'=>'D',
	'jsLoad'=>'{"app":"0cnf/'.$cnfPlan.'"}',
	'mdlCnf'=>'{'.substr($mdlCnf,0,-1).'}'
	);
	die(print_r($tDa));
 a_sql::transaction(); $cmt=false;
	// .ocard
 	a_sql::qInsert($_J['OC'],array('tbk'=>$dbA.'.ocard'));
	if(a_sql::$err){ $js=a_sql::$errText; $errs=1; }
	// .loadMdl
	if($errs==0){
		a_sql::uniRow($tDa,
		array('tbk'=>$dbA.'.loadMdl','wh_change'=>'ocardId=\''.$ocardId.'\' LIMIT 1'));
		if(a_sql::$err){ $js=_js::e(3,'loadMdl: '.a_sql::$errText); $errs=1; }
 }
	// .mdl_stor
	if($errs==0){
		$_J['STOR']['ocardId']=$ocardId;
		a_sql::uniRow($_J['STOR'],
		array('tbk'=>$dbA.'.mdl_stor','wh_change'=>'ocardId=\''.$ocardId.'\' LIMIT 1'));
		if(a_sql::$err){ $js=_js::e(3,'mdl_stor: '.a_sql::$errText); $errs=1; }
	}
	// .oclogin
	if($errs==0){
		a_sql::uniRow($_J['LOGIN'],
		array('tbk'=>$dbA.'.oclogin','wh_change'=>'ocardcode=\''.$ocardcode.'\' LIMIT 1'));
		if(a_sql::$err){ $js=_js::e(3,'oclogin: '.a_sql::$errText); $errs=1; }
	}
	if($errs==0){ a_sql::transaction(true);
		$qC='ocardCode '.$_J['OC']['ocardcode'].' creado con # '.$ocardId;
	}
	else{ echo $js; }
	print_r($tDa);
}
/** 2- Crear schema y duplicar tablas SQL-txt*/
else if($_GET['q']=='2-schema'){
 //a_sql::query('DROP SCHEMA '.$dbName);
 $qC .= "\n".'CREATE SCHEMA `'.$_J['dbName'].'` DEFAULT CHARACTER SET utf8mb4  COLLATE=utf8mb4_general_ci;';
 $q = a_sql::query('SELECT table_name tbName FROM '.$tbs_tables.' S WHERE S.table_schema=\''.$dbIni.'\'',array(1=>'Error obteniendo listado de tablas: ',2=>'No se encontraron tablas'));
 foreach($quBa['TB'] as $tbk => $v){
  $qC .="\n".'CREATE table IF NOT EXISTS `'.$_J['dbName'].'`.`'.$tbk.'` LIKE `'.$dbIni.'`.`'.$tbk.'`;';
 }
 while($L=$q->fetch_assoc()){
  if(!array_key_exists($L['tbName'],$quBa['TB'])){
 	$qC .="\n".'CREATE table  IF NOT EXISTS `'.$_J['dbName'].'`.`'.$L['tbName'].'` LIKE `'.$dbIni.'`.`'.$L['tbName'].'`;';
  }
 }
 echo '-->
 Resultado: '.print_r(a_sql::$DB->multi_query($qC),1).'';
}

/** 3- Inicializar Variables en tablas de modulos SQL-txt */
else if($_GET['q']=='3-ini'){
	$quBa['_sys_']=array('i'=>[
  array('i',1=>$dbName.'.a0_vs0_ousr','userId'=>1,'userName'=>'Admin','userEmail'=>'admin@admin.com','user'=>'supersu','password'=>'ppaa'),
	array('i',1=>$dbName.'.a0_vs0_ousr','userId'=>2,'userEmail'=>'@',
	'userName'=>'Elvia Gonzalez','user'=>'elvia','password'=>'1234'),
  array('i',1=>$dbName.'.a0_vs0_ousp','userId'=>1,'cards'=>'all','slps'=>'all'),
	array('i',1=>$dbName.'.a0_vs0_ousp','userId'=>2,'cards'=>'all','slps'=>'all'),
	array('i',1=>$dbName.'.a1_ojsv','kMdl'=>'par','kObj'=>'parDpto','value'=>'General'),
	$DatosCard /* me_crd*/
 ]);
	$qC='';
 foreach($quBa as $mdl => $X){
		$qC .= '# Modulo '.$mdl."\n";
		if($X['txt']){ $qC .=$X['txt']; }
		if($X['i']){ $qC .= a_sql::toQuerys($X['i']); }
	}
 echo '--> Resultado: '.print_r(a_sql::$DB->multi_query($qC),1).'';
}
/**
 x10- Resetear DB mucho cuidado */
 else if($_GET['q']=='x10-delete'){
		$ocardcode='demo1';
  die('Eliminando ocardcode: '.$ocardcode.'. No se puede eliminar, revisar todo');
  $errs=0;
  $Da=a_sql::fetch('SELECT ocardId,sqldb FROM adms.ocard WHERE ocardcode=\''.$ocardcode.'\' LIMIT 1');
  a_sql::query('DROP database '.$Da['sqldb']); if(a_sql::$errNo){ $errs=1; }
  $rw=a_sql::$DB->multi_query('use adms;
  DELETE FROM ocard WHERE ocardId=\''.$Da['ocardId'].'\' LIMIT 1;
  DELETE FROM loadMdl WHERE ocardId=\''.$Da['ocardId'].'\' LIMIT 1;
  ');
  print_r($rw);
 }
echo "\n
Modulos: ".$mldsTxt."
------------------\n\n".$qC;
?>
