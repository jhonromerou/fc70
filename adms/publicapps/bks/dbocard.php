<?php
_ADMS::lib('_Sys,_File');
a_sql::dbase(c::$Sql2);
date_default_timezone_set('America/Bogota');
header('content-type:text/html');
$ayer=date('Y-m-d',strtotime('-0 days'));
$todayA=date('Y-m-d');
$todayTime=strtotime($todayA.' H:00');
$dirB='/var/www/_bk/'; /* directorio de copias, añade ocardcode/bk.up */
/*Actualizar lo que sea diferente al dia a pendiente */
$wh=($_GET['dbmanual'])
?'A.ocardCode=\''.$_GET['dbmanual'].'\''
:'(BU.lastBackup<=\''.$ayer.' 00:00\' OR BU.lastBackup IS NULL)';
$q=a_sql::query('SELECT A.ocardcode,A.sqlh h,A.sqlu u,A.sqlp p, A.sqldb db,
BU.sqlRepetDays
FROM ocard A
LEFT JOIN obks BU ON (BU.ocardcode=A.ocardcode)
WHERE '.$wh.' AND A.sqlBackup=\'Y\' LIMIT 1',
array(1=>'Error obteniendo consultas: ',2=>'No hay copias por ejecutar '.$ayer));
if(a_sql::$err){ die(a_sql::$errNoText); }
$date=date('Y_m_d_H_i_s');
$timeIni=date('Y-m-d H:i:s');
while($L=$q->fetch_assoc()){
	echo $L['ocardcode'].'<br/>';
	$nextDay=$todayTime+(86400*$L['sqlRepetDays']);/* proxima fecha */
	$DIR=$dirB;
	$Di=array('dateC'=>$timeIni,'logType'=>'ok','tt'=>'bkSql','ocardCode'=>$L['ocardcode'],'sqlFile'=>$saveFile);
	$saveFile=$DIR.$L['ocardcode'].'-'.$date.'.sql.gz';
	$linux=' | gzip';
	//$linux='';
	$qe='(mysqldump -h '.$L['h'].' -u '.$L['u'].' -p'.$L['p'].' --default-character-set=utf8 --single-transaction '.$L['db'].''.$linux.' > "'.$saveFile.'")';
	
	_Sys::_exec('test -d '.$DIR.' || mkdir -p '.$DIR.'');
	if(_Sys::$err){ $Di['logType']='error'; $Di['lineMemo']=_Sys::$errText; }
	else{
		$re=_Sys::_exec($qe);
		$Di['fileSizeMb']=_File::getSizeMb(filesize($saveFile));
		$dateC=date('Y-m-d H:i:s');
		if(_Sys::$err){ $Di['logType']='error'; $Di['lineMemo']=_Sys::$errText; }
		else{
			a_sql::uniRow(['ocardcode'=>$L['ocardcode'],'timeIni'=>$timeIni,'lastBackup'=>$dateC,'lastSize'=>$Di['fileSizeMb']],['tbk'=>'obks','wh_change'=>'ocardcode=\''.$L['ocardcode'].'\' LIMIT 1']);
		}
	}
	echo $L['ocardcode'].' ('.file_exists($saveFile).' / '.$Di['fileSizeMb'].'Mb) on '.$saveFile;
	if($_GET['view']=='Y'){
		echo '<br/><textarea>'.$qe.'</textarea><br/>';
	}
}
?>
