<?php
if(_0s::$router=='GET rep/stockValue'){
	$___D['W.whsId']=$___D['whsId'];
	$___D['W.wfaId']=$___D['wfaId'];
	$docDate=$___D['docDate'];
	unset($___D['docDate'],$___D['whsId'],$___D['wfaId']);
	_ADMS::_lb('sql/filter');
	$wh=a_sql_filtByT($___D);
	$gb='I.itemCode,I.itemName,I.udm,W.itemSzId,W.whsId,W.wfaId,W.onHand,W.avgPrice,W.stockValue,PC.sumCost,PC.sumCostMP,PC.sumCostMO,PC.sumCostSV,PC.sumCostMA,PC.sumCif';
	$js= a_sql::queryL('SELECT '.$gb.'
	FROM pep_oitw W
	JOIN itm_oitm I ON (I.itemId=W.itemId)
	LEFT JOIN ivt_owhs WH ON (WH.whsId=W.whsId)
	LEFT JOIN itm_oipc PC ON (PC.itemId=I.itemId AND PC.itemSzId=W.itemSzId AND PC.wfaId=W.wfaId)
	WHERE W.onHand>0 '.$wh);
	echo $js;
}

else if(a_ses::$userId==10000000 && _0s::$router=='GET rep/handAt'){ a_ses::hashKey('pepRep.handAt');
	if($js=_js::ise($___D['docDate'],'Se debe definir la fecha a consultar.')){}
	else{
		$q=a_sql::query('SELECT M1.itemId
		FROM wma_mpg1 M1
		WHERE 1 GROUP BY M1.itemId ');
		_ADMS::mapps('wma/Cost');
		while($L=$q->fetch_assoc()){
			wmaCost::sumTo($L);
			if(_err::$err){ $js=_err::$errText; $errs=1; }
		}
	}
}

else if(_0s::$router=='GET rep/handAt'){
	if($js=_js::ise($___D['docDate'],'Se debe definir la fecha a consultar.')){}
	else{
		$___D['W.whsId']=$___D['whsId'];
		$___D['W.wfaId']=$___D['wfaId'];
		$docDate=$___D['docDate'];
		unset($___D['docDate'],$___D['whsId'],$___D['wfaId']);
		_ADMS::_lb('sql/filter');
		$wh=a_sql_filtByT($___D);
		$gb='I.itemCode,I.itemName,W.itemSzId,W.whsId,W.wfaId,W.onHand,PC.sumCost,PC.sumCostMP,PC.sumCostMO,PC.sumCostSV,PC.sumCostMA,PC.sumCif';
		$js= a_sql::queryL('SELECT '.$gb.',SUM(WT.inQty) inQty,SUM(WT.outQty) outQty
		FROM pep_oitw W
		JOIN ivt_owhs WH ON (WH.whsId=W.whsId)
		JOIN wma_owfa WFA ON (WFA.wfaId=W.wfaId)
		JOIN itm_oitm I ON (I.itemId=W.itemId)
		LEFT JOIN itm_oipc PC ON (PC.itemId=I.itemId AND PC.itemSzId=W.itemSzId AND PC.wfaId=W.wfaId)
		LEFT JOIN pep_wtr1 WT ON (WT.docDate >\''.$docDate.'\' AND WT.whsId=W.whsId AND WT.wfaId=W.wfaId AND WT.itemId=W.itemId AND WT.itemSzId=W.itemSzId)
		WHERE 1 AND WFA.wfaClass=\'N\' '.$wh.'
	GROUP BY '.$gb);
	}
	echo $js;
}
else if(_0s::$router=='GET rep/dcfValue'){ //valor movimiento dcf
	if($js=_js::ise($_GET['date1'],'Se debe definir la fecha inicial.')){}
	else if($js=_js::ise($_GET['date2'],'Se debe definir la fecha de corte.')){}
	else {
		$_GET['A.docDate(E_mayIgual']=$_GET['date1'];
		$_GET['A.docDate(E_menIgual']=$_GET['date2'];
		unset($_GET['date1'],$_GET['date2']);
		_ADMS::_lb('sql/filter');
		$wh=a_sql_filtByT($_GET);
		$gb='I.itemCode,I.itemName, B.itemSzId,B.wfaId, IC.sumCost, IC.sumCostMP, IC.sumCostMO,IC.sumCostMA,IC.sumCostSV,IC.sumCif';
		$js= a_sql::queryL('SELECT '.$gb.', SUM(B.quantity) quantity
		FROM wma3_odcf A
		JOIN wma3_dcf1 B ON (B.docEntry=A.docEntry)
		LEFT JOIN ivt_owhs WH ON (WH.whsId=B.whsId)
		LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
		LEFT JOIN itm_oipc IC ON (IC.itemId=B.itemId AND IC.itemSzId=B.itemSzId AND IC.wfaId=B.wfaId)
		WHERE A.canceled=\'N\' '.$wh.'
		GROUP BY '.$gb.'');
	}
	echo $js;
}
?>