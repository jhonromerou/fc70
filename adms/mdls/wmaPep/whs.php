<?php
if(_0s::$router =='GET whs'){
	_ADMS::_lb('sql/filter');
	$gb='I.itemName,W.itemSzId';
	switch($_GET['gBy']){
		case 'whsId': $gb .=',W.whsId'; break;
		case 'whsIdItem': $gb .=',W.whsId,I.itemCode'; break;
		case 'whsIdWfa': $gb .=',W.whsId,W.wfaId'; break;
		case 'wfaId': $gb .=',W.wfaId'; break;
		case 'wfaIdItem': $gb .=',W.wfaId,I.itemCode'; break;
		case 'itemId': $gb .=',I.itemCode'; break;
		default: $gb .=',W.whsId,W.wfaId,I.itemCode'; break;
	} unset($_GET['gBy']);
	$whAd='';
	$viewType2=($_GET['viewType2'])?$_GET['viewType2']:'all';
	switch($viewType2){
		case 'P': $whAd .= ' AND W.onHand>0'; break;
		case 'N': $whAd .= ' AND W.onHand<0'; break;
		case '0': $whAd .= ' AND W.onHand=0'; break;
	}
	if($_GET['reportLen']=='full'){ a_sql::$limitFull='Y'; }
	$M=array('_view'=>$_GET['viewType'],'L'=>array());
	unset($_GET['viewType'],$_GET['viewType2'],$_GET['reportLen'],$_GET['grTypeId']);
	$wh=a_sql_filtByT($_GET);
	$wh .= ' ORDER BY I.itemCode,WFA.wfaId,G1.itemSize';
	if($M['_view']=='A'){
		 $q=a_sql::query('SELECT '.$gb.',onHand 
		FROM itm_oitm I
		JOIN itm_grs2 G2 ON (G2.grsId=I.grsId)
		JOIN itm_grs1 G1 ON (G1.itemSzId=G2.itemSzId)
		JOIN pep_oitw W  ON (W.itemId=I.itemId AND W.itemSzId=G2.itemSzId) 
		JOIN ivt_owhs oW ON (oW.whsId=W.whsId) 
		LEFT JOIN wma_owfa WFA ON (WFA.wfaId=W.wfaId) 
		WHERE 1 '.$whAd.' '.$wh.' ',array(1=>'Error obteniendo inventario de producto en proceso',2=>'No se encontraron resultados en proceso.')); 
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			while($L=$q->fetch_assoc()){ $M['L'][]=$L; }
			$js=_js::enc2($M); unset($M);
		}
	}
	else if($M['_view']=='F'){
		$gb='I.itemCode,I.itemName,WFA.wfaName,G2.itemSzId';
		 $q=a_sql::query('SELECT '.$gb.',SUM(W.onHand) onHand 
		FROM itm_oitm I
		JOIN itm_grs2 G2 ON (G2.grsId=I.grsId)
		JOIN itm_grs1 G1 ON (G1.itemSzId=G2.itemSzId)
		JOIN pep_oitw W  ON (W.itemId=I.itemId AND W.itemSzId=G2.itemSzId) 
		JOIN ivt_owhs oW ON (oW.whsId=W.whsId) 
		LEFT JOIN wma_owfa WFA ON (WFA.wfaId=W.wfaId) 
		WHERE 1 '.$whAd.' '.$wh.' GROUP BY '.$gb,array(1=>'Error obteniendo inventario de producto en proceso',2=>'No se encontraron resultados en proceso.')); 
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			while($L=$q->fetch_assoc()){ $M['L'][]=$L; }
			$js=_js::enc2($M); unset($M);
		}
	}
	else if($M['_view']=='WH'){
		$gb='I.itemCode,I.itemName,oW.whsCode,oW.whsName,G2.itemSzId';
		 $q=a_sql::query('SELECT '.$gb.',SUM(W.onHand) onHand 
		FROM itm_oitm I
		JOIN itm_grs2 G2 ON (G2.grsId=I.grsId)
		JOIN pep_oitw W  ON (W.itemId=I.itemId AND W.itemSzId=G2.itemSzId) 
		JOIN ivt_owhs oW ON (oW.whsId=W.whsId) 
		LEFT JOIN wma_owfa WFA ON (WFA.wfaId=W.wfaId) 
		WHERE 1 '.$whAd.' '.$wh.' GROUP BY '.$gb,array(1=>'Error obteniendo inventario de producto en proceso',2=>'No se encontraron resultados en proceso.')); 
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			while($L=$q->fetch_assoc()){ $M['L'][]=$L; }
			$js=_js::enc2($M); unset($M);
		}
	}
	echo $js;
}
else if(_0s::$router=='GET whs/history'){ a_ses::hashKey('pepWhs');
	$dateIni=$___D['W1_docDate(E_mayIgual)'];
	if(0){ $js=_js::e(3,'Se debe definir la fecha inicial para consultar el histórico.');}
	else{
			_ADMS::_lb('sql/filter');
		$whLimit=($___D['reportLen']=='full')?'':a_sql::nextLimit();
		unset($___D['reportLen']);
		$wh=a_sql_filtByT($___D).$whAd;
		$q=a_sql::query('SELECT W1.tt,W1.tr,I.itemCode,I.itemName,W1.itemId,W1.itemSzId,oW.whsId,W1.wfaId,W1.inQty,W1.outQty,W1.onHandAt ,W1.docDate
	FROM pep_wtr1 W1
	JOIN itm_oitm I ON (I.itemId=W1.itemId)
	LEFT JOIN ivt_owhs oW ON (oW.whsId=W1.whsId)
	WHERE 1 '.$wh.' ORDER BY W1.id DESC '.$whLimit,array(1=>'Error obteniendo movimientos del producto.',2=>'No se encontraron movimientos.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $Mx=array('L'=>array());
			while($L=$q->fetch_assoc()){
				if($L['inQty']!=0){ $L['quantity']=$L['inQty']; }
				if($L['outQty']!=0){ $L['quantity']=$L['outQty']; }
				$Mx['L'][]=$L;
			}
			$js=_js::enc($Mx); unset($Mx);
		}
	}
	echo $js;
}
?>