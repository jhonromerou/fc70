<?php
if(_0s::$router=='GET ing'){ a_ses::hashKey('pepIng');
	$___D['fromA']='A.docEntry,A.docDate,A.docType,A.lineMemo,A.dateC,A.userId FROM pep_oing A ';
	echo Doc::get($___D);
}
else if(_0s::$router =='POST ing'){ a_ses::hashKey('pepIng');
	_ADMS::_lb('com/_2d');
	_ADMS::_app('wma3.PeP');
	$docEntry=$_J['docEntry'];
	if($js=_js::ise($_J['docDate'],'La fecha del documento debe estar definida.','Y-m-d')){}
	else if($js=_js::ise($_J['docType'],'Se debe definir el tipo de documento.')){}
	else if(!_js::textLimit($_J['lineMemo'],200)){ $js= _js::e(3,'Los detalles no pueden exceder los 100 caracteres.'); }
	else if(!is_array($_J['L'])){ $js=_js::e(3,'No se han enviado lineas para el documento.'); }
	else{ 
		unset($_J['textSearch']);
		$Ld=$_J['L']; unset($_J['L'],$_J['serieId']);
		$errs=0; $n=1;$rows=0;
		$Di=array(); $Liv=array();
		foreach($Ld as $nk=>$L){
			$totaln=0; $ln ='Linea '.$nk.': ';
			if($L['quantity']==''){ $Ld[$nk]['delete']='Y'; continue; }
			if($js=_js::ise($L['itemId'],$ln.'No se ha encontrado el ID del artículo.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['itemSzId'],$ln.'No se ha definido el ID de la talla.')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'La cantidad debe ser mayor a 0.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['whsId'],$ln.'Se debe definir la bodega de ingreso.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['wfaId'],$ln.'Se debe definir la fase de ingreso.','numeric>0')){ $errs++; break; }
			else{
					$Di[$n]=$L;
					$Di[$n]['lineNum']=$n;
					$Liv[]=array('whsId'=>$L['whsId'],'wfaId'=>$L['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity'],'inQty'=>'Y');
				
			}
			$n++;
		}
		$rows=count($Di);
		if($errs==0 && ($rows==0)){ $js=_js::e(3,'No se ha definido ninguna cantidad para dar ingreso. ('.$rows.')'); }
		else if($errs==0){
		a_sql::transaction(); $cmt=false;
		$ins=a_sql::insert($_J,array('kui'=>'ud','tbk'=>'pep_oing','qDo'=>'insert'));
		if($ins['err']){ $js=_js::e(1,'Error guardando documento pep.egr: '.$ins['text']); }
		else{
			$docEntry=$ins['insertId'];
			$jsAdd='"docEntry":"'.$docEntry.'"';
			foreach($Di as $nk=>$L2){
				$n=$L2['lineNum']; $L2['docEntry']=$docEntry;
				unset($L2['handInv'], $L2['priceList']);
				$ins2=a_sql::insert($L2,array('table'=>'pep_ing1','qDo'=>'insert'));
				if($ins2['err']){
					$js=_js::e(1,'Linea '.$n.': '.$ins2['text'],$jsAdd);
					$errs++; break;
				}
			}
			if($errs==0){
				PeP::onHand_put($Liv,array('docDate'=>$_J['docDate'],'tt'=>'pepIng','tr'=>$docEntry));
				if(PeP::$err){ $js=PeP::$errText; $errs++;; }
				else if($errs==0){ $cmt=true;
					$js=_js::r('Información guardada correctamente.',$jsAdd);
				}
			}
			a_sql::transaction($cmt);
		}
		}
	}
	echo $js;
}
else if(_0s::$router =='GET ing/view'){ a_ses::hashKey('pepIng');
	echo Doc::getOne(array('docEntry'=>$___D['docEntry'],'fromA'=>'A.docEntry,A.docDate,A.docType,A.lineMemo,A.dateC,A.userId FROM pep_oing A ','fromB'=>'B.whsId,B.wfaId,I.itemCode,I.itemName,B.itemSzId,B.quantity FROM pep_ing1 B JOIN itm_oitm I ON (I.itemId=B.itemId)','owh'=>'N'));
}

?>