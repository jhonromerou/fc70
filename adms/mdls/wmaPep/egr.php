<?php
if(_0s::$router=='GET egr'){
	$___D['fromA']='A.docEntry,A.docDate,A.docType,A.lineMemo,A.dateC,A.userId FROM pep_oegr A ';
	echo Doc::get($___D);
}
else if(_0s::$router =='POST egr'){
	_ADMS::_lb('com/_2d');
	_ADMS::_app('wma3.PeP');
	a_sql::transaction(); $c=false;
	$js=PeP::egrDoc_post($_J);
	if(_err::$err){ $js=_err::$errText; }
	else{ $c=true; }
	a_sql::transaction($c); 
	echo $js;
}
else if(_0s::$router =='GET egr/view'){
	echo Doc::getOne(array('docEntry'=>$___D['docEntry'],'fromA'=>'A.docEntry,A.docDate,A.docType,A.lineMemo,A.dateC,A.userId FROM pep_oegr A ','fromB'=>'I.itemCode,I.itemName,B.whsId,B.wfaId,B.itemSzId,B.quantity FROM '._0s::$Tb['pep_egr1'].' B JOIN itm_oitm I ON (I.itemId=B.itemId)','owh'=>'N'));
}

else if(_0s::$router =='POST egr/fromCat'){
	_ADMS::_lb('com/_2d');
	if($js=_js::ise($_J['docEntry'],'Se debe definir el número del documento.','numeric>0')){}
	else if($js=_js::ise($_J['docDate'],'La fecha del documento debe estar definida.','Y-m-d')){  }
	else if($js=_js::ise($_J['whsId'],'Se debe definir la bodega','numeric>0')){  }
	else if($js=_js::ise($_J['docType'],'Se debe definir el tipo de documento.')){}
	else{
		a_sql::transaction(); $c=false;
		$qD=a_sql::fetch('SELECT docEntry FROM pep_oegr WHERE tt=\'ivtCat\' AND tr=\''.$_J['docEntry'].'\' AND canceled=\'N\' LIMIT 1',[1=>'Error verificando documento']);
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else if(a_sql::$errNo==-1){ $js=_js::e(3,'No se puede realizar la acción, ya se generó un documento para este proceso: '.$qD['docEntry']); }
		else{
			$q=a_sql::queryL('SELECT F1.wfaId,\''.$_J['whsId'].'\' whsId,I.itemCode,B.itemId,B.itemSzId,SUM(B.quantity) quantity
			FROM ivt_cat1 B
			LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
			LEFT JOIN wma_mpg1 F1 ON (F1.itemId=B.itemId AND F1.wfaIdNext=9999)
			WHERE B.docEntry=\''.$_J['docEntry'].'\'
			GROUP BY F1.wfaId,B.itemId,I.itemCode,B.itemSzId',array('enc'=>'N',1=>'Error obteniendo documento de toma de inventario.',2=>'El documento no existe.'));
			if(a_sql::$err){ die(a_sql::$errNoText); }
			else{
				$_J['L']=$q['L'];
				_ADMS::_app('wma3.PeP');
				$_J['tt']='ivtCat'; $_J['tr']=$_J['docEntry'];
				$_J['lineMemo']='Origen (ivtCat #'.$_J['docEntry'].') '.$_J['lineMemo'];
				unset($_J['docEntry'],$_J['whsId']);
				$js=PeP::egrDoc_post($_J,array('kText'=>'itemCode','ln'=>'N'));
				if(_err::$err){
					$js= _err::$errText;
				}
				else{ $c=true; }
			}
		}
		a_sql::transaction($c); 
	}
	echo $js;
}
?>