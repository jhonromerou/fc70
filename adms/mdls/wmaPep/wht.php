<?php
if(_0s::$router=='GET wht'){ a_ses::hashKey('pepWht.basic');
	$___D['fromA']='A.docEntry,A.docDate,A.docType,A.whsIdFrom,A.wfaIdFrom,A.whsIdTo,A.wfaIdTo,A.lineMemo,A.dateC,A.userId FROM pep_owht A ';
	echo Doc::get($___D);
}
else if(_0s::$router =='POST wht'){ a_ses::hashKey('pepWht.basic');
	_ADMS::_lb('com/_2d'); _ADMS::_app('wma3.PeP');
	$docEntry=$_J['docEntry'];
	$sameWhs=($_J['whsIdFrom']==$_J['whsIdTo']);
	$samewFa=($_J['wfaIdFrom']==$_J['wfaIdTo']);
	if($js=_js::ise($_J['docDate'],'La fecha del documento debe estar definida.','Y-m-d')){}
	else if($js=_js::ise($_J['whsIdFrom'],'Se debe definir el almacen de salida.')){}
	else if($js=_js::ise($_J['wfaIdFrom'],'Se debe definir la fase de salida.')){}
	else if($js=_js::ise($_J['whsIdTo'],'Se debe definir el almacen de Ingreso.')){}
	else if($js=_js::ise($_J['wfaIdTo'],'Se debe definir la fase de ingreso.')){}
	else if($sameWhs && $samewFa){ $js=_js::e(3,'Transferencia sin lógica.'); }
	else if($js=_js::ise($_J['docType'],'Se debe definir el tipo de documento.')){}
	else if(!_js::textLimit($_J['lineMemo'],100)){ $js= _js::e(3,'Los detalles no pueden exceder los 100 caracteres.'); }
	else if(!is_array($_J['L'])){ $js=_js::e(3,'No se han enviado lineas para el documento.'); }
	else{
		$Ld=$_J['L']; unset($_J['L'],$_J['serieId']);
		$errs=0; $n=1;$rows=0;
		$Di=array(); $Liv=array();
		a_sql::transaction(); $cmt=false;
		foreach($Ld as $nk=>$L){
			$totaln=0; $ln ='Linea '.$n.': ';
			unset($L['handInv'], $L['priceList']);
			if($L['quantity']==''){ $Ld[$nk]['delete']='Y'; continue; }
			if($js=_js::ise($L['itemId'],$ln.'No se ha encontrado el ID del artículo.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['itemSzId'],$ln.'No se ha definido el ID de la talla.')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'La cantidad debe ser mayor a 0.','numeric>0')){ $errs++; break; }
			else{
				PeP::onHand($L,array('whsId'=>$_J['whsIdFrom'],'wfaId'=>$_J['wfaIdFrom'],'lnt'=>$ln));
				if(PeP::$err){ $js=Pep::$errText; $errs++; break; }
				else{
					$Di[$n]=$L;
					$Di[$n]['lineNum']=$n;
					$Liv[]=array('whsId'=>$_J['whsIdFrom'],'wfaId'=>$_J['wfaIdFrom'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity'],'outQty'=>'Y');
					$Liv[]=array('whsId'=>$_J['whsIdTo'],'wfaId'=>$_J['wfaIdTo'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity'],'inQty'=>'Y');
				}
			}
			$n++;
		}
		$rows=count($Di);
		if($errs==0 && ($rows==0)){ $js=_js::e(3,'No se ha definido ninguna cantidad a producir. ('.$rows.')'); }
		else if($errs==0){
		$ins=a_sql::insert($_J,array('kui'=>'ud','tbk'=>'pep_owht','qDo'=>'insert'));
		if($ins['err']){ $js=_js::e(1,'Error guardando documento pep.wht: '.$ins['text']); }
		else{
			$docEntry=$ins['insertId'];
			$jsAdd='"docEntry":"'.$docEntry.'"';
			foreach($Di as $nk=>$L2){
				$n=$L2['lineNum']; $L2['docEntry']=$docEntry;
				unset($L2['handInv'], $L2['priceList']);
				$ins2=a_sql::insert($L2,array('table'=>'pep_wht1','qDo'=>'insert'));
				if($ins2['err']){
					$js=_js::e(1,'Linea '.$n.': '.$ins2['text'],$jsAdd);
					$errs++; break;
				}
			}
			if($errs==0){
				PeP::onHand_put($Liv,array('docDate'=>$_J['docDate'],'tt'=>'pepWht','tr'=>$docEntry));
				if(PeP::$err){ $js=PeP::$errText; $errs++;; }
				else if($errs==0){ $cmt=true;
					$js=_js::r('Información guardada correctamente.',$jsAdd);
				}
			}
		}
		}
		a_sql::transaction($cmt);
	}
	echo $js;
}
else if(_0s::$router =='GET wht/view'){ a_ses::hashKey('pepWht.basic');
	echo Doc::getOne(array('docEntry'=>$___D['docEntry'],'fromA'=>'A.docEntry,A.docDate,A.docType,A.whsIdFrom,A.wfaIdFrom,A.whsIdTo,A.wfaIdTo,A.lineMemo,A.dateC,A.userId FROM pep_owht A ','fromB'=>'I.itemCode,I.itemName,B.itemSzId,B.quantity FROM pep_wht1 B JOIN itm_oitm I ON (I.itemId=B.itemId)','owh'=>'N'));
}

?>