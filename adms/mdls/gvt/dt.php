<?php
if(_0s::$router=='POST dt/dlvNews'){
	if(_js::isArray($_J['L'])){ die(_err::err('No se enviaron lineas en el documento',3)); }
	_err::errDie();
	a_sql::transaction();
	
	$lineNum = 1;
	foreach($_J['L'] as $ln => $L){
		$lnt = 'Linea '.$lineNum; $lineNum++;
		if(_js::iseErr($L['docEntry'], $lnt.'Numero documento debe definirse','numeric>0')){}
		else if(_js::iseErr($L['fiev'], $lnt.'Se debe definir un tipo de novedad')){}
		else if(_js::iseErr($L['lineMemo'], $lnt.'Debe definir detalles')){}
		_err::errDie();

		a_sql::qInsert(
			[
				'serieType'=> 'odlvFol',
				'docEntry' => $L['docEntry'],
				'fiek' => 'F',
				'fiev' => $L['fiev'],
				'lineMemo' => $L['lineMemo'],
			],
			[
				'tbk'=>'gvt_doc98',
				'qk'=>'ud',
			]
		);
		if(a_sql::$err){
			a_sql::transaction(false);
			die(_js::e(3, $lnt.'Error registrando: '.a_sql::$errText));
		}
	}
	a_sql::transaction(true);
	
	echo _js::r('Información registrada correctamente');
}
?>