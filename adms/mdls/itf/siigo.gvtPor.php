<?php
$D=$___D;
_ADMS::lib('sql/filter');
require('siigo/lb.php');
$R=Siigo::fromJson(
	"extracontable",
	"siigo.gvtPor"
);
$Th= $R['ths'];

$n=0;
$tds=count($Th)-1;
$Mx=array('tds'=>$tds,'fileName'=>'Plantilla Documento '.$docnam,'L'=>array());
/*
$Mx['L'][$n]=Siigo::$Tds;
$Mx['L'][$n]['1']=Siigo::$iSoc['cardName']; $n++;
$Mx['L'][$n]=Siigo::$Tds;
$Mx['L'][$n]['1']='MODELO PARA LA IMPORTACION DE DOCUMENTO EXTRACONTABLEE';
$n++;
$Mx['L'][$n]=Siigo::$Tds; $n++;
$Mx['L'][$n]=Siigo::$Tds; $n++;/* blank to line 5
*/
$Mx['L'][$n]=$Th; $n++;
//*/
$gb='A.docEntry,A.docDate,A.dueDate,owhs.whsCode,B.vatRate,B.vatSum,B.priceLine,
P.pymCode,I.itemName,BC.barCode,I.invPrice,grs1.itemSize, grs1.uniqSize ';
$grTypeId=1;
$wh = a_sql_filtByT([
	'serieId' => $D['serieId'] ,
	'docNum(E_in)' => $D['docNum']
]);
$q=a_sql::query('SELECT '.$gb.', B.quantity
FROM gvt_opor A
JOIN gfi_opym P ON (P.pymId = A.pymId)
JOIN gvt_por1 B ON (B.docEntry=A.docEntry)
JOIN ivt_owhs owhs ON (owhs.whsId = A.whsId)
JOIN itm_oitm I ON (I.itemId=B.itemId) 
JOIN itm_grs1 grs1 ON (grs1.itemSzId=B.itemSzId) 
LEFT JOIN itm_bar1 BC ON (grTypeId=\''.$grTypeId.'\' && BC.itemId=B.itemId AND BC.itemSzId=B.itemSzId AND BC.barCode!=\'\') 
WHERE 1 '.$wh . ' ORDER BY A.docNum ASC',
array(1=>'Error obteniendo información del documento: ',2=>'No se encontró el documento solicitado '.$D['docEntry'].'.'));
$total=0; $lastWh='';
$nl=1; $docSiigo = $D['docSiigo'] -1;
$totalDeb=0; $lastDoc= 0;
if(a_sql::$err){ die(a_sql::$errNoText); }
else{
	while($L=$q->fetch_assoc()){

		if ($lastDoc != $L["docEntry"]) {
			$nl = 1;
			$docSiigo++;
		}
		$lastDoc = $L["docEntry"];
	$Tc=Siigo::barcodeSep($L['barCode']);
	Siigo::$Base['lineNum']=$nl;
	Siigo::$Base['noC']=$docSiigo;
	$Mx['L'][$n]=Siigo::$Base;
	$L['invPrice']=round($L['invPrice'],2);
	$deb=$L['quantity']*$L['invPrice'];
	$Mx['L'][$n]['payMethod'] = $L['pymCode'];
	$Mx['L'][$n]['ivaRate'] = $L['vatRate']*1;
	$Mx['L'][$n]['ivaSum'] = $L['vatSum']*1;
	$Mx['L'][$n]['valorSec'] = $L['priceLine']*1;
	$totalDeb+=$deb;
	
	$total+=$Mx['L'][$n]['valorSec'];
	$Mx['L'][$n]['docYear']=substr($L['docDate'],0,4);
	$Mx['L'][$n]['docMonth']=substr($L['docDate'],5,2);
	$Mx['L'][$n]['docDay']=substr($L['docDate'],8,2);
	$Mx['L'][$n]['dueYear']=substr($L['dueDate'],0,4);
	$Mx['L'][$n]['dueMonth']=substr($L['dueDate'],5,2);
	$Mx['L'][$n]['dueDay']=substr($L['dueDate'],8,2);
	$Mx['L'][$n]['whsCode']=$L['whsCode'];
	if ($L["uniqSize"] == "Y"){
		$Mx['L'][$n]['itemName']=$L['itemName'];
	}
	else {
		$Mx['L'][$n]['itemName']=$L['itemName'].' T:  '.$L['itemSize'];
	}
	$Mx['L'][$n]['quantity']=$L['quantity']*1;
	if($Tc['l']){ $Mx['L'][$n]['_itemLine']=$Tc['l']; }
	if($Tc['g']){ $Mx['L'][$n]['_itemGr']=$Tc['g']; }
	if($Tc['code']){ $Mx['L'][$n]['itemIdSiigo']=$Tc['code']; }
	$lastWh = $Mx['L'][$n]['whsCode'];
	$nl++; $n++;
}
}
//*/
echo _js::enc2($Mx);
?>