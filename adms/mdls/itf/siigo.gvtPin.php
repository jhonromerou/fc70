<?php
$D=$___D;
_ADMS::lib('sql/filter');
require('siigo/lb.php');
$R=Siigo::fromJson(
	"contable",
	"siigo.gvtPin"
);
$Th= $R['ths'];

$n=0;
$tds=count($Th)-1;
$Mx=array('tds'=>$tds,'fileName'=>'Plantilla Documento '.$docnam,'L'=>array());
/*
$Mx['L'][$n]=Siigo::$Tds;
$Mx['L'][$n]['1']=Siigo::$iSoc['cardName']; $n++;
$Mx['L'][$n]=Siigo::$Tds;
$Mx['L'][$n]['1']='MODELO PARA LA IMPORTACION DE DOCUMENTO EXTRACONTABLEE';
$n++;
$Mx['L'][$n]=Siigo::$Tds; $n++;
$Mx['L'][$n]=Siigo::$Tds; $n++;/* blank to line 5
*/
$Mx['L'][$n]=$Th; $n++;
//*/
$gb='A.docEntry,A.docDate,A.ref1,A.dueDate,A.baseAmnt,A.docTotal,A.rteSum,A.rteIvaSum,A.vatSum,owhs.whsCode,B.priceLine,
P.pymCode,I.itemName,BC.barCode,I.invPrice,grs1.itemSize, grs1.uniqSize ';
$grTypeId=1;
$wh = a_sql_filtByT([
	//'serieId' => $D['serieId'] ,
	'docNum(E_in)' => $D['docNum']
]);
$q=a_sql::query('SELECT '.$gb.', B.quantity
FROM gvt_opor A
JOIN gfi_opym P ON (P.pymId = A.pymId)
JOIN gvt_por1 B ON (B.docEntry=A.docEntry)
JOIN ivt_owhs owhs ON (owhs.whsId = A.whsId)
JOIN itm_oitm I ON (I.itemId=B.itemId) 
JOIN itm_grs1 grs1 ON (grs1.itemSzId=B.itemSzId) 
LEFT JOIN itm_bar1 BC ON (grTypeId=\''.$grTypeId.'\' && BC.itemId=B.itemId AND BC.itemSzId=B.itemSzId AND BC.barCode!=\'\') 
WHERE 1 '.$wh . ' ORDER BY A.docNum ASC',
array(1=>'Error obteniendo información del documento: ',2=>'No se encontró el documento solicitado '.$D['docEntry'].'.'));
$total=0; $lastWh='';
$nl=1; $docSiigo = $D['docSiigo'] -1;
$totalDeb=0; $lastDoc= 0;
$DocEnds = []; // $ndcoc --- Lines, IVA, RTEIVA, RTEFTE y CXP
if(a_sql::$err){ die(a_sql::$errNoText); }
else{
	while($L=$q->fetch_assoc()){
		if ($lastDoc != $L["docEntry"]) {
			$nl = 1;
			$docSiigo++;
		}
        $L["baseAmnt"] *= 1;
        $L["vatSum"] *= 1;
        $L["docTotal"] *= 1;
		$lastDoc = $L["docEntry"];
        if(!isset($DocEnds[$lastDoc])) {
            $DocEnds[$lastDoc] = [
                'L' => [],
                "D" => $L,
                'IVA' => ["acc" => "2408100100", "debBal" => $L['vatSum']],
                'RTEIVA' => ["acc" => "2367010100", "creBal" => $L['rteIvaSum']],
                'RTEFTE' => ["acc" => "2365400100", "creBal" => $L['rteSum']],
                'CXP' =>  ["acc" => "2205010100", "creBal" => $L['docTotal']],
            ];

        }
	$Tc=Siigo::barcodeSep($L['barCode']);
	Siigo::$Base['secPedido']=$nl;
	Siigo::$Base['noC']=$docSiigo;
    $docRef1 = explode("-", $L["ref1"]);
    Siigo::$Base["cardPrefix"] =  $docRef1[0];
    Siigo::$Base["cardDocNum"] =  $docRef1[1];
	$DocEnds[$lastDoc]['L'][$n]=Siigo::$Base;
	//$DocEnds[$lastDoc]['L'][$n]['payMethod'] = $L['pymCode'];
	$DocEnds[$lastDoc]['L'][$n]['vatRate'] = $L['vatRate']*1;
	$DocEnds[$lastDoc]['L'][$n]['vatSum'] = $L['vatSum']*1;
	$DocEnds[$lastDoc]['L'][$n]['valorSec'] = $L['priceLine']*1;
	$totalDeb+=$deb;
	
	$total+=$DocEnds[$lastDoc]['L'][$n]['valorSec'];
	$DocEnds[$lastDoc]['L'][$n]['docYear']=substr($L['docDate'],0,4);
	$DocEnds[$lastDoc]['L'][$n]['docMonth']=substr($L['docDate'],5,2);
	$DocEnds[$lastDoc]['L'][$n]['docDay']=substr($L['docDate'],8,2);
	$DocEnds[$lastDoc]['L'][$n]['dueYear']=substr($L['dueDate'],0,4);
	$DocEnds[$lastDoc]['L'][$n]['dueMonth']=substr($L['dueDate'],5,2);
	$DocEnds[$lastDoc]['L'][$n]['dueDay']=substr($L['dueDate'],8,2);
	$DocEnds[$lastDoc]['L'][$n]['whsCode']=$L['whsCode'];
	if($Tc['l']){ $DocEnds[$lastDoc]['L'][$n]['_itemLine']=$Tc['l']; }
	if($Tc['g']){ $DocEnds[$lastDoc]['L'][$n]['_itemGr']=$Tc['g']; }
	if($Tc['code']){ $DocEnds[$lastDoc]['L'][$n]['itemIdSiigo']=$Tc['code']; }
	$lastWh = $DocEnds[$lastDoc]['L'][$n]['whsCode'];
	$nl++; $n++;
}
}
//*/
$MxAnt = $Mx["L"];

foreach ($DocEnds as $docEntry => $R) {
    $lineNum = 0;  $baseLine =[];
    foreach ($R["L"] as $L) { $Mx['L'][] = $L; $baseLine =$L; }
    Siigo::$Base['secPedido']= $baseLine["secPedido"] +1;
	Siigo::$Base['noC']=$baseLine['noC'];
    $tB =  Siigo::$Base;
    $tB["cardPrefix"] = $baseLine["cardPrefix"];
    $tB["cardDocNum"] = $baseLine["cardDocNum"];

    if ($R["IVA"]["debBal"]> 0){
        $tB["debCred"] = "D";
        $tB["debAcc"] = $R["IVA"]["acc"];
        $tB["valorSec"] = $R["IVA"]["debBal"];
        $Mx['L'][] = $tB;
    }

    if ($R["RTEIVA"]["creBal"]> 0){
        $tB["debCred"] = "C";
        $tB["debAcc"] = $R["RTEIVA"]["acc"];
        $tB["valorSec"] = $R["RTEIVA"]["creBal"];
        $tB["baseRte"] = $R["IVA"]["debBal"];
        $tB["baseCtaRteIva"] = $R["D"]["baseAmnt"];
        $Mx['L'][] = $tB;
        $tB["baseRte"] = ""; $tB["baseCtaRteIva"] = "";
    }

    if ($R["RTEFTE"]["creBal"]> 0){
        $tB["debCred"] = "C";
        $tB["debAcc"] = $R["RTEFTE"]["acc"];
        $tB["valorSec"] = $R["RTEFTE"]["creBal"];
        $tB["baseRte"] = $R["D"]["baseAmnt"];
        $Mx['L'][] = $tB;
        $tB["baseRte"] = "";
    }

    $tB["debCred"] = "C";
    $tB["debAcc"] = $R["CXP"]["acc"];
    $tB["valorSec"] = $R["CXP"]["creBal"];
    $tB["tipoDocCruce"] = $baseLine["tipoC"]."-0".$baseLine["codiC"];
    $tB["numDocCruce"] = $baseLine["cardDocNum"];
    $Mx['L'][] = $tB;
}
echo _js::enc2($Mx);
?>