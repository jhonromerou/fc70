<?php
class Siigo{
static $iSoc=array('cardName'=>'Nombre Empresa SAS',
'slpCode'=>1,'codeCity'=>283,'codeZone'=>0,
'ccosto'=>200,
'nit'=>1125082000,
);
static $Temp=[];
static public function fieReset($fies='',$D=array()){
	$F=explode(',',$fies);
	foreach($F as $n => $k){
		$D[$k]='';
	}
	return $D;
}
static public function fromJson($jsonFile,$kDefault="siigo.docInfo"){
	$string = file_get_contents(__DIR__."/json/".$jsonFile.".json");
	$jsonData = json_decode($string,true);
	$tdN= 1;
	$ths = ["ths"=>[]];
	$q=a_sql::fetch('SELECT jsv,trs FROM itf_odov WHERE k=\''.$kDefault.'\' LIMIT 1',array(1=>'Error obteniendo parametrización para ['.$k1.']: ',2=>'No se ha definido parametrización para ['.$k1.']'));
	$defineFields = json_decode($q['jsv'], 1);
	if(a_sql::$err){ die(a_sql::$errNoText); }
	self::$iSoc= $defineFields;

	foreach($jsonData as $k => $DL){
		self::$Tds[$tdN]="";
		$ths["ths"][$tdN] = $DL["t"];
		if(array_key_exists('d',$DL)){ self::$Base[$k]=$DL['d']; }
		else{ self::$Base[$k] =''; }
		if ($defineFields[$k]) {
			if ($defineFields[$k]["d"]) {
				self::$Base[$k] = $defineFields[$k]["d"];
			}
		}
		$tdN++;
	}
	return $ths;
}
static public function iniSoc($k1='',$k2=''){
	$q=a_sql::fetch('SELECT jsv FROM itf_odov WHERE k=\''.$k1.'\' LIMIT 1',array(1=>'Error obteniendo parametrización para ['.$k1.']: ',2=>'No se ha definido parametrización para ['.$k1.']'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	self::$iSoc=json_decode($q['jsv'],1);
	$q=a_sql::fetch('SELECT jsv,trs FROM itf_odov WHERE k=\''.$k2.'\' LIMIT 1',array(1=>'Error obteniendo parametrización para ['.$k2.']: ',2=>'No se ha definido parametrización para ['.$k2.']'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	$q['trs']=json_decode($q['trs'],1);
	return $q;
}
static $Base=array(); /* valor bases */
static $Tds=array();/* genera tds vacios para ancho fijo de lineas */
static public function getDocHead($tr=array(),$P=array()){
	$trR=array();
	$nM=array();
	if($P['noDraw'] && self::$iSoc[$P['noDraw']]){ $nM=self::$iSoc[$P['noDraw']]; }
	$tdN=1;
	foreach($tr as $k => $L){
		if(array_key_exists($k,$nM)){ continue; }
		if(array_key_exists($k,self::$iSoc)){ $L['d']=self::$iSoc[$k]; }
		if(array_key_exists('d',$L)){ self::$Base[$k]=$L['d']; }
		else{ self::$Base[$k] =''; }
		self::$Tds[$tdN]=''; $tdN++;
		$trR[$k]=($L['t']);
	}
	return $trR;
}
static public function barcodeSep($code=''){
	// * para 0 inicial en siigo
	$code = preg_replace("/^\*/","", $code);
	return array('l'=>substr($code,0,3),'g'=>substr($code,3,4),'code'=>substr($code,-6));
}

static public function acc2Itm($Mx=array(),$P=array(),$L=array()){
	if($P['accSell']){/* 4120 - venta */
		$Mx['lineNum']=$P['lineNum'];
		$Mx['debCred']=$P['debCred'];
		$Mx['vatPrc']=$L['vatPrc'];
		$Mx['vatSum']=$L['vatSum'];
		$Mx['whsCode']=$L['whsCode'];
		$Mx['accCode']=$L['accSell'];
		$Mx['quantity']=$L['quantity'];
		$Mx['valorSec']=$P['valorSec'];
		$Mx['secGravada']=$P['secGravada'];
	}
	if($P['accIvt']){/* 14 - inventario */
		$Mx['lineNum']=$P['lineNum'];
		$Mx['debCred']=$P['debCred'];
		$Mx['accCode']=$L['accIvt'];
		$Mx['valorSec']=$P['valorSec'];//costo
		$Mx['secGravada']=$P['secGravada'];
	}
	if($P['accCost']){/* 61 - costo */
		$Mx['lineNum']=$P['lineNum'];
		$Mx['debCred']=$P['debCred'];
		$Mx['accCode']=$L['accCost'];
		$Mx['valorSec']=$P['valorSec'];//costo
		$Mx['secGravada']=$P['secGravada'];
	}
	return $Mx;
}
}

?>