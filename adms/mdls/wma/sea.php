<?php
if(_0s::$router=='GET sea/odp'){
	_ADMS::_lb('sql/filter');
	$wh=a_sql_filtByT($___D);
	$gb='A.docEntry,A.docStatus,A.docDate,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm,B.quantity';
	$q=a_sql::query('SELECT '.$gb.' FROM '._0s::$Tb['wma3_oodp'].' A
LEFT JOIN '._0s::$Tb['wma3_odp1'].' B ON (B.docEntry=A.docEntry)
LEFT JOIN '._0s::$Tb['itm_oitm'].' I ON (I.itemId=B.itemId)
WHERE 1 '.$wh.' ORDER BY docEntry DESC LIMIT 20',array(1=>'Error obteniendo ordenes de producción.',2=>'No se encontraron resultados.'));
		$M=array('L'=>array());
		if(a_sql::$err){ die(a_sql::$errNoText); }
		else{ $k=0; $A=array(); $n=0;
			while($L=$q->fetch_assoc()){
				$M['L'][]=$L;
			}
		}
		$js=_js::enc($M,'just');
	echo $js;
}
else if(_0s::$router=='GET sea/odp20'){
	_ADMS::_lb('sql/filter');
	/*
	Se consulta la fase siguiente B20,
	pero se obtienen las cantidades desde la fase a realizar B20Q
	*/
	$fie=($___D['fie'])?','.$___D['fie']:'';;
	unset($___D['fie']);
	$wh =''; $wfaIdNext=$___D['wfaIdNext'];
	$lef_wfa='';
	if($___D['wfaIdNext']){
$wh .=' AND (
(B20.wfaIdNext=\''.$wfaIdNext.'\')
OR (B20.wfaIdBef=0 AND B20.wfaId=\''.$wfaIdNext.'\')
)
';
		unset($___D['wfaIdNext']);
	}
	$wh .=a_sql_filtByT($___D);
	$gb='B20.nfId,A.docEntry,A.docStatus,A.docDate,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm,B.quantity'.$fie;
	$q=a_sql::query('SELECT '.$gb.'
FROM wma3_oodp A
LEFT JOIN wma3_odp1 B ON (B.docEntry=A.docEntry)
LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
LEFT JOIN wma3_odp20 B20 ON (B20.docEntry=A.docEntry AND B20.itemId=B.itemId AND B20.itemSzId=B.itemSzId)
LEFT JOIN wma3_odp20 B20Q ON (B20Q.docEntry=A.docEntry AND B20Q.itemId=B.itemId AND B20Q.itemSzId=B.itemSzId AND B20Q.wfaId=\''.$wfaIdNext.'\')
LEFT JOIN wma_owfa WF ON (WF.wfaId=B20.wfaId)
WHERE 1 '.$wh.' ORDER BY docEntry DESC LIMIT 30',array(1=>'Error obteniendo ordenes de producción.',2=>'No se encontraron resultados.'));
		$M=array('L'=>array());
		if(a_sql::$err){ die(a_sql::$errNoText); }
		else{ $k=0; $A=array(); $n=0;
			_ADMS::_app('wma3');
			wma3::i_wfaFS();
			while($L=$q->fetch_assoc()){
				if($L['wfaId']==$wfaIdNext){
					$L['wfaId']=0; $L['wfaName']=wma3::$V['wfaFSName'];
				}
				$M['L'][]=$L;
			}
		}
		$js=_js::enc($M,'just');
	echo $js;
}
else if(_0s::$router=='GET sea/itmFase'){
	$whB='I.status=\'active\' AND I.prdItem=\'Y\'
	'; /*de tener alguna de las fases AND (MP1.wfaId=\''.$wfaIdNext.'\' OR MP1.wfaIdBef=\''.$wfaIdNext.'\' OR MP1.wfaIdNext=\''.$wfaIdNext.'\' )*/
	$whL=''; $wfaIdNext=$___D['wfaIdNext'];
	if($___D['wfaIdNext']){
		$whL .=' AND (
		(MP1.wfaIdNext=\''.$wfaIdNext.'\')
		OR (MP1.wfaIdBef=0 AND MP1.wfaId=\''.$wfaIdNext.'\'))';
		unset($___D['wfaIdNext']);
	}
	_ADMS::_lb('sql/filter');
	$fie=($___D['fie'])?','.$___D['fie']:'';; unset($___D['fie']);
	$wh=a_sql_filtByT($___D);
	$gb='I.itemId,I.itemCode,I.itemName,I.grsId,WF.wfaId,WF.wfaName,MP1.whsId,MP1.whsIdBef'.$fie;
	$q=a_sql::query('SELECT '.$gb.' FROM itm_oitm I
JOIN wma_mpg1 MP1 ON (MP1.itemId=I.itemId '.$whL.')
LEFT JOIN wma_owfa WF ON (WF.wfaId=MP1.wfaId)
WHERE '.$whB.' '.$wh.' ORDER BY MP1.lineNum ASC LIMIT 20',array(1=>'Error obteniendo ordenes de producción.',2=>'No se encontraron resultados.'));
		$M=array('L'=>array());
		if(a_sql::$err){ die(a_sql::$errNoText); }
		else{ $k=0; $A=array(); $n=0;
			_ADMS::_app('wma3');
			wma3::i_wfaFS();
			while($L=$q->fetch_assoc()){
				if($L['wfaId']==$wfaIdNext){
					$L['wfaId']=0; $L['wfaName']=wma3::$V['wfaFSName'];
				}
				$M['L'][]=$L;
			}
		}
		$js=_js::enc($M,'just');
	echo $js;
}

?>
