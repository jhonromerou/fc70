<?php
$serieName=$series='wmaDdf';
$serieDoc=array('serieType'=>'wmaOdp','docEntry'=>$___D['docEntry']);
Doc::$owh=false;
if(_0s::$router=='GET ddf'){
	$___D['fromA']='A.*,C.cardName 
	FROM wma3_oddf A LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)';
	echo Doc::get($___D);
}
else if(_0s::$router=='GET ddf/tb99'){
	echo Doc::tb99($series,$___D);
}
else if(_0s::$router=='POST ddf'){
	_ADMS::_app('wma3');
	wma3::i_wfaFS();
	a_sql::transaction();
	$js=wma3::ddf_post($_J);
	if(_err::$err){ $js=_err::$errText; }
	else{ a_sql::transaction(true);
		Doc::log_post(array('serieType'=>$serieName,'docEntry'=>wma3::$Ds['docEntry'],'dateC'=>1));
	}
	echo $js;
}
else if(_0s::$router=='GET ddf/view'){
	echo Doc::getOne(array('docEntry'=>$___D['docEntry'],'fromA'=>'A.*,C.cardName FROM wma3_oddf A LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)',
	'fromB'=>'B.nfId,B.tt,B.tr,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm,B.wfaIdBef,B.quantity,B.openQty FROM wma3_ddf1 B 
	JOIN itm_oitm I ON (I.itemId=B.itemId)'));
}
else if(_0s::$router=='PUT ddf/statusCancel'){
	if($js=_js::ise($___D['docEntry'],'Se debe definir Id del documento a anular.')){ }
	else{
		/* Obtener lineas del documento a anular. wfaId esta en head,y wfaIdBef en lines */
		$q=a_sql::query('SELECT A.docStatus,A.wfaId,B.wfaIdBef,B.nfId,B.tt,B.tr,B.itemId,B.itemSzId,B.quantity 
		FROM wma3_oddf A 
		JOIN wma3_ddf1 B ON (B.docEntry=A.docEntry) 
		WHERE A.docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo información de documento',2=>'El documento '.$___D['docEntry'].' no existe o no tiene lineas.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $errs=0; $Liv=array();
			_ADMS::_app('wma3'); wma3::i_wfaFS();
			a_sql::transaction(); $n=1;
			while($L=$q->fetch_assoc()){
				/* Revisar que el documento no este cerrado o anulado total o por emisiones */
				if($n==1){
					if($L['docStatus']=='N'){ $js=_js::e(3,'El documento ya ha sido anulado.'); $errs++; break; }
					else if($L['docStatus']=='C'){ $js=_js::e(3,'El documento está cerrado y no puede anularse.'); $errs++; break; }
				}
				if($L['wfaIdBef']==0){ $L['wfaIdBef']= wma3::$V['wfaFS']; }
				$qty=$L['quantity']; $ln='Linea '.$n.': '; $n++;
				$fromDdf=($L['nfId']>0);
				/* Si hay relacion a una odp
				1. actualizar las cantidades con base a lo anulado.
				2. Si es tiene relaciona a pdp y es fase0, aumentar lo programado
				*/
				if($errs==0 && $L['tt']=='wmaOdp'){
					$L['docEntry']=$L['tr'];
					$L['qSet']='onProQty=onProQty-'.$qty.',openQty=quantity-compQty-onProQty-rejQty';
					wma3::odp_fasePut($L,array('revOdp'=>'Y','ln'=>$ln));
					if(_err::$err){ $js=_err::$errText; $errs++; break; }
					/* Si es facil inicial,debo actualizar pdp */
					else if(wma3::$D['pdp_docEntry'] && $L['wfaIdBef']==wma3::$V['wfaFS']){
						wma3::pdp_putLine(array('docEntry'=>wma3::$D['pdp_docEntry'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'qSet'=>'progQty=progQty+'.$L['quantity']));
						if(_err::$err){ $js=_err::$errText; $errs++; break; }
					}
				}
			}
			$docDate=date('Y-m-d');/* reversar movimientos de inventario */
			if($errs==0){ _ADMS::_app('wma3.PeP');
				$docDate=date('Y-m-d');
				PeP::onHand_rever(array('tt'=>$series,'tr'=>$___D['docEntry'],'docDate'=>$docDate));
				if(_err::$err){ $js=_err::$errText; $errs++; }
			}
			/* Anular el documento si todo es correcto */
			if($errs==0){
				$upd=a_sql::query('UPDATE wma3_oddf SET docStatus=\'N\',canceled=\'Y\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1 ',array(1=>'Error anulando documento de fase.'));
				if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; }
				else{ $js=_js::r('Documento Anulado correctamente.'); a_sql::transaction(true);
						Doc::log_post(array('serieType'=>$series,'docEntry'=>$___D['docEntry'],'docStatus'=>'N','lineMemo'=>$___D['lineMemo']));
				}
			}
		}
	}
	echo $js;
}
?>