<?php
$serieDoc=array('serieType'=>'wmaOdp','docEntry'=>$___D['docEntry']);
$serieType=$series ='wmaOdp';
unset($___D['serieType'],$___D['cardName']);
class _oodp{
	static public function getOne($D=array()){
	$q=a_sql::query('SELECT A.docEntry,A.tr,A.dateC,A.docDate,A.docStatus,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.sellUdm,B.quantity,A.lineMemo FROM wma3_oodp A JOIN wma3_odp1 B ON (B.docEntry=A.docEntry) JOIN itm_oitm I ON (I.itemId=B.itemId) WHERE B.docEntry=\''.$D['docEntry'].'\' ',array(1=>'Error obteniendo orden de producción',2=>'No se encontró la orden de producción '.$D['docEntry']));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else{ return $q; }
}
}

if(_0s::$router=='GET odp/fromPdp_nuevo'){
	if($js=_js::ise($___D['tr'],'Se debe definir el número tt.')){ die($js); }
	if($js=_js::ise($___D['itemId'],'Se debe definir el ID del artículo.')){ die($js); }
	$M = Doc::getOne(array('r'=>'query','docEntry'=>$___D['tr'],'fromA'=>'A.docEntry,A.dateC,A.userId,A.userName,A.docDate,A.docStatus,A.lineMemo FROM wma3_opdp A','fromB'=>'B.lineNum,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.sellUdm,B.quantity,B.openQty FROM wma3_pdp1 B JOIN itm_oitm I ON (I.itemId=B.itemId)','whB'=>'AND B.itemId=\''.$___D['itemId'].'\' '));
	if(a_sql::$err){ die($M); }
	else{
		$M['L']=array(); $It=array(); $n=0;
		while($L=$M['qL']->fetch_assoc()){
			$M['L'][]=$L;
		}
		unset($M['qL']);
		$js=_js::enc($M,'just'); unset($M);
	}
	echo $js;
}
else if(_0s::$router=='GET odp/fromPdp'){
	if($js=_js::ise($___D['tr'],'Se debe definir el número tt.')){ die($js); }
	if($js=_js::ise($___D['itemId'],'Se debe definir el ID del artículo.')){ die($js); }
	$M = Doc::getOne(array('r'=>'query','docEntry'=>$___D['tr'],'fromA'=>'A.docEntry,A.dateC,A.userId,A.docType,A.docDate,A.docStatus,A.lineMemo FROM wma3_opdp A','fromB'=>'B.lineNum,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.sellUdm,B.quantity,B.openQty FROM wma3_pdp1 B JOIN itm_oitm I ON (I.itemId=B.itemId)','whB'=>'AND B.itemId=\''.$___D['itemId'].'\' '));
	if(a_sql::$err){ die($M); }
	else{
		$M['L']=array(); $It=array(); $n=0;
		while($L=$M['qL']->fetch_assoc()){
			$kr=$L['itemId']; $ta=$L['itemSzId'];
			if(!array_key_exists($kr,$It)){ $It[$kr]=$n; $n++; }
			$k=$It[$kr];
			if(!array_key_exists($k,$M['L'])){
				$M['L'][$k]=array('itemId'=>$L['itemId'],'itemCode'=>$L['itemCode'],'itemName'=>$L['itemName'],'T'=>array());
			}
			if(!array_key_exists($ta,$M['L'][$k]['T'])){
				$M['L'][$k]['T'][$ta]= array('itemSzId'=>$ta,'quantity'=>0,'openQty'=>0);
			}
			$M['L'][$k]['T'][$ta]['quantity'] +=$L['quantity'];
			$M['L'][$k]['T'][$ta]['openQty'] +=$L['openQty'];
		}
		unset($M['qL']);
		$js=_js::enc($M,'just'); unset($M);
	}
	echo $js;
}
else if(_0s::$router=='POST odp/fromPdp'){
	_ADMS::_app('wma3'); _ADMS::_lb('com/_2d');
	$___D['tt']='wmaPdp';
	if($js=Doc::status(array('docEntry'=>$___D['tr'],'serieType'=>'wmaPdp'),array('D'=>'Y','fie'=>'docType'))){ die($js); }
	else if($js=_js::ise($___D['docDate'],'La fecha del documento debe estar definida.','Y-m-d')){}
	else if($js=_js::ise($___D['dueDate'],'La fecha de entrega debe estar definida.','Y-m-d')){}
	else if($js=_js::ise($___D['docType'],'Se debe definir el tipo de orden.')){}
	else if(!_js::textLimit($___D['lineMemo'],100)){ $js= _js::e(3,'Los detalles no pueden exceder los 100 caracteres.'); }
	else if(!is_array($___D['L'])){ $js=_js::e(3,'No se han enviado lineas para el documento.'); }
	else{ 
		unset($___D['textSearch']);
		$Ld=$___D['L']; unset($___D['L']); $errs=0; $n=1; $Di=array();
		unset($___D['itemId']); $rows=0;
		foreach($Ld as $nk=>$L){//ln->T[xx] ->
			$ln ='Linea '.$n.': ';
			if($L['quantity']==''){ unset($Ld[$nk]); continue; }
			if($js=_js::ise($L['itemId'],$ln.'No se ha encontrado el ID del artículo.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['itemSzId'],$ln.'No se ha definido el ID de la talla.')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'La cantidad debe ser mayor a 0.','numeric>0')){ $errs++; break; }
			else{
				$ql=wma3::pdp_getLine(array('docEntry'=>$___D['tr'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId']));
				if(wma3::$err){ $js = wma3::$errText; $errs++; break; }
				else if($ql['quantity']<$L['quantity']){ $js=_js::e(3,$ln.'La cantidad a producir ('.$L['quantity'].') es mayor a lo programado ('.($ql['quantity']*1).').'); $errs++; break; }
				else if($ql['openQty']<$L['quantity']){ $js=_js::e(3,$ln.'La cantidad a producir ('.$L['quantity'].') es mayor a lo pendiente ('.($ql['openQty']*1).').'); $errs++; break; }
				else{
					$Di[$nl]=$L;
					$Di[$nl]['lineNum']=$n;
					if($L1['delete']){ $Di[$nl]=$L1['delete']; }
					$nl++; 
				}
			}
			$n++;
		}
		$rows=count($Di);
		if($errs==0 && ($rows==0)){ $js=_js::e(3,'No se ha definido ninguna cantidad a producir.'); }
		else if($errs==0){
		a_sql::transaction(); $commit=false;
		$___D['docType']=Doc::$D['docType'];
		$ins=a_sql::insert($___D,array('kui'=>'uid_dateC','table'=>'wma3_oodp','qDo'=>'insert'));
		if($ins['err']){ $js=_js::e(1,'Error generando orden de producción: '.$ins['text']); $errs++; }
		else{
			$docEntry=($ins['insertId']);
			$Wh=array();
			foreach($Di as $nk=>$L2){
				$n=$L2['lineNum'];
				$L2['docEntry']=$docEntry;
				wma3::pdp_putLine(array('docEntry'=>$___D['tr'],'itemId'=>$L2['itemId'],'itemSzId'=>$L2['itemSzId'],'qSet'=>'openQty=openQty-'.$L2['quantity'].', progQty=progQty+'.$L2['quantity'].' '));
				if(_err::$err){ $js=_err::$errText; $errs++; break; }
				else{
					$ins2=a_sql::insert($L2,array('tbk'=>'wma3_odp1','qDo'=>'insert'));
					if($ins2['err']){
						$js=_js::e(1,'Linea '.$n.': '.$ins2['text']);
						$errs++; break;
					}
				}
			}
			if($errs==0){ $commit=true;
				$js=_js::r('Orden de Produción generada correctamente.','"docEntry":"'.$docEntry.'"');
				Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$docEntry,'dateC'=>'1','lineMemo'=>'Create from pdp #'.$___D['tr']));
			}
			a_sql::transaction($commit);
		}
		}
	}
	echo $js;
}
else if(_0s::$router=='POST odp/form'){
	_ADMS::_app('wma3'); _ADMS::_lb('com/_2d');
	if($js=_js::ise($___D['docDate'],'La fecha del documento debe estar definida.','Y-m-d')){}
	else if($js=_js::ise($___D['dueDate'],'La fecha de entrega debe estar definida.','Y-m-d')){}
	else if($js=_js::ise($___D['docType'],'Se debe definir el tipo de orden.')){}
	else if(!_js::textLimit($___D['ref1'],20)){ $js= _js::e(3,'La Referencia no puede exceder 20 caracteres'); }
	else if(!_js::textLimit($___D['lineMemo'],100)){ $js= _js::e(3,'Los detalles no pueden exceder los 100 caracteres.'); }
	else if(!is_array($___D['L'])){ $js=_js::e(3,'No se han enviado lineas para el documento.'); }
	else{ 
		unset($___D['textSearch']);
		$Ld=$___D['L']; unset($___D['L']); $errs=0; $n=1; $Di=array();
		foreach($Ld as $nk=>$L){//ln->T[xx] ->
			$totaln=0; $ln ='Linea '.$n.': ';$n++;
			if($L['quantity']==''){ continue; }
			if($js=_js::ise($L['itemId'],$ln.'No se ha encontrado el ID del artículo.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['itemSzId'],$ln.'No se ha definido el ID de la talla.')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'La cantidad debe ser mayor a 0.','numeric>0')){ $errs++; break; }
			else{
				$Di[$nl]=$L;
				$Di[$nl]['lineNum']=$n;
				$nl++; 
			}
		}
		$rows=count($Di);
		if($errs==0 && ($rows==0)){ $js=_js::e(3,'No se ha definido ninguna cantidad a producir.'); }
		else if($errs==0){
		a_sql::transaction(); $commit=false;
		$ins=a_sql::insert($___D,array('kui'=>'uid_dateC','table'=>'wma3_oodp','qDo'=>'insert'));
		if($ins['err']){ $js=_js::e(1,'Error generando orden de producción: '.$ins['text']); $errs++; }
		else{
			$docEntry=($ins['insertId']);
			$Wh=array();
			foreach($Di as $nk=>$L2){
				$n=$L2['lineNum'];
				$L2['docEntry']=$docEntry;
				$ins2=a_sql::insert($L2,array('table'=>'wma3_odp1','qDo'=>'insert'));
				if($ins2['err']){
					$js=_js::e(1,'Linea '.$n.': '.$ins2['text']);
					$errs++; break;
				}
			}
			if($errs==0){ $commit=true;
				$js=_js::r('Orden de Produción generada correctamente.','"docEntry":"'.$docEntry.'"');
				Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$docEntry,'dateC'=>'1'));
			}
			a_sql::transaction($commit);
		}
		}
	}
	echo $js;
}

else if(_0s::$router=='GET odp'){
	_ADMS::_lb('sql/filter');
	$wh=a_sql_filtByT($___D);
	$gb='A.docEntry,A.docStatus,A.docDate,A.dueDate,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm,B.quantity,A.cardId,C.cardName';
	$q=a_sql::query('SELECT '.$gb.' FROM wma3_oodp A 
LEFT JOIN wma3_odp1 B ON (B.docEntry=A.docEntry) 
LEFT JOIN itm_oitm I ON (I.itemId=B.itemId) 
LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
WHERE 1 '.$wh.' ORDER BY docEntry DESC',array(1=>'Error obteniendo ordenes de producción.',2=>'No se encontraron resultados.'));
		$M=array('L'=>array());
		if(a_sql::$err){ die(a_sql::$errNoText); }
		else{ $k=0; $A=array(); $n=0;
			while($L=$q->fetch_assoc()){
				$M['L'][]=$L;
			}
		}
		$js=_js::enc($M,'just');
	echo $js;
}
else if(_0s::$router=='GET odp/view'){
	echo Doc::getOne(array('docEntry'=>$___D['docEntry'],'fromA'=>'A.*,C.cardName FROM wma3_oodp A 
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId) ','fromB'=>'B.lineNum,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm,B.quantity FROM wma3_odp1 B JOIN itm_oitm I ON (I.itemId=B.itemId)'));
}
else if(_0s::$router=='GET odp/docFase'){
	$Mx= Doc::getOne(array('r'=>'Mx','docEntry'=>$___D['docEntry'],'fromA'=>'A.* FROM wma3_oodp A','fromB'=>'B.lineNum,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm,B.quantity 
	FROM wma3_odp1 B 
	JOIN itm_oitm I ON (I.itemId=B.itemId) '));
	if(_err::$err){ $js= _err::$errText; }
	else{
		$Mx['itemCode']=$Mx['L'][0]['itemCode'];
		$Mx['itemName']=$Mx['L'][0]['itemName'];
		$Mx['wfaId']=0;
		$Mx['docIso']=_0s::jSocGet('wmaOdpIsoDoc',array('r'=>1,'f'=>'json'));
		$Mx['docTemplate']=_0s::jSocGet('wmaOdpDocFase',array('r'=>1));
		$wfa0=($___D['wfaId'])?$___D['wfaId']:false;
		$Mx['WF']=array();
		$Mx['Bom']=array();
		/*obtener fases para opts, usando la 0 */
		$qw=a_sql::query('SELECT DA.wfaOrder,WF.wfaId,WF.wfaName 
		FROM wma3_odp20 DA 
		JOIN wma_owfa WF ON (WF.wfaId=DA.wfaId) 
		WHERE DA.docEntry=\''.$___D['docEntry'].'\' GROUP BY DA.wfaOrder,WF.wfaId,WF.wfaName ',array(1=>'Error obteniendo fases de la orden: ',2=>'La orden de producción no tiene fases definidas'));
		if(a_sql::$err){ $Mx['WF']=json_decode(a_sql::$errNoText,1); }
		else{
			while($L=$qw->fetch_assoc()){
				$Mx['WF'][]=array('k'=>$L['wfaId'],'v'=>$L['wfaName']);
				if($wfa0==false && $L['wfaOrder']==1){ $wfa0= $L['wfaId']; }
			}
			$Mx['wfaId']=$wfa0; /*fase definida */
			_ADMS::libC('wma','mrp'); _ADMS::lib('sql/filter');
			$Pw=array('whType'=>'IN(\'MP\',\'SE\')');
			foreach($Mx['L'] as $n =>$L){
				//I.itemId,B.wfaId,B.itemSzId,SUM(B.onHand) reqQty
				$Pw['wh']['T1.wfaId(E_igual)']=$wfa0;
				wmaMrp::get(array('itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'reqQty'=>$L['quantity']),$Pw);
			}
			$Mx['Bom']=wmaMrp::$MR;
		}
		$js=_js::enc2($Mx);
	}
	echo $js;
}

else if(_0s::$router=='GET odp/docHistory'){
	$origen=$___D['origen'];
	if($js=_js::ise($origen,'Se debe definir el tipo de documento de origen')){}
	else{
		if($origen=='wmaDdf'){
			$___D['A_wfaId']=$___D['B_wfaId']; unset($___D['B_wfaId']);
			$___D['A_whsId']=$___D['B_whsId']; unset($___D['B_whsId']);
		}
		$___D['B.tt(E_igual)']='wmaOdp';
		unset($___D['origen']);
		_ADMS::_lb('sql/filter');
		$wh=a_sql_filtByT($___D);
		switch($origen){
			case 'wmaDcf': $qt='SELECT \''.$origen.'\' ttDoc,A.docEntry,A.docStatus,A.docDate,I.itemCode,I.itemName,B.itemSzId,B.whsId,B.wfaId,B.lineType,B.rejReason,B.quantity,C.cardName 
			FROM wma3_odcf A 
			JOIN wma3_dcf1 B ON (B.docEntry=A.docEntry) 
			LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
			LEFT JOIN par_ocrd C ON (C.cardId=B.cardId)
			WHERE 1 '.$wh.' '.a_sql::nextLimit();
			break;
			case 'wmaDdf': 
			
			$qt='SELECT \''.$origen.'\' ttDoc,A.docEntry,A.docStatus,A.docDate,I.itemCode,I.itemName,B.itemSzId,A.whsId,A.wfaId,B.quantity,C.cardName 
			FROM wma3_oddf A 
			JOIN wma3_ddf1 B ON (B.docEntry=A.docEntry) 
			LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
			LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
			WHERE 1 '.$wh.' '.a_sql::nextLimit();
			break;
			default : die(_js::e(3,'Tipo de documento no definido en consultas.')); break;
		}
		$js=a_sql::queryL($qt,array(1=>'Error obteniendo historial.',2=>'No se encontraron resultados.'));
	}
	echo $js;
}


else if(_0s::$router=='GET odp/liberar'){
	if($js=_js::ise($___D['docEntry'],'Se debe definir el número del documento','numeric>0')){ die($js); }
	$M=a_sql::fetch('SELECT A.* FROM wma3_oodp A WHERE A.docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo información de la orden: ',2=>'La orden de producción #'.$___D['docEntry'].', no existe.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	$ql=a_sql::query('SELECT B.lineNum,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm,B.quantity 
	FROM wma3_odp1 B 
	JOIN itm_oitm I ON (I.itemId=B.itemId) 
	WHERE B.docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo información de las lineas orden: ',2=>'La orden de producción #'.$___D['docEntry'].', no tiene lineas registradas.'));
	if(a_sql::$err){ $M['L']=json_decode(a_sql::$errNoText); }
	else{
		_ADMS::_lb('sql/filter');
		$M['L']=array(); $itemIdB=false; $IL=array();
		while($L=$ql->fetch_assoc()){
			$M['L'][]=$L; $itemIdB=$L['itemId'];
			$IL[]=$L;
			$L['reqQty']=$L['quantity'];
		}
		_ADMS::_app('wma3');
		$M['Lf']=wma3::mpg_get(array('itemId'=>$itemIdB));
		//$M['Lm']=wmaMrp::$MR;
		if(_err::$err){ $M['Lf']=json_decode(_err::$errText,2); }
	}
	echo _js::enc2($M);
}
else if(_0s::$router=='POST odp/liberar'){
	if($js=_js::ise($___D['docEntry'],'Se debe definir el número del documento','numeric>0')){ die($js); }
	_ADMS::lib('iDoc');
	iDoc::vStatus(array('tbk'=>'wma3_oodp','docEntry'=>$D['docEntry'],'serieType'=>'wmaOdp','log'=>'wma3_doc99','lineMemo'=>'Orden Liberada'));
	if(_err::$err){ die(_err::$errText); }
	else if(Doc::$D['docStatus']=='O'){ die(_js::e(3,'La orden ya se encuentra liberada.')); }
	$q=a_sql::query('SELECT A.tr,B.itemId,B.itemSzId,B.quantity FROM wma3_oodp A JOIN wma3_odp1 B ON (B.docEntry=A.docEntry) WHERE B.docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo orden de producción',2=>'No se encontró la orden de producción '.$___D['docEntry']));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	a_sql::transaction(); $comit=false; $errs=0;
	$total=0; $Liv=array();
	_ADMS::_app('wma3'); _ADMS::_lb('wma/PeP');
 $totalQty=0; $IL=array(); $itemIdB=false;
	wma3::i_wfaFS(); /*obtener fase inicial sistema y cargar inventario fase 0*/
	while($L=$q->fetch_assoc()){
		$totalQty+=$L['quantity'];
		if(_err::$err){ $js=_err::$errText; $errs++; break; }
		else{
			$k=$L['itemId'].$L['itemSzId'];
			$IL[$k]=array('itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']);
			$Liv[]=array('inQty'=>'Y','whsId'=>$___D['whsId'],'wfaId'=>wma3::$V['wfaFS'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']);
			$itemIdB=$L['itemId'];
		}
	}
	/* definir fases y operaciones desde datos en odp20 */
	if($errs==0){
		$Di=array(); $n_f=1; $n_o=1;
		$Dl=wma3::mpg_faseOrder($___D['L']); $wfa1=0;
		foreach($Dl as $nk =>$L){
			if($L['lineType']=='F'){ $L['wfaOrder']=$n_f; 
				if($n_f==1){ $wfa1=$L['wfaId']; }
				$n_f++;
			}
			else if($L['lineType']=='O'){ $L['wopOrder']=$n_o; $n_o++; }
			$L['docEntry']=$___D['docEntry'];
			foreach($IL as $iks => $Ld){
				$L['timeBase']=$L['teoricTime']; unset($L['teoricTime']);
				$L['itemId']=$Ld['itemId'];
				$L['itemSzId']=$Ld['itemSzId'];
				$L['quantity']=$Ld['quantity'];
				$L['openQty']=$Ld['quantity'];
				$L['cost']=$L['costMP']+$L['costMO']+$L['costMA']+$L['cif'];
				$L['lineTotal']=$L['cost']*$L['quantity'];
				$ins2=a_sql::insert($L,array('table'=>'wma3_odp20','qDo'=>'insert'));
				if($ins2['err']){ $js=_js::e(3,'Error liberando fases y operaciones: '.$ins2['text']); $errs++; break 2; }
			}
		}
	}
	if(0 && $errs==0 && count($Liv)>0){/* Ingresar a inventario sin fase */
		PeP::onHand_put($Liv,array('docDate'=>$___D['docDate'],'tt'=>$series,'tr'=>$___D['docEntry']));
		if(PeP::$err){ $js=PeP::$errText; $errs++;; }
	}
	if($errs==0){/* actualizar estado documento */
		$qu=a_sql::query('UPDATE wma3_oodp SET docStatus=\'O\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error actualizando estado de orden de producción: '));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $js=_js::r('Orden Liberada correctamente.','"docEntry":"'.$___D['docEntry'].'"'); a_sql::transaction(true);
			_ADMS::lib('JLog');
			JLog::post(array('tbk'=>'wma3_doc99','serieType'=>'wmaOdp','docEntry'=>$___D['docEntry'],'docStatus'=>'O'));
		}
	}
	echo $js;
}

else if(_0s::$router=='GET odp/tbFase'){
	if($js=_js::ise($___D['docEntry'],'Se debe definir el número del documento','numeric>0')){ die($js); }
	$M=a_sql::fetch('SELECT A.* 
	FROM wma3_oodp A WHERE A.docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo información de la orden: ',2=>'La orden de producción #'.$___D['docEntry'].', no existe.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else if($M['docStatus']=='N'){ die(_js::e(3,'La orden de producción está anulada.')); }
	else if($M['docStatus']=='P'){ die(_js::e(3,'La orden de producción no ha sido liberada.')); }
	$ql=a_sql::query('SELECT lineStatus,wfaOrder,wfaId,itemId,itemSzId,quantity,openQty,onProQty,compQty,rejQty,qtyExcess
	FROM wma3_odp20 
	WHERE docEntry=\''.$___D['docEntry'].'\' AND lineType=\'F\' ORDER BY wfaOrder ASC',array(1=>'Error obteniendo lineas de fases de la orden: ',2=>'La orden de producción #'.$___D['docEntry'].', no tiene lineas registradas.'));
	if(a_sql::$err){ $M['L']=json_decode(a_sql::$errNoText); }
	else{
		$M['L']=array();
		while($L=$ql->fetch_assoc()){
			$M['L'][]=$L;
		}
	}
	echo _js::enc2($M);
}

else if(_0s::$router=='GET odp/itemStatusFase'){
	$itemCode=$___D['itemCode'];
	$wfaId=$___D['wfaId'];
	unset($___D['itemCode'],$___D['wfaId']);
	if($js=_js::ise($itemCode,'Se debe definir el código del artículo')){ die($js); }
	else if($js=_js::ise($wfaId,'Se debe definir la fase','numeric>0')){ die($js); }
	else{
		_ADMS::_lb('sql/filter');
	$wh=a_sql_filtByT($___D);
		$ql=a_sql::query('SELECT A.docEntry,D.itemId,D.itemSzId,D.quantity,D.compQty,D.onProQty,D.rejQty,D.openQty,D.qtyExcess
		FROM wma3_oodp A 
		JOIN wma3_odp20 D ON (D.docEntry=A.docEntry)
		JOIN itm_oitm I ON (I.itemId=D.itemId)
		WHERE I.itemCode=\''.$itemCode.'\' AND lineType=\'F\' AND D.wfaId=\''.$wfaId.'\' '.$wh.' ',array(1=>'Error obteniendo lineas de fases de ordenes de producción: ',2=>'No existen ordenes de producción registradas.'));
		if(a_sql::$err){ $M['L']=json_decode(a_sql::$errNoText); }
		else{
			$M=array('L'=>array());
			while($L=$ql->fetch_assoc()){
				$M['L'][]=$L;
			}
		}
		echo _js::enc2($M);
	}
}

else if(_0s::$router=='GET odp/form2'){
	$gb='B.id,A.docEntry,A.docStatus,A.docDate,A.dueDate,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm,B.quantity,B.lineStatus';
	$q=a_sql::query('SELECT '.$gb.' FROM wma3_oodp A 
LEFT JOIN wma3_odp1 B ON (B.docEntry=A.docEntry) 
LEFT JOIN itm_oitm I ON (I.itemId=B.itemId) 
WHERE A.docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo orden de producción.',2=>'No se encontró la orden de producción para editar.'));
		$M=array('L'=>array());
		if(a_sql::$err){ die(a_sql::$errNoText); }
		else{ $k=0; $A=array(); $n=0;
			while($L=$q->fetch_assoc()){
				$M['L'][]=$L;
			}
		}
		$js=_js::enc($M,'just');
	echo $js;
}
else if(_0s::$router=='PUT odp/form2'){
	if($js=Doc::status($serieDoc,array('D'=>'Y','fie'=>'tt,tr'))){ die($js); }
	else{ $nl=1; $lins=0;
		/*revisar pdp si... */
		if(Doc::$D['tt']=='wmaPdp'){
			$qd=a_sql::fetch('SELECT docStatus,canceled FROM wma3_opdp WHERE docEntry=\''.Doc::$D['tr'].'\' LIMIT 1',array(1=>'Error consultando documento de planificación relacionado.',2=>'El documento de planificación #'.Doc::$D['tr'].',no existe.'));
			if(a_sql::$err){ die(a_sql::$errNoText); }
			if($qd['docStatus']=='C'){ die(_js::e(3,'El documento de planificación relacionado a la orden,está cerrado.')); }
			if($qd['docStatus']=='N'){ die(_js::e(3,'El documento de planificación relacionado a la orden,está anulado.')); }
		}
		/*revisar lineas */
		foreach($___D['L'] as $n =>$L){
			$ln='Linea '.$nl.': '; $nl++; $diffs=0;
			if($js=_js::ise($L['id'],$ln.'Id de linea no definida','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'La cantidad debe ser mayor a 0','numeric>0')){ $errs++; break; }
			else{/* no se puede modificar si fase 0 ... */
				$ql=a_sql::fetch('SELECT B1.lineStatus,B1.itemId,B1.itemSzId,B1.quantity,B20.nfId id,(B20.onProQty+B20.compQty+B20.rejQty) qtyOk 
				FROM wma3_odp1 B1 
				LEFT JOIN wma3_odp20 B20 ON (B20.docEntry=B1.docEntry AND B20.lineType=\'F\' AND B20.wfaOrder=1 AND B20.itemId=B1.itemId AND B20.itemSzId=B1.itemSzId)
				WHERE B1.id=\''.$L['id'].'\' LIMIT 1',array(1=>$ln.'Error obteniendo linea del documento.',2=>$ln.'La linea no existe.'));
				if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
				if($L['quantity']==$ql['quantity'] && $L['lineStatus']==$ql['lineStatus']){ 
					unset($___D['L'][$n]);  continue; 
				}
				$___D['L'][$n]['itemId']=$ql['itemId'];
				$___D['L'][$n]['itemSzId']=$ql['itemSzId'];
				$___D['L'][$n]['diff']=$L['quantity']-$ql['quantity'];
				if($L['quantity']<$ql['qtyOk']){ $js=_js::e(3,$ln.'La cantidad definida ('.$L['quantity'].') es menor a la sumatoria de lo completo,rechazado y en proceso ('.($ql['qtyOk']*1).') de la fase inicial.'); $errs++; break; }
				else if($L['lineStatus']=='C' && $ql['qtyOk']>0){ $js=_js::e(3,$ln.'No se puede cerrar una linea que su sumatoria de lo completo,rechazado y en proceso ('.($ql['qtyOk']*1).') sea mayor 0; en la fase inicial.'); $errs++; break; }
				else if($ql['id']>0){ $___D['L'][$n]['odp20']=$ql['id']; }
				$lins++;
			}
		}
		if($errs==0 && $lins==0){ $js=_js::r('Sin modificaciones'); $errs++; }
		/* actualizar lineas odp y odp20 */
		if($errs==0){
			a_sql::transaction();
			foreach($___D['L'] as $n=>$L){
				$diffs+=$L['diff']*1;
				/* actualizar todas las fases */
				if($L['odp20']){
					$Di=array('lineStatus'=>$L['lineStatus'],'quantity'=>'+-+quantity+'.$L['diff'],'openQty'=>'+-+openQty+'.$L['diff']);
					$ins=a_sql::insert($Di,array('table'=>'wma3_odp20','qDo'=>'update','wh_change'=>'WHERE docEntry=\''.$___D['docEntry'].'\' AND itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' '));
					if($ins['err']){ $js=_js::e(3,'Error actualizando tabla de fases: '.$ins['text']); $errs++; break; }
				}
				/* actualizar pdp si, *-1 xk invierte */
				if(Doc::$D['tt']=='wmaPdp'){
					a_sql::query('UPDATE wma3_pdp1 SET progQty=progQty+'.$L['diff'].', openQty=openQty+'.$L['diff'].' WHERE docEntry=\''.Doc::$D['tr'].'\' AND itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\'  LIMIT 1',[1=>'Error actualizando linea de la orden de producción: ']);
					if(a_sql::$err){ $errs++; break; }
				}
				/*actualizar odp */
				$Di=array('lineStatus'=>$L['lineStatus'],'quantity'=>'+-+quantity+'.$L['diff']);
				$ins=a_sql::insert($Di,array('table'=>'wma3_odp1','qDo'=>'update','wh_change'=>'WHERE id=\''.$L['id'].'\' LIMIT 1'));
				if($ins['err']){ $js=_js::e(3,'Error actualizando linea de la orden de producción: '.$ins['text']); $errs++; break; }
			}
		}
		/* ejecutar actualización */
		if($errs==0){
			$js=_js::r('Se actualizo la orden de producción. Dif: '.$diffs.'.');
			a_sql::transaction(true);
			if(Doc::$D['tt']=='wmaPdp'){
				Doc::log_post(array('serieType'=>'wmaPdp','docEntry'=>Doc::$D['tr'],'lineMemo'=>'From odp. Lineas Actualizadas. Diferencia: '.$diffs,'lines'=>'Y'));
			}
			Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'lineMemo'=>'Lineas Actualizadas. Diferencia: '.$diffs,'lines'=>'Y'));
		}
	}
	echo $js;
}
else if(_0s::$router=='GET odp/tb99'){
	echo Doc::tb99($serieType,$___D);
}

else if(_0s::$router=='POST odp/liberarBase'){ a_ses::hashKey('wma3.oodp.liberar');
	if($js=_js::ise($___D['docEntry'],'Se debe definir el número del documento','numeric>0')){ die($js); }
	else if($js=Doc::status($serieDoc,array('D'=>'Y'))){ die($js); }
	else if(Doc::$D['docStatus']=='O'){ die(_js::e(3,'La orden ya se encuentra liberada.')); }
	$q=a_sql::query('SELECT A.tr,B.itemId,B.itemSzId,B.quantity FROM wma3_oodp A JOIN wma3_odp1 B ON (B.docEntry=A.docEntry) WHERE B.docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo orden de producción',2=>'No se encontró la orden de producción '.$___D['docEntry']));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	a_sql::transaction(); $comit=false; $errs=0;
	$total=0; $Liv=array();
	_ADMS::_app('wma3'); $totalQty=0; $IL=array(); $itemIdB=false;
	while($L=$q->fetch_assoc()){/* actualizar lineas pdp y definir items[] */
		wma3::pdp_putLine(array('docEntry'=>$L['tr'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'qSet'=>'progQty=progQty-'.$L['quantity']));
		$totalQty+=$L['quantity'];
		if(_err::$err){ $js=_err::$errText; $errs++; break; }
		else{
			$k=$L['itemId'].$L['itemSzId'];
			$IL[$k]=array('itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']);
			$itemIdB=$L['itemId'];
		}
	}
	if($errs==0){/* definir fases y operaciones */
		$q2=a_sql::query('SELECT B.lineNum,B.lineType,B.wfaId, B.wopId FROM wma_mpg1 B JOIN wma_mpg2 B2 ON (B2.mpgId=B.mpgId) 
		WHERE B2.itemId=\''.$itemIdB.'\' 
		ORDER BY B.lineNum ASC,CASE WHEN B.lineType=\'F\' THEN 1 END DESC',array(1=>'Error obteniendo fases y operaciones del artículo: ',2=>'El artículo no tiene definido fases y operaciones.'));
		$Di=array(); $nl=0;
		if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; }
		else while($L=$q2->fetch_assoc()){
			if($L['lineType']=='F'){ $L['wfaOrder']=$L['lineNum']; }
			else if($L['lineType']=='O'){ $L['wopOrder']=$L['lineNum']; }
			unset($L['lineNum']);
			$L['docEntry']=$___D['docEntry'];
			foreach($IL as $iks => $Ld){
				$L['itemId']=$Ld['itemId'];
				$L['itemSzId']=$Ld['itemSzId'];
				$L['quantity']=$Ld['quantity'];
				$L['openQty']=$Ld['quantity'];
				$ins2=a_sql::insert($L,array('tbk'=>'wma3_odp20','qDo'=>'insert'));
				if($ins2['err']){ $js=_js::e(3,'Error liberando fases y operaciones: '.$ins2['text']); $errs++; break 2; }
			}
		}
	}
	if($errs==0){/* actualizar estado documento */
		$qu=a_sql::query('UPDATE wma3_oodp SET docStatus=\'O\' WHERE docEntry=\''.$D['docEntry'].'\' LIMIT 1',array(1=>'Error actualizando estado de orden de producción: '));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $js=_js::r('Orden Liberada correctamente.'); a_sql::transaction(true);}
	}
	echo $js;
}


/* anulación relacionada a pdp */
else if(_0s::$router=='PUT odp/statusCancel'){ a_ses::hashKey('wma3.oodp.statusCancel');
	if($js=Doc::status($serieDoc,array('D'=>'Y'))){ die($js); }
	else{
		if(Doc::$D['docStatus']=='O'){
			$qf=a_sql::fetch('SELECT docEntry FROM wma3_odp20 WHERE docEntry=\''.$___D['docEntry'].'\' AND quantity!=openQty LIMIT 1',array(1=>'Error revisando estado de fases y operaciones de la orden.'));
			if(a_sql::$err){ die(a_sql::$errNoText); }
			else if(a_sql::$errNo=='-1'){
				die(_js::e(3,'La orden de producción presenta registros de fases. No se puede anular.'));
			}
		}
		$qf=a_sql::fetch('SELECT A.tt,A.tr,B.docEntry opdp,B.docStatus 
		FROM wma3_oodp A 
		LEFT JOIN wma3_opdp B ON (A.tt=\'wmaPdp\' AND B.docEntry=A.tr) 
		WHERE A.docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo información de orden y relación a opdp: ',2=>'Orden de producción # '.$___D['docEntry'].' no existe'));
		if(a_sql::$err){ die(a_sql::$errNoText); }
		if($qf['docStatus']=='C'){ die(_js::e(3,'No se puede anular una orden relacionada a un documento de planificación que está cerrado: opdp='.$qf['opdp'].'.')); }
		else{
			a_sql::transaction(); $cmt=false; $errs=0;
			/* Modificar pdp si aplica. OJO SI SE LIBERA Y PRESENTA REGISTROS NO DEBERIA MODIFICARSE */
			if($qf['tt']=='wmaPdp'){
			_ADMS::_app('wma3');
				$qf2=a_sql::query('SELECT B.itemId,B.itemSzId,B.quantity,B2.quantity quantityOpdp,B2.openQty 
				FROM wma3_odp1 B 
				LEFT JOIN wma3_pdp1 B2 ON (B2.docEntry=\''.$qf['opdp'].'\' AND B2.itemId=B.itemId AND B.itemSzId=B2.itemSzId) 
				WHERE B.docEntry=\''.$___D['docEntry'].'\' ORDER BY B.itemSzId DESC',array(1=>'Error obteniendo lineas del documento a anular.',2=>'No existe relación de lineas entre la orden de producción '.$___D['docEntry'].', y el documento de planificación '.$qf['opdp'].'.')); $itemId=0;
				if(a_sql::$err){ die(a_sql::$errNoText); }
				while($L=$qf2->fetch_assoc()){
					$itemId=$L['itemId'];
					wma3::pdp_putLine(array('docEntry'=>$qf['opdp'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'qSet'=>'progQty=progQty-'.$L['quantity'].',openQty=quantity-progQty'));
					if(_err::$err){ $js=_err::$errText; $errs++; break; }
				}
			}
			/*Anular odp */
			if($errs==0){
				$serieDoc['lineMemo']=$___D['lineMemo'];
				$js=Doc::statusCancel($serieDoc,array('omitSel'=>'Y'));
				if(Doc::$ok==true){ $cmt=true;
					$js=_js::r('Orden de Producción anulada correctamente.');
				}
			}
			a_sql::transaction($cmt);
			echo $js;
		}
	}
}


?>