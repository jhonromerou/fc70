<?php
require('mpaNty.php');
class mpaTas{
static $tbk='mpa_otas';
static public function rev($P=array()){
	//if($js=_js::ise($P['gType'],'El tipo debe estar definidido','numeric>0')){}
	if(_js::iseErr($P['title'],'Se debe definir el asunto.')){}
	else if($js=_js::textMax($P['title'],200,'Asunto')){ _err::err($js); }
}
static public function post($P=array(),$rep=false){
	self::rev($P); if(_err::$err){ return _err::$errText; }
	$prsName=$P['name']; $cardName=$P['cardName'];
	unset($P['name'],$P['cardName']);
	if($P['calId']>0){ $P['onCal']='Y'; } else{ $P['onCal']='N'; }
	$P['doDateAt']=substr($P['doDate'],11,5);
	$P['endDateAt']=substr($P['endDate'],11,5);
	$P['obj']='T';
	a_sql::transaction();
	$nTy=new mpaNty(); $nTy->userAssg($P);
	$gid=a_sql::qInsert($P,array('tbk'=>self::$tbk,'qk'=>'ud'));
	if(a_sql::$err){ return _err::err('Error guardando tarea.'.a_sql::$errText,3); }
	else{
		$P['gid']=$gid;
		$nTy->mail2Assg($P);
		//if(_err::$err){ return _err::$errText; }
		$P['prsName']=$prsName; $P['cardName']=$cardName;
		a_sql::transaction(true);
		return _js::r('Tarea creada correctamente.',$P);
	}
}
static public function put($P=array()){
	if(_js::iseErr($P['gid'],'Se debe definir Id de tarea a actualizar','numeric>0')){ return _err::$errtext; }
	$prsName=$P['name']; $cardName=$P['cardName'];
	unset($P['name'],$P['cardName']);
	self::rev($P); if(_err::$err){ return _err::$errText; }
	if($P['calId']>0){ $P['onCal']='Y'; } else{ $P['onCal']='N'; }
	$P['doDateAt']=substr($P['doDate'],11,5);
	$P['endDateAt']=substr($P['endDate'],11,5);
	$P['obj']='T';
	a_sql::transaction();
	$nTy=new mpaNty(); $nTy->userAssg($P);
	a_sql::qUpdate($P,array('tbk'=>self::$tbk,'wh_change'=>'gid=\''.$P['gid'].'\' LIMIT 1'));
	if(a_sql::$err){ return _err::err('Error actualizando tarea.'.a_sql::$errText,3); }
	else{
		$P['prsName']=$prsName; $P['cardName']=$cardName;
		$nTy->mail2Assg($P);
		a_sql::transaction(true);
		return _js::r('Tarea actualizada correctamente.',$P);
	}
}
static public function get($P=array(),$P2=array()){
	$wh='';
	$cardId=($P['cardId']);
	$prsId=($P['prsId']);
	a_sql::$limitDefBef=30;
	unset($P['cardId'],$P['prsId']);
	if($cardId>0){
		_ADMS::lib('iPerms');
		iPerms::slp2Crd($cardId);
		if(_err::$err){ return (_err::$errText); }
	}
	if($P['whText']){ $wh .=' AND ('.$P['whText'].') '; };
	$profile=$P['profile']; unset($P['profile']);
	/* Permisos con base tercero o usuario */
	$wh .= a_sql_filtByT($P['wh']);
	$M['L']=array();
	$fie='A.gid,A.completed,A.title,A.gType,A.gPrio,A.gStatus,A.userAssg,A.endDate,A.endDateAt,A.repeatKey';
	$fie.=($P['_fie'])?','.$P['_fie']:'';
	$le='';
	if(!$prsId){
		$fie.=',A.prsId,P.name';
		$le .='LEFT JOIN par_ocpr P ON (P.prsId=A.prsId)';
	}else{ $wh .=' AND A.prsId=\''.$prsId.'\' '; }
	if(!$cardId){
		$fie.=',C.cardId,C.cardName';
		$le .='LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)';
	}else{ $wh .=' AND A.cardId=\''.$cardId.'\' '; }
	$JoinTb='';
	if($P2['Join']){
		$fie .=','.$P2['Join'][0];
		$le .=$P2['Join'][1].' ';
	}
	$qu='SELECT '.$fie.'
	FROM '.self::$tbk.' A
	'.$le.'
	WHERE A.obj=\'T\' '.$wh.' '.$ordBy.a_sql::nextLimit();
	$q=a_sql::query($qu,array(1=>'Error obteniendo tareas.',2=>'No se encontraron resultados.'));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); }
	else{
		while($L=$q->fetch_assoc()){
			$M['L'][]=$L;
		}
		return _js::enc($M);
	}
	if(_err::$err){ return _err::$errText; }
}
static public function getOne($P=array()){
	$fie='A.*,P.name,C.cardName';
	$fie .=($P['_fie'])?','.$P['_fie']:'';
	if($P['__fields']){ $fie .=','.$P['__fields']; }
	$M=a_sql::fetch('SELECT '.$fie.'
	FROM '.self::$tbk.' A
	LEFT JOIN par_ocpr P ON (P.prsId=A.prsId)
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
	WHERE gid=\''.$P['gid'].'\' LIMIT 1',array(1=>'Error obteniendo información.',2=>'La información de gid:'.$P['gid'].', no existe.'));
	if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
	else{
		$js=_js::enc2($M);
	}
	return $js;
}

static public function markComp($D=array()){
	if($js=_js::ise($D['gid'],'Debe definir ID para actualizar estado.','numeric>0')){}
	else{
		$errs=0;
		$Di=array('completed'=>$D['completed']);
		a_sql::transaction(); $cmt=false;
		$doRep=null;
		if($D['completed']=='Y'){
			$Di['completedAt']=date('Y-m-d H:i:s');
			$Di['completUser']=a_ses::$userId;
			$L=a_sql::duplicatGet(array('tbk'=>self::$tbk,'wh'=>'gid=\''.$D['gid'].'\'','noFie'=>'gid','fN'=>array('gidParent'=>$D['gid'])));
			if(_err::$err){ return false; }
			/* Duplicar tarea si ya no tiene repeticion */
			if($L['repeatKey']!='' && $L['repeatKey']!='N' && $L['repeatDo']=='N'){
				_ADMS::lib('dateCicle');
				$L['completDate']=$Di['completedAt'];
				//Repetir con base a fecha inicial iD
				$R=dateCicle::calc($L['repeatKey'].'oniD',$L);
				if(!is_array($R)){ $R=array(); }
				unset($L['completDate'],$L['gStatus']);
				$L['doDate']=$R['nextDate'];
				$L['endDate']=$R['nextDate'];
				$doRep=self::post($L,'R');
				if(_err::$err){ $js=_err::$errText; }
				$Di['repeatDo']='Y';
			}
		}
		else{ $Di['completedAt']=''; $Di['completUser']=0; }
		if($errs==0){
			a_sql::qUpdate($Di,array('tbk'=>self::$tbk,'qku'=>'ud','wh_change'=>'gid=\''.$D['gid'].'\' LIMIT 1'));
			if(a_sql::$err){ return _js::e(3,'Error actualizando tarea: '.a_sql::$errText); }
			else{ $cmt=true;
				$js=_js::r('Tarea cambio de estado correctamente.',$doRep);

			}
		}
		a_sql::transaction($cmt);
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
}
static public function sendCopy($D=array()){
	if(_js::iseErr($D['gid'],'Se debe definir Id de tarea','numeric>0')){}
	else if($js=_err::iff(!is_array($D['email']),'No se envio información de correos.')){ _err::err($js); }
	else if($js=_err::iff(count($D['email'])==0,'No se definieron correos para enviar la copia.')){ _err::err($js); }
	else if($js=_err::iff(count($D['email'])>2,'No se puede enviar más de 3 copias.')){ _err::err($js); }
	else{
		foreach($D['email'] as $v){
			if(!preg_match('/\@/',$v)){  _err::err('Se debe definir el correo en el campo.',3); break;  }
		}
		if(_err::$err){ return _err::$errText; }
		$qT=a_sql::fetch('SELECT U.userName, U2.userName userNameFrom, A.title,A.shortDesc,P.name prsName, C.cardName,A.endDate,A.endDateAt
		FROM '.self::$tbk.' A
		LEFT JOIN a0_vs0_ousr U ON (U.userId=A.userId)
		LEFT JOIN a0_vs0_ousr U2 ON (U2.userId=A.userAssg)
		LEFT JOIN par_ocpr P ON (P.prsId=A.prsId)
		LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
		WHERE A.gid=\''.$D['gid'].'\' LIMIT 1',array(1=>'Error obteniendo información de la tarea a copiar.',2=>'La tarea no existe.'));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			_ADMS::lib('_2d,xCurl,_File,_MailCurl');
			$m= new _MailCurl(array('accK'=>'mpaTasCopy','templateCode'=>'crmTaskCopy'));
			$m->addFile(array('file_jsData'=> array('userName'=>$qT['userName'],'userNameFrom'=>$qT['userNameFrom'],'title'=>$qT['title'],'taskNotes'=>$qT['shortDesc'],'prsName'=>$qT['prsName'],'cardName'=>$qT['cardName'], 'endDate'=>_2d::f($qT['endDate'].' '.$qT['endDateAt'],'d M H:i'))));
			$m->send(['mailTo'=>implode(',',$D['email'])]);
			if(_err::$err){ return _err::$errText; }
			else{ $js=_js::r('Copias Enviadas.'); }
		}
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
}
}
?>
