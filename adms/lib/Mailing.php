<?php
class Mailing{
static $tbTemp='ema_otmp';
static $err=false; static $errText='';
static public function ncf($k='',$D=array()){
	if($js=_js::ise($D['userId'],'Se debe definir la ID del usuario.','numeric>0')){
		self::$err=true; self::$errText=$js; return false;
	}
	$q=a_sql::fetch('SELECT U.userEmail,A.emailNty,A.mobileNty FROM ema_oncf A JOIN a0_vs0_ousr U ON (U.userId=A.userId) WHERE A.notyName=\''.$k.'\' AND A.userId=\''.$D['userId'].'\' LIMIT 1',array(1=>'Error obteniendo estado de notificación: ',2=>'La notificación ('.$k.') no ha sido definida para el usuario.'));
	if(a_sql::$err){ self::$err=true; self::$errText=a_sql::$errNoText; return false; }
	else{
		if(array_key_exists('emailNty',$D)){
			if($q['emailNty']=='Y'){ return $q; }
			else{ self::$err=true; self::$errText=_js::e(3,'emailNty no esta definido como Y.'); return false; }
		}
		if($q['emailNty']=='Y'){ return $q; }
		return $q;
	}
}
static public function fromTemp($wh=array(),$D=array(),$P=array()){
	$wh=a_sql_filtByT($wh);
	$q=a_sql::fetch('SELECT * FROM '.self::$tbTemp.' WHERE 1 '.$wh.' LIMIT 1',array(1=>'Error obteniendo plantilla de email: ',2=>'La plantilla '.$tId.' no existe'));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); return ''; }
	else{
		$html=$q['tHtml'];
		if(is_array($D)) foreach($D as $kName => $value){
			$html=preg_replace('/\<\{'.$kName.'\}\>(.*)\<\/\{'.$kName.'\}\>/','$1',$html);
			$html=preg_replace('/\{\{'.$kName.'\}\}/',$value,$html);
		}
		/*quitar posibles que no esten en campos D */
		$html=preg_replace('/\{\{(\w+)\}\}/','',$html);
		$html=preg_replace('/\<\{.*\}\>(.*)\<\/\{.*\}\>/','',$html); //quitar <$logoUrl$>
		if($P['encodeHTML']=='Y'){ return htmlentities($html); }
		return $html;
	}
}
static public function ics_create($P=array()){
	$tim0=time();
	$tim1=strtotime($P['date1']);
	$tim2=strtotime($P['date2']); //20190625T192100Z
	$ics='BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//ADM Sistems//NONSGML Calendar//ES
TZID:America/Bogota
';
if($P['method']){ $ics.= 'METHOD:'.$P['method'].'
'; }
$ics.='BEGIN:VEVENT
DTSTAMP:'.date('Ymd',$tim0).'T'.date('Hi',$tim0).'00
ORGANIZER;CN='.$P['cn'].':mailto:'.$P['mailto'].'
DTSTART:'.date('Ymd',$tim1).'T'.date('Hi',$tim1).'00
DTEND:'.date('Ymd',$tim2).'T'.date('Hi',$tim2).'00
SUMMARY:'.$P['title'].'
';
	unset($P['date1'],$P['date2'],$P['cn'],$P['mailto'],$P['title'],$P['method']);
	/*UID,LOCATION*/
	if(is_array($P)) foreach($P as $k=>$v){ $ics .= strtoupper($k).':'.$v.'
'; }
	$ics.='END:VEVENT
END:VCALENDAR';
	return $ics;
}
}
?>
