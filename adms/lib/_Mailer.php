<?php
$rootPATH = realpath($_SERVER['DOCUMENT_ROOT']);
require(c::$V['PATH_libexterna'].'phpmailer/class.phpmailer.php');

class _Mail{
	static $mail;
	static $err=false; static $errText='';
	static public function readAddress($email=''){
		preg_match('/\<(.*)\>/',$email,$ma);
		if($ma[1]) return $ma[1];
		else return $email;
	}
	static public function send($P=array(),$aD=array()){
		self::$err=false;
		/* asunt, to, body,
		fromName: Plataforma  ADMS
		fromEmail: noresponse@admsistems.com
		*/
		$EConf = c::$EmailSvr;
		if($P['mailTo']){ $P['to'] = $P['mailTo']; }
		if($P['to'] == ''){ $js = array('errNo'=>1,'text'=>'No se ha definido ningun destinatario.');}
		else if($P['asunt'] == ''){ $js =  array('errNo'=>1,'text'=>'El asunto no puede estar vacío'); }
		else{
			$to = $P['to'];
			self::$mail = new PHPMailer();
			//self::$confirmReadingTo="johnromero492@gmail.com";// recibir confirmación lectura
			self::$mail->CharSet = 'UTF-8';
			$multiTo = explode(',',$to);
			$body = '<html>
			<head>
			<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8">
			</head>
			<body>'.
			$P['body'].
			'</body>
			</html>';
			$fromName = ($P['fromName']) ? $P['fromName'] : 'Plataforma ADM';
			$fromEmail = $EConf['email'];
			if($P['fromEmail']){ $fromEmail = $P['fromEmail']; }
			else{
				$fromEmail = ($EConf['fromEmail'])?$EConf['fromEmail']:$fromEmail;
			}
			if($P['replyTo']){ $replyTo = $P['replyTo']; }
			else{
				$replyTo = ($EConf['replyTo']) ? $EConf['replyTo']:$fromEmail;
			}

			$replyToName = ($P['replyToName']) ? $P['replyToName'] : 'Plataforma ADM';
			self::$mail->IsSMTP();
			self::$mail->addCustomHeader('X-SES-CONFIGURATION-SET', 'ConfigSet');
			if($aD['heads']){
				foreach($aD['heads'] as $k1=>$v1){
					self::$mail->addCustomHeader($k1,$v1);
				}
			}
			self::$mail->SMTPDebug  = 1;
			self::$mail->Host = $EConf['host'];
			self::$mail->SetFrom($fromEmail,$fromName);
			self::$mail->ClearReplyTos();
			self::$mail->AddReplyTo($replyTo,$replyToName);
			self::$mail->Subject =  '=?ISO-8859-1?B?'.base64_encode(utf8_decode($P['asunt'])).'=?=';
			if(count($multiTo) >1){ foreach($multiTo as $to2){
				if($to2 != ''){
					$realAddress = self::readAddress($to2);
					$addA = @self::$mail->AddAddress($realAddress);
				if(preg_match('/^Invalid address/is',$addA)){ return array(1,'Dirección Incorrecta: '.$realAddress); }
				};
			}}
			else{
				$realAddress = self::readAddress($to);
				$addA = @self::$mail->AddAddress($realAddress);
				if(preg_match('/^Invalid address/is',$addA)){ return array(1,'Dirección Incorrecta: '.$realAddress); }
			}
			if($aD['F']){/* Añadir archivos [n]{}  */
				foreach($aD['F']['tmp_name'] as $n => $L){
					$tmp_name=$aD['F']['tmp_name'][$n];
					$f_name=$aD['F']['name'][$n];
					self::$mail->AddAttachment($tmp_name,$f_name);
				}
			}
			self::$mail->IsHTML(true);
			self::$mail->CharSet = 'UTF-8';
			self::$mail->Port = $EConf['port']; //587 ipage, 465 2host;
			self::$mail->SMTPAuth = true;
			if($EConf['secure']){
				self::$mail->SMTPSecure = ($EConf['secure'])?$EConf['secure']:'ssl';
			}
			self::$mail->Username = $EConf['email'];
			self::$mail->Password = $EConf['password'];
			self::$mail->MsgHTML($body);
			try{
				@$send = self::$mail->Send();
				if(!$send){
					$js = array('err'=>1,'errNo'=>1,'text'=>'Error en envio: '.self::$mail->ErrorInfo);
					unset($EConf['password']);
					self::$err=true; self::$errText='{"err:"1","errNo":"1","text":"Error en envio: '.self::$mail->ErrorInfo.'"}';;
				}
				else{ $js = array('ok'=>1,'text'=>'Correo Enviado correctamente.'); }
			}
			catch(Exception $ex) {
				$js = array('err'=>1,'errNo'=>1,'text'=>'Error en envio: '.$ex);
				self::$err=true; self::$errText='{"errNo":"3","text":"Error en envio: '.$ex.'"}';
			}
		}
		return $js;
	}
	static public function curl($D=array(),$P=array()){
		xCurl::$attachV='v3';
		//uid, uri,fromEmail
		$P['uid']=c::$EmailSvr['uid'];//api:key
		if(!$P['subject'] && $P['asunt']){ $P['subject']=$P['asunt']; }
		if($D['fromEmail']){ $D['from'] = $D['fromEmail']; }
		else{
			$D['from'] = (c::$EmailSvr['fromEmail'])?c::$EmailSvr['fromEmail']:'';
		}
		if($D['fromName']){ $D['from'] = $D['fromName'].'<'.$D['from'].'>'; }
		$D['to']=$D['mailTo']; unset($D['mailTo']);
		$r=xCurl::post(c::$EmailSvr['uri'],$D,$P);
		if($r['id']){ return $r; }
		else{ return ['errNo'=>3,'text'=>$r['message']]; }
		return $r;
	}
	static public function mailGun($D=array(),$P=array()){
		return self::curl($D,$P);
	}
}
?>