<?php
class JStor{
static $rootPath='';
static $filePath=''; //E:/ o /
static $cD=array(); //filePath
static $F=array(); //archivos subidos
static public function getSize($bytes=0,$mb='Mb'){
	$bytes=($bytes==0)?16:$bytes;
	switch($mb){
		case 'Gb': $b = number_format($bytes/1073741824,6); break;
		case 'Mb': $b = number_format($bytes/1048576,6); break;
		case 'Kb': $b = number_format($bytes/1024,6); break;
		case 'Mb': $b = number_format($bytes/1048576,6); break;
		default: $b = number_format($bytes/1048576,6); break;
	}
	return $b;
}
static public function getSizeText($bytes=0){
	$bytes=($bytes==0)?16:$bytes;
	if ($bytes >= 1073741824){ $bytes = number_format($bytes / 1073741824, 2) . ' Gb'; }
	elseif ($bytes >= 1048576){ $bytes = number_format($bytes / 1048576, 2) . ' Mb'; }
	elseif ($bytes >= 1024){ $bytes = number_format($bytes / 1024, 2) . ' Kb'; }
	elseif ($bytes > 1){ $bytes = $bytes . ' Bytes'; }
	elseif ($bytes == 1){ $bytes = $bytes . ' Byte'; }
	else{ $bytes = '0 Bytes'; }
	return $bytes;
}
static public function getExt($name=''){
	$ext = explode('.',$name); $c = count($ext)-1;
	$ext = $ext[$c];
	return $ext;
}
static public function getError($errNo=0,$si=''){
 if($errNo){
  $ori=' on[JStor::getError()]';
	 if($errNo == 1){
  $svr1= ' [DEFINE: '.ini_get('upload_max_filesize').']';
		return _err::err('El archivo excede el tamaño máximo permitido por el servidor. '.$svr1 .$ori,3);
  // ' Max. Mb: '.ini_get('upload_max_filesize')
	 }
	 else if($errNo == 2){
		 return _err::err('El archivo excede el tamaño máximo (Er-02), Peso Actual:'.$FIL['size'].$ori,2);
	 }
	 else{ return _err::err('Error No. '.$errNo.$ori,3); }
 }
 else{ return false; }
}
static public function getTypeMe($ext=''){
	if(preg_match('/(png|jpeg|jpg|gif)/is',$ext)){ $fileType = 'img';}
	else if(preg_match('/(xls|xlsx)/is',$ext)){ $fileType = 'xls';}
	else if(preg_match('/(csv|tsv)/is',$ext)){ $fileType = 'xls';}
	else if(preg_match('/(txt|rtf)/is',$ext)){ $fileType = 'txt';}
	else if(preg_match('/(html)/is',$ext)){ $fileType = 'html';}
	else if(preg_match('/(avi|mpeg|mp4)/is',$ext)){ $fileType = 'video';}
	else if(preg_match('/(mp3|wav|ra|au|aiff)/is',$ext)){ $fileType = 'audio';}
	else if(preg_match('/(doc|docx)/is',$ext)){ $fileType = 'doc';}
	else if(preg_match('/(pdf)/is',$ext)){ $fileType = 'pdf';}
	else { $fileType = $ext; }
	return $fileType;
}
static public function multiLoad($Fx=array(),$P=array()){
	$ori=' on[JStor::multiLoad()]';
	$INF = array(); $errs=0;
 $lastDir='';
	$relPath = $P['filePath']; $num=1;
 $baseName=self::randName(10); //nombre aleatorio
 $baseNum=substr(time(),6); /* a2345678b(8475+) */
	if(!preg_match('/\/$/',$relPath)){ $relPath .='/'; }
	foreach($Fx['name'] as $n => $Fl){
		$F=array('name'=>$Fx['name'][$n],'type'=>$Fx['type'][$n],'tmp_name'=>$Fx['tmp_name'][$n],'error'=>$Fx['error'][$n],'size'=>$Fx['size'][$n]);
		if($F['error']!= 0){
			$INF['error'] = 1; $INF['text'] = self::getError($F['error']).'.'.$ori;
   $errs++; break;
		}
		else{
			//tt,tr,svr,fileType,fileSize,fileSizeText,fileName,file
   $dirCreate=c::$V['STOR_ROOT'].$relPath;
   if($lastDir!=$dirCreate){
    self::mkDir($dirCreate);
    if(_err::$err){ return _err::$errText; }
   } $lastDir=$dirCreate;
   $nName=$baseNum.$baseName; $baseNum++;
			$ext = self::getExt($F['name']);
   $pathSave =c::$V['STOR_ROOT'].$relPath . $nName . '.'.$ext;;
			if(copy($F['tmp_name'],$pathSave)){
				$INF['svr'] = c::$V['STOR_SVR'];
				$INF['file'] = $relPath . $nName . '.'.$ext;
				$INF['purl'] = c::$V['STOR_URI'] . $INF['file'];
				$INF['fileSize'] = self::getSize($F['size']);
				$INF['fileSizeText'] = self::getSizeText($F['size']);
				$INF['fileName'] = $F['name'];
				$INF['fileType'] = self::getTypeMe($ext);;
			}
			else{
				$errors= error_get_last();
				$pathErr = (preg_match('/failed to open stream/',$errors['message']));
				$textErr = ($pathErr) ? 'El directorio de destino no existe: '.$relPath : 'No se subió el archivo (2).';
				$errPhp = $errors['type'].'. '.$errors['message'];
				$R['errNo'] = 1; $errs++; break;
				$R['text']= $textErr.' ('.$errPhp.') '.$ori;
			}
   self::$F[] = $INF;
  }
	}
 /* Eliminar todos los archivos */
	if($errs>0){
  _err::err($INF['text'],3);
  self::delLast();
	}
	if($P['getL']){ return self::$F; }
	return json_encode(self::$F);
}
static public function delLast(){
 foreach(self::$F as $n =>$L){ @unlink($L['file']); }
}
/* Files */
static public function upTotal($Fils,$mb='Mb'){
 $FD=array('upTotal'=>0,'maxFile'=>0,'upFiles'=>0);
	foreach($Fils['name'] as $n =>$X){
		$FD['upTotal'] +=$Fils['size'][$n];
		if($Fils['size'][$n]>$FD['maxFile']){
   $FD['maxFile']=$Fils['size'][$n];
   $FD['maxName']=$Fils['name'][$n];
  }
		self::getError($Fils['error'][$n]);
		if(_err::$err){ die(_err::$errText); }
		$FD['upFiles']=$FD['upFiles']+1;
	}
 $FD['upTotal']=self::getSize($FD['upTotal'],$mb);
 $FD['maxFile']=self::getSize($FD['maxFile'],$mb);
 return $FD;
}
static public function randName($xLen=20) {
 $chrs='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 $iLen = strlen($chrs)-1;
 $xString = '';
 for($i = 0; $i < $xLen; $i++){
  $xString .= $chrs[mt_rand(0, $iLen)];
 }
 return $xString;
}
static public function mkDir($dir=false){
	if($dir && !file_exists($dir)){
		if(!@mkdir($dir, 0777, true)){
			_err::err('Error creando directorio '.$dir.' on AttachMin::mkDir',3);
		}
	}
	return $dir;
}
static public function copy($tmp='',$fil='',$path=false){
	$path=self::mkDir($path);
	if(_err::$err){ return false; }
	$filePath=$path.$fil;
	self::$cD['filePath']=$filePath;
	$copya=@copy($tmp,$filePath);
	if($copya){ return true; }
	else if(!file_exists($filePath)){
		_err::err('El archivo no fue copiado correctamente on AttachMin::copy(): ',3);
		return false;
	}
	else{
		_err::err('No se subió el archivo',3);
		return false;
	}
}
/* origins */
static public function origins(){
	$svr = $_SERVER['REMOTE_ADDR'];
	if($svr=='127.0.0.1'){ return false; }
	$ori=(is_array(c::$V['STOR_ORIGINS']))?c::$V['STOR_ORIGINS']:array();
	if(!array_key_exists($svr,$ori)){
  return _err::err('('.$svr.') is not an authorized server to upload images. (err-S901) on[Stor::origins()]. '.implode(',',c::$V['STOR_ORIGINS']),3);
	}
	else{ return false; }
}
}
?>
