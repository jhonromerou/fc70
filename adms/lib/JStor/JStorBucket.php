<?php
class JStorBucket{
	var $D=array(); var $P=array();
	var $ocardCode;
	var $DB=array('h'=>'','u'=>'','p'=>'','db'=>'');
	function __construct($P=array(),$DB=array()){
		if($P['D']){ $this->D= $this->P['D'];  }
		$this->ocardCode=$P['ocardCode']; unset($P['D'],$P['ocardCode']);
		$this->P=$P['ocardCode'];
		if($DB){ $this->DB=$DB; }
	}
	function setManual($P){ $this->P=$P; }
	function setFromDb(){
		a_sql::dbase($this->DB);
		_js::o('StorBucket::setFromDb()');
		if(_js::iseErr($this->ocardCode,'ocardcode no definido.'.$ori)){ return _err::$errText; }
		$this->D=a_sql::fetch('SELECT C.ocardId,P.filePath,P.invStatus,P.storMnt,P.storOpen,P.storUsaged,P.fileMaxSize,P.httpAu,P.storBucket,P.svr
			FROM ocard C
			LEFT JOIN mdl_stor P ON (P.ocardId=C.ocardId)
			WHERE C.ocardCode=\''.$this->ocardCode.'\' LIMIT 1',array(1=>'Error obteniendo información de almacenamiento para ocard.'.$ori,2=>'El ocard '.$this->ocardCode.' no está registrado.'.$ori));
		if(a_sql::$err){ $this->D=json_decode(a_sql::$errNoText,1); return _err::err(a_sql::$errNoText); }
		else if($this->D['invStatus']=='S'){ return _err::err('Su servicio de almanecamiento está suspendido. Solicite soporte para habilitarlo.',3);  }
		else{
			//usados en JStor::multi
			c::$V['STOR_URI']=$this->D['storBucket'];
			//c::$V['STOR_ROOT'] / o E:/svr/, no se pone en ruta, solo en GET
			c::$V['STOR_SVR']=$this->D['svr'];
		}
	}
	function origins(){
		_js::o('StorBucket::origins()');
		//if($_SERVER['REMOTE_ADDR']=='127.0.0.1'){ return false; }
		if($this->D['httpAu']==''){ return _err::err('Rules to origin not defined.',3); }
		if($this->D['httpAu']!='*'){/* validar origen */
			$sep=explode(' ',$this->D['httpAu']);
			$ok=0; $origen=$_SERVER['HTTP_ORIGIN'];
			foreach($sep as $n=>$h){
				if($h=='localhost'){ $ok=true; break; }
				$re=preg_quote($h,'/');
				if(preg_match('/\.?'.$re.'$/i',$origen)){ $ok=true; break; }
			}
			if($ok==0){ _err::err('No se puede realizar está acción. Locked '.$origen,3); }
		}
	}
	function freeSpace($UFILE=array()){
		$storOpen=$this->D['storOpen']*1;
		$storMnt=$this->D['storMnt']*1;
		if($storOpen<=0){ return _err::err('No tiene espacio libre en el disco. '.$storOpen.' de '.$storMnt.' Mb'.$ori); }
		if($UFILE['upTotal']){
			if($UFILE['upTotal']>$storOpen){
				return _err::err('El tamaño total de los archivos ('.($UFILE['upTotal']*1).' Mb) superan el espacio disponible ('.$storOpen.' Mb).'.$ori,3);
			}
			else if($UFILE['maxFile']>$this->D['fileMaxSize']){
				return _err::err('El archivo '.$UFILE['maxName'].', supera el tamaño por archivo a subir ('.($this->D['fileMaxSize']*1).') en Mb.'.$ori,3);
			}
			else{
				$this->D['upTotal']=$UFILE['upTotal'];
				$this->D['upFiles']=$UFILE['upFiles']; //numero archivos
			}
		}
	}
	function put($isUpd=true){
		$ori=' on[JStorBucket::put]';
		if(!$this->D['upTotal']){ $this->D['upTotal']=0.001; }
		$Di=array('ocardId'=>$this->D['ocardId']);
		$Di['storOpen=']='storOpen-'.$this->D['upTotal']; //poner +-
		$Di['storUsaged=']='storUsaged+'.$this->D['upTotal'];
		$qi=a_sql::qUpdate($Di,array('tbk'=>'mdl_stor','wh_change'=>'ocardId=\''.$this->D['ocardId'].'\' LIMIT 1'));
		if(a_sql::$err){ return _err::err(a_sql::$errText.$ori,3); }
		$dateUpd=date('Y-m-d H:i:s'); $periodo=substr($dateUpd,0,7);
		//Almacenamiento usado en totales por periodo
		if($isUpd!='N'){
			$Di=array('ocardId'=>$this->D['ocardId'],'periodo'=>$periodo,'dateUpd'=>$dateUpd,
			'upTotal='=>'upTotal+'.$this->D['upTotal'],'upTotal'=>$this->D['upTotal'],
			'upFiles='=>'upFiles+'.$this->D['upFiles'],'upFiles'=>$this->D['upFiles']
			);
			$qi=a_sql::uniRow($Di,array('tbk'=>'mdl_stor1','wh_change'=>'ocardId=\''.$this->D['ocardId'].'\' AND periodo=\''.$periodo.'\' LIMIT 1'));
		}
	}
}
?>
