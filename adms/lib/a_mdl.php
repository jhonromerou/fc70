<?php
class a_cnf{
static $sql=array();
}
class a_mdl{
static public function ctrl($P=array()){
	_js::$mdlOn='a_mdl::ctrl';
	if(!$P['period']){ $P['period']=date('Y-m'); }
	if(!$P['qty']){ $P['qty']=1; }
	$Di=array('ocardId'=>$P['ocardId'],'mdl'=>$P['mdl'],'variName'=>$P['variName'],'period'=>$P['period']);
	$Di['._.qty']='+-+qty+'.$P['qty'];
	$Di['qty']=$P['qty'];
	$qi=a_sql::insert($Di,array('table'=>'mdl_ctrl','wh_change'=>'WHERE ocardId=\''.$P['ocardId'].'\' AND variName=\''.$P['variName'].'\' AND period=\''.$P['period'].'\' LIMIT 1'));
	if($qi['err']){ return _js::e(3,$qi['text']); }
	else{ return _js::r($P['variName'].': actualizada correctamente para periodo.'); }
}
static public function oaut($P=array()){
	$ori=' on[a_mdl::oaut]';
	if($js=_js::ise($P['ocardCode'],'Código de Cliente no definido.'.$ori)){ die($js); }
	else if($js=_js::ise($P['accK'],'accK no definido para revisión de permiso.'.$ori)){ die($js); }
	$q0=a_sql::fetch('SELECT C.ocardId,C.ocardName,M.actived,M.accK,M.jsPars
FROM ocard C
LEFT JOIN mdl_oaut M ON (M.ocardId=C.ocardId AND M.accK=\''.$P['accK'].'\')
WHERE C.ocardCode=\''.$P['ocardCode'].'\' LIMIT 1',array(1=>'Error obteniendo información de autorización para ocard.'.$ori,2=>'El ocard '.$P['ocardCode'].' no está registrado.'.$ori));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	if(!$q0['accK']){ die(_js::e(3,$P['ocardCode'].': Permiso no definido para esta acción: '.$P['accK'].$ori)); }
	if($q0['actived']=='N'){ die(_js::e(3,'Permiso no autorizado para esta acción: '.$P['accK'].$ori)); }
	return $q0;
}
static public function autByCode($P=array()){
	$ori=' on[a_mdl::autByCode()]';
	$ocardcode=JRoute::reqHeads('ocardcode');
	if(!isset($ocardcode)){ die(_js::e(3,'Tooken de cliente no definido: '.$P['accK'].$ori)); }
	$P['accK']=JRoute::reqHeads('acck');
	if($js=_js::ise($P['accK'],'accK no definido para revisión de permiso.'.$ori)){ die($js); }
	$q0=a_sql::fetch('SELECT C.ocardId,C.ocardName,M.actived,M.accK,M.jsPars
FROM ocard C
LEFT JOIN mdl_oaut M ON (M.ocardId=C.ocardId AND M.accK=\''.$P['accK'].'\')
WHERE C.ocardCode=\''.$ocardcode.'\' LIMIT 1',array(1=>'Error obteniendo información de autorización para ocard.'.$ori,2=>'El código no está registrado ('.$ocardcode.').'.$ori));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	if(!$q0['accK']){ die(_js::e(3,$ocardcode.': Permiso no definido para esta acción: '.$P['accK'].$ori)); }
	if($q0['actived']=='N'){ die(_js::e(3,'Permiso no autorizado para esta acción: '.$P['accK'].$ori)); }
	$q0['J']=json_decode($q0['jsPars'],1);
	if(!is_array($q0['J'])){ $q0['J']=array(); }
	return $q0;
}
static public function stor($P=array(),$P2=array()){
	$ori=' on[a_mdl::stor]';
	if($js=_js::ise($P['ocardCode'],'Código de Cliente no definido.'.$ori)){ die($js); }
	$q0=a_sql::fetch('SELECT C.ocardId,C.ocardName,P.tpath,P.storMnt,P.storOpen,P.storUsaged,P.fileMaxSize,P.httpAu
FROM ocard C
LEFT JOIN mdl_stor P ON (P.ocardId=C.ocardId )
WHERE C.ocardCode=\''.$P['ocardCode'].'\' LIMIT 1',array(1=>'Error obteniendo información de almacenamiento para ocard.'.$ori,2=>'El ocard '.$P['ocardCode'].' no está registrado.'.$ori));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	$storOpen=$q0['storOpen']*1;
	$storMnt=$q0['storMnt']*1;
	if($q0['httpAu']!='*'){
		$sep=explode(' ',$q0['httpAu']);
		$ok=0; $origen=$_SERVER['HTTP_ORIGIN'];
		foreach($sep as $n=>$h){
			$re=preg_quote($h,'/');
			if(preg_match('/\.?'.$re.'$/i',$origen)){ $ok=true; break; }
		}
		if($ok==0){ die(_js::e(3,'No tiene permisos para subir este archivo (err_0x001)'.$ori)); }
	}
	if($storOpen<=0){ die(_js::e(3,'No tiene espacio libre en el disco. '.$storOpen.' de '.$storMnt.' Mb'.$ori)); }
	if($UpI=$P2['upI']){
		if($UpI['t']>$storOpen){ die(_js::e(3,'El tamaño de los archivos a subir ('.($UpI['t']*1).') superan el espacio disponible ('.$storOpen.') en Mb.'.$ori)); }
		else if($UpI['max']>$q0['fileMaxSize']){ die(_js::e(3,'El archivo '.$UpI['maxName'].', supera el tamaño por archivo a subir ('.($q0['fileMaxSize']*1).') en Mb.'.$ori)); }
		else{
			self::storPut(array('ocardId'=>$q0['ocardId'],'qty'=>$UpI['t']));
			if(_err::$err){ die(_err::$errText); }
		}
	}
	return $q0;
}
static public function storGet($P=array(),$revi=true){
	//ocardcode,upTotal,maxFile
	$ori=' on[a_mdl::stor]-'.$P['ocardCode'];
	if($js=_js::ise($P['ocardCode'],'Código de Cliente no definido.'.$ori)){ die($js); }
	$q0=a_sql::fetch('SELECT C.ocardId,P.svr,C.filePath,P.storMnt,P.storOpen,P.storUsaged,P.fileMaxSize,P.httpAu
FROM ocard C
LEFT JOIN mdl_stor P ON (P.ocardId=C.ocardId )
WHERE C.ocardCode=\''.$P['ocardCode'].'\' LIMIT 1',array(1=>'Error obteniendo información de almacenamiento para ocard. '.$ori.': ',2=>'El ocard '.$P['ocardCode'].' no está registrado.'.$ori));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); return false; }
	if($revi==true){ self::storRev($q0,$P); }
	return array('ocardId'=>$q0['ocardId'],'filePath'=>$q0['filePath'],'svr'=>$q0['svr']);
}
static public function storPut($P=array()){
	$ori=' on[a_mdl::storPut]';
	if(!$P['qty']){ $P['qty']=0.01; }
	$Di=array('ocardId'=>$P['ocardId']);
	$Di['storOpen=']='storOpen-'.$P['qty']; //poner +-
	$Di['storUsaged=']='storUsaged+'.$P['qty'];
	$qi=a_sql::qUpdate($Di,array('tbk'=>'mdl_stor','wh_change'=>'ocardId=\''.$P['ocardId'].'\' LIMIT 1'));
	if(a_sql::$err){ _err::err(3,a_sql::$errText.$ori); }
}
static public function storRev($q0=array(),$P=array()){
	$ori=' on[a_mdl::storRev]-'.$P['ocardCode'];
	if($q0['httpAu']!='*'){
		$sep=explode(' ',$q0['httpAu']);
		$ok=0; $origen=$_SERVER['HTTP_ORIGIN'];
		foreach($sep as $n=>$h){
			$re=preg_quote($h,'/');
			if(preg_match('/\.?'.$re.'$/i',$origen)){ $ok=true; break; }
		}
		if($ok==0){ _err::err('No tiene permisos para subir este archivo (err-S001)'.$ori,3); return false; }
	}
	if($q0['storOpen']<=0){
		_err::err('Alcanzó el máximo de almacenamiento permitido '.($q0['storMnt']*1).' Mb (err-S002).'.$ori,3);
	}
	else if($P['upTotal']>$q0['storOpen']){ _err::err('No hay espacio suficiente para subir la información. Disponible: '.($q0['storOpen']*1).' Mb. (err-S003)'.$ori,3); }
	else if($P['maxFile']>$q0['fileMaxSize']){ _err::err('No se permiten subir archivos que pesen más de '.($q0['fileMaxSize']*1).' Mb. (err-S004)'.$ori,3); }
}

static public function clogs($P=array()){
	_js::$mdlOn='a_mdl::clogs';
	$Di=array('tt'=>$P['tt'],'lineMemo'=>$P['lineMemo']);
	$qi=a_sql::insert($Di,array('table'=>'mdl_clogs','qDo'=>'insert'));
	if($qi['err']){ return _js::e(3,$qi['text']); }
	else{ return _js::r($P['tt'].': Log registrado'); }
}

static public function login($ocardcode='',$P=array()){
	//Siempre se usa este en el REST, usado en emailSend para EmailSvr
	$ori =' on[a_mdl::login()]';
	if($js=_js::ise($ocardcode,'codigo ocard no definido ('.$ocardcode.').'.$ori)){ die($js); }
	a_sql::dbase(c::$Sql2,' --> a_mdl::login');
	$q=a_sql::fetch('SELECT A.ocardId,A.ocardName,A.sqlh sql_h, A.sqlu sql_u, A.sqlp sql_p, A.sqldb sql_db,A.mailConf,A.errMsg,A.cartMsg,A.cartDays,A.cardId
	FROM ocard A
	WHERE A.ocardcode=\''.$ocardcode.'\' LiMIT 1',[1=>'Error realizando login.'.$ori.' ',2=>'El código de cliente ('.$ocardcode.') no existe en la base de datos.'.$ori]);
	if(a_sql::$err){ die(a_sql::$errNoText); }
	c::$EmailSvr=json_decode($q['mailConf'],1);
	$Cnf=c::$V['mdlLogin'];
	if($Cnf['apiReq']=='Y'){ //consultando REST
		if($q['errMsg']!='N'){ die(_js::e(3,'errMsg: '.$q['errMsg'])); }
		else if(1 && $q['cartMsg']!='N'){
			$daysDue=($q['cartDays']>0)?$q['cartDays']:15;
			$d15=date('Y-m-d',strtotime('-'.$daysDue.'days'));
			$hoy=strtotime(date('Y-m-d'));
			$De=['errNo'=>3,'_format'=>'html','text'=>'Estimado cliente, no se puede acceder a la información, presenta facturas pendientes de pago con <b>más de '.$daysDue.' dias de vencimiento</b>, si considera que es un error o ya realizo los pagos; escribamos al whatsapp <b>323 338 9324</b> o al correo <b>info@admsistems.com</b>. Lamentamos los inconvenientes.<p><b>Código error</b>: cartMsg='.$q['cartMsg'].'</p>'];
			$qL=a_sql::query('SELECT C.cardName, A.docNum,A.docDate,A.dueDate,A.docTotal,A.balDue
				FROM mapp_jrom.par_ocrd C 
				JOIN mapp_jrom.gvt_oinv A ON (A.cardId=C.cardId)
				WHERE C.cardId IN('.$q['cardId'].') AND A.dueDate<=\''.$d15.'\' AND A.balDue>0
				',[1=>'Y',2=>'Y']);
			if(a_sql::$errNo==1){ $De['text']='Error verificando su estado de cuentas';  }
			else if(a_sql::$errNo==-1){
				$De['text'] .='<table class="table_zh" style="background-color:#FFF;"><thead><tr><td>Factura</td><td>Valor Pendiente</td><td>Vencimiento</td><td>Dias</td><td>Valor Factura</td></tr></thead><tbody>';
				while($L=$qL->fetch_assoc()){
					$dias=($hoy-strtotime($L['dueDate']))/86400;
					$De['text'] .= '<tr><td>'.$L['docNum'].'</td><td>$'.number_format($L['balDue'], 0, '.', ',').'</td><td>'.($L['dueDate']).'</td><td>'.$dias.'</td><td>$'.number_format($L['docTotal'], 0, '.', ',').'</td></tr>';
				}
				$De['text'] .='</tbody></table>';
			}
			die(_js::enc2($De));
		}
	}
	if($Cnf['mdlLoad']=='Y' || $P['mdlLoad']=='Y'){//en __1_v1
		$q2=a_sql::fetch('SELECT A.*,S.svr,S.storBucket,S.fileMaxSize,S.firebase,
		CF.jsVersion
		FROM aconf CF
		JOIN loadMdl A ON (A.ocardId=\''.$q['ocardId'].'\')
		JOIN mdl_stor S ON (S.ocardId=A.ocardId)
		WHERE A.ocardId=\''.$q['ocardId'].'\' LIMIT 1');
		if(a_sql::$errNo==1){ die(_js::e(3,'Error obteniendo configuración de login()')); }
		if(a_sql::$errNo==2){ die(_js::e(3,'No existe configuración de login()')); }
		c::$useHashKey=$q2['permsxUser'];
		/* en local debo definir en archivo sociedad, ocl/fc70.js */
		c::$ys['storage']='\''.$q2['svr'].'\'';
		c::$ys['storBucket']='\''.$q2['storBucket'].'\'';
		c::$ys['jsVersion']='\''.$q2['jsVersion'].'\'';
		if($q2['svr']=='FB'){
			c::$ys['JFB']=$q2['firebase'];
		}
		else{ c::$ys['JFB']='{}'; }
		a_ses::$ocardId=$q['ocardId'];
		_Mdl::jsFileTxt('/* a_mdl::login */
$0s.ocardId=\''.$q['ocardId'].'\';
$0s.ocardcode=\''.$ocardcode.'\';
$0s.ocardName=\''.$q['ocardName'].'\';
$0s.storBucket=\''.$q['storBucket'].'\';
$0s.fileMaxSize=\''.$q['fileMaxSize'].'\';
/* end a_mdl::login */
$MdlK='.$q2['mdlCnf']).';';
		c::$V['jsLibType']=$q2['jsLibType'];
		c::$V['jsLoad']=json_decode($q2['jsLoad'],1);;
		if($q2['jsLibType']=='D'){ }
		else{
			_Mdl::A($q2['mdlA'],json_decode($q2['mdlJsv'],1));
			$js1=json_decode($q2['mdlJsFile'],1);
			if(is_array($js1)){ _Mdl::pushJSFile($js1); }
			$js1=json_decode($q2['mdlJsFileLast'],1);
			if(is_array($js1)){ _Mdl::pushJSFile($js1,'last'); }
		}
		c::$V['STOR_URI']=$q['storBucket'];
		_Mdl::$C['fileFromCache']='N'; //$q2['fileFromCache'];
	}
	unset($q['mailConf']);
	return $q;
}
}
?>