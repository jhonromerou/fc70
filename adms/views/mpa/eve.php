<?php
JRoute::get('mpa/eve',function($D,$P,$M){
	return $M->get($_GET,array('prsLeft'=>'Y'));
},['hashPerms'=>'mpaEve','lib'=>'sql/filter']);
JRoute::get('mpa/eve/{gid}','getOne',['hashPerms'=>'mpaEve','_wh'=>['gid'=>'\d+']]);
JRoute::get('mpa/eve/form','getOne',['hashPerms'=>'mpaEve']);

JRoute::post('mpa/eve','post',['hashPerms'=>'mpaEve.write','lib'=>'_2d']);
JRoute::put('mpa/eve','put',['hashPerms'=>'mpaEve.write','lib'=>'_2d']);
?>
