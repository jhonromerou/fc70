<?php
JRoute::get('mpa/tas',function($D,$P,$M){
	_ADMS::_lb('sql/filter'); a_sql::$limitDefBef=30;
	return $M->get($_GET,array('prsLeft'=>'Y'));
	//'Join'=>array('AP.commets','LEFT JOIN app_ottc AP ON (AP.tt=\'crmNov\' AND AP.tr=A.gid) ')
},['hashPerms'=>'mpaTas']);
JRoute::get('mpa/tas/{gid}','getOne',['hashPerms'=>'mpaTas','_wh'=>['gid'=>'\d+']]);
JRoute::get('mpa/tas/form','getOne',['hashPerms'=>'mpaTas.write']);

JRoute::post('mpa/tas','post',['hashPerms'=>'mpaTas.write']);
JRoute::put('mpa/tas','put',['hashPerms'=>'mpaTas.write']);

JRoute::put('mpa/tas/markCompleted','markComp',['hashPerms'=>'mpaTas.write']);
JRoute::post('mpa/tas/sendCopy','sendCopy',['hashPerms'=>'mpaTas']);
?>
