<?php
require('__0_requi.php');
require('__0_aorigin.php');
header('Content-Type: application/json');
require 'phpbase2.php';
c::setURI([
	'i'=>[1],
	'app'=>[2],
	'ctr'=>[3],
	'view'=>['>',1],
]);
_ADMS::lib('_err,_js,a_sql,a_ses,JApp,JRoute,_jwt');
JRoute::reqHeads('_c');
JRoute::$Appi=true;
JRoute::$path=c::$URI['view'];
$canCont = false;//true si tooken o cookie
if(c::$URI['i']=='open'){
	JRoute::render('open');
}
else if(c::$URI['i']=='public'){ /* require ocardcode */
	a_sql::dbase(c::$Sql2,'__appi -> public');
	$q=a_sql::fetch('SELECT * from sel_ocrd WHERE ocardCode=\''.c::$H['ocardcode'].'\' LIMIT 1');
	a_ses::$ocardId=$q['ocardId'];
	JRoute::render('public');
}
else if(c::$URI['i']=='private'){//private, requeri user
	$jwt=a_ses::ocardcode(false,['D'=>'Y']);
	if(_err::$err){ die(_err::$errText); }
	if(c::$V['SVR']!='L'){
		_ADMS::lib('a_mdl');
		//die('--> '.print_r($ocardcode,1));
		c::$Sql=a_mdl::login($jwt['ocardcode']);
	}
	a_sql::dbase(c::$Sql,'__appi--> private');
	a_ses::U_data($jwt);
	JRoute::render('private');
}
?>
