<?php
class JCPro{
	static $P=array();
	static public function vDoc($D,$canEdit=false){
		if(_js::iseErr($D['docDate'],'Se debe fecha inicial del proyecto')){}
		else if(_js::iseErr($D['proCode'],'Se debe definir un código al proyecto')){}
		else if($js=_js::textMax($D['proCode'],10,'Código')){ _err::err($js); }
		else if(_js::iseErr($D['proName'],'Se debe definir nombre del proyecto')){}
		else if($js=_js::textMax($D['proName'],100,'Nombre')){ _err::err($js); }
		if(_js::iseErr($D['cardId'],'Se debe definir el tercero')){}
		else if($js=_js::textMax($D['docMemo'],500,'Detalles')){ _err::err($js); }
		else{}
	}
	static public function get($D){
		$D['from']='A.*,C.cardName FROM par_opro A
		JOIN par_ocrd C ON (C.cardId=A.cardId)';
		return a_sql::rPaging($D);
	}
	static public function getSea($D){
		$sea=a_sql::toSe(c::$T['textSearch']);
		return a_sql::fetchL('SELECT C.cardId,A.proId,A.proCode,A.proName lineText FROM par_opro A
		JOIN par_ocrd C ON (C.cardId=A.cardId)
		WHERE (A.proCode '.$sea.' OR A.proName '.$sea.' OR C.cardName '.$sea.') LIMIT 6',
		[1=>'Error buscando proyectos',2=>'No se encontraron resultados','k'=>'L'],true);
	}
	static public function getOne($D,$ar=false){
		$js=false;
		$M=a_sql::fetch('SELECT A.*,C.cardName FROM par_opro A
		JOIN par_ocrd C ON (C.cardId=A.cardId)
		WHERE A.proId=\''.$D['proId'].'\' LIMIT 1',[1=>'Error obteniendo proyecto',2=>'El proyecto no existe']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			$js=($a)? $M : _js::enc2($M);
		}
		if(_err::$err){ return _err::$errText; }
		return $js;
	}
	static public function post($D){
		$js=false;
		self::vDoc($D);
		if(!_err::$err){// post data
			$D['proId']=a_sql::qInsert($D,['tbk'=>'par_opro']);
			if(a_sql::$err){ _err::err(a_sql::$errText,3); }
			else{
				$js=_js::r('Proyecto creado correctamente',$D); 
			}
		}
		if(_err::$err){return _err::$errText; }
		return $js;
	}
	static public function put($D,$P=array()){
		$js=false;
		self::vDoc($D);
		if(!_err::$err){// post data
			a_sql::qUpdate($D,['tbk'=>'par_opro','wh_change'=>'proId=\''.$D['proId'].'\' LIMIT 1']);
			if(a_sql::$err){ _err::err(a_sql::$errText,3); }
			else{
				$js=_js::r('Formulario actualizado correctamente',$D); 
			}
		}
		if(_err::$err){return _err::$errText; }
		return $js;
	}
}
?>
