
Api.JCPro={b:'/appi/private/jcpro/'};
$V.projTypes=[{k:'G',v:'General'},{k:'O',v:'Otros'}];

_Fi['JCPro']=function(wrap,x){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:'Código',I:{tag:'input',type:'text','class':jsV,name:'A.proCode'}},wrap);
	$1.T.divL({wxn:'wrapx4', L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'A.proName(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx4', L:'Tercero',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ JCPro.get(); }},wrap);
};

$M.kauAssg('crd',[
	{k:'crdProy',t:'Proyectos'}
]);
$M.liAdd('JCPro',[
{_lineText:'Proyectos de Terceros'},
{k:'JCPro',t:'Proyectos', kau:'crdProy', func:function(){
	$M.Ht.ini({btnGo:'JCPro.form',f:'crdProy',gyp:function(){ JCPro.get(); }});
}},
{k:'JCPro.form',t:'Proyecto (Form)', kau:'JCPro', func:function(){
	$M.Ht.ini({g:function(){ JCPro.form(); }});
}},
]);

var JCPro={
	Lg:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ $M.to('JCPro.form','proId:'+T.P.proId); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.JCPro.b+'a',inputs:$1.G.filter(),loade:cont,errWrap:cont,func:function(Jr){
			var tb=$1.T.table(['','Proyecto','Tercero','Inicio','Detalles'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				JCPro.Lg(L,td);
				$1.t('td',{textNode:L.proName},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:L.docMemo},tr);
			}
		}});
	},
	form:function(){
		var Pa=$M.read();
		var api1=Api.JCPro.b+'a';
		var jsF=$Api.JS.cls;
		var wrap=$M.Ht.cont;
		$Api.get({f:api1+'/form',loadVerif:!Pa.proId,inputs:'proId='+Pa.proId,loade:wrap,func:function(Jr){
			if(Pa.proId){ $Api.JS.addF({name:'proId',value:Pa.proId},wrap); }
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Codigo',req:'Y',I:{tag:'input',type:'text',name:'proCode','class':jsF,value:Jr.proCode}},wrap);
			$1.T.divL({wxn:'wrapx2',L:'Nombre Proyecto',req:'Y',I:{tag:'input',type:'text',name:'proName','class':jsF,value:Jr.proName}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Estado',req:'Y',I:{tag:'select',name:'docStatus','class':jsF,opts:$V.docStatusOCN,selected:Jr.docStatys,noBlank:'Y'}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Inicio',req:'Y',I:{tag:'input',type:'date',name:'docDate','class':jsF,value:Jr.docDate}},divL);
			var sea=$crd.Sea.get({cardType:'C'},wrap,Jr);
			$1.T.divL({divLine:1,wxn:'wrapx2',L:'Tercero',req:'Y',Inode:sea},wrap);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea','class':jsF,name:'docMemo',value:Jr.docMemo}},wrap);
			var resp=$1.t('div',0,wrap);
			var Ps={POST:api1,jsBody:wrap,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
			}};
			if(Pa.proId){ Ps.PUT=Ps.POST; delete(Ps.POST); }
			$Api.send(Ps,wrap);
		}});
	}
}

JCPro.Sea={
	get:function(P,cont,D){
		if(!P.vSea){ P.vSea=''; }
		if(!P.AJsPut){ P.AJsPut=[]; }
		P.AJsPut.push('proId');
		P.tag='apiSeaBox'; P.fieDefAt=cont;
		P.value=(D)?D.proName:'';
		P.api=Api.JCPro.b+'a/sea';
		P.D=D;
		P['class']=(P.jsF)?P.jsF:$Api.JS.cls;;
		return $1.lTag(P,cont);
	}
}