vApp.Sign={
	cardLogin:function(){
		var cnt=document.querySelector('.register');
		var jsF=$Api.JS.cls;
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Correo Electrónico',I:{tag:'input',type:'text',name:'email','class':jsF,placeholder:'tucorreo@gmail.com'}},cnt);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Contraseña',I:{tag:'input',type:'password',name:'pass','class':jsF}},cnt);
		var resp=$1.t('div',0,cnt);
		$Api.send({POST:Api.vApp.o+'sign/cardLogin',jsBody:cnt,loade:resp,textNode:'Iniciar Sesión',func:function(Jr,o){
			$Api.resp(resp,Jr);
			if(!Jr.errNo){
				if(o && o.SD){ $y.sesion(o.SD); }
				location.href = './';
			}
		}},cnt);
		vApp.Sign.cardRestore(cnt);
		var p=$1.t('p',{style:'textAlign:center'},cnt);
		$1.T.btnFa({fa:'ui_button fa-user',textNode:'Crear una cuenta',func:function(){ location.href='registrovendedor.php'}},p);
	},
	cardRegister:function(){
		var cnt=document.querySelector('.register');
		var jsF=$Api.JS.cls;
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Nombre de tu Negocio',I:{tag:'input',type:'text','class':jsF+' _ocardName',name:'ocardName',placeholder:'Calzado Fantástico'}},cnt);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:{textNode:'Usuario Web',_iHelp:'Con este usuario la gente podria ingresar a tu página mediente http://unclicc.com/minegocio'},I:{tag:'input','class':jsF+' _nickname',type:'text',name:'ocardCode',placeholder:'calzadofantastico',maxlength:20}},cnt);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Telefono',I:{tag:'input',type:'text','class':jsF,name:'phone1',placeholder:'+57 323 338 93 35'}},cnt);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Correo Electrónico',I:{tag:'input',type:'text',name:'email','class':jsF,placeholder:'tucorreo@gmail.com'}},cnt);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Confirmar Correo Electrónico',I:{tag:'input',type:'text',name:'emailr','class':jsF}},cnt);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Contraseña',I:{tag:'input',type:'password',name:'pass','class':jsF}},cnt);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Repetir Contraseña',I:{tag:'input',type:'password',name:'passr','class':jsF}},cnt);
		var resp=$1.t('div',0,cnt);
		$Api.send({POST:Api.vApp.o+'sign/cardRegister',jsBody:cnt,loade:resp,textNode:'Registrarme!',func:function(Jr){
			$Api.resp(resp,Jr);
		}},cnt);
		var tg1=$1.q('._ocardName'); var tg2=$1.q('._nickname');
		tg1.onkeyup=function(){
			tg2.value=tg1.value.replace(/[^\w]+/,'').toLowerCase();
		}
	},
	cardRestore:function(cnt){
		var jsF=$Api.JS.cls;
		var a=$1.t('a',{href:'javascript://',textNode:'¿No recuerdas tu contraseña?',style:'display:block; marginBottom:5px;'}, $1.t('p',{style:'textAlign:center'},cnt));
		a.onclick=function(){
			cnt.innerHTML='';
			$1.t('h5',{textNode:'Digita el correo o usuario de tu cuenta, te enviaremos un correo electrónico con tus datos de acceso.'},cnt); divL=$1.T.divL({divLine:1,wxn:'wrapx1',I:{tag:'input',type:'text',name:'datoRecu','class':jsF,placeholder:'tucorreo@gmail.com'}},cnt);
			var resp=$1.t('div',0,cnt);
			$Api.send({POST:Api.vApp.o+'sign/cardRestore',jsBody:cnt,loade:resp,textNode:'Recuperar Datos Inicio',func:function(Jr,o){
				if(!Jr.errNo){ $Api.resp(cnt,Jr); }
				else{ $Api.resp(resp,Jr); }
			}},cnt);
	}
	}
}
