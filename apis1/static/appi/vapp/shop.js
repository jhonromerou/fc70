
$M.liAdd('wma',[
{_lineText:'--'},
{k:'category',t:'Encuentra lo que necesitas',func:function(){
	$M.Ht.ini({gyp:function(){ vApp.Home.category(); }});
}},
{k:'shops',noTitle:'Y',t:' ',func:function(){
	$M.Ht.ini({gyp:function(){ vApp.Home.shops(); }});
}},
{k:'vApp.itm',t:'Productos',func:function(){
	$M.Ht.ini({gyp:function(){ vApp.Itm.get(); }});
}},
]);


_Fi['vApp.itm']=function(wrap){
	var jsV = 'jsFiltVars';
	var Pa=$M.read();
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:'Código',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'101,400',value:Pa.itemCode}},wrap);
	$1.T.divL({wxn:'wrapx6', L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	$1.T.divL({wxn:'wrapx6', L:'Req. Act.',I:{tag:'select','class':jsV,name:'M.updSubEsc',opts:$V.YN}},divL);
	$1.T.divL({wxn:'wrapx6', L:'Costo <=',I:{tag:'input',type:'number',min:0,'class':jsV,name:'M.cost(E_menIgual)'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:Wma.Mpg.get},wrap);
};

vApp.Home={
	category:function(){
		var cont=$M.Ht.cont;
		$1.t('h4',{textNode:'Escoge un sector rapidamente..'},cont);
		for(var i in $V.vAppCategory){ var L=$V.vAppCategory[i];
			var divt=$1.t('div',{'class':'vAppShop_secRow'},cont);
			var div=$1.t('a',{href:$M.to('shops','category:'+L.k,'r')},divt);
			$1.t('b',{textNode:L.v,'class':'bTitle'},div);
			$1.t('img',{src:'/imgs/category-'+L.k+'.jpg','class':'bimg'},div);
		}
	},
	shops:function(){
		var cont=$M.Ht.cont;
		var vPost=$M.read('pars');
		$1.t('h4',{textNode:'Escoge un sector rapidamente..'},cont);
		$Api.get({f:Api.vApp.pu+'shops',inputs:vPost, loade:cont, func:function(Jr){
			for(var i in Jr.L){ L=Jr.L[i];
				var divt=$1.t('div',{'class':'vAppSeller_row'},cont);
				var div=$1.t('div',{'class':'blogo'},divt);
				$1.t('img',{src:L.logoSrc},div);
				var div=$1.t('div',{'class':'binfo'},divt);
				$1.t('div',{textNode:L.ocardName,'class':'cardName'},div);
				$1.t('div',{textNode:_g(L.cityCode,Addr.City),'class':'city'},div);
				$1.t('div',{textNode:L.sellDescription,'class':'cardInfo'},div);
				var div=$1.t('div',{'class':''},divt);
				$1.t('a',{textNode:'Ver Tienda',href:'./'+L.ocardCode+'#vApp.itm!!'},div);
			}
		}});
	},
}
vApp.Itm={
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.vApp.pu+'itm',inputs:$1.G.filter(),loade:cont, errWrap:cont, func:function(Jr){
			for(var i in Jr.L){ var L=Jr.L[i];
				var dv=$1.t('hdiv',{'class':'vAppItm_row'},cont);
				var h5=$1.t('h5',0,dv);
				$1.t('span',{textNode:L.itemName},h5);
				var priceTxt='';
				if(L.sellPriceView=='D'){ priceTxt=$Str.money(L.sellPrice); }
				else if(L.sellPriceView=='H'){ priceTxt='Consultar Precio'; }
				else if(L.sellPriceView=='F'){ priceTxt='Desde '+$Str.money(L.sellPrice); }
				$1.t('b',{'class':'iPrice',textNode:priceTxt},dv);
				$1.t('img',{'class':'iImg',src:L.mainSrc,style:'maxWidth:16rem; maxHeight:32rem;'},dv);
			}
		}});
	},
}
