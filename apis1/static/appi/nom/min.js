$VsU.push({k:'$JsV.nomLeaveRea',v:'Motivos Retiro',optGroup:'Nomina'});

Api.Nom={b:'/appi/private/nom/',pr:'/appi/private/nom/',pu:'/appi/public/nom/',o:''};

$M.kauAssg('nom',[
	{k:'nomCrd',t:'Empleados'},
	{k:'nomCpr',t:'Personas Contacto (Empleados)'},
	{k:'nomCfr',t:'Datos Complementarios Empleado'},
	{k:'nomCla',t:'Contratos (Altas/Bajas)'},
	//novedades
	{k:'nomNov.turnOpen',t:'Entrada y Salida Planta'},{k:'nomNov.turn',t:'Registro de Entrada/Salida Planta'},{k:'nomNov.reg',t:'Registro de Novedades'},{k:'nomNovRep',t:'Reportes de Novedades'},
	//
	{k:'nomAil',t:'Accidentes/Incidentes'}
]);

_Fi['nomNov.her']=function(wrap,x){
	var Pa=$M.read('!');
	var tDa=$2d.rang({rang:'days--',n:1});
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Fecha',I:{lTag:'date','class':jsV,name:'N.lineDate(E_mayIgual)',value:tDa.date2}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Fin',I:{lTag:'date','class':jsV,name:'N.lineDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx4', L:'Empleado',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:Nom.Nov.Her.get},wrap);
};
_Fi['nomNov.lic']=function(wrap,x){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8',L:'Ver por',I:{tag:'select','class':jsV+' nomNovVType',opts:$V.NomNovVtype,name:'viewType',noBlank:'Y'}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha',I:{tag:'input',type:'date','class':jsV,name:'date1'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Fin',I:{tag:'input',type:'date','class':jsV,name:'date2'}},divL);
	$1.T.divL({wxn:'wrapx4', L:'Empleado',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},divL);
	func=Nom.Nov.Lic.get;
	if(Pa.match(/\.inc/)){ func=Nom.Nov.Inc.get; }
	else if(Pa.match(/\.sus/)){ func=Nom.Nov.Sus.get; }
	else if(Pa.match(/\.vac/)){ func=Nom.Nov.Vac.get; }
	$1.T.btnSend({textNode:'Actualizar', func:func},wrap);
};
_Fi['nomNov.inc']=function(wrap){
	return _Fi['nomNov.lic'](wrap,{});
}
_Fi['nomNov.vac']=function(wrap){
	return _Fi['nomNov.lic'](wrap,{});
}
_Fi['nomNov.sus']=function(wrap){
	return _Fi['nomNov.lic'](wrap,{});
}
_Fi['nomCrd']=function(wrap,x){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4', L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'C.name(E_like3)'}},wrap);
	$1.T.divL({wxn:'wrapx4', L:'Apellido',I:{tag:'input',type:'text','class':jsV,name:'C.surName(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx4', L:'No. Documento',I:{tag:'input',type:'text','class':jsV,name:'CC.licTradNum(E_like3)'}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Sede',I:{tag:'select','class':jsV,name:'C.workSede',opts:$Tb.owsu}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Area',I:{tag:'select','class':jsV,name:'C.workArea',opts:$JsV.nomWorkArea}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Cargo',I:{tag:'select','class':jsV,name:'C.workPosi',opts:$JsV.nomWorkPosi}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ Nom.Crd.get(); }},wrap);
};
_Fi['nomCpr']=function(wrap,x){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Parentesco',I:{tag:'select','class':jsV,name:'P.positionId',opts:$JsV.nomCprRel}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Genero',I:{tag:'select','class':jsV,name:'P.gender',opts:$V.gender}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Mayor a',I:{lTag:'number','class':jsV,name:'ageMay'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Menor a',I:{lTag:'number','class':jsV,name:'ageMen'}},divL);
	$1.T.divL({wxn:'wrapx4', L:'Nombre Persona',I:{tag:'input',type:'text','class':jsV,name:'P.name(E_like3)'}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4', L:'Empleado',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:'Reporte',I:{tag:'select','class':jsV,name:'__dbReportLen',opts:$V.dbReportLen,noBlank:1}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ Nom.Cpr.get(); }},wrap);
};
_Fi['nomNov.turn']=function(wrap,x){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Estado',I:{tag:'select','class':jsV,name:'N2.lineStatus',opts:$V.docStatusOCN}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha',I:{lTag:'date','class':jsV,name:'N2.openDate(E_mayIgual)',value:$2d.yesterday}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Fin',I:{lTag:'date','class':jsV,name:'N2.openDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Jornada',I:{tag:'select','class':jsV,name:'N2.dominical',opts:$V.nomNovFestivo}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Extra',I:{tag:'select','class':jsV,name:'N2.hextra',opts:$V.NY}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Origen',I:{tag:'select','class':jsV,name:'N2.tt',opts:$V.nomBioReg}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Salida Pendiente',I:{tag:'select','class':jsV,name:'salidaPend',opts:[{k:'Y',v:'Si'}]}},divL);
	$1.T.divL({divLine:1,wxn:'wrapx4', L:'Empleado',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},wrap);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ Nom.Nov.Turn.get(); }},wrap);
};
_Fi['nomNovRep.turn']=function(wrap,x){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4', L:'Empleado',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha',I:{tag:'input',type:'date','class':jsV,name:'N3.openDate(E_mayIgual)',value:$2d.today}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Fin',I:{tag:'input',type:'date','class':jsV,name:'N3.openDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Estado',I:{tag:'select','class':jsV,name:'N3.lineStatus',opts:$V.docStatusOC}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Sede',I:{tag:'select','class':jsV,name:'C.workSede',opts:$Tb.owsu}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Area',I:{tag:'select','class':jsV,name:'C.workArea',opts:$JsV.nomWorkArea}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Cargo',I:{tag:'select','class':jsV,name:'C.workPosi',opts:$JsV.nomworkPosi}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ Nom.Nov.Rep.turn(); }},wrap);
};
_Fi['nomNovRep.nov']=function(wrap,x){
	var Pa=$M.read('!');
	var tDa=$2d.rang({rang:'days--',n:7});
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:'Tipo',I:{tag:'select','class':jsV,name:'N2.lineType',opts:$V.nomNovType}},wrap);
	$1.T.divL({wxn:'wrapx8', L:'Clase',I:{tag:'select','class':jsV,name:'N2.lineType2',opts:$V.nomNovClass}},divL);
	$1.T.divL({wxn:'wrapx4', L:'Empleado',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha',I:{tag:'input',type:'date','class':jsV,name:'date1',value:tDa.date2}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Fin',I:{tag:'input',type:'date','class':jsV,name:'date2',value:tDa.date1}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Estado',I:{tag:'select','class':jsV,name:'N2.lineStatus',opts:$V.docStatusOC}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Sede',I:{tag:'select','class':jsV,name:'C.workSede',opts:$Tb.owsu}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Area',I:{tag:'select','class':jsV,name:'C.workArea',opts:$JsV.nomWorkArea}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Cargo',I:{tag:'select','class':jsV,name:'C.workPosi',opts:$JsV.nomworkPosi}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ Nom.Nov.Rep.nov(); }},wrap);
};
_Fi['nomNovRep.novHer']=function(wrap,x){
	var Pa=$M.read('!');
	var tDa=$2d.rang({rang:'days--',n:7});
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4', L:'Empleado',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha',I:{tag:'input',type:'date','class':jsV,name:'date1',value:tDa.date2}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Fin',I:{tag:'input',type:'date','class':jsV,name:'date2',value:tDa.date1}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Sede',I:{tag:'select','class':jsV,name:'C.workSede',opts:$Tb.owsu}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Area',I:{tag:'select','class':jsV,name:'C.workArea',opts:$JsV.nomWorkArea}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Cargo',I:{tag:'select','class':jsV,name:'C.workPosi',opts:$JsV.nomworkPosi}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ Nom.Nov.Rep.novHer(); }},wrap);
};

_Fi['nomCla']=function(wrap,x){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Estado',I:{tag:'select','class':jsV,name:'A.lineStatus',opts:$V.docStatusOC,noBlank:1}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha',subText:'Ingreso',I:{tag:'input',type:'date','class':jsV,name:'A.closeDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Fin',I:{tag:'input',type:'date','class':jsV,name:'A.closeDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx4', L:'Empleado',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Retiro Voluntario',I:{tag:'select','class':jsV,name:'A.leaveVol',opts:$V.YN}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Motivo',I:{tag:'select','class':jsV,name:'A.leaveReason',opts:$JsV.nomLeaveRea}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Encuesta',I:{tag:'select','class':jsV,name:'A.leaveForm',opts:$V.nomLeaForm}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha',subText:'Retiro',I:{tag:'input',type:'date','class':jsV,name:'A.leaveDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Fin',I:{tag:'input',type:'date','class':jsV,name:'A.leaveDate(E_menIgual)'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ Nom.Cla.get(); }},wrap);
};
_Fi['nomAil']=function(wrap,x){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Estado',I:{tag:'select','class':jsV,name:'A.docStatus',opts:$V.docStatusOCN,noBlank:1}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha',I:{lTag:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Fin',I:{lTag:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx4', L:'Empleado',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Tipo',I:{tag:'select','class':jsV,name:'A.docType',opts:$V.nomAilType}},wrap);
		$1.T.divL({wxn:'wrapx8',L:'Clase',I:{tag:'select','class':jsV,name:'A.docClass',opts:$JsV.nomAilClass}},divL);
	$1.T.divL({wxn:'wrapx2',L:'Detalles',I:{tag:'textarea','class':jsV,name:'A.lineMemo(E_like3)'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ Nom.Ail.get(); }},wrap);
};

_Fi['JForm.p.nomCfr']=function(wrap,x){
	var Pa=$M.read('!');
	var opts1=[{k:'Y',v:'Pendiente'}];
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:'Complementario',I:{lTag:'select','class':jsV,name:'F.docDate(E_IsN)',opts:opts1}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Inicio',I:{lTag:'date','class':jsV,name:'F.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Corte',I:{lTag:'date','class':jsV,name:'F.docDate(E_mebIgual)'}},divL);
	$1.T.divL({wxn:'wrapx4', L:'Empleado',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:'Técnico',I:{lTag:'select','class':jsV,name:'F2.docDate(E_IsN)',opts:opts1}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Inicio',I:{lTag:'date','class':jsV,name:'F2.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Corte',I:{lTag:'date','class':jsV,name:'F2.docDate(E_mebIgual)'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ Nom.Cfr.get(); }},wrap);
};


var Nom={};
$V.nomInfoStatus=[{k:'P',v:'Pendiente'},{k:'C',v:'Completa'},{k:'A',v:'Actualizar'}];
$V.NomNovVtype=[{k:'1',v:'Periodo'},{k:'L',v:'Registros'}];
$V.nomNovType=[
{k:'HER',v:'Horas/Recargos'}, {k:'VAC',v:'Vacaciones'},
{k:'INC',v:'Incapacidades'},{k:'LIC',v:'Licencias'},{k:'SUS',v:'Suspenciones'}
];
$V.nomNovClass=[
{k:'RN',v:'Recargo Nocturno',f:1.35,type:'H'},{k:'FL',v:'Festivo Laborado',f:1.75,type:'H'},{k:'FLN',v:'Festivo Laborado Nocturno',f:2.1,type:'H'},
{k:'ED',v:'Extra Diurna',f:1.25,type:'H'},{k:'EN',v:'Extra Nocturna',f:1.75,type:'H'},
{k:'EFD',v:'Extra Festiva',f:2,type:'H'},{k:'EFN',v:'Extra Festiva Noctura',f:2.5,type:'H'},
//Vacaciones
{k:'D',v:'Disfrutadas',type:'V'},{k:'C',v:'Compensadas',type:'V'},
//Incapacidad
{k:'G',v:'General',type:'I'},{k:'L',v:'Laboral',type:'I'},
//Licencias
{k:'M',v:'Maternidad',type:'L'},{k:'P',v:'Paternidad',type:'L'},
{k:'R',v:'Remunerada',type:'L'},{k:'N',v:'No Remunerada',type:'L'},
{k:'S',v:'Suspención',type:'L'},{k:'A',v:'Abandono',type:'L'},
//Suspenciones
{k:'S1',v:'Otros',type:'S'},
];
$V.nomLeaForm=[{k:'NA',v:'No Aplica'},{k:'N',v:'Pendiente'},{k:'Y',v:'Realizada'}];
$V.NomReinT=[{k:'N',v:'No'},{k:'PO',v:'Posible'}];
$V.nomTSang=[{k:'A+',v:'A+'},{k:'O+',v:'O+'},{k:'B+',v:'B+'},{k:'AB+',v:'AB+'},{k:'A-',v:'A-'},{k:'O-',v:'O-'},{k:'B-',v:'B-'},{k:'AB-',v:'AB-'}];
$V.nomTurnHini='07:00';
$V.nomTurnHend='14:00';
$V.nomNovFestivo=[{k:'N',v:'No Festivo'},{k:'FH',v:'Festivo (a Habil)'},{k:'HF',v:'Habil a Festivo'},{k:'FF',v:'Festivo a Festivo'}];
$V.nomBioReg=[{k:'B',v:'Sistema'},{k:'M',v:'Manual'}];
$V.nomAilType=[{k:1,v:'Accidente'},{k:2,v:'Incidente'}];
$V.nomAilLType=[{k:1,v:'Correctiva'},{k:2,v:'Mejora'}];

$M.liAdd('nom',[
{_lineText:'Nomina - Novedades'},
{k:'nomCrd',t:'Empleados', kau:'nomCrd', func:function(){
	$M.Ht.ini({btnGo:'nomCrd.form',f:'nomCrd',gyp:function(){ Nom.Crd.get(); }});
}},
{k:'nomCrd.form',t:'Formulario Empleado (Base)', kau:'nomCrd', func:function(){
	$M.Ht.ini({g:function(){ Nom.Crd.form(); }});
}},
{k:'nomCpr',t:'Nucleo Familiar',d:'Personas relacionadas a los empleados', kau:'nomCpr', ico:'fa fa-users', func:function(){
	var btn = $1.T.btnFa({faBtn:'fa_doc',textNode:'Nueva', func:function(){ $M.to('nomCpr.form'); }});
	$M.Ht.ini({fieldset:'Y',btnNew:btn, f:'nomCpr', gyp:Nom.Cpr.get });
}},
{k:'nomCpr.form',t:'Formulario de Contacto', kau:'nomCpr', func:function(){ $M.Ht.ini({g:function(){ Nom.Cpr.form(); }}); }},

{k:'nomCla',t:'Contratos',d:'Contratos de Empleados', kau:'nomCla', func:function(){
	bn=$1.T.btnFa({faBtn:'fa-file',textNode:'Nuevo Proceso',func:function(){ Nom.Cla.form(); }});
	$M.Ht.ini({btnNew:bn,f:'nomCla',gyp:function(){ Nom.Cla.get(); }});
}},
{k:'JForm.ans.nomCla.leaveAns',t:'Encuesta de Retiro', kau:'nomCla', func:function(){
	$M.Ht.ini({g:function(){ Nom.Cla.leaveAns() }});
}},

{k:'JForm.p.nomCfr',t:'Datos Complementarios',d:'Información complementaria del empleado', kau:'nomCfr', func:function(){
	$M.Ht.ini({f:'JForm.p.nomCfr',gyp:function(){ Nom.Cfr.get(); }});
}},
{k:'JForm.ans.nomCfr.comp',t:'Datos Complementarios', kau:'nomCfr', func:function(){
	$M.Ht.ini({g:function(){ Nom.Cfr.ans() }});
}},
{k:'JForm.ans.nomCfr.tecni',t:'Datos Técnicos', kau:'nomCfr', func:function(){
	$M.Ht.ini({g:function(){ Nom.Cfr.ans() }});
}},
{k:'JForm.forms.nomCfr',t:'Complementarios',d:'Formularios complementarios información Empleado', kau:'JForm.forms', func:function(){
	$M.Ht.ini({g:function(){ Nom.Cfr.forms() }});
}},
{k:'JForm.form.nomCfr',t:'Complementario (form)', kau:'JForm.forms', func:function(){
	$M.Ht.ini({g:function(){ Nom.Cfr.form() }});
}},
{k:'JForm.rep.nomCfr',t:'Reporte Complementario', kau:'nomCfr', func:function(){
	$M.Ht.ini({g:function(){ Nom.Cfr.rep() }});
}},

{k:'nomAil',t:'Accidentes/Incidentes',d:'Seguimiento de accidentes e incidentes', kau:'nomAil', func:function(){
	$M.Ht.ini({f:'nomAil',gyp:Nom.Ail.get,btnGo:'nomAil.form'});
}},
{k:'nomAil.form',t:'Accidentes/Incidentes (Form)',kau:'nomAil', func:function(){
	$M.Ht.ini({g:()=>{ Nom.Ail.form(); }});
}},
{k:'nomAil.view',noTitle:'Y',kau:'nomAil', func:function(){
	$M.Ht.ini({g:()=>{ Nom.Ail.view(); }});
}},

{k:'nomNov.turnOpen',t:'Entrada y Salida', kau:'nomNov.turnOpen', func:function(){
	$M.Ht.ini({g:function(){ Nom.Nov.Turn.ini(); }});
}},
{k:'nomNov.turn',t:'Registros', kau:'nomNov.turn', func:function(){
	$M.Ht.ini({f:'nomNov.turn',gyp:function(){ Nom.Nov.Turn.get(); },bNew:{faBtn:'fa fa-doc',textNode:'Nuevo',func:function(){ Nom.Nov.Turn.form(); } }});
}},
{k:'nomNov.her',t:'Horas y Recargos', kau:'nomNov.reg', func:function(){
	var div=$1.t('div');
	$1.T.btnFa({faBtn:'fa-doc',textNode:'Nuevo Registro',func:function(){ Nom.Nov.Her.form(); }},div);
	$M.Ht.ini({btnNew:div,f:'nomNov.her',gyp:function(){ Nom.Nov.Her.get(); }});
}},
{k:'nomNov.lic',t:'Licencias', kau:'nomNov.reg', func:function(){
	var div=$1.t('div');
	$1.T.btnFa({faBtn:'fa-doc',textNode:'Nuevo Registro',func:function(){ Nom.Nov.Lic.form(); }},div);
	$M.Ht.ini({btnNew:div,f:'nomNov.lic',gyp:function(){ Nom.Nov.Lic.get(); }});
}},
{k:'nomNov.vac',t:'Vacaciones', kau:'nomNov.reg', func:function(){
	var div=$1.t('div');
	$1.T.btnFa({faBtn:'fa-doc',textNode:'Nuevo Registro',func:function(){ Nom.Nov.Vac.form(); }},div);
	$M.Ht.ini({btnNew:div,f:'nomNov.vac',gyp:function(){ Nom.Nov.Vac.get(); }});
}},
{k:'nomNov.inc',t:'Incapacidades', kau:'nomNov.reg', func:function(){
	var div=$1.t('div');
	$1.T.btnFa({faBtn:'fa-doc',textNode:'Nuevo Registro',func:function(){ Nom.Nov.Inc.form(); }},div);
	$M.Ht.ini({btnNew:div,f:'nomNov.inc',gyp:function(){ Nom.Nov.Inc.get(); }});
}},
{k:'nomNov.sus',t:'Suspenciones', kau:'nomNov.reg', func:function(){
	var div=$1.t('div');
	$1.T.btnFa({faBtn:'fa-doc',textNode:'Nuevo Registro',func:function(){ Nom.Nov.Sus.form(); }},div);
	$M.Ht.ini({btnNew:div,f:'nomNov.sus',gyp:function(){ Nom.Nov.Sus.get(); }});
}},
/* rep */
{k:'nomNovRep.turn',t:'Reporte de Turnos', kau:'nomNovRep', func:function(){
	$M.Ht.ini({f:'nomNovRep.turn'});
}},
{k:'nomNovRep.nov',t:'Reporte de Novedades', kau:'nomNovRep', func:function(){
	$M.Ht.ini({f:'nomNovRep.nov'});
}},
{k:'nomNovRep.novHer',t:'Reporte Turnos/Recargos', kau:'nomNovRep', func:function(){
	$M.Ht.ini({f:'nomNovRep.novHer'});
}}
]);

Nom.Crd={
	Lg:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ $M.to('nomCrd.form','cardId:'+T.P.cardId); } },men);
			Li.push({ico:'fa fa-barcode',textNode:' Código de Barras', P:L, func:function(T){
				code=$1.barCode({code:T.P.bCode});
				$1.Win.open(code,{winSize:'small',winTitle:'Código de Barras Empleado',winId:'nomCrdBcode'});
			} });
			Li= $Opts.add('nomCrd',Li,L);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Nom.b+'crd',inputs:$1.G.filter(),loade:cont,errWrap:cont,func:function(Jr){
			var tb=$1.T.table(['','Nombre','Sucursal','Area','Cargo'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				Nom.Crd.Lg(L,td);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:_g(L.workSede,$Tb.owsu)},tr);
				$1.t('td',{textNode:_g(L.workArea,$JsV.nomWorkArea)},tr);
				$1.t('td',{textNode:_g(L.workPosi,$JsV.nomWorkPosi)},tr);
			}
		}});
	},
	form:function(Pa){
		var Pa=$M.read();
		var api1=Api.Nom.b+'crd';
		var jsF=$Api.JS.cls;
		var wrap=$M.Ht.cont;;
		$Api.get({f:api1+'/form',loadVerif:!Pa.cardId,inputs:'cardId='+Pa.cardId,loade:wrap,func:function(Jr){
			if(Pa.cardId){ $Api.JS.addF({name:'cardId',value:Pa.cardId},wrap); }
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Tipo Doc.',I:{tag:'select',opts:$V.licTradType,name:'licTradType','class':jsF,selected:Jr.licTradType}},wrap);
			$1.T.divL({wxn:'wrapx8',L:'No. Doc',I:{tag:'input',type:'text',name:'licTradNum','class':jsF,value:Jr.licTradNum}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Nombre/s',I:{tag:'input',type:'text',name:'name','class':jsF,value:Jr.name}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Apellido/s',I:{tag:'input',type:'text',name:'surName','class':jsF,value:Jr.surName}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Tipo Sang.',I:{tag:'select',opts:$V.nomTSang,name:'tSang','class':jsF,selected:Jr.tSang}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Información',I:{tag:'select',opts:$V.nomInfoStatus,name:'infoStatus','class':jsF,selected:Jr.infoStatus,noBlank:'Y'}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Sede',I:{tag:'select',opts:$Tb.owsu,name:'workSede','class':jsF,selected:Jr.workSede}},wrap);
			$1.T.divL({wxn:'wrapx8',L:'Area',I:{tag:'select',opts:$JsV.nomWorkArea,name:'workArea','class':jsF,selected:Jr.workArea}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Cargo',I:{tag:'select',opts:$JsV.nomWorkPosi,name:'workPosi','class':jsF,selected:Jr.workPosi}},divL);
			var sea=JCPro.Sea.get({},wrap,Jr);
			$1.T.divL({wxn:'wrapx2',L:'Proyecto',Inode:sea},divL);
			$1.T.divLTitle('Contacto',wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Tel. Movil',I:{tag:'input',type:'text',name:'cellular','class':jsF,value:Jr.cellular}},wrap);
			$1.T.divL({wxn:'wrapx8',L:'Tel. Fijo',I:{tag:'input',type:'text',name:'phone1','class':jsF,value:Jr.phone1}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Tel. 2',I:{tag:'input',type:'text',name:'phone2','class':jsF,value:Jr.phone2}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Correo',I:{tag:'input',type:'text',name:'email','class':jsF,value:Jr.email}},divL);
			$1.T.divLTitle('Dirección',wrap);
			Addr.type1(Jr,wrap,{jsFk:'Ad',tt:'card',tr:Jr.cardId});
			$1.aGo({P:{claId:Jr.claId},func:function(T){ Nom.Cla.form(T); }},null,
			$1.T.divLTitle('Contrato',wrap));
			if(Jr.claId==0){
				$1.t('div',{textNode:'Ningun contrato vigente'},wrap)
			}
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Salario',
			I:{lTag:'$',value:Jr.salarioBal,disabled:'disabled'}},wrap);
			var resp=$1.t('div',0,wrap);
			var Ps={PUT:api1,jsBody:wrap,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
			}};
			$Api.send(Ps,wrap);
		}});
	},
}

Nom.Crd.Bio={
	bCode:function(func,P){
		P=(P)?P:{};
		return $1.T.barCode({jsF:$Api.JS.cls,L:' ',placeholder:'Leer código de empleado',func:function(val){
			$Api.get({f:Api.Nom.pr+'crdBio/bCode',inputs:'bCode='+val,loade:P.loade,func:function(Da){
				if(Da.errNo && P.loade){ $Api.resp(P.loade,Da); }
				else if(Da.errNo){ $1.Win.message(Da); }
				else{ func(Da); }
			}});
		}},P.pare);
	}
}


Nom.Cpr={
opts:function(P,pare){
	var L=P.L;
	var Li=[];
	Li=$Opts.add('nomCpr',Li,L);
	$1.Menu.winLiRel({Li:Li,textNode:P.textNode},pare);
},
at:function(cont){
	var Pa=$M.read();
	var cont=(cont)?cont:$M.Ht.cont;
	var vPost=(Pa.cardId)?'tt=card&tr='+Pa.cardId:'';
	var jsF='jsFields'; var tBody=$1.t('tbody');
	$Api.get({f:Api.Crd.b+'cpr/at', inputs:vPost,loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{ $M.Ht.title.innerHTML =Jr.cardName+' - '+Jr.licTradNum; }
		var cs=8; var ni=1;
		var tb=$1.T.table(['#','Nombre','Parentesco','Celular','Teléfono 1','Email','Documento','Fecha Nacimiento']); cont.appendChild(tb);
		tb.appendChild(tBody);
		var n=1;
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:n},tr); n++;
			$1.t('td',{textNode:L.name},tr);
			$1.t('td',{textNode:_g(L.positionId,$JsV.nomCprRel)},tr);
			$1.t('td',{textNode:L.cellular},tr);
			$1.t('td',{textNode:L.tel1},tr);
			$1.t('td',{textNode:L.email},tr);
			$1.t('td',{textNode:_g(L.licTradType,$V.crdLicTradType)+' '+L.licTradNum},tr);
			$1.t('td',{textNode:L.birthDay},tr);
		}
	}});
},
get:function(){
	cont=$M.Ht.cont;
	$Api.get({f:Api.Nom.b+'cpr',inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); return false; }
		var tbl=['','Nombre','Relacion','Celular','Email','Fecha Nac.','Empleado'];
		var tb=$1.T.table(tbl);
		cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			var td=$1.t('td',0,tr);
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ $M.to('nomCpr.form','prsId:'+T.P.prsId); } },td);
			Nom.Cpr.opts({L:L},td);
			var td=$1.t('td',{textNode:L.name},tr);
			$1.t('td',{textNode:_g(L.positionId,$JsV.nomCprRel)},tr);
			$1.t('td',{textNode:L.cellular},tr);
			$1.t('td',{textNode:L.email},tr);
			$1.t('td',{textNode:L.birthDay},tr);
			$1.t('td',{textNode:L.cardName},tr);
		}
	}});
},
form:function(){
	var Pa=$M.read(); var cont=$M.Ht.cont;
	var jsF=$Api.JS.cls; var tBody=$1.t('tbody');
	$Api.get({f:Api.Nom.b+'cpr/form', loadVerif:!Pa.prsId, inputs:'prsId='+Pa.prsId,loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{ $M.Ht.title.innerHTML =Jr.name;
			L=Jr;
			var wid=$1.t('input',{type:'hidden','class':jsF,name:'prsId',value:Pa.prsId},cont);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',req:'Y',L:'Nombre Completo',placeholder:'Nombre Apellido',I:{tag:'input',type:'text','class':jsF,name:'name',value:L.name,maxLen:50}},cont);
			$1.T.divL({wxn:'wrapx6',L:'Relación',aGo:'jsv.nomCprRel',I:{tag:'select','class':jsF,name:'positionId',opts:$JsV.nomCprRel,selected:L.positionId}},divL);
			var valCrd=(Jr.cardId)?Jr.cardName:'';
			var sea=$1.lTag({tag:'crd','class':jsF,value:valCrd,D:Jr});
			$1.T.divL({wxn:'wrapx4',L:'Empleado Rel.',req:'Y',I:{node:sea}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Cumpleaños',I:{tag:'input',type:'date','class':jsF,name:'birthDay',value:Jr.birthDay}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Celular',I:{tag:'input',type:'text','class':jsF,name:'cellular',value:Jr.cellular}},cont);
			$1.T.divL({wxn:'wrapx8',L:'Teléfono 1',I:{tag:'input',type:'text','class':jsF,name:'tel1',value:Jr.tel1}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Teléfono 2',I:{tag:'input',type:'text','class':jsF,name:'tel2',value:Jr.tel2}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Email',I:{tag:'input',type:'text','class':jsF,name:'email',value:Jr.email}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',req:'Y',L:'Genero',I:{tag:'select','class':jsF,name:'gender',opts:$V.gender,selected:L.gender}},cont);
		$1.T.divL({wxn:'wrapx4',L:'Profesión',I:{tag:'input',type:'text','class':jsF,name:'title',value:L.title}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Tipo Documento',I:{tag:'select',sel:{'class':jsF,name:'licTradNum'},opts:$V.crdLicTradType,selected:Jr.licTradType}},divL);
			$1.T.divL({wxn:'wrapx8',L:'N°. Documento',I:{tag:'input',type:'text','class':jsF,name:'licTradNum',value:L.licTradNum}},divL);
			$1.t('h4',{textNode:'Direcciónes','class':'divLineTitleSection'},cont);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx2',L:'Dirección Principal',placeholder:'Cr 29-10 #33B',I:{tag:'input',type:'text','class':jsF,name:'address',value:Jr.address}},cont);
			$1.T.divL({wxn:'wrapx2',L:'Dirección Secundaria',placeholder:'Cr 29-10 #33B',I:{tag:'input',type:'text','class':jsF,name:'address2',value:Jr.address2}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx1',L:'Notas',I:{tag:'textarea','class':jsF,name:'notes',textNode:Jr.notes}},cont);
		}
		var resp=$1.t('div',0,cont);
		var btn=$Api.send({PUT:Api.Nom.b+'cpr', loade:resp,jsBody:cont,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(Jr2.prsId){ wid.value=Jr2.prsId; }
		}},cont);
	}});
}
}


Nom.Cfr={
	Lg:function(L,men){
		var Li=[];
		if(men){
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	LgForms:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ JForm.h('form','nomCfr',T.P); } },men);
			$1.T.btnFa({faBtn:'fa-bolt',title:'Reporte',P:L,func:function(T){ JForm.h('rep','nomCfr',T.P); }},men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Nom.b+'cfr/emp',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['Empleado','Complementario','Técnico'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var val=(L.docDate)?L.docDate:'Pendiente';
				$1.t('td',{textNode:L.cardName},tr);
				var td=$1.t('td',{textNode:val},tr);
				$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ $M.to('JForm.ans.nomCfr.comp','fId:'+T.P.fId+',cardId:'+T.P.cardId); } },td);
				var val=(L.docDate2)?L.docDate2:'Pendiente';
				var td=$1.t('td',{textNode:val},tr);
				$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ $M.to('JForm.ans.nomCfr.tecni','fId:'+T.P.fId2+',cardId:'+T.P.cardId); } },td);
			}
		}});
	},
	ans:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		vPost ='&cardId='+Pa.cardId+'&fId='+Pa.fId;
		$Api.get({f:Api.Nom.b+'cfr/ans',inputs:vPost,loade:cont,errWrap:cont, func:function(Jr){ Jr.fId=Jr.fId;
			$1.t('h5',{textNode:Jr.fCode+' - '+Jr.fName},cont);
			var xF= new JForm.Res({Jr:Jr,api:Api.Nom.b+'cfr/ans',func:function(Jr2){
			},
			F:[
				{divLine:1,wxn:'wrapx4',L:'Empleado',I:{lTag:'disabled',value:Jr.cardName,AJs:{cardId:Pa.cardId}}},
				{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea',name:'lineMemo',value:Jr.lineMemo}},
			]
			},
			cont);
			xF.send();
		}});
	},
	forms:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Nom.b+'cfr/forms',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['','Código','Nombre','Versión'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				Nom.Cfr.LgForms(L,td);
				
				$1.t('td',{textNode:L.fCode},tr);
				$1.t('td',{textNode:L.fName},tr);
				$1.t('td',{textNode:L.version},tr);
			}
		}});
	},
	form:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Nom.b+'cfr/form',loadVerif:!Pa.fId,inputs:'fId='+Pa.fId,loade:cont,errWrap:cont, func:function(Jr){
			var xF=new JForm.Tpt({Jr:Jr,api:Api.Nom.b+'cfr/form',func:function(Jr2){
			}},
			cont,{});
			xF.send();
		}});
	},
	rep:function(){
		var cont=$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.Nom.b+'cfr/rep',inputs:'fId='+Pa.fId+'&'+$1.G.filter(),loade:cont,func:function(Jr){
			R=JForm.repData(Jr,{Tbf:['Empleado']});
			var tb=$1.T.table(R.Tbf);
			var tBody=$1.t('tbody',0,tb);
			for(var i in R.L){ L=R.L[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.cardName},tr);
				for(var i in R.aL){
					val=(L.a[R.aL[i].aid])?L.a[R.aL[i].aid].aText:'';
					$1.t('td',{textNode:val},tr);
				}
				$1.T.tbExport(tb,{ext:'xlsx',print:'Y'},cont);
			}
		}});
	},
}

Nom.Cla={
	Lg:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ Nom.Cla.form(T.P); } },men);
			$1.T.btnFa({faBtn:'fa-sign-out',title:'Formulario de Retiro',P:L,func:function(T){ Nom.Cla.leaveForm(T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Nom.pr+'cla',inputs:$1.G.filter(),loade:cont,errWrap:cont,func:function(Jr){
			var tb=$1.T.table(['','Tercero','Apertura','Ingreso','Retiro','Detalles'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				var lc=(L.closeDate)?L.closeDate:'Pendiente';
				var lt=(L.leaveDate)?L.leaveDate:'No';
				Nom.Cla.Lg(L,td);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.openDate},tr);
				$1.t('td',{textNode:lc},tr);
				td=$1.t('td',{textNode:lt},tr);
				if(L.leaveForm!='NA'){
					$1.T.btnFa({faBtn:'fa fa-file-text-o',title:'Encuenta de Retiro',P:L,func:function(T){ $M.to('JForm.ans.nomCla.leaveAns','fId:'+$V.nomCfrLeaFid+',claId:'+T.P.claId); } },td);
				}
				$1.t('td',{textNode:L.lineMemo},tr);
			}
		}});
	},
	form:function(Pa){
		var Pa=(Pa)?Pa:{};
		var api1=Api.Nom.pr+'cla';
		var jsF=$Api.JS.cls;
		var wrap=$1.t('div');
		$Api.get({f:api1+'/form',loadVerif:!Pa.claId,inputs:'claId='+Pa.claId,errWrap:wrap,loade:wrap,func:function(Jr){
			if(Pa.claId){ $Api.JS.addF({name:'claId',value:Pa.claId},wrap); }
			var nd=Nom.Sea.crd(Jr,wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx1',L:'Empleado',Inode:nd},wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Apertura',I:{lTag:'date',name:'openDate','class':jsF,value:Jr.openDate}},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Ingreso',I:{lTag:'date',name:'closeDate','class':jsF,value:Jr.closeDate}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Salario',I:{lTag:'$',name:'salarioBal','class':jsF,value:Jr.salarioBal}},divL);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea',name:'lineMemo','class':jsF,value:Jr.lineMemo}},wrap);
			var resp=$1.t('div',0,wrap);
			var Ps={PUT:api1,jsBody:wrap,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
			}};
			$Api.send(Ps,wrap);
		}});
		var wrapBk=$1.Win.open(wrap,{winSize:'medium',winTitle:'Formulario Contrato'});
	},
	leaveForm:function(Pa){
		var Pa=(Pa)?Pa:{};
		var api1=Api.Nom.pr+'cla';
		var jsF=$Api.JS.cls;
		var wrap=$1.t('div');
		$Api.get({f:api1+'/leave/form',loadVerif:!Pa.claId,inputs:'claId='+Pa.claId,loade:wrap,func:function(Jr){
			$Api.JS.addF({name:'claId',value:Pa.claId},wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx1',L:'Empleado',I:{tag:'disabled',value:Jr.cardName}},wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Apertura',I:{tag:'disabled',value:Jr.openDate}},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Ingreso',I:{tag:'disabled',value:Jr.closeDate}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Fecha Retiro',I:{lTag:'date','class':jsF,name:'leaveDate',value:Jr.leaveDate}},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Voluntario',I:{tag:'select','class':jsF,name:'leaveVol',opts:$V.YN,selected:Jr.leaveVol}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Motivo',I:{tag:'select','class':jsF,name:'leaveReason',opts:$JsV.nomLeaveRea,selected:Jr.leaveReason}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Reintegro',I:{tag:'select','class':jsF,name:'reinStatus',opts:$V.NomReinT,selected:Jr.reinStatus}},divL);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Detalles del Retiro',I:{tag:'textarea',name:'leaveMemo','class':jsF,value:Jr.leaveMemo}},wrap);
			var resp=$1.t('div',0,wrap);
			var Ps={PUT:api1+'/leave',jsBody:wrap,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
			}};
			$Api.send(Ps,wrap);
		}});
		var wrapBk=$1.Win.open(wrap,{winSize:'medium',winTitle:'Formulario de Terminación'});
	},
	leaveAns:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		var vPost='fId='+Pa.fId+'&claId='+Pa.claId;
		$Api.get({f:Api.Nom.pr+'cla/leaveAns',inputs:vPost,loade:cont,errWrap:cont, func:function(Jr){ Jr.fId=Pa.fId;
			$1.t('h5',{textNode:Jr.fCode+' - '+Jr.fName},cont);
			$Api.JS.addF({name:'claId',value:Jr.claId},cont);
			$Api.JS.addF({name:'cardId',value:Jr.cardId},cont);
			var xF= new JForm.Res({Jr:Jr,api:Api.Nom.pr+'cla/leaveAns',func:function(Jr2){
			},
			F:[
				{divLine:1,wxn:'wrapx4',L:'Empleado',I:{lTag:'disabled',value:Jr.cardName,AJs:{cardId:Pa.cardId}}},
				{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea',name:'lineMemo',value:Jr.lineMemo}},
			]
			},
			cont);
			xF.send();
		}});
	}
}

Nom.Nov={};
Nom.Nov.Turn={
	ini:function(){
		var cont =$M.Ht.cont;
		var wt=$1.t('div',{'class':'nomTurn_top'},cont);
		var wm=$1.t('div',{'class':'nomTurn_wm'},cont);
		Nom.Nov.Turn.open(wm);
	},
	open:function(cont){
		var jsF=$Api.JS.cls;
		var divL=$1.T.divL({divLine:1,L:'Realizando...',wxn:'wrapx4',I:{tag:'select','class':jsF+' elineType',name:'lineType',opts:{E:'Entradas',S:'Salidas'},noBlank:'Y'}},cont);
		$1.T.divL({wxn:'wrapx10',L:'Inicio turno',I:{lTag:'time',name:'openTurn','class':jsF,value:$V.nomTurnHini}},divL);
		$1.T.divL({wxn:'wrapx10',L:'Fin turno',I:{lTag:'time',name:'closeTurn','class':jsF,value:$V.nomTurnHend}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Dominical',I:{tag:'select',name:'dominical','class':jsF,opts:$V.nomNovFestivo,noBlank:'Y'}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Extra',I:{tag:'select',name:'hextra','class':jsF,opts:$V.NY,noBlank:'Y'}},divL);
		var divL=$1.T.divL({divLine:1,L:'Código...',wxn:'wrapx1',I:{tag:'input',type:'text',placeholder:'Leer o Digitar código...','class':jsF+' bcLeer',name:'lineCode',style:'fontSize:20px;'}},cont);
		var resp=$1.t('div',0,cont);
		var eType=$1.q('.elineType',cont);
		var bc=$1.q('.bcLeer',cont);
		bc.keyPresi('enter',function(Tv){
			var sapi=(eType.value=='S')?'close':'open';
			$Api.post({f:Api.Nom.b+'novTurn/'+sapi,jsBody:cont,func:function(Jr,o){
				Tv.value='';
				$Api.resp(resp,Jr);
				if(o){
					$Api.resp(wtb,{text:o.cardName+'. Entrada registrada'});
				}
			}});
		});
			var wtb1=$1.t('div',0,cont);
			var wtb=$1.t('div',0,cont);
	},
	get:function(){
		var cont =$M.Ht.cont;
		$Api.get({f:Api.Nom.b+'novTurn',inputs:$1.G.filter() ,loade:cont,errWrap:cont,func:function(Jr){
			var tb=$1.T.table(['','Nombre','Ingreso','Salida'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){
					Nom.Nov.Turn.form(T.P);
				}},td);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.timeEnt},tr);
				$1.t('td',{textNode:L.timeOut},tr);
				var td=$1.t('td',0,tr);
				$1.T.btnFa({faBtn:'fa-trash',title:'Eliminar',P:L,func:function(T){
					$Api.put({f:Api.Nom.b+'novTurn/cancel',inputs:'tid='+T.P.tid,confirm:{text:'Se va a anular el registro de entrada, no se puede reversar esta acción',noClose:true}});
				}},td);
			}
		}});
	},
	form:function(Pa){
		var Pa=(Pa)?Pa:{};
		Pa.cardId=1;
		var api1=Api.Nom.b+'novTurn';
		var jsF=$Api.JS.cls;
		var wrap=$1.t('div');
		$Api.get({f:api1+'/one',loadVerif:!Pa.tid,inputs:'tid='+Pa.tid,loade:wrap,func:function(Jr){
			if(Jr.errNo){ return $Api.resp(wrap,Jr); }
			if(Jr.tid){
				$Api.JS.addF([
				{name:'tid',value:Jr.tid},
				{name:'cardId',value:Jr.cardId},
				{name:'openDate',value:Jr.openDate}
				],wrap);
				var divL=$1.T.divL({divLine:1,wxn:'wrapx4_1',L:'Empleado',I:{tag:'input',type:'text',disabled:'disabled',value:Jr.cardName}},wrap);
				$1.T.divL({wxn:'wrapx4',L:'Fecha',I:{lTag:'date',value:Jr.openDate,disabled:'disabled'}},divL);
			}
			else{
				var divL=$1.T.divL({divLine:1,wxn:'wrapx4_1',L:'Empleado',Inode:Nom.Sea.crd({},wrap,Jr)},wrap);
				$1.T.divL({wxn:'wrapx4',L:'Fecha',I:{lTag:'date',value:Jr.openDate,'class':jsF,name:'openDate'}},divL);
			}
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Inicio turno',I:{lTag:'time',name:'openTurn','class':jsF,value:Jr.openTurn}},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Fin turno',I:{lTag:'time',name:'closeTurn','class':jsF,value:Jr.closeTurn}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Dominical',I:{tag:'select',name:'dominical','class':jsF,opts:$V.nomNovFestivo,noBlank:'Y',selected:Jr.dominical}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Extra',I:{tag:'select',name:'hextra','class':jsF,opts:$V.NY,noBlank:'Y',selected:Jr.hextra}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Entrada',I:{lTag:'time',name:'timeEnt','class':jsF,value:Jr.timeEnt}},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Salida',I:{lTag:'time',time:'Y',name:'timeOut','class':jsF,value:Jr.timeOut}},divL);
			var resp=$1.t('div',0,wrap);
			var Ps={POST:api1,jsBody:wrap,loade:resp,func:function(Jr2){
			$Api.resp(resp,Jr2); prev.innerHTML='';
				if(Jr2.preview){
					var tb=$1.T.table(['',''],0,prev);
					var tbody=$1.t('tbody',0,tb);
					tr=$1.t('tr',0,tbody);
					$1.t('td',{textNode:Jr2.hrsTurn},tr);
					$1.t('td',{textNode:'Horas Turno'},tr);
					$js.forIf($V.nomNovClass,{type:'H'},function(O,P2){
						if(P2[O.k]){
							tr=$1.t('tr',0,tbody);
							$1.t('td',{textNode:P2[O.k]},tr);
							$1.t('td',{textNode:O.v},tr);
						}
					},Jr2);
				}
			}}
			if(Jr.tid){ Ps.PUT=Ps.POST; delete(Ps.POST); }
			var btn=$Api.send(Ps,wrap);
			$1.T.btnFa({fa:'fa-eye',textNode:'Ver Calculo',func:function(){
				var pre=$Api.JS.addF({name:'preview',value:'Y'},wrap);
				btn.click();
				$1.delet(pre);
			}},wrap);
			var prev=$1.t('div',0,wrap);
		}});
		$1.Win.open(wrap,{winSize:'medium',winTitle:'Registro Turno'});
	},
}
Nom.Nov.Her={
	Lg:function(L,men){
		var Li=[];;
		Li= $Opts.add('nomNovHer',Li,L);
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar Registro',P:L,func:function(T){ Nom.Nov.Her.form(T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Nom.b+'novHer',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['','Empleado','Fecha','Entrada','Salida'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',{textNode:''},tr);
				Nom.Nov.Her.Lg(L,td);
				var ldt=(L.timeOut)?$2d.f(L.timeOut,'H:i'):'Pendiente';
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.lineDate},tr);
				$1.t('td',{textNode:$2d.f(L.timeEnt,'H:i')},tr);
				$1.t('td',{textNode:ldt},tr);
			}
		}});
	},
	form:function(Pa){
		var Pa=(Pa)?Pa:{};
		var api1=Api.Nom.b+'novHer';
		var jsF=$Api.JS.cls;
		var wrap=$1.t('div');
		$Api.get({f:api1+'/one',loadIf:!Pa.cardId,inputs:'cardId='+Pa.cardId+'&lineDate='+Pa.lineDate,loade:wrap,func:function(Jr){
			var D1={};
			for(var i in Jr.L){ D1=Jr.L[i]; break; }
			var nd=Nom.Sea.crd(D1,wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx2',L:'Empleado',Inode:nd},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Fecha',I:{tag:'date',name:'lineDate','class':jsF,value:D1.lineDate}},divL);
			var tb=$1.T.table(['','Factor','# de horas'],0,wrap);
			var tBody=$1.t('tbody',0,tb);
			for(var i in $V.nomNovClass){ var X=$V.nomNovClass[i];
				if(X.type!='H'){ continue; }
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:X.v},tr);
				$1.t('td',{textNode:X.f},tr);
				var td=$1.t('td',0,tr);
				var val=(Jr.L && Jr.L[X.k])?Jr.L[X.k].quantity*1:'';
				$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF,name:X.k,value:val},td);
			}
			var resp=$1.t('div',0,wrap);
			var Ps={POST:api1,jsBody:wrap,loade:resp,func:function(Jr2){
				$Api.resp(resp,Jr2);
			}}; if(Pa.cardId){ Ps.PUT=Ps.POST; delete(Ps.POST); }
			$Api.send(Ps,wrap);
		}});
		$1.Win.open(wrap,{winSize:'medium',winTitle:'Horas / Recargos'});
	},
}
Nom.Nov.Vac={
	Lg:function(L,men){
		var Li=[];
		Li= $Opts.add('nomNovVac',Li,L);
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar Registro',P:L,func:function(T){ Nom.Nov.Vac.form(T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Nom.b+'novVac',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['','Empleado','Periodo','Dias','Detalles'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',{textNode:''},tr);
				Nom.Nov.Vac.Lg(L,td);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:_g(L.periodo,$Tb.nomOpdt)},tr);
				$1.t('td',{textNode:L.quantity2*1},tr);
				$1.t('td',{textNode:L.slineMemo},tr);
			}
		}});
	},
	form:function(Pa){
		var Pa=(Pa)?Pa:{};
		var api1=Api.Nom.b+'novVac';
		var jsF=$Api.JS.cls;
		var wrap=$1.t('div');
		$Api.get({f:api1+'/form',loadVerif:!Pa.cardId,inputs:'cardId='+Pa.cardId+'&periodo='+Pa.periodo,loade:wrap,func:function(Jr){
			var D1=$js.one(Jr.L,{});
			var nd=Nom.Sea.crd(D1,wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx6_1',L:'Empleado',Inode:nd},wrap);
			$1.T.divL({wxn:'wrapx6',L:'Periodo',I:{tag:'select',name:'periodo','class':jsF,opts:$Tb.nomOpdt,selected:D1.periodo}},divL);
			var tb=$1.T.table(['Tipo','Inicio','Final','Dias',''],0,wrap);
			var tBody=$1.t('tbody',0,tb);
			if(Jr.L){ for(var i in Jr.L){
				trA(Jr.L[i],tBody);
			}}
			else{ trA([{}],tBody); }
			$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Linea',P:L,func:function(T){ trA(T.P,tBody); }},wrap);
			var resp=$1.t('div',0,wrap);
			var Ps={PUT:api1,jsBody:wrap,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
				if(o){ $1.delet(wrapBk); Nom.Nov.Vac.form(o); }
			}};
			$Api.send(Ps,wrap);
		}});
		function trA(L,tBody){ L=(L)?L:{};
			var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
			var jsF2=$Api.JS.clsLN;;
			var td=$1.t('td',0,tr);
			$1.T.sel({'class':jsF2,name:'lineType2',selected:L.lineType2,opts:$V.nomNovClass,kIf:{type:'V'}},td);
			var td=$1.t('td',0,tr);
			var tinp=$1.t('input',{type:'date','class':jsF2,name:'lineDate',value:L.lineDate,style:'width:8rem'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'date','class':jsF2,name:'lineDue',value:L.lineDue,style:'width:8rem'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF2,name:'quantity',value:L.quantity,style:'width:4rem'},td);
			var td=$1.t('td',0,tr);
			if(L.id){ 
				$1.T.check({'class':jsF2,name:'delete',AJs:{id:L.id}},td);
			}
			else{ $1.T.btnFa({faBtn:'fa-trash',title:'Quitar',P:L,func:function(T){ $1.delet(tr); }},td);
			}
		}
		var wrapBk=$1.Win.open(wrap,{winSize:'medium',winTitle:'Vacaciones'});
	}
}
Nom.Nov.Inc={
	Lg:function(L,men){
		var Li=[];
		Li= $Opts.add('nomNovInc',Li,L);
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar Registro',P:L,func:function(T){ Nom.Nov.Inc.form(T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Nom.b+'novInc',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['','Empleado','Periodo','Dias'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',{textNode:''},tr);
				Nom.Nov.Inc.Lg(L,td);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:_g(L.periodo,$Tb.nomOpdt)},tr);
				$1.t('td',{textNode:L.quantity2*1},tr);
			}
		}});
	},
	form:function(Pa){
		var Pa=(Pa)?Pa:{};
		var api1=Api.Nom.b+'novInc';
		var jsF=$Api.JS.cls;
		var wrap=$1.t('div');
		$Api.get({f:api1+'/form',loadVerif:!Pa.cardId,inputs:'cardId='+Pa.cardId+'&periodo='+Pa.periodo,loade:wrap,func:function(Jr){
			var D1=$js.one(Jr.L,{});
			var nd=Nom.Sea.crd(D1,wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx6_1',L:'Empleado',Inode:nd},wrap);
			$1.T.divL({wxn:'wrapx6',L:'Periodo',I:{tag:'select',name:'periodo','class':jsF,opts:$Tb.nomOpdt,selected:D1.periodo}},divL);
			var tb=$1.T.table(['Tipo','Inicio','Final','Detalles',''],0,wrap);
			var tBody=$1.t('tbody',0,tb);
			if(Jr.L){ for(var i in Jr.L){
				trA(Jr.L[i],tBody);
			}}
			else{ trA([{}],tBody); }
			$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Linea',P:L,func:function(T){ trA(T.P,tBody); }},wrap);
			var resp=$1.t('div',0,wrap);
			var Ps={PUT:api1,jsBody:wrap,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
				if(o){ $1.delet(wrapBk); Nom.Nov.Inc.form(o); }
			}};
			$Api.send(Ps,wrap);
		}});
		function trA(L,tBody){ L=(L)?L:{};
			var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
			var jsF2=$Api.JS.clsLN;;
			var td=$1.t('td',0,tr);
			$1.T.sel({'class':jsF2,name:'lineType2',selected:L.lineType2,opts:$V.nomNovClass,kIf:{type:'I'}},td);
			var td=$1.t('td',0,tr);
			var tinp=$1.t('input',{type:'date','class':jsF2,name:'lineDate',value:L.lineDate,style:'width:8rem'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'date','class':jsF2,name:'lineDue',value:L.lineDue,style:'width:8rem'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text',min:0,'class':jsF2,name:'lineMemo',value:L.lineMemo,style:'width:10rem'},td);
			var td=$1.t('td',0,tr);
			if(L.id){ 
				$1.T.check({'class':jsF2,name:'delete',AJs:{id:L.id}},td);
			}
			else{ $1.T.btnFa({faBtn:'fa-trash',title:'Quitar',P:L,func:function(T){ $1.delet(tr); }},td);
			}
		}
		var wrapBk=$1.Win.open(wrap,{winSize:'medium',winTitle:'Incapacidades'});
	}
}
Nom.Nov.Lic={
	Lg:function(L,men){
		var Li=[];
		Li= $Opts.add('nomNovLic',Li,L);
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar Registro',P:L,func:function(T){ Nom.Nov.Lic.form(T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Nom.b+'novLic',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			if(Jr.errNo){ return $Api.resp(cont,Jr); }
			var vType=$1.qOpt('.nomNovVType');
			var tbf=[];
			if(vType=='1'){ tbf=['','Empleado','Periodo','Dias']; }
			else{ tbf=['','Empleado','Periodo','Dias','Inicia','Termina']; }
			var tb=$1.T.table(tbf,0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',{textNode:''},tr);
				Nom.Nov.Lic.Lg(L,td);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:_g(L.periodo,$Tb.nomOpdt)},tr);
				if(vType=='1'){
					$1.t('td',{textNode:L.quantity2*1},tr);
				}
				else{
					$1.t('td',{textNode:L.quantity*1},tr);
					$1.t('td',{textNode:L.lineDate},tr);
					$1.t('td',{textNode:L.lineDue},tr);
				}
				
			}
		}});
	},
	form:function(Pa){
		var Pa=(Pa)?Pa:{};
		var api1=Api.Nom.b+'novLic';
		var jsF=$Api.JS.cls;
		var wrap=$1.t('div');
		$Api.get({f:api1+'/form',loadVerif:!Pa.cardId,inputs:'cardId='+Pa.cardId+'&periodo='+Pa.periodo,loade:wrap,func:function(Jr){
			var isReg=($1.qOpt('.nomNovVType')!='1');
			var D1=$js.one(Jr.L,{});
			var nd=Nom.Sea.crd(D1,wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx6_1',L:'Empleado',Inode:nd},wrap);
			$1.T.divL({wxn:'wrapx6',L:'Periodo',I:{tag:'select',name:'periodo','class':jsF,opts:$Tb.nomOpdt,selected:D1.periodo}},divL);
			var tb=$1.T.table(['Tipo','Inicio','Final','Detalles',''],0,wrap);
			var tBody=$1.t('tbody',0,tb);
			if(Jr.L){ for(var i in Jr.L){
				trA(Jr.L[i],tBody,{isReg:isReg,id:Pa.id});
			}}
			else{ trA([{}],tBody); }
			$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Linea',P:L,func:function(T){ trA(T.P,tBody); }},wrap);
			var resp=$1.t('div',0,wrap);
			var Ps={PUT:api1,jsBody:wrap,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
				if(o){ $1.delet(wrapBk); Nom.Nov.Lic.form(o); }
			}};
			$Api.send(Ps,wrap);
		}});
		function trA(L,tBody,Px){
			L=(L)?L:{}; Px=(Px)?Px:{};
			var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
			var jsF2=$Api.JS.clsLN;;
			var td=$1.t('td',0,tr);
			$1.T.sel({'class':jsF2,name:'lineType2',selected:L.lineType2,opts:$V.nomNovClass,kIf:{type:'L'}},td);
			if(Px.isReg && Px.id==L.id){ td.style.backgroundColor='#73b2fe';
				td.childNodes[0].focus();
			}
			var td=$1.t('td',0,tr);
			var tinp=$1.t('input',{type:'date','class':jsF2,name:'lineDate',value:L.lineDate,style:'width:8rem'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'date','class':jsF2,name:'lineDue',value:L.lineDue,style:'width:8rem'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text',min:0,'class':jsF2,name:'lineMemo',value:L.lineMemo,style:'width:10rem'},td);
			var td=$1.t('td',0,tr);
			if(L.id){ 
				$1.T.check({'class':jsF2,name:'delete',AJs:{id:L.id}},td);
			}
			else{ $1.T.btnFa({faBtn:'fa-trash',title:'Quitar',P:L,func:function(T){ $1.delet(tr); }},td);
			}
		}
		var wrapBk=$1.Win.open(wrap,{winSize:'medium',winTitle:'Incapacidades'});
	}
}
Nom.Nov.Sus={
	Lg:function(L,men){
		var Li=[];
		Li= $Opts.add('nomNovSus',Li,L);
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar Registro',P:L,func:function(T){ Nom.Nov.Sus.form(T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Nom.b+'novSus',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			if(Jr.errNo){ return $Api.resp(cont,Jr); }
			var vType=$1.qOpt('.nomNovVType');
			var tbf=[];
			if(vType=='1'){ tbf=['','Empleado','Periodo','Dias']; }
			else{ tbf=['','Empleado','Periodo','Dias','Inicia','Termina']; }
			var tb=$1.T.table(tbf,0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',{textNode:''},tr);
				Nom.Nov.Sus.Lg(L,td);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:_g(L.periodo,$Tb.nomOpdt)},tr);
				if(vType=='1'){
					$1.t('td',{textNode:L.quantity2*1},tr);
				}
				else{
					$1.t('td',{textNode:L.quantity*1},tr);
					$1.t('td',{textNode:L.lineDate},tr);
					$1.t('td',{textNode:L.lineDue},tr);
				}
				
			}
		}});
	},
	form:function(Pa){
		var Pa=(Pa)?Pa:{};
		var api1=Api.Nom.b+'novSus';
		var jsF=$Api.JS.cls;
		var wrap=$1.t('div');
		$Api.get({f:api1+'/form',loadVerif:!Pa.cardId,inputs:'cardId='+Pa.cardId+'&periodo='+Pa.periodo,loade:wrap,func:function(Jr){
			var isReg=($1.qOpt('.nomNovVType')!='1');
			var D1=$js.one(Jr.L,{});
			var nd=Nom.Sea.crd(D1,wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx6_1',L:'Empleado',Inode:nd},wrap);
			$1.T.divL({wxn:'wrapx6',L:'Periodo',I:{tag:'select',name:'periodo','class':jsF,opts:$Tb.nomOpdt,selected:D1.periodo}},divL);
			var tb=$1.T.table(['Tipo','Inicio','Final','Detalles',''],0,wrap);
			var tBody=$1.t('tbody',0,tb);
			if(Jr.L){ for(var i in Jr.L){
				trA(Jr.L[i],tBody,{isReg:isReg,id:Pa.id});
			}}
			else{ trA([{}],tBody); }
			$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Linea',P:L,func:function(T){ trA(T.P,tBody); }},wrap);
			var resp=$1.t('div',0,wrap);
			var Ps={PUT:api1,jsBody:wrap,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
				if(o){ $1.delet(wrapBk); Nom.Nov.Sus.form(o); }
			}};
			$Api.send(Ps,wrap);
		}});
		function trA(L,tBody,Px){
			L=(L)?L:{}; Px=(Px)?Px:{};
			var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
			var jsF2=$Api.JS.clsLN;;
			var td=$1.t('td',0,tr);
			$1.T.sel({'class':jsF2,name:'lineType2',selected:L.lineType2,opts:$V.nomNovClass,kIf:{type:'S'}},td);
			if(Px.isReg && Px.id==L.id){ td.style.backgroundColor='#73b2fe';
				td.childNodes[0].focus();
			}
			var td=$1.t('td',0,tr);
			var tinp=$1.t('input',{type:'date','class':jsF2,name:'lineDate',value:L.lineDate,style:'width:8rem'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'date','class':jsF2,name:'lineDue',value:L.lineDue,style:'width:8rem'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text',min:0,'class':jsF2,name:'lineMemo',value:L.lineMemo,style:'width:10rem'},td);
			var td=$1.t('td',0,tr);
			if(L.id){ 
				$1.T.check({'class':jsF2,name:'delete',AJs:{id:L.id}},td);
			}
			else{ $1.T.btnFa({faBtn:'fa-trash',title:'Quitar',P:L,func:function(T){ $1.delet(tr); }},td);
			}
		}
		var wrapBk=$1.Win.open(wrap,{winSize:'medium',winTitle:'Suspención'});
	}
}

Nom.Nov.Rep={
	turn:function(){
		$Api.Rep.base({f:Api.Nom.b+'novRep/turn',inputs:$1.G.filter(),
		Fs:[{f:'licTradNum',t:'Documento'},{f:'cardName',t:'Nombre Empleado'},{f:'openDate',t:'Fecha'},{f:'timeEnt',t:'Ingreso'},{f:'timeOut',t:'Salida'},{f:'workSede',t:'Sede',_g:$Tb.owsu},{f:'workArea',t:'Area',_g:$JsV.nomWorkArea},{f:'workPosi',t:'Cargo',_g:$JsV.nomWorkPosi}
		]},$M.Ht.cont);
	},
	novHer:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Nom.b+'novRep/novHer',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var Dtk={}; var nk1=0;
			var Dts=[]; var Lc={};
			var cssWar='backgroundColor:#CC0';
			var Dtot={}; var totalLen=0; //para totales al final
			for(var i in Jr.L){ L=Jr.L[i];
				var k1=L.openDate; var k2=L.cardId;
				var k3=L.lineType2;
				if(!Dtk[k1]){ Dtk[k1]=nk1; nk1++; }
				var kr=Dtk[k1];
				if(!Dts[kr]){ Dts[kr]={openDate:L.openDate,Ty:{}}; }
				if(!Lc[k2]){
					Lc[k2]=L; Lc[k2].To={};
				}//cardName
				if(!Lc[k2][k1]){ Lc[k2][k1]=L;}//dia1..dia2...
				if(k3){
					if(!Dtot[k3]){ totalLen++; Dtot[k3]=k3; }
					Dts[kr].Ty[k3]=k3;
					if(!Lc[k2].To[k3]){ Lc[k2].To[k3]=0; }
					Lc[k2].To[k3] +=L.quantity*1;
					Lc[k2][k1][k3]=L.quantity*1;
				}
			}
			var cssa='backgroundColor:#CCC;';
			var tb=$1.T.table(['Documento','Nombre','Cargo','Area']);
			var tBody=$1.t('tbody',0,tb);
			var trh=$1.q('tr',tb);
			var tr1=$1.t('tr',0,tBody); 
			$1.t('td',{textNode:'Documento'},tr1);
			$1.t('td',{textNode:'Nombre'},tr1);
			$1.t('td',{textNode:'Cargo'},tr1);
			$1.t('td',{textNode:'Area'},tr1);
			//dias --->
			Dts=$js.sortBy('openDate',Dts);
			for(var i in Dts){
				var len=3;
				$1.t('td',{textNode:'Ent.-Sal.',style:cssa},tr1);
				$1.t('td',{textNode:'Horario'},tr1);
				$1.t('td',{textNode:'HO'},tr1);
				for(var i2 in $V.nomNovClass){ 
					k3=$V.nomNovClass[i2].k;
					if(Dts[i].Ty && Dts[i].Ty[k3]){ len++;
						$1.t('td',{textNode:k3},tr1);
					}
				}
				$1.t('th',{textNode:Dts[i].openDate},trh);
				for(var z=1; z<len; z++){
					$1.t('th',{textNode:''},trh);
				}
			}
			// totales --->
			var csst='backgroundColor:#37a6c9';
			$1.t('th',{textNode:'Consolidado',style:csst},trh);
			for(var z=1; z<totalLen; z++){
					$1.t('th',{textNode:''},trh);
				}
			for(var i2 in $V.nomNovClass){ 
				k3=$V.nomNovClass[i2].k;
				if(Dtot && Dtot[k3]){ $1.t('td',{textNode:k3},tr1); }
			}
			// empleado, dias --->
			var csst='backgroundColor:#9ce7ff';
			var cssFalto='backgroundColor:#cc002f';
			for(var i in Lc){ L=Lc[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.licTradNum},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:_g(L.workPosi,$JsV.nomWorkPosi)},tr);
				$1.t('td',{textNode:_g(L.workArea,$JsV.nomWorkArea)},tr);
				// dias --->
				for(var i in Dts){
					var k1=Dts[i].openDate;
					var falto=false;
					var L2=(L[k1])?L[k1]:{};
					if(L2.timeOut){
						$1.t('td',{textNode:L2.timeEnt.substr(11,5)+'-'+L2.timeOut.substr(11,5),style:cssa},tr);
					} 
					else{ $1.t('td',{style:cssFalto},tr); falto=true; }
					if(L2.closeTurn){
						var cssDef=(L2.hrsTurn*1>8)?cssWar:'';
						$1.t('td',{textNode:L2.openTurn.substr(11,5)+'-'+L2.closeTurn.substr(11,5),style:cssDef},tr);
						$1.t('td',{textNode:L2.hrsTurn*1,style:cssDef},tr);
					}
					else{ $1.t('td',{colspan:2,style:cssFalto},tr); falto=true; }
					for(var i2 in $V.nomNovClass){
						k3=$V.nomNovClass[i2].k;
						var val='';
						if(L[k1] && L[k1][k3]){ val=L[k1][k3]; }
						var cssDef=(val>8)?cssWar:'';
						if(falto){ css1=cssFalto; }
						if(Dts[i].Ty && Dts[i].Ty[k3]){
							$1.t('td',{textNode:val,style:cssDef},tr);
						}
					}
				}
				// totales -->
				for(var i2 in $V.nomNovClass){ 
					k3=$V.nomNovClass[i2].k;
					if(L.To && L.To[k3]){ $1.t('td',{textNode:$js.toFixed(L.To[k3],2),style:csst},tr); }
					else if(Dtot[k3]){ $1.t('td',{style:cssa},tr); }
				}
			}
			tb=$1.T.tbExport(tb,{ext:'xlsx',fileName:'Novedades',print:'Y'});
			cont.appendChild(tb);
		}});
	},
	nov:function(){
		$Api.Rep.base({f:Api.Nom.b+'novRep/nov',inputs:$1.G.filter(),
		Fs:[{f:'licTradNum',t:'Documento'},{f:'cardName',t:'Nombre Empleado'},{f:'lineType',t:'Tipo',_g:$V.nomNovType},{f:'lineType2',t:'Clase',_g:$V.nomNovClass},{f:'lineDate',t:'Inicio'},{f:'lineDue',t:'Final'},{f:'quantity',t:'Cant.',fType:'number'},{f:'numFactor',t:'Factor',fType:'number'},{f:'salarioBal',t:'Salario',fType:'$'}
		]},$M.Ht.cont);
	}
}

Nom.Ail={
	OLg:function(L){
	var Li=[];
	Li.push({ico:'fa fa-eye',textNode:' Visualizar', P:L, func:function(T){ $Doc.go('nomAil','v',T.P,1); } });
	if(L.canceled=='N'){
		Li.push({k:'edit',ico:'fa fa-pencil',textNode:' Modificar', P:L, func:function(T){ $Doc.go('nomAil','f',T.P,1); } });
		Li.push({k:'attach',ico:'fa fa-paperclip',textNode:' Archivos', P:L, func:function(T){ Attach.openWin({tt:'nomAil',tr:T.P.docEntry, title:'Relación de Archivos: '+T.P.docEntry}) } });
		Li.push({k:'statusN',ico:'fa fa_prio_high',textNode:' Anular Documento', P:L, func:function(T){ $Doc.statusDefine({reqMemo:'N',docEntry:T.P.docEntry,api:Api.Nom.pr+'ail/statusCancel',text:'Se va anular el documento.'}); } });
	}
	return $Opts.add('nomAil',Li,L);;
},
	opts:function(P,pare){
		Li={Li:Nom.Ail.OLg(P.L),PB:P.L,textNode:P.textNode};
		var mnu=$1.Menu.winLiRel(Li);
		if(pare){ pare.appendChild(mnu); }
		return mnu;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Doc.tbList({api:Api.Nom.pr+'ail',inputs:$1.G.filter(),
		fOpts:Nom.Ail.opts,view:'Y',docBy:'userDate',docUpd:'userDate',
		tbSerie:'nomAil',
		TD:[
			{H:'Estado',k:'docStatus',_V:'docStatus'},
			{H:'Fecha',k:'docDate'},
			{H:'Empleado',k:'cardName'},
			{H:'Tipo',k:'docType',_g:$V.nomAilType},
			{H:'Clase',k:'docClass',_g:$JsV.nomAilClass},
			{H:'',fTag:function(D){
				return Attach.btnWin({tt:'nomAil',tr:D.docEntry, title:'Relación de Archivos: '+D.docEntry});
			}}
		],
		tbExport:{ext:'xlsx',fileName:'Accidentes/Incidentes'}
		},cont);
	},
	form:function(){
		Pa=$M.read();
		var cont=$M.Ht.cont; D={};
		$Api.get({f:Api.Nom.pr+'ail/form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,errWrap:cont,loade:cont,func:function(Jr){
			var nd=Nom.Sea.crd(Jr,cont);
			$Doc.form({tbSerie:'N',go:'nomAil',docEdit:Pa.docEntry,cont:cont,POST:Api.Nom.pr+'ail',func:D.func,
			HLs:[
				{divLine:1,L:'Fecha',req:'Y',wxn:'wrapx8',lTag:'date',I:{name:'docDate',value:Jr.docDate}},
				{L:'Empleado',req:'Y',wxn:'wrapx4',Inode:nd},
				{lTag:'select',L:'Sede',wxn:'wrapx8',req:'Y',I:{name:'prp1',opts:$Tb.owsu,selected:Jr.prp1}},
				{L:'Tipo',req:'Y',wxn:'wrapx8',lTag:'select',I:{name:'docType',opts:$V.nomAilType,value:Jr.docType}},
				{L:'Clase',wxn:'wrapx8',lTag:'select',I:{name:'docClass',opts:$JsV.nomAilClass,value:Jr.docClass}},
				{divLine:1,L:'Detalles del evento',wxn:'wrapx1',lTag:'textarea',I:{name:'lineMemo',textNode:Jr.lineMemo}}
			],
			Ls:{D:Jr.L,xMov:'Y',xNum:'N',xDel:'Y',AJs:['id'],
			L:[
				{th:'Tipo',k:'lineType',lTag:'select',opts:$V.nomAilLType},
				{th:'Programada',k:'lineDate',lTag:'date'},
				{th:'Ejecutada',k:'lineDue',lTag:'date'},
				{th:'Actividad',k:'lineMemo',lTag:'textarea'},
				{th:'Responsables',k:'Assgs',lTag:'input'}
			]}
			});
		}});
	},
	view:function(){
	var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:Api.Nom.pr+'ail/view',inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		$Doc.view(cont,{tbSerie:'N',D:Jr,
			btnsTop:{ks:'print,edit,statusN,attach,',icons:'Y',Li:Nom.Ail.OLg},
			THs:[
				{docEntry:1},{docTitle:'Seguimiento de Accidentes/Incidentes',cs:7,ln:1},
				{t:'Fecha',k:'docDate'},{middleInfo:'Y'},{logo:'Y'},
				{t:'Estado',k:'docStatus',_V:'docStatus'},
				{t:'Tipo',k:'docType',_g:$V.nomAilType},
				{k:'licTradType',_V:'licTradType'},{k:'licTradNum',ln:1},{k:'cardName',cs:4,ln:1},{k:'prp1',_g:$Tb.owsu,ln:1,cs:2},
				{k:'docClass',_g:$JsV.nomAilClass,ln:1,cs:2},
				{k:'lineMemo',cs:8,addB:$1.t('b',{textNode:'Detalles del evento:\u0020'}),HTML:1,Tag:{'class':'pre'}},
			],
			mTL:[
			{L:'L',fieldset:'Plan de Acción',tb:{style:'fontSize:14px'},TLs:[
				{t:'Tipo',k:'lineType',_g:$V.nomAilLType},
				{t:'Programada',k:'lineDate'},
				{t:'Ejecutada',k:'lineDue'},
				{t:'Actividad',k:'lineMemo'},
				{t:'Responsables',k:'Assgs'},
			]}
			]
		});
	}});
},
}

Nom.Sea={
	crd:function(D1,wrap){
		var jsF=(D1.jsF)?D1.jsF:$Api.JS.cls;
		return $1.lTag({tag:'apiSeaBox',value:D1.cardName,api:Api.Nom.b+'crd/sea',fieDefAt:wrap,'class':jsF,D:D1,AJsPut:['cardId'],func:D1.func});
	}
}

$Tc._i({kObj:'owsu',
liK:'owsu',liTxtG:'Sucursales',mdlActive:false,liTxtF:'Sucursal (Form)',
Cols:[
{divLine:1,t:'Nombre',k:'value',wxn:'wrapx4',T:{tag:'input'}}
]
});
$JsV._i({kObj:'nomCprRel',mdl:'nom',liK:'nomCprRel',liTxtG:'Parentesco Personas',liTxtF:'Parentesco Persona (Form)'});
$JsV._i({kObj:'nomWorkSede',mdl:'nom',liK:'nomWorkSede',liTxtG:'Sedes de trabajo',liTxtF:'Sede de Trabajo (Form)'});
$JsV._i({kObj:'nomWorkArea',mdl:'nom',liK:'nomWorkArea',liTxtG:'Areas de trabajo',liTxtF:'Area de trabajo (Form)'});
$JsV._i({kObj:'nomWorkPosi',mdl:'nom',liK:'nomWorkPosi',liTxtG:'Cargos Laborales',liTxtF:'Cargo Laboral (Form)'});
$JsV._i({kObj:'nomLeaveRea',mdl:'nom',liK:'nomLeaveRea',liTxtG:'Motivos de Retiro',liTxtF:'Motivo de Retiro (Form)'});
$JsV._i({kObj:'nomAilClass',mdl:'nom',liK:'nomAilClass',liTxtG:'Clases Accidentes/Incidentes',liTxtF:'Clase Accidente/Incidente (Form)'});
$JsV._i({kObj:'nomCapCls',mdl:'nom',liK:'nomCapCls',liTxtG:'Clases Capacitaciones',liTxtF:'Clase Capacitacion (Form)'});