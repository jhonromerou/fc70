$MdlStatus.put('mpaEve','Y');

$M.liAdd('mpa',[
{k:'mpaEve',t:'Eventos', kau:'mpaEve', func:function(){
	$M.Ht.ini({f:'mpaEve',gyp:function(){  Mpa.Eve.xHtml[1]=$M.Ht.cont; Mpa.Eve.get(); }
	});
}}
]);

_Fi['mpaEve']=function(w){
	Mpa.Eve.Fi.F.push(['Contacto',{tag:'input',name:'P.name(E_like3)'}]);
	Mpa.Eve.Fi.F.push(['Socio',{tag:'input',name:'C.cardName(E_like3)'}]);
	Mpa.Eve.profile='A';
	Mpa.Eve.xHtml=[];
	Mpa.Eve.xHtml[0]=w;
	Mpa.Eve.ini({openAlw:'Y'});
}

Mpa.Eve={
profile:false,//A,crd,cpr
gData:['',{}],//&cardId=1
openAlw:'Y',//Abrir siempre
xHtml:[],//0-filt,1-list
ini:function(P){
	var P=(P)?P:{};
	if(Mpa.Eve.openAlw=='Y' || P.openAlw=='Y'){
		Mpa.Eve.Fi.form({cont:Mpa.Eve.xHtml[0]});
		$1.addBtnFas([
		{fa:'fa_doc',textNode:'Nuevo',func:function(){ Mpa.Eve.form(); }}
		],Mpa.Eve.xHtml[0]);
		Mpa.Eve.get();
	}
	Mpa.Eve.openAlw=P.openAlw;
},
Fi:new $Api.Filt({active:'Y',
	func:function(T){ Mpa.Eve.get(T); },
	ordBy:[{k:'doDate',v:'Fecha Inicio'}],
	F:[
		['Asunto',{tag:'input',name:'A.title(E_like3)'}],
		['Responsable',{tag:'userAssg',name:'A.userAssg'}],
		['Tipo',{tag:'select',name:'A.gType',opts:$JsV.mpaEveType}],
		['Prioridad',{tag:'select',name:'A.gPrio',opts:$JsV.mpaEvePrio}],
		['Vencimiento',{TA:[{tag:'date',name:'A.dueDate(E_mayIgual)'},{tag:'date',name:'A.dueDate(E_menIgual)'}]}],
		['Creada',{TA:[{tag:'date',name:'A.dateC(E_mayIgual)'},{tag:'date',name:'A.dateC(E_menIgual)'}]}]
	]
}),
Lg:function(L,men){
	var Li=[]; var n=0;
 Li.push({k:'edit',ico:'fa fa-pencil',textNode:'Modificar',P:L,func:function(T){ Mpa.Eve.form(T.P); }});
	Li= $Opts.add('mpaEve',Li,L);
	if(men){
		Li={Li:Li,PB:L};
		return $1.Menu.winLiRel(Li,men);
	}
	return Li;
},

get:function(T){
	var vPost =Mpa.Eve.Fi.get()+Mpa.Tas.gData[0];
	var wList=Mpa.Eve.xHtml[1];
	$Api.get({f:Api.Mpa.b+'eve',inputs:vPost,btnDisabled:T, loade:wList, func:function(Jr){
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); }
		else{ Mpa.Eve.viewTb(Jr,wList); }
	}
	});
},
viewTb:function(Jr,wList){
	var tb=  $1.T.table(['','Evento','Tipo','Prioridad','Inicia','Finaliza','Responsable','Contacto','Tercero'],{'class':'table_lucid1'},wList);
	var tB=$1.t('tbody',0,tb);
	for(var i in Jr.L){ Mpa.Eve.drawRow(Jr.L[i],tB); }
},
drawRow:function(L,wPare){
	var L=Mpa.Eve.Ht.vD(L);
	var ide='_mpaEve_'+L.gid;
	var tr=$1.uniNode($1.t('tr',{id:ide}),wPare);
	var td=$1.t('td',0,tr);
	Mpa.Eve.Lg(L,td);
	$1.t('td',{textNode:L.title,'class':'mPointer',P:L},tr).onclick=function(){
		Mpa.Eve.view(this.P);
	};
	Mpa.Eve.Ht.type(L,$1.t('td',0,tr));
	Mpa.Eve.Ht.prio(L,$1.t('td',0,tr));
	$1.t('td',{textNode:L.doDateText,xls:{t:L.doDateFull}},tr);
	$1.t('td',{textNode:L.endDateText,xls:{t:L.endDateFull}},tr);
	$1.t('td',{textNode:L.userAssgText},tr);
	$1.t('td',{textNode:L.prsName},tr);
	$1.t('td',{textNode:L.cardName},tr);
},
form:function(P){ var P=(P)?P:{};
	var fP={Win:{title:'Evento'},
	vidn:'gid',gid:P.gid,jsAdd:Mpa.Eve.gData[1],api:Api.Mpa.b+'eve',
	Cols:[
	['Asunto',{k:'title',T:{divLine:1,req:'Y',wxn:'wrapx2',tag:'input'}}],
	['Agendar',{k:'calId',T:{wxn:'wrapx6',tag:'select',opts:$Tb.crmOcal,noBlank:'Y',opt1:{k:0,t:'No'}}}],
	['Tipo',{k:'gType',T:{divLine:1,req:'Y',wxn:'wrapx4',tag:'select',opts:$JsV.mpaEveType}}],
	['Prioridad',{k:'gPrio',T:{wxn:'wrapx4',tag:'select',opts:$JsV.mpaEvePrio}}],
	['Responsable',{k:'userAssg',T:{wxn:'wrapx2',tag:'select',opts:$Tb.ousr}}],
	['Desde',{k:'doDate',T:{kTxt:'value',divLine:1,wxn:'wrapx2',req:'Y',tag:'btnDate',time:'Y'}}],
	['Hasta',{k:'endDate',T:{kTxt:'value',wxn:'wrapx2',req:'Y',tag:'btnDate',time:'Y'}}]
	],
	fCont:function(Jr,pare,Px){
		Mpa.Eve.addCnt(pare);
		if(Jr.iL){
			var nt=100;
			for(var i in Jr.iL){ var L=Jr.iL[i];
				var ki=(L.email+'').replace(/\@/,'').replace(/\./,'');
				var AJs={id:L.id,lineType:L.lineType,email:L.email};
				$Api.boxi({name:'INV',uid:'email_'+ki,line1:L.prsName,lineBlue:L.email,AJs:AJs},pare);
			}
		}
	},
	delCont:'Y',
	oFunc:function(o){
		Mpa.Eve.drawRow(o);
	}
	};
	if(Mpa.Eve.profile=='A' || Mpa.Eve.profile=='crd'){
		fP.Cols.push(['Contacto',{k:'name',T:{divLine:1,wxn:'wrapx2',tag:'cpr'}}]);
	}
	if(Mpa.Eve.profile=='A' || Mpa.Eve.profile=='cpr'){
		fP.Cols.push(['Socio Neg.',{k:'cardName',T:{wxn:'wrapx2',tag:'crd'}}]);
	}
	fP.Cols.push(['Descripción',{k:'shortDesc',T:{divLine:1,req:'Y',wxn:'wrapx1',tag:'textarea'}}]);
	var x=$Api.form(fP);
},
viewGrid:function(T){
	vPost =Mpa.Eve.Fi.get()+Mpa.Eve.gData[0];
	var wList=Mpa.Eve.xHtml[1];
	$Api.get({f:Api.Mpa.b+'eve',inputs:vPost,btnDisabled:T, loade:wList, func:function(Jr){
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L && Jr.L.errNo){ $Api.resp(wList,Jr.L); }
		else{ leer(Jr); }
	},moreWrap:wList,moreFunc:leer
	});
	function leer(Jr){
		if(!Jr.errNo){
			for(var i in Jr.L){
				Mpa.Eve.drawRow(Jr.L[i]);
			}
		}
	}
},
drawRowGrip:function(L){
	L=Mpa.Eve.Ht.vD(L);
	var wList=Mpa.Eve.xHtml[1];
	var ide='_mpaEve_'+L.gid;
	var div=$1.uniNode($1.t('div',{id:ide,style:'border:0.0625rem solid #000; position:relative; padding:0.25rem; margin-bottom:0.5rem;'}),wList);
	$1.t('span',{textNode:L.title,'class':'mPointer _oOpen',P:L},div).onclick=function(){ Mpa.Eve.view(this.P); }
	Mpa.Eve.Ht.edit(L,div);
	Mpa.Eve.Ht.type(L,div);
	Mpa.Eve.Ht.prio(L,div);
	Mpa.Eve.Ht.doEndDate(L,div);
	Mpa.Tas.Ht.userAssg(L,div);
	Mpa.Eve.Ht.profileBot(L,div);
	Mpa.Eve.Ht.nums(L,div);
	Mpa.Eve.Ht.shortDesc(L,div);
},
view:function(P){ P=(P)?P:{};
	var cont=(P.cont)?P.cont:$M.Ht.cont;
	var cont=$1.t('div',0); var jsF='jsFields';
	$Api.get({f:Api.Mpa.b+'eve/'+P.gid,loade:cont,func:function(Jr){
		$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'Tipo',iT:{k:'gType',_JsV:'mpaEveType'}},
		{wxn:'tbf_x4',t:'Prioridad',iT:{k:'gPrio',_JsV:'mpaEvePrio'}},
		{wxn:'tbf_x4',t:'Responsable',iT:{k:'userAssg',_gTb:'ousr'}},
		{line:1,wxn:'tbf_x1',t:'Fecha Evento',v:'De '+$2d.f(Jr.doDate+' '+Jr.doDateAt,'mmm d H:i')+' a '+$2d.f(Jr.endDate+' '+Jr.endDateAt,'mmm d H:i')},
		{line:1,wxn:'tbf_x1',t:'Asunto',v:Jr.title},
		{line:1,wxn:'tbf_x2',t:'Contacto',v:Jr.name},
		{wxn:'tbf_x2',t:'Socio Negocios',v:Jr.cardName},
		{line:1,wxn:'tbf_x1',t:'Detalles',v:Jr.shortDesc,T:{'class':'pre'}},
		],cont,Jr);
		Mpa.Eve.Ht.commAtt(Jr,cont);
	}});
	var bk=$1.Win.open(cont,{winSize:'medium',onBody:1,winTitle:'Evento'});
},
}

Mpa.Eve.Ht={
vD:function(L){
	L.doDateFull=L.doDate+' '+L.doDateAt;
	L.endDateFull=L.endDate+' '+L.endDateAt;
	if(L.gPrio){ L.prioText=_g(L.gPrio,$JsV.mpaEvePrio); }
	var oT=_gO(L.gType,$JsV.mpaEveType);;
	L.typeText=oT.v;
	L.typeCss=(oT.prp1)?'backgroundColor:'+oT.prp1:'';
	L.userAssgText=_g(L.userAssg,$Tb.ousr);
	if(!$2d.is0(L.doDate)){ L.doDateDefine=true;
		L.doDateText=$2d.f(L.doDateFull,'mmm d H:i');
	}
	if(!$2d.is0(L.endDate)){ L.endDateDefine=true;
		L.endDateText=$2d.f(L.endDate+' '+L.endDateAt,'mmm d H:i');
	}
	if(L.prsId>0){ L.prsDefine=true; }
	if(L.cardId>0){ L.cardDefine=true; }
	return L;
},
edit:function(L,div){
	var divR=$1.t('div',{style:'float:right'},div);
	$1.T.btnFa({fa:'fa-pencil',title:'Modificar',P:L,func:function(T){
		Mpa.Eve.form(T.P);
	}},divR);
},
prio:function(L,divT){
	if(L.gPrio>0){
		return $1.t('div',{title:'Prioridad','class':'crm_prpLine fa fa_prio',textNode:L.prioText},divT);
	}
},
type:function(L,divT){
	if(L.gType>0){
		var div=$1.t('div',{textNode:L.typeText,title:'Tipo Evento','class':'crm_prpLine',style:L.typeCss},divT);
		return div;
	}
},
status:function(L,divT){
	if(L.gStatus>0){
	var div=$1.t('div',{textNode:L.statusText,title:'Estado Gestión','class':'crm_prpLine',style:L.statusCss},divT);
	return div;
	}
},
userAssg:function(L,divT){
	if(L.userAssg>0){
		return $1.t('div',{title:'Responsable','class':'fa fa-user crm_prpLine',textNode:L.userAssgText},divT);
	}
},
profileBot:function(L,divP){
	if(Mpa.Eve.profile=='A'){
		Mpa.Eve.Ht.prs(L,divP);
		Mpa.Eve.Ht.crd(L,divP);
	}
	else if(Mpa.Eve.profile=='crd'){
		Mpa.Eve.Ht.prs(L,divP);
	}
	else if(Mpa.Eve.profile=='cpr'){
		Mpa.Eve.Ht.crd(L,divP);
	}
},
shortDesc:function(L,divT){
	if(L.shortDesc && L.shortDesc!=''){
		div= $1.t('div',{textNode:L.shortDesc,style:'padding:0.5rem 0.25rem; background-color:#FFFFBE;'},divT);
		div.onclick=function(){ this.classList.toggle('pre'); }
		return div;
	}
},
nums:function(L,divT){
	if(L.commets>0){
		var div=$1.t('div',{title:'Cantidad de Comentarios','class':'mPointer fa fa-comments crm_prpLine',textNode:L.commets*1},divT);
		div.P=L;
		div.onclick=function(){
			Commt.formLine({load:'Y',tt:'crmNov',tr:this.P.gid,addLine:'Y',vP:{ottc:'Y'},winTitle:'Comentarios'});
		}
		return div;
	}
},
sendCopy:function(L,P){
	var divR=$1.t('div',{style:'float:right'},P.pare);
	$1.T.btnFa({fa:'fa-envelope',title:'Enviar Copia a Correo',P:L,func:function(T){
		Mpa.Eve.sendEvekCopy(T.P);
	}},divR);
},
prs:function(L,divB){
	if(L.prsDefine){
		var lineText=(L.name)?L.name:'Ninguno';
		lineText=(L.prsName)?L.prsName:lineText;
		var div=$1.t('div',{title:'Contacto','class':'fa fa-handshake-o crm_prpLine',textNode:lineText},divB);
		return div;
	}
},
crd:function(L,divB){
	if(L.cardDefine){
		var lineText=(L.cardName)?L.cardName:'Ninguno';
		lineText=(L.cardName)?L.cardName:lineText;
		var div= $1.t('div',{title:'Socio de Negocios','class':'fa fa-institution crm_prpLine',textNode:lineText},divB);

		return div;
	}
},
commAtt:function(L,pare){
	var cFol={tt:'crmNov',tr:L.gid};
	var Pm=[
	{textNode:'','class':'fa fa-comments',active:1,winClass:'win5c'},
	{textNode:'','class':'fa fa_fileUpd',winClass:'win5f', func:function(){ Attach.Tt.get(cFol,_5fw); } },
	{textNode:'','class':'fa fa-info',winClass:'winInfo'}
	];
	var Wins = $1M.tabs(Pm,pare,{w:{style:'margin-top:0.5rem;'}});
	var _5c=Wins['win5c'];
	var _5f=Wins['win5f']
	var inf=Wins['winInfo'];
	//Commt.formLine({load:'Y',tt:'crmNov',tr:L.gid,addLine:'Y',vP:{ottc:'Y'}},_5c);
	//Attach.Tt.form({tt:'crmNov',tr:L.gid, addLine:'Y',func:function(Jrr,o){}},_5f);
	var _5fw=$1.t('div',0,_5f);
	var fc=L.completedAt;
	var fcU=_g(L.completuser,$Tb.ousr);
	if($2d.is0(L.completedAt)){ fc='Sin Completar'; fcU='Sin Completar'; }
	$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'Fecha Creada',v:L.dateC},
		{wxn:'tbf_x4',t:'Creada por',v:_g(L.userId,$Tb.ousr)}
		],inf);
},
doEndDate:function(L,divT){
	if(L.doDateDefine){
		return $1.t('div',{title:'Fecha Evento','class':'fa fa-calendar crm_prpLine',textNode:'De '+L.doDateText+' a '+L.endDateText},divT);
	}
},
};
Mpa.Eve.addCnt=function(pare){
	var ln='INV'; jsF='jsFields';
	var wrap=$1.t('div',0,pare);
	//var opts=[{k:'prs',v:'Contacto'},{k:'user',v:'Usuario'},{k:'m',v:'Manual'}];
	var opts=[{k:'m',v:'Manual'},{k:'prs',v:'Contacto'}];
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'.',I:{tag:'select',sel:{},opts:opts,noBlank:'Y'}},wrap);
	var sel=$1.q('select',divL);
	var node=$1.t('div',{style:'position:relative'});
	$1.T.divL({L:'...',wxn:'wrapx2',Inode:node},divL);
	var n=0;
	var maxiL=20;//no mas de 20
	sel.onchange=function(){ changer(this); }
	function changer(T){ node.innerHTML='';
		if(T.value=='prs'){
			$crd.Fx.inpSeaPrs({vPost:'_fie=A.email',clearInp:'Y',func:function(R,inp){
				var ki=(R.email+'').replace(/\@/,'').replace(/\./,'');
				var AJs={oid:R.prsId,lineType:'prs',email:R.email};
				$Api.boxi({maxi:maxiL,name:'INV',uid:'email_'+ki,line1:R.name,lineBlue:R.email,AJs:AJs},divCn);
			}},node);
		}
		else if(T.value=='m'){
			var inp=$1.t('input',{type:'text',placeholder:'cuenta@gmail.com'},node);
			$1.T.btnFa({fa:'btnRight fa_plusCircle',title:'Añadir',P:inp,func:function(T){
				var ki=(T.P.value+'').replace(/\@/,'').replace(/\./,'');
				var AJs={lineType:'free',email:T.P.value};
				$Api.boxi({maxi:maxiL,name:'INV',uid:'email_'+ki,lineBlue:T.P.value,AJs:AJs},divCn);
				T.P.value='';
			}},node);
		}
	};
	changer({value:'m'});
	var divCn=$1.t('div',{style:'border:0.0625rem solid #EEE;'},wrap);
	return divCn;
}


$JsV._i({kObj:'mpaEveType',mdl:'mpa',liK:'mpaJsv',liTxtG:'Tipo de Eventos',liTxtF:'Tipo Evento (Form)'});
$JsV._i({kObj:'mpaEvePrio',mdl:'mpa',liK:'mpaJsv',liTxtG:'Prioridad de Eventos',liTxtF:'Prioridad Evento (Form)'});
