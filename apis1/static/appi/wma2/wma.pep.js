PeP={}; //MODULE wma.pep
Api.WmaPep={b:'/a/wmaPep/'};

$oB.push($V.docTT,[
{k:'pepAwh',v:'PeP Ajuste Inventario'}
]);

$Doc.Serie['pepEgr']=[{k:1,v:'PeP Sal.'}];
$Doc.Serie['pepIng']=[{k:1,v:'PeP Ing.'}];
$Doc.Serie['pepWht']=[{k:1,v:'PeP Trans.'}];
$Doc.Serie['pepAwh']=[{k:1,v:'PeP Ajuste'}];
$V.docSerieType['pepWht']='PeP Trans.';
$V.docSerieType['pepEgr']='PeP Sal.';
$V.docSerieType['pepIng']='PeP Ing.';
$V.docSerieType['pepAwh']='PeP Ajuste';
$V.docSerieType['pepMov']='PeP Mov.';
$TbV['whsClass']={N:'Normal',E:'Externo',R:'Reproceso',D:'Desecho',O:'Otros'};
$TbV['wfaClass']={N:'Primeras',EC:'En Clasificación',NC:'Sin Clasificar'};
$Doc.a['pepWht']={a:'pepWht.view',docT:'Transferencia Producto en Proceso'};
$Doc.a['pepIng']={a:'pepIng.view',docT:'Ingreso Producto en Proceso'};
$Doc.a['pepEgr']={a:'pepEgr.view',docT:'Salida Producto en Proceso'};
$Doc.a['pepAwh']={a:'pepAwh.view',docT:'Ajuste Producto en Proceso'};
$Doc.a['pepMov']={a:'pepMov.view',docT:'Movimiento Inventario',abbr:'PeP Mov.'};
$V.pepDocType={I:'Interno',E:'Externo'};
$SysEnt.L.push(
{k:'$Tb.whsPeP',v:'Almacen Producto en Proceso',D:{k:'ID Sistema',v:'Almacen',ordBy:'v'}}
);
$i['wmaWfaClass']={t:'Define el tipo de producto en proceso a obtener. Las opciones son las siguientes: <ul><li><b>Normal:</b> Todas las fases de labor normal.</li><li><b>Reproceso:</b> Fase de reproceso, retrabajo o reclasificación (repetición de trabajo ya realizado).<li><b>No Clasificado:</b> Lo que está en proceso, pero no debe tenerse en cuenta porque es producto no conforme o desperdicio.</li></ul>'};

$M.add([
{liA:'wma',folder:'Producto en Proceso',
	L:[
{k:'pepWhs',t:'Inventario Producto en Proceso'},{k:'pepWht.basic',t:'Transferencias'},{k:'pepIng',t:'Ingreso de Producto en Proceso'},{k:'pepEgr',t:'Salida Producto en Proceso'},{k:'pepAwh',t:'Ajuste Producto en Proceso'},
	{k:'pepMov',t:'Movimientos Inventario (PeP)'},
	{k:'pepRep.handAt',t:'Inventario a una Fecha'},
	{k:'pepRep.stockValue',t:'Inventario Valorizado por Fase'},
	{k:'pepRep.dcfValue',t:'Valor Movimiento Doc. Cierre'},
]
},
]);

_Fi['pepWhs']=function(wrap){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', _i:'wmaWfaClass',L:{textNode:'Clase Fase'},I:{tag:'select',sel:{'class':jsV,name:'WFA.wfaClass(E_in)'},opts:$V._wfaClassPdP,noBlank:1}},wrap);
	$1.T.divL({wxn:'wrapx8', L:'Almacen',I:{tag:'select',sel:{'class':jsV,name:'W.whsId(E_igual)'},opts:$Tb.whsPeP}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Externo',subText:'Solo Almacens Externas',I:{tag:'select',sel:{'class':jsV,name:'oW.external(E_igual)'},opts:$V.NY}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Fase',I:{tag:'select',sel:{'class':jsV,name:'W.wfaId(E_igual)'},opts:$Tb.owfa}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Códigos',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:$TXT.itemSize,I:{tag:'select',sel:{'class':jsV,name:'W.itemSzId(E_in)',multiple:'multiple',optNamer:'IN',style:'height:5rem;'},opts:$V.grs1}},divL);
	$1.T.divL({wxn:'wrapx4',L:'Descripción Articulo',I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:PeP.Whs.get});
	wrap.appendChild(btnSend);
};
_Fi['pepWht']=function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', subText:'Fecha Creado',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:{textNode:'Tipo'},I:{tag:'select',sel:{'class':jsV,name:'A.docType(E_igual)'},opts:$V.pepDocType}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Almacen Salida'},I:{tag:'select',sel:{'class':jsV,name:'A.whsIdFrom(E_igual)'},opts:$Tb.whsPeP}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fase'},I:{tag:'select',sel:{'class':jsV,name:'A.wfaIdFrom(E_igual)'},opts:$Tb.owfa}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Almacen Ingreso'},I:{tag:'select',sel:{'class':jsV,name:'A.whsIdTo(E_igual)'},opts:$Tb.whsPeP}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fase Ingreso'},I:{tag:'select',sel:{'class':jsV,name:'A.wfaIdTo(E_igual)'},opts:$Tb.owfa}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:PeP.Wht.get});
	wrap.appendChild(btnSend);
},
_Fi['pepWhs.history']=function(wrap,itemType){
	var jsV = 'jsFiltVars';
	var Pa=$M.read();
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', subText:'Fecha Creado',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'W1.docDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'W1.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Reporte'},I:{tag:'select',sel:{'class':jsV,name:'reportLen'},opts:$V.dbReportLen,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx6',L:'Almacen',I:{tag:'select',sel:{'class':jsV,name:'W1.whsId(E_igual)'},opts:$Tb.whsPeP,selected:Pa.whsId}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Externo',subText:'Solo Almacens Externas',I:{tag:'select',sel:{'class':jsV,name:'oW.external(E_igual)'},opts:$V.NY}},divL);
		$1.T.divL({wxn:'wrapx8', L:'Fase',I:{tag:'select',sel:{'class':jsV,name:'W1.wfaId(E_igual)'},opts:$Tb.owfa,selected:Pa.wfaId}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:{textNode:'Código /s'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'401,501',value:Pa.itemCode}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:$TXT.itemSize},I:{tag:'select',sel:{'class':jsV,name:'W1.itemSzId(E_in)',multiple:'multiple',optNamer:'IN',style:'height:5rem;'},opts:$V.grs1,selected:Pa.itemSzId}},divL);
	$1.T.divL({wxn:'wrapx4', L:{textNode:'Descripción'},I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',O:{vPost___:'I.itemType(E_igual)='+itemType}}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Ids Docs.',I:{tag:'input',type:'text','class':jsV,name:'W1.tr(E_in)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:PeP.Whs.history});

	wrap.appendChild(btnSend);

};
_Fi['pepRep.handAt']=function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', req:'Y',L:'Fecha',I:{tag:'input',type:'date','class':jsV,name:'docDate'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:'Almacen',I:{tag:'select','class':jsV,name:'whsId',opts:$Tb.whsPeP}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Externo',I:{tag:'select','class':jsV,name:'WH.external(E_igual)',opts:$V.NY}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Fase',I:{tag:'select','class':jsV,name:'wfaId',opts:$Tb.owfa}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Código',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:$TXT.itemSize,I:{tag:'select','class':jsV,name:'W.itemSzId(E_in)',multiple:'multiple',optNamer:'IN',style:'height:5rem;',opts:$V.grs1}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:PeP.Rep.handAt});
	wrap.appendChild(btnSend);
};
_Fi['pepRep.stockValue']=function(wrap){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Código',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode'}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Almacen',I:{tag:'select','class':jsV,name:'whsId',opts:$Tb.whsPeP}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Externo',I:{tag:'select','class':jsV,name:'WH.external(E_igual)',opts:$V.NY}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Fase',I:{tag:'select','class':jsV,name:'wfaId',opts:$Tb.owfa}},divL);
	$1.T.divL({wxn:'wrapx8', L:$TXT.itemSize,I:{tag:'select','class':jsV,name:'W.itemSzId(E_in)',multiple:'multiple',optNamer:'IN',style:'height:5rem;',opts:$V.grs1}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:PeP.Rep.stockValue},wrap);
};
_Fi['pepRep.dcfValue']=function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', subText:'Fecha Creado',L:'Fecha Inicio',I:{tag:'input',type:'date','class':jsV,name:'date1'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:'Fecha Corte',I:{tag:'input',type:'date','class':jsV,name:'date2'}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Código',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode'}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Almacen',I:{tag:'select','class':jsV,name:'WH.whsId',opts:$Tb.whsPeP}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Externo',I:{tag:'select','class':jsV,name:'WH.external(E_igual)',opts:$V.NY}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Fase',I:{tag:'select','class':jsV,name:'B.wfaId',opts:$Tb.owfa}},divL);
	$1.T.divL({wxn:'wrapx8', L:$TXT.itemSize,I:{tag:'select','class':jsV,name:'B.itemSzId(E_in)',multiple:'multiple',optNamer:'IN',style:'height:5rem;',opts:$V.grs1}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:PeP.Rep.dcfValue});
	wrap.appendChild(btnSend);
};

$M.li['pepWht']={t:'Transferencias Producto en Proceso', kau:'pepWht.basic', func:function(){
	btnA=$1.T.btnFa({fa:'faBtnCt fa_doc',textNode:'Nueva Transferencia',func:function(){ $M.to('pepWht.form'); }});
	$M.Ht.ini({ fieldset:'Y', func_filt:'pepWht', btnNew:btnA,func_pageAndCont:PeP.Wht.get });
}};
$M.li['pepWht.form']={t:'Transferencia de Producto en Proceso', kau:'pepWht.basic', func:function(){
	$M.Ht.ini({func_cont:PeP.Wht.form });
}};

$M.li['pepWht.view']={noTitle:1, kau:'pepWht.basic', func:function(){
	$M.Ht.ini({func_cont:PeP.Wht.view });
}};
$M.li['pepIng']={t:'Ingreso Producto en Proceso', kau:'pepEgr', func:function(){
	btnA=$1.T.btnFa({faBtn:'fa_doc',textNode:'Nuevo Ingreso',func:function(){ $M.to('pepIng.form'); }});
	$M.Ht.ini({ fieldset:'Y', func_filt:'pepIng', btnNew:btnA,func_pageAndCont:PeP.Ing.get });
}};
$M.li['pepIng.form']={t:'Ingreso de Producto en Proceso', kau:'pepIng', func:function(){
	$M.Ht.ini({func_cont:PeP.Ing.form });
}};
$M.li['pepIng.view']={noTitle:1, kau:'pepIng', func:function(){
	$M.Ht.ini({func_cont:PeP.Ing.view });
}};

$M.li['pepEgr']={t:'Salida Producto en Proceso', kau:'pepEgr', func:function(){
	btnA=$1.T.btnFa({fa:'faBtnCt fa_doc',textNode:'Nueva Salida',func:function(){ $M.to('pepEgr.form'); }});
	$M.Ht.ini({ fieldset:'Y', func_filt:'pepEgr', btnNew:btnA,func_pageAndCont:PeP.Egr.get });
}};
$M.li['pepEgr.form']={t:'Salida de Producto en Proceso', kau:'pepEgr', func:function(){
	$M.Ht.ini({func_cont:PeP.Egr.form });
}};
$M.li['pepEgr.view']={noTitle:1, kau:'pepEgr', func:function(){
	$M.Ht.ini({func_cont:PeP.Egr.view });
}};
$M.li['pepAwh']={t:'Ajuste Producto en Proceso', kau:'pepAwh', func:function(){
	btnA=$1.T.btnFa({faBtn:'fa_doc',textNode:'Nuevo Ajuste',func:function(){ $M.to('pepAwh.form'); }});
	$M.Ht.ini({fieldset:'Y', func_filt:'pepAwh', btnNew:btnA,func_pageAndCont:PeP.Awh.get });
}};
$M.li['pepAwh.form']={t:'Ajuste Producto en Proceso', kau:'pepAwh', func:function(){
	$M.Ht.ini({func_cont:PeP.Awh.form });
}};
$M.li['pepAwh.view']={noTitle:1, kau:'pepAwh', func:function(){
	$M.Ht.ini({func_cont:PeP.Awh.view });
}};
$M.li['pepRep.handAt']={t:'Inventario a una Fecha',kau:'pepRep.handAt', func:function(){
	$M.Ht.ini({func_filt:'pepRep.handAt'});
}};
$M.li['pepRep.stockValue']={t:'Inventario valorizado por fase',kau:'pepRep.stockValue', func:function(){
	$M.Ht.ini({f:'pepRep.stockValue'});
}};
$M.li['pepRep.dcfValue']={t:'Valor Documentos Cierre',kau:'pepRep.dcfValue', d:'Cantidad generada con el costo por fase de los documentos no anulados.', func:function(){
	$M.Ht.ini({f:'pepRep.dcfValue'});
}};

$M.li['pepMov'] = {t:'Movimientos Inventarios (PeP)', kau:'pepMov', func:function(){
	var btn = $1.T.btnFa({faBtn:'fa_doc',textNode:' Nuevo Documento', func:function(){  $M.to('pepMov.form'); }});
	$M.Ht.ini({fieldset:true, fieldDisplay:'Y', func_filt:'pepMov', btnNew:btn,func_pageAndCont:PeP.Mov.get });
}};
$M.li['pepMov.form'] = {t:'Movimiento Inventario',kau:'pepMov',func:function(){ $M.Ht.ini({func_cont:PeP.Mov.form}); }};
$M.li['pepMov.view'] ={t:'-', kau:'pepMov',func:function(){
	$M.Ht.ini({func_cont:PeP.Mov.view});
}};

$M.li['pepWhs'] ={t:'Estado de Stock (PeP)', kau:'pepWhs', func:function(){
	$M.Ht.ini({fieldset:'Y', func_filt:'pepWhs', func_page:PeP.Whs.get});
}};
$M.li['pepWhs.history']={t:'Histórico de Movimientos', kau:'pepWhs', func:function(){
	$M.Ht.ini({fieldset:'Y', func_filt:'pepWhs.history', func_pageAndCont:PeP.Whs.history });
}};
$M.li['pep.lopCat']={t:'Captura por Código', kau:'pepIng', func:function(){
	$M.Ht.ini({func_cont:Wma3.Odp.Lop.cat});
}};

PeP.Wht={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.WmaPep.b+'wht', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['N°','Tipo','Fecha Doc.','Alm. Sal.','Fase Sal.','Alm. Ing.','Fase Ing.','Detalles','Realizado']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				if(i==2){ tr.classList.add($Xls.trNo); }
				var td=$1.t('td',0,tr);
				$Doc.href('pepWht',L,td);
				$1.t('td',{textNode:_g(L.docType,$V.pepDocType)},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d'),xls:{t:L.docDate,style:{format:'dd-mm-yyyy'}}},tr);
				$1.t('td',{textNode:_g(L.whsIdFrom,$Tb.whsPeP)},tr);
				$1.t('td',{textNode:_g(L.wfaIdFrom,$Tb.owfa)},tr);
				$1.t('td',{textNode:_g(L.whsIdTo,$Tb.whsPeP)},tr);
				$1.t('td',{textNode:_g(L.wfaIdTo,$Tb.owfa)},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:$Doc.by('userDate',L)},tr);
			}
			cont.appendChild(tb);
		}
	}});
},
view:function(){
	var contr=$M.Ht.cont; Pa=$M.read();
	$Api.get({f:Api.WmaPep.b+'wht/view', inputs:'docEntry='+Pa.docEntry, loade:contr, func:function(Jr){
		if(Jr.errNo){ $Api.resp(contr,Jr); }
		else{
			var cont=$1.t('div',0,contr);//to Print
			$DocT.B.h({docEntry:Pa.docEntry,dateC:Jr.dateC,serieType:'pepWht',print:'Y',styT:'font-weight:bold;',
			Ls:[{t:'Fecha', v:Jr.docDate},{v:' ',cs:4,ln:1},{t:'Tipo',v:_g(Jr.docType,$V.pepDocType),ln:1},
			{t:'Alm. Salida',v:_g(Jr.whsIdFrom,$Tb.whsPeP)},{t:'Fase Sal.',v:_g(Jr.wfaIdFrom,$Tb.owfa),ln:1},
			{t:'Alm. Ingreso',v:_g(Jr.whsIdTo,$Tb.whsPeP),ln:1},{t:'Fase Ing.',v:_g(Jr.wfaIdTo,$Tb.owfa),ln:1},
			{t:'Detalles',v:Jr.lineMemo,cs:7}
			]
			},cont);

			var tb=$DocT.B.l({pare:cont,softFrom:'Y',tb:['#','Código','Descripción',$TXT.itemSize,'Cant.']});
			var tBody=$1.q('tbody',tb); var n=1;
			for(var i in Jr.L){ L=Jr.L[i];
				$DocT.B.l({Td:[
				n,L.itemCode,L.itemName,_g(L.itemSzId,$V.grs1),L.quantity*1
				]},tBody); n++;
			}
		}
	}});
},
form:function(){
	var Px=$M.Cch();
	var cont=$M.Ht.cont; var jsF=$Api.JS.cls; var n=1;
	var tb=$1.T.table(['#','Código','Descripción',$TXT.itemSize,'Cantidad','']);
	var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Documento'}});
	$Doc.form({cont:cont, jsF:jsF, midCont:fie,itmSea:'prdItem', serieOpts:'pepWht',Jr:{}, POST:Api.WmaPep.b+'wht',func:function(Jr2){ $M.to('pepWht.view','docEntry:'+Jr2.docEntry); },
	Li:[
	{fType:'user'},
	{wxn:'wrapx8',fType:'date',req:'Y',name:'docDate',value:$2d.today},
	{wxn:'wrapx8',req:'Y',L:'Tipo',I:{tag:'select',sel:{name:'docType','class':jsF},opts:$V.pepDocType}},
	{divLine:1,wxn:'wrapx4',req:'Y',L:'Almacen Salida',I:{tag:'select',sel:{name:'whsIdFrom','class':jsF},opts:$Tb.whsPeP}},
	{wxn:'wrapx4',req:'Y',L:'Fase de Salida',I:{tag:'select',sel:{name:'wfaIdFrom','class':jsF},opts:$Tb.owfa}},
	{wxn:'wrapx4',req:'Y',L:'Almacen Entrada',I:{tag:'select',sel:{name:'whsIdTo','class':jsF},opts:$Tb.whsPeP}},
	{wxn:'wrapx4',req:'Y',L:'Fase de Ingreso',I:{tag:'select',sel:{name:'wfaIdTo','class':jsF},opts:$Tb.owfa}},
	{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'input',type:'text',name:'lineMemo','class':jsF}},
	]});
	var tBody= $1.t('tbody',0,tb);
	$Doc.L.itmWinSz(tBody,{priceDefiner:'N',cont:fie,fields:'I.grsId&wh[I.prdItem]=Y', func:function(T,L){
		for(var t in T){ L.itemSzId=t; L.quantity=T[t]; trA(L,tBody); }
	}},fie);
	var n=1; var jsFN=$Api.JS.clsLN;
	function trA(D,tBody){
		var ln='L['+n+']';
		var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
		$1.t('td',{textNode:n},tr); n++;
		$1.t('td',{textNode:D.itemCode},tr);
		$1.t('td',{textNode:D.itemName},tr);
		$1.t('td',{textNode:_g(D.itemSzId,$V.grs1)},tr);
		var td=$1.t('td',0,tr);
		var inpQ=$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsFN,name:'quantity',value:D.quantity},td);
		inpQ.AJs={itemId:D.itemId,itemSzId:D.itemSzId};
			var td=$1.t('td',0,tr);
		$1.T.btnFa({fa:'fa_close',textNode:'Eliminar',func:function(T){ $1.delet(T.parentNode.parentNode); }},td);
	}
	if(Px && Px.L){ for(var i in Px.L){ trA(Px.L[i],tBody); } }
}
}

PeP.Ing={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.WmaPep.b+'ing', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['N°','Tipo','Fecha Doc.','Detalles','Realizado']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				if(i==2){ tr.classList.add($Xls.trNo); }
				var td=$1.t('td',0,tr);
				$Doc.href('pepIng',L,td);
				$1.t('td',{textNode:_g(L.docType,$V.pepDocType)},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d'),xls:{t:L.docDate,style:{format:'dd-mm-yyyy'}}},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:$Doc.by('userDate',L)},tr);
			}
			cont.appendChild(tb);
		}
	}});
},
view:function(){
	var contr=$M.Ht.cont; Pa=$M.read();
	$Api.get({f:Api.WmaPep.b+'ing/view', inputs:'docEntry='+Pa.docEntry, loade:contr, func:function(Jr){
		if(Jr.errNo){ $Api.resp(contr,Jr); }
		else{
			var cont=$1.t('div',0,contr);//to Print
			$DocT.B.h({docEntry:Pa.docEntry,dateC:Jr.dateC,serieType:'pepIng',print:'Y',styT:'font-weight:bold;',
			Ls:[{t:'Fecha', v:Jr.docDate},{v:' ',cs:4,ln:1},{t:'Tipo',v:_g(Jr.docType,$V.pepDocType),ln:1},
			{t:'Detalles',v:Jr.lineMemo,cs:7}
			]
			},cont);

			var tb=$DocT.B.l({pare:cont,softFrom:'Y',tb:['#','Código','Descripción','Almacen','Fase','Cant.']});
			var tBody=$1.q('tbody',tb); var n=1;
			for(var i in Jr.L){ L=Jr.L[i];
				$DocT.B.l({Td:[
				n,Itm.Txt.code(L),Itm.Txt.name(L),_g(L.whsId,$Tb.whsPeP),_g(L.wfaId,$Tb.owfa),L.quantity*1
				]},tBody); n++;
			}
		}
	}});
},
form:function(){
	var Px=$M.Cch();
	var cont=$M.Ht.cont; var jsF=$Api.JS.cls;; var n=1;
	var tb=$1.T.table(['#','Código','Descripción',$TXT.itemSize,'Almacen','Fase','Cantidad','']);
	var midCont=$1.t('div');
	var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Documento'}});
	$Doc.form({cont:cont,midCont:fie, jsF:jsF,itmSea:'prdItem', tbLines:fie, serieOpts:'pepEgr',Jr:{}, POST:Api.WmaPep.b+'ing',func:function(Jr2){ $M.to('pepIng.view','docEntry:'+Jr2.docEntry); },
	Li:[
	{fType:'user'},
	{wxn:'wrapx8',fType:'date',req:'Y',name:'docDate',value:$2d.today},
	{wxn:'wrapx8',req:'Y',L:'Tipo',I:{tag:'select',sel:{name:'docType','class':jsF},opts:$V.pepDocType}},
	{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'input',type:'text',name:'lineMemo','class':jsF}},
	{divLine:1,wxn:'wrapx8',L:'Almacen Por Defecto',I:{tag:'select','class':'whsId',opts:$Tb.whsPeP}},
	{wxn:'wrapx8',L:'Fase Por Defecto',I:{tag:'select','class':'wfaId',opts:$Tb.owfa}}
	]});
	var whsId=$1.q('.whsId',cont);
	var wfaId=$1.q('.wfaId',cont);
	$1.T.btnFa({faBtn:'fa',textNode:'Definir la Almacen y Fase para todas las lineas',func:function(){
		var a=$1.q('._whsL',tb,'all');
		var b=$1.q('._wfaL',tb,'all');
		for(var i=0; i<a.length; i++){
			$1.selDef(whsId.value,a[i]);
			$1.selDef(wfaId.value,b[i]);
		}
	}},midCont);
	var tBody= $1.t('tbody',0,tb);
	$Doc.L.itmWinSz(tBody,{priceDefiner:'N',cont:fie,fields:'I.grsId&wh[I.prdItem]=Y', func:function(T,L){
		for(var t in T){ L.itemSzId=t; L.quantity=T[t]; trA(L,tBody); }
	}},fie);
	var n=1;
	var jsFN=$Api.JS.clsLN;
	function trA(D,tBody){
			var ln='L['+n+']';
			var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
			$1.t('td',{textNode:n},tr); n++;
			$1.t('td',{textNode:D.itemCode},tr);
			$1.t('td',{textNode:D.itemName},tr);
			$1.t('td',{textNode:_g(D.itemSzId,$V.grs1)},tr);
			var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsFN+' _whsL',name:'whsId'},opts:$Tb.whsPeP,selected:whsId.value},td);
			var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsFN+' _wfaL',name:'wfaId'},opts:$Tb.owfa,selected:wfaId.value},td);
			var td=$1.t('td',0,tr);
			var inpQ=$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsFN,name:'quantity',value:D.quantity},td);
			inpQ.AJs={itemId:D.itemId,itemSzId:D.itemSzId};
				var td=$1.t('td',0,tr);
			$1.T.btnFa({fa:'fa_close',textNode:'Eliminar',func:function(T){ $1.delet(T.parentNode.parentNode); }},td);
	}
	if(Px && Px.L){ for(var i in Px.L){ trA(Px.L[i],tBody); } }
}
}
PeP.Egr={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.WmaPep.b+'egr', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['N°','Tipo','Fecha Doc.','Detalles','Realizado']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				if(i==2){ tr.classList.add($Xls.trNo); }
				var td=$1.t('td',0,tr);
				$Doc.href('pepEgr',L,td);
				$1.t('td',{textNode:_g(L.docType,$V.pepDocType)},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d'),xls:{t:L.docDate,style:{format:'dd-mm-yyyy'}}},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:$Doc.by('userDate',L)},tr);
			}
			cont.appendChild(tb);
		}
	}});
},
view:function(){
	var contr=$M.Ht.cont; Pa=$M.read();
	$Api.get({f:Api.WmaPep.b+'egr/view', inputs:'docEntry='+Pa.docEntry, loade:contr, func:function(Jr){
		if(Jr.errNo){ $Api.resp(contr,Jr); }
		else{
			var cont=$1.t('div',0,contr);//to Print
			$DocT.B.h({docEntry:Pa.docEntry,dateC:Jr.dateC,serieType:'pepEgr',print:'Y',styT:'font-weight:bold;',
			Ls:[{t:'Fecha', v:Jr.docDate},{v:' ',cs:4,ln:1},{t:'Tipo',v:_g(Jr.docType,$V.pepDocType),ln:1},
			{t:'Detalles',v:Jr.lineMemo,cs:7}
			]
			},cont);
			var tb=$DocT.B.l({pare:cont,softFrom:'Y',tb:['#','Código','Descripción',$TXT.itemSize,'Almacen','Fase','Cant.']});
			var tBody=$1.q('tbody',tb); var n=1;
			for(var i in Jr.L){ L=Jr.L[i];
				$DocT.B.l({Td:[
				n,L.itemCode,L.itemName,_g(L.itemSzId,$V.grs1),_g(L.whsId,$Tb.whsPeP),_g(L.wfaId,$Tb.owfa),L.quantity*1
				]},tBody); n++;
			}
		}
	}});
},
form:function(){
	var Px=$M.Cch();
	var cont=$M.Ht.cont; var jsF=$Api.JS.cls; var n=1;
	var tb=$1.T.table(['#','Código','Descripción',$TXT.itemSize,'Almacen','Fase','Cantidad','']);
	var midCont=$1.t('div');
	var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Documento'}});
	$Doc.form({cont:cont,midCont:fie, jsF:jsF, tbLines:fie,itmSea:'prdItem', serieOpts:'pepEgr',Jr:{}, POST:Api.WmaPep.b+'egr',func:function(Jr2){ $M.to('pepEgr.view','docEntry:'+Jr2.docEntry); },
	Li:[
	{fType:'user'},
	{wxn:'wrapx8',fType:'date',req:'Y',name:'docDate',value:$2d.today},
	{wxn:'wrapx8',req:'Y',L:'Tipo',I:{tag:'select',name:'docType','class':jsF,opts:$V.pepDocType}},
	{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'input',type:'text',name:'lineMemo','class':jsF}},
	{divLine:1,wxn:'wrapx8',L:'Almacen Por Defecto',I:{tag:'select','class':'whsId',opts:$Tb.whsPeP}},
	{wxn:'wrapx8',L:'Fase Por Defecto',I:{tag:'select','class':'wfaId',opts:$Tb.owfa}}
	]});
	var tBody= $1.t('tbody',0,tb);
	$Doc.L.itmWinSz(tBody,{priceDefiner:'N',cont:fie,fields:'I.grsId&wh[I.prdItem]=Y', func:function(T,L){
		for(var t in T){ L.itemSzId=t; L.quantity=T[t]; trA(L,tBody); }
	}},fie);
	var n=1;
	var jsFN=$Api.JS.clsLN;
	var whsId=$1.q('.whsId',cont);
	var wfaId=$1.q('.wfaId',cont);
	$1.T.btnFa({faBtn:'fa',textNode:'Definir la Almacen y Fase para todas las lineas',func:function(){
		var a=$1.q('._whsL',tb,'all');
		var b=$1.q('._wfaL',tb,'all');
		for(var i=0; i<a.length; i++){
			$1.selDef(whsId.value,a[i]);
			$1.selDef(wfaId.value,b[i]);
		}
	}},midCont);
	function trA(D,tBody){
		var ln='L['+n+']';
		var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
		$1.t('td',{textNode:n},tr); n++;
		$1.t('td',{textNode:D.itemCode},tr);
		$1.t('td',{textNode:D.itemName},tr);
		$1.t('td',{textNode:_g(D.itemSzId,$V.grs1)},tr);
		var td=$1.t('td',0,tr);
		$1.T.sel({sel:{'class':jsFN+' _whsL',name:'whsId'},opts:$Tb.whsPeP,selected:whsId.value},td);
		var td=$1.t('td',0,tr);
		$1.T.sel({sel:{'class':jsFN+' _wfaL',name:'wfaId'},opts:$Tb.owfa,selected:wfaId.value},td);
		var td=$1.t('td',0,tr);
		var inpQ=$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsFN,name:'quantity',value:D.quantity},td);
			inpQ.AJs={itemId:D.itemId,itemSzId:D.itemSzId};
			var td=$1.t('td',0,tr);
		$1.T.btnFa({fa:'fa_close',textNode:'Eliminar',func:function(T){ $1.delet(T.parentNode.parentNode); }},td);
	}
	if(Px && Px.L){ for(var i in Px.L){ trA(Px.L[i],tBody); } }
}
}
/* Salida desde Ivt */
$Opts['ivtCat']=[
{textNode:'Generar Doc. Salida PeP', func:function(L){ PeP.Egr.fromCat(L.P); } }
];
PeP.Egr.fromCat=function(P){
	var cont=$1.t('div');
	$1.t('p',{textNode:'Se realiza un documento de Salida de Producto en Proceso de la Almacen definida, según la fase final de cada artículo registrado en el documento de toma de inventario #'+P.docEntry},cont);
	var jsF=$Api.JS.cls;
	$1.t('input',{type:'hidden','class':jsF,name:'docEntry',value:P.docEntry},cont);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4',req:'Y',L:'Fecha',I:{tag:'input',type:'date',name:'docDate','class':jsF}},cont);
	$1.T.divL({wxn:'wrapx4',req:'Y',L:'Almacen Proceso',I:{tag:'select',name:'whsId','class':jsF,opts:$Tb.whsPeP}},divL);
	$1.T.divL({wxn:'wrapx4',req:'Y',L:'Tipo',I:{tag:'select',name:'docType','class':jsF,opts:$V.pepDocType}},divL);
	$1.T.divL({divLine:1,wxn:'wrapx1',L:'Detalles Documento',I:{tag:'input',type:'text',name:'lineMemo','class':jsF}},cont);
	var resp=$1.t('div',0,cont);
	$Api.send({POST:Api.WmaPep.b+'egr/fromCat',jsBody:cont,func:function(Jr){
		$Api.resp(resp,Jr);
	}},cont);
	$1.Win.open(cont,{winTitle:'Generar Documento Salida',winSize:'medium'});
}

PeP.Awh={
OLg:function(L){
	var Li=[];
	return Li;
},
opts:function(P,pare){
	Li={Li:PeP.Awh.OLg(P.L),PB:P.L,textNode:P.textNode};
	var mnu=$1.Menu.winLiRel(Li);
	if(pare){ pare.appendChild(mnu); }
	return mnu;
},
get:function(){
	var cont=$M.Ht.cont;
	$Doc.tbList({api:Api.WmaPep.b+'awh',inputs:$1.G.filter(),
	fOpts:PeP.Awh.opts,view:'Y',docBy:'userDate',
	tbSerie:'pepAwh',
	TD:[
	{H:'Estado',k:'docStatus',_V:'docStatus'},
	{H:'Fecha',k:'docDate'},
	{H:'Almacen',k:'whsId',_g:$Tb.whsPeP},
	{H:'Fase',k:'wfaId',_g:$Tb.owfa},
	{H:'Detalles',k:'lineMemo'}
	]
	},cont);
},
view:function(){
	var cont=$M.Ht.cont; var Pa=$M.read();
	$Api.get({f:Api.WmaPep.b+'awh/view',inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		var tP={tbSerie:'pepAwh',D:Jr,
			btnsTop:{ks:'print,statusN,logs,viewDac,',icons:'Y',Li:PeP.Awh.OLg},
			THs:[
				{sdocNum:'Y'},{sdocTitle:'Y',cs:5,ln:1},{t:'Estado',k:'docStatus',_V:'docStatus',ln:1},
				{t:'Fecha',k:'docDate'},{middleInfo:'Y'},{logo:'Y'},
				{k:'whsId',_g:$Tb.whsPeP,cs:2},
				{k:'wfaId',_g:$Tb.owfa,cs:2},
				{k:'lineMemo',cs:8,addB:$1.t('b',{textNode:'Detalles:\u0020'}),HTML:1,Tag:{'class':'pre'}},
			],
			mTL:[
			{L:'L',fieldset:'Lineas',tb:{style:'fontSize:14px'},TLs:[
				{t:'Código',k:'itemCode',fText:Itm.Txt.code},
				{t:'Descripción',k:'itemName',fText:Itm.Txt.name},
				{t:'Fisico',k:'quantity',format:'number'},
				{t:'Sistema',k:'onHand',format:'number'},
				{t:'Desviación',k:'diff',format:'number'}
			]}
			]
		};
		$Doc.view(cont,tP);
	}});
},
form:function(){
	var cont=$M.Ht.cont; var jsF=$Api.JS.cls; var n=1;
	$Doc.form({cont:cont, jsF:jsF,itmSea:'prdItem', tbSerie:'pepAwh',Jr:{}, POST:Api.WmaPep.b+'awh',
		HLs:[
		{lTag:'date',wxn:'wrapx8',L:'Fecha',req:'Y',I:{name:'docDate',value:$2d.today}},
		{lTag:'select',wxn:'wrapx8',req:'Y',L:'Tipo',I:{name:'docType',opts:$V.pepDocType}},
		{lTag:'select',wxn:'wrapx8',L:'Almacen',I:{name:'whsId',opts:$Tb.whsPeP}},
		{lTag:'select',wxn:'wrapx8',L:'Fase',I:{name:'wfaId',opts:$Tb.owfa}},
		{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'input',type:'text',name:'lineMemo','class':jsF}},
		],
		tbL:{xNum:'Y',xDel:'Y',docTotal:'Y',uniqLine:'Y',itmSea:'prdItem',bCode:'Y',
			AJs:['itemId','itemSzId'],
			RowsL:[
				['Codigo',{tag:'span',funcText:function(L){ return Itm.Txt.code(L); } }],
				['Descripción',{tag:'span',funcText:function(L){ return Itm.Txt.name(L); } }],
				['Udm',{tag:'span',funcText:function(L){ return _g(L.udm,Udm.O); } }],
				['Cant. Fisica',{tag:'number',k:'quantity',kf:'quantity'}],
			]
		}
	});
	return false;
	var tBody= $1.t('tbody',0,tb);
	$Doc.L.itmWinSz(tBody,{priceDefiner:'N',can0:'Y',cont:fie,fields:'I.grsId&wh[I.prdItem]=Y', func:function(T,L){
		trA(tBody,T,L);
	}},fie);
	var n=1;
	var jsFN=$Api.JS.clsLN;
	function trA(tBody,T,D){
		var whsId=$1.q('.whsId',cont);
		var wfaId=$1.q('.wfaId',cont);
		for(var t in T){
			var ln='L['+n+']';
			var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
			$1.t('td',{textNode:n},tr); n++;
			$1.t('td',{textNode:D.itemCode},tr);
			$1.t('td',{textNode:D.itemName},tr);
			$1.t('td',{textNode:_g(t,$V.grs1)},tr);
			var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsFN,name:'whsId'},opts:$Tb.whsPeP,selected:whsId.value},td);
			var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsFN,name:'wfaId'},opts:$Tb.owfa,selected:wfaId.value},td);
			var td=$1.t('td',0,tr);
			var inpQ=$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsFN,name:'quantity',value:T[t]},td);
				inpQ.AJs={itemId:D.itemId,itemSzId:t};
				var td=$1.t('td',0,tr);
			$1.T.btnFa({fa:'fa_close',textNode:'Eliminar',func:function(T){ $1.delet(T.parentNode.parentNode); }},td);
		}
	}
}
}


PeP.Mov={
opts:function(P){
	var L=P.L; var Jr=P.Jr;
	var Li=[];
	Li.push({ico:'fa fa-eye',textNode:' Visualizar Documento', P:L, func:function(T){ $Doc.href('pepMov',T.P); } });
	return Li={Li:Li,textNode:P.textNode};
},
get:function(cont){
	cont =$M.Ht.cont;
	$Api.get({f:Api.WmaPep.b+'mov', inputs:$1.G.filter(), loade:cont,
	func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb = $1.T.table(['','No.','Estado','Fecha','Detalles','Creado']); cont.appendChild(tb);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var tdB = $1.t('td',0,tr);
				tdB.appendChild($1.Menu.winLiRel(PeP.Mov.opts({L:L})));
				var td = $1.t('td',0,tr);
				$1.t('a',{href:$Doc.href('pepMov',L,'r'),'class':'fa fa_eye',textNode:L.docEntry},td);
				$1.t('td',{textNode:_g(L.docStatus,$V.docStatus)},tr);
				$1.t('td',{textNode:$2d.f(L.docDate,'mmm d')},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:$Doc.by('userDate',L)},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; jsF=$Api.JS.cls;
	var tb=$1.T.table(['','Código','Descripción','Tipo Linea','Almacen','Fase','Cant','']);
	var tBody=$1.t('tbody',0,tb);
	var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Documento'}});
	cont.appendChild(fie);
	$Doc.form({cont:cont, midCont:fie, jsF:jsF,itmSea:'prdItem', middleCont:fie, tbSerie:'N',Jr:{}, POST:Api.WmaPep.b+'mov',func:function(Jr2){ $Doc.href('pepMov',Jr2,'to'); },
	HLs:[
	{divLine:1,wxn:'wrapx8',L:'Fecha',req:'Y',I:{lTag:'date',name:'docDate',value:$2d.today}},
	{wxn:'wrapx4',L:'Tercero',req:'Y',I:{lTag:'crd'}},
	{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'input',type:'text',name:'lineMemo','class':jsF}},
	{divLine:1,wxn:'wrapx8',L:'Bodefa Por Defecto',I:{tag:'select','class':'whsId',opts:$Tb.whsPeP}},
	{wxn:'wrapx8',L:'Fase Por Defecto',I:{tag:'select','class':'wfaId',opts:$Tb.owfa}},
	{wxn:'wrapx8',L:'Tipo por Defecto',I:{tag:'select','class':'typer',opts:$V.ivt_lineType,noBlank:1}}
	],
	tbL:{xNum:'Y',xDel:'Y',itmSea:'prdItem',bCode:'Y',
			RowsL:[
				['Codigo',{tag:'span',funcText:function(L){ return Itm.Txt.code(L); } }],
				['Descripción',{tag:'span',funcText:function(L){ return Itm.Txt.name(L); } }],
				['Tipo Linea',{tag:'select',k:'lineType',kf:'lineType',opts:$V.ivt_lineType, funcText:function(){ return $1.q('.typer',cont).value; } }],
				['Almacen',{tag:'select',k:'whsId',kf:'whsId',opts:$Tb.whsPeP, funcText:function(){ return $1.q('.whsId',cont).value; } }],
				['Fase',{tag:'select',k:'wfaId',kf:'wfaId',opts:$Tb.owfa, funcText:function(){ return $1.q('.wfaId',cont).value; } }],
				['Cant. Total',{tag:'number',k:'quantity',kf:'quantity'}],
			]
		}
	});
	//Itm.Fx.sea2Size({vPost:'fie=I.udm&wh[I.handInv]=Y&wh[I.prdItem]=Y',func:function(Ds){ trA(Ds,tBody); }},fie);
},
view:function(){
	var Pa=$M.read(); var cont=$M.Ht.cont;
	$Api.get({f:Api.WmaPep.b+'mov/view', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); return false; }
			var THs=[];
			var TLs=[{k:'lineNum',textNode:'#'},{k:'itemCode',textNode:'Código',fText:Itm.Txt.code},{k:'itemName',textNode:'Nombre',fText:Itm.Txt.name},{k:'lineType',textNode:'Tipo Linea',_g:$V.ivt_lineType},{k:'whsId',textNode:'Almacen',_g:$Tb.whsPeP},{k:'wfaId',textNode:'Fase',_g:$Tb.owfa},{k:'quantity',textNode:'Cantidad'}]
			$Doc.view(cont,{D:Jr,
			THs:THs,
			TLs:TLs,
			});
	}});
}
}

PeP.Whs={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.WmaPep.b+'whs', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var Lx=[]; var Ta={}; var Lkn={}; var n=0; /* convert */
			for(var i in Jr.L){ L=Jr.L[i];
				L.whsName=_g(L.whsId,$Tb.whsPeP);
				L.wfaName=_g(L.wfaId,$Tb.owfa);
				L.onHand*=1;
				var k=L.whsName+'_'+L.wfaName+'_'+L.itemCode;
				L.k=k;
				Lx.push(L);
			}
			var Tds=tdsLeft(); colspan=Tds.length-1;
			var tb=$1.T.table(Tds);
			var trH=$1.q('thead tr',tb);
			var tBody=$1.t('tbody',0,tb);
			Lx=$js.sortNum(Lx,{k:'k'});
			for(var i in Lx){ var L=Lx[i];
				var tr=$1.t('tr',0,tBody);
				tdsLeft(tr,'line',L)
			}
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:'Total',colspan:colspan},tr);
			$1.t('td',{'class':tbSum.tbColNumTotal+1},tr);
			tbSum.get(tb);
			tb=$1.T.tbExport(tb,{ext:'xlsx',print:'Y',fileName:'Reporte Producto en Proceso'});
			cont.appendChild(tb);
		}
		function tdsLeft(tr,type,L){
			var Tds=[];
			var line=(type=='line');
			if(line){
				var bs='itemCode:'+L.itemCode+',itemSzId:'+L.itemSzId;
				$1.t('a',{'class':'fa faBtn fa_eye',href:$M.to('pepWhs.history','whsId:'+L.whsId+',wfaId:'+L.wfaId+','+bs,'r'),title:'Visualizar historial',textNode:''},$1.t('td',{},tr));
				$1.t('a',{'class':'fa faBtn fa_eye',href:$M.to('pepWhs.history','whsId:'+L.whsId+','+bs,'r'),title:'Visualizar Hhstorial solo de Almacen',textNode:_g(L.whsId,$Tb.whsPeP)},$1.t('td',{},tr));
				$1.t('a',{'class':'fa faBtn fa_eye',href:$M.to('pepWhs.history','wfaId:'+L.wfaId+','+bs,'r'),title:'Visualizar historial solo de fase',textNode:_g(L.wfaId,$Tb.owfa)},$1.t('td',{},tr));
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:_g(L.itemSzId,$V.grs1)},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:L.onHand,'class':tbSum.tbColNums,tbColNum:1},tr);
			}
			else{
				Tds.push('');
				Tds.push('Almacen');
				Tds.push('Fase');
				Tds.push('Código');
					Tds.push($TXT.itemSize);
				Tds.push('Descripción');
				Tds.push('Cant.');
			}
			return Tds;
		}
	}});
},
history:function(){
	cont =$M.Ht.cont;
	$Api.get({f:Api.WmaPep.b+'whs/history', inputs:$1.G.filter(), loade:cont,
	func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['Tipo','Número','Almacen','Fase','Artículo','Descripción',$TXT.itemSize,'Entradas','Salidas','Saldo','Fecha']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var css1=(L.onHandAt<0)?'color:#E00; font-weight:bold;':'';
				var td=$1.t('td',0,tr);
				L.inQty*=1; L.outQty*=1;
				var inQty=(L.inQty!=0)?L.inQty:'';
				var outQty=(L.outQty!=0)?L.outQty:'';
				$Doc.href(L.tt,{docEntry:L.tr},{pare:td,format:'serie'});
				$1.t('td',{textNode:L.tr},tr);
				$1.t('td',{textNode:_g(L.whsId,$Tb.whsPeP)},tr);
				$1.t('td',{textNode:_g(L.wfaId,$Tb.owfa)},tr);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:_g(L.itemSzId,$V.grs1)},tr);
				$1.t('td',{textNode:inQty,style:css1},tr);
				$1.t('td',{textNode:outQty,style:css1},tr);
				$1.t('td',{textNode:L.onHandAt*1,style:css1},tr);
				$1.t('td',{textNode:L.docDate},tr);
			}
			tb=$1.T.tbExport(tb,{ico:'xlsx',fileName:'Reporte de Movimientos PeP'});
			cont.appendChild(tb);
		}
	}});
}

}

PeP.Rep={
handAt:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.WmaPep.b+'rep/handAt', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['Código','Descripción','Almacen','Fase','A la Fecha','Costo Unitario','Costo Total','MP','MO','SV','MA','CIF','+Entradas','-Salidas','A Hoy']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ var L=Jr.L[i];
				var tr=$1.t('tr',{'class':tbCal._row},tBody);
				var handAt=L.onHand*1-L.inQty*1+L.outQty*1;
				$1.t('td',{textNode:Itm.Txt.code(L)},tr);
				$1.t('td',{textNode:Itm.Txt.name(L)},tr);
				$1.t('td',{textNode:_g(L.whsId,$Tb.whsPeP)},tr);
				$1.t('td',{textNode:_g(L.wfaId,$Tb.owfa)},tr);
				$1.t('td',{textNode:handAt,'class':tbCal._cell,ncol:1},tr);
				$1.t('td',{textNode:$Str.money(L.sumCost*1)},tr);
				$1.t('td',{textNode:$Str.money(handAt*L.sumCost*1),'class':tbCal._cell,ncol:2,style:'backgroundColor:#CCC'},tr);
				$1.t('td',{textNode:$Str.money(handAt*L.sumCostMP*1),'class':tbCal._cell,ncol:3},tr);
				$1.t('td',{textNode:$Str.money(handAt*L.sumCostMO*1),'class':tbCal._cell,ncol:4},tr);
				$1.t('td',{textNode:$Str.money(handAt*L.sumCostSV*1),'class':tbCal._cell,ncol:5},tr);
				$1.t('td',{textNode:$Str.money(handAt*L.sumCostMA*1),'class':tbCal._cell,ncol:6},tr);
				$1.t('td',{textNode:$Str.money(handAt*L.sumCif*1),'class':tbCal._cell,ncol:7},tr);
				$1.t('td',{textNode:L.inQty*1},tr);
				$1.t('td',{textNode:L.outQty*1},tr);
				$1.t('td',{textNode:L.onHand*1},tr);
			}
			var tr=$1.t('tr',{'class':tbCal._row},tBody);
			$1.t('td',0,tr); $1.t('td',0,tr); $1.t('td',0,tr); $1.t('td',0,tr);
			$1.t('td',{'class':tbCal._cell+'_1'},tr);
			$1.t('td',0,tr);
			$1.t('td',{'class':tbCal._cell+'_2',format:'$',style:'backgroundColor:#CCC'},tr);
			$1.t('td',{'class':tbCal._cell+'_3',format:'$'},tr);
			$1.t('td',{'class':tbCal._cell+'_4',format:'$'},tr);
			$1.t('td',{'class':tbCal._cell+'_5',format:'$'},tr);
			$1.t('td',{'class':tbCal._cell+'_6',format:'$'},tr);
			$1.t('td',{'class':tbCal._cell+'_7',format:'$'},tr);
			$1.t('td',0,tr); $1.t('td',0,tr); $1.t('td',0,tr);
			tbCal.sumCells(tb);
			tb=$1.T.tbExport(tb,{fileName:'Inventario a una fecha',ext:'xlsx'});
			cont.appendChild(tb);
		}
	}
	});
},
stockValue:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.WmaPep.b+'rep/stockValue', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['Código','Descripción','Fase','Almacen','Costo Promedio','Disponible','Udm','Costo Total','','Costo Estandar','% Desviación']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ var L=Jr.L[i];
				var tr=$1.t('tr',{'class':tbCal._row},tBody);
				var desv=L.avgPrice-L.sumCost;
				css=(desv>0)?'color:red; fontWeight:bold':'';
				$1.t('td',{textNode:Itm.Txt.code(L)},tr);
				$1.t('td',{textNode:Itm.Txt.name(L)},tr);
				$1.t('td',{textNode:_g(L.wfaId,$Tb.owfa)},tr);
				$1.t('td',{textNode:_g(L.whsId,$Tb.whsPeP)},tr);
				$1.t('td',{textNode:$Str.money(L.avgPrice)},tr);
				$1.t('td',{textNode:L.onHand*1,'class':tbCal._cell,ncol:1},tr);
				$1.t('td',{textNode:_g(L.udm,Udm.O)},tr);
				$1.t('td',{textNode:$Str.money(L.stockValue),'class':tbCal._cell,ncol:2},tr);
				$1.t('td',{style:'backgroundColor:#CCC'},tr);
				$1.t('td',{textNode:$Str.money(L.sumCost),'class':tbCal._cell,ncol:3},tr);
				$1.t('td',{textNode:$Str.money(desv),'class':tbCal._cell,ncol:4,style:css},tr);
			}
			var tr=$1.t('tr',{'class':tbCal._row},tBody);
			$1.t('td',0,tr); $1.t('td',0,tr); $1.t('td',0,tr); $1.t('td',0,tr);
			$1.t('td',0,tr);
			$1.t('td',{'class':tbCal._cell+'_1',format:'$'},tr);
			$1.t('td',0,tr);
			$1.t('td',{'class':tbCal._cell+'2',format:'$'},tr);
			$1.t('td',{style:'backgroundColor:#CCC'},tr);
			$1.t('td',{'class':tbCal._cell+'_3',format:'$'},tr);
			$1.t('td',{'class':tbCal._cell+'_4',format:'$'},tr);
			tbCal.sumCells(tb);
			tb=$1.T.tbExport(tb,{fileName:'Inventario Valorizado',ext:'xlsx'});
			cont.appendChild(tb);
		}
	}
	});
},
dcfValue:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.WmaPep.b+'rep/dcfValue', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var Tbs=['Código','Descripción','S/P','Fase','Cantidad','Total','MP','MO','SV','MA','CIF', 'Unitario','MP','MO','SV','MA','CIF'];
			var tb=$1.T.table(Tbs);
			var tBody=$1.t('tbody',0,tb);
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{colspan:5},tr);
			css1='backgroundColor:#DDD';
			$1.t('td',{colspan:6,textNode:'Coste Total',style:css1},tr);
			$1.t('td',{colspan:6,textNode:'Coste Unitario'},tr);
			for(var i in Jr.L){ var L=Jr.L[i];
				var tr=$1.t('tr',{'class':tbCal._row},tBody);
				qty =L.quantity*1;
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:_g(L.itemSzId,$V.grs1)},tr);
				$1.t('td',{textNode:_g(L.wfaId,$Tb.owfa)},tr);
				$1.t('td',{textNode:qty},tr);
				$1.t('td',{textNode:$Str.money(L.sumCost*qty),style:css1},tr);
				$1.t('td',{textNode:$Str.money(L.sumCostMP*qty),style:css1},tr);
				$1.t('td',{textNode:$Str.money(L.sumCostMO*qty),style:css1},tr);
				$1.t('td',{textNode:$Str.money(L.sumCostSV*qty),style:css1},tr);
				$1.t('td',{textNode:$Str.money(L.sumCostMA*qty),style:css1},tr);
				$1.t('td',{textNode:$Str.money(L.sumCif*qty),style:css1},tr);
				$1.t('td',{textNode:$Str.money(L.sumCost)},tr);
				$1.t('td',{textNode:$Str.money(L.sumCostMP)},tr);
				$1.t('td',{textNode:$Str.money(L.sumCostMO)},tr);
				$1.t('td',{textNode:$Str.money(L.sumCostSV)},tr);
				$1.t('td',{textNode:$Str.money(L.sumCostMA)},tr);
				$1.t('td',{textNode:$Str.money(L.sumCif)},tr);
			}
			var tr=$1.t('tr',{'class':tbCal._row},tBody);
			$1.t('td',0,tr); $1.t('td',0,tr); $1.t('td',0,tr); $1.t('td',0,tr); $1.t('td',0,tr);
			$1.t('td',{'class':tbCal._cell+'_1'},tr);
			$1.t('td',{'class':tbCal._cell+'_2',format:'$'},tr);
			tbCal.sumCells(tb);
			tb=$1.T.tbExport(tb,{fileName:'Inventario a una fecha',ext:'xlsx'});
			cont.appendChild(tb);
		}
	}
	});
}
}

$1.xTag['pepWhs']=function(D,pare){
	if(!D.opts){ D.opts=$Tb.itmOwhsPeP; }
	if(!D.name){ D.name='whsId'; }
	var tTag=$1.T.sel(D,pare);
	return tTag;
}
$M.li['itfDT.pepAwh']={t:'Generar Doc. PeP Ajuste Inventario', kau:'itfDT.pepAwh', func:function(){
	$M.Ht.ini({g:function(){
		Itf.DT.form({text:'Documento Ajuste Inventario',api:Api.Wma.pr+'dt/pepAwh',fileName:'Plantilla Ajuste Inventario',descrip:'Se genera un nuevo documento de ajuste de inventario, y se actualizan las cantidades en el inventario solo de los articulos definidos para la bodega y la fase.',
		divL:[
		{divLine:1,L:'Serie',req:'Y',wxn:'wrapx8',I:{tag:'select',name:'serieId',opts:$Tb.docSerie['pepAwh']}},
		{L:'Tipo',req:'Y',wxn:'wrapx8',I:{tag:'select',name:'docType',opts:$V.pepDocType}},
		{L:'Almacen',req:'Y',wxn:'wrapx8',I:{tag:'select',name:'whsId',opts:$Tb.whsPeP}},
		{L:'Fase',req:'Y',wxn:'wrapx8',I:{tag:'select',name:'wfaId',opts:$Tb.owfa}},
		
		],
		Li:[
			{t:'Código Articulo',d:'Código de Artículo'},
			{t:'Código S/P',d:'Código S/P'},
			{t:'Cant.',d:'Cantidad contada'},
		]
		});
	}
	});
}};
