
Api.Wma={b:'/a/wma/',b2:'/a/wma/',js:'/js/wma/'
,pr:'/appi/private/wma/',pu:'/appi/public/wma/',o:''};

$V.wopType=[{k:'I',v:'Interna'},{k:'E',v:'Externa'}];
$V.variType={N:'Ninguna',tcons:'Consumo por Talla',tcode:'Código por Talla'};
$V.faseUdm={minutes:'Minutos',hours:'Horas',seconds:'Segundos',days:'Días'};
$V.itmLog2Type={mpUpdatePrice:'Costo de Materia Prima',model:'Materiales',modelVari:'Materiales (Variantes)',defineCost:'Definición Manual',defineMO:'Mano de Obra',defineCIF:'CIF'};

$js.push($V.gfiAccItmGr,{k:'accPeP',v:'Prod. en Proceso'},true);

$M.kauAssg('wma',[
	{k:'wma.supersu',t:'Administrador Mod. Producción'},
	{k:'wmaBom',t:'Lista  de Materiales'},
]);


$M.liAdd('wma',[
{_lineText:'Producción'},
{k:'wmaFas',t:'Fases Producción', kau:'wma.supersu',mdlActive:'wma',func:function(){
	$M.Ht.ini({btnGo:'wmaFas.form',f:'wmaFas', gyp:Wma.Fas.get});
}},
{k:'wmaFas.form',t:'Formulario de Fase', kau:'wma.supersu',mdlActive:'wma',func:function(){ $M.Ht.ini({g:function(){ Wma.Fas.form(); }}); }},
{k:'wmaWop',t:'Mano de Obra',kau:'wma.supersu',func:function(){
	btn=$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nueva Mano de Obra',func:function(){ $M.to('wmaWop.form'); }});
	$M.Ht.ini({btnNew:btn,f:'wmaWop', gyp:function(){ Wma.Wop.get(); }});
}},
{k:'wmaWop.form',t:'Mano de Obra (Form)',kau:'wma.supersu',mdlActive:'wma', func:function(){ $M.Ht.ini({g:function(){ Wma.Wop.form(); }}); }},
{k:'wmaIsv',t:'Servicios',kau:'wma.supersu',func:function(){
	$M.Ht.ini({btnGo:'wmaIsv.form',f:'wmaWop', gyp:function(){ Wma.Isv.get(); }});
}},
{k:'wmaIsv.form',t:'Servicios (Form)',kau:'wma.supersu', mdlActive:'wma',func:function(){ $M.Ht.ini({g:function(){ Wma.Isv.form(); }}); }},
{k:'wmaCif',t:'CIF',kau:'wma.supersu',mdlActive:'wma',func:function(){
	$M.Ht.ini({btnGo:'wmaCif.form',f:'wmaWop', g:function(){ Wma.Cif.get(); }});
}},
{k:'wmaCif.form',t:'CIF (Form)',kau:'wma.supersu', mdlActive:'wma',func:function(){ $M.Ht.ini({g:function(){ Wma.Cif.form(); }}); }},
{k:'wmaMaq',t:'Máquinas',kau:'wma.supersu',mdlActive:'wma',func:function(){
	$M.Ht.ini({btnGo:'wmaMaq.form',f:'wmaWop', gyp:function(){ Wma.Maq.get(); }});
}},
{k:'wmaMaq.form',t:'Máquina (Form)',kau:'wma.supersu',mdlActive:'wma',func:function(){ $M.Ht.ini({g:function(){ Wma.Maq.form(); }}); }},

{k:'wmaMpg',t:'Fases Articulo', kau:'wma.supersu',mdlActive:'wma', func:function(){
	$M.Ht.ini({f:'wmaMpg',gyp:function(){ Wma.Mpg.get(); }});
}},
{k:'wmaMpg.form',t:'Fases de Artículo (Form)',kau:'wma.supersu',mdlActive:'wma', func:function(){
	$M.Ht.ini({g:function(){ Wma.Mpg.form(); }});
}},

{k:'wmaBom',t:'Lista de Materiales',kau:'wmaBom',mdlActive:'wma',func:function(){
	$M.Ht.ini({f:'wmaBom', gyp:Wma.Bom.get});
}},
{k:'wmaBom.form',t:'Lista de Materiales (Base)',kau:'wmaBom',mdlActive:'wma',d:'Lista de Materiales modelo.', func:function(){
	$M.Ht.ini({g:Wma.Bom.form});
}},
{k:'wmaBom.form2',t:'Lista de Materiales (Variantes)',kau:'wmaBom',mdlActive:'wma',d:'Lista de materiales variante subproducto.', func:function(){ $M.Ht.ini({g:Wma.Bom.form2}); }}
]);

_Fi['wmaFas']=function(wrap){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:'Código',I:{tag:'input',type:'text','class':jsV,name:'wfaCode(E_in)'}},wrap);
	$1.T.divL({wxn:'wrapx6', L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'wfaName(E_like3)',placeholder:'Nombre...'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:Wma.Fas.get},wrap);
};
_Fi['wmaWop']=function(wrap){
	var jsV = 'jsFiltVars';
	var Pa=$M.read('!');
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:'Código',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)'}},wrap);
	$1.T.divL({wxn:'wrapx6', L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	func=Wma.Wop.get;
	if(Pa=='wmaIsv'){ func=Wma.Isv.get; }
	else if(Pa=='wmaMaq'){ func=Wma.Maq.get; }
	else if(Pa=='wmaCif'){ func=Wma.Cif.get; }
	$1.T.btnSend({textNode:'Actualizar', func:func},wrap);
};
_Fi['wmaMpg']=function(wrap){
	var jsV = 'jsFiltVars';
	var Pa=$M.read();
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:'Código',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'101,400',value:Pa.itemCode}},wrap);
	$1.T.divL({wxn:'wrapx6', L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:Wma.Mpg.get},wrap);
};
_Fi['wmaBom']=function(wrap){
	var jsV = 'jsFiltVars';
	var Pa=$M.read();
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', L:'Código',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'101,400',value:Pa.itemCode}},wrap);
	$1.T.divL({wxn:'wrapx6', L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'I.itemName(E_like3)',placeholder:'Nombre...'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:Wma.Bom.get},wrap);
};

var Wma={};
Wma.Wop={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma.pr+'wop', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Grupo','Tipo','Nombre','Coste Und','Coste Prep.','Tiempo','Udm'],0,cont);
			$Doc.IDtd({tb:tb});
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wmaWop.form','itemId:'+T.P.itemId); }},td);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:_g(L.itemGr,$JsV.wmaWopGr)},tr);
				$1.t('td',{textNode:_g(L.prdType,$V.wopType)},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:$Str.money(L.invPrice)},tr);
				$1.t('td',{textNode:$Str.money(L.prdNum1)},tr);
				$1.t('td',{textNode:L.prdNum2*1},tr);
				$1.t('td',{textNode:_g(L.udm,Udm.O)},tr);
				$Doc.IDtd({tr:tr},L.itemId);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:Api.Wma.pr+'wop/form', loadVerif:!Pa.itemId, inputs:'itemId='+Pa.itemId, loade:cont, func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Activo',I:{tag:'select','class':jsF+' __wopId',name:'status',opts:$V.YN,noBlank:1,selected:Jr.status}},cont);
		$1.T.divL({wxn:'wrapx8',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'itemCode',value:Jr.itemCode}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Nombre',I:{tag:'input',type:'text','class':jsF,name:'itemName',value:Jr.itemName}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Tipo',I:{tag:'select','class':jsF,name:'prdType',opts:$V.wopType,noBlank:1,selected:Jr.prdType}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Grupo Operación',aGo:'jsv.wmaWopGr',I:{tag:'select','class':jsF,name:'itemGr',opts:$JsV.wmaWopGr,selected:Jr.itemGr}},divL);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Coste Und',I:{tag:'input',type:'text','class':jsF,name:'invPrice',value:Jr.invPrice,numberformat:'mil'}},cont);
		$1.T.divL({wxn:'wrapx8',L:'Tiempo Und',I:{tag:'input',type:'number',inputmode:'numeric','class':jsF,name:'prdNum2',value:Jr.prdNum2}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Udm',I:{tag:'select','class':jsF,name:'udm',opts:Udm.O,noBlank:1,selected:Jr.udm}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Coste Fijo',I:{tag:'input',type:'text','class':jsF,name:'prdNum1',value:Jr.prdNum1,numberformat:'mil'}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Coste Und (2)',_iHelp:'Coste adicional especial ',I:{tag:'input',type:'text',numberformat:'mil','class':jsF,name:'buyPrice',value:Jr.buyPrice}},divL);
		var wId=$1.q('.__wopId',cont);
		wId.AJs={itemId:Jr.itemId};
		var resp=$1.t('div',0,cont);
		btnS=$Api.send({textNode:'Guardar Información',PUT:Api.Wma.pr+'wop/form', loade:resp, jsBody:cont,func:function(Jr2){
		if(Jr2.itemId){ wId.AJs={itemId:Jr2.itemId};; }
		$Api.resp(resp,Jr2);
		}},cont);
	}});
}
}
Wma.Isv={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma.pr+'isv', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Grupo','Nombre','Coste Und','Coste Prep.','Tiempo','Udm'],0,cont);
			$Doc.IDtd({tb:tb});
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wmaIsv.form','itemId:'+T.P.itemId); }},td);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:_g(L.itemGr,$JsV.wmaWopGr)},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:$Str.money(L.invPrice)},tr);
				$1.t('td',{textNode:$Str.money(L.prdNum1)},tr);
				$1.t('td',{textNode:L.prdNum2*1},tr);
				$1.t('td',{textNode:_g(L.udm,Udm.O)},tr);
				$Doc.IDtd({tr:tr},L.itemId);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:Api.Wma.pr+'isv/form', loadVerif:!Pa.itemId, inputs:'itemId='+Pa.itemId, loade:cont, func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Activo',I:{tag:'select','class':jsF+' __wopId',name:'status',opts:$V.YN,noBlank:1,selected:Jr.status}},cont);
		$1.T.divL({wxn:'wrapx8',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'itemCode',value:Jr.itemCode}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Nombre',I:{tag:'input',type:'text','class':jsF,name:'itemName',value:Jr.itemName}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Tipo',I:{tag:'select',sel:{'class':jsF,name:'prdType'},opts:$V.wopType,noBlank:1,selected:Jr.prdType}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Grupo Operación',aGo:'jsv.wmaWopGr',I:{tag:'select','class':jsF,name:'itemGr',opts:$JsV.wmaWopGr,selected:Jr.itemGr}},divL);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Coste Und',I:{tag:'input',type:'text','class':jsF,name:'invPrice',value:Jr.invPrice,numberformat:'mil'}},cont);
		$1.T.divL({wxn:'wrapx8',L:'Tiempo Und',I:{tag:'input',type:'number',inputmode:'numeric','class':jsF,name:'prdNum2',value:Jr.prdNum2}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Udm',I:{tag:'select','class':jsF,name:'udm',opts:Udm.O,noBlank:1,selected:Jr.udm}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Coste Fijo',I:{tag:'input',type:'text','class':jsF,name:'prdNum1',value:Jr.prdNum1,numberformat:'mil'}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Coste Und (2)',_i:'Coste adicional especial ',I:{tag:'input',type:'text',numberformat:'mil','class':jsF,name:'buyPrice',value:Jr.buyPrice}},divL);
		var wId=$1.q('.__wopId',cont);
		wId.AJs={itemId:Jr.itemId};
		var resp=$1.t('div',0,cont);
		btnS=$Api.send({textNode:'Guardar Información',PUT:Api.Wma.pr+'isv/form', loade:resp, jsBody:cont,func:function(Jr2){
		if(Jr2.itemId){ wId.AJs={itemId:Jr2.itemId};; }
		$Api.resp(resp,Jr2);
		}},cont);
	}});
}
}
Wma.Cif={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma.pr+'cif', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código',,'Nombre','Coste Und','Coste Prep.','Tiempo','Udm'],0,cont);
			$Doc.IDtd({tb:tb});
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wmaCif.form','itemId:'+T.P.itemId); }},td);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:$Str.money(L.invPrice)},tr);
				$1.t('td',{textNode:$Str.money(L.prdNum1)},tr);
				$1.t('td',{textNode:L.prdNum2*1},tr);
				$1.t('td',{textNode:_g(L.udm,Udm.O)},tr);
				$Doc.IDtd({tr:tr},L.itemId);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:Api.Wma.pr+'cif/form', loadVerif:!Pa.itemId, inputs:'itemId='+Pa.itemId, loade:cont, func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Activo',I:{tag:'select','class':jsF+' __wopId',name:'status',opts:$V.YN,noBlank:1,selected:Jr.status}},cont);
		$1.T.divL({wxn:'wrapx8',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'itemCode',value:Jr.itemCode}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Nombre',I:{tag:'input',type:'text','class':jsF,name:'itemName',value:Jr.itemName}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Tipo',I:{tag:'select',sel:{'class':jsF,name:'prdType'},opts:$V.wopType,noBlank:1,selected:Jr.prdType}},divL);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Coste Und',I:{tag:'input',type:'text','class':jsF,name:'invPrice',value:Jr.invPrice,numberformat:'mil'}},cont);
		$1.T.divL({wxn:'wrapx8',L:'Tiempo Und',I:{tag:'input',type:'number',inputmode:'numeric','class':jsF,name:'prdNum2',value:Jr.prdNum2}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Udm',I:{tag:'select','class':jsF,name:'udm',opts:Udm.O,noBlank:1,selected:Jr.udm}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Coste Fijo',I:{tag:'input',type:'text','class':jsF,name:'prdNum1',value:Jr.prdNum1,numberformat:'mil'}},divL);
		var wId=$1.q('.__wopId',cont);
		wId.AJs={itemId:Jr.itemId};
		var resp=$1.t('div',0,cont);
		btnS=$Api.send({textNode:'Guardar Información',PUT:Api.Wma.pr+'cif/form', loade:resp, jsBody:cont,func:function(Jr2){
		if(Jr2.itemId){ wId.AJs={itemId:Jr2.itemId};; }
		$Api.resp(resp,Jr2);
		}},cont);
	}});
}
}
Wma.Maq={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma.pr+'maq', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Nombre','Coste Und','Coste Prep.','Tiempo','Udm']); cont.appendChild(tb);
			$Doc.IDtd({tb:tb});
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wmaMaq.form','itemId:'+T.P.itemId); }},td);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:$Str.money(L.invPrice)},tr);
				$1.t('td',{textNode:$Str.money(L.prdNum1)},tr);
				$1.t('td',{textNode:L.prdNum2*1},tr);
				$1.t('td',{textNode:_g(L.udm,Udm.O)},tr);
				$Doc.IDtd({tr:tr},L.itemId);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:Api.Wma.pr+'maq/form', loadVerif:!Pa.itemId, inputs:'itemId='+Pa.itemId, loade:cont, func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Activo',I:{tag:'select',sel:{'class':jsF+' __wopId',name:'status'},opts:$V.YN,noBlank:1,selected:Jr.status}},cont);
		$1.T.divL({wxn:'wrapx10',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'itemCode',value:Jr.itemCode}},divL);
		$1.T.divL({wxn:'wrapx2',L:'Nombre',I:{tag:'input',type:'text','class':jsF,name:'itemName',value:Jr.itemName}},divL);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Coste Und',I:{tag:'input',type:'text','class':jsF,name:'invPrice',value:Jr.invPrice,numberformat:'mil'}},cont);
		$1.T.divL({wxn:'wrapx8',L:'Tiempo Und',I:{tag:'input',type:'number',inputmode:'numeric','class':jsF,name:'prdNum2',value:Jr.prdNum2}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Udm',I:{tag:'select','class':jsF,name:'udm',opts:Udm.O,noBlank:1,selected:Jr.udm}},divL);
		$1.T.divL({wxn:'wrapx4',L:'Coste Fijo',I:{tag:'input',type:'text','class':jsF,name:'prdNum1',value:Jr.prdNum1,numberformat:'mil'}},divL);
		var wId=$1.q('.__wopId',cont);
		wId.AJs={itemId:Jr.itemId};
		var resp=$1.t('div',0,cont);
		btnS=$Api.send({textNode:'Guardar Información',PUT:Api.Wma.pr+'maq/form', loade:resp, jsBody:cont,func:function(Jr2){
		if(Jr2.itemId){ wId.AJs={itemId:Jr2.itemId}; }
		$Api.resp(resp,Jr2);
		}},cont);
	}});
}
}

Wma.Fas={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma.pr+'fas', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Nombre','Descripción']); cont.appendChild(tb);
			$Doc.IDtd({tb:tb,p:2});
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				btnA=$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wmaFas.form','wfaId:'+T.P.wfaId); }},td);
				$1.t('td',{textNode:L.wfaCode},tr);
				$1.t('td',{textNode:L.wfaName},tr);
				$1.t('td',{textNode:L.descrip},tr);
				$Doc.IDtd({tr:tr,p:2},L.wfaId);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:Api.Wma.pr+'fas/form', loadVerif:!Pa.wfaId, inputs:'wfaId='+Pa.wfaId, loade:cont, func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Activo',I:{tag:'select',sel:{'class':jsF+' __wfaId',name:'active'},opts:$V.YN,noBlank:1,selected:Jr.active}},cont);
		$1.T.divL({wxn:'wrapx10',L:'Código',I:{tag:'input',type:'text','class':jsF,name:'wfaCode',value:Jr.wfaCode}},divL);
		$1.T.divL({wxn:'wrapx2',L:'Nombre',I:{tag:'input',type:'text','class':jsF,name:'wfaName',value:Jr.wfaName}},divL);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',I:{tag:'input',type:'text','class':jsF,name:'descrip',value:Jr.descrip}},cont);
		var wId=$1.q('.__wfaId',cont);
		wId.AJs={wfaId:Jr.wfaId};
		var resp=$1.t('div',0,cont);
	$Api.send({textNode:'Guardar Información',PUT:Api.Wma.pr+'fas', loade:resp, jsBody:cont,func:function(Jr2){
		if(Jr2.wfaId){ wId.AJs={wfaId:Jr2.wfaId}; }
		$Api.resp(resp,Jr2);
		}},cont);
	}});
}
}

Wma.Mpg={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma.pr+'mpg', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Nombre','Coste Total',{textNode:'$ M.P',_iHelp:'Coste Materiales y Semielaborados'},{textNode:'$ M.O',_iHelp:'Coste de Operaciones'},{textNode:'$ S.V',_iHelp:'Coste de Servicios'},{textNode:'$ M.A',_iHelp:'Coste Máquinaria'},{textNode:'$ CIF',_iHelp:'Otros Costes'},'Tiempo','Udm','Actualizado'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.T.btnFa({fa:'fa fa_pencil',textNode:' Modificar', P:L, func:function(T){ $M.to('wmaMpg.form','itemId:'+T.P.itemId); }},td);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:$Str.money(L.cost)},tr);
				$1.t('td',{textNode:$Str.money(L.costMP)},tr);
				$1.t('td',{textNode:$Str.money(L.costMO)},tr);
				$1.t('td',{textNode:$Str.money(L.costSV)},tr);
				$1.t('td',{textNode:$Str.money(L.costMA)},tr);
				$1.t('td',{textNode:$Str.money(L.cif)},tr);
				$1.t('td',{textNode:L.faseTime*1},tr);
				$1.t('td',{textNode:_g(L.faseUdm,$V.faseUdm)},tr);
				$1.t('td',{textNode:$2d.f(L.dateUpd,'mmm d H:iam')},tr);
			}
		}
	}});
},
form:function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:Api.Wma.pr+'mpg/form', loadVerif:!Pa.itemId, inputs:'itemId='+Pa.itemId, loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		var divL=$1.T.divL({divLine:1,wxn:'wrapx2',L:'Artículo',I:{tag:'input',type:'text',value:Jr.itemName,disabled:'disabled'}},cont);
		$1.T.divL({wxn:'wrapx10',L:'Udm Tiempo',I:{tag:'select',sel:{'class':jsF+' __itemId',name:'faseUdm'},opts:$V.faseUdm,noBlank:1,selected:Jr.faseUdm}},divL);
		var wId=$1.q('.__itemId',cont);
		wId.AJs={itemId:Pa.itemId};
		var Pm=[
	{textNode:'Fases','class':'fa fa_priv_members active',active:1,winClass:'wfaOper'}];
	var Tabs = $1M.tabs(Pm,cont,{w:{style:'margin-top:0.5rem;'}});
		var wfaOper=Tabs.wfaOper
		if(Jr.L && Jr.L.errNo){ $Api.resp(wfaOper,JrL); }
		var inf=$1.t('div',{'class':'input_ok',style:'font-weight:normal; font-size:inherit'},wfaOper);
		$1.t('span',{'class':'fa fa-info'},inf);
		$1.t('span',{},inf).innerHTML = '<ul><li><u>bodega def.</u> es donde ingresará por defecto el componente nuevo.</li><li>La <u>Bod. Componentes</u> es desde donde se obtienen los componentes en la <u>fase anterior</u> a la que se desea realizar.</li></ul>';
		var tb=$1.T.table(['','Fase',{textNode:'Exceder',_iHelp:'Permite que la fase permita más cantidades de la planificada'},'Almacen Def.','Bod. Componentes','Comentarios']);
		wfaOper.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		var tF=$1.t('tfoot',0,tb);
		var trF=$1.t('tr',0,tF);
		var td=$1.t('td',{colspan:7},trF);
		$1.T.btnFa({fa:'fa fa_plusCircle',textNode:'Añadir Linea', func:function(){ trOpet(tBody,{}); ni++; }},td);
		var ni=1;
		function trOpet(tBody,L){
			var jsF=$Api.JS.clsLN;
			var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
			var td=$1.t('td',0,tr);
			$1.T.btnFa({fa:'fa-caret-up',title:'Poner Arriba',func:function(T){
			$1.Move.to('before',T.parentNode.parentNode,{rev:'Y',func:function(trAnt){}});
			}},td);
			$1.T.btnFa({fa:'fa-caret-down',title:'Poner Abajo',func:function(T){
			$1.Move.to('next',T.parentNode.parentNode,{rev:'Y',func:function(trAnt){}});
			}},td);
			var td=$1.t('td',0,tr);
			var sel=$1.T.sel({'class':jsF+' __wfaId',name:'wfaId',opts:$Tb.owfa,selected:L.wfaId},td);
			if(L.id){
				var q=$1.q('.__wfaId',td);
				q.AJs={id:L.id};
			}
				var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsF,name:'canExcess'},opts:$V.NY,selected:L.canExcess,noBlank:1},td);
			var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsF,name:'whsId'},opts:$Tb.whsPeP,selected:L.whsId},td);
			var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsF,name:'whsIdBef'},opts:$Tb.whsPeP,selected:L.whsIdBef},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF,name:'lineMemo',value:L.lineMemo},td);
			var td=$1.t('td',0,tr);
			$1.T.ckLabel({t:'Quitar',I:{'class':jsF+' checkSel_trash',name:'delete'}},td);
		}
		for(var i in Jr.L){ ni=Jr.L[i].lineNum;
			trOpet(tBody,Jr.L[i]); ni++;
		}
		$Tol.tbSum(tb);
		var resp=$1.t('div',0,cont);
		$Api.send({PUT:Api.Wma.pr+'mpg', loade:resp, jsBody:cont,func:function(Jr2){
		$Api.resp(resp,Jr2);
		if(!Jr2.errNo){ Wma.Mpg.form(); }
		}},cont);
	}});
},
}
Wma.Bom={
fases:function(P,func,cont){
	vGet='itemId='+P.itemId+'&itemSzId='+P.itemSzId;
	if(P.wfaId){ vGet +='&wfaId='+P.wfaId;}
	$Api.get({f:Api.Wma.pr+'bom/fases',inputs:vGet,loade:cont,func:func});
},
get:function(){
	cont =$M.Ht.cont;
	$Api.get({f:Api.Wma.pr+'bom', inputs:$1.G.filter(), loade:cont,
	func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb = $1.T.table(['','Código','Nombre','UdM','Tipo'],0,cont);
			var tBody = $1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr = $1.t('tr',0,tBody);
				var td = $1.t('td',0,tr);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:_g(L.udm,$V.Udm)},tr);
				$1.t('td',{textNode:_g(L.itemType,$V.itemType)},tr);
				$1.t('td',{textNode:_g(L.itemGr,ty)},tr);;
				$1.Menu.winLiRel({Li:[
					{ico:'fa fa-pencil',textNode:' Composición Base', P:L, func:function(T){ $M.to('wmaBom.form','itemId:'+T.P.itemId); } },
					{ico:'fa fa_cells',textNode:' Composición de Variantes', P:L, func:function(T){ $M.to('wmaBom.form2','itemId:'+T.P.itemId); } },
				]},td);
			};
		}
	}});
},
form:function(){
	cont =$M.Ht.cont; Pa=$M.read();
	var jsF=$Api.JS.clsLN; n=1;
	$Api.get({f:Api.Wma.pr+'bom/form', errWrap:cont, loade:cont, loadVerif:!Pa.itemId ,inputs:'itemId='+Pa.itemId, func:function(Jr){
		$1.t('h3',{textNode:Jr.itemCode+') '+Jr.itemName},cont);
		var tb=$1.T.table(['','Tipo',{textNode:'Material',colspan:2},'Costo','UdM','Consumo','Total','Pieza','Variante',''],0,cont);
		var tBody=$1.t('tbody',0,tb);
		var inp=$Api.JS.addF({name:'itemId',value:Pa.itemId},cont);
		inp.AJs={WFA:{}};
			var i_WFA=(Jr._WFA)?Jr._WFA:{}; var wfaAnt=0;
		if(Jr.L.errNo==1){ $Api.resp(tb,Jr.L); }
		var nx=0;
		/* trFase tiene en cuenta trFaseSiguiente para añadir antes de */
		for(var i in i_WFA){ var L=i_WFA[i];
			inp.AJs.WFA[L.k]={wfaId:L.k,wfaOrder:L.wfaOrder};
			if(nx==0){ nx=1;
				var tr=$1.t('tr',{'wfaId':L.k,'_1moveLimits':'top'},tBody);
			}
			else{
				var tr=$1.t('tr',{'wfaId':L.k,'wfaIdBef':wfaAnt,'class':'__wmaBomFase_'+wfaAnt},tBody);
			}
			var wfaAnt=L.k;
			var td=$1.t('td',{textNode:L.v,colspan:10,style:'backgroundColor:#CCC;'},tr);
			$1.T.btnFa({fa:'fa_cells', textNode:'Añadir Linea',P:{k:L.k},func:function(T){
				Wma.Bom._trMP({wfaId:T.P.k},{n:n,tBody:tBody,jsF:jsF,tr:$1.q('.__wmaBomFase_'+T.P.k)}); n++;
			} },td);
		}
		if(!Jr.L.errNo){ for(var i in Jr.L){ var L=Jr.L[i];
				Wma.Bom._trMP(L,{n:n,tBody:tBody,jsF:jsF,tr:$1.q('.__wmaBomFase_'+L.wfaId)}); n++;
			}
		}
		var tfoot = $1.t('tfoot',0,tb);
		var tr=$1.t('tr',0,tfoot);
		$1.t('td',{colspan:6,textNode:'Total'},tr);
		$1.t('td',{},tr);
		$1.t('td',{'class':'__tbTotal',vformat:'money'},tr);
		var resp=$1.t('div',0,cont);
		$Api.send({textNode:'Guardar Información',PUT:Api.Wma.pr+'bom/form', loade:resp, jsBody:cont, func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ Wma.Bom.form(); }
		} },cont);
		$Tol.tbSum(tBody.parentNode);
	}});
},
form2:function(){
	cont =$M.Ht.cont; Pa=$M.read();
	n=1;
	var jsF=$Api.JS.clsLN; var ni = 1;
	$Api.get({f:Api.Wma.pr+'bom/form2', errWrap:cont, loade:cont, loadVerif:!Pa.itemId ,inputs:'itemId='+Pa.itemId, func:function(Jr){
		$1.t('h3',{textNode:Jr.itemCode+') '+Jr.itemName},cont);
		var tb=$1.T.table(['#','Pieza',{textNode:'Material',colspan:2},'Costo','UdM','Consumo','Total'],0,cont)
		var trH=$1.q('thead tr',tb); trH.classList.add('trHead');
		var Ta=$V.grs2[Jr.grsId];
		$1.t('td',{textNode:'Variante'},trH);
		var trs=1;
		for(var t in Ta){ $1.t('td',{textNode:_g(t,$V.grs1)},trH); }
		var tBody=$1.t('tbody',0,tb);
		if(Jr.L.errNo==1){ $Api.resp(tb,Jr.L); }
		else if(!Jr.L.errNo){
			for(var i in Jr.L){ L=Jr.L[i];
				if(trs==5){ tBody.appendChild(trH.cloneNode(1)); trs=1; } trs++;
				var tr=$1.t('tr',0,tBody);
				var csp=1;
				$1.t('td',{textNode:n,rowspan:csp},tr); n++;
				$1.t('td',{textNode:L.pieceName,rowspan:csp},tr);
				$1.t('td',{textNode:L.itemCode,rowspan:csp},tr);
				$1.t('td',{textNode:L.itemName,rowspan:csp},tr);
				$1.t('td',{textNode:L.buyPrice,pformat:'money',rowspan:csp},tr);
				$1.t('td',{textNode:L.udm,rowspan:csp},tr);
				$1.t('td',{textNode:L.quantity,pformat:'float_2',rowspan:csp},tr);
				$1.t('td',{textNode:L.lineTotal,pformat:'money',rowspan:csp},tr);
				$1.t('td',{textNode:$V.variType[L.variType],rowspan:csp},tr);
				trTabl(L,tr,ni); ni++;
			}
		}
		var resp=$1.t('div',0,cont);
		$Api.send({textNode:'Guardar Información',PUT:Api.Wma.pr+'bom/form2', loade:resp, jsBody:cont, func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ Wma.Bom.form2(); }
		}},cont);
		function trTabl(Ld,tr,ni){
			for(var ta in Ta){
				var L = (Ld && Ld.TA && Ld.TA[ta])?Ld.TA[ta]:{};
				var tdDiv=$1.t('div',{'class':jsF},$1.t('td',{'class':$Api.JS.clsL},tr));
				var inpc=$1.t('input',{type:'number',inputmode:'numeric',min:0,style:'width:4rem; display:block; margin:3px 0;','class':jsF+' __inpTalla',name:'quantity',value:L.quantity},tdDiv);
				inpc.P=L;
				if(Ld.variType=='tcode'){
					inpc.AJs={lineNum:ni,fatherId:Ld.id,itemSzId:ta,buyPrice:L.buyPrice,citemId:L.citemId,citemSzId:L.itemSzId,pieceName:Ld.pieceName};
					inpc.ta=ta;
					if(L.id){ inpc.AJs.id=L.id; }
					var itex=(L.citemId)?Itm.Txt.name({itemName:L.itemName,itemSzId:L.citemSzId}):'';
					var cs=$1.lTag({tag:'apiSeaBox',api:Api.Itm.b+'sea/itemSz',vSea:'I.itemType(E_in)=MP,SE&fie=I.buyPrice',value:itex,func:function(R,inp){
						var inpc=$1.q('.__inpTalla',inp.pare);
						inp.value=Itm.Txt.name({itemName:R.itemName,itemSzId:R.itemSzId});
						inpc.AJs={lineNum:ni,fatherId:Ld.id,itemSzId:inpc.ta,buyPrice:R.buyPrice,citemId:R.itemId,citemSzId:R.itemSzId,pieceName:Ld.pieceName};
						if(inpc.P.id){ inpc.AJs.id=inpc.P.id; }
						$Sea.replaceData(R,inp.pare.parentNode.parentNode);
					}},tdDiv);
					tdDiv.insertBefore(cs,inpc);
				}
				else{
					inpc.AJs={lineNum:ni,itemId:Ld.itemId,fatherId:Ld.id,itemSzId:ta,buyPrice:Ld.buyPrice,citemId:Ld.citemId,citemSzId:L.citemSzId,pieceName:Ld.pieceName};
					if(L.id){ inpc.AJs.id=L.id; }
				}
				if(L.id){
					$1.T.ckLabel({id:'varId_'+L.id,t:'Borrar',I:{'class':'checkSel_trash '+jsF,name:'delete'}},tdDiv);
				}
			}
		}
	}});
},
_trMP:function(L,P){
		$1.nullBlank=''; var jsF=P.jsF;
		var ln='L['+P.n+']';
		var tr=$1.t('tr',{'class':$Api.JS.clsL});
		tr.setAttribute('wfaDef',L.wfaId);
		P.tBody.insertBefore(tr,P.tr);
		var td0=$1.t('td',{'class':jsF},tr);
		td0.setAttribute('wfaDef',L.wfaId);
		td0.AJs={wfaId:L.wfaId}
		$1.T.btnFa({fa:'fa-caret-up',title:'Poner Arriba',func:function(T){
			$1.Move.to('before',T.parentNode.parentNode,{rev:'Y',func:function(trAnt){
				var wfa=trAnt.getAttribute('wfaIdBef');
				var td0=tr.childNodes[0];
				tr.setAttribute('wfaDef',wfa);
				td0.setAttribute('wfaDef',wfa);
				if(wfa){ td0.AJs={wfaId:wfa} }
			}});
		}},td0);
		$1.T.btnFa({fa:'fa-caret-down',title:'Poner Abajo',func:function(T){
			$1.Move.to('next',T.parentNode.parentNode,{rev:'Y',func:function(trAnt){
				var wfa=trAnt.getAttribute('wfaId');
				var td0=tr.childNodes[0];
				tr.setAttribute('wfaDef',wfa);
				td0.setAttribute('wfaDef',wfa);
				if(wfa){ td0.AJs={wfaId:wfa} }
			}});
		}},td0);
		$1.t('td',{textNode:L.lineType,'class':$Sea.clsNameInp,k:'itemType'},tr);
		var AJs=(L.udm)?{lineType:L.lineType,citemId:L.citemId,citemSzId:L.citemSzId,buyPrice:L.buyPrice}:{};
		if(L.id){ AJs.id=L.id; }
		var td=$1.t('td',{'class':$Sea.clsName,k:'itemCode',style:'width:6rem;',textNode:L.itemCode},tr);//itemCode
		var td=$1.t('td',0,tr);
		var inpText=(L.udm)?Itm.Txt.name({itemName:L.itemName,itemSzId:L.citemSzId}):'';
		/* ojo estoy usand invPrice */
		$Api.Sea.input({api:Api.Itm.b+'sea/itemSz',vPost:'I.itemType(E_in)=MP,SE,MA,MO,SV,CIF&fie=I.udm,I.invPrice,I.itemType',lineTfunc:Itm.Txt.name,value:inpText,func:function(R,inp){
			var tTr=inp.pare.parentNode;
			inp.value =Itm.Txt.name({itemName:R.itemName,itemSzId:R.citemSzId});
			var AJs=(R.itemId)?{lineType:R.itemType,citemId:R.itemId,citemSzId:R.itemSzId,buyPrice:R.invPrice}:{};
			if(L.id){ AJs.id=L.id; }
			inpQty.AJs = AJs;
			R.udm=_g(R.udm,Udm.O);
			R.buyPrice=R.invPrice;
			$Sea.replaceData(R,tTr);
			if(!inpQty.value){ inpQty.value=1; $Tol.tbSum(P.tBody.parentNode); }
			Wma.Bom._trCambia(R,tTr);
		}},td);
		var td=$1.t('td',{'class':$Sea.clsNameInp+' __tdNum',k:'buyPrice',kformat:'money',style:'width:6rem;',textNode:$Str.money(L.buyPrice)},tr);
		var td=$1.t('td',{'class':$Sea.clsNameInp,k:'udm',style:'width:4rem',textNode:_g(L.udm,Udm.O)},tr);
		var td=$1.t('td',0,tr);
		var inpQty= $1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' __tdNum2',name:'quantity',style:'width:6rem;',value:L.quantity*1},td);
		inpQty.AJs=AJs;
		inpQty.onkeyup = inpQty.onchange = function(){ $Tol.tbSum(P.tBody.parentNode); }
		var td=$1.t('td',{'class':'__tdTotal',style:'width:6rem;',vformat:'money',textNode:$Str.money(L.lineTotal)},tr);
		var td=$1.t('td',0,tr);
		var tdPie=$1.t('div',{'class':'wmaBomPie'},td);
		$1.t('input',{type:'text','class':jsF,name:'pieceName',value:L.pieceName},tdPie);
		var td=$1.t('td',0,tr);
		var tdVari=$1.t('div',{'class':'wmaBomVari'},td);
		var sel=$1.T.sel({sel:{'class':jsF,name:'variType'},opts:$V.variType,selected:L.variType,noBlank:1}); tdVari.appendChild(sel);
		
		Wma.Bom._trCambia(L,tr);
		var td=$1.t('td',0,tr);
		if(L.id){
			$1.T.ckLabel({id:'varId_'+L.id,t:'Borrar',I:{'class':'checkSel_trash '+jsF,name:'delete'}},td);
		}
		else{ var btn =$1.T.btnFa({fa:'fa_close',textNode:'Quitar',func:function(){ $1.delet(tr); }}); td.appendChild(btn); }
		n++; $1.nullBlank=false;
	},
	_trCambia:function(L,tr){
		var lineType=(L.itemType)?L.itemType:L.lineType;
		var tdPie=$1.q('.wmaBomPie',tr);
		var tdVari=$1.q('.wmaBomVari',tr);
		if(lineType=='MP' || lineType=='SE'){
			tdPie.style.display='block'; tdVari.style.display='block';
		}
		else{ tdPie.style.display='none'; tdVari.style.display='none'; }
	}
}

$1.xTag['wmaWfa']=function(D,pare){
	if(!D.opts){ D.opts=$Tb.owfa; }
	if(!D.name){ D.name='wfaId'; }
	var tTag=$1.T.sel(D,pare);
	return tTag;
}
Wma.Ht={
	bomFases:function(pare,P){/* tabla con fases y opcion añadir itt */
		var P=(P)?P:{};
		if(P.reset=='Y'){
			$1.q('.wmaBomFasesWrap',pare).innerHTML='';
			if(!P.L){ return true; }
		}
		var tb=$1.T.fieset({L:{textNode:'Fases de Producción'}},pare);
		var tBody=$1.t('div',{'class':'wmaBomFasesWrap'},tb);
		var tFoot=$1.t('div',0,tb);
		var td=$1.t('td',{colspan:2},$1.t('tr',0,tFoot));
		var tsel=$1.T.sel({opts:$Tb.owfa},td);
		$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Fase',func:function(){
			tOpt=$1.gVal(tsel,'tag');
			trWfa({wfaId:tOpt.value});
		}},td);
		if(P.L){
			var LF={};
			if(P.LF && !P.LF.errNo){
				for(var i in P.LF){
					var k=P.LF[i].wfaId;
					if(!LF[k]){ LF[k]=[]; }
					LF[k].push(P.LF[i]);
				}
			}
			for(var i in P.L){ trWfa(P.L[i],LF[P.L[i].wfaId]); }
		}
		function trWfa(L,LF){
			if(!L.wfaId){ return false; }
			var kcls='_wfa_'+L.wfaId;
			var tr=$1.t('div',{'class':kcls+' '+$Api.JS.clsL,style:'border:1px solid #EEE; border-radius:5px; padding:5px; margin-bottom:8px; background-color:#EEE;'});
			if($1.uniRow(kcls,tBody,tr)){
				var div1=$1.t('div',0,tr);
				if(P.itt!='N'){
					var div2=$1.t('div',{style:'background-color:#FFF; border-radius:5px; padding:3px; margin:3px 0 3px 20px;'},tr);
					var tb2=Wma.Ht.bomTr('ini',div2,{iFase:iFase});
					var tBody2=$1.t('tbody',0,tb2);
					$1.T.btnFa({fa:'fa_cells', textNode:'Añadir Componente',P:L,func:function(T){
						Wma.Ht.bomTr({wfaId:T.P.wfaId},tBody2,{iFase:iFase,jsk:'LF',jsL:$Api.JS.clsLName,jsLN:$Api.JS.clsLNames});
					} },$1.q('tfoot tr td',tb2));
				}
				if(LF){ for(var i in LF){
					Wma.Ht.bomTr(LF[i],tBody2,{iFase:iFase,jsk:'LF',jsL:$Api.JS.clsLName,jsLN:$Api.JS.clsLNames});
				} }
				$1.display(div2,div1,true);
				$1.Move.btns({},div1);
				var iFase=$1.t('b',{'class':$Api.JS.clsLN,textNode:_g(L.wfaId,$Tb.owfa)},div1);
				$1.lineDel(L,null,div1);
				iFase.AJs={wfaId:L.wfaId,priceLine:0};
			}
		}
	},
	bomTr:function(L,tBody,P){ /* Añador tr de Linea componente */
	if(L=='ini'){
		tb= $1.T.table(['','Tipo','Código','Descripcion','Costo','Requerido','Udm','Total','Detalles'],0,tBody);
		var css='background-color:#0EEE'
		var trf=$1.t('tr',0,$1.t('tfoot',0,tb));
		$1.t('td',{colspan:5,style:css},trf);
		$1.t('td',{colspan:2,textNode:'Total',style:css},trf);
		$1.t('td',{'class':tbCal.tbTotal,colspan:3,style:'text-align:left;'+css},trf);
		return tb;
	}
	$1.nullBlank='';
	P.jsL=(P.jsL)?P.jsL:$Api.JS.clsL;
	var jsF=(P.jsLN)?P.jsLN:$Api.JS.clsLN;
	var tr=$1.t('tr',{'class':P.jsL+' '+tbCal.trLine},tBody);
	if(P.jsk){ tr.setAttribute('jsk',P.jsk); }
	var td0=$1.t('td',{'class':jsF},tr);
	td0.AJs={wfaId:L.wfaId}
	$1.Move.btns({},td0);
	$1.t('td',{textNode:L.lineType,'class':$Api.Sea.clsBox,k:'itemType'},tr);
	var AJs=(L.udm)?{lineType:L.lineType,itemId:L.itemId,itemSzId:L.itemSzId,price:L.price}:{};
	if(L.id){ AJs.id=L.id; }
	var td=$1.t('td',{'class':$Api.Sea.clsBox,k:'itemCode',style:'width:6rem;',textNode:Itm.Txt.code(L)},tr);//itemCode
	var td=$1.t('td',0,tr);
	Itm.Sea.sub({boxRep:tr,AJsBlank:'Y',itemType:'MP,SE,MA,MO,SV,CIF',fie:'I.udm,I.invPrice,I.itemType', value:Itm.Txt.name(L), lFunc:function(R,Tinp,Ttr){
		R.udm=_g(R.udm,Udm.O);
		R.price =R.invPrice*1;
		R.itemCode=Itm.Txt.code(R);
		var inpQty=$1.q('.'+tbCal.trQty,Ttr);
		inpQty.AJs = (R.itemId)
		?{lineType:R.itemType,itemId:R.itemId,itemSzId:R.itemSzId,price:R.price}
		:{};
		if(!inpQty.value){ inpQty.value=1; }
		xTotal(tBody.parentNode);
	}},td);
	var td=$1.t('td',{'class':$Api.Sea.clsBox+' '+tbCal.trPrice,k:'price',kformat:'money',style:'width:6rem;',textNode:$Str.money(L.price)},tr);
	var td=$1.t('td',0,tr);
	var inpQty= $1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' '+tbCal.trQty,name:'quantity',style:'width:6rem;',value:L.quantity*1},td);
	inpQty.AJs=AJs;
	inpQty.onkeyup = inpQty.onchange = function(){ xTotal(tBody.parentNode); }
	$1.t('td',{'class':$Api.Sea.clsBox,k:'udm',style:'width:4rem',textNode:_g(L.udm,Udm.O)},tr);
	$1.t('td',{'class':tbCal.trTotal,style:'width:6rem;',vformat:'money',textNode:$Str.money(L.lineTotal)},tr);
	var td=$1.t('td',0,tr);
	var tdPie=$1.t('div',{'class':'wmaBomPie'},td);
	$1.t('input',{type:'text','class':jsF,name:'lineMemo',value:L.lineMemo},tdPie);
	var td=$1.t('td',0,tr);
	$1.lineDel(L,{jsFL:jsF},td);
	$1.nullBlank=false;
	function xTotal(pare){
		tbCal.docTotal(pare,{func:function(T){
			if(P.iFase && P.iFase.AJs){ P.iFase.AJs.priceLine=T.docTotal; }
		}});
	}
	},
	bomReq:function(pare,P){/* tabla con fases y opcion añadir itt */
		var P=(P)?P:{};
		if(P.reset=='Y'){ pare.innerHTML=''; }
		var tb=$1.T.fieset({L:{textNode:'Componentes'}},pare);
		var tBody=$1.t('div',0,tb);
		Wma.Ht.bomReqTr('ini',tBody,{jsk:'LF'});
		var tBody2=$1.q('tbody',tBody);
		for(var i in P.L){ 
			Wma.Ht.bomReqTr(P.L[i],tBody2,{jsk:'LF'});
		}
	},
	bomReqTr:function(L,tBody,P){ /* consumo */
		if(L=='ini'){
			tb= $1.T.table(['','Tipo','Código','Descripcion','Costo','Udm','Cant.','Base','Almacen','Total',''],0,tBody);
			var css='background-color:#0EEE'
			var tBody=$1.t('tbody',0,tb);
			var trf=$1.t('tr',0,$1.t('tfoot',0,tb));
			var td=$1.t('td',{colspan:6,style:css},trf);
			$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Componente',func:function(){
				Wma.Ht.bomReqTr({},tBody,{jsk:'LF'});
			}},td);
			$1.t('td',{colspan:2,textNode:'Total',style:css},trf);
			$1.t('td',{'class':tbCal.tbTotal,colspan:3,style:'text-align:left;'+css},trf);
			return tb;
		}
		P=(P)?P:{};
		$1.nullBlank='';
		P.jsL=(P.jsL)?P.jsL:$Api.JS.clsL;
		var jsF=(P.jsLN)?P.jsLN:$Api.JS.clsLN;
		var tr=$1.t('tr',{'class':P.jsL+' '+tbCal.trLine},tBody);
		if(P.jsk){ tr.setAttribute('jsk',P.jsk); }
		var td0=$1.t('td',{'class':'_td0 '+jsF},tr);
		$1.t('td',{textNode:L.lineType,'class':$Api.Sea.clsBox,k:'itemType'},tr);
		var AJs=(L.itemId)?{lineType:L.lineType,itemId:L.itemId,itemSzId:L.itemSzId}:{};
		if(L.wfaId){ AJs.wfaId=L.wfaId; }
		AJs.baseQty=(L.baseQty)?L.baseQty:0;
		AJs.price2=(L.price2)?L.price2:0;//ficha
		if(L.id){ AJs.id=L.id; }
		td0.AJs=AJs;
		if(L.lineType=='PeP'){
			td0.AJs.quantity=1;
			td0.AJs.baseQty=1;
			var td=$1.t('td',{colspan:2},tr);
			$1.t('b',{textNode:'Fase Anterior: '+_g(L.wfaId,$Tb.owfa)},td);
			var td=$1.t('td',0,tr);
			var priceQty=$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' '+$Api.Sea.clsBox+' '+tbCal.trPrice,k:'price',name:'price',style:'width:6rem;',value:L.price*1},td);
			priceQty.keyChange(function(){ xTotal(tBody.parentNode); });
			$1.t('td',0,tr);
			$1.t('td',{textNode:1,'class':tbCal.trQty+' '+tbCal.rcellsB},tr);
			$1.t('td',{textNode:1,'class':tbCal.rcellsB},tr);
			$1.T.sel({'class':jsF,name:'whsId',opts:$Tb.itmOwhsPeP,selected:L.whsId},$1.t('td',0,tr));
			$1.t('td',{'class':tbCal.trTotal,style:'width:6rem;',vformat:'money',textNode:$Str.money(L.price)},tr);
			$1.t('td',0,tr);
		}
		else{
			var td=$1.t('td',{'class':$Api.Sea.clsBox,k:'itemCode',style:'width:6rem;',textNode:Itm.Txt.code(L)},tr);//itemCode
			var td=$1.t('td',0,tr);
			Itm.Sea.sub({boxRep:tr,AJsBlank:'Y',itemType:'MP,SE,MA,MO,SV,CIF',fie:'I.udm,I.invPrice,I.itemType',value:Itm.Txt.name(L), lFunc:function(R,Tinp,Ttr){
				R.udm=_g(R.udm,Udm.O);
				R.price =R.invPrice*1;
				R.itemCode=Itm.Txt.code(R);
				var inpQty=$1.q('.'+tbCal.trQty,Ttr);
				var whsId=$1.q('._whsId',Ttr);
				if(R.itemType.match(/^(MP|SE)$/)){ whsId.style.display='block'; }
				else{ whsId.style.display='none'; }
				$1.q('._td0',Ttr).AJs = (R.itemId)
				?{lineType:R.itemType,itemId:R.itemId,itemSzId:R.itemSzId,price:R.price}
				:{};
				if(!inpQty.value){ inpQty.value=1; }
				xTotal(tBody.parentNode);
			}},td);
			var td=$1.t('td',{},tr);
			var priceQty=$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' '+$Api.Sea.clsBox+' '+tbCal.trPrice,k:'price',name:'price',style:'width:6rem;',value:L.price*1},td);
			priceQty.keyChange(function(){ xTotal(tBody.parentNode); });
			$1.t('td',{'class':$Api.Sea.clsBox,k:'udm',style:'width:4rem',textNode:_g(L.udm,Udm.O)},tr);
			var td=$1.t('td',0,tr);
			if(!L.quantity){ L.quantity=L.baseQty*1; }
			var inpQty= $1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' '+tbCal.trQty+' '+tbCal.rcellsB,name:'quantity',style:'width:6rem;',value:L.quantity*1},td);
			inpQty.baseQty=L.baseQty*1; //use tbCal.rCells()
			inpQty.keyChange(function(){ xTotal(tBody.parentNode); });
			$1.t('td',{textNode:L.baseQty*1,'class':tbCal.rcellsB},tr);
			var td=$1.t('td',0,tr);
			_whsId=$1.T.sel({'class':'_whsId '+jsF,name:'whsId',opts:$Tb.itmOwhs,kIf:{whsType:'MP'},selected:L.whsId},td);
			if(L.itemType && !L.itemType.match(/^(MP|SE)$/)){ _whsId.style.display='none'; }
			$1.t('td',{'class':tbCal.trTotal,style:'width:6rem;',vformat:'money',textNode:$Str.money(L.lineTotal)},tr);
			$1.lineDel(L,{jsFL:jsF},$1.t('td',0,tr));
		}
		$1.nullBlank=false;
		function xTotal(pare){ tbCal.docTotal(pare); }
		xTotal(tBody.parentNode);
	},
	bomLs:function(pare,P){/* SubProductos Generados */
		var P=(P)?P:{};
		if(P.reset=='Y'){ pare.innerHTML=''; }
		var tb=$1.T.fieset({L:{textNode:'Productos Generados'}},pare);
		var tBody=$1.t('div',0,tb);
		Wma.Ht.bomLsTr('ini',tBody,{jsk:'LS'});
		var tBody2=$1.q('tbody',tBody);
		for(var i in P.L){ 
			Wma.Ht.bomLsTr(P.L[i],tBody2,{jsk:'LS'});
		}
	},
	bomLsTr:function(L,tBody,P){ /* consumo */
		if(L=='ini'){
			tb= $1.T.table(['','Tipo','Código','Descripcion','Costo','Udm','Cant.','Base','Almacen','Total',''],0,tBody);
			var css='background-color:#0EEE'
			var tBody=$1.t('tbody',0,tb);
			var trf=$1.t('tr',0,$1.t('tfoot',0,tb));
			var td=$1.t('td',{colspan:6,style:css},trf);
			$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Articulo',func:function(){
				Wma.Ht.bomLsTr({},tBody,{jsk:P.jsk});
			}},td);
			$1.t('td',{colspan:2,textNode:'Total',style:css},trf);
			$1.t('td',{'class':tbCal.tbTotal,colspan:3,style:'text-align:left;'+css},trf);
			return tb;
		}
		P=(P)?P:{};
		$1.nullBlank='';
		P.jsL=(P.jsL)?P.jsL:$Api.JS.clsL;
		var jsF=(P.jsLN)?P.jsLN:$Api.JS.clsLN;
		var tr=$1.t('tr',{'class':P.jsL+' '+tbCal.trLine,jsk:P.jsk},tBody);
		var td0=$1.t('td',{'class':'_td0 '+jsF},tr);
		$1.t('td',{textNode:L.lineType,'class':$Api.Sea.clsBox,k:'itemType'},tr);
		var AJs=(L.itemId)?{lineType:L.lineType,itemId:L.itemId,itemSzId:L.itemSzId,price:L.price}:{};
		if(L.id){ AJs.id=L.id; }
		td0.AJs=AJs;
		{
			var td=$1.t('td',{'class':$Api.Sea.clsBox,k:'itemCode',style:'width:6rem;',textNode:Itm.Txt.code(L)},tr);//itemCode
			var td=$1.t('td',0,tr);
			Itm.Sea.sub({boxRep:tr,AJsBlank:'Y',itemType:'P,MP,SE',fie:'I.udm,I.invPrice,I.itemType',value:Itm.Txt.name(L), lFunc:function(R,Tinp,Ttr){
				R.udm=_g(R.udm,Udm.O);
				R.price =R.invPrice*1;
				R.itemCode=Itm.Txt.code(R);
				var inpQty=$1.q('.'+tbCal.trQty,Ttr);
				$1.q('._td0',Ttr).AJs = (R.itemId)
				?{lineType:R.itemType,itemId:R.itemId,itemSzId:R.itemSzId,price:R.price}
				:{};
				if(!inpQty.value){ inpQty.value=1; }
				xTotal(tBody.parentNode);
			}},td);
			var td=$1.t('td',{},tr);
			$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' '+$Api.Sea.clsBox+' '+tbCal.trPrice,k:'price',name:'price',style:'width:6rem;',value:L.price*1},td);
			$1.t('td',{'class':$Api.Sea.clsBox,k:'udm',style:'width:4rem',textNode:_g(L.udm,Udm.O)},tr);
			var td=$1.t('td',0,tr);
			if(!L.quantity){ L.quantity=L.baseQty*1; }
			var inpQty= $1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' '+tbCal.trQty,name:'quantity',style:'width:6rem;',value:L.quantity*1},td);
			inpQty.onkeyup = inpQty.onchange = function(){ xTotal(tBody.parentNode); }
			$1.t('td',{textNode:L.baseQty*1},tr);
			var td=$1.t('td',0,tr);
			_whsId=$1.T.sel({'class':jsF,name:'whsId',opts:$Tb.itmOwhs,selected:L.whsId},td);
			$1.t('td',{'class':tbCal.trTotal,style:'width:6rem;',vformat:'money',textNode:$Str.money(L.lineTotal)},tr);
			$1.lineDel(L,{jsFL:jsF},$1.t('td',0,tr));
		}
		$1.nullBlank=false;
		function xTotal(pare){
			tbCal.docTotal(pare,{func:function(T){
			}});
		}
		xTotal(tBody.parentNode);
	},
}

$JsV._i({kObj:'wmaWopGr',mdl:'wma',
liK:'wmaJsv',liTxtG:'Grupo Operaciónes',liTxtF:'Grupo de Operación (Form)',
Cols:[
{t:'Nombre',k:'value',T:{tag:'input'}}
]
});
