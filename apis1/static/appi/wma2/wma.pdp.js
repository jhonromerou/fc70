var Wma3={}; //Module wma.pdp
$js.push($V.docSerieType,{
'wmaPdp':{t:'Planif. Producción'},
//'wmaOdp':{t:'Orden de Producción'},

'opdp':'Plan. Producción',
'wmaOdp':'Orden de Producción',
'wmaDdf':'Documento de Fase',
'wmaDcf':'Doc. Cierre de Fase'
});

$oB.push($V.docTT,[
{k:'wmaPdp',v:'Planif. de Producción'},{k:'wmaDop',v:'Orden de Producción'},{k:'wmaPdf',v:'Documento Fase'}
]);

$Doc.a['opdp']={a:'wma3.opdp.view'};
$Doc.a['wmaOdp']={a:'wma3.opdp.view',t:'Orden de Producción'};
$Doc.a['wmaDdf']={a:'wma3.ddf.view',t:'Documento de Fase'};
$Doc.a['wmaDcf']={a:'wma3.dcf.view',t:'Doc. Cierre Fase'};
$V.wmaBomType=[
	{k:'MP',v:'Materia Prima'},
	{k:'MO',v:'Mano de Obra'},
	{k:'SV',v:'Servicios'},
	{k:'MA',v:'Maquinaria'},
	{k:'CIF',v:'Costes Indirectos'},
	{k:'SE',v:'Semielaborados'},
	{k:'PeP',v:'Fases'}
];
$V.wma3DcfRejs=[{k:'C',v:'Calidad'},{k:'O',v:'Otros'}];
$V.wmaDopType=[{k:'T',v:'General'}];

$V._wfaClassPdP=[{k:'N',v:'Normal'},{k:'N,RC',v:'Normal y Reproceso'},{k:'RC',v:'Reproceso'},{k:'NC',v:'Producto No Conforme'},{k:'',v:'Todo'}];
$V.oOdpType={P:'Programado',O:'Liberado',C:'Cerrado',N:'Anulado'};
$V.wmaOdpStatus={P:'Programado',O:'Liberado',C:'Cerrado',N:'Anulado'};
$V.wmaPdpStatus=[{k:'O',v:'Abierta'},{k:'C',v:'Cerrada'},{k:'N',v:'Anulada'}];
$V.wma3DdfType=[{k:'I',v:'Interno'},{k:'E',v:'Externa'}];
$V.wma3DdfLineType=[{k:'C',v:'Completar'},{k:'R',v:'Rechazado'},{k:'D',v:'Desperdicio'}];

$Tb.N999['whsPeP']='Ninguno';

$M.kauAssg('itm',[
	{k:'wmaPdp',t:'Planif. de Producción'},
	{k:'wmaDop',t:'Ordenes de Producción'},
	{k:'wmaBom',t:'Estructura de Artículo'},
	{k:'wmaMrp',t:'MRP Producción'},
	{k:'wmaDpf',t:'Documentos de Fase'},
	{k:'wmaDdf',t:'Documentos de Fase'},
	{k:'wmaDcf',t:'Documentos de Cierre'},
	
]);
$M.liAdd('wma',[
{_lineText:'Orden de Produccion'},
{k:'wmaDop',t:'Ordenes de Producción', kau:'wmaDop',mdlActive:'wma', func:function(){
	$M.Ht.ini({btnGo:'wmaDop.form',f:'wma3.oodp',gyp:Wma.Dop.get});
}},
{k:'wmaDop.form',t:'Orden de Producción (Form)', kau:'wmaDop',mdlActive:'wma', func:function(){
	$M.Ht.ini({gyp:Wma.Dop.form});
}},
{k:'wmaDop.view',noTitle:'Y',t:'Orden de Producción', kau:'wmaDop',mdlActive:'wma', func:function(){
	$M.Ht.ini({g:Wma.Dop.view});
}},

{_lineText:'Documento Fase'},
{k:'wmaDpf',t:'Documento Fase', kau:'wmaDpf',mdlActive:'wma', func:function(){ $M.Ht.ini({btnGo:'wmaDpf.form',f:'wmaPdf',gyp:Wma.Dpf.get}); }},
{k:'wmaDpf.form',t:'Documento de Fase (Form)', kau:'wmaDpf',mdlActive:'wma', func:function(){ $M.Ht.ini({gyp:Wma.Dpf.form}); }},
{k:'wmaDpf.view',noTitle:'Y',t:'Documento de Fase', kau:'wmaDpf',mdlActive:'wma', func:function(){ $M.Ht.ini({g:Wma.Dpf.view}); }},

{_lineText:'Remisión de Servicios - Fases'},
{k:'wmaDrs',t:'Remisión Servicios', kau:'wmaDrs',mdlActive:'wma', func:function(){ $M.Ht.ini({btnGo:'wmaDrs.form',f:'wmaDrs',gyp:Wma.Drs.get}); }},
{k:'wmaDrs.form',t:'Remisión Servicios (Form)', kau:'wmaDrs',mdlActive:'wma', func:function(){ $M.Ht.ini({gyp:Wma.Drs.form}); }},
{k:'wmaDrs.view',noTitle:'Y',t:'Remisión Servicios', kau:'wmaDrs',mdlActive:'wma', func:function(){ $M.Ht.ini({g:Wma.Drs.view}); }},

{_lineText:'Planif. Producción'},
{k:'wmaOdp.lop',t:'Lotes Unitarios Orden de Producción',d:'Generar un código único para cada unidad de la orden de producción',kau:'wmaOdp',mdlActive:'wma', func:function(){ $M.Ht.ini({g:Wma.Odp.Lop.form}); }},
{k:'wmaPdp',t:'Planificación Producción', kau:'wmaPdp',mdlActive:'wma', func:function(){
	$M.Ht.ini({fieldset:'Y-none',f:'wma3.opdp',btnGo:'wmaPdp.form',gyp:Wma.Pdp.get});
}},
{k:'wmaPdp.form',t:'Documento Planificación Producción', kau:'wmaPdp',mdlActive:'wma', func:function(){ $M.Ht.ini({g:Wma.Pdp.form}); }},
{k:'wmaPdp.view',noTitle:'Y',t:'Ver Documento Planificación Producción',kau:'wmaPdp',mdlActive:'wma', func:function(){ $M.Ht.ini({g:Wma.Pdp.view}); }},
{k:'wmaPdp.orders',kau:'wmaPdp',mdlActive:'wma', func:function(){ $M.Ht.ini({g:Wma.Pdp.orders}); }},
{k:'wmaPdp.consol',t:'Consolidado Planificación Producción', kau:'wmaPdp',mdlActive:'wma', func:function(){ $M.Ht.ini({f:'wma3.opdp.consol'}); }},
{k:'wmaPdp.consolGroup',t:'Planificación Pendientes por Tipo', kau:'wmaPdp',mdlActive:'wma', func:function(){ $M.Ht.ini({f:'wma3.opdp.consolGroup'}); }},
{k:'wmaPdp.corteProg',t:'Corte para Planificación', kau:'wmaPdp',mdlActive:'wma', func:function(){ $M.Ht.ini({f:'wma3.opdp.corteProg'}); }},
{k:'wmaPdp.auxCumpProd',t:'Auxiliar Producción - Comercial', kau:'wmaPdp', mdlActive:'wma',func:function(){ $M.Ht.ini({f:'wma3.opdp.auxCumpProd'}); }},

{_lineText:'MRP Producción'},
{k:'wmaMrp.fromPdp',t:'MRP Plan de Producción', kau:'wmaMrp',mdlActive:'wma', func:function(){ $M.Ht.ini({f:'wmaMrp.fromPdp',g:null}); }},
{k:'wmaMrp.fromOdp',t:'MRP Orden de Producción', kau:'wmaMrp',mdlActive:'wma', func:function(){ $M.Ht.ini({f:'wmaMrp.fromOdp'}); }},
{k:'wmaMrp.fromPep',t:'MRP Producto en Proceso', kau:'wmaMrp',mdlActive:'wma', func:function(){ $M.Ht.ini({f:'wmaMrp.fromPeP'}); }},
]);


_Fi['wma3.opdp']=function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', subText:'Fecha Programada',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Tipo'},I:{tag:'select',sel:{'class':jsV,name:'A.docType(E_igual)'},opts:$V.oPdpType}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Estado'},I:{tag:'select',sel:{'class':jsV,name:'A.docStatus(E_igual)'},opts:$V.dStatus}},divL);
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', subText:'Fecha Entrega',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_menIgual)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Pdp.get});
	wrap.appendChild(btnSend);
},
_Fi['wma3.opdp.consol']=function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx10', L:{textNode:'Estado'},I:{tag:'select',sel:{'class':jsV,name:'A.docStatus(E_igual)'},opts:$V.ordStatus,blankText:'Todos',selected:'O'}},wrap);
	$1.T.divL({wxn:'wrapx8', subText:'Fecha Programada',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Tipo'},I:{tag:'select',sel:{'class':jsV,name:'A.docType(E_igual)'},opts:$V.oPdpType}},divL);
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', subText:'Fecha Entrega',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Pdp.consol});
	wrap.appendChild(btnSend);
},
_Fi['wma3.opdp.consolGroup']=function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', subText:'Fecha Programada',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8', subText:'Fecha Entrega',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Pdp.consolGroup});
	wrap.appendChild(btnSend);
},

_Fi['wma3.oodp']=function(wrap){
	var jsV = 'jsFiltVars';
	var dini=$2d.f('next-7','Y-m-d');
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Número Orden',I:{tag:'input',type:'number','class':jsV,name:'A.docEntry(E_igual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', subText:'Fecha de Corte',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)',value:dini}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)',value:$2d.tomorrow}},divL);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Estado'},I:{tag:'select',sel:{'class':jsV,name:'A.docStatus(E_igual)'},opts:$V.wmaOdpStatus}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Código/s'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma3.Odp.get});
	wrap.appendChild(btnSend);
},
_Fi['wma3.opdp.corteProg']=function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', subText:'Fecha Creado',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', subText:'Fecha Entrega',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_menIgual)'}},divL);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx4', L:'Nombre Cliente',I:{tag:'input',type:'text','class':jsV,name:'A.cardName(E_like3)',placeholder:''}},wrap);
		$1.T.divL({wxn:'wrapx4', L:'Número/s Doc',I:{tag:'input',type:'text','class':jsV,name:'A.docEntry(E_in)',placeholder:'145,149,201'}},divL);
		$1.T.divL({wxn:'wrapx4', L:'Omitir Número/s Doc',I:{tag:'input',type:'text','class':jsV,name:'A.docEntry(E_noIn)',placeholder:'145,149,201'}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx6',subText:'Almacen de pedido',L:'Almacen',I:{tag:'select',sel:{'class':jsV,name:'A.whsId(E_igual)'},opts:$V.whsCode,noBlank:1}},wrap);
	$1.T.divL({wxn:'wrapx6',L:'Clase de Fase',_i:'wfaClass',I:{tag:'select',sel:{'class':jsV,name:'wfaClassPdP'},opts:$V._wfaClassPdP,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'101,511...'}},divL);
	$1.T.divL({wxn:'wrapx6',L:'Grupo Artículos',I:{tag:'select',sel:{'class':jsV,name:'I.itemGr(E_igual)'},opts:$V.itemGrP}},divL);
	$1.T.divL({wxn:'wrapx6',L:'Visualizar',I:{tag:'select',sel:{'class':'__viewType'},opts:{'':'Todo',aProgramar:'Nuevo a Programar'},noBlank:1}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Pdp.corteProg});
	wrap.appendChild(btnSend);
},
_Fi['wma3.opdp.auxCumpProd']=function(wrap){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var dEnd= $2d.weekE($2d.weekB($2d.add($2d.today,'+14days')));
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', subText:'Fecha Creado',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', subText:'Fecha Entrega',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.dueDate(E_menIgual)',value:dEnd}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx6',L:'Almacen',I:{tag:'select',sel:{'class':jsV,name:'A.whsId(E_igual)'},opts:$V.whsCode,noBlank:1}},wrap);
	$1.T.divL({wxn:'wrapx6',L:'Clase de Fase',_i:'wfaClass',I:{tag:'select',sel:{'class':jsV,name:'wfaClassPdP'},opts:$V._wfaClassPdP,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)',placeholder:'101,511...'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Pdp.auxCumpProd});
	wrap.appendChild(btnSend);
},
_Fi['wmaOdp.docHistory']=function(wrap){
	var jsV = 'jsFiltVars';
	var tOpts=[{k:'wmaDcf',v:'Doc. Cierre'},{k:'wmaDdf',v:'Doc. de Fase'}];
	var dini=$2d.f('today','Y-m-d');
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', subText:'Fecha Documento',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)',value:dini}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)',value:$2d.today}},divL);
	$1.T.divL({wxn:'wrapx8', L:'No. Orden Prod.',I:{tag:'input',type:'number',inputmode:'numeric','class':jsV,name:'B.tr'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Código/s'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode'}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Talla',I:{tag:'select','class':jsV,opts:$V.grs1,name:'B.itemSzId'}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx10', L:'Origen',I:{tag:'select',sel:{'class':jsV+' __origenDoc',name:'origen'},opts:tOpts,noBlank:'Y'}},wrap);
	$1.T.divL({wxn:'wrapx10', L:'Estado Doc.',I:{tag:'select','class':jsV,name:'A.docStatus',opts:$V.docStatusOC}},divL);
	$1.T.divL({wxn:'wrapx8', L:'No. Doc',I:{tag:'input',type:'text','class':jsV,name:'A.docEntry'}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Fase',I:{tag:'select','class':jsV,name:'B.wfaId',opts:$Tb.owfa}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Almacen',I:{tag:'select','class':jsV,opts:$Tb.whsPeP,name:'B.whsId'}},divL);
	$1.T.divL({wxn:'wrapx10', L:'Tipo Linea',I:{tag:'select','class':jsV,name:'B.lineType',opts:$V.wma3DdfLineType}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma3.Odp.docHistory});
	wrap.appendChild(btnSend);
},


_Fi['wmaMrp.fromPdp']=function(wrap){
	var Pa=$M.read();
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', subText:'Fecha Programada',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Estado',I:{tag:'select',sel:{'class':jsV,name:'A.docStatus(E_igual)'},opts:$V.docStatusOC,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Tipo',I:{tag:'select',sel:{'class':jsV,name:'A.docType(E_igual)'},opts:$V.oPdpType}},divL);
	$1.T.divL({wxn:'wrapx10', L:'Semilaborados',I:{tag:'select',sel:{'class':jsV,name:'seView'},opts:{d:'Como Artículo',tree:'Descomponer'},noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Almacen',I:{tag:'select','class':jsV,name:'whsId',opts:$Tb.itmOwhs}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx6', L:{textNode:'Necesidad Según'},I:{tag:'select',sel:{'class':jsV,name:'viewType'},opts:{quantity:'Planificado Total',openQty:'Pendientes por Producir'},noBlank:1}},wrap);
	$1.T.divL({wxn:'wrapx10', L:{textNode:'Fase/s'},I:{tag:'select',sel:{'class':jsV,name:'T1.wfaId(E_in)',multiple:'Y'},opts:$Tb.owfa}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Mrp.get});
	$1.T.divL({wxn:'wrapx8', subText:'Ids documentos',L:{textNode:'Documentos'},I:{tag:'input',type:'text','class':jsV,name:'A.docEntry(E_in)',value:Pa.docEntry}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Código Material'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx6', L:{textNode:'Grupo'},I:{tag:'select',sel:{'class':jsV,name:'I.itemGr(E_igual)'},opts:$JsV.itmGr}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Ref/s',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Mrp.get});
	wrap.appendChild(btnSend);
},
_Fi['wmaMrp.fromOdp']=function(wrap){
	var Pa=$M.read();
	var tdate=$2d.rang({rang:'days--',n:1});
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', subText:'Fecha Programada',L:{textNode:'Fecha Inicio'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)',value:tdate.date1}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Fecha Fin'},I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Estado',I:{tag:'select',sel:{'class':jsV,name:'A.docStatus(E_igual)'},opts:$V.docStatusOC,noBlank:1}},divL);
	$1.T.divL({wxn:'wrapx8', subText:'Ids documentos',L:'Documentos',I:{tag:'input',type:'text','class':jsV,name:'docEntrys',value:Pa.docEntry,placeholder:'1,3,n o 7-13'}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Tipo',I:{tag:'select',sel:{'class':jsV,name:'A.docType(E_igual)'},opts:$V.oPdpType}},divL);
	var divL= $1.T.divL({divLine:1,wxn:'wrapx10', L:'Semilaborados',I:{tag:'select','class':jsV,name:'seView',opts:{d:'Como Artículo',tree:'Descomponer'},noBlank:1}},wrap);
	$1.T.divL({wxn:'wrapx8', L:'Almacen',I:{tag:'select','class':jsV,name:'whsId',opts:$Tb.itmOwhs}},divL);
	$1.T.divL({wxn:'wrapx10', L:'Fase/s',I:{tag:'select',sel:{'class':jsV,name:'T1.wfaId(E_in)',multiple:'Y'},opts:$Tb.owfa}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Mrp.get});
	$1.T.divL({wxn:'wrapx8', L:'Ref/s',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Código Articulo',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx6', L:'Grupo',I:{tag:'select','class':jsV,name:'I.itemGr(E_igual)'},opts:$JsV.itmGr},divL);
	wrap.appendChild(btnSend);
},
_Fi['wmaMrp.fromPeP']=function(wrap){
	var Pa=$M.read();
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:'Semilaborados',I:{tag:'select',sel:{'class':jsV,name:'seView'},opts:{d:'Como Artículo',tree:'Descomponer'},noBlank:1}},wrap);
	$1.T.divL({wxn:'wrapx8', L:'Almacen',I:{tag:'select','class':jsV,name:'whsId',opts:$Tb.itmOwhs}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Ref/s',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)'}},divL);
	$1.T.divL({wxn:'wrapx4', L:'Fase en Proceso',I:{tag:'select','class':jsV,name:'B.wfaId(E_in)',multiple:'Y',opts:$Tb.owfa}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Mrp.get});
	$1.T.divL({wxn:'wrapx8', L:'Código Articulo',I:{tag:'input',type:'text','class':jsV,name:'citemCode'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:Wma.Mrp.get},wrap);
}

_Fi['wmaMrp.consulta']=function(wrap){
	var Pa=$M.read();
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({wxn:'wrapx8', L:'Ref/s',I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_in)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Código Material'},I:{tag:'input',type:'text','class':jsV,name:'I.itemCode(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx6', L:{textNode:'Grupo'},I:{tag:'select',sel:{'class':jsV,name:'I.itemGr(E_igual)'},opts:$JsV.itmGr}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma.Mrp.get});
	wrap.appendChild(btnSend);
};


_Fi['wma3.ddf']=function(wrap){
	var jsV = 'jsFiltVars';
	var dini=$2d.f('next-7','Y-m-d');
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Número Doc.',I:{tag:'input',type:'number','class':jsV,name:'A.docEntry(E_igual)'}},wrap);
	$1.T.divL({wxn:'wrapx8', L:'Fecha Inicio',I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)',value:dini}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Fecha Fin',I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)',value:$2d.tomorrow}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Tipo'},I:{tag:'select',sel:{'class':jsV,name:'A.docType(E_igual)'},opts:$V.wma3DdfType}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Estado'},I:{tag:'select',sel:{'class':jsV,name:'A.docStatus(E_igual)'},opts:$V.dStatus}},divL);
	$1.T.divL({wxn:'wrapx6',L:'Bod. Proceso',I:{tag:'select',sel:{'class':jsV,name:'A.whsId(E_igual)'},opts:$Tb.whsPeP}},divL);
	$1.T.divL({wxn:'wrapx6',L:'Fase a Realizar',I:{tag:'select',sel:{'class':jsV,name:'A.wfaId(E_igual)'},opts:$Tb.owfa}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx2', L:'Tercero',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},wrap);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma3.Ddf.get});
	wrap.appendChild(btnSend);
},
_Fi['wma3.dcf']=function(wrap){
	var jsV = 'jsFiltVars';
	var Oris={wmaDdf:'Doc. Fase'};;
	var dini=$2d.f('next-7','Y-m-d');
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Número Doc.',I:{tag:'input',type:'number','class':jsV,name:'A.docEntry(E_igual)'}},wrap);
	$1.T.divL({divLine:1,wxn:'wrapx8', L:'Fecha Inicio',I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)',value:dini}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Fecha Fin',I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)',value:$2d.tomorrow}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Tipo'},I:{tag:'select',sel:{'class':jsV,name:'A.docType(E_igual)'},opts:$V.wma3DdfType}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Estado',I:{tag:'select',sel:{'class':jsV,name:'A.docStatus(E_igual)'},opts:$V.dStatus}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Estado Consumos',I:{tag:'select',sel:{'class':jsV,name:'A.emiStatus(E_igual)'},opts:$V.statusOC}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx2', L:'Tercero',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},wrap);
	$1.T.divL({wxn:'wrapx6',L:'Origen',I:{tag:'select',sel:{'class':jsV,name:'A.tt(E_igual)'},opts:Oris}},divL);
	$1.T.divL({wxn:'wrapx6', L:'Doc. Origen',I:{tag:'input',type:'text','class':jsV,name:'A.tr(E_menIgual)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Wma3.Dcf.get});
	wrap.appendChild(btnSend);
},

$M.li['wma3.oodp.fromPdp']={t:'Orden de Producción (Desde Planificación)', kau:'wma3.oodp.basic', func:function(){ $M.Ht.ini({func_cont:Wma3.Odp.fromPdp}); }};
$M.li['wma3.odp.libeForm']={t:'Liberar Orden de Producción',kau:'wma3.oodp.basic', func:function(){ $M.Ht.ini({func_cont:Wma3.Odp.Libe.form}); }};
$M.li['wma3.odp.tbFase']={t:'Estado Fases Orden de Producción',kau:'wma3.oodp.basic', func:function(){ $M.Ht.ini({func_cont:Wma3.Odp.tbFase}); }};
$M.li['wma3.odp.itemStatusFase']={t:'Estado Ordenes de Artículo',kau:'wma3.oodp.basic', func:function(){ $M.Ht.ini({func_cont:Wma3.Odp.itemStatusFase}); }};
$M.li['wmaOdp.docFase']={noTitle:'Y',kau:'wma3.oodp.basic', func:function(){ $M.Ht.ini({func_cont:Wma3.Odp.docFase}); }};
$M.li['wmaOdp.docHistory']={t:'Historial de Ordenes',kau:'wma3.oodp.basic', func:function(){ $M.Ht.ini({func_filt:'wmaOdp.docHistory',func_pageAndCont:Wma3.Odp.docHistory}); }};
$M.li['wmaOdp.form2']={noTitle:'Y',kau:'wma3Odp.form2', func:function(){ $M.Ht.ini({g:Wma3.Odp.form2}); }};


$M.li['wma3.ddf']={t:'Documentos de Fase',kau:'wma3.ddf.read',func:function(){
	$M.Ht.ini({g:'wma3.ddf',btnGo:'wma3.ddf.form',gyp:function(){ $M.Ht.ini({g:Wma3.Ddf.get}); }});
}};
$M.li['wma3.ddf.form']={t:'Documento de Fase',kau:'wma3.ddf.write',func:function(){ $M.Ht.ini({g:Wma3.Ddf.form}); }};
$M.li['wma3.ddf.view']={t:'',kau:'wma3.ddf.read', func:function(){ $M.Ht.ini({g:Wma3.Ddf.view}); }};

$M.li['wma3.dcf']={t:'Documentos de Cierre',kau:'wma3.dcf.read',func:function(){
	var btnA=$1.T.btnFa({faBtn:'fa_doc',textNode:'Nuevo Documento',func:function(){ $M.go('wma3.dcf.form'); }});
	$M.Ht.ini({func_filt:'wma3.dcf',btnNew:btnA,func_pageAndCont:Wma3.Dcf.get}); }};
$M.li['wma3.dcf.form']={t:'Documento de Cierre',kau:'wma3.dcf.write',func:function(){ Wma3.Dcf.form(); }};
$M.li['wma3.dcf.fromDdf']={t:'Documento de Cierre (1)',kau:'wma3.dcf.write', func:function(){ $M.Ht.ini({func_cont:Wma3.Dcf.fromDdf}); }};
$M.li['wma3.dcf.emision']={t:'Consumo de Materiales Documento de Cierre',kau:'wma3.dcf.emision', func:function(){ $M.Ht.ini({func_cont:Wma3.Dcf.emision}); }};
$M.li['wma3.dcf.view']={t:'',kau:'wma3.ddf.read', func:function(){ $M.Ht.ini({func_cont:Wma3.Dcf.view}); }};

Wma.Mrp={
	get:function(){
		var Pad=$M.read('!');
		var ty=(Pad=='wmaMrp.fromPep')?'pep':'pdp';
		if(Pad=='wmaMrp.fromOdp'){ ty='odp'; }
		if(ty=='pep'){ Wma.Mrp.data(Api.Wma.pr+'mrp/fromPep'); }
		else if(ty=='odp'){ Wma.Mrp.data(Api.Wma.pr+'mrp/fromOdp'); }
		else{ Wma.Mrp.data(Api.Wma.pr+'mrp/fromPdp'); }
	},
	data:function(api){
		var Pa=$M.read(); var cont=$M.Ht.cont;
		$Api.get({f:api, inputs:$1.G.filter(),loade:cont, func:function(Jr){
			if(Jr.errNo){ $Api.resp(cont,Jr); }
			else{
				var tb=$1.T.table(['Estado','Fase Cons.','Código','Descripción','Cant. Req.','Udm','Und. Compra','Conversión','Req. Und. Comp.','Disponible','Pedido','Necesidad',{textNode:'Ped. Min',_iHelp:'Cantidad mínima que se debe pedir'},'Precio Unitario','Precio Total']);
				var tBody=$1.t('tbody',0,tb);
				Jr.L=$js.sortNum(Jr.L,{k:'itemCode'});
				for(var i in Jr.L){ var L=Jr.L[i];
					var tr=$1.t('tr',0,tBody);
					
					var onHand=(L.onHand)?L.onHand*1:'';
					var onOrder=(L.onOrder)?L.onOrder*1:'';
					var reqQty=L.reqQty*1;
					val= $js.toFixed(reqQty/L.buyFactor,3);
					onOrder=$js.toFixed(onOrder/L.buyFactor,3);
					var reqQty=$js.toFixed(reqQty,3);
					val=$js.toFixed(val,3);
					var necPed=val-onHand*1;
					var nec=(necPed-onOrder*1);
					if(nec<=0){ nec=''; }
					var lineTotal=L.buyPrice*(nec*1);
					var cssF=''; var sText='Pedir';
					var lineIvt=(L.itemType.match(/(MO|MA|SV|CIF)/is));
					if(necPed<=0){ sText='Ok'; cssF='backgroundColor:#0F0;' }
					else if(nec==''){ sText='Revisar'; cssF='backgroundColor:#FF0;' }
					if(lineIvt){ sText='N/A'; }
					$1.t('td',{textNode:sText,style:cssF},tr);
					$1.t('td',{textNode:_g(L.wfaId,$Tb.owfa)},tr);
					$1.t('td',{textNode:Itm.Txt.code(L)},tr);
					$1.t('td',{textNode:Itm.Txt.name(L)},tr);
					$1.t('td',{textNode:reqQty,style:'backgroundColor:#EE0;'},tr);
					$1.t('td',{textNode:_g(L.udm,Udm.O)},tr);
					if(lineIvt){
						$1.t('td',{colspan:7,style:'backgroundColor:#CCC'},tr);
					}
					else{
						$1.t('td',{textNode:_g(L.buyUdm,Udm.O)},tr);
						$1.t('td',{textNode:'x '+L.buyFactor*1},tr);
						$1.t('td',{textNode:val,style:'backgroundColor:#EE0;'},tr);
						$1.t('td',{textNode:onHand},tr);
						$1.t('td',{textNode:onOrder},tr);
						$1.t('td',{textNode:nec,style:'backgroundColor:#EE0;'},tr);
						$1.t('td',{textNode:L.minBuyOrd*1},tr);
					}
					$1.t('td',{textNode:$Str.money(L.buyPrice),t:L.buyPrice},tr);
					$1.t('td',{textNode:$Str.money(lineTotal),t:lineTotal},tr);
				}
				tb=$1.T.tbExport(tb,{ext:'xlsx',fileName:'Requerimiento Materiales pdp '+Pa.docEntry,print:1});
				cont.appendChild(tb);
			}
		}});
	}
}

Wma.Dop={
	OLg:function(L){
		var Li=[]; var n=0;
		var ab=new $Doc.liBtn(Li,L,{api:Api.Wma.pr+'dop',tbSerie:'wmaDop'});
		ab.add('v');
		// Li.push({k:'barCode',ico:'iBg iBg_barCode',textNode:' Definir Lotes Unitarios', P:L, func:function(T){ $Doc.go('wmaOdp','lop',T.P,1); }});
		if(L.docStatus=='P'){
			ab.add('E');
			Li.push({k:'public',ico:'fa fa-rocket',textNode:' Liberar Orden',P:L,func:function(T){
				$Api.post({f:Api.Wma.pr+'dop/liberate',inputs:'docEntry='+T.P.docEntry,xConf:'Se va a liberar la orden, no se podrá modificar más el documento.'});
			}});
		}
		ab.add('C'); ab.add('N'); ab.add('R'); ab.add('L');
		return $Opts.add('wmaDop',Li,L);;
	},
	opts:function(P,pare){
		Li={Li:Wma.Dop.OLg(P.L),PB:P.L,textNode:P.textNode};
		var mnu=$1.Menu.winLiRel(Li);
		if(pare){ pare.appendChild(mnu); }
		return mnu;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Doc.tbList({api:Api.Wma.pr+'dop',inputs:$1.G.filter(),
		fOpts:Wma.Dop.opts,view:'Y',docBy:'userDate',
		tbSerie:'gvtSop',
		TD:[
			{H:'Estado',k:'docStatus',_V:'dStatus'},
			{H:'Fecha',k:'docDate',dateText:'mmm d'},
			{H:'Código',k:'docDate',fText:Itm.Txt.code},
			{H:'Descripción',k:'docDate',fText:Itm.Txt.name},
			{H:'Cant.',format:'number'},
			{H:'Cliente',k:'cardName'},
			{H:'Validez',k:'dueDate',dateText:'mmm d'},
		]
		},cont);
	},
	form:function(){
		var D=$Cche.d(0,{});
		D.docDate=$2d.today;
		var cont =$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.Wma.pr+'dop/form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
			if(Jr.docEntry){ D=Jr; }
			var sSub=Itm.Sea.sub({D:D,'class':'_itemInfo',itemType:'P',vSea:'I.prdItem=Y',funcAll:function(L){
				
				if(L.itemId){
					Wma.Bom.fases({itemId:L.itemId,itemSzId:L.itemSzId},function(Jr2){
						Wma.Ht.bomFases(midCont,{reset:'Y',itt:'Y',L:Jr2.L,LF:Jr2.LF});
					});
				}
				else{ Wma.Ht.bomFases(midCont,{reset:'Y'}); }
			}});
			var midCont=function(Jr,cont){
				var midCont=$1.t('div',{'class':'midCont'},cont);
				Wma.Ht.bomFases(midCont,{itt:'Y',L:Jr.L,LF:Jr.LF});
			}
			var jsF=$Api.JS.cls;
			$Api.form2({api:Api.Wma.pr+'dop',AJs:D.AJs,PUTid:Pa.docEntry,JrD:D,vidn:'docEntry',to:'wmaDop.view',midCont:midCont,
			tbH:[
				{divLine:1,L:'Tipo',wxn:'wrapx8',req:'Y',I:{tag:'select','class':'_docType '+jsF,name:'docType',opts:$V.wmaDopType,noBlank:'Y',selected:D.docType}},
				{L:'Articulo',req:'Y',wxn:'wrapx4',Inode:sSub},
				{L:'Cantidad',req:'Y',wxn:'wrapx8',I:{lTag:'number','class':jsF,name:'quantity',value:D.quantity}},
				{L:'Clase',wxn:'wrapx8',I:{tag:'select','class':jsF,name:'docClass',opts:$JsV.wmaDopCls,selected:D.docClass}},
				{divLine:1,L:'Fecha',req:'Y',wxn:'wrapx8',I:{tag:'date','class':jsF,name:'docDate',value:D.docDate}},
				{L:'Fecha entrega',req:'Y',wxn:'wrapx8',I:{tag:'date','class':jsF,name:'dueDate',value:D.dueDate}},
				{L:'Orden de Venta',wxn:'wrapx8',I:{tag:'input',type:'text','class':jsF,name:'ref1',value:D.ref1}},
				{L:'Cliente',wxn:'wrapx4',Inode:Crd.Sea.get({cardType:'C',D:D})},
				{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea','class':jsF,name:'lineMemo',textNode:D.lineMemo}}
			],
			reqFields:{
				D:[{k:'itemId',iMsg:'Articulo'}, {k:'itemSzId',iMsg:'Articulo (2)'},{k:'quantity',iMsg:'Cantidad'},
					{k:'docDate',iMsg:'Fecha'}, {k:'dueDate',iMsg:'Fecha Entrega'}, 
				],
				L:['_req',{_t:'Se debe definir las fases de la orden'},{k:'wfaId',iMsg:'Fase'}
				],
				LF:[{k:'wfaId',iMsg:'Fase para articulo'},
					{k:'itemId',iMsg:'Articulo'},
					{k:'quantity',iMsg:'Cantidad Requerida'}
				],
			}
			},cont);
			var midCont=$1.q('.midCont',cont);
			if(D.itemId){
				Wma.Bom.fases({itemId:D.itemId,itemSzId:D.itemSzId},function(Jr2){
					Wma.Ht.bomFases(midCont,{reset:'Y',itt:'Y',L:Jr2.L,LF:Jr2.LF});
				});
			}
		}});
	},
	view:function(){
		var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
		$Api.get({f:Api.Wma.pr+'dop/view',inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
			var tP={tbSerie:'wmaDop',D:Jr,
				main:Wma.Dop.OLg,
				THs:[
					{k:'docEntry'},{docTitle:'Orden de Producción',cs:5,ln:1},{k:'docClass',_g:$JsV.wmaDopCls,ln:1,cs:2},
					{k:'docType',cs:2,_g:$V.wmaDopType},{middleInfo:'Y'},{logo:'Y'},
					{t:'Fecha',k:'docDate'},
					{t:'Venc.',k:'dueDate'},
					{t:'Cant.',k:'quantity'},
					{t:'Articulo',k:'itemCode',fText:Itm.Txt.code,ln:1},
					{k:'itemName',fText:Itm.Txt.name,ln:1,cs:4},
					{t:'Cliente',k:'cardName',cs:5},{k:'ref1',cs:2,ln:1},
					{k:'lineMemo',cs:8,addB:$1.t('b',{textNode:'Detalles:\u0020'}),HTML:1,Tag:{'class':'pre'}}
				],
				mTL:[
					{L:'L',fieldset:'Fases',TLs:[
						{t:'Fase',k:'wfaId',_g:$Tb.owfa},
						{t:'Planificado',k:'quantity',format:'number'},
						{t:'Pendiente',k:'openQty',format:'number'},
						{t:'Completo',k:'okQty',format:'number'},
						{t:'Rechazado',k:'rejQty',format:'number'}
					]},
					{L:'LF',fieldset:'Componentes',TLs:[
						{t:'Fase',k:'wfaId',_g:$Tb.owfa},
						{t:'Código',k:'itemCode',fText:Itm.Txt.code},
						{t:'Descripción',k:'itemName',fText:Itm.Txt.name},
						{t:'Precio',k:'price',format:'$'},
						{t:'Requerido',k:'quantity',format:'number'},
						{t:'Udm',k:'udm',_g:$V.Udm},
						{t:'Total',k:'priceLine',format:'$'},
						{t:'Tipo',k:'lineType'},
						{t:'Detalles',k:'lineText'}
					]}
				]
			};
			$Doc.view(cont,tP);
		}});
	},
}

Wma.Pdp={
	OLg:function(L){
		var Li=[];
		var ab=new $Doc.liBtn(Li,L,{api:Api.Wma.pr+'pdp',tbSerie:'wmaPdp'});
		ab.add('v');
		if(L.docStatus=='O'){ ab.add('E'); ab.add('C'); }
		ab.add('N'); ab.add('R');ab.add('L');  
		Li.push({plus:'Y',k:'mrp',ico:'fa fa-bolt',textNode:'Requerimiento de Materiales', P:L, func:function(T){ $M.to('wmaMrp.fromPdp','docEntry:'+T.P.docEntry); } });
		return $Opts.add('wmaPdp',Li,L);;
	},
	opts:function(P,pare){
		Li={Li:Wma.Pdp.OLg(P.L),PB:P.L,textNode:P.textNode};
		var mnu=$1.Menu.winLiRel(Li);
		if(pare){ pare.appendChild(mnu); }
		return mnu;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Doc.tbList({api:Api.Wma.pr+'pdp',inputs:$1.G.filter(),
		fOpts:Wma.Pdp.opts,view:'Y',docBy:'userDate',docUpd:'userDate',
		tbSerie:'wmaPdp',
		TD:[
			{H:'Estado',k:'docStatus',_V:'docStatus',_colMt:{k:'base',v:'docStatus'}},
			{H:'Fecha',k:'docDate'},
			{H:'Vencimiento',k:'dueDate'},
			{H:'Detalles',k:'lineMemo'},
			{H:'',_fNode:function(L){
				var td=$1.t('div');
					$1.t('a',{href:$M.to('wmaPdp.orders','docEntry:'+L.docEntry,'r'),'class':'iBg iBg_supervisor',textNode:'Ordenes'},td);
				return td;
			}}
		]
		},cont);
	},
	form:function(){
		var D=$Cche.d(0,{});
		var cont=$M.Ht.cont;
		var Pa=$M.read();
		$Api.get({f:Api.Wma.pr+'pdp/form',inputs:'docEntry='+Pa.docEntry,loadVerif:!Pa.docEntry,loade:cont,func:function(Jr){
			if(Jr.docEntry){ D=Jr; }
			if(!D.docDate){ D.docDate=$2d.today; }
			var crdVal=(D.cardId)?D.cardName:'';
			$Doc.form({tbSerie:'wmaPdp',docEdit:Pa.docEntry,cont:cont,POST:Api.Wma.pr+'pdp',func:D.func,
			HLs:[
				{lTag:'date',L:'Fecha Inicial',wxn:'wrapx8',req:'Y',I:{name:'docDate',value:D.docDate}},
				{lTag:'select',wxn:'wrapx8',L:'Tipo',req:'Y',I:{name:'docType',opts:$JsV.wmaPdpType,selected:D.docType}},
				{lTag:'date',L:'Vencimiento',wxn:'wrapx8',req:'Y',I:{name:'dueDate',value:D.dueDate}},
				{divLine:1,lTag:'textarea',L:'Detalles',wxn:'wrapx1',I:{name:'lineMemo',textNode:D.lineMemo}}
			],
			tbL:{xNum:'Y',xDel:'Y',uniqLine:'Y',L:D.L,itmSea:'prdItem',bCode:'Y',rteIva:D.rteIva,rteIca:D.rteIca,
			kTb:'gvtItmL',AJs:[{}],
			kFie:'itemCode,itemName,quantity,udm'
			}
			});
		}});
	},
	view:function(){
		var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
		$Api.get({f:Api.Wma.pr+'pdp/view',inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
			Jr.L=$js.sortBy('lineNum',Jr.L);
			//organizar
			var tb=$1.T.table([{textNode:'Código',style:'width:6rem;'},'Descripción',{textNode:'UdM',style:'width:3rem;'},'Programado','Pendiente','']);
			var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas de Documento'}}); cont.appendChild(fie);
			tb.classList.add('table_x100');
			var tBody=$1.t('tbody',0,tb);
			if(Jr.L && Jr.L.errNo){
				$1.t('td',{colspan:6,textNode:Jr.L.text},$1.t('tr',0,tBody));
			}
			else{ for(var i in Jr.L){ var L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var tr2=$1.t('tr',0,tBody);
				$1.t('td',{textNode:Itm.Txt.code(L)},tr);
				$1.t('td',{textNode:Itm.Txt.name(L)},tr);
				$1.t('td',{textNode:_g(L.udm,Udm.O)},tr);
				$1.t('td',{textNode:L.quantity*1},tr);
				$1.t('td',{textNode:L.openQty*1},tr);
				var tdB=$1.t('td',0,tr);
				$1.T.btnFa({fa:'fa_listWin __btnSellOrdSend',textNode:' Generar Orden',title:'Generar Orden de Producción', P:{docEntry:Jr.docEntry,L:L}, func:function(T){
					L=T.P.L;
					$Cche.d({AJs:{ott:'wmaPdp',otr:T.P.docEntry}, itemId:L.itemId,itemSzId:L.itemSzId,itemCode:L.itemCode,itemName:L.itemName,quantity:L.openQty*1,lineMemo:'Basado en Doc #'+T.P.docEntry});
					$M.to('wmaDop.form');
				}},tdB);
			}}
			//draw
			var tP={tbSerie:'wmaPdp',D:Jr,middleCont:fie,
				main:Wma.Pdp.OLg,
				THs:[
					{sdocNum:1},{sdocTitle:1,cs:5,ln:1},{t:'Estado',k:'docStatus',_V:'docStatus',ln:1},
					{t:'Fecha',k:'docDate'},{middleInfo:'Y'},{logo:'Y'},
					{t:'Fecha Venc.',k:'dueDate'},
					{k:'docType',_g:$V.oPdpType,cs:2},
					{k:'lineMemo',cs:8,addB:$1.t('b',{textNode:'Descripción:\u0020'}),HTML:1,Tag:{'class':'pre'}},
				]
			};
			$Doc.view(cont,tP);
		}});
	}
}

Wma.Dpf={
	OLg:function(L){
		var Li=[];
		var ab=new $Doc.liBtn(Li,L,{api:Api.Wma.pr+'dpf',tbSerie:'wmaDpf'});
		ab.add('v');
		ab.add('R'); ab.add('L');
		return $Opts.add('wmaDpf',Li,L);;
	},
	opts:function(P,pare){
		Li={Li:Wma.Dpf.OLg(P.L),PB:P.L,textNode:P.textNode};
		var mnu=$1.Menu.winLiRel(Li);
		if(pare){ pare.appendChild(mnu); }
		return mnu;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Doc.tbList({api:Api.Wma.pr+'dpf',inputs:$1.G.filter(),
		fOpts:Wma.Dpf.opts,view:'Y',docBy:'userDate',
		tbSerie:'wmaDpf',
		TD:[
			{H:'Estado',k:'docStatus',_V:'dStatus'},
			{H:'Fecha',k:'docDate',dateText:'mmm d'},
			{H:'Fase',k:'wfaId',_g:$Tb.owfa},
			{H:'Código',k:'docDate',fText:Itm.Txt.code},
			{H:'Descripción',k:'docDate',fText:Itm.Txt.name},
			{H:'Cant.',k:'quantity',format:'number'},
		]
		},cont);
	},
	form:function(){
		var D=$Cche.d(0,{});
		D.docDate=$2d.today;
		var cont =$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.Wma.pr+'dop/form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
			if(Jr.docEntry){ D=Jr; }
			var sSub=Itm.Sea.sub({D:D,'class':'_itemInfo',itemType:'P',vSea:'I.prdItem=Y',funcAll:function(L){
				if(L.itemId){
					var wfaId=$1.q('._wfaId').value;
					Wma.Bom.fases({itemId:L.itemId,itemSzId:L.itemSzId,wfaId:wfaId},function(Jr2){
						Wma.Ht.bomReq(midCont.childNodes[0],{reset:'Y',L:Jr2.L});
						Wma.Ht.bomLs(midCont.childNodes[1],{reset:'Y',LS:Jr2.LS});
					});
				}
				else{
					Wma.Ht.bomReq(midCont.childNodes[0],{reset:'Y'});
					Wma.Ht.bomLs(midCont.childNodes[1],{reset:'Y'});
				}
			}});
			var midContf=function(Jr,cont){
				var midCont=$1.t('div',{'class':'midCont'},cont);
				$1.t('div',0,midCont);
				$1.t('div',0,midCont);
				Wma.Ht.bomReq(midCont.childNodes[0],{L:Jr.L});
				Wma.Ht.bomLs(midCont.childNodes[1],{});
			}
			var jsF=$Api.JS.cls;
			$Api.form2({api:Api.Wma.pr+'dpf',AJs:D.AJs,PUTid:Pa.docEntry,JrD:D,vidn:'docEntry',to:'wmaDop.view',midCont:midContf,
			tbH:[
				{divLine:1,L:'Fecha',req:'Y',wxn:'wrapx8',I:{tag:'date','class':jsF,name:'docDate',value:D.docDate}},
				{L:'Fase',wxn:'wrapx8',req:'Y',I:{tag:'select','class':'_wfaId '+jsF,name:'wfaId',opts:$Tb.owfa,noBlank:'Y',selected:D.wfaId}},
				{L:'Articulo',req:'Y',wxn:'wrapx4',Inode:sSub},
				{L:'Cantidad',req:'Y',wxn:'wrapx8',I:{lTag:'number','class':jsF+' '+tbCal.rcellsA,name:'quantity',value:D.quantity}},
				{L:'Almacen Ing.',req:'Y',wxn:'wrapx9',I:{lTag:'select','class':jsF,name:'whsId',value:D.whsId,opts:$Tb.itmOwhsPeP}},
				{L:'Orden Prod.',wxn:'wrapx9',I:{lTag:'number','class':jsF,name:'pdocEntry',value:D.pdocEntry}},
			],
			reqFields:{
				D:[{k:'wfaId',iMsg:'Fase'},{k:'itemId',iMsg:'Articulo'}, {k:'itemSzId',iMsg:'Articulo (2)'},{k:'quantity',iMsg:'Cantidad'}, {k:'docDate',iMsg:'Fecha'},{k:'whsId',iMsg:'Almacen Ingreso'}
				],
				LF:[{k:'lineType',iMsg:'Tipo de Linea'},
					{k:'itemId',iMsg:'Articulo'},
					{k:'itemSzId',iMsg:'Articulo (2)'},
					{k:'quantity',iMsg:'Cantidad Requerida'}
				],
			}
			},cont);
			var midCont=$1.q('.midCont',cont);
			tbCal.rcells(cont,function(){ tbCal.docTotal(midCont); });
			if(D.itemId){
				Wma.Bom.fases({itemId:D.itemId,itemSzId:D.itemSzId,wfaId:D.wfaId},function(Jr2){
					Wma.Ht.bomReq(midCont.childNodes[0],{reset:'Y',L:Jr2.L});
					Wma.Ht.bomLs(midCont.childNodes[1],{reset:'Y',LS:Jr2.LS});
				});
			}
		}});
	},
	view:function(){
		var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
		$Api.get({f:Api.Wma.pr+'dpf/view',inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
			function trLineC(L,t){
				if(L.lineType=='PeP'){ return 'Fase'; }
				else{ return Itm.Txt.code(L); }
			}
			function trLineN(L,t){
				if(L.lineType=='PeP'){ return _g(L.wfaId,$Tb.owfa); }
				else{ return Itm.Txt.name(L); }
			}
			var midCont=$1.t('div');
			var tP={tbSerie:'wmaDpf',D:Jr,
				main:Wma.Dpf.OLg,midCont:midCont,
				THs:[
					{k:'docEntry'},{docTitle:'Documento de Fase',cs:5,ln:1},{t:'Fecha',k:'docDate',ln:1},
					{t:'Cant.',k:'quantity'},{t:'Fase',k:'wfaId',_g:$Tb.owfa,ln:1,cs:2},
					{t:'Ingreso a',k:'whsId',_g:$Tb.itmOwhsPeP,ln:1,cs:2},
					{t:'Articulo',k:'itemCode',fText:Itm.Txt.code},
					{k:'itemName',fText:Itm.Txt.name,ln:1,cs:6},
					{k:'lineMemo',cs:8,addB:$1.t('b',{textNode:'Detalles:\u0020'}),HTML:1,Tag:{'class':'pre'}}
				]
			};
			var WM=$1.tabs([
				{textNode:'Costes',winClass:'costes',active:'Y','class':'fa fa-money'},
				{textNode:'Componentes',winClass:'bom','class':'fa fa-cubes'},
			],midCont);
			var tb=$1.T.table(['Concepto','Total','Unitario'],0,WM.costes);
			var trh=$1.q('thead tr',tb);
			var leyen=$1.t('p',0,WM.costes);
			var tBody=$1.t('tbody',0,tb);
			XS=[
				{t:'Costo Documento',n:'',docTotal:Jr.docTotal},
				{t:'Costo Estandar',n:2,docTotal:Jr.docTotal2},
				{t:'Costo Promedio',n:3,docTotal:Jr.docTotal3},
			];
			for(var i in XS){ X=XS[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('th',{textNode:X.t},tr);
				$1.t('td',{textNode:$Str.money(X.docTotal)},tr);
				$1.t('td',{textNode:$Str.money(X.docTotal/Jr.quantity),style:'backgroundColor:#CCC'},tr);
				for(var z in $V.wmaBomType){
					Z=$V.wmaBomType[z];
					$1.t('td',{textNode:$Str.money(Jr[Z.k+'total'+X.n])},tr);
					if(i==0){
						$1.t('th',{textNode:Z.k},trh);
						var d1=$1.t('span',0,leyen);
						$1.t('span',{'class':'badge bf-n2',textNode:Z.k},d1);
						$1.t('span',{textNode:' '+Z.v},d1);
					}
				}
			}
			/*bom*/
			var cssSep='backgroundColor:#CCC';
			var tb=$1.T.table(['Tipo','Articulo','Coste','Cant.','Udm','Coste Total','','Coste Base','Cant. Base','','Desviación $','Desviación'],0,WM.bom);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.LF){ L=Jr.LF[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.lineType},tr);
				if(L.lineType=='PeP'){
					$1.t('td',{textNode:'Fase: '+_g(L.wfaId,$Tb.owfa)},tr);
				}
				else{ $1.t('td',{textNode:Itm.Txt.name(L)},tr); }
				$1.t('td',{textNode:$Str.money(L.price)},tr);
				$1.t('td',{textNode:L.quantity*1},tr);
				$1.t('td',{textNode:_g(L.udm,Udm.O)},tr);
				$1.t('td',{textNode:$Str.money(L.priceLine)},tr);
				$1.t('td',{style:cssSep},tr);
				$1.t('td',{textNode:$Str.money(L.priceLine2)},tr);
				$1.t('td',{textNode:L.reqQty*1},tr);
				$1.t('td',{style:cssSep},tr);
				$1.t('td',{textNode:$Str.money(L.diffPrice)},tr);
				$1.t('td',{textNode:L.diffQty*1},tr);
			}
			$Doc.view(cont,tP);
		}});
	},
}

Wma.Drs={
	OLg:function(L){
		var Li=[];
		var ab=new $Doc.liBtn(Li,L,{api:Api.Wma.pr+'drs',tbSerie:'wmaDrs'});
		ab.add('v');
		ab.add('R'); ab.add('L');
		return $Opts.add('wmaDrs',Li,L);;
	},
	opts:function(P,pare){
		Li={Li:Wma.Drs.OLg(P.L),PB:P.L,textNode:P.textNode};
		var mnu=$1.Menu.winLiRel(Li);
		if(pare){ pare.appendChild(mnu); }
		return mnu;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Doc.tbList({api:Api.Wma.pr+'drs',inputs:$1.G.filter(),
		fOpts:Wma.Drs.opts,view:'Y',docBy:'userDate',
		tbSerie:'wmaDrs',
		TD:[
			{H:'Estado',k:'docStatus',_V:'dStatus'},
			{H:'Fecha',k:'docDate',dateText:'mmm d'},
			{H:'Fase',k:'wfaId',_g:$Tb.owfa},
			{H:'Tercero',k:'cardName'},
			{H:'Almacen',k:'whsId'},
		]
		},cont);
	},
	form:function(){
		var D=$Cche.d(0,{});
		D.docDate=$2d.today;
		var cont =$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.Wma.pr+'drs/form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
			if(Jr.docEntry){ D=Jr; }
			function midContf (Jr,cont){
				var midCont=$1.t('div',{'class':'midCont'},cont);
				$1.t('div',0,midCont);
				$1.t('div',0,midCont);
				var tb=$1.T.table(['Código','Descripción','Fase Ant.','Almacen','Cant Entregada'],0,midCont.childNodes[0])
				$1.t('tbody',{'class':'_tBodyItems'},tb);
			}
			var jsF=$Api.JS.cls;
			var card=$1.lTag({tag:'card',cardType:'S',func:function(){}});
			$Api.form2({api:Api.Wma.pr+'drs',AJs:D.AJs,PUTid:Pa.docEntry,JrD:D,vidn:'docEntry',to:'wmaDrs.view',midCont:midContf,
			tbH:[
				{divLine:1,L:'Fecha',req:'Y',wxn:'wrapx8',I:{tag:'date','class':jsF,name:'docDate',value:D.docDate}},
				{L:'Fase',wxn:'wrapx8',req:'Y',I:{xtag:'wmaWfa','class':jsF,selected:D.wfaId}},
				{L:'Tercero',req:'Y',wxn:'wrapx4',Inode:card},
				{L:'Almacen',req:'Y',wxn:'wrapx9',I:{xtag:'pepWhs','class':jsF,selected:D.whsId}},
				{divLine:1,L:'Detalles',wxn:'wrapx1',I:{lTag:'textarea','class':jsF,name:'lineMemo',value:D.lineMemo}},
			],
			reqFields:{
				D:[{k:'wfaId',iMsg:'Fase'},{k:'docDate',iMsg:'Fecha'},{k:'whsId',iMsg:'Almacen Proceso'},{k:'cardId',iMsg:'Tercero'}
				],
				LF:[
					{k:'wfaId',iMsg:'Fase'},
					{k:'itemId',iMsg:'Articulo'},
					{k:'itemSzId',iMsg:'Articulo (2)'},
					{k:'quantity',iMsg:'Cantidad Requerida'}
				],
			}
			},cont);
			var midCont=$1.q('.midCont',cont);
			//$1.lTag({tag:'itmSub',D:D,vSea:'I.prdItem=Y',funcAll:function(L){
			$1.xtag('itmSub',{D:D,vSea:'I.prdItem=Y',func:function(L){
				tBody=$1.q('._tBodyItems',midCont);
				var jsF=$Api.JS.clsLN;
				var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
				$1.t('td',{textNode:Itm.Txt.code(L)},tr);
				$1.t('td',{textNode:Itm.Txt.name(L)},tr);
				$1.t('td',{textNode:_g(L.wfaIdBef,$Tb.owfa)},tr);
				td=$1.t('td',0,tr);
				$1.lTag({tag:'select','class':jsF,name:'whsId',opts:$Tb.itmOwhsPeP,value:L.whsIdBef},td);
				td=$1.t('td',0,tr);
				$1.lTag({tag:'number','class':jsF,name:'quantity'},td).AJs={itemId:L.itemId,itemSzId:L.itemSzId,wfaId:L.wfaIdBef};
			}},midCont);
		}});
	},
	view:function(){
		var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
		$Api.get({f:Api.Wma.pr+'dpf/view',inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
			function trLineC(L,t){
				if(L.lineType=='PeP'){ return 'Fase'; }
				else{ return Itm.Txt.code(L); }
			}
			function trLineN(L,t){
				if(L.lineType=='PeP'){ return _g(L.wfaId,$Tb.owfa); }
				else{ return Itm.Txt.name(L); }
			}
			var midCont=$1.t('div');
			var tP={tbSerie:'wmaDpf',D:Jr,
				main:Wma.Dpf.OLg,midCont:midCont,
				THs:[
					{k:'docEntry'},{docTitle:'Documento de Fase',cs:5,ln:1},{t:'Fecha',k:'docDate',ln:1},
					{t:'Cant.',k:'quantity'},{t:'Fase',k:'wfaId',_g:$Tb.owfa,ln:1,cs:2},
					{t:'Ingreso a',k:'whsId',_g:$Tb.itmOwhsPeP,ln:1,cs:2},
					{t:'Articulo',k:'itemCode',fText:Itm.Txt.code},
					{k:'itemName',fText:Itm.Txt.name,ln:1,cs:6},
					{k:'lineMemo',cs:8,addB:$1.t('b',{textNode:'Detalles:\u0020'}),HTML:1,Tag:{'class':'pre'}}
				]
			};
			var WM=$1.tabs([
				{textNode:'Costes',winClass:'costes',active:'Y','class':'fa fa-money'},
				{textNode:'Componentes',winClass:'bom','class':'fa fa-cubes'},
			],midCont);
			var tb=$1.T.table(['Concepto','Total','Unitario'],0,WM.costes);
			var trh=$1.q('thead tr',tb);
			var leyen=$1.t('p',0,WM.costes);
			var tBody=$1.t('tbody',0,tb);
			XS=[
				{t:'Costo Documento',n:'',docTotal:Jr.docTotal},
				{t:'Costo Estandar',n:2,docTotal:Jr.docTotal2},
				{t:'Costo Promedio',n:3,docTotal:Jr.docTotal3},
			];
			for(var i in XS){ X=XS[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('th',{textNode:X.t},tr);
				$1.t('td',{textNode:$Str.money(X.docTotal)},tr);
				$1.t('td',{textNode:$Str.money(X.docTotal/Jr.quantity),style:'backgroundColor:#CCC'},tr);
				for(var z in $V.wmaBomType){
					Z=$V.wmaBomType[z];
					$1.t('td',{textNode:$Str.money(Jr[Z.k+'total'+X.n])},tr);
					if(i==0){
						$1.t('th',{textNode:Z.k},trh);
						var d1=$1.t('span',0,leyen);
						$1.t('span',{'class':'badge bf-n2',textNode:Z.k},d1);
						$1.t('span',{textNode:' '+Z.v},d1);
					}
				}
			}
			/*bom*/
			var cssSep='backgroundColor:#CCC';
			var tb=$1.T.table(['Tipo','Articulo','Coste','Cant.','Udm','Coste Total','','Coste Base','Cant. Base','','Desviación $','Desviación'],0,WM.bom);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.LF){ L=Jr.LF[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.lineType},tr);
				if(L.lineType=='PeP'){
					$1.t('td',{textNode:'Fase: '+_g(L.wfaId,$Tb.owfa)},tr);
				}
				else{ $1.t('td',{textNode:Itm.Txt.name(L)},tr); }
				$1.t('td',{textNode:$Str.money(L.price)},tr);
				$1.t('td',{textNode:L.quantity*1},tr);
				$1.t('td',{textNode:_g(L.udm,Udm.O)},tr);
				$1.t('td',{textNode:$Str.money(L.priceLine)},tr);
				$1.t('td',{style:cssSep},tr);
				$1.t('td',{textNode:$Str.money(L.priceLine2)},tr);
				$1.t('td',{textNode:L.reqQty*1},tr);
				$1.t('td',{style:cssSep},tr);
				$1.t('td',{textNode:$Str.money(L.diffPrice)},tr);
				$1.t('td',{textNode:L.diffQty*1},tr);
			}
			$Doc.view(cont,tP);
		}});
	},
}



Wma.Odp={};
Wma.Odp.Lop={
form:function(){
	var cont=$M.Ht.cont;
	var Pa=$M.read();
	$Api.get({f:Api.Wma.pr+'lop',inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		if(Jr.errNo){ return $Api.resp(cont,Jr); }
		$1.t('h4',{textNode:'Orden de Producción: '+Pa.docEntry},cont);
		var resp=$1.t('div'); var jsF=$Api.JS.cls;
		if(Jr.created=='Y'){
			$Api.send({textNode:'Eliminar Estos Códigos',DELETE:Api.Wma.pr+'lop',jsBody:divL,jsAdd:Pa,loade:resp,xConf:'Los códigos serán eliminados, deberá definirlos nuevamente.',winErr:1,func:function(Jr2){
				Wma.Odp.Lop.form();
			}},cont);
		}
		if(Jr.created=='N'){
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',req:'Y',L:'Agrupar cada',I:{tag:'input',type:'number',inputmode:'numeric',min:1,max:30,'class':jsF,name:'var1'}},cont);
			$1.T.divL({wxn:'wrapx8',L:'Si resto menor a',subText:'Se añade a fila 1',I:{tag:'input',type:'number',inputmode:'numeric',min:1,max:30,'class':jsF,name:'var2'}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Repeticion',I:{tag:'input',type:'number',inputmode:'numeric',min:1,max:2,'class':jsF,name:'var3',value:1}},divL);
			$Api.send({POST:Api.Wma.pr+'lop',jsBody:divL,jsAdd:Pa,loade:resp,func:function(Jr2){
				$Api.resp(resp,Jr2);
				if(!Jr2.errNo){ Wma.Odp.Lop.form(); }
			}},cont);
		} cont.appendChild(resp);
		if(Jr.L && !Jr.L.errNo){
			var tb=$1.T.table(['Código Lote','Código','Talla','Descripción','Traz.','Cant. Traz']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.lopId},tr);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:_g(L.itemSzId,$V.grs1)},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:L.opTraz},tr);
				$1.t('td',{textNode:L.quantity*1},tr);
			}
			var tb=$1.T.tbExport(tb,{ext:'xlxs',tab:1,fileName:'Listado de Códigos Lotes Orden Produción # '+Pa.docEntry});
			cont.appendChild(tb);
		}
	}});
},
cat:function(){
	var cont=$M.Ht.cont;
	var breads=$1.t('div');
	var tb=$1.T.table(['Código Lote','Código','Talla','Descripción'],0,breads);
	var tBody=$1.t('tbody',0,tb);
	var Ds={};
	$BRead.inp(function(v,R){
		if(R.ex){ $1.Win.message(R.ex); }
		else{
			$Api.get({f:Api.Wma.pr+'lop/code',inputs:'lopId='+v,winErr:1,func:function(Jr){
				var kx=Jr.itemId+'_'+Jr.itemSzId;
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:Jr.lopId},tr);
				$1.t('td',{textNode:Jr.itemCode},tr);
				$1.t('td',{textNode:_g(Jr.itemSzId,$V.grs1)},tr);
				$1.t('td',{textNode:Jr.itemName},tr);
				if(!Ds[kx]){ Ds[kx]=Jr; Ds[kx].quantity=0; }
				Ds[kx].quantity+=1;
			}});
		}
	},{ex:1},cont);
	cont.appendChild(breads);
	var bts=$1.t('p',0,cont);
	$1.T.btnFa({faBtn:'fa-file-o',textNode:'Documento Transferencia',func:function(){
			$M.Cch({L:Ds}); $Doc.go('pepWht','f',0,1);
		}},bts);
		$1.T.btnFa({faBtn:'fa-file-o',textNode:'Documento Ingreso',func:function(){
			$M.Cch({L:Ds}); $Doc.go('pepIng','f',0,1);
		}},bts);
		$1.T.btnFa({faBtn:'fa-file-o',textNode:'Documento Salida',func:function(){
			$M.Cch({L:Ds}); $Doc.go('pepEgr','f',0,1);
		}},bts);
}
};

Wma.Pdp.corteProg=function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma.pr+'pdp/cortePlan', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			$Api.respWarn(cont,Jr);
			var viewType=$1.q('.__viewType').value;
			var tb=$1.T.table(['Código','Descripción','Talla','= Pendiente','- Almacen',{textNode:'= Nec.',_i:'pdpNecesidad'},'- Planificado','- En Proceso',{textNode:'= A Planificar',_i:'pdpPorPlanificar'},{textNode:'Acumulado',_i:'pdpAcumulado'},{textNode:'Doc.',_iHelp:'Digite las cantidades que desea planificar para agilizar el proceso'}]);
			var tBody=$1.t('tbody',0,tb);
			var css1='backgroundColor:#EEE;';
			var css10='backgroundColor:#d24907;';
			Jr.L=$js.sortNum(Jr.L,{k:'itemCode'});
			for(var i in Jr.L){ var L=Jr.L[i];
				var pEp=(L.pEp.onHand)?L.pEp.onHand*1:0;
				var progr=(L.progr)?L.progr*1:0;
				var nec1=(L.onHand>0)?L.openQty*1-L.onHand:L.openQty*1;
				var nec2=(nec1>0)?nec1-progr-pEp:0;
				var nec1t=(nec1<0)?'':nec1;
				var nec2t=(nec2<=0)?'':nec2;
				var onHandR=(L.onHand>0)?L.onHand:0;
				var acum=onHandR*1-L.openQty*1+progr+pEp;
				var acumt=(acum>0)?acum:'';
				if(viewType=='aProgramar' && (nec2t=='')){ }
				else{
				var css=(nec2t!='')?'backgroundColor:#C000EE;':'';
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:Itm.Txt.size(L)},tr);
				$1.t('td',{textNode:L.openQty,'class':tbSum.tbColNums,tbColNum:10},tr);
				$1.t('td',{textNode:L.onHand*1,'class':tbSum.tbColNums,tbColNum:11},tr);
				$1.t('td',{textNode:nec1t,style:css1,'class':tbSum.tbColNums,tbColNum:12},tr);
				$1.t('td',{textNode:((progr!=0)?progr:''),'class':tbSum.tbColNums,tbColNum:13},tr);
				$1.t('td',{textNode:((pEp!=0)?pEp:''),'class':tbSum.tbColNums,tbColNum:14},tr);
				$1.t('td',{textNode:nec2t,style:css,'class':tbSum.tbColNums,tbColNum:15},tr);
				$1.t('td',{textNode:acumt,style:((acum>0)?css10:''),'class':tbSum.tbColNums,tbColNum:16},tr);
					var td=$1.t('td',0,tr);
				var inp=$1.t('input',{type:'number',inputmode:'numeric',value:nec2t,'class':'wmaPdPDocLine',style:'width:4rem;',min:0},td);
				inp.D={itemId:L.itemId,itemCode:L.itemCode,itemName:L.itemName,itemSzId:L.itemSzId};
				}
			}
			var tr=$1.t('tr',0,tBody);
			$1.t('td',0,tr);
			$1.t('td',{texNode:'Total'},tr);
			$1.t('td',0,tr);
			$1.t('td',{'class':tbSum.tbColNumTotal+'10'},tr);
			$1.t('td',{'class':tbSum.tbColNumTotal+'11'},tr);
			$1.t('td',{'class':tbSum.tbColNumTotal+'12'},tr);
			$1.t('td',{'class':tbSum.tbColNumTotal+'13'},tr);
			$1.t('td',{'class':tbSum.tbColNumTotal+'14'},tr);
			$1.t('td',{'class':tbSum.tbColNumTotal+'15'},tr);
			$1.t('td',{'class':tbSum.tbColNumTotal+'16'},tr);
			tbSum.get(tb);
			var tb=$1.T.tbExport(tb,{ext:'xlsx',fileName:'Corte para Programación '+$2d.today,print:1}); cont.appendChild(tb);
			$1.T.btnFa({fa:'fa-doc',textNode:'Generar Estructura Doc. Planificación',func:function(){
				var Ds=[];
				var lis=$1.q('.wmaPdPDocLine',cont,'all');
				for(var i=0; i<lis.length; i++){
					if(lis[i].value>0){
						lis[i].D.quantity=lis[i].value;
						Ds.push(lis[i].D);
					}
				}
				$M.Ht.ini({func_cont:function(){
					Wma.Pdp.form({docType:'venta',docDate:$2d.today,L:Ds,lineMemo:'Basado en Corte Planif. '+$2d.today+'. '});
				}});
			}
			},cont);
		}
	}});
}
Wma.Pdp.auxCumpProd=function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma.pr+'pdp/cortePlan', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $ps_DB.response(cont,Jr); }
		else{
			var viewType=$1.q('.__viewType');
			var css0='backgroundColor:#DDD;';
			var tb=$1.T.table(['Código','Descripción','Talla',{textNode:'= Nec.',_i:'pdpNecesidad'},'- Planificado','- En Proceso',{textNode:'= Por Planificar',_i:'pdpPorPlanificar'}]);
			var trh=$1.q('tr',tb);
			$1.t('td',{style:css0},trh);
			for(var i in Jr._W){ $1.t('td',{textNode:Jr._W[i].k},trh); }
			var tBody=$1.t('tbody',0,tb);
			var css1='backgroundColor:#EEE;';
			var css10='backgroundColor:#d24907;';
			Jr.L=$js.sortNum(Jr.L,{k:'itemCode'});
			for(var i in Jr.L){ var L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var pEp=(L.pEp.onHand)?L.pEp.onHand*1:0;
				var progr=(L.progr)?L.progr*1:0;
				var nec1=(L.onHand>0)?L.openQty*1-L.onHand:L.openQty*1;
				if(nec1<=0){ continue; }
				var nec2=(nec1>0)?nec1-progr-pEp:'';
				var nec1t=(nec1<0)?'':nec1;
				var nec2t=(nec2<0)?'':nec2;
				var acum=L.onHand*1-L.openQty*1+progr+pEp;
				var acumt=(acum>0)?acum:'';
				var css=(nec2==0)?'backgroundColor:#0E0;':'';
				$1.t('td',{textNode:L.itemCode},tr);
				$1.t('td',{textNode:L.itemName},tr);
				$1.t('td',{textNode:Itm.Txt.size(L)},tr);
				$1.t('td',{textNode:nec1t,style:css1,'class':tbSum.tbColNums,tbColNum:12},tr);
				$1.t('td',{textNode:((progr!=0)?progr:''),'class':tbSum.tbColNums,tbColNum:13},tr);
				$1.t('td',{textNode:((pEp!=0)?pEp:''),'class':tbSum.tbColNums,tbColNum:14},tr);
				$1.t('td',{textNode:nec2t,style:css,'class':tbSum.tbColNums,tbColNum:15},tr);
				$1.t('td',{style:css0},tr);
				var ntd=30;
				for(var i in Jr._W){ var tk=Jr._W[i].k;
					var qo=(L.pEp && L.pEp[tk])?L.pEp[tk]*1:'';
					$1.t('td',{textNode:qo,'class':tbSum.tbColNums,tbColNum:ntd},tr); ntd++;
				}
			}
			var tr=$1.t('tr',0,tBody);
			$1.t('td',0,tr);
			$1.t('td',{texNode:'Total'},tr);
			$1.t('td',0,tr);
			$1.t('td',{'class':tbSum.tbColNumTotal+'12'},tr);
			$1.t('td',{'class':tbSum.tbColNumTotal+'13'},tr);
			$1.t('td',{'class':tbSum.tbColNumTotal+'14'},tr);
			$1.t('td',{'class':tbSum.tbColNumTotal+'15'},tr);
			$1.t('td',{style:css0},tr);
			var ntd=30;
			for(var i in Jr._W){ var tk=Jr._W[i].k;
				$1.t('td',{'class':tbSum.tbColNumTotal+ntd},tr); ntd++;
			}
			tbSum.get(tb);
			var tb=$1.T.tbExport(tb,{ext:'xlsx',fileName:'Corte para Programación '+$2d.today,print:1}); cont.appendChild(tb);
		}
	}});
}

Wma3.Odp.docHistory=function(){
	var cont=$M.Ht.cont;
	var origen=$1.q('.__origenDoc');
	var vPost=$1.G.filter();
	$Api.get({f:Api.Wma.b+'odp/docHistory', inputs:vPost,loade:cont, func:function(Jr){
		if(Jr && Jr.errNo){ $Api.resp(cont,Jr); return false; }
		var tb=$1.T.table(['Origen','Estado','Código','Descripción','Cant.','Almacen','Fase','Tipo Linea','Motivo','Tercero']);
		cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			var lineT='N/A'; var lineR='';
			if(origen.value=='wmaDcf'){
				lineT=_g(L.lineType,$V.wma3DdfLineType);
				lineR=(L.lineType=='C')?'':_g(L.rejReason,$V.wma3DcfRejs);
			}
			var td=$1.t('td',0,tr);
			$Doc.href(L.ttDoc,L,td);
			$1.t('td',{textNode:_g(L.docStatus,$V.dStatus)},tr);
			$1.t('td',{textNode:Itm.Txt.code(L)},tr);
			$1.t('td',{textNode:Itm.Txt.name(L)},tr);
			$1.t('td',{textNode:L.quantity*1},tr);
			$1.t('td',{textNode:_g(L.whsId,$Tb.whsPeP)},tr);
			$1.t('td',{textNode:_g(L.wfaId,$Tb.owfa)},tr);
			$1.t('td',{textNode:lineT},tr);
			$1.t('td',{textNode:lineR},tr);
			$1.t('td',{textNode:L.cardName},tr);
		}
	}});
}
Wma3.Odp.itemStatusFase=function(){
	var cont=$M.Ht.cont; Pa=$M.read(); var jsF='jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',req:'Y',L:'Código',I:{tag:'input',type:'number',inputmode:'numeric','class':jsF,name:'itemCode'}},cont);
	$1.T.divL({wxn:'wrapx4',L:'Fase',req:'Y',I:{tag:'select','class':jsF,name:'wfaId',opts:$Tb.owfa}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Talla',I:{tag:'select','class':jsF,name:'itemSzId',opts:$V.grs1}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Inicio',I:{tag:'input',type:'date','class':jsF,name:'A.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:'Fecha Fin',I:{tag:'input',type:'date','class':jsF,name:'A.docDate(E_menIgual)'}},divL);
	var wList=$1.t('div');
	var btn=$Api.send({textNode:'Consultar Estado',f:Api.Wma.b+'odp/itemStatusFase', getInputs:function(){ return $1.G.filter(cont); }, loade:wList,func:function(Jr){
		if(Jr.errNo){ $Api.resp(wList,Jr); return false; }
		else if(Jr.L.errNo){ $Api.resp(wList,Jr.L); return false; }
		var tb=$1.T.table(['No. Doc','Talla','Prog','Completo','En Proceso','Rechazado','Pendiente']); wList.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		var ccsTr='backgroundColor:#DDD;';
		for(var f in Jr.L){ var L=Jr.L[f];
		var css1=(L.quantity==L.compQty)?'backgroundColor:#00EE00':'';
			var tr=$1.t('tr',{},tBody);
			$1.t('td',{textNode:L.docEntry},tr);
			$1.t('td',{textNode:_g(L.itemSzId,$V.grs1),style:css1},tr);
			$1.t('td',{textNode:L.quantity*1,style:css1},tr);
			$1.t('td',{textNode:L.compQty*1,style:css1},tr);
			$1.t('td',{textNode:L.onProQty*1},tr);
			$1.t('td',{textNode:L.rejQty*1},tr);
			$1.t('td',{textNode:L.openQty*1},tr);
		}
	}},cont);
	cont.appendChild(wList);
};

Wma3.Ddf={
opts:function(P,pare){
	var L=P.L; var Jr=P.Jr;
	var Li=[]; var n=0;
	Li[n]={ico:'fa fa_pencil',textNode:' Visualizar Documento', P:L, func:function(T){ $M.to('wma3.ddf.view','docEntry:'+L.docEntry); } }; n++;
	Li[n]={ico:'fa fa-history',textNode:' Logs de Documento', P:L, func:function(T){ $Doc.tb99({api:Api.Wma.b+'ddf/tb99',serieType:'gvtPdn',docEntry:T.P.docEntry}); } }; n++;
	Li[n]={ico:'fa fa_prio_high',textNode:' Anular Documento', P:L, func:function(T){ $Doc.cancel({docEntry:T.P.docEntry,api:Api.Wma.b+'ddf/statusCancel',text:'Se va anular el documento de fase. Se realizan los movimientos inversos en el inventario y se actualizan las fases de las ordenes de producción que estén relacionadas. También si aplica, se actualiza la planificación de producción.'}); } }; n++;
	Li=$Opts.add('wmaDdf',Li,L);
	Li={Li:Li,PB:L,textNode:P.textNode};
	var mnu=$1.Menu.winLiRel(Li);
	if(pare){ pare.appendChild(mnu); }
	return mnu;
},
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma.b+'ddf', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','N°','Estado','Tipo','','Fecha Doc.','Fase','Alm. Proceso','Proveedor','Alm. Anterior','Realizado','Detalles']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				Wma3.Ddf.opts({L:L},td);
				$Doc.href('wmaDdf',L,$1.t('td',0,tr));
				$1.t('td',{textNode:_g(L.docStatus,$V.docStatus)},tr);
				$1.t('td',{textNode:_g(L.docType,$V.wma3DdfType)},tr);
				var td=$1.t('td',0,tr);
				$1.T.btnFa({fa:'fa_transfer',textNode:'Recibir',P:L,func:function(T){ $M.to('wma3.dcf.fromDdf','docEntry:'+T.P.docEntry);}},td);
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:$Tb._g('owfa',L.wfaId)},tr);
				$1.t('td',{textNode:$Tb._g('whsPeP',L.whsId)},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:$Tb._g('whsPeP',L.whsIdFrom)},tr);
				$1.t('td',{textNode:$Doc.by('userDate',L)},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
			}
			cont.appendChild(tb);
		}
	}});
},
view:function(){
	var Pa=$M.read(); var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma.b+'ddf/view', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); return false; }
		$DocT.B.h({docEntry:Pa.docEntry,dateC:Jr.dateC,serieType:'wmaDdf',print:'Y', styDef:'width:6rem;',styT:'font-weight:bold;',
		Ls:[{t:'Estado',v:_g(Jr.docStatus,$V.docStatus)},{middleInfo:'Y'},{logoRight:'Y'},
			{t:'Fecha', v:Jr.docDate},
			{t:'Tipo',v:_g(Jr.docType,$V.wma3DdfType)},
			{t:'Fase',v:$Tb._g('owfa',Jr.wfaId)},{t:'Alm. Proceso',v:$Tb._g('whsPeP',Jr.whsId),ln:1},{t:'Proveedor',v:Jr.cardName,cs:3,ln:1},
			{t:'Detalles',v:Jr.lineMemo,cs:5},{t:'Alm. Anterior',v:$Tb._g('whsPeP',Jr.whsIdFrom),ln:1},
		]
		},cont);
		var tb=$1.T.table(['#','Serie','Número','Código',{textNode:'Descripción'},{textNode:'Fase Anterior',style:'width:5rem;'},{textNode:'Cant.',style:'width:5rem;'}]);
		$1.t('p',0,cont);
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del documento'}}); cont.appendChild(fie);
		tb.classList.add('table_x100');
		var tBody=$1.t('tbody',0,tb);
		if(Jr.L && Jr.L.errNo){
			$1.t('td',{colspan:6,textNode:Jr.L.text},$1.t('tr',0,tBody));
		}else{
		Jr.L= $js.sortNum(Jr.L,{k:'itemCode'});
		var va='vertical-align:middle';
		var n=1;
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',{'class':tbCal._row},tBody);
			$1.t('td',{textNode:n},tr); n++;
			$1.t('td',{textNode:L.tt},tr); n++;
			$1.t('td',{textNode:L.tr},tr); n++;
			$1.t('td',{textNode:Itm.Txt.code(L)},tr);
			$1.t('td',{textNode:Itm.Txt.name(L)},tr);
			$1.t('td',{textNode:$Tb._g('owfa',L.wfaIdBef)},tr);
			$1.t('td',{textNode:L.quantity*1,'class':tbCal._cell,ncol:1},tr);
		}
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{colspan:6,style:'text-align:right;',textNode:'Total'},tr);
		$1.t('td',{'class':tbCal._cell+'_1'},tr);
		tbCal.sumCells(tb);
		$1.t('div',{textNode:$Soc.softFrom,style:'font-size:0.75rem; text-align:center; padding:0.25rem;'},cont);
	}});
},
form:function(){
	var cont=$M.Ht.cont; var n=1;
	var tb=$1.T.table(['#','Serie','Número','Código','Descripción','Cantidad','']);
	var opt1={k:'N999',t:'Ninguno'};
	var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Documento'}});
	$Doc.form({tbSerie:'wmaDdf',cont:cont,midCont:fie,Jr:{}, POST:Api.Wma.pr+'ddf',
	HLs:[
	{lTag:'select',wxn:'wrapx8',req:'Y',L:'Tipo',I:{name:'docType',opts:$V.wma3DdfType}},
	{lTag:'date',wxn:'wrapx8',req:'Y',L:'Fecha',I:{name:'docDate',value:$2d.today}},
	{lTag:'date',wxn:'wrapx8',req:'Y',L:'Vencimiento',I:{name:'dueDate'}},
	{fType:'crd',wxn:'wrapx3',L:'Tercero',req:'Y'},
	{divLine:1,lTag:'select',wxn:'wrapx4',req:'Y',L:'Obtener Componentes de',I:{name:'whsIdFrom',opts:$Tb.whsPeP}},
	{lTag:'select',wxn:'wrapx4',req:'Y',L:'Fase a Realizar',I:{name:'wfaId','class':'__wfaId',opts:$Tb.owfa}},
	{lTag:'select',wxn:'wrapx4',L:'Almacen en Proceso',req:'Y',I:{name:'whsId',opts:$Tb.whsPeP}},
	{divLine:1,lText:'textarea',wxn:'wrapx1',L:'Detalles',I:{name:'lineMemo'}},
	]});
	var wfaDo=$1.q('.__wfaId',cont);
	wfaDo.onchange=function(){ tBody.innerHTML=''; }
	var tBody= $1.t('tbody',0,tb);
	var n=1;
	Wma3.Fx.seaItmFase({func:trA2,reqFase:'__wfaId'},cont);
	Wma3.Fx.seaOdp20({func:trA2,reqFase:'__wfaId'},cont);
	function trA2(Ds){
		var optSerie=[{k:'',v:'Ninguna'},{k:'wmaOdp',v:'Ord. Producción'}];
		for(var i in Ds){ var L=Ds[i];
			var jsF=$Api.JS.clsLN;
			if(L.openQty){ L.quantity=L.openQty; }
			var kc=L.docEntry+'_'+L.itemId+'_'+L.itemSzId+'_'+L.wfaId;
			if($Htm.uniqLine(kc,tBody,{win:'Y',cNode:0})){ return false; }
			var tr=$1.t('tr',{'class':$Api.JS.clsL+' '+$Htm.uLk+kc},tBody);
			$1.t('td',{textNode:n},tr); n++;
			var tt=(L.docEntry)?'wmaOdp':null;
			var trr=L.docEntry;
			var AJs={itemId:L.itemId,itemSzId:L.itemSzId,wfaIdBef:L.wfaId};
			var td=$1.t('td',0,tr);
			$1.T.sel({'class':jsF,name:'tt',opts:optSerie,noBlank:'Y',selected:tt},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF,name:'tr',value:L.docEntry,style:'width:6rem;'},td);
			$1.t('td',{textNode:Itm.Txt.code(L)},tr);
			$1.t('td',{textNode:Itm.Txt.name(L)},tr);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF,name:'quantity',value:L.quantity,AJs:AJs,style:'width:6rem;'},td);
				var td=$1.t('td',0,tr);
			$1.T.btnFa({fa:'fa_close',textNode:'Eliminar',func:function(T){ $1.delet(T.parentNode.parentNode); }},td);
		}
	}
},
}

Wma3.Dcf={
OLi:[],
OLg:function(L){
	var Li=[]; var n=0;
	Li[n]={k:'view',ico:'fa fa_pencil',textNode:' Visualizar Documento', P:L, func:function(T){ $M.to('wma3.dcf.view','docEntry:'+L.docEntry); } }; n++;
	Li[n]={k:'transfer',ico:'fa fa_transfer',textNode:' Consumo de materiales', P:L, func:function(T){ $M.to('wma3.dcf.emision','docEntry:'+T.P.docEntry); } }; n++;
	Li[n]={k:'logs',ico:'fa fa-history',textNode:' Logs de Documento', P:L, func:function(T){ $Doc.tb99({api:Api.Wma.b+'dcf/tb99',serieType:'gvtPdn',docEntry:T.P.docEntry}); } }; n++;
	Li[n]={k:'statusC',ico:'fa fa_prio_high',textNode:' Anular Documento', P:L, func:function(T){ $Doc.cancel({docEntry:T.P.docEntry,api:Api.Wma.b+'dcf/statusCancel',text:'Se va anular el documento de cierre. Se realizan los movimientos inversos en el inventario y se actualizan las fases de las ordenes de producción que estén relacionadas.'}); } }; n++;
	Wma3.Dcf.OLi=$Opts.add('wmaDcf',Li,L);
	return Wma3.Dcf.OLi;
},
opts:function(P,pare){
	Li=Wma3.Dcf.OLg(P.L);
	Li={Li:Li,PB:P.L,textNode:P.textNode};
	var mnu=$1.Menu.winLiRel(Li);
	if(pare){ pare.appendChild(mnu); }
	return mnu;
},
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma.b+'dcf', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','N°','Fecha Doc.','Estado','Consumos','Origen','Socios Neg.','Realizado','Detalles']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				Wma3.Dcf.opts({L:L},td);
				$Doc.href('wmaDcf',L,$1.t('td',0,tr));
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:_g(L.docStatus,$V.docStatus)},tr);
				var td=$1.t('td',0,tr);
				var faEmi=(L.emiStatus=='C')?'fa fa_history':'fa_transfer';
				$1.T.btnFa({fa:faEmi,textNode:'Registro Consumos',P:L,func:function(T){ $M.to('wma3.dcf.emision','docEntry:'+T.P.docEntry);}},td);
				var td=$1.t('td',0,tr);
				if(L.tt=='wmaDdf'){
					$Doc.href(L.tt,{docEntry:L.tr},{pare:td,format:'serie'});
				}
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:$Doc.by('userDate',L)},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
			}
			cont.appendChild(tb);
		}
	}});
},
view:function(){
	var Pa=$M.read(); var contR=$M.Ht.cont;
	$Api.get({f:Api.Wma.b+'dcf/view', inputs:'docEntry='+Pa.docEntry,loade:contR, func:function(Jr){
		if(Jr.errNo){ $Api.resp(contR,Jr); return false; }
		var cont=$1.t('div',0,contR);
		$Doc.btnsTop('print,transfer,logs,statusC,',{icons:'Y',Li:Wma3.Dcf.OLg(Jr),contPrint:cont},contR);
		$DocT.B.h({docEntry:Pa.docEntry,dateC:Jr.dateC,serieType:'wmaDcf',styDef:'width:6rem;',styT:'font-weight:bold;',
		Ls:[{t:'Estado',v:_g(Jr.docStatus,$V.docStatus)},{middleInfo:'Y'},{logoRight:'Y'},
			{t:'Fecha',v:Jr.docDate},{t:' ',v:' '},
			{t:'Tercero',v:Jr.cardName,cs:7},
			{t:'Detalles',v:Jr.lineMemo,cs:7}
		]
		},cont);
		var tb=$1.T.table(['Origen','Código','Descripción','Fase Realizada',,'Almacen Componentes','Almacen Ingreso','Tipo',{textNode:'Cant.',style:'width:5rem;'},'Mot. Rechazo']);
		$1.t('p',0,cont);
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del documento'}}); cont.appendChild(fie);
		tb.classList.add('table_x100');
		var tBody=$1.t('tbody',0,tb);
		if(Jr.L && Jr.L.errNo){
			$1.t('td',{colspan:6,textNode:Jr.L.text},$1.t('tr',0,tBody));
		}else{
		Jr.L= $js.sortNum(Jr.L,{k:'itemCode'});
		var va='vertical-align:middle';
		var n=1;
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:L.tt+'-'+L.tr},tr);
			$1.t('td',{textNode:Itm.Txt.code(L)},tr);
			$1.t('td',{textNode:Itm.Txt.name(L)},tr);
				$1.t('td',{textNode:_g(L.wfaId,$Tb.owfa)},tr);
			$1.t('td',{textNode:_g(L.whsIdFrom,$Tb.whsPeP)},tr);
			$1.t('td',{textNode:_g(L.whsId,$Tb.whsPeP)},tr);
			$1.t('td',{textNode:_g(L.lineType,$V.wma3DdfLineType)},tr);
			$1.t('td',{textNode:L.quantity*1,'class':tbSum.tbColNums,tbColNum:6},tr);
			$1.t('td',{textNode:_g(L.rejReason,$V.wma3DcfRejs,'')},tr);
		}
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{colspan:7,style:'text-align:right;',textNode:'Total'},tr);
		$1.t('td',{'class':tbSum.tbColNumTotal+'6'},tr);
		$1.t('td',0,tr);
		tbSum.get(tb);
		$1.t('div',{textNode:$Soc.softFrom,style:'font-size:0.75rem; text-align:center; padding:0.25rem;'},cont);
	}});
},
form:function(){
	var cont=$M.Ht.cont; var jsF='jsFields'; var n=1;
	var tb=$1.T.table(['#','Serie','Número','Código','Descripción',{textNode:'Tipo',title:'Tipo de Recepción'},'Fase Realizada','Almacen Componentes','Bod. Ingreso','Cantidad','Mot. Rechazo','']);
	var opt1={k:'N999',t:'Ninguno'};
	var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del Documento'}});
	$Doc.formSerie({cont:cont, jsF:jsF, middleCont:fie, serieType:'wmaDcf',Jr:{}, POST:Api.Wma.b+'dcf',func:function(Jr2){ $Doc.href('wmaDcf',Jr2,'to'); },
	Li:[
	{fType:'user'},
	{wxn:'wrapx8',fType:'date',req:'Y',name:'docDate',value:$2d.today},
	{wxn:'wrapx8',req:'Y',L:'Tipo',I:{tag:'select',sel:{name:'docType','class':jsF},opts:$V.wma3DdfType}},
	{fType:'crd',wxn:'wrapx4',L:'Tercero',req:'Y'},
	{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'input',type:'text',name:'lineMemo','class':jsF}},
	]});
	var tBody= $1.t('tbody',0,tb);
	Wma3.Fx.itmFase2Table({func:trA2},cont);
	Wma3.Fx.seaItmFase({func:trA2},cont);
	Wma3.Fx.seaOdp20({func:trA2,openQtyFrom:'fase'},cont);
	var n=1;
	function trA2(Ds){
		for(var i in Ds){ var L=Ds[i];
			var ln='L['+n+']';
			var tr=$1.t('tr',0,tBody);
			var td0=$1.t('td',{textNode:n,'data-vPost':'Y'},tr); n++;
			td0.vPost=ln+'[itemId]='+L.itemId+'&'+ln+'[itemSzId]='+L.itemSzId;
			var optSerie=[{k:'',v:'Ninguna'},{k:'wmaOdp',v:'Ord. Producción'}];
			var tt=(L.docEntry)?'wmaOdp':null;
			var qty=L.quantity;
			if(L.openQty){ qty=L.openQty; }
			var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsF,name:ln+'[tt]'},opts:optSerie,noBlank:'Y',selected:tt},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF,name:ln+'[tr]',value:L.docEntry,style:'width:6rem;'},td);
			$1.t('td',{textNode:Itm.Txt.code(L)},tr);
			$1.t('td',{textNode:Itm.Txt.name(L)},tr);
			var td=$1.t('td',0,tr);
				$1.T.sel({sel:{'class':jsF+' _lineType',name:ln+'[lineType]'},opts:$V.wma3DdfLineType,noBlank:'Y'},td);
			$1.q('._lineType',td).onchange=function(){
				var rej=$1.q('.__rejReason',this.parentNode.parentNode);
				if(this.value=='R'){ rej.classList.add(jsF); rej.removeAttribute('disabled'); }
				else{ rej.classList.remove(jsF); rej.disabled='disabled'; }
			}
			var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsF,name:ln+'[wfaId]'},opts:$Tb.owfa,selected:L.wfaId},td);
			var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsF,name:ln+'[whsIdFrom]'},opts:$Tb.whsPeP,selected:L.whsIdBef},td);
			var td=$1.t('td',0,tr);
			$1.T.sel({sel:{'class':jsF,name:ln+'[whsId]'},opts:$Tb.whsPeP,selected:L.whsId},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF,name:ln+'[quantity]',value:qty,style:'width:6rem;'},td);
			var td=$1.t('td',0,tr);
				$1.T.sel({sel:{'class':'__rejReason',name:ln+'[rejReason]',disabled:'disabled'},opts:$V.wma3DcfRejs},td);
			var td=$1.t('td',0,tr);
			$1.T.btnFa({fa:'fa_close',textNode:'Eliminar',func:function(T){ $1.delet(T.parentNode.parentNode); }},td);
		}
	}
},
fromDdf:function(){
	var Pa=$M.read(); var cont=$M.Ht.cont; var jsF='jsFields';
	$Api.get({f:Api.Wma.b+'ddf/view', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); return false; }
		$DocT.B.h({docEntry:Pa.docEntry,dateC:Jr.dateC,serieType:'wmaDdf',print:'Y', styDef:'width:6rem;',styT:'font-weight:bold;',
		Ls:[{t:'Estado',v:_g(Jr.docStatus,'docStatus')},{middleInfo:'Y'},{logoRight:'Y'},
			{t:'Fecha', v:Jr.docDate},
			{t:'Tipo',v:_g(Jr.docType,$V.wma3DdfType)},
			{t:'Fase',v:$Tb._g('owfa',Jr.wfaId)},{t:'Alm. Proceso',v:$Tb._g('whsPeP',Jr.whsId),ln:1},{t:'Proveedor',v:Jr.cardName,cs:3,ln:1},
			{t:'Detalles',v:Jr.lineMemo,cs:5},{t:'Alm. Anterior',v:$Tb._g('whsPeP',Jr.whsIdFrom),ln:1},
		]
		},cont);
		var tb=$1.T.table(['#',{textNode:'N.F',title:'Nota Fabricación'},'Serie','Número','Código',{textNode:'Descripción'},{textNode:'Fase Anterior',style:'width:5rem;'},{textNode:'Alm. Ingreso',style:'width:5rem;'},{textNode:'Tipo',style:'width:5rem;'},{textNode:'Ent.',style:'width:5rem;',title:'Entregado'},{textNode:'Pend.',title:'Pendiente',style:'width:5rem;'},{textNode:'Cant.',style:'width:5rem;'},{textNode:'Mot. Rechazo'}]);
		$1.t('p',0,cont);
		var fie=$1.T.fieldset(tb,{L:{textNode:'Lineas del documento'}}); cont.appendChild(fie);
		tb.classList.add('table_x100');
		var tBody=$1.t('tbody',0,tb);
		if(Jr.L && Jr.L.errNo){
			$1.t('td',{colspan:6,textNode:Jr.L.text},$1.t('tr',0,tBody));
		}else{
		Jr.L= $js.sortNum(Jr.L,{k:'itemCode'});
		var va='vertical-align:middle';
		var n=1;
		for(var i in Jr.L){ var L=Jr.L[i];
			var tr=$1.t('tr',0,tBody); var ln='L['+n+']';
			$1.t('td',{textNode:n},tr); n++;
			$1.t('td',{textNode:L.nfId},tr);
			$1.t('td',{textNode:L.tt},tr);
			$1.t('td',{textNode:L.tr},tr);
			$1.t('td',{textNode:Itm.Txt.code(L)},tr);
			$1.t('td',{textNode:Itm.Txt.name(L)},tr);
			$1.t('td',{textNode:$Tb._g('owfa',L.wfaIdBef)},tr);
			if(L.openQty==0){
				var td=$1.t('td',{colspan:5},tr);
				$1.t('span',{textNode:'Completado'},td);
			}
			else{
				var vPost=ln+'[nfId]='+L.nfId;
				var td=$1.t('td',0,tr);
				$1.T.sel({sel:{'class':jsF,name:ln+'[whsId]'},opts:$Tb.whsPeP,selected:L.whsId},td);
				var td=$1.t('td',0,tr);
				$1.T.sel({sel:{'class':jsF+' _lineType',name:ln+'[lineType]'},opts:$V.wma3DdfLineType,noBlank:'Y'},td);
				$1.q('._lineType',td).onchange=function(){
					var rej=$1.q('.__rejReason',this.parentNode.parentNode);
					if(this.value=='R'){ rej.classList.add(jsF); rej.removeAttribute('disabled'); }
					else{ rej.classList.remove(jsF); rej.disabled='disabled'; }
				}
				$1.t('td',{textNode:L.quantity*1,'class':tbSum.tbColsNum,tbColNum:2},tr);
				$1.t('td',{textNode:L.openQty*1,'class':tbSum.tbColsNum,tbColNum:3},tr);
				var td=$1.t('td',0,tr);
				$1.t('input',{type:'number',inputmode:'numeric',min:0,'class':jsF+' '+tbSum.tbColsNum,tbColNum:6,O:{vPost:vPost},name:ln+'[quantity]'},td);
				var td=$1.t('td',0,tr);
				$1.T.sel({sel:{'class':'__rejReason',name:ln+'[rejReason]',disabled:'disabled'},opts:$V.wma3DcfRejs},td);
			}
		}
		}
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{colspan:9,style:'text-align:right;',textNode:'Total'},tr);
		$1.t('td',{'class':tbSum.tbColNumTotal+'2'},tr);
		$1.t('td',{'class':tbSum.tbColNumTotal+'3'},tr);
		$1.t('td',{'class':tbSum.tbColNumTotal+'6'},tr);
		tbSum.get(tb);
		$1.t('div',{textNode:$Soc.softFrom,style:'font-size:0.75rem; text-align:center; padding:0.25rem;'},cont);
		var resp=$1.t('div',0,cont);
		$Api.send({textNode:'Recepción',Conf:{text:'Se generan el documento de cierre y se actualizará el inventario. Esta acción no se puede revertir.'},
		POST:Api.Wma.b+'dcf/fromDdf',loade:resp,getInputs:function(){ return 'docEntry='+Pa.docEntry+'&'+$1.G.inputs(cont); }, func:function(Jr2){
			$Api.resp(resp,Jr2);
		}},cont);
	}});
},
emision:function(){
		var Pa=$M.read(); var cont=$M.Ht.cont;
	$Api.get({f:Api.Wma.b+'dcf/emision', inputs:'docEntry='+Pa.docEntry,loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['Código','Descripción','Cant.','UdM','Fase']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:Itm.Txt.code(L)},tr);
				$1.t('td',{textNode:Itm.Txt.name(L)},tr);
				$1.t('td',{textNode:L.quantity*1},tr);
				$1.t('td',{textNode:_g(L.udm,Udm.O)},tr);
				$1.t('td',{textNode:$Tb._g('owfa',L.wfaId)},tr);
			}
			/* Consolidado Entregas */
			var tb=$1.T.table(['Código','Descripción','Cant. Base','Cant. Real','UdM','Desviación','Factor Compra']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.Lc){ L=Jr.Lc[i];
				var tr=$1.t('tr',0,tBody);
				var reqQty=(L.reqQty)?L.reqQty*1:0;
				var realQty=(L.realQty)?L.realQty*1:'';
				var diff= $js.toFixed(reqQty-realQty,3);
				$1.t('td',{textNode:Itm.Txt.code(L)},tr);
				$1.t('td',{textNode:Itm.Txt.name(L)},tr);
				$1.t('td',{textNode:reqQty},tr);
				$1.t('td',{textNode:realQty},tr);
				$1.t('td',{textNode:_g(L.udm,Udm.O)},tr);
				$1.t('td',{textNode:diff},tr);
				$1.t('td',{textNode:L.buyFactor*1},tr);
			}
			fie=$1.T.fieldset(tb,{L:{textNode:'Consolidado Despachos'}}); cont.appendChild(fie);
			/* Registros */
			var tb=$1.T.table(['Código','Descripción','Cant. Consumo','UdM','Almacen','Detalle','Factor Compra','']);
			var tBody=$1.t('tbody',0,tb); var jsF='jsFields'; var n=0;
			for(var i in Jr.Lm){ L=Jr.Lm[i];
				trA(L,tBody);
			}
			function trA(L,tBody){
				var ln='Lm['+n+']'; n++;
				var kc=$Htm.uLk+L.itemId;
				// if($Htm.uniqLine(L.itemId,tBody,{win:'Y',cNode:0})){ return false; }
				var tr=$1.t('tr',{'class':kc+' '+tbCal._row,'data-vPost':'Y'},tBody);
				var reqQty=(L.reqQty)?L.reqQty*1:0;
				var realQty=(L.realQty)?L.realQty*1:'';
				var diff=(L.diff)?L.diff*1:'';
				$1.t('td',{textNode:Itm.Txt.code(L)},tr);
				$1.t('td',{textNode:Itm.Txt.name(L)},tr);
				var td=$1.t('td',0,tr);
				tr.vPost=(L.id)?ln+'[id]='+L.id+'&':'';
				tr.vPost +=ln+'[itemId]='+L.itemId+'&'+ln+'[itemSzId]='+L.itemSzId+'&'+ln+'[reqQty]='+reqQty;
				$1.t('input',{type:'number',inputmode:'numeric','class':jsF+' '+tbCal._cell,cn:2,name:ln+'[realQty]',value:realQty,style:'width:6rem;',onkeychange:function(T){
					tbCal.get(tb,['cn2','-','cn1']);
				}},td);
				$1.t('td',{textNode:_g(L.udm,Udm.O)},tr);
				var td=$1.t('td',{},tr);
				$1.lTag({tag:'select','class':jsF,name:ln+'[whsId]',value:L.whsId,opts:$Tb.itmOwhs,kIf:{whsType:'MP'}},td);
				var td=$1.t('td',{},tr);
				$1.t('input',{type:'input','class':jsF,type:'text',name:ln+'[detail]',value:L.detail},td);
				$1.t('td',{textNode:L.buyFactor*1},tr);
				var td=$1.t('td',0,tr);
				if(L.id){ $1.T.ckLabel({t:'Borrar',I:{name:ln+'[delete]','class':jsF}},td); }
				else{ $1.T.btnFa({faBtn:'fa_close',title:'Quitar Linea',P:tr,func:function(T){ $1.delet(T.P) } },td); }
			}
			fie=$1.T.fieldset(tb,{L:{textNode:'Materiales'}}); cont.appendChild(fie);
			fie.appendChild(Itm.Fx.sea({itemType:'MP',vPost:'I.handInv=Y',func:function(Ds){
				for(var i in Ds){ trA(Ds[i],tBody); }
			}},fie));
			var resp=$1.t('div',0,cont);
			$Api.send({textNode:'Actualizar Emisión',PUT:Api.Wma.b+'dcf/emision', getInputs:function(){ return 'docEntry='+Pa.docEntry+'&'+$1.G.inputs(cont); },loade:resp, func:function(Jr2){ $Api.resp(resp,Jr2);
				if(!Jr2.errNo){ Wma3.Dcf.emision(); }
				}},cont);
			$1.t('p',{textNode:' '},cont);
			$1.t('h3',{textNode:'Cierre de Emisiones','class':'head1'},cont);
			$1.t('p',{textNode:'Ejecute está opción para realizar los movimientos de inventario de los materiales.'},cont);
			var resp2=$1.t('div',0,cont);
			$Api.send({textNode:'Cerrar Emisiones',Conf:{text:'Se realizarán los consumos en el inventario, no se puede modificar está acción una vez realizada.'},
			POST:Api.Wma.b+'dcf/emisionClose', getInputs:function(){ return 'docEntry='+Pa.docEntry+'&'+$1.G.inputs(cont); },loade:resp2, func:function(Jr2){
					$Api.resp(resp2,Jr2);
				} },cont);
		}
	}});
}
}

$JsV._i({kObj:'wmaDopCls',mdl:'wma',liK:'wma',liTxtG:'Clases Ord. Produccion',liTxtF:'Clase Orden Producción (Form)'});

$JsV._i({kObj:'wmaPdpType',mdl:'wma',liK:'wma',liTxtG:'Tipos Planificacion',liTxtF:'Tipo Planificación (Form)'});


/* Sysd */
$M.li['sysd.wmaDef']={t:'Parametros de Producción',kau:'sysd.suadmin', func:function(){ $M.Ht.ini({func_pageAndCont:function(){
	$Sysd.a0crd2.get({mdlk:'wma3'});
}}); }};
$M.li['sysd.massData.wmaWfa']={t:'Fases Producción - Actualización Multiple',kau:'sysd.suadmin', func:function(){ $M.Ht.ini({gyp:function(){ $Sysd.MassData.form({k:'wmaWfa'}) }}); }};

sHt.wmaOdp=function(Jr,cont){
	var td=$1.t('div');
	$1.t('div',{textNode:$Soc.address},td);
	$1.t('div',{textNode:'PBX: '+$Soc.pbx},td);
	$1.t('div',{textNode:$Soc.mail},td);
	$1.t('div',{textNode:$Soc.web},td);
	var logo=$1.t('img',{style:'width:20rem;',src:$Soc.logo});
	var tr4_1=(Jr.tr4_1)?Jr.tr4_1:{t:'',v:''};
	var Ls=[
	{v:'Estado: '+Jr.docStatusText},{v:Jr.docTitle,vSty:'text-align:center; font-weight:bold;',cs:6,ln:1},{v:'Creado: '+Jr.dateC,ln:1},
	{t:'Número',v:Jr.docEntry},
		{v:td,vSty:'width:20rem; text-align:center; vertical-align:middle',ln:1,cs:4,rs:3},
		{v:logo,vSty:'width:20rem;text-align:right;',ln:1,cs:2,rs:3},
	{t:'Fecha Doc.',v:Jr.docDate,vSty:'width:7rem;'},
	{t:'Entregar',v:Jr.dueDate},
	{t:'Tipo',v:_g(Jr.docType,$V.oPdpType)},{t:'Ref.',v:Jr.ref1,ln:1},{t:'Cliente',v:Jr.cardName,ln:1,cs:5},
	{t:'Notas',v:Jr.lineMemo,cs:7}
	];
	var tb=$1.Tb.trCols(Ls,{cols:8,styT:'width:7rem; font-weight:bold;'},cont);
}
