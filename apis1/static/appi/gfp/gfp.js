Api.Gfp={a:'/js/gfp/'};

$V.gfpMovType=[{k:'G',v:'Gasto'},{k:'I',v:'Ingreso'},{k:'IS',v:'Otros Ingresos'},{k:'GS',v:'Otros Gastos'},{k:'R',v:'Retiros'}];
$V.gfpMovTypeWB=[{k:'IS',v:'Otros Ingresos'},{k:'GS',v:'Otros Gastos'},{k:'G',v:'Gasto'},{k:'I',v:'Ingreso'}];
$V.gfpWallType=[{k:'EF',v:'Efectivo'},{k:'CB',v:'Cuenta Bancaria'},{k:'TC',v:'T. Crédito'},{k:'CR',v:'Crédito'},{k:'IP',v:'Ing. Proximos'},{k:'CC',v:'CxC'},{k:'PT',v:'Prest. Terc.'},{k:'SP',v:'Separado'}];
$V.gfpCredType=[{k:'compra',v:'Compra'},{k:'pago',v:'Pago'},{k:'interes',v:'Interes'},{k:'seguro',v:'Seguro'},{k:'manejo',v:'Manejo'},{k:'rediferido',v:'Rediferido'},{k:'avance',v:'Avance'}];

_Fi['gfp.wal']=function(wrap){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Fecha Corte',I:{tag:'input',type:'date','class':jsV,name:'M.docDate(E_menIgual)'}},wrap);
		$1.T.divL({wxn:'wrapx8', L:'Tipo',I:{tag:'select','class':jsV,name:'A.wallType',opts:$V.gfpWallType}},divL);
	$1.T.divL({wxn:'wrapx4', L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'A.wallName(E_like3)'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:Gfp.Wal.get},wrap);
};
_Fi['gfp.mov']=function(wrap){
	var jsV = 'jsFiltVars';
	var Pa=$M.read();
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Fecha Inicio',I:{tag:'input',type:'date','class':jsV,name:'M.docDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Corte',I:{tag:'input',type:'date','class':jsV,name:'M.docDate(E_menIgual)'}},divL);
		$1.T.divL({wxn:'wrapx8', L:'Tipo Mov.',I:{tag:'select','class':jsV,name:'M.mType',opts:$V.gfpMovType}},divL);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:'Billetera',I:{tag:'select','class':jsV,name:'M.wallId',opts:$Tb.gfpWal,selected:Pa.wallId}},wrap);
		$1.T.divL({wxn:'wrapx8', L:'Bolsillo',I:{tag:'select','class':jsV,name:'M.wbId',opts:$Tb.gfpBol,selected:Pa.wbId}},divL);
		$1.T.divL({wxn:'wrapx8', L:'Categoria',I:{tag:'select','class':jsV,name:'M.categId',opts:$JsV.gfpCateg}},divL);
		var ordBy=[{k:'',v:'Fecha Mayor'},{k:'docDateASC',v:'Fecha Menor'}]
		$1.T.divL({wxn:'wrapx8', L:'Orden',I:{tag:'select','class':jsV,name:'ordBy',opts:ordBy}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4', L:'Detalle',I:{tag:'input',type:'text','class':jsV,name:'M.lineMemo(E_like3)'}},wrap);
	$1.T.btnSend({textNode:'Actualizar', func:Gfp.Mov.get},wrap);
};

$M.li['gfp.wal']={t:'Billeteras', kau:'gfp',func:function(){
	var wrap=$1.t('div');
	$1.T.btnFa({fa:'fa_circlePlus',textNode:'Nueva',func:function(){ $M.to('gfp.wal.form'); }},wrap)
	$1.T.btnFa({fa:'fa-money',textNode:'Registrar Movimiento',func:function(){ $M.to('gfp.movForm'); }},wrap)
	$M.Ht.ini({btnNew:wrap, func_filt:'gfp.wal', func_cont:Gfp.Wal.get});
}};
$M.li['gfp.wal.bol']={t:'Bolsillos', kau:'gfp',func:function(){ $M.Ht.ini({func_cont:Gfp.Bol.get}); }};

$M.li['gfp.mov']={t:'Movimientos', kau:'gfp',func:function(){
	var wrap=$1.t('div');
	$1.T.btnFa({fa:'fa-money faBtnCtn',textNode:'Registrar Movimiento',func:function(){ $M.to('gfp.movForm'); }},wrap);
	$M.Ht.ini({btnNew:wrap, func_filt:'gfp.mov', func_pageAndCont:Gfp.Mov.get});
}};
$M.li['gfp.movForm']={t:'Registrar', kau:'gfp',func:function(){ $M.Ht.ini({func_cont:Gfp.Mov.form}); }};

$V.gfpTCjs={
cManejo:'C. Manejo', seguro:'Seguro',avances:"Avances",otros:"Otros",puntos:"Puntos"
};
var Gfp={};
Gfp.Wal={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Gfp.a+'wal',inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['',{textNode:'Tipo'},{textNode:'Nombre'},{textNode:'Disponible'},'',{textNode:'Deuda'},{textNode:'Cupo'},{textNode:'Cupo Disp.'},'','Detalles']);
			var css='backgroundColor:#EEE;';
			var cssSep='backgroundColor:#CCC;';
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',{'class':'wallId_'+L.wallId},tBody);
				var td=$1.t('td',0,tr);
				$1.t('td',{textNode:_g(L.wallType,$V.gfpWallType)},tr);
				var td=$1.t('td',{},tr);
				$1.T.btnFa({fa:'fa fa-money',P:L,func:function(T){ $M.to('gfp.mov','wallId:'+T.P.wallId)},title:'Visualizar Movimientos'},td);
				$1.t('span',{textNode:L.wallName},td);
				if(L.wallType=='TC'){
					var cssDeu='';
					var cssClose=($2d.diff({date1:L.closeDate})>=-7)?'backgroundColor:#FF0;':'';
					var percDeu=$js.toFixed((Math.abs(L.amount*1)/L.creditLine*1)*100,1);
					if(percDeu>50){ cssDeu='fontWeight:bold; color:purple;'; }
					else if(percDeu>40){ cssDeu='fontWeight:bold; color:red;'; }
					else if(percDeu>30){ cssDeu='fontWeight:bold; color:orange;'; }
					$1.t('td',{style:css},tr);
					$1.t('td',{style:cssSep},tr);
			$1.t('td',{textNode:$Str.money(L.amount),style:cssDeu,title:percDeu+' % Endeudamiento'},tr);
					$1.t('td',{textNode:$Str.money(L.creditLine)},tr);
					$1.t('td',{textNode:$Str.money(L.creditLine*1+L.amount*1)},tr);
					var td=$1.t('td',{style:cssClose},tr);
					var css1=(L.payDateMin<$2d.today)?'color:#0F0':'';
					$1.t('div',{textNode:'Pago Min.: '+$2d.f(L.payDateMin,'d mmm'),style:css1},td);
					$1.t('div',{textNode:'Avances: '+$Str.money(L.creditLineAvan)},td);
					$1.t('div',{textNode:'Corte : '+$2d.f(L.closeDate,'d mmm')},td);
					$1.t('div',{textNode:'Pago : '+$2d.f(L.payDate,'d mmm')},td);

				}
				else if(L.wallType=='CR'){
					$1.t('td',{style:css},tr);
					$1.t('td',{style:cssSep},tr);
					$1.t('td',{textNode:$Str.money(L.amount)},tr);
					$1.t('td',{style:css},tr);
					$1.t('td',{style:css},tr);
					$1.t('td',{style:css},tr);
				}
				else{
					$1.t('td',{textNode:$Str.money(L.amount)},tr);
					$1.t('td',{style:cssSep},tr);
					$1.t('td',{style:css},tr);
					$1.t('td',{style:css},tr);
					$1.t('td',{style:css},tr);
					$1.t('td',{style:css},tr);
				}
				var td=$1.t('td',0,tr);
				var jsTc=$js.parse(L.jsTc);
				if(jsTc){for(var ki in jsTc){
						var d=$1.t('div',0,td);
						$1.t('b',{textNode:$V.gfpTCjs[ki]},d);
						$1.t('span',{textNode:' '+jsTc[ki]},d);
				}}
				$1.t('div',{textNode:L.lineMemo},td);
				var Clo=$js.parse(L.jsData);
				if(Clo.Wbs){ for(var i in Clo.Wbs){
					var X=_gO(Clo.Wbs[i],Jr.wL);
					if(X){ X=$js.clone(X);
						X.cloned='Y';
						X.wallId=L.wallId;
						Jr.wL.push(X);
					}
				}}
			}
			/* Bolsillos */
			if(Jr.wL){ Jr.wL=$js.sortNum(Jr.wL,{k:'wbOrder',ksort:1}); }
			for(var i in Jr.wL){ L=Jr.wL[i];
				var wtr=$1.q('.wallId_'+L.wallId,tBody);
				var tr=$1.t('tr',{'class':'wallId_'+L.wallId+' wbId_'+L.wbId});
				$1.I.after(tr,wtr);
				var cssBol='backgroundColor:f7d2d2;';
				var txt='Bolsillo';
				var td=$1.t('td',0,tr);
				if(L.cloned=='Y'){
					txt='(R) Bolsillo'; cssBol='backgroundColor:d2f7d2;';
				}
				$1.t('td',{textNode:txt,style:cssBol},tr);
				var td=$1.t('td',{style:cssBol},tr);
				$1.T.btnFa({fa:'fa fa-money',P:L,func:function(T){ $M.to('gfp.mov','wbId:'+T.P.wbId)},title:'Visualizar Movimientos'},td);
				$1.t('span',{textNode:L.wbName,style:cssBol},td);
				$1.t('td',{textNode:$Str.money(L.amount),style:cssBol},tr);
				$1.t('td',{style:cssSep},tr);
				$1.t('td',{style:css},tr);
				$1.t('td',{style:css},tr);
				$1.t('td',{style:css},tr);
				$1.t('td',{style:css},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
			}
			tb=$1.T.tbExport(tb,{ext:'xlsx'});
			cont.appendChild(tb);
		}
	}});
},
}
Gfp.Bol={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Gfp.a+'wal/bol', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Billetera','Bolsillo','Saldo']);
			var css='backgroundColor:#EEE;';
			var tBody=$1.t('tbody',0,tb); cont.appendChild(tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.t('td',{textNode:L.wallName},tr);
				$1.t('td',{textNode:L.wbName},tr);
				$1.t('td',{textNode:$Str.money(L.amount)},tr);
			}
		}
	}});
},
}

Gfp.Mov={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Gfp.a+'mov',inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Tipo','Billetera','Valor','Fecha','Categoria','Bolsillo','TC Cuotas','TC Tipo','Detalles']);
			var css='backgroundColor:#EEE;';
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){L=Jr.L[i];
				var bal=(L.debBal>0)?L.debBal:'?';
				bal=(L.creBal>0)?L.creBal:bal;
				balCss=(L.creBal>0)?'color:red;':'color:green';
				var wbName=(L.wbId)?_g(L.wbId,$Tb.gfpBol):'';
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.t('td',{textNode:_g(L.mType,$V.gfpMovType)},tr);
				$1.t('td',{textNode:_g(L.wallId,$Tb.gfpWal)},tr);
				var td=$1.t('td',{textNode:$Str.money(bal),style:'fontWeight:bold; '+balCss},tr);
				L.editar='bal'; td.L=L;
				td.ondblclick=function(){
					Gfp.Mov.editLine(this.L,this);
				}
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:_g(L.categId,$JsV.gfpCateg)},tr);
				$1.t('td',{textNode:wbName},tr);
				$1.t('td',{textNode:L.credCuotas},tr);
				$1.t('td',{textNode:_g(L.credType,$V.gfpCredType)},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				var td=$1.t('td',0,tr);
				$Api.send({'class':'',textNode:'Borrar',DELETE:Api.Gfp.a+'mov', inputs:'mid='+L.mid,func:function(Jr2){
					$1.Win.message(Jr2);
				}},td);
			}
			tb=$1.T.tbExport(tb,{ext:'xlsx'});
			cont.appendChild(tb);
		}
	}});
},
form:function(P){ P=(P)?P:{};
	var cont=$M.Ht.cont; var Pa=$M.read();
	cont.innerHTML='';
	$Api.get({f:Api.Gfp.a+'mov',inputs:'mId='+Pa.mId,loadVerif:!Pa.mId,loade:cont, func:function(Jr){
		var wMov=$1.t('div',0,cont);
		var jsF=$Api.JS.clsLN;
		function trA(){
		var wm=$1.t('fieldset',0,wMov);
		var lg=$1.t('legend',{textNode:'Movimiento'},wm);
		$1.T.btnFa({fa:'fa_close',title:'Borrar movimiento',P:wm,func:function(T){ $1.delet(T.P); }},lg);
		var opts={a:'Cuenta',b:'Bolsillo'};
		var divL=$1.T.divL({divLine:1,wxn:'wrapx10',L:'Desde',I:{tag:'select','class':'inpFocus',opts:opts,noBlank:1}},wm);
		waSel('a',divL);
		wm.classList.add($Api.JS.clsL);
		var inp=$1.q('.inpFocus',wMov);
		inp.divL=divL;
		inp.onchange=function(){ waSel(this.value,this.divL); }
		inp.focus();
		inp.classList.remove('inpFocus');
		$1.T.divL({wxn:'wrapx8',L:'Fecha Mov.',req:'Y',I:{tag:'input',type:'date','class':jsF,name:'docDate',value:Pa.docDate}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Valor',req:'Y',I:{tag:'input',type:'text',numberformat:'mil','class':jsF,name:'bal',min:0}},divL);
		$1.T.divL({wxn:'wrapx8',L:'Categoria',req:'Y',I:{tag:'select','class':jsF,name:'categId',opts:$JsV.gfpCateg}},divL);
		$1.T.divL({wxn:'wrapx8',L:'TC Tipo',req:'Y',I:{tag:'select','class':jsF,name:'credType',opts:$V.gfpCredType}},divL);
		$1.T.divL({wxn:'wrapx8',L:'TC Cuotas',I:{tag:'input',type:'text','class':jsF,name:'credCuotas'}},divL);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Detalles',I:{tag:'input',type:'text','class':jsF,name:'lineMemo'}},wm);
			$1.T.divL({wxn:'wrapx4',L:'Tercero',I:{tag:'input',type:'text','class':jsF,name:'cardName'}},divL);
		}
		trA({});
		$1.t('br',0,cont);
		$1.T.btnFa({fa:'faBtnCtn fa_circlePlus',textNode:'Añadir Movimiento',func:trA},cont);
		var resp=$1.t('div',0,cont);
		$Api.send({POST:Api.Gfp.a+'mov',jsBody:cont,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ Gfp.Mov.form({respBef:Jr2.text}); }
		}},cont);
		//func
		function waSel(ty,pare){
			var old=$1.q('.walle',pare);
			var olty=$1.q('.__mtype',pare);
			if(ty=='b'){
				var nue=$1.T.divL({wxn:'wrapx8',L:'Bolsillo',req:'Y',I:{tag:'select','class':jsF,name:'wbId',opts:$Tb.gfpBol}});
				var nue2=$1.T.divL({wxn:'wrapx8',req:'Y',L:'Tipo',I:{tag:'select','class':jsF,name:'mType',opts:$V.gfpMovTypeWB,noBlank:1}});
			}
			else{
				var nue=$1.T.divL({wxn:'wrapx8',L:'cuenta',req:'Y',I:{tag:'select','class':jsF,name:'wallId',opts:$Tb.gfpWal}});
				var nue2=$1.T.divL({wxn:'wrapx8',req:'Y',L:'Tipo',I:{tag:'select','class':jsF,name:'mType',opts:$V.gfpMovType,noBlank:1}});
			}
			nue.classList.add('walle');
			nue2.classList.add('__mtype');
			if(old){ pare.replaceChild(nue,old); pare.replaceChild(nue2,olty); }
			else{ pare.appendChild(nue); pare.appendChild(nue2); }
		}
	}});
},
editLine:function(L,td){
	var wid='gfpMovLineEdit';
	$1.delet($1.q('#'+wid));
	td.style.position='relative'; var jsF=$Api.JS.cls;
	var wi=$1.t('div',{id:wid,style:'position:absolute; top:90%; left:0; backgroundColor:#FFF; padding:6px; border:1px solid #000;'},td);
	if(L.editar=='bal'){
		var bal=(L.debBal>0)?L.debBal:L.creBal;
		var inp=$1.t('input',{type:'text',numberformat:'mil','class':jsF,name:'bal',min:0,value:bal},wi);
	}
	inp.AJs={mid:L.mid};
		var resp=$1.t('div',0,wi);
		$Api.send({PUT:Api.Gfp.a+'mov/one',textNode:'Modificar',jsBody:wi,loade:resp,func:function(Jr){
			$Api.resp(resp,Jr);
		}},wi);
		$1.T.btnFa({textNode:'Cerrar',fa:'fa_close',func:function(){ $1.delet(wi); }},wi);
}
}

Gfp.Ctc={
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Gfp.a+'wal/bol', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Billetera','Bolsillo','Saldo']);
			var css='backgroundColor:#EEE;';
			var tBody=$1.t('tbody',0,tb); cont.appendChild(tb);
			for(var i in Jr.L){L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.t('td',{textNode:L.wallName},tr);
				$1.t('td',{textNode:L.wbName},tr);
				$1.t('td',{textNode:$Str.money(L.amount)},tr);
			}
		}
	}});
},
}
