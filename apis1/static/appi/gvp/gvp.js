Api.Gvp={b:'/appi/private/gvp/'};

$V.Mdls.gvp={t:'POS',ico:''};
$oB.pus('gvtSin',$Opts,[
{k:'print',ico:'fa fa-ticket',textNode:' Tirilla POS',func:function(T){
	$M.to('gvtSin.view','docEntry:'+T.P.docEntry+',pos:label1',null,Gvt.Sin.view);
}}]);

_Fi['gvp.cr']=function(wrap,x){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:'Código',I:{tag:'input',type:'text','class':jsV,name:'A.proCode'}},wrap);
	$1.T.divL({wxn:'wrapx4', L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'A.proName(E_like3)'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ Gvp.Cr.get(); }},wrap);
};

var Gvp={}; 

Gvp.Cr={
	Lg:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ $M.to('gvp.cr.form','crId:'+T.P.crId); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Gvp.b+'cr',inputs:$1.G.filter(),loade:cont,errWrap:cont,func:function(Jr){
			var tb=$1.T.table(['','Código','Nombre','Activa',''],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				Gvp.Cr.Lg(L,td);
				$1.t('td',{textNode:L.crCode},tr);
				$1.t('td',{textNode:L.crName},tr);
				$1.t('td',{textNode:_g(L.active,$V.YN)},tr);
				$1.t('td',{textNode:L.docMemo},tr);
			}
		}});
	},
	form:function(){
		var Pa=$M.read();
		var api1=Api.Gvp.b+'cr';
		var jsF=$Api.JS.cls;
		var wrap=$M.Ht.cont;
		$Api.get({f:api1+'/form',loadVerif:!Pa.crId,inputs:'crId='+Pa.crId,loade:wrap,func:function(Jr){
			if(Pa.crId){ $Api.JS.addF({name:'crId',value:Pa.crId},wrap); }
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Codigo',req:'Y',I:{tag:'input',type:'text',name:'crCode','class':jsF,value:Jr.crCode}},wrap);
			$1.T.divL({wxn:'wrapx2',L:'Nombre',req:'Y',I:{tag:'input',type:'text',name:'crName','class':jsF,value:Jr.crName}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Activa',req:'Y',I:{tag:'select',name:'active','class':jsF,opts:$V.YN,selected:Jr.active,noBlank:'Y'}},divL);
			$1.T.divLTitle('Númeración',wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Facturación',req:'Y',I:{tag:'select',name:'docSin','class':jsF,opts:$Tb.docSerie['gvtSin'],selected:Jr.docSin}},wrap);
			$1.T.divL({wxn:'wrapx8',L:'Pagos',req:'Y',I:{tag:'select',name:'docRcv','class':jsF,opts:$Tb.docSerie['gvtRcv'],selected:Jr.docRcv}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Almacen',req:'Y',I:{tag:'select',name:'whsId','class':jsF,opts:$Tb.itmOwhs,selected:Jr.whsId}},divL);
			$1.T.divLTitle('Medios de Pago - Cuentas',wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx3',L:'Efectivo',req:'Y',I:{tag:'select',name:'fdpIdEFE','class':jsF,opts:$Tb.gfiOfdp,selected:Jr.fdpIdEFE}},wrap);
			$1.T.divL({wxn:'wrapx3',L:'Transferencia',req:'Y',I:{tag:'select',name:'fdpIdTIB','class':jsF,opts:$Tb.gfiOfdp,selected:Jr.fdpIdTIB}},divL);
			//$1.T.divL({wxn:'wrapx3',L:'Crédito',I:{tag:'select',name:'fdpIdCRE','class':jsF,opts:$Tb.gfiOfdp,selected:Jr.fdpIdCRE}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Tarj. Crédito',I:{tag:'select',name:'fdpIdTCR','class':jsF,opts:$Tb.gfiOfdp,selected:Jr.fdpIdTCR}},wrap);
			$1.T.divL({wxn:'wrapx8',L:'Tarj. Débito',I:{tag:'select',name:'fdpIdTDE','class':jsF,opts:$Tb.gfiOfdp,selected:Jr.fdpIdDE}},divL);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea','class':jsF,name:'docMemo',value:Jr.docMemo}},wrap);
			var resp=$1.t('div',0,wrap);
			var Ps={PUT:api1,jsBody:wrap,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
			}};
			$Api.send(Ps,wrap);
		}});
	}
}

Gvp.Pc={
	Lg:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ Gvp.Pc.form(T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.Gvp.b+'pc',inputs:$1.G.filter(),loade:cont,errWrap:cont,func:function(Jr){
			var tb=$1.T.table(['','Código','Nombre'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				Gvp.Pc.Lg(L,td);
				$1.t('td',{textNode:L.pcCode},tr);
				$1.t('td',{textNode:L.pcName},tr);
			}
		}});
	},
	form:function(Pa){ Pa=(Pa)?Pa:{};
		var api1=Api.Gvp.b+'pc';
		var jsF=$Api.JS.cls;
		var wrap=$1.t('div');
		$Api.get({f:api1+'/form',loadVerif:!Pa.pcId,inputs:'pcId='+Pa.pcId,loade:wrap,func:function(Jr){
			if(Pa.pcId){ $Api.JS.addF({name:'pcId',value:Pa.pcId},wrap); }
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Codigo',req:'Y',I:{tag:'input',type:'text',name:'pcCode','class':jsF,value:Jr.pcCode}},wrap);
			$1.T.divL({wxn:'wrapx2',L:'Nombre',req:'Y',I:{tag:'input',type:'text',name:'pcName','class':jsF,value:Jr.pcName}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Contraseña',req:'Y',I:{tag:'input',type:'password',name:'pcPass','class':jsF,value:Jr.pcPass}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Confirmar',req:'Y',I:{tag:'input',type:'password',name:'pcPass2','class':jsF,value:Jr.pcPass}},divL);
			var resp=$1.t('div',0,wrap);
			var Ps={PUT:api1,jsBody:wrap,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
			}};
			$Api.send(Ps,wrap);
		}});
		$1.Win.open(wrap,{winTitle:'Formulario Cajero',winSize:'medium'});
	},
	login:function(P,pare){
		var api1=Api.Gvp.b+'pc';
		var jsF=$Api.JS.cls;
		var txt='Iniciar...';
		var div=$1.t('div',{},pare);
		var tinp=$Api.JS.addF({name:'crId'},div);
		var R=$1.Sys.storAr(['posPcId','posCrId']);
		var btn=$1.T.btnFa({faBtn:'fa-user',textNode:txt,func:function(){
			var wrap=$1.t('div');
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Codigo',req:'Y',I:{tag:'input',type:'text',name:'pcCode','class':jsF}},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Contraseña',req:'Y',I:{tag:'input',type:'password',name:'pcPass','class':jsF}},divL);
			$1.T.divL({wxn:'wrapx4',L:'Terminal',req:'Y',I:{tag:'select','class':jsF,name:'crId',opts:$Tb.gvpOcre}},divL);
			var resp=$1.t('div',0,wrap);
			$Api.send({POST:api1+'/login',jsBody:wrap,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
				if(o){ $1.Sys.stor({set:'posPcId',v:o.pcId});
					$1.Sys.stor({set:'posCrId',v:o.crId});
					tinp.value=o.crId;
					tinp.AJs={pcId:o.pcId};
					btn.innerText=_g(o.pcId,$Tb.gvpOpca)+' / '+_g(o.crId,$Tb.gvpOcre);
				}
			},textNode:'Iniciar'},wrap);
			$1.Win.open(wrap,{winTitle:'Login de Cajero',winSize:'medium',winId:'gvpPcLogin'});
		}},div);
		if(R.posPcId>0){
			tinp.value=R.posCrId;
			tinp.AJs={pcId:R.posPcId};
			btn.innerText=_g(R.posPcId,$Tb.gvpOpca)+' / '+_g(R.posCrId,$Tb.gvpOcre);
		}
		return div;
	}
}

Gvp.Doc={
	form:function(D){
		D=(D)?D:{};
		var cont=$M.Ht.cont; var jsF=$Api.JS.cls;
		D.func=function(Jr2){
			if(Jr2.docEntry){ $M.to('gvtSin.view','docEntry:'+Jr2.docEntry+',pos:label1'); }
		}
		//var divL=$1.T.divL({divLine:1,wxn:'wrapx2',L:'Cliente',req:'Y',Inode:sea},cont);
		var botHtml=[];
		var term=Gvp.Pc.login({});
		botHtml[0]=Gfi.Fdp.formPos({topPare:cont});
		$Doc.form({tbSerie:'N',cont:cont,
		POST:Api.Gvp.b+'doc',func:D.func,AJs:D.AJs,
		HLs:[
		{divLine:1,L:'Cliente',wxn:'wrapx2',Inode:$crd.Sea.get({cardType:'C'},cont,D)},
		{L:'Terminal',wxn:'wrapx4',Inode:term}
		],
		tbL:{xNum:'Y',xDel:'Y',uniqLine:'Y',docTotal:'Y',L:D.L,itmSea:'sell',bCode:'Y',rteIva:'Y',rteIca:'Y',
		kTb:'gvtItmL',AJs:[{k:'sellFactor',a:'numFactor'}],
		kFie:'itemCode,itemName,price,quantity,udm,vatId,rteId,priceLine,lineText'
		},botHtml:botHtml
		});
	}
}

Gvp.Ticket={
	label1:function(Jr){
		$1.delet($1.q('#posTicket_1'));
		var nca=new Date().getTime();
		var iframe=$1.t('iframe',{id:'posTicket_1',src:'/ext/pos/pos.ticket1.html?time='+nca,style:'width:200px; height:100px; display:none'},document.body);
		iframe.onload=function(){
			Jr.ocardName=$Soc.ocardName;
			Jr.olicTradNum=_g($Soc.licTradType,$V.licTradType)+' '+$Soc.licTradNum;
			Jr.address=$Soc.address;
			Jr.phone=$Soc.pbx;
			Jr.pcName=_g(Jr.pcId,$Tb.gvpOpca);
			Jr.docEntry=$Doc.docNumSerie('gvtSin',Jr);
			Jr.address2=_g($Soc.cityCode,Addr.City)+', '+_g($Soc.countyCode,Addr.County);
			Jrx={ocardName:'ADM Sistems',olicTradNum:'NIT 1125082294',address:'',address2:'',phone:'',
			docEntry:'#',cardName:'',licTradNum:'',pcName:'',dateC:'',
			docTotal:13400,balGet:14000,balRet:600,
			L:[
			{itemName:'Pan Hamburguesa',quantity:2,priceLine:5900},
			{itemName:'Pan Perro',quantity:3,priceLine:7500}
			]
			};
			idom=iframe.contentDocument.body || iframe.contentWindow.document.body;
			for(var k in Jr){
				var id=$1.q('#'+k,idom);
				if(id){ id.innerHTML = Jr[k]; }
			}
			var docTable=$1.q('#docTable',idom);
			var tBody=$1.q('tbody',docTable);
			var totals=$1.q('#docTotals',idom);
			var balGetWrap=$1.q('#balGetWrap',idom);
			tBody.innerHTML=''; totals.innerHTML='';
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{'class':'quantity',textNode:L.quantity*1},tr);
				$1.t('td',{'class':'item',textNode:L.itemName},tr);
				$1.t('td',{'class':'price',textNode:$Str.money(L.priceLine)},tr);
			}
			// <div><b>Subtotal</b>&nbsp;&nbsp;&nbsp;<span id="baseAmnt">$1.157.600</span></div>
			d1=$1.t('div',0,totals); 
			d1.innerHTML +='<b>Subtotal</b>&nbsp;&nbsp;&nbsp;';
			$1.t('span',{textNode:$Str.money(Jr.baseAmnt*1)},d1);
			if(!Jr.Lt.errNo){ for(var i in Jr.Lt){ L=Jr.Lt[i];
				d1=$1.t('div',0,totals);
				var tbt=(L.lineType=='rte')?$Tb.otaxR:$Tb.otaxI;
				d1.innerHTML +='<b>'+_g(L.vatId,tbt)+'</b>&nbsp;&nbsp;&nbsp;';
				$1.t('span',{textNode:$Str.money(L.vatSum*1)},d1);
			}}
			d1=$1.t('div',0,totals);
			d1.innerHTML +='<b>Total</b>&nbsp;&nbsp;&nbsp;';
			$1.t('span',{textNode:$Str.money(Jr.docTotal*1)},d1);
			if(balGetWrap && Jr.balGet*1>0){ balGetWrap.style.display='block';
				balGetWrap.innerHTML='</hr><div><b>Efectivo</b>&nbsp;&nbsp;&nbsp;<span>'+$Str.money(Jr.balGet)+'</span></div>';
				balGetWrap.innerHTML +='<div><b>Cambio</b>&nbsp;&nbsp;&nbsp;<span>'+$Str.money(Jr.balRet)+'</span></div>';
			}
		}
		setTimeout(function(){ $1.delet(iframe); },2000);
	}
}
$M.kauAssg('gvp',[
	{k:'gvp',t:'POS'}
]);

$M.liAdd('gvp',[
	{k:'gvp.form',t:'Terminal POS',kau:'gvp',ini:{g:()=>{ Gvp.Doc.form(); } }},
	{k:'gvp.cr',t:'Terminales',kau:'sysd.suadmin',ini:{f:'gvp.cr', btnGo:'gvp.cr.form',gyp:()=>{ Gvp.Cr.get();} }},
	{k:'gvp.cr.form',t:'Terminal',kau:'sysd.suadmin', ini:{g:()=>{ Gvp.Cr.form();} }},
	{k:'gvp.pc',t:'Personal de Caja',kau:'sysd.suadmin', ini:{fNew:()=>{ Gvp.Pc.form() },gyp:()=>{ Gvp.Pc.get();} }},
	{k:'gvp.pc.form',t:'Personal de Caja',kau:'sysd.suadmin',ini:{g:()=>{  Gvp.Pc.form();} }}
],{prp:{mActive:'gvp',type:'C'}});
