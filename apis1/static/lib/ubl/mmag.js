Adr$={};
Adr$.County=[
{k:'05',v:'ANTIOQUIA'},
{k:'08',v:'ATLANTICO'},
{k:'11',v:'BOGOTA'},
{k:'13',v:'BOLIVAR'},
{k:'15',v:'BOYACA'},
{k:'17',v:'CALDAS'},
{k:'18',v:'CAQUETA'},
{k:'19',v:'CAUCA'},
{k:'20',v:'CESAR'},
{k:'23',v:'CORDOBA'},
{k:'25',v:'CUNDINAMARCA'},
{k:'27',v:'CHOCO'},
{k:'41',v:'HUILA'},
{k:'44',v:'LA GUAJIRA'},
{k:'47',v:'MAGDALENA'},
{k:'50',v:'META'},
{k:'52',v:'NARIÑO'},
{k:'54',v:'N. DE SANTANDER'},
{k:'63',v:'QUINDIO'},
{k:'66',v:'RISARALDA'},
{k:'68',v:'SANTANDER'},
{k:'70',v:'SUCRE'},
{k:'73',v:'TOLIMA'},
{k:'76',v:'VALLE DEL CAUCA'},
{k:'81',v:'ARAUCA'},
{k:'85',v:'CASANARE'},
{k:'86',v:'PUTUMAYO'},
{k:'88',v:'SAN ANDRES'},
{k:'91',v:'AMAZONAS'},
{k:'94',v:'GUAINIA'},
{k:'95',v:'GUAVIARE'},
{k:'97',v:'VAUPES'},
{k:'97',v:'VAUPES'},
{k:'99',v:'VICHADA'}
];
$V_Mmag=[
{'k':'91001', 'v':'LETICIA'},
{'k':'91263', 'v':'EL ENCANTO'},
{'k':'91405', 'v':'LA CHORRERA'},
{'k':'91407', 'v':'LA PEDRERA'},
{'k':'91430', 'v':'LA VICTORIA'},
{'k':'91460', 'v':'MIRITI - PARANÁ'},
{'k':'91530', 'v':'PUERTO ALEGRÍA'},
{'k':'91536', 'v':'PUERTO ARICA'},
{'k':'91540', 'v':'PUERTO NARIÑO'},
{'k':'91669', 'v':'PUERTO SANTANDER'},
{'k':'91798', 'v':'TARAPACÁ'},
{'k':'05001', 'v':'MEDELLÍN'},
{'k':'05002', 'v':'ABEJORRAL'},
{'k':'05004', 'v':'ABRIAQUÍ'},
{'k':'05021', 'v':'ALEJANDRÍA'},
{'k':'05030', 'v':'AMAGÁ'},
{'k':'05031', 'v':'AMALFI'},
{'k':'05034', 'v':'ANDES'},
{'k':'05036', 'v':'ANGELÓPOLIS'},
{'k':'05038', 'v':'ANGOSTURA'},
{'k':'05040', 'v':'ANORÍ'},
{'k':'05042', 'v':'SANTAFÉ DE ANTIOQUIA'},
{'k':'05044', 'v':'ANZA'},
{'k':'05045', 'v':'APARTADÓ'},
{'k':'05051', 'v':'ARBOLETES'},
{'k':'05055', 'v':'ARGELIA'},
{'k':'05059', 'v':'ARMENIA'},
{'k':'05079', 'v':'BARBOSA'},
{'k':'05086', 'v':'BELMIRA'},
{'k':'05088', 'v':'BELLO'},
{'k':'05091', 'v':'BETANIA'},
{'k':'05093', 'v':'BETULIA'},
{'k':'05101', 'v':'CIUDAD BOLÍVAR'},
{'k':'05107', 'v':'BRICEÑO'},
{'k':'05113', 'v':'BURITICÁ'},
{'k':'05120', 'v':'CÁCERES'},
{'k':'05125', 'v':'CAICEDO'},
{'k':'05129', 'v':'CALDAS'},
{'k':'05134', 'v':'CAMPAMENTO'},
{'k':'05138', 'v':'CAÑASGORDAS'},
{'k':'05142', 'v':'CARACOLÍ'},
{'k':'05145', 'v':'CARAMANTA'},
{'k':'05147', 'v':'CAREPA'},
{'k':'05148', 'v':'EL CARMEN DE VIBORAL'},
{'k':'05150', 'v':'CAROLINA'},
{'k':'05154', 'v':'CAUCASIA'},
{'k':'05172', 'v':'CHIGORODÓ'},
{'k':'05190', 'v':'CISNEROS'},
{'k':'05197', 'v':'COCORNÁ'},
{'k':'05206', 'v':'CONCEPCIÓN'},
{'k':'05209', 'v':'CONCORDIA'},
{'k':'05212', 'v':'COPACABANA'},
{'k':'05234', 'v':'DABEIBA'},
{'k':'05237', 'v':'DONMATÍAS'},
{'k':'05240', 'v':'EBÉJICO'},
{'k':'05250', 'v':'EL BAGRE'},
{'k':'05264', 'v':'ENTRERRIOS'},
{'k':'05266', 'v':'ENVIGADO'},
{'k':'05282', 'v':'FREDONIA'},
{'k':'05284', 'v':'FRONTINO'},
{'k':'05306', 'v':'GIRALDO'},
{'k':'05308', 'v':'GIRARDOTA'},
{'k':'05310', 'v':'GÓMEZ PLATA'},
{'k':'05313', 'v':'GRANADA'},
{'k':'05315', 'v':'GUADALUPE'},
{'k':'05318', 'v':'GUARNE'},
{'k':'05321', 'v':'GUATAPE'},
{'k':'05347', 'v':'HELICONIA'},
{'k':'05353', 'v':'HISPANIA'},
{'k':'05360', 'v':'ITAGUI'},
{'k':'05361', 'v':'ITUANGO'},
{'k':'05364', 'v':'JARDÍN'},
{'k':'05368', 'v':'JERICÓ'},
{'k':'05376', 'v':'LA CEJA'},
{'k':'05380', 'v':'LA ESTRELLA'},
{'k':'05390', 'v':'LA PINTADA'},
{'k':'05400', 'v':'LA UNIÓN'},
{'k':'05411', 'v':'LIBORINA'},
{'k':'05425', 'v':'MACEO'},
{'k':'05440', 'v':'MARINILLA'},
{'k':'05467', 'v':'MONTEBELLO'},
{'k':'05475', 'v':'MURINDÓ'},
{'k':'05480', 'v':'MUTATÁ'},
{'k':'05483', 'v':'NARIÑO'},
{'k':'05490', 'v':'NECOCLÍ'},
{'k':'05495', 'v':'NECHÍ'},
{'k':'05501', 'v':'OLAYA'},
{'k':'05541', 'v':'PEÑOL'},
{'k':'05543', 'v':'PEQUE'},
{'k':'05576', 'v':'PUEBLORRICO'},
{'k':'05579', 'v':'PUERTO BERRÍO'},
{'k':'05585', 'v':'PUERTO NARE'},
{'k':'05591', 'v':'PUERTO TRIUNFO'},
{'k':'05604', 'v':'REMEDIOS'},
{'k':'05607', 'v':'RETIRO'},
{'k':'05615', 'v':'RIONEGRO'},
{'k':'05628', 'v':'SABANALARGA'},
{'k':'05631', 'v':'SABANETA'},
{'k':'05642', 'v':'SALGAR'},
{'k':'05647', 'v':'SAN ANDRÉS DE CUERQUÍA'},
{'k':'05649', 'v':'SAN CARLOS'},
{'k':'05652', 'v':'SAN FRANCISCO'},
{'k':'05656', 'v':'SAN JERÓNIMO'},
{'k':'05658', 'v':'SAN JOSÉ DE LA MONTAÑA'},
{'k':'05659', 'v':'SAN JUAN DE URABÁ'},
{'k':'05660', 'v':'SAN LUIS'},
{'k':'05664', 'v':'SAN PEDRO DE LOS MILAGROS'},
{'k':'05665', 'v':'SAN PEDRO DE URABA'},
{'k':'05667', 'v':'SAN RAFAEL'},
{'k':'05670', 'v':'SAN ROQUE'},
{'k':'05674', 'v':'SAN VICENTE'},
{'k':'05679', 'v':'SANTA BÁRBARA'},
{'k':'05686', 'v':'SANTA ROSA DE OSOS'},
{'k':'05690', 'v':'SANTO DOMINGO'},
{'k':'05697', 'v':'EL SANTUARIO'},
{'k':'05736', 'v':'SEGOVIA'},
{'k':'05756', 'v':'SONSON'},
{'k':'05761', 'v':'SOPETRÁN'},
{'k':'05789', 'v':'TÁMESIS'},
{'k':'05790', 'v':'TARAZÁ'},
{'k':'05792', 'v':'TARSO'},
{'k':'05809', 'v':'TITIRIBÍ'},
{'k':'05819', 'v':'TOLEDO'},
{'k':'05837', 'v':'TURBO'},
{'k':'05842', 'v':'URAMITA'},
{'k':'05847', 'v':'URRAO'},
{'k':'05854', 'v':'VALDIVIA'},
{'k':'05856', 'v':'VALPARAÍSO'},
{'k':'05858', 'v':'VEGACHÍ'},
{'k':'05861', 'v':'VENECIA'},
{'k':'05873', 'v':'VIGÍA DEL FUERTE'},
{'k':'05885', 'v':'YALÍ'},
{'k':'05887', 'v':'YARUMAL'},
{'k':'05890', 'v':'YOLOMBÓ'},
{'k':'05893', 'v':'YONDÓ'},
{'k':'05895', 'v':'ZARAGOZA'},
{'k':'81001', 'v':'ARAUCA'},
{'k':'81065', 'v':'ARAUQUITA'},
{'k':'81220', 'v':'CRAVO NORTE'},
{'k':'81300', 'v':'FORTUL'},
{'k':'81591', 'v':'PUERTO RONDÓN'},
{'k':'81736', 'v':'SARAVENA'},
{'k':'81794', 'v':'TAME'},
{'k':'08001', 'v':'BARRANQUILLA'},
{'k':'08078', 'v':'BARANOA'},
{'k':'08137', 'v':'CAMPO DE LA CRUZ'},
{'k':'08141', 'v':'CANDELARIA'},
{'k':'08296', 'v':'GALAPA'},
{'k':'08372', 'v':'JUAN DE ACOSTA'},
{'k':'08421', 'v':'LURUACO'},
{'k':'08433', 'v':'MALAMBO'},
{'k':'08436', 'v':'MANATÍ'},
{'k':'08520', 'v':'PALMAR DE VARELA'},
{'k':'08549', 'v':'PIOJÓ'},
{'k':'08558', 'v':'POLONUEVO'},
{'k':'08560', 'v':'PONEDERA'},
{'k':'08573', 'v':'PUERTO COLOMBIA'},
{'k':'08606', 'v':'REPELÓN'},
{'k':'08634', 'v':'SABANAGRANDE'},
{'k':'08638', 'v':'SABANALARGA'},
{'k':'08675', 'v':'SANTA LUCÍA'},
{'k':'08685', 'v':'SANTO TOMÁS'},
{'k':'08758', 'v':'SOLEDAD'},
{'k':'08770', 'v':'SUAN'},
{'k':'08832', 'v':'TUBARÁ'},
{'k':'08849', 'v':'USIACURÍ'},
{'k':'11001', 'v':'BOGOTÁ, D.C.'},
{'k':'13001', 'v':'CARTAGENA'},
{'k':'13006', 'v':'ACHÍ'},
{'k':'13030', 'v':'ALTOS DEL ROSARIO'},
{'k':'13042', 'v':'ARENAL'},
{'k':'13052', 'v':'ARJONA'},
{'k':'13062', 'v':'ARROYOHONDO'},
{'k':'13074', 'v':'BARRANCO DE LOBA'},
{'k':'13140', 'v':'CALAMAR'},
{'k':'13160', 'v':'CANTAGALLO'},
{'k':'13188', 'v':'CICUCO'},
{'k':'13212', 'v':'CÓRDOBA'},
{'k':'13222', 'v':'CLEMENCIA'},
{'k':'13244', 'v':'EL CARMEN DE BOLÍVAR'},
{'k':'13248', 'v':'EL GUAMO'},
{'k':'13268', 'v':'EL PEÑÓN'},
{'k':'13300', 'v':'HATILLO DE LOBA'},
{'k':'13430', 'v':'MAGANGUÉ'},
{'k':'13433', 'v':'MAHATES'},
{'k':'13440', 'v':'MARGARITA'},
{'k':'13442', 'v':'MARÍA LA BAJA'},
{'k':'13458', 'v':'MONTECRISTO'},
{'k':'13468', 'v':'MOMPÓS'},
{'k':'13473', 'v':'MORALES'},
{'k':'13490', 'v':'NOROSÍ'},
{'k':'13549', 'v':'PINILLOS'},
{'k':'13580', 'v':'REGIDOR'},
{'k':'13600', 'v':'RÍO VIEJO'},
{'k':'13620', 'v':'SAN CRISTÓBAL'},
{'k':'13647', 'v':'SAN ESTANISLAO'},
{'k':'13650', 'v':'SAN FERNANDO'},
{'k':'13654', 'v':'SAN JACINTO'},
{'k':'13655', 'v':'SAN JACINTO DEL CAUCA'},
{'k':'13657', 'v':'SAN JUAN NEPOMUCENO'},
{'k':'13667', 'v':'SAN MARTÍN DE LOBA'},
{'k':'13670', 'v':'SAN PABLO'},
{'k':'13673', 'v':'SANTA CATALINA'},
{'k':'13683', 'v':'SANTA ROSA'},
{'k':'13688', 'v':'SANTA ROSA DEL SUR'},
{'k':'13744', 'v':'SIMITÍ'},
{'k':'13760', 'v':'SOPLAVIENTO'},
{'k':'13780', 'v':'TALAIGUA NUEVO'},
{'k':'13810', 'v':'TIQUISIO'},
{'k':'13836', 'v':'TURBACO'},
{'k':'13838', 'v':'TURBANÁ'},
{'k':'13873', 'v':'VILLANUEVA'},
{'k':'13894', 'v':'ZAMBRANO'},
{'k':'15001', 'v':'TUNJA'},
{'k':'15022', 'v':'ALMEIDA'},
{'k':'15047', 'v':'AQUITANIA'},
{'k':'15051', 'v':'ARCABUCO'},
{'k':'15087', 'v':'BELÉN'},
{'k':'15090', 'v':'BERBEO'},
{'k':'15092', 'v':'BETÉITIVA'},
{'k':'15097', 'v':'BOAVITA'},
{'k':'15104', 'v':'BOYACÁ'},
{'k':'15106', 'v':'BRICEÑO'},
{'k':'15109', 'v':'BUENAVISTA'},
{'k':'15114', 'v':'BUSBANZÁ'},
{'k':'15131', 'v':'CALDAS'},
{'k':'15135', 'v':'CAMPOHERMOSO'},
{'k':'15162', 'v':'CERINZA'},
{'k':'15172', 'v':'CHINAVITA'},
{'k':'15176', 'v':'CHIQUINQUIRÁ'},
{'k':'15180', 'v':'CHISCAS'},
{'k':'15183', 'v':'CHITA'},
{'k':'15185', 'v':'CHITARAQUE'},
{'k':'15187', 'v':'CHIVATÁ'},
{'k':'15189', 'v':'CIÉNEGA'},
{'k':'15204', 'v':'CÓMBITA'},
{'k':'15212', 'v':'COPER'},
{'k':'15215', 'v':'CORRALES'},
{'k':'15218', 'v':'COVARACHÍA'},
{'k':'15223', 'v':'CUBARÁ'},
{'k':'15224', 'v':'CUCAITA'},
{'k':'15226', 'v':'CUÍTIVA'},
{'k':'15232', 'v':'CHÍQUIZA'},
{'k':'15236', 'v':'CHIVOR'},
{'k':'15238', 'v':'DUITAMA'},
{'k':'15244', 'v':'EL COCUY'},
{'k':'15248', 'v':'EL ESPINO'},
{'k':'15272', 'v':'FIRAVITOBA'},
{'k':'15276', 'v':'FLORESTA'},
{'k':'15293', 'v':'GACHANTIVÁ'},
{'k':'15296', 'v':'GAMEZA'},
{'k':'15299', 'v':'GARAGOA'},
{'k':'15317', 'v':'GUACAMAYAS'},
{'k':'15322', 'v':'GUATEQUE'},
{'k':'15325', 'v':'GUAYATÁ'},
{'k':'15332', 'v':'GÜICÁN'},
{'k':'15362', 'v':'IZA'},
{'k':'15367', 'v':'JENESANO'},
{'k':'15368', 'v':'JERICÓ'},
{'k':'15377', 'v':'LABRANZAGRANDE'},
{'k':'15380', 'v':'LA CAPILLA'},
{'k':'15401', 'v':'LA VICTORIA'},
{'k':'15403', 'v':'LA UVITA'},
{'k':'15407', 'v':'VILLA DE LEYVA'},
{'k':'15425', 'v':'MACANAL'},
{'k':'15442', 'v':'MARIPÍ'},
{'k':'15455', 'v':'MIRAFLORES'},
{'k':'15464', 'v':'MONGUA'},
{'k':'15466', 'v':'MONGUÍ'},
{'k':'15469', 'v':'MONIQUIRÁ'},
{'k':'15476', 'v':'MOTAVITA'},
{'k':'15480', 'v':'MUZO'},
{'k':'15491', 'v':'NOBSA'},
{'k':'15494', 'v':'NUEVO COLÓN'},
{'k':'15500', 'v':'OICATÁ'},
{'k':'15507', 'v':'OTANCHE'},
{'k':'15511', 'v':'PACHAVITA'},
{'k':'15514', 'v':'PÁEZ'},
{'k':'15516', 'v':'PAIPA'},
{'k':'15518', 'v':'PAJARITO'},
{'k':'15522', 'v':'PANQUEBA'},
{'k':'15531', 'v':'PAUNA'},
{'k':'15533', 'v':'PAYA'},
{'k':'15537', 'v':'PAZ DE RÍO'},
{'k':'15542', 'v':'PESCA'},
{'k':'15550', 'v':'PISBA'},
{'k':'15572', 'v':'PUERTO BOYACÁ'},
{'k':'15580', 'v':'QUÍPAMA'},
{'k':'15599', 'v':'RAMIRIQUÍ'},
{'k':'15600', 'v':'RÁQUIRA'},
{'k':'15621', 'v':'RONDÓN'},
{'k':'15632', 'v':'SABOYÁ'},
{'k':'15638', 'v':'SÁCHICA'},
{'k':'15646', 'v':'SAMACÁ'},
{'k':'15660', 'v':'SAN EDUARDO'},
{'k':'15664', 'v':'SAN JOSÉ DE PARE'},
{'k':'15667', 'v':'SAN LUIS DE GACENO'},
{'k':'15673', 'v':'SAN MATEO'},
{'k':'15676', 'v':'SAN MIGUEL DE SEMA'},
{'k':'15681', 'v':'SAN PABLO DE BORBUR'},
{'k':'15686', 'v':'SANTANA'},
{'k':'15690', 'v':'SANTA MARÍA'},
{'k':'15693', 'v':'SANTA ROSA DE VITERBO'},
{'k':'15696', 'v':'SANTA SOFÍA'},
{'k':'15720', 'v':'SATIVANORTE'},
{'k':'15723', 'v':'SATIVASUR'},
{'k':'15740', 'v':'SIACHOQUE'},
{'k':'15753', 'v':'SOATÁ'},
{'k':'15755', 'v':'SOCOTÁ'},
{'k':'15757', 'v':'SOCHA'},
{'k':'15759', 'v':'SOGAMOSO'},
{'k':'15761', 'v':'SOMONDOCO'},
{'k':'15762', 'v':'SORA'},
{'k':'15763', 'v':'SOTAQUIRÁ'},
{'k':'15764', 'v':'SORACÁ'},
{'k':'15774', 'v':'SUSACÓN'},
{'k':'15776', 'v':'SUTAMARCHÁN'},
{'k':'15778', 'v':'SUTATENZA'},
{'k':'15790', 'v':'TASCO'},
{'k':'15798', 'v':'TENZA'},
{'k':'15804', 'v':'TIBANÁ'},
{'k':'15806', 'v':'TIBASOSA'},
{'k':'15808', 'v':'TINJACÁ'},
{'k':'15810', 'v':'TIPACOQUE'},
{'k':'15814', 'v':'TOCA'},
{'k':'15816', 'v':'TOGÜÍ'},
{'k':'15820', 'v':'TÓPAGA'},
{'k':'15822', 'v':'TOTA'},
{'k':'15832', 'v':'TUNUNGUÁ'},
{'k':'15835', 'v':'TURMEQUÉ'},
{'k':'15837', 'v':'TUTA'},
{'k':'15839', 'v':'TUTAZÁ'},
{'k':'15842', 'v':'UMBITA'},
{'k':'15861', 'v':'VENTAQUEMADA'},
{'k':'15879', 'v':'VIRACACHÁ'},
{'k':'15897', 'v':'ZETAQUIRA'},
{'k':'17001', 'v':'MANIZALES'},
{'k':'17013', 'v':'AGUADAS'},
{'k':'17042', 'v':'ANSERMA'},
{'k':'17050', 'v':'ARANZAZU'},
{'k':'17088', 'v':'BELALCÁZAR'},
{'k':'17174', 'v':'CHINCHINÁ'},
{'k':'17272', 'v':'FILADELFIA'},
{'k':'17380', 'v':'LA DORADA'},
{'k':'17388', 'v':'LA MERCED'},
{'k':'17433', 'v':'MANZANARES'},
{'k':'17442', 'v':'MARMATO'},
{'k':'17444', 'v':'MARQUETALIA'},
{'k':'17446', 'v':'MARULANDA'},
{'k':'17486', 'v':'NEIRA'},
{'k':'17495', 'v':'NORCASIA'},
{'k':'17513', 'v':'PÁCORA'},
{'k':'17524', 'v':'PALESTINA'},
{'k':'17541', 'v':'PENSILVANIA'},
{'k':'17614', 'v':'RIOSUCIO'},
{'k':'17616', 'v':'RISARALDA'},
{'k':'17653', 'v':'SALAMINA'},
{'k':'17662', 'v':'SAMANÁ'},
{'k':'17665', 'v':'SAN JOSÉ'},
{'k':'17777', 'v':'SUPÍA'},
{'k':'17867', 'v':'VICTORIA'},
{'k':'17873', 'v':'VILLAMARÍA'},
{'k':'17877', 'v':'VITERBO'},
{'k':'18001', 'v':'FLORENCIA'},
{'k':'18029', 'v':'ALBANIA'},
{'k':'18094', 'v':'BELÉN DE LOS ANDAQUÍES'},
{'k':'18150', 'v':'CARTAGENA DEL CHAIRÁ'},
{'k':'18205', 'v':'CURILLO'},
{'k':'18247', 'v':'EL DONCELLO'},
{'k':'18256', 'v':'EL PAUJIL'},
{'k':'18410', 'v':'LA MONTAÑITA'},
{'k':'18460', 'v':'MILÁN'},
{'k':'18479', 'v':'MORELIA'},
{'k':'18592', 'v':'PUERTO RICO'},
{'k':'18610', 'v':'SAN JOSÉ DEL FRAGUA'},
{'k':'18753', 'v':'SAN VICENTE DEL CAGUÁN'},
{'k':'18756', 'v':'SOLANO'},
{'k':'18785', 'v':'SOLITA'},
{'k':'18860', 'v':'VALPARAÍSO'},
{'k':'85001', 'v':'YOPAL'},
{'k':'85010', 'v':'AGUAZUL'},
{'k':'85015', 'v':'CHAMEZA'},
{'k':'85125', 'v':'HATO COROZAL'},
{'k':'85136', 'v':'LA SALINA'},
{'k':'85139', 'v':'MANÍ'},
{'k':'85162', 'v':'MONTERREY'},
{'k':'85225', 'v':'NUNCHÍA'},
{'k':'85230', 'v':'OROCUÉ'},
{'k':'85250', 'v':'PAZ DE ARIPORO'},
{'k':'85263', 'v':'PORE'},
{'k':'85279', 'v':'RECETOR'},
{'k':'85300', 'v':'SABANALARGA'},
{'k':'85315', 'v':'SÁCAMA'},
{'k':'85325', 'v':'SAN LUIS DE PALENQUE'},
{'k':'85400', 'v':'TÁMARA'},
{'k':'85410', 'v':'TAURAMENA'},
{'k':'85430', 'v':'TRINIDAD'},
{'k':'85440', 'v':'VILLANUEVA'},
{'k':'19001', 'v':'POPAYÁN'},
{'k':'19022', 'v':'ALMAGUER'},
{'k':'19050', 'v':'ARGELIA'},
{'k':'19075', 'v':'BALBOA'},
{'k':'19100', 'v':'BOLÍVAR'},
{'k':'19110', 'v':'BUENOS AIRES'},
{'k':'19130', 'v':'CAJIBÍO'},
{'k':'19137', 'v':'CALDONO'},
{'k':'19142', 'v':'CALOTO'},
{'k':'19212', 'v':'CORINTO'},
{'k':'19256', 'v':'EL TAMBO'},
{'k':'19290', 'v':'FLORENCIA'},
{'k':'19300', 'v':'GUACHENÉ'},
{'k':'19318', 'v':'GUAPI'},
{'k':'19355', 'v':'INZÁ'},
{'k':'19364', 'v':'JAMBALÓ'},
{'k':'19392', 'v':'LA SIERRA'},
{'k':'19397', 'v':'LA VEGA'},
{'k':'19418', 'v':'LÓPEZ'},
{'k':'19450', 'v':'MERCADERES'},
{'k':'19455', 'v':'MIRANDA'},
{'k':'19473', 'v':'MORALES'},
{'k':'19513', 'v':'PADILLA'},
{'k':'19517', 'v':'PAEZ'},
{'k':'19532', 'v':'PATÍA'},
{'k':'19533', 'v':'PIAMONTE'},
{'k':'19548', 'v':'PIENDAMÓ'},
{'k':'19573', 'v':'PUERTO TEJADA'},
{'k':'19585', 'v':'PURACÉ'},
{'k':'19622', 'v':'ROSAS'},
{'k':'19693', 'v':'SAN SEBASTIÁN'},
{'k':'19698', 'v':'SANTANDER DE QUILICHAO'},
{'k':'19701', 'v':'SANTA ROSA'},
{'k':'19743', 'v':'SILVIA'},
{'k':'19760', 'v':'SOTARA'},
{'k':'19780', 'v':'SUÁREZ'},
{'k':'19785', 'v':'SUCRE'},
{'k':'19807', 'v':'TIMBÍO'},
{'k':'19809', 'v':'TIMBIQUÍ'},
{'k':'19821', 'v':'TORIBIO'},
{'k':'19824', 'v':'TOTORÓ'},
{'k':'19845', 'v':'VILLA RICA'},
{'k':'20001', 'v':'VALLEDUPAR'},
{'k':'20011', 'v':'AGUACHICA'},
{'k':'20013', 'v':'AGUSTÍN CODAZZI'},
{'k':'20032', 'v':'ASTREA'},
{'k':'20045', 'v':'BECERRIL'},
{'k':'20060', 'v':'BOSCONIA'},
{'k':'20175', 'v':'CHIMICHAGUA'},
{'k':'20178', 'v':'CHIRIGUANÁ'},
{'k':'20228', 'v':'CURUMANÍ'},
{'k':'20238', 'v':'EL COPEY'},
{'k':'20250', 'v':'EL PASO'},
{'k':'20295', 'v':'GAMARRA'},
{'k':'20310', 'v':'GONZÁLEZ'},
{'k':'20383', 'v':'LA GLORIA'},
{'k':'20400', 'v':'LA JAGUA DE IBIRICO'},
{'k':'20443', 'v':'MANAURE'},
{'k':'20517', 'v':'PAILITAS'},
{'k':'20550', 'v':'PELAYA'},
{'k':'20570', 'v':'PUEBLO BELLO'},
{'k':'20614', 'v':'RÍO DE ORO'},
{'k':'20621', 'v':'LA PAZ'},
{'k':'20710', 'v':'SAN ALBERTO'},
{'k':'20750', 'v':'SAN DIEGO'},
{'k':'20770', 'v':'SAN MARTÍN'},
{'k':'20787', 'v':'TAMALAMEQUE'},
{'k':'27001', 'v':'QUIBDÓ'},
{'k':'27006', 'v':'ACANDÍ'},
{'k':'27025', 'v':'ALTO BAUDÓ'},
{'k':'27050', 'v':'ATRATO'},
{'k':'27073', 'v':'BAGADÓ'},
{'k':'27075', 'v':'BAHÍA SOLANO'},
{'k':'27077', 'v':'BAJO BAUDÓ'},
{'k':'27099', 'v':'BOJAYA'},
{'k':'27135', 'v':'EL CANTÓN DEL SAN PABLO'},
{'k':'27150', 'v':'CARMEN DEL DARIÉN'},
{'k':'27160', 'v':'CÉRTEGUI'},
{'k':'27205', 'v':'CONDOTO'},
{'k':'27245', 'v':'EL CARMEN DE ATRATO'},
{'k':'27250', 'v':'EL LITORAL DEL SAN JUAN'},
{'k':'27361', 'v':'ISTMINA'},
{'k':'27372', 'v':'JURADÓ'},
{'k':'27413', 'v':'LLORÓ'},
{'k':'27425', 'v':'MEDIO ATRATO'},
{'k':'27430', 'v':'MEDIO BAUDÓ'},
{'k':'27450', 'v':'MEDIO SAN JUAN'},
{'k':'27491', 'v':'NÓVITA'},
{'k':'27495', 'v':'NUQUÍ'},
{'k':'27580', 'v':'RÍO IRÓ'},
{'k':'27600', 'v':'RÍO QUITO'},
{'k':'27615', 'v':'RIOSUCIO'},
{'k':'27660', 'v':'SAN JOSÉ DEL PALMAR'},
{'k':'27745', 'v':'SIPÍ'},
{'k':'27787', 'v':'TADÓ'},
{'k':'27800', 'v':'UNGUÍA'},
{'k':'27810', 'v':'UNIÓN PANAMERICANA'},
{'k':'23001', 'v':'MONTERÍA'},
{'k':'23068', 'v':'AYAPEL'},
{'k':'23079', 'v':'BUENAVISTA'},
{'k':'23090', 'v':'CANALETE'},
{'k':'23162', 'v':'CERETÉ'},
{'k':'23168', 'v':'CHIMÁ'},
{'k':'23182', 'v':'CHINÚ'},
{'k':'23189', 'v':'CIÉNAGA DE ORO'},
{'k':'23300', 'v':'COTORRA'},
{'k':'23350', 'v':'LA APARTADA'},
{'k':'23417', 'v':'LORICA'},
{'k':'23419', 'v':'LOS CÓRDOBAS'},
{'k':'23464', 'v':'MOMIL'},
{'k':'23466', 'v':'MONTELÍBANO'},
{'k':'23500', 'v':'MOÑITOS'},
{'k':'23555', 'v':'PLANETA RICA'},
{'k':'23570', 'v':'PUEBLO NUEVO'},
{'k':'23574', 'v':'PUERTO ESCONDIDO'},
{'k':'23580', 'v':'PUERTO LIBERTADOR'},
{'k':'23586', 'v':'PURÍSIMA'},
{'k':'23660', 'v':'SAHAGÚN'},
{'k':'23670', 'v':'SAN ANDRÉS SOTAVENTO'},
{'k':'23672', 'v':'SAN ANTERO'},
{'k':'23675', 'v':'SAN BERNARDO DEL VIENTO'},
{'k':'23678', 'v':'SAN CARLOS'},
{'k':'23682', 'v':'SAN JOSÉ DE URÉ'},
{'k':'23686', 'v':'SAN PELAYO'},
{'k':'23807', 'v':'TIERRALTA'},
{'k':'23815', 'v':'TUCHÍN'},
{'k':'23855', 'v':'VALENCIA'},
{'k':'25001', 'v':'AGUA DE DIOS'},
{'k':'25019', 'v':'ALBÁN'},
{'k':'25035', 'v':'ANAPOIMA'},
{'k':'25040', 'v':'ANOLAIMA'},
{'k':'25053', 'v':'ARBELÁEZ'},
{'k':'25086', 'v':'BELTRÁN'},
{'k':'25095', 'v':'BITUIMA'},
{'k':'25099', 'v':'BOJACÁ'},
{'k':'25120', 'v':'CABRERA'},
{'k':'25123', 'v':'CACHIPAY'},
{'k':'25126', 'v':'CAJICÁ'},
{'k':'25148', 'v':'CAPARRAPÍ'},
{'k':'25151', 'v':'CAQUEZA'},
{'k':'25154', 'v':'CARMEN DE CARUPA'},
{'k':'25168', 'v':'CHAGUANÍ'},
{'k':'25175', 'v':'CHÍA'},
{'k':'25178', 'v':'CHIPAQUE'},
{'k':'25181', 'v':'CHOACHÍ'},
{'k':'25183', 'v':'CHOCONTÁ'},
{'k':'25200', 'v':'COGUA'},
{'k':'25214', 'v':'COTA'},
{'k':'25224', 'v':'CUCUNUBÁ'},
{'k':'25245', 'v':'EL COLEGIO'},
{'k':'25258', 'v':'EL PEÑÓN'},
{'k':'25260', 'v':'EL ROSAL'},
{'k':'25269', 'v':'FACATATIVÁ'},
{'k':'25279', 'v':'FOMEQUE'},
{'k':'25281', 'v':'FOSCA'},
{'k':'25286', 'v':'FUNZA'},
{'k':'25288', 'v':'FÚQUENE'},
{'k':'25290', 'v':'FUSAGASUGÁ'},
{'k':'25293', 'v':'GACHALA'},
{'k':'25295', 'v':'GACHANCIPÁ'},
{'k':'25297', 'v':'GACHETÁ'},
{'k':'25299', 'v':'GAMA'},
{'k':'25307', 'v':'GIRARDOT'},
{'k':'25312', 'v':'GRANADA'},
{'k':'25317', 'v':'GUACHETÁ'},
{'k':'25320', 'v':'GUADUAS'},
{'k':'25322', 'v':'GUASCA'},
{'k':'25324', 'v':'GUATAQUÍ'},
{'k':'25326', 'v':'GUATAVITA'},
{'k':'25328', 'v':'GUAYABAL DE SIQUIMA'},
{'k':'25335', 'v':'GUAYABETAL'},
{'k':'25339', 'v':'GUTIÉRREZ'},
{'k':'25368', 'v':'JERUSALÉN'},
{'k':'25372', 'v':'JUNÍN'},
{'k':'25377', 'v':'LA CALERA'},
{'k':'25386', 'v':'LA MESA'},
{'k':'25394', 'v':'LA PALMA'},
{'k':'25398', 'v':'LA PEÑA'},
{'k':'25402', 'v':'LA VEGA'},
{'k':'25407', 'v':'LENGUAZAQUE'},
{'k':'25426', 'v':'MACHETA'},
{'k':'25430', 'v':'MADRID'},
{'k':'25436', 'v':'MANTA'},
{'k':'25438', 'v':'MEDINA'},
{'k':'25473', 'v':'MOSQUERA'},
{'k':'25483', 'v':'NARIÑO'},
{'k':'25486', 'v':'NEMOCÓN'},
{'k':'25488', 'v':'NILO'},
{'k':'25489', 'v':'NIMAIMA'},
{'k':'25491', 'v':'NOCAIMA'},
{'k':'25506', 'v':'VENECIA'},
{'k':'25513', 'v':'PACHO'},
{'k':'25518', 'v':'PAIME'},
{'k':'25524', 'v':'PANDI'},
{'k':'25530', 'v':'PARATEBUENO'},
{'k':'25535', 'v':'PASCA'},
{'k':'25572', 'v':'PUERTO SALGAR'},
{'k':'25580', 'v':'PULÍ'},
{'k':'25592', 'v':'QUEBRADANEGRA'},
{'k':'25594', 'v':'QUETAME'},
{'k':'25596', 'v':'QUIPILE'},
{'k':'25599', 'v':'APULO'},
{'k':'25612', 'v':'RICAURTE'},
{'k':'25645', 'v':'SAN ANTONIO DEL TEQUENDAMA'},
{'k':'25649', 'v':'SAN BERNARDO'},
{'k':'25653', 'v':'SAN CAYETANO'},
{'k':'25658', 'v':'SAN FRANCISCO'},
{'k':'25662', 'v':'SAN JUAN DE RÍO SECO'},
{'k':'25718', 'v':'SASAIMA'},
{'k':'25736', 'v':'SESQUILÉ'},
{'k':'25740', 'v':'SIBATÉ'},
{'k':'25743', 'v':'SILVANIA'},
{'k':'25745', 'v':'SIMIJACA'},
{'k':'25754', 'v':'SOACHA'},
{'k':'25758', 'v':'SOPÓ'},
{'k':'25769', 'v':'SUBACHOQUE'},
{'k':'25772', 'v':'SUESCA'},
{'k':'25777', 'v':'SUPATÁ'},
{'k':'25779', 'v':'SUSA'},
{'k':'25781', 'v':'SUTATAUSA'},
{'k':'25785', 'v':'TABIO'},
{'k':'25793', 'v':'TAUSA'},
{'k':'25797', 'v':'TENA'},
{'k':'25799', 'v':'TENJO'},
{'k':'25805', 'v':'TIBACUY'},
{'k':'25807', 'v':'TIBIRITA'},
{'k':'25815', 'v':'TOCAIMA'},
{'k':'25817', 'v':'TOCANCIPÁ'},
{'k':'25823', 'v':'TOPAIPÍ'},
{'k':'25839', 'v':'UBALÁ'},
{'k':'25841', 'v':'UBAQUE'},
{'k':'25843', 'v':'VILLA DE SAN DIEGO DE UBATE'},
{'k':'25845', 'v':'UNE'},
{'k':'25851', 'v':'ÚTICA'},
{'k':'25862', 'v':'VERGARA'},
{'k':'25867', 'v':'VIANÍ'},
{'k':'25871', 'v':'VILLAGÓMEZ'},
{'k':'25873', 'v':'VILLAPINZÓN'},
{'k':'25875', 'v':'VILLETA'},
{'k':'25878', 'v':'VIOTÁ'},
{'k':'25885', 'v':'YACOPÍ'},
{'k':'25898', 'v':'ZIPACÓN'},
{'k':'25899', 'v':'ZIPAQUIRÁ'},
{'k':'94001', 'v':'INÍRIDA'},
{'k':'94343', 'v':'BARRANCO MINAS'},
{'k':'94663', 'v':'MAPIRIPANA'},
{'k':'94883', 'v':'SAN FELIPE'},
{'k':'94884', 'v':'PUERTO COLOMBIA'},
{'k':'94885', 'v':'LA GUADALUPE'},
{'k':'94886', 'v':'CACAHUAL'},
{'k':'94887', 'v':'PANA PANA'},
{'k':'94888', 'v':'MORICHAL'},
{'k':'95001', 'v':'SAN JOSÉ DEL GUAVIARE'},
{'k':'95015', 'v':'CALAMAR'},
{'k':'95025', 'v':'EL RETORNO'},
{'k':'95200', 'v':'MIRAFLORES'},
{'k':'41001', 'v':'NEIVA'},
{'k':'41006', 'v':'ACEVEDO'},
{'k':'41013', 'v':'AGRADO'},
{'k':'41016', 'v':'AIPE'},
{'k':'41020', 'v':'ALGECIRAS'},
{'k':'41026', 'v':'ALTAMIRA'},
{'k':'41078', 'v':'BARAYA'},
{'k':'41132', 'v':'CAMPOALEGRE'},
{'k':'41206', 'v':'COLOMBIA'},
{'k':'41244', 'v':'ELÍAS'},
{'k':'41298', 'v':'GARZÓN'},
{'k':'41306', 'v':'GIGANTE'},
{'k':'41319', 'v':'GUADALUPE'},
{'k':'41349', 'v':'HOBO'},
{'k':'41357', 'v':'IQUIRA'},
{'k':'41359', 'v':'ISNOS'},
{'k':'41378', 'v':'LA ARGENTINA'},
{'k':'41396', 'v':'LA PLATA'},
{'k':'41483', 'v':'NÁTAGA'},
{'k':'41503', 'v':'OPORAPA'},
{'k':'41518', 'v':'PAICOL'},
{'k':'41524', 'v':'PALERMO'},
{'k':'41530', 'v':'PALESTINA'},
{'k':'41548', 'v':'PITAL'},
{'k':'41551', 'v':'PITALITO'},
{'k':'41615', 'v':'RIVERA'},
{'k':'41660', 'v':'SALADOBLANCO'},
{'k':'41668', 'v':'SAN AGUSTÍN'},
{'k':'41676', 'v':'SANTA MARÍA'},
{'k':'41770', 'v':'SUAZA'},
{'k':'41791', 'v':'TARQUI'},
{'k':'41797', 'v':'TESALIA'},
{'k':'41799', 'v':'TELLO'},
{'k':'41801', 'v':'TERUEL'},
{'k':'41807', 'v':'TIMANÁ'},
{'k':'41872', 'v':'VILLAVIEJA'},
{'k':'41885', 'v':'YAGUARÁ'},
{'k':'44001', 'v':'RIOHACHA'},
{'k':'44035', 'v':'ALBANIA'},
{'k':'44078', 'v':'BARRANCAS'},
{'k':'44090', 'v':'DIBULLA'},
{'k':'44098', 'v':'DISTRACCIÓN'},
{'k':'44110', 'v':'EL MOLINO'},
{'k':'44279', 'v':'FONSECA'},
{'k':'44378', 'v':'HATONUEVO'},
{'k':'44420', 'v':'LA JAGUA DEL PILAR'},
{'k':'44430', 'v':'MAICAO'},
{'k':'44560', 'v':'MANAURE'},
{'k':'44650', 'v':'SAN JUAN DEL CESAR'},
{'k':'44847', 'v':'URIBIA'},
{'k':'44855', 'v':'URUMITA'},
{'k':'44874', 'v':'VILLANUEVA'},
{'k':'47001', 'v':'SANTA MARTA'},
{'k':'47030', 'v':'ALGARROBO'},
{'k':'47053', 'v':'ARACATACA'},
{'k':'47058', 'v':'ARIGUANÍ'},
{'k':'47161', 'v':'CERRO SAN ANTONIO'},
{'k':'47170', 'v':'CHIVOLO'},
{'k':'47189', 'v':'CIÉNAGA'},
{'k':'47205', 'v':'CONCORDIA'},
{'k':'47245', 'v':'EL BANCO'},
{'k':'47258', 'v':'EL PIÑON'},
{'k':'47268', 'v':'EL RETÉN'},
{'k':'47288', 'v':'FUNDACIÓN'},
{'k':'47318', 'v':'GUAMAL'},
{'k':'47460', 'v':'NUEVA GRANADA'},
{'k':'47541', 'v':'PEDRAZA'},
{'k':'47545', 'v':'PIJIÑO DEL CARMEN'},
{'k':'47551', 'v':'PIVIJAY'},
{'k':'47555', 'v':'PLATO'},
{'k':'47570', 'v':'PUEBLOVIEJO'},
{'k':'47605', 'v':'REMOLINO'},
{'k':'47660', 'v':'SABANAS DE SAN ANGEL'},
{'k':'47675', 'v':'SALAMINA'},
{'k':'47692', 'v':'SAN SEBASTIÁN DE BUENAVISTA'},
{'k':'47703', 'v':'SAN ZENÓN'},
{'k':'47707', 'v':'SANTA ANA'},
{'k':'47720', 'v':'SANTA BÁRBARA DE PINTO'},
{'k':'47745', 'v':'SITIONUEVO'},
{'k':'47798', 'v':'TENERIFE'},
{'k':'47960', 'v':'ZAPAYÁN'},
{'k':'47980', 'v':'ZONA BANANERA'},
{'k':'50001', 'v':'VILLAVICENCIO'},
{'k':'50006', 'v':'ACACÍAS'},
{'k':'50110', 'v':'BARRANCA DE UPÍA'},
{'k':'50124', 'v':'CABUYARO'},
{'k':'50150', 'v':'CASTILLA LA NUEVA'},
{'k':'50223', 'v':'CUBARRAL'},
{'k':'50226', 'v':'CUMARAL'},
{'k':'50245', 'v':'EL CALVARIO'},
{'k':'50251', 'v':'EL CASTILLO'},
{'k':'50270', 'v':'EL DORADO'},
{'k':'50287', 'v':'FUENTE DE ORO'},
{'k':'50313', 'v':'GRANADA'},
{'k':'50318', 'v':'GUAMAL'},
{'k':'50325', 'v':'MAPIRIPÁN'},
{'k':'50330', 'v':'MESETAS'},
{'k':'50350', 'v':'LA MACARENA'},
{'k':'50370', 'v':'URIBE'},
{'k':'50400', 'v':'LEJANÍAS'},
{'k':'50450', 'v':'PUERTO CONCORDIA'},
{'k':'50568', 'v':'PUERTO GAITÁN'},
{'k':'50573', 'v':'PUERTO LÓPEZ'},
{'k':'50577', 'v':'PUERTO LLERAS'},
{'k':'50590', 'v':'PUERTO RICO'},
{'k':'50606', 'v':'RESTREPO'},
{'k':'50680', 'v':'SAN CARLOS DE GUAROA'},
{'k':'50683', 'v':'SAN JUAN DE ARAMA'},
{'k':'50686', 'v':'SAN JUANITO'},
{'k':'50689', 'v':'SAN MARTÍN'},
{'k':'50711', 'v':'VISTAHERMOSA'},
{'k':'52001', 'v':'PASTO'},
{'k':'52019', 'v':'ALBÁN'},
{'k':'52022', 'v':'ALDANA'},
{'k':'52036', 'v':'ANCUYÁ'},
{'k':'52051', 'v':'ARBOLEDA'},
{'k':'52079', 'v':'BARBACOAS'},
{'k':'52083', 'v':'BELÉN'},
{'k':'52110', 'v':'BUESACO'},
{'k':'52203', 'v':'COLÓN'},
{'k':'52207', 'v':'CONSACA'},
{'k':'52210', 'v':'CONTADERO'},
{'k':'52215', 'v':'CÓRDOBA'},
{'k':'52224', 'v':'CUASPUD'},
{'k':'52227', 'v':'CUMBAL'},
{'k':'52233', 'v':'CUMBITARA'},
{'k':'52240', 'v':'CHACHAGÜÍ'},
{'k':'52250', 'v':'EL CHARCO'},
{'k':'52254', 'v':'EL PEÑOL'},
{'k':'52256', 'v':'EL ROSARIO'},
{'k':'52258', 'v':'EL TABLÓN DE GÓMEZ'},
{'k':'52260', 'v':'EL TAMBO'},
{'k':'52287', 'v':'FUNES'},
{'k':'52317', 'v':'GUACHUCAL'},
{'k':'52320', 'v':'GUAITARILLA'},
{'k':'52323', 'v':'GUALMATÁN'},
{'k':'52352', 'v':'ILES'},
{'k':'52354', 'v':'IMUÉS'},
{'k':'52356', 'v':'IPIALES'},
{'k':'52378', 'v':'LA CRUZ'},
{'k':'52381', 'v':'LA FLORIDA'},
{'k':'52385', 'v':'LA LLANADA'},
{'k':'52390', 'v':'LA TOLA'},
{'k':'52399', 'v':'LA UNIÓN'},
{'k':'52405', 'v':'LEIVA'},
{'k':'52411', 'v':'LINARES'},
{'k':'52418', 'v':'LOS ANDES'},
{'k':'52427', 'v':'MAGÜI'},
{'k':'52435', 'v':'MALLAMA'},
{'k':'52473', 'v':'MOSQUERA'},
{'k':'52480', 'v':'NARIÑO'},
{'k':'52490', 'v':'OLAYA HERRERA'},
{'k':'52506', 'v':'OSPINA'},
{'k':'52520', 'v':'FRANCISCO PIZARRO'},
{'k':'52540', 'v':'POLICARPA'},
{'k':'52560', 'v':'POTOSÍ'},
{'k':'52565', 'v':'PROVIDENCIA'},
{'k':'52573', 'v':'PUERRES'},
{'k':'52585', 'v':'PUPIALES'},
{'k':'52612', 'v':'RICAURTE'},
{'k':'52621', 'v':'ROBERTO PAYÁN'},
{'k':'52678', 'v':'SAMANIEGO'},
{'k':'52683', 'v':'SANDONÁ'},
{'k':'52685', 'v':'SAN BERNARDO'},
{'k':'52687', 'v':'SAN LORENZO'},
{'k':'52693', 'v':'SAN PABLO'},
{'k':'52694', 'v':'SAN PEDRO DE CARTAGO'},
{'k':'52696', 'v':'SANTA BÁRBARA'},
{'k':'52699', 'v':'SANTACRUZ'},
{'k':'52720', 'v':'SAPUYES'},
{'k':'52786', 'v':'TAMINANGO'},
{'k':'52788', 'v':'TANGUA'},
{'k':'52835', 'v':'SAN ANDRES DE TUMACO'},
{'k':'52838', 'v':'TÚQUERRES'},
{'k':'52885', 'v':'YACUANQUER'},
{'k':'54001', 'v':'CÚCUTA'},
{'k':'54003', 'v':'ABREGO'},
{'k':'54051', 'v':'ARBOLEDAS'},
{'k':'54099', 'v':'BOCHALEMA'},
{'k':'54109', 'v':'BUCARASICA'},
{'k':'54125', 'v':'CÁCOTA'},
{'k':'54128', 'v':'CACHIRÁ'},
{'k':'54172', 'v':'CHINÁCOTA'},
{'k':'54174', 'v':'CHITAGÁ'},
{'k':'54206', 'v':'CONVENCIÓN'},
{'k':'54223', 'v':'CUCUTILLA'},
{'k':'54239', 'v':'DURANIA'},
{'k':'54245', 'v':'EL CARMEN'},
{'k':'54250', 'v':'EL TARRA'},
{'k':'54261', 'v':'EL ZULIA'},
{'k':'54313', 'v':'GRAMALOTE'},
{'k':'54344', 'v':'HACARÍ'},
{'k':'54347', 'v':'HERRÁN'},
{'k':'54377', 'v':'LABATECA'},
{'k':'54385', 'v':'LA ESPERANZA'},
{'k':'54398', 'v':'LA PLAYA'},
{'k':'54405', 'v':'LOS PATIOS'},
{'k':'54418', 'v':'LOURDES'},
{'k':'54480', 'v':'MUTISCUA'},
{'k':'54498', 'v':'OCAÑA'},
{'k':'54518', 'v':'PAMPLONA'},
{'k':'54520', 'v':'PAMPLONITA'},
{'k':'54553', 'v':'PUERTO SANTANDER'},
{'k':'54599', 'v':'RAGONVALIA'},
{'k':'54660', 'v':'SALAZAR'},
{'k':'54670', 'v':'SAN CALIXTO'},
{'k':'54673', 'v':'SAN CAYETANO'},
{'k':'54680', 'v':'SANTIAGO'},
{'k':'54720', 'v':'SARDINATA'},
{'k':'54743', 'v':'SILOS'},
{'k':'54800', 'v':'TEORAMA'},
{'k':'54810', 'v':'TIBÚ'},
{'k':'54820', 'v':'TOLEDO'},
{'k':'54871', 'v':'VILLA CARO'},
{'k':'54874', 'v':'VILLA DEL ROSARIO'},
{'k':'86001', 'v':'MOCOA'},
{'k':'86219', 'v':'COLÓN'},
{'k':'86320', 'v':'ORITO'},
{'k':'86568', 'v':'PUERTO ASÍS'},
{'k':'86569', 'v':'PUERTO CAICEDO'},
{'k':'86571', 'v':'PUERTO GUZMÁN'},
{'k':'86573', 'v':'PUERTO LEGUÍZAMO'},
{'k':'86749', 'v':'SIBUNDOY'},
{'k':'86755', 'v':'SAN FRANCISCO'},
{'k':'86757', 'v':'SAN MIGUEL'},
{'k':'86760', 'v':'SANTIAGO'},
{'k':'86865', 'v':'VALLE DEL GUAMUEZ'},
{'k':'86885', 'v':'VILLAGARZÓN'},
{'k':'63001', 'v':'ARMENIA'},
{'k':'63111', 'v':'BUENAVISTA'},
{'k':'63130', 'v':'CALARCA'},
{'k':'63190', 'v':'CIRCASIA'},
{'k':'63212', 'v':'CÓRDOBA'},
{'k':'63272', 'v':'FILANDIA'},
{'k':'63302', 'v':'GÉNOVA'},
{'k':'63401', 'v':'LA TEBAIDA'},
{'k':'63470', 'v':'MONTENEGRO'},
{'k':'63548', 'v':'PIJAO'},
{'k':'63594', 'v':'QUIMBAYA'},
{'k':'63690', 'v':'SALENTO'},
{'k':'66001', 'v':'PEREIRA'},
{'k':'66045', 'v':'APÍA'},
{'k':'66075', 'v':'BALBOA'},
{'k':'66088', 'v':'BELÉN DE UMBRÍA'},
{'k':'66170', 'v':'DOSQUEBRADAS'},
{'k':'66318', 'v':'GUÁTICA'},
{'k':'66383', 'v':'LA CELIA'},
{'k':'66400', 'v':'LA VIRGINIA'},
{'k':'66440', 'v':'MARSELLA'},
{'k':'66456', 'v':'MISTRATÓ'},
{'k':'66572', 'v':'PUEBLO RICO'},
{'k':'66594', 'v':'QUINCHÍA'},
{'k':'66682', 'v':'SANTA ROSA DE CABAL'},
{'k':'66687', 'v':'SANTUARIO'},
{'k':'88001', 'v':'SAN ANDRÉS'},
{'k':'88564', 'v':'PROVIDENCIA'},
{'k':'68001', 'v':'BUCARAMANGA'},
{'k':'68013', 'v':'AGUADA'},
{'k':'68020', 'v':'ALBANIA'},
{'k':'68051', 'v':'ARATOCA'},
{'k':'68077', 'v':'BARBOSA'},
{'k':'68079', 'v':'BARICHARA'},
{'k':'68081', 'v':'BARRANCABERMEJA'},
{'k':'68092', 'v':'BETULIA'},
{'k':'68101', 'v':'BOLÍVAR'},
{'k':'68121', 'v':'CABRERA'},
{'k':'68132', 'v':'CALIFORNIA'},
{'k':'68147', 'v':'CAPITANEJO'},
{'k':'68152', 'v':'CARCASÍ'},
{'k':'68160', 'v':'CEPITÁ'},
{'k':'68162', 'v':'CERRITO'},
{'k':'68167', 'v':'CHARALÁ'},
{'k':'68169', 'v':'CHARTA'},
{'k':'68176', 'v':'CHIMA'},
{'k':'68179', 'v':'CHIPATÁ'},
{'k':'68190', 'v':'CIMITARRA'},
{'k':'68207', 'v':'CONCEPCIÓN'},
{'k':'68209', 'v':'CONFINES'},
{'k':'68211', 'v':'CONTRATACIÓN'},
{'k':'68217', 'v':'COROMORO'},
{'k':'68229', 'v':'CURITÍ'},
{'k':'68235', 'v':'EL CARMEN DE CHUCURÍ'},
{'k':'68245', 'v':'EL GUACAMAYO'},
{'k':'68250', 'v':'EL PEÑÓN'},
{'k':'68255', 'v':'EL PLAYÓN'},
{'k':'68264', 'v':'ENCINO'},
{'k':'68266', 'v':'ENCISO'},
{'k':'68271', 'v':'FLORIÁN'},
{'k':'68276', 'v':'FLORIDABLANCA'},
{'k':'68296', 'v':'GALÁN'},
{'k':'68298', 'v':'GAMBITA'},
{'k':'68307', 'v':'GIRÓN'},
{'k':'68318', 'v':'GUACA'},
{'k':'68320', 'v':'GUADALUPE'},
{'k':'68322', 'v':'GUAPOTÁ'},
{'k':'68324', 'v':'GUAVATÁ'},
{'k':'68327', 'v':'GÜEPSA'},
{'k':'68344', 'v':'HATO'},
{'k':'68368', 'v':'JESÚS MARÍA'},
{'k':'68370', 'v':'JORDÁN'},
{'k':'68377', 'v':'LA BELLEZA'},
{'k':'68385', 'v':'LANDÁZURI'},
{'k':'68397', 'v':'LA PAZ'},
{'k':'68406', 'v':'LEBRIJA'},
{'k':'68418', 'v':'LOS SANTOS'},
{'k':'68425', 'v':'MACARAVITA'},
{'k':'68432', 'v':'MÁLAGA'},
{'k':'68444', 'v':'MATANZA'},
{'k':'68464', 'v':'MOGOTES'},
{'k':'68468', 'v':'MOLAGAVITA'},
{'k':'68498', 'v':'OCAMONTE'},
{'k':'68500', 'v':'OIBA'},
{'k':'68502', 'v':'ONZAGA'},
{'k':'68522', 'v':'PALMAR'},
{'k':'68524', 'v':'PALMAS DEL SOCORRO'},
{'k':'68533', 'v':'PÁRAMO'},
{'k':'68547', 'v':'PIEDECUESTA'},
{'k':'68549', 'v':'PINCHOTE'},
{'k':'68572', 'v':'PUENTE NACIONAL'},
{'k':'68573', 'v':'PUERTO PARRA'},
{'k':'68575', 'v':'PUERTO WILCHES'},
{'k':'68615', 'v':'RIONEGRO'},
{'k':'68655', 'v':'SABANA DE TORRES'},
{'k':'68669', 'v':'SAN ANDRÉS'},
{'k':'68673', 'v':'SAN BENITO'},
{'k':'68679', 'v':'SAN GIL'},
{'k':'68682', 'v':'SAN JOAQUÍN'},
{'k':'68684', 'v':'SAN JOSÉ DE MIRANDA'},
{'k':'68686', 'v':'SAN MIGUEL'},
{'k':'68689', 'v':'SAN VICENTE DE CHUCURÍ'},
{'k':'68705', 'v':'SANTA BÁRBARA'},
{'k':'68720', 'v':'SANTA HELENA DEL OPÓN'},
{'k':'68745', 'v':'SIMACOTA'},
{'k':'68755', 'v':'SOCORRO'},
{'k':'68770', 'v':'SUAITA'},
{'k':'68773', 'v':'SUCRE'},
{'k':'68780', 'v':'SURATÁ'},
{'k':'68820', 'v':'TONA'},
{'k':'68855', 'v':'VALLE DE SAN JOSÉ'},
{'k':'68861', 'v':'VÉLEZ'},
{'k':'68867', 'v':'VETAS'},
{'k':'68872', 'v':'VILLANUEVA'},
{'k':'68895', 'v':'ZAPATOCA'},
{'k':'70001', 'v':'SINCELEJO'},
{'k':'70110', 'v':'BUENAVISTA'},
{'k':'70124', 'v':'CAIMITO'},
{'k':'70204', 'v':'COLOSO'},
{'k':'70215', 'v':'COROZAL'},
{'k':'70221', 'v':'COVEÑAS'},
{'k':'70230', 'v':'CHALÁN'},
{'k':'70233', 'v':'EL ROBLE'},
{'k':'70235', 'v':'GALERAS'},
{'k':'70265', 'v':'GUARANDA'},
{'k':'70400', 'v':'LA UNIÓN'},
{'k':'70418', 'v':'LOS PALMITOS'},
{'k':'70429', 'v':'MAJAGUAL'},
{'k':'70473', 'v':'MORROA'},
{'k':'70508', 'v':'OVEJAS'},
{'k':'70523', 'v':'PALMITO'},
{'k':'70670', 'v':'SAMPUÉS'},
{'k':'70678', 'v':'SAN BENITO ABAD'},
{'k':'70702', 'v':'SAN JUAN DE BETULIA'},
{'k':'70708', 'v':'SAN MARCOS'},
{'k':'70713', 'v':'SAN ONOFRE'},
{'k':'70717', 'v':'SAN PEDRO'},
{'k':'70742', 'v':'SAN LUIS DE SINCÉ'},
{'k':'70771', 'v':'SUCRE'},
{'k':'70820', 'v':'SANTIAGO DE TOLÚ'},
{'k':'70823', 'v':'TOLÚ VIEJO'},
{'k':'73001', 'v':'IBAGUÉ'},
{'k':'73024', 'v':'ALPUJARRA'},
{'k':'73026', 'v':'ALVARADO'},
{'k':'73030', 'v':'AMBALEMA'},
{'k':'73043', 'v':'ANZOÁTEGUI'},
{'k':'73055', 'v':'ARMERO'},
{'k':'73067', 'v':'ATACO'},
{'k':'73124', 'v':'CAJAMARCA'},
{'k':'73148', 'v':'CARMEN DE APICALÁ'},
{'k':'73152', 'v':'CASABIANCA'},
{'k':'73168', 'v':'CHAPARRAL'},
{'k':'73200', 'v':'COELLO'},
{'k':'73217', 'v':'COYAIMA'},
{'k':'73226', 'v':'CUNDAY'},
{'k':'73236', 'v':'DOLORES'},
{'k':'73268', 'v':'ESPINAL'},
{'k':'73270', 'v':'FALAN'},
{'k':'73275', 'v':'FLANDES'},
{'k':'73283', 'v':'FRESNO'},
{'k':'73319', 'v':'GUAMO'},
{'k':'73347', 'v':'HERVEO'},
{'k':'73349', 'v':'HONDA'},
{'k':'73352', 'v':'ICONONZO'},
{'k':'73408', 'v':'LÉRIDA'},
{'k':'73411', 'v':'LÍBANO'},
{'k':'73443', 'v':'SAN SEBASTIÁN DE MARIQUITA'},
{'k':'73449', 'v':'MELGAR'},
{'k':'73461', 'v':'MURILLO'},
{'k':'73483', 'v':'NATAGAIMA'},
{'k':'73504', 'v':'ORTEGA'},
{'k':'73520', 'v':'PALOCABILDO'},
{'k':'73547', 'v':'PIEDRAS'},
{'k':'73555', 'v':'PLANADAS'},
{'k':'73563', 'v':'PRADO'},
{'k':'73585', 'v':'PURIFICACIÓN'},
{'k':'73616', 'v':'RIOBLANCO'},
{'k':'73622', 'v':'RONCESVALLES'},
{'k':'73624', 'v':'ROVIRA'},
{'k':'73671', 'v':'SALDAÑA'},
{'k':'73675', 'v':'SAN ANTONIO'},
{'k':'73678', 'v':'SAN LUIS'},
{'k':'73686', 'v':'SANTA ISABEL'},
{'k':'73770', 'v':'SUÁREZ'},
{'k':'73854', 'v':'VALLE DE SAN JUAN'},
{'k':'73861', 'v':'VENADILLO'},
{'k':'73870', 'v':'VILLAHERMOSA'},
{'k':'73873', 'v':'VILLARRICA'},
{'k':'76001', 'v':'CALI'},
{'k':'76020', 'v':'ALCALÁ'},
{'k':'76036', 'v':'ANDALUCÍA'},
{'k':'76041', 'v':'ANSERMANUEVO'},
{'k':'76054', 'v':'ARGELIA'},
{'k':'76100', 'v':'BOLÍVAR'},
{'k':'76109', 'v':'BUENAVENTURA'},
{'k':'76111', 'v':'GUADALAJARA DE BUGA'},
{'k':'76113', 'v':'BUGALAGRANDE'},
{'k':'76122', 'v':'CAICEDONIA'},
{'k':'76126', 'v':'CALIMA'},
{'k':'76130', 'v':'CANDELARIA'},
{'k':'76147', 'v':'CARTAGO'},
{'k':'76233', 'v':'DAGUA'},
{'k':'76243', 'v':'EL ÁGUILA'},
{'k':'76246', 'v':'EL CAIRO'},
{'k':'76248', 'v':'EL CERRITO'},
{'k':'76250', 'v':'EL DOVIO'},
{'k':'76275', 'v':'FLORIDA'},
{'k':'76306', 'v':'GINEBRA'},
{'k':'76318', 'v':'GUACARÍ'},
{'k':'76364', 'v':'JAMUNDÍ'},
{'k':'76377', 'v':'LA CUMBRE'},
{'k':'76400', 'v':'LA UNIÓN'},
{'k':'76403', 'v':'LA VICTORIA'},
{'k':'76497', 'v':'OBANDO'},
{'k':'76520', 'v':'PALMIRA'},
{'k':'76563', 'v':'PRADERA'},
{'k':'76606', 'v':'RESTREPO'},
{'k':'76616', 'v':'RIOFRÍO'},
{'k':'76622', 'v':'ROLDANILLO'},
{'k':'76670', 'v':'SAN PEDRO'},
{'k':'76736', 'v':'SEVILLA'},
{'k':'76823', 'v':'TORO'},
{'k':'76828', 'v':'TRUJILLO'},
{'k':'76834', 'v':'TULUÁ'},
{'k':'76845', 'v':'ULLOA'},
{'k':'76863', 'v':'VERSALLES'},
{'k':'76869', 'v':'VIJES'},
{'k':'76890', 'v':'YOTOCO'},
{'k':'76892', 'v':'YUMBO'},
{'k':'76895', 'v':'ZARZAL'},
{'k':'97001', 'v':'MITÚ'},
{'k':'97161', 'v':'CARURU'},
{'k':'97511', 'v':'PACOA'},
{'k':'97666', 'v':'TARAIRA'},
{'k':'97777', 'v':'PAPUNAUA'},
{'k':'97889', 'v':'YAVARATÉ'},
{'k':'99001', 'v':'PUERTO CARREÑO'},
{'k':'99524', 'v':'LA PRIMAVERA'},
{'k':'99624', 'v':'SANTA ROSALÍA'},
{'k':'99773', 'v':'CUMARIBO'}
];
$V.AddrCity=$V_Mmag;
$V.AddrCounty=Adr$.County;