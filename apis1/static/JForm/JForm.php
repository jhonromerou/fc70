<?php
class JForm{
	static $P=array();
	static public function ini($P=[]){
		if(!$P['tbk']){ $P['tbk']='app_ofrm'; }
		if(!$P['tbk1']){ $P['tbk1']='app_frm1'; }
		return $P;
	}
	static public function t_vDoc($D,$canEdit=false){
		if(_js::iseErr($D['fCode'],'Se debe definir un código para el formulario')){}
		else if($js=_js::textMax($D['fCode'],10,'Código')){ _err::err($js); }
		if(_js::iseErr($D['fName'],$ln.'Se debe definir un nombre para el formulario')){}
		else if($js=_js::textMax($D['fName'],50,'Nombre')){ _err::err($js); }
		else{
			$ex=a_sql::fetch('SELECT A.fId,A.fName,A.version,A.canEdit,R.docEntry 
			FROM '.self::$P['tbk'].' A
			LEFT JOIN '.self::$P['tbk'].'r R ON (R.fId=A.fId AND R.canceled=\'N\')
			WHERE fCode=\''.$D['fCode'].'\' LIMIT 1',[1=>'Error verificando código']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else if(a_sql::$errNo==-1){
				if($ex['fId']!=$D['fId']){ _err::err('Ya existe un formulario con ese código: '.$ex['fName'],3); }
				//else if($ex['canEdit']!='Y' && $ex['docEntry']>0){ _err::err('El formulario tiene respuesta registradas y no se permite modificar.',3); }
			}
		}
	}
	static public function t_vLine($D,$nl=1){
		$ln='Linea '.$nl.': ';
		$isTop=($D['lineTop']=='Y');
		if($isTop){
			if(_js::iseErr($D['lineText'],$ln.'Debe digitar algo para la sección')){}
			else if($js=_js::textMax($D['lineText'],50,$ln.'Titulo de Sección')){ _err::err($js); }
		}
		else{
			if(_js::iseErr($D['lineText'],$ln.'El texto de la pregunta se debe definir')){}
			else if($js=_js::textMax($D['lineText'],500,$ln.'Texto de pregunta')){ _err::err($js); }
			else if(_js::iseErr($D['lineType'],$ln.'El tipo de pregunta debe estar definido.','numeric>0')){}
			else if(_js::iseErr($D['lineType'],$ln.'El tipo de pregunta debe estar definido.','numeric>0')){}
			else if($js=_js::textMax($D['descrip'],1000,$ln.'Descripción')){ _err::err($js); }
			if(!_err::$err){
				if($D['lineType']==3 && $D['lOpts']==''){ _err::err($ln.'Se deben definir opciones para el tipo de pregunta.',3); } 
			}
		}
		if(!_err::$err && $D['aid']>0){
			$ex=a_sql::fetch('SELECT B.lineText,B.lineType,B.lOpts,B.descrip
			FROM '.self::$P['tbk1'].' B
			JOIN '.self::$P['tbk1'].'r R ON (R.aid=B.aid)
			WHERE B.aid=\''.$D['aid'].'\' LIMIT 1',[1=>'Error verificando linea']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else if(a_sql::$errNo==-1){
				if($D['delete']=='Y'){
					_err::err('No se puede eliminar porque tiene respuestas registradas: '.$L['lineText'],3);
				}
				else if($D['lineText']!=$ex['lineText'] || $D['lineType']!=$ex['lineType'] || $D['lOpts'] !=$ex['lOpts'] || $D['descrip'] !=$ex['descrip']){
					_err::err('No se puede modificar porque tiene respuestas registradas: '.$D['lineText'],3);
				}
			}
		}
		if(!_err::$err){ $D['lineNum']=$nl; }
		return $D;
	}
	static public function t_get($D,$P=array()){
		$P=self::ini($P);
		$D['from']='A.* FROM '.$P['tbk'].' A';
		return a_sql::rPaging($D);
	}
	static public function t_getSea($D,$P){
		$P=self::ini($P);
		_ADMS::lib('sql/filter');
		$wh=a_sql_filtByT($D);
		$sea=a_sql::toSe(c::$T['textSearch']);
		return a_sql::fetchL('SELECT A.fId,A.fCode,A.fName, CONCAT(A.fCode,\' \',A.fName) lineText FROM '.$P['tbk'].' A
		WHERE (A.fCode '.$sea.' OR A.fName '.$sea.') '.$wh,
		[1=>'Error buscando formularios',2=>'No se encontraron resultados','k'=>'L'],true);
	}
	static public function t_getOne($D,$P,$ar=false){
		$js=false;
		$P=self::ini($P);
		$M=a_sql::fetch('SELECT * FROM '.$P['tbk'].' WHERE fId=\''.$D['fId'].'\' LIMIT 1',[1=>'Error obteniendo plantilla de formulario',2=>'El formulario no existe']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			$M['L']=array();
			$q=a_sql::query('SELECT * FROM '.$P['tbk1'].' WHERE fId=\''.$D['fId'].'\' ',[1=>'Error obteniendo preguntas de formulario']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else if(a_sql::$errNo==-1){
				while($L=$q->fetch_assoc()){ $M['L'][]=$L; }
			}
			$js=($a)? $M : _js::enc2($M);
		}
		if(_err::$err){ return _err::$errText; }
		return $js;
	}
	static public function t_post($D,$P=array()){
		//tbk,tbk1
		$P=self::ini($P);
		$js=false; self::$P=$P; unset($P);
		a_sql::transaction(); $c=false;
		self::t_vDoc($D);
		if(!_err::$err){ $n=1; // revision
			$Lx=$D['L']; unset($D['L']); $qI=[];
			foreach($Lx as $x =>$L){
				$L=self::t_vLine($L,$n); $n++;
				if(_err::$err){ break; }
				else{
					$L[0]='i'; $L[1]=self::$P['tbk1'];
					$L['fId']='x';
					$qI[]=$L;
				}
			}
		}
		if(!_err::$err){// post data
			$fid=a_sql::qInsert($D,['tbk'=>self::$P['tbk']]);
			if(a_sql::$err){ _err::err(a_sql::$errText,3); }
			else{
				$D['fid']=$fid;
				a_sql::multiQuery($qI,null,['fId'=>$fid]);
				if(!_err::$err){ $c=true; 
				$js=(self::$P['ok'])?_js::r(self::$P['ok']):_js::r('Formulario creado correctamente',$D); 
				}
			}
		}
		a_sql::transaction($c);
		if(_err::$err){return _err::$errText; }
		return $js;
	}
	static public function t_put($D,$P=array()){
		$P=self::ini($P);
		$js=false; self::$P=$P; unset($P);
		a_sql::transaction(); $c=false;
		self::t_vDoc($D);
		if(!_err::$err){ $n=1; // revision
			$Lx=$D['L']; unset($D['L']); $qI=[];
			foreach($Lx as $x =>$L){
				$L=self::t_vLine($L,$n); $n++;
				if(_err::$err){ break; }
				else{
					$L[0]='i'; $L[1]=self::$P['tbk1'];
					$L['_unik']='aid';
					$L['fId']=$D['fId'];
					$qI[]=$L;
				}
			}
		}
		if(!_err::$err){// post data
			a_sql::qUpdate($D,['tbk'=>self::$P['tbk'],'wh_change'=>'fId=\''.$D['fId'].'\' LIMIT 1']);
			if(a_sql::$err){ _err::err(a_sql::$errText,3); }
			else{
				a_sql::multiQuery($qI);
				if(!_err::$err){ $c=true; 
				$js=(self::$P['ok'])?_js::r(self::$P['ok']):_js::r('Formulario actualizado correctamente',$D); 
				}
			}
		}
		a_sql::transaction($c);
		if(_err::$err){return _err::$errText; }
		return $js;
	}
	
	/* r-eply  sin transacion*/
	static public function r_docs($D,$P=array()){
		$P=self::ini($P);
		//l 
		$fie=($P['f'])?','.$P['f']:'';
		$D['from']='F.fId,F.fCode,F.fName,A.docDate,A.docEntry,A.docStatus,A.canceled,A.lineMemo,A.userId,A.dateC,A.userUpd,A.dateUpd'.$fie.'
		FROM '.$P['tbk'].'r A 
		'.$P['l'].'';
		return a_sql::rPaging($D);
	}
	static public function r_doc($D,$P=array()){
		$ori=' on[JForm::r_doc()]';
		$P=self::ini($P);
		$fie=($P['f'])?','.$P['f']:'';
		$M=a_sql::fetch('SELECT F.fId,F.fCode,F.fName,F.version,F.descrip,A.docDate,A.docEntry,A.docStatus,A.canceled,A.timeTotal,A.qualNum,A.lineMemo'.$fie.'
		FROM '.$P['tbk'].' F
		LEFT JOIN '.$P['tbk'].'r A ON (A.fId=F.fId AND A.docEntry=\''.$D['docEntry'].'\')
		'.$P['l'].'
		WHERE F.fId=\''.$D['fId'].'\' LIMIT 1',[1=>'Error obtiendo datos del formulario.'.$ori,2=>'El formulario base no existe.'.$ori]);
		if(a_sql::$err){ return a_sql::$errNoText; }
		$M['L']=a_sql::fetchL('SELECT F1.*,B.id,B.aText
		FROM '.$P['tbk1'].' F1 
		LEFT JOIN '.$P['tbk1'].'r B ON (B.aid=F1.aid AND B.docEntry=\''.$M['docEntry'].'\')
		WHERE F1.fId=\''.$M['fId'].'\' ORDER BY F1.lineNum ASC',[1=>'Error obteniendo preguntas del formulario',2=>'El formulario no tiene lineas registradas']);
		return _js::enc2($M);
	}
	static public function r_post($D,$P=array()){
		$P=self::ini($P);
		$js=false;
		$Lx=$D['L']; unset($D['L']);
		$D['dateC']=date('Y-m-d H:i:s');
		$D['docEntry']=a_sql::qInsert($D,['tbk'=>$P['tbk'].'r','qk'=>'ud','qku'=>'ud']);
		if(a_sql::$err){ _err::err(a_sql::$errText,3); }
		else{
			foreach($Lx as $n=>$L){
				$L[0]='i'; $L[1]=$P['tbk1'].'r'; $L['docEntry']=$D['docEntry'];
				$Lx[$n]=$L;
			}
			a_sql::multiQuery($Lx);
			if(!_err::$err){ $c=true;
				$js=$D;
			}
		}
		return $js;
	}
	static public function r_put($D,$P=array()){
		$P=self::ini($P);
		$js=false;
		if(_js::iseErr($D['docEntry'],'Id de documento no definido','numeric>0')){ return $js; }
		/* revisar si se puede editar */
		$Lx=$D['L']; unset($D['L']);
		a_sql::qUpdate($D,['tbk'=>$P['tbk'].'r','wh_change'=>'docEntry=\''.$D['docEntry'].'\' LIMIT 1','qku'=>'ud']);
		if(a_sql::$err){ _err::err(a_sql::$errText,3); }
		else{
			foreach($Lx as $n=>$L){
				$L[0]='i'; $L[1]=$P['tbk1'].'r'; $L['docEntry']=$D['docEntry'];
				$L['_unik']='id';
				$Lx[$n]=$L;
			}
			a_sql::multiQuery($Lx);
			if(!_err::$err){ $c=true;
				$js=$D;
			}
		}
		return $js;
	}
	
	/* q-ualify */
	static public function q_put($D,$P=array()){
		$P=self::ini($P);
		$js=false;
		if(_js::iseErr($D['docEntry'],'Id de documento no definido','numeric>0')){ return $js; }
		/* revisar si se puede editar */
		$Lx=$D['L']; unset($D['L']);
		$D['dateC']=date('Y-m-d H:i:s');
		$D['qualNum']=0;
		foreach($Lx as $n=>$L){
			$L[0]='u'; $L[1]=$P['tbk1'].'r';
			$L['_unik']='aid'; $L['_unikWh'] ='AND docEntry=\''.$D['docEntry'].'\'';
			if($D['aPoints']>0){ $D['qualNum'] += $L['aPoints']; }
			$Lx[$n]=$L;
		}
			a_sql::qUpdate($D,['tbk'=>$P['tbk'].'r','wh_change'=>'docEntry=\''.$D['docEntry'].'\' LIMIT 1']);
		if(a_sql::$err){ _err::err(a_sql::$errText,3); }
		else{
			a_sql::multiQuery($Lx);
		}
		if(!_err::$err){ $c=true;
			$js=$D;
		}
		return $js;
	}
	
	/* p-laning */
	static public function p_get($D,$P=[]){
		$P=self::ini($P);
		$D['from']='P.*,P.pdocEntry,A.fId,A.fCode,A.fName,A.version'.$P['f'].' 
		FROM '.$P['tbk'].'p P
		JOIN '.$P['tbk'].' A ON (A.fId=P.fId) '.$P['l'];
		a_sql::$AordBy='P';
		return a_sql::rPaging($D);
	}
	static public function p_getOne($D,$P,$ar=false){
		$P=self::ini($P);
		$js=false;
		$M=a_sql::fetch('SELECT A.fCode,A.fId,A.fName,P.* FROM '.$P['tbk'].'p P
		JOIN '.$P['tbk'].' A ON (A.fId=P.fId)
		WHERE P.pdocEntry=\''.$D['pdocEntry'].'\' LIMIT 1',[1=>'Error obteniendo documento',2=>'El documento no existe']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			$fie=($P['p1Fie'])?','.$P['p1Fie']:'';
			$M['L']=a_sql::fetchL('SELECT P1.*'.$fie.' 
			FROM '.$P['tbk1'].'p P1 '.$P['p1Join'].'
			WHERE P1.pdocEntry=\''.$D['pdocEntry'].'\' ',[1=>'Error obteniendo participantes',2=>'N']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			$js=_js::enc2($M);
		}
		if(_err::$err){ return _err::$errText; }
		return $js;
	}
	static public function p_post($D,$P=array()){
		$P=self::ini($P);
		$js=false;
		$peoReq=($P['peoReq']!='N'); //requiere personal
		if(_js::iseErr($D['fId'],'Se debe definir el formulario base','numeric>0')){}
		else if(_js::iseErr($D['docDate'],'Se debe definir la fecha')){}
		else if($peoReq && _js::isArray($D['L'],'No se ha relacionado ningun contacto')){}
		else{
			$Lx=$D['L']; unset($D['L']);
			$D['dateC']=date('Y-m-d H:i:s');
			$D['peoQty']=(is_array($Lx))?count($Lx):0;
			$D['pdocEntry']=a_sql::qInsert($D,['tbk'=>$P['tbk'].'p']);
			if(a_sql::$err){ _err::err(a_sql::$errText,3); }
			else{
			if($D['peoQty']>0){ foreach($Lx as $n=>$L){
						$L[0]='i'; $L[1]=$P['tbk1'].'p'; $L['pdocEntry']=$D['pdocEntry'];
						$Lx[$n]=$L;
					}
					a_sql::multiQuery($Lx);
					if(!_err::$err){ $js=_err::$errText; }
				}
				if(!_err::$err){ $c=true; $js=$D; }
			}
		}
		return $js;
	}
	static public function p_put($D,$P=array()){
		$P=self::ini($P);
		$js=false;
		$peoReq=($P['peoReq']!='N'); //requiere personal
		if(_js::iseErr($D['pdocEntry'],'Se debe definir ID de documento','numeric>0')){}
		else if(_js::iseErr($D['fId'],'Se debe definir el formulario base','numeric>0')){}
		else if(_js::iseErr($D['docDate'],'Se debe definir la fecha')){}
		else if($peoReq && _js::isArray($D['L'],'No se ha relacionado ningun contacto')){}
		else{
			$Lx=$D['L']; unset($D['L']);
			$D['peoQty']=(is_array($Lx))?count($Lx):0;
			if($D['peoQty']>0){ foreach($Lx as $n=>$L){
				$L[0]='i'; $L[1]=$P['tbk1'].'p'; $L['pdocEntry']=$D['pdocEntry'];
				$L['_unik']='id';
				if($L['delete']=='Y'){
					if($P['isSigned']!='N' && $L['id']>0){//verificar si ya fue firmado
						$ex=a_sql::fetch('SELECT id,signed FROM '.$P['tbk1'].'p WHERE id=\''.$L['id'].'\' LIMIT 1',[1=>'Error verificando actualización de contacto']);
						if(a_sql::$err){ _err::err(a_sql::$errText,3); break; }
						else if(a_sql::$errNo==-1 && $ex['signed']=='Y'){ _err::err('El registro no se puede eliminar, ya se realizo la firma digital.',3); break; }
					}
				}else{ $D['peoQty']++; }
				$Lx[$n]=$L;
			}}
			if(!_err::$err){
				a_sql::qUpdate($D,['tbk'=>$P['tbk'].'p','wh_change'=>'pdocEntry=\''.$D['pdocEntry'].'\' LIMIT 1']);
				if(a_sql::$err){ _err::err(a_sql::$errText,3); }
			}
			if(!_err::$err && $D['peoQty']>0){ a_sql::multiQuery($Lx); }
			if(!_err::$err){ $js=$D; }
		}
		return $js;
	}
	
	/* sign */
	static public function p_signGet($D,$P,$ar=false){
		$P=self::ini($P);
		$js=false;
		$M=a_sql::fetch('SELECT A.fCode,A.fId,A.fName,P.* FROM '.$P['tbk'].'p P
		JOIN '.$P['tbk'].' A ON (A.fId=P.fId)
		WHERE P.pdocEntry=\''.$D['pdocEntry'].'\' LIMIT 1',[1=>'Error obteniendo documento',2=>'El documento no existe']);
		if(a_sql::$err){ $js=_err::err(a_sql::$errNoText); }
		else{
			$fie=($P['p1Fie'])?','.$P['p1Fie']:'';
			$M['L']=a_sql::fetchL('SELECT P1.*'.$fie.' 
			FROM '.$P['tbk1'].'p P1 '.$P['p1Join'].'
			WHERE P1.pdocEntry=\''.$D['pdocEntry'].'\' ',[1=>'Error obteniendo participantes']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			$js=_js::enc2($M);
		}
		if(_err::$err){ return _err::$errText; }
		return $js;
	}
	static public function p_signPut($D,$P=array()){
		$P=self::ini($P);
		$js=false;
		if(_js::iseErr($D['pdocEntry'],'Se debe definir ID de documento','numeric>0')){}
		else if(_js::iseErr($D['tt'],'Se debe definir tt de firma digital')){}
		else if(_js::iseErr($D['tr'],'Se debe definir tr de firma digital','numeric>0')){}
		else{
			$whUni='pdocEntry=\''.$D['pdocEntry'].'\' AND tt=\''.$D['tt'].'\' AND tr=\''.$D['tr'].'\' LIMIT 1';
			$qE=a_sql::fetch('SELECT signed,signDate FROM '.$P['tbk1'].'p WHERE '.$whUni,[1=>'Error verificando confirmación de asistencia']);
			if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
			if(a_sql::$errNo==2){
				a_sql::query('UPDATE '.$P['tbk'].'p SET peoQty=peoQty+1 WHERE pdocEntry=\''.$D['pdocEntry'].'\' LIMIT 1',[1=>'Error actualizando cantidad de participantes']);
				if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
			}
			else if($qE['signed']=='Y'){
				return _err::err('Ya hay una confirmación de asistencia: '.$qE['signDate'],1);
			}
			$D['signed']='Y';
			$D['signDate']=date('Y-m-d H:i:s');
			a_sql::uniRow($D,['tbk'=>$P['tbk1'].'p','wh_change'=>$whUni]);
			if(a_sql::$err){ _err::err(a_sql::$errText,3); }
			if(!_err::$err){ $js=$D; }
		}
		return $js;
	}
	
}
?>