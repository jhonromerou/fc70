/*
lValidate:{
	minLen:, maxLen, 
}
*/
$V.JFormLType=[{k:1,v:'Respuesta Corta'},{k:2,v:'Número'},{k:3,v:'Lista'},{k:4,v:'Fecha'},{k:5,v:'Parrafo'},
{k:6,v:'Rango'},//1-10, 10-1, 0-10-2 (cada 2)
{k:51,v:'Check'},
{k:50,v:'Si/No'},
{k:20,v:'Archivo',_active:'N'},
{k:7,v:'Variables Sistema'},//$Vs{k:'YN',v:'Si/No',o:'$V.YN'}
{k:91,v:'Tercero',_active:'N'}
];

$M.kauAssg('JForm',[
	{k:'JForm.forms',t:'Definir Plantillas Forms'}
]);

var JForm={ /* Forms Querys, omitir de batso*/
	a:function(g,k,P){
		return '#'+JForm.h(g,k,P,'a');
	},
	h:function(g,k,P,r){
		x='JForm'; var a=''; P=(P)?P:{};
		if(g=='forms'){//Listado form-template
			x +='.forms';
		}
		else if(g=='form'){// Edición de form-template
			x +='.form'; a=(P.fId)?'fId:'+P.fId:'';
		}
		else if(g=='docs'){ //Listado de Respuestas por docEntry
			x +='.docs'; a=(P.fId)?'fId:'+P.fId:'';
		}
		else if(g=='doc'){ //ver docEntry
			x +='.doc'; a=(P.docEntry)?'docEntry:'+P.docEntry:'';
		}
		else if(g=='ans'){ //Form nuevo para respuestas
			x +='.ans'; a=(P.fId)?'fId:'+P.fId:'';
			if(P.docEntry){ a +=(P.docEntry)?',docEntry:'+P.docEntry:''; }
		}
		else if(g=='qua'){ //Calificar Respuestas Manual
			x +='.qua'; a='fId:'+P.fId+',docEntry:'+P.docEntry;
		}
		else if(g=='rep'){// Edición de form-template
			a=(P.fId)?'fId:'+P.fId:'';
			x +='.rep';
		}
		else{// .get Listado de Documentos para responder
			
		}
		x=x+'.'+k; //JForm.form.xGen33rindu
		if(r=='a'){ return x+'!!{'+a+'}'; }
		if(r){ return x; }
		$M.to(x,a)
	},
	namer:function(k,ln){
		var name='';
		var ln=(ln)?ln:'L';
		if(ln=='key'){ return k; }
		else{ return ln+'['+k+']'; } 
	},
	lineTypeD:function(L){
		/*interpretar LineType*/
		var R={};
		if(L.lineType==3 && (typeof(L.lOpts)) == 'string'){
			R.opts=L.lOpts.split(',');
		}
		else if(L.lineType==50 || L.lineType==51){ R.opts=$V.YN; }
		else if(L.lineType==6){//rango de numeros
			var sep=L.lOpts.split('-');
			var n1=sep[0]*1;
			var n2=sep[1]*1;
			var n3=sep[2]*1;//sumar de a 2 o 3
			var nSum=1;
			if(n3>1){ nSum=n3; }
			R.opts=[];
			if(n1>n2){//10a1
				while(n1>=n2){ R.opts.push({k:n1,v:n1}); n1=n1-nSum; }
			}
			else{//1a10
				while(n1<=n2){ R.opts.push({k:n1,v:n1}); n1=n1+nSum; }
			}
		}
		return R;
	},
	repData:function(Jr,P){
		//Devuelve Tbf para TH,  L, y aL
		var R={Tbf:P.Tbf,aL:[],L:[]}
		var xA={};
		for(var i in Jr.aL){
			if(P.Tbf){ R.Tbf.push(Jr.aL[i].lineText); }
			xA[Jr.aL[i].aid]=Jr.aL[i];
			R.aL.push(Jr.aL[i]);
		}
		Rs={}; n=0;
		for(var i in Jr.L){ var L=Jr.L[i];
			ik=L.docEntry+'_'+L.fId;
			if(typeof(Rs[ik])=='undefined'){
				Rs[ik]=n;
				ikn=Rs[ik]; 
				R.L[ikn]=L; R.L[ikn].a={};
			}
			R.L[ikn].a[L.aid]={aText:gVal(L,xA)};
		}
		function gVal(L,xA){
			var tv=xA[L.aid];
			if(tv && tv.lineType==3){
				if(typeof(tv.lOpts) == 'string'){
					opts=tv.lOpts.split(',');
					return opts[L.aText];
				}
			}
			else{ return L.aText; }
		}
		return R;
	},
	
	/* obsoleto */
	reply_put:function(){ 
		var Pa=$M.read(); var cont=$M.Ht.cont;
		$ps_DB.get({f:'GET '+Api.Fqu.a+'batso.fre', inputs:'freId='+Pa.freId, loade:cont, func:function(Jr){
				if(Jr.errNo){ $Api.resp(cont,Jr); }
				else{
					JForm.Tpt.base(Jr,cont);
				}
			}
		});
	},
}

JForm.Tpt=function(P,cont,P2){
	var cont=(cont)?cont:$M.Ht.cont;
	var P2=(P2)?P2:{};
	var JrX=(P.Jr)?P.Jr:{};
	this.send=function(){ var This=this;
		var resp=$1.t('div',0,cont);
		var xD={jsBody:cont,loade:resp,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(P.func){ P.func(Jr2,resp); }
		}};
		if(JrX && JrX.fId){ xD.PUT=P.api; }
		else{ xD.POST=P.api; }
		$Api.send(xD,cont);
	}
	{//create form
		var jsF=$Api.JS.cls;
		cont=$1.t('div',{'class':'JFormTptForm'},cont);
		if(!JrX.L){ JrX.L=[]; }
		if(!JrX.version){ JrX.version=1; }
		if(!JrX.docType){ JrX.docType='F'; }
		if(P.docType){ JrX.docType=P.docType; }
		JrX.L=$js.sortBy('lineNum',JrX.L);
		{
			if(JrX.fId){ $Api.JS.addF({name:'fId',value:JrX.fId},cont); }
			$Api.JS.addF({name:'docType',value:JrX.docType},cont);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Código',req:'Y',I:{lTag:'input',name:'fCode',value:JrX.fCode,'class':jsF}},cont);
			$1.T.divL({wxn:'wrapx4',L:'Nombre',req:'Y',I:{lTag:'input',name:'fName',value:JrX.fName,'class':jsF}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Versión',req:'Y',I:{lTag:'number',inputmode:'numeric',min:1,name:'version',value:JrX.version,'class':jsF}},divL);
			if(P.grIds){
				$1.T.divL({wxn:'wrapx8',L:'Grupo',I:{lTag:'select',name:'grId',value:JrX.grId,'class':jsF,opts:P.grIds}},divL);
			}
			if(P.docClass){
				if(!P.docClass.I){ P.docClass.I={lTag:'select'}; }
				if(!P.docClass.I['class']){ P.docClass.I['class']=jsF; }
				else{ P.docClass.I['class'] +=' '+jsF; }
				P.docClass.I.name='docClass'; P.docClass.I.value=JrX.docClass;
				$1.T.divL(P.docClass,divL);
			}
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',req:'Y',I:{lTag:'textarea',name:'descrip',value:JrX.descrip,'class':jsF}},cont);
			/* Propiedades */
			if(P2.Prp){
				var n=1;
				for(var i in P2.Prp){ var Lp=P2.Prp[i];
					if(n==1){
						var prpL=$1.T.divL({divLine:1,wxn:'wrapx5',L:Lp.t,I:{lTag:'select',name:'prp'+n,value:JrX['prp'+n],'class':jsF,opts:Lp.opts}},cont);
					}
					else{
						$1.T.divL({wxn:'wrapx5',L:Lp.t,I:{lTag:'select',name:'prp'+n,value:JrX['prp'+n],'class':jsF,opts:Lp.opts}},prpL);
					}
					n++;
				}
			}
			if(JrX.docType=='CL'){
				var Tbs=['','Punto a Revisar','Tipo','','Detalles de revisión','Eliminar']; 
			}
			else{ var Tbs=['','Pregunta','Tipo','','Descripción','Eliminar']; }
			var tb=$1.T.table(Tbs,0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in JrX.L){ trA(JrX.L[i],tBody); }
			if(JrX.L.length<=0){ trA(L,tBody); }
			$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Linea',func:function(){ trA({fid:1},tBody); }},cont);
			$1.t('span',{textNode:' | '},cont);
			$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Sección',func:function(){ trASep({fid:1},tBody); }},cont);
		}
	}
	function trASep(L,tBody){
		var jsFL=$Api.JS.clsL;
		var jsFLN=$Api.JS.clsLN;
		var css1='backgroundColor:#b4eaff';
		var tr=$1.t('tr',{'class':jsFL,style:css1},tBody);
		var td=$1.t('td',{style:css1},tr);/* orden */
		$1.Move.btns({},td);
		var td=$1.t('td',{colspan:4,style:css1},tr);
		$1.t('input',{type:'text',name:'lineText',value:L.lineText,'class':jsFLN,AJs:{lineTop:'Y'},style:'width:100%; fontSize:105%; border:none; borderBottom:1px solid #000; font-weigth:bold; background:transparent;'},td);
		var td=$1.t('td',0,tr);
		$1.lineDel(L,{id:'aid'},td);
	}
	
	function trA(L,tBody){
		if(L.lineTop=='Y'){ return trASep(L,tBody); }
		L=(L)?L:{};
		var lTag={tag:'input',style:'width:280px'};
		var jsFL=$Api.JS.clsL;
		var jsFLN=$Api.JS.clsLN;
		lTag.value=L.lineText;
		lTag.name='lineText'; lTag['class']=jsFLN;
		var AJs={};
		var tr=$1.t('tr',{'class':jsFL},tBody);
		var td=$1.t('td',0,tr);/* orden */
		$1.Move.btns({},td);
		var td=$1.t('td',0,tr);
		$1.lTag(lTag,td);
		var td=$1.t('td',0,tr);
		$1.T.sel({name:'lineType',selected:L.lineType,opts:$V.JFormLType,noBlank:'Y','class':'JFormOptsOpts '+jsFLN},td);
		var tsel=$1.q('.JFormOptsOpts',td);
		tsel.P=L;
		tsel.onchange=function(){ trAType(this); }
		var td=$1.t('td',0,tr);
		var span=$1.t('div',{style:'fontSize:13px;','class':'JFormOptsTxt'},td);
		$1.t('div',0,span);
		$1.t('input',{type:'text',name:'lOpts',value:L.lOpts,'class':'_opt1'},span);
		$1.T.sel({name:'lOpts',opts:$VsU,selected:L.lOpts,'class':'_opt2'},span);
		var td=$1.t('td',0,tr);
		$1.t('textarea',{name:'descrip',value:L.descrip,'class':jsFLN},td);
		var td=$1.t('td',0,tr);
		$1.lineDel(L,{id:'aid'},td);
		trAType(tsel);
	}
	function trAType(T){
		var jsFLN=$Api.JS.clsLN;
		var span=$1.q('.JFormOptsTxt',T.parentNode.parentNode);
		var sText=span.firstChild;
		var opt1=$1.q('._opt1',span);
		var opt2=$1.q('._opt2',span);
		opt1.classList.remove(jsFLN); opt1.style.display='none';
		opt2.classList.remove(jsFLN); opt2.style.display='none';
		var val=$1.qOpt(T);
		sText.innerHTML='';
		if(val==3){
			span.style.display='block'; sText.innerHTML='Separar por comas (,)';
			opt1.classList.add(jsFLN);
			opt1.style.display='block';
		}
		else if(val==6){
			span.style.display='block';
			$1.t('span',{_iHelp:'<b>1-10</b>: Entre 1 y 10\n<b>10-1</b>: Entre 10 y 1\n<b>0-12-3</b>:Entre 0 y 12 (0,3,6,9,12)\n<b>8-0-2</b>:Entre 8 y 0 (8,6,4,2,0)'},sText);
			$1.t('span',{textNode:'1-10 (Bajo-Alto)'},sText);
			opt1.classList.add(jsFLN);
			opt1.style.display='block';
		}
		else if(val==7){
			span.style.display='block';
			opt2.classList.add(jsFLN);
			opt2.style.display='block';
		}
		else{ span.style.display='none'; }
	}
}
JForm.Res=function(P,cont){
	this.send=function(){ var This=this;
		var resp=$1.t('div',0,cont);
		var xD={jsBody:cont,loade:resp,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(P.func){ P.func(Jr2,resp); }
		}};
		if(JrX && JrX.docEntry){ xD.PUT=P.api; }
		else{ xD.POST=P.api; }
		$Api.send(xD,cont);
	}
	{//form
		var cont=(cont)?cont:$M.Ht.cont;
		var fpath=(P.relpath)?P.relpath:'f'; // /dyd/f/archivo.pdf
		var JrX=(P.Jr)?P.Jr:{};
		var jsF=$Api.JS.cls;
		$Api.JS.addF({name:'fId',value:JrX.fId},cont); 
		if(JrX.docEntry){ $Api.JS.addF({name:'docEntry',value:JrX.docEntry},cont); }
		if(P.F){
			var pare=cont;
			for(var i in P.F){ var F=P.F[i];
				F['class']=jsF;
				if(F.I){
					if(!F.I['class']){ F.I['class']=jsF; }
					else{ F.I['class']=jsF+' '+F['class']; }
				}
				if(F.divLine){
					pare=$1.T.divL(F,cont);
				}
				else{ $1.T.divL(F,pare); }
			}
		}
		var tBody=$1.t('div',{'class':'JFormResWrap'},cont);
		lineNum=1;
		for(var i in JrX.L){ L=JrX.L[i];
			if(L.lineTop=='Y'){ trASep(L,tBody); }
			else{ trA(L,tBody,lineNum); lineNum++; }
		}
	}
	function trASep(L,tBody){
		var css1='backgroundColor:#b4eaff';
		var tr=$1.t('div',{'class':'JFormRes-row',style:css1},tBody);
		$1.t('h5',{textNode:L.lineText},tr);
	}
	function trA(L,tBody,lineNum){
		L=(L)?L:{};
		var lTag={tag:'input',style:'width:280px'};
		L.lineType *=1;
		switch(L.lineType){
			case 2: lTag.type='number'; lTag.inputmode='numeric'; break;
			case 3:{ lTag.tag='select'; }break;
			case 4: lTag.type='date'; break;
			case 5: lTag.tag='textarea'; break;
			case 6: lTag.tag='select'; break;
			case 7: lTag.tag='select'; //VsU
				lTag.opts=eval(L.lOpts);
			break;
			case 20: lTag.tag='attach'; 
				lTag.tt='fId'; lTag.tr=1; //solo necesito obtener fileId
				lTag.path=fpath; break;
			case 50: lTag.tag='select'; lTag.opts=$V.YN; break;
			case 51: lTag.tag='check';
				lTag.checked=(L.aText=='Y');
				lTag.t='Seleccione para SI';
			break;
			case 91: lTag.tag='apiBox';  break;
			default: lTag.type='text'; break;
		}
		if(L.lineType==3 && (typeof(L.lOpts)) == 'string'){
			lTag.opts=L.lOpts.split(',');
		}
		else if(L.lineType==6){//rango de numeros
			var sep=L.lOpts.split('-');
			var n1=sep[0]*1;
			var n2=sep[1]*1;
			var n3=sep[2]*1;//sumar de a 2 o 3
			var nSum=1;
			if(n3>1){ nSum=n3; }
			lTag.opts=[];
			if(n1>n2){//10a1
				while(n1>=n2){ lTag.opts.push({k:n1,v:n1}); n1=n1-nSum; }
			}
			else{//1a10
				while(n1<=n2){ lTag.opts.push({k:n1,v:n1}); n1=n1+nSum; }
			}
		}
		var jsFL=$Api.JS.clsL; var jsFLN=$Api.JS.clsLN;
		lTag.value=(L.aText)?L.aText:'';
		lTag.name='aText'; lTag['class']=jsFLN;
		lTag.AJs={aid:L.aid};
		if(L.id){ lTag.AJs['id']=L.id; }
		var tr=$1.t('div',{'class':jsFL+' JFormRes-row'},tBody);
		var td=$1.t('div',{'class':'JFormRes-rowText'},tr);/* orden */
		$1.t('b',{textNode:lineNum,'class':'JFormRes-rowNum'},td);
		$1.t('span',{textNode:L.lineText},td);
		if(L.descrip){ $1.t('div',{textNode:L.descrip,'class':'JFormRes-rowDesc pre'},tr); }
		var td=$1.t('div',{'class':'JFormRes-rowAns'},tr);
		var etiq=$1.lTag(lTag,td);
	}
}
JForm.Doc=function(P,cont){
	{//form
		var cont=(cont)?cont:$M.Ht.cont;
		var JrX=(P.Jr)?P.Jr:{};
		var jsF=$Api.JS.cls;
		if(P.F){
			var pare=cont;
			for(var i in P.F){ var F=P.F[i];
				F['class']=jsF;
				if(F.I){
					if(!F.I['class']){ F.I['class']=jsF; }
					else{ F.I['class']=jsF+' '+F['class']; }
				}
				if(F.divLine){ pare=$1.T.divL(F,cont); }
				else{ $1.T.divL(F,pare); }
			}
		}
		var tBody=$1.t('div',{'class':'JFormResWrap'},cont);
		lineNum=1;
		for(var i in JrX.L){ L=JrX.L[i];
			if(L.lineTop=='Y'){ trASep(L,tBody); }
			else{ trA(L,tBody,lineNum); lineNum++; }
		}
	}
	function trASep(L,tBody){
		var css1='backgroundColor:#b4eaff';
		var tr=$1.t('div',{'class':'JFormRes-row',style:css1},tBody);
		$1.t('h5',{textNode:L.lineText},tr);
	}
	function trA(L,tBody,lineNum){
		L=(L)?L:{};
		var lTag={tag:'input',style:'width:280px'};
		L.lineType *=1;
		var R=JForm.lineTypeD(L);
		var tr=$1.t('div',{'class':' JFormRes-row'},tBody);
		var td=$1.t('div',{'class':'JFormRes-rowText'},tr);/* orden */
		$1.t('b',{textNode:lineNum,'class':'JFormRes-rowNum'},td);
		$1.t('span',{textNode:L.lineText},td);
		if(L.descrip){ $1.t('div',{textNode:L.descrip,'class':'JFormRes-rowDesc pre'},tr); }
		var td=$1.t('div',{'class':'JFormRes-rowAns'},tr);
		if(R.opts){
			$1.t('div',{textNode:_g(L.aText,R.opts)},td);
		}
		else if(L.lineType==20){
			if(L.aText){
				$1.t('a',{href:L.aText,textNode:'Ver Archivo','class':'fa fa-paperclip',target:'_BLANK'},td);
			}
			else{
				$1.t('a',{textNode:'No Definido','class':'fa fa-paperclip'},td);
			}
		}
		else{ $1.t('div',{textNode:L.aText},td); }
	}
}

JForm.Qua=function(P,cont){
	this.send=function(){ var This=this;
		var resp=$1.t('div',0,cont);
		var xD={jsBody:cont,loade:resp,func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(P.func){ P.func(Jr2,resp); }
		}};
		if(JrX && JrX.docEntry){ xD.PUT=P.api; }
		else{ xD.POST=P.api; }
		$Api.send(xD,cont);
	}
	{//form
		var cont=(cont)?cont:$M.Ht.cont;
		var JrX=(P.Jr)?P.Jr:{};
		var jsF=$Api.JS.cls;
		$Api.JS.addF({name:'fId',value:JrX.fId},cont); 
		$Api.JS.addF({name:'docEntry',value:JrX.docEntry},cont);
		if(P.F){
			var pare=cont;
			for(var i in P.F){ var F=P.F[i];
				F['class']=jsF;
				if(F.I){
					if(!F.I['class']){ F.I['class']=jsF; }
					else{ F.I['class']=jsF+' '+F['class']; }
				}
				if(F.divLine){
					pare=$1.T.divL(F,cont);
				}
				else{ $1.T.divL(F,pare); }
			}
		}
		var tBody=$1.t('div',{'class':'JFormResWrap'},cont);
		for(var i in JrX.L){ trA(JrX.L[i],tBody); }
	}
	function trA(L,tBody){
		L=(L)?L:{};
		var val=L.aText;
		var R=JForm.lineTypeD(L);
		if(R.opts){ val=_g(L.aText,R.opts); }
		var jsFL=$Api.JS.clsL; var jsFLN=$Api.JS.clsLN;
		var tr=$1.t('div',{'class':jsFL+' JFormRes-row'},tBody);
		rQua=$1.t('div',{'class':'JFormRes-rowQua'},tr);
		var punt=$1.lTag({tag:'number','class':jsFLN,name:'aPoints',value:L.aPoints,AJs:{aid:L.aid}},rQua);
		rQua2=$1.t('div',{'class':'JFormRes-rowQua2'},tr);
		var td=$1.t('div',{'class':'JFormRes-rowText'},rQua2);/* orden */
		$1.t('b',{textNode:L.lineNum,'class':'JFormRes-rowNum'},td);
		$1.t('span',{textNode:L.lineText},td);
		if(L.descrip){ $1.t('div',{textNode:L.descrip,'class':'JFormRes-rowDesc pre'},rQua2); }
		var td=$1.t('div',{'class':'JFormRes-rowAns'},rQua2);
		$1.t('div',{textNode:val},td);
	}
}
JForm.Sign={
	win:function(P){
		var cont=$1.t('div');
		var vPost='pdocEntry='+P.pdocEntry;
		$Api.get({f:P.api,inputs:vPost,loade:cont,func:function(Jr){
			$1.T.barCode({jsF:$Api.JS.cls,func:function(val){
				$Api.put({f:P.api,inputs:'bCode='+val+'&'+vPost,func:P.func});
			}},cont);
			if(!P.func){
				P.func=function(Jr2){ trAdds(Jr2.L); }
			}
			var tb=$1.T.table(['','Participante','Fecha'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			trAdds(Jr.L);
			function trAdds(Lx){
				if(Lx){ for(var i in Lx){ L=Lx[i];
					var tr=$1.t('tr',0,tBody);
					$1.t('td',0,tr);
					$1.t('td',{textNode:L.lineText},tr);
					$1.t('td',{textNode:L.signDate},tr);
				}}
			}
		}});
		$1.Win.open(cont,{winTitle:'Asistencia',winSize:'medium'});
	}
}

JForm.Sea={
	one:function(D1,wrap,P2){
		var jsF=$Api.JS.cls;
		var P2=(P2)?P2:{};
		return $1.lTag({tag:'apiSeaBox',value:D1.fName,api:P2.api,fieDefAt:wrap,'class':jsF,D:D1,AJsPut:['fId'],func:P2.func});
	}
}