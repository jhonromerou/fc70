/* xGe */
$V.nomCfrLeaFid=3; //fId form retiro
$oB.push($V.JDriveTtypes,[
	{k:'nomAil',v:'Accidentes'},
	{k:'xGenCdp',v:'Capacitación'},
	{k:'xGenCkPop',v:'Preoperacionales'}
])
$js.active($V.JFormLType,'20,');
$oB.push($V.docTT,[
{k:'xGenCyp',v:'Constancias/Permisos'}
]);

Api.xGe={b:'/appi/private/xGe/'};
_Fi['xGen31.cae']=function(wrap,x){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx4', L:'Empleado',I:{tag:'input',type:'text','class':jsV,name:'C.cardName(E_like3)'}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha',I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Fin',I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Estado',I:{tag:'select','class':jsV,name:'A.docStatus',opts:$V.docStatusOCN}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ xGen.B1.Cae.get(); }},wrap);
};
_Fi['JForm.xGen33rindu']=function(wrap,x){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8', L:'Código',I:{tag:'input',type:'text','class':jsV,name:'A.fCode(E_like)'}},wrap);
	$1.T.divL({wxn:'wrapx4',L:'Nombre',I:{tag:'input',type:'text','class':jsV,name:'A.fName(E_like)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Versión',I:{tag:'input',type:'number','class':jsV,name:'A.version'}},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ xGen.B1.Rindu.get(); }},wrap);
};
_Fi['JForm.p.xGen33Cdp']=function(wrap,x){
	$Doc.filter({func:function(){ xGen.B1.Cdp.pGet(); } },
	[{k:'d1',f:'P.docDate',value:$2d.today},{k:'d2',f:'P.docDate'},
	{wxn:'wrapx8',L:'Sede',I:{tag:'select',name:'P.prp1',opts:$Tb.owsu}},
	{wxn:'wrapx8',L:'Publico',I:{tag:'select',name:'P.prp2',opts:$V.xGeB1CdpPo}},
	{k:'ordBy'},
	{wxn:'wrapx8', L:'Participantes =>',I:{tag:'input',type:'number',name:'P.peoQty(E_mayIgual)'}},
	{wxn:'wrapx8', L:'Código Formato',I:{tag:'input',type:'text',name:'A.fCode(E_like3)'}},
	{wxn:'wrapx4',L:'Nombre Formato',I:{tag:'input',type:'text',name:'A.fName(E_like3)'}},
	],wrap);
};
_Fi['xGenCyp']=function(wrap){
	var Fid={func:xGen.Cyp.get,rows:'N',tbSerie:'xGenCyp',
	adds:[{wxn:'wrapx8',L:'Clase',I:{lTag:'select',name:'A.docClass',opts:$JsV.xGeCypCls}}]
	};
	$Doc.filtForm(Fid,wrap);
};

$M.liAdd('xGen',[
{_lineText:'Pilar Gente'},
{k:'xGenEva',t:'Evaluación DPO', kau:'xGenEva', func:function(){
	$M.Ht.ini({btnGo:'xGenEva.form',gyp:function(){ xGen.Eva.get() }});
}},
{k:'xGenEva.form',t:'Evaluación DPO (form)', kau:'xGenEva', func:function(){
	$M.Ht.ini({g:function(){ xGen.Eva.form(); }});
}},
{k:'xGenEva.acc',t:'Acciones de Mejora', kau:'xGenEva', func:function(){
	$M.Ht.ini({g:function(){ xGen.Eva.accGet(); }});
}},
{k:'xGenEva.des',t:'Hallazgos', kau:'xGenEva', func:function(){
	$M.Ht.ini({g:function(){ xGen.Eva.desGet(); }});
}},
{k:'xGen11.presu',t:'Presupuesto', kau:'xGen11.presu', func:function(){
	$M.Ht.ini({btnGo:'xGen11.presuForm',g:function(){ xGen.B1.Presu.get(); }});
}},
{k:'xGen11.presuForm',t:'Presupuesto (Form)', kau:'xGen11.presu', func:function(){
	$M.Ht.ini({g:function(){ xGen.B1.Presu.form(); }});
}},
{k:'xGen31.cae',t:'Entrenamiento', kau:'xGen31.cae', func:function(){
	bn=$1.T.btnFa({faBtn:'fa-file',textNode:'Nuevo Registro',func:function(){ xGen.B1.Cae.form(); }});
	$M.Ht.ini({btnNew:bn,f:'xGen31.cae',gyp:function(){ xGen.B1.Cae.get(); }});
}},
{k:'xGen31.cae.rep1',t:'Reporte Hrs Entranamiento', kau:'xGen31.cae', func:function(){
	bn=$1.T.btnFa({faBtn:'fa-file',textNode:'Nuevo Registro',func:function(){ xGen.B1.Cae.form(); }});
	$M.Ht.ini({btnNew:bn,f:'xGen31.cae',gyp:function(){ xGen.B1.Cae.get(); }});
}},
//form Induccion
{k:'JForm.xGen33rindu',t:'Registrar Inducción', kau:'xGen33.rindu', func:function(){
	$M.Ht.ini({f:'JForm.xGen33rindu',gyp:function(){ xGen.B1.Rindu.get() }});
}},
{k:'JForm.docs.xGen33rindu',t:'Respuestas de Inducción', kau:'xGen33.rindu', func:function(){
	$M.Ht.ini({g:function(){ xGen.B1.Rindu.docs() }});
}},
{k:'JForm.rep.xGen33rindu',t:'Reporte de Inducción', kau:'xGen33.rindu', func:function(){
	$M.Ht.ini({g:function(){ xGen.B1.Rindu.rep() }});
}},
{k:'JForm.ans.xGen33rindu',t:'Registro Inducción', kau:'xGen33.rindu', func:function(){
	$M.Ht.ini({g:function(){ xGen.B1.Rindu.ans() }});
}},
// form Capacitación
{k:'JForm.p.xGen33Cdp',t:'Capacitaciones',d:'Planificación de capacitaciones del personal', kau:'xGen33.cdp', func:function(){
	bn=$1.T.btnFa({faBtn:'fa-file',textNode:'Nueva',func:function(){ xGen.B1.Cdp.pForm(); }});
	$M.Ht.ini({btnNew:bn,f:'JForm.p.xGen33Cdp',gyp:function(){ xGen.B1.Cdp.pGet() }});
}},
{k:'JForm.docs.xGen33Cdp',t:'Respuestas de Capacitación', kau:'xGen33.cdp', func:function(){
	$M.Ht.ini({g:function(){ xGen.B1.Cdp.docs() }});
}},
{k:'JForm.rep.xGen33Cdp',t:'Evaluaciones de la Capacitación', kau:'xGen33.cdp', func:function(){
	$M.Ht.ini({g:function(){ xGen.B1.Cdp.rep() }});
}},
{k:'JForm.ans.xGen33Cdp',t:'Evaluación de Capacitación (form)', kau:'xGen33.cdp', func:function(){
	$M.Ht.ini({g:function(){ xGen.B1.Cdp.ans() }});
}},

{k:'JForm.qua.xGen33Cdp',t:'Calificar Capacitación', kau:'xGen33.cdp', func:function(){
	$M.Ht.ini({g:function(){ xGen.B1.Cdp.qua() }});
}},
{k:'JForm.forms.xGen33Cdp',t:'Plantilla Capacitación', kau:'JForm.forms', func:function(){
	$M.Ht.ini({btnGo:JForm.h('form','xGen33Cdp',0,1),gyp:function(){ xGen.B1.Cdp.forms() }});
}},
{k:'JForm.form.xGen33Cdp',t:'Plantilla Capacitación (Form)', kau:'JForm.forms', func:function(){
	$M.Ht.ini({g:function(){ xGen.B1.Cdp.form() }});
}},

{k:'xGenTor.gente',t:'TOR Gente', kau:'xGenTor.gente', func:function(){
	$M.Ht.ini({btnGo:'xGenTor.gente.form',gyp:function(){ xGen.Tor.Gente.get(); }});
}},
{k:'xGenTor.gente.view',noTitle:'Y', kau:'xGenTor.gente', func:function(){
	$M.Ht.ini({g:function(){ xGen.Tor.Gente.view(); }});
}},
{k:'xGenTor.gente.form',t:'TOR Gente (Form)', kau:'xGenTor.gente', func:function(){
	$M.Ht.ini({g:function(){ xGen.Tor.Gente.form(); }});
}},

{k:'xGenTor.seg',t:'TOR Seguridad', kau:'xGenTor.seg', func:function(){
	$M.Ht.ini({btnGo:'xGenTor.seg.form',gyp:function(){ xGen.Tor.Seg.get(); }});
}},
{k:'xGenTor.seg.view',noTitle:'Y', kau:'xGenTor.seg', func:function(){
	$M.Ht.ini({g:function(){ xGen.Tor.Seg.view(); }});
}},
{k:'xGenTor.seg.form',t:'TOR Seguridad (Form)', kau:'xGenTor.seg', func:function(){
	$M.Ht.ini({g:function(){ xGen.Tor.Seg.form(); }});
}},

{k:'xGenCyp',t:'Constancia/Permisos', kau:'xGenCyp', func:function(){
	$M.Ht.ini({btnGo:'xGenCyp.form',f:'xGenCyp',g:function(){ xGen.Cyp.get() }});
}},
{k:'xGenCyp.form',t:'Constancia/Permisos (form)', kau:'xGenCyp', func:function(){
	$M.Ht.ini({g:function(){ xGen.Cyp.form() }});
}},
{k:'xGenCyp.view',noTitle:'Y', kau:'xGenCyp', func:function(){
	$M.Ht.ini({g:function(){ xGen.Cyp.view() }});
}},

]);

$M.kauAssg('xGen',[
	{t:'Evaluación Gente', k:'xGenEva'},
	{t:'Presupuesto', k:'xGen11.presu'},
	{k:'xGen31.cae',t:'Entrenamiento'},
	{k:'xGen33.rindu',t:'Registrar Inducción'},
	{k:'xGen33.cdp',t:'Registrar Capacitación'},
	{k:'xGenTor.gente',t:'TOR Gente'},
	{k:'xGenTor.seg',t:'TOR Seguridad'},
	{k:'xGenCyp',t:'Constancia/Permisos'},
	{k:'xGenCkPop',t:'Registrar Preoperacionales'},
]);

var xGen={};
$V.xGeB1CdpPo=[{k:1,v:'Todo'},{k:2,v:'Operativo'},{k:3,v:'Administrativo'},{k:4,v:'Externo'},{k:5,v:'Otros'}];
$V.xGeEvaAcc=[{k:'P',v:'Pendiente'},{k:'R',v:'En Revisión'},{k:'C',v:'Cerrada'}];
$V.xGeDPOB=[
	{k:'1.0',v:'Diseño organizacional, roles y responsabilidades'},
	{k:'2.0',v:'Recompensas'},
	{k:'3.0',v:'Reclutamiento y Selección'},
	{k:'4.0',v:'Labor Legislation'},
	{k:'5.0',v:'Ambiente laboral y compromiso'},
	{k:'6.0',v:'Aprendizaje y desarrollo'},
	{k:'7.0',v:'Gestión de rotación'},
	{k:'8.0',v:'Gestión de talento y carrera'}
];
$V.xGeDPOP=[
	{k:'1.1',v:'Organización en el sitio y plan de la zona',B:'1.0'},
	{k:'1.2',v:'Revisión del presupuesto del paquete de gente',B:'1.0'},
	{k:'1.3',v:'Roles y responsabilidades',B:'1.0'},
	{k:'2.1',v:'Conocimiento del paquete de compensaciones',B:'2.0'},
	{k:'2.2',v:'Conocimiento del paquete de beneficios',B:'2.0'},
	{k:'2.3',v:'Gestión de compensación',B:'2.0'},
	{k:'2.4',v:'Compensación variable ',B:'2.0'},
	{k:'3.1',v:'Cumplimiento de vacantes',B:'3.0'},
	{k:'3.2',v:'Base de datos de recursos temporales',B:'3.0'},
	{k:'3.3',v:'Prácticas de employ justas',B:'3.0'},
	{k:'3.4',v:'Inducción e integración',B:'3.0'},
	{k:'3.5',v:'Inducción funcional',B:'3.0'},
	{k:'4.1',v:'Control de horas laborales ',B:'4.0'},
	{k:'4.2',v:'Negociación sindical',B:'4.0'},
	{k:'4.3',v:'Responsabilidades laborales',B:'4.0'},
	{k:'4.4',v:'Programa de vacaciones ',B:'4.0'},
	{k:'5.1',v:'Ambiente laboral',B:'5.0'},
	{k:'5.2',v:'Proceso de encuesta de clima y compromiso',B:'5.0'},
	{k:'5.3',v:'Plan de comunicación',B:'5.0'},
	{k:'5.4',v:'Programas de reconocimiento',B:'5.0'},
	{k:'5.5',v:'Conocimiento DPO',B:'5.0'},
	{k:'5.6',v:'Comité de gente',B:'5.0'},
	{k:'5.7',v:'Ausentismo',B:'5.0'},
	{k:'6.1',v:'SKAP',B:'6.0'},
	{k:'6.2',v:'Pilar funcional',B:'6.0'},
	{k:'6.3',v:'DPO para operadores',B:'6.0'},
	{k:'6.4',v:'Programa White Belt',B:'6.0'},
	{k:'6.5',v:'Pilar liderazgo',B:'6.0'},
	{k:'6.6',v:'DPO lideres',B:'6.0'},
	{k:'7.1',v:'Proceso de retención ',B:'7.0'},
	{k:'7.2',v:'Rotación de nuevos empleados',B:'7.0'},
	{k:'7.3',v:'Entrevista de salida',B:'7.0'},
	{k:'8.1',v:'Evaluación de competencias',B:'8.0'},
	{k:'8.2',v:'Competencias de liderazgo',B:'8.0'},
	{k:'8.3',v:'Adherencia OPR',B:'8.0'},
	{k:'8.4',v:'Pipeline Health',B:'8.0'}
];
$V.xGeDPOR=[
	{k:'1.1.1',v:'Cada zona/pais debe pre-definir la estructura organizacional...',P:'1.1'},
];
xGen.Eva={
	Lg:function(L,men){
		var Li=[];
		Li.push({k:'desvia',ico:'fa fa-thumbs-o-up',textNode:'Acciones Mejora',P:L,func:function(T){ $M.to('xGenEva.acc','docEntry:'+T.P.docEntry) }});
		Li.push({k:'desvia',ico:'fa fa-warning',textNode:'Hallazgos',P:L,func:function(T){ $M.to('xGenEva.des','docEntry:'+T.P.docEntry) }});
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ $M.to('xGenEva.form','docEntry:'+T.P.docEntry); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'eva',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['','Fecha','Sede','Detalles'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				xGen.Eva.Lg(L,td);
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:_g(L.workSede,$JsV.nomWorkSede)},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
			}
		}});
	},
	form:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'eva/form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,errWrap:cont,func:function(Jr){
			var jsF=$Api.JS.cls; var jsFN=$Api.JS.clsLN;
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Fecha',I:{tag:'input',type:'date',name:'docDate','class':jsF,value:Jr.docDate}},cont);
			$1.T.divL({wxn:'wrapx8',L:'Sede',I:{tag:'select',name:'workSede','class':jsF,opts:$JsV.nomWorkSede,selected:Jr.workSede}},divL);
			var divL=$1.T.divL({divLine:1,wxn:'wrap1',L:'Observación General',I:{tag:'textarea',name:'lineMemo','class':jsF,textNode:Jr.lineMemo}},cont);
			var tP=[{k:0,v:0},{k:1,v:1},{k:3,v:3}];
			var rL={};
			var tbWrap=$1.T.fieset({},cont);
			if(!Pa.docEntry){
				$Api.resp(tbWrap,{text:'Inicie el formulario para ver la información completa'});
			}
			else{
				var tb=$1.T.table(['','',''],0,tbWrap);
				var tBody=$1.t('tbody',0,tb);
				$Api.JS.addF({name:'docEntry',value:Pa.docEntry},cont);
				if(Jr.L){ for(var i in Jr.L){ //de L  a Keys
					var k=Jr.L[i].pregunta;
					rL[k]=Jr.L[i];
				}}
				//dibujar datos
				for(var i in $V.xGeDPOB){ var L1=$V.xGeDPOB[i];
					var tr=$1.t('tr',0,tBody);
					$1.t('td',{textNode:L1.k+' '+L1.v,colspan:4,style:'fontWeight:bold'},tr);
					for(var i2 in $V.xGeDPOP){ var L2=$V.xGeDPOP[i2];
						if(L2.B==L1.k){
							var tV=(rL[L2.k])?rL[L2.k]:{};
							var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
							var td=$1.t('td',0,tr);
							$1.t('span',{'class':'fa fa-star'},td);
							var sele=$1.T.sel({'class':jsF,name:'aPoints',opts:tP,'class':jsFN,style:'width:40px',selected:tV.aPoints,AJs:{bloque:L1.k,pregunta:L2.k}},td);
							$1.t('td',{textNode:L2.k+' '+L2.v,style:'fontWeight:bold',colspan:2},tr);
							if(tV.id){ sele.AJs.id=tV.id; }
							var td=$1.t('td',0,tr);
							var dD={docEntry:Pa.docEntry,bloque:L1.k,pregunta:L2.k};
							$1.T.btnFa({faBtn:'fa-warning',title:'Registra Hallazgo',P:dD,func:function(T){ xGen.Eva.desForm(T.P); }},td);
							$1.T.btnFa({faBtn:'fa-heartbeat',title:'Registrar Acción de Mejora',P:dD,func:function(T){ xGen.Eva.accForm(T.P); }},td);;
							if(0){for(var i3 in $V.xGeDPOR){ var L3=$V.xGeDPOR[i3];
								if(L3.P==L2.k){
									var tr=$1.t('tr',0,tBody);
									$1.t('td',0,tr);
									$1.t('td',0,tr);
									$1.t('td',{textNode:L3.v},tr);
									
								}
							}}
						}
					}
				}
			}
			var resp=$1.t('div',0,cont);
			var tS={POST:Api.xGe.b+'eva',jsBody:cont,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
				if(!Pa.docEntry && o && o.docEntry){ $M.to('xGenEva.form','docEntry:'+o.docEntry); xGen.Eva.form(); }
			}};
			if(Pa.docEntry){ tS.PUT=tS.POST; delete(tS.POST); }
			$Api.send(tS,cont);
		}});
	},
	accGet:function(){
		var cont=$M.Ht.cont; Pa=$M.read();
		$Api.get({f:Api.xGe.b+'eva/acc',inputs:'docEntry='+Pa.docEntry,loade:cont,errWrap:cont,func:function(Jr){
			var tb=$1.T.table(['','Fecha','Estado','Titulo','Detalles','Plazo','Fecha Cierre','Detalles Cierre','Bloque','Pregunta']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ xGen.Eva.accForm(T.P); } },td);
				$1.t('td',{textNode:L.lineDate},tr);
				$1.t('td',{textNode:_g(L.lineStatus,$V.xGeEvaAcc)},tr);
				$1.t('td',{textNode:L.lineTitle},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:L.lineDue},tr);
				$1.t('td',{textNode:L.lineClose},tr);
				$1.t('td',{textNode:L.lineMemoClose},tr);
				$1.t('td',{textNode:L.bloque},tr);
				$1.t('td',{textNode:L.pregunta},tr);
			}
			$1.T.tbExport(tb,{ext:'xlsx',print:'Y'},cont);
		}});
	},
	accForm:function(Pa){
		var wrap=$1.t('div'); var jsF=$Api.JS.cls;
		$Api.get({f:Api.xGe.b+'eva/acc/form',loadVerif:!Pa.id,inputs:'id='+Pa.id,loade:wrap,func:function(Jr){
			if(Pa.id){ $Api.JS.addF({name:'id',value:Pa.id},wrap); }
			$Api.JS.addF({name:'docEntry',value:Pa.docEntry},wrap);
			$Api.JS.addF({name:'bloque',value:Pa.bloque},wrap);
			$Api.JS.addF({name:'pregunta',value:Pa.pregunta},wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Fecha',I:{tag:'input',type:'date','class':jsF,name:'lineDate',value:Jr.lineDate}},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Plazo',I:{tag:'input',type:'date','class':jsF,name:'lineDue',value:Jr.lineDue}},divL);
			$1.T.divL({wxn:'wrapx2',L:'Titulo',I:{tag:'input',type:'text','class':jsF,name:'lineTitle',value:Jr.lineTitle}},divL);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Observación / detalles',I:{tag:'textarea','class':jsF,name:'lineMemo',value:Jr.lineMemo}},wrap);
			$1.T.divLTitle('Estado de la acción de mejora',wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Estado',I:{tag:'select','class':jsF,name:'lineStatus',opts:$V.xGeEvaAcc,noBlank:'Y',selected:Jr.lineStatus}},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Fecha Realizada',I:{tag:'input',type:'date','class':jsF,name:'lineClose',value:Jr.lineClose}},divL);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Detalles del cierre',I:{tag:'textarea','class':jsF,name:'lineMemoClose',value:Jr.lineMemoClose}},wrap);
			var resp=$1.t('div',0,wrap);
			$Api.send({PUT:Api.xGe.b+'eva/acc',jsBody:wrap,loade:resp,func:function(Jr2){
				$Api.resp(resp,Jr2);
			}},wrap);
		}});
		$1.Win.open(wrap,{winTitle:'Acción de Mejora',winSize:'medium'});
	},
	desGet:function(){
		var cont=$M.Ht.cont; Pa=$M.read();
		$Api.get({f:Api.xGe.b+'eva/des',inputs:'docEntry='+Pa.docEntry,loade:cont,errWrap:cont,func:function(Jr){
			var tb=$1.T.table(['','Fecha','Estado','Titulo','Detalles','Plazo','Fecha Solución','Detalles Solución','Bloque','Pregunta']);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ xGen.Eva.desForm(T.P); } },td);
				$1.t('td',{textNode:L.lineDate},tr);
				$1.t('td',{textNode:_g(L.lineStatus,$V.xGeEvaAcc)},tr);
				$1.t('td',{textNode:L.lineTitle},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:L.lineDue},tr);
				$1.t('td',{textNode:L.lineClose},tr);
				$1.t('td',{textNode:L.lineMemoClose},tr);
				$1.t('td',{textNode:L.bloque},tr);
				$1.t('td',{textNode:L.pregunta},tr);
			}
			$1.T.tbExport(tb,{ext:'xlsx',print:'Y'},cont);
		}});
	},
	desForm:function(Pa){
		var wrap=$1.t('div'); var jsF=$Api.JS.cls;
		$Api.get({f:Api.xGe.b+'eva/des/form',loadVerif:!Pa.id,inputs:'id='+Pa.id,loade:wrap,func:function(Jr){
			if(Pa.id){ $Api.JS.addF({name:'id',value:Pa.id},wrap); }
			$Api.JS.addF({name:'docEntry',value:Pa.docEntry},wrap);
			$Api.JS.addF({name:'bloque',value:Pa.bloque},wrap);
			$Api.JS.addF({name:'pregunta',value:Pa.pregunta},wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Fecha',I:{tag:'input',type:'date','class':jsF,name:'lineDate',value:Jr.lineDate}},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Plazo',I:{tag:'input',type:'date','class':jsF,name:'lineDue',value:Jr.lineDue}},divL);
			$1.T.divL({wxn:'wrapx2',L:'Titulo',I:{tag:'input',type:'text','class':jsF,name:'lineTitle',value:Jr.lineTitle}},divL);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Observación / detalles',I:{tag:'textarea','class':jsF,name:'lineMemo',value:Jr.lineMemo}},wrap);
			$1.T.divLTitle('Estado del Hallazgo',wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Estado',I:{tag:'select','class':jsF,name:'lineStatus',opts:$V.xGeEvaAcc,noBlank:'Y',selected:Jr.lineStatus}},wrap);
			$1.T.divL({wxn:'wrapx4',L:'Fecha Solución',I:{tag:'input',type:'date','class':jsF,name:'lineClose',value:Jr.lineClose}},divL);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Detalles del cierre',I:{tag:'textarea','class':jsF,name:'lineMemoClose',value:Jr.lineMemoClose}},wrap);
			var resp=$1.t('div',0,wrap);
			$Api.send({PUT:Api.xGe.b+'eva/des',jsBody:wrap,loade:resp,func:function(Jr2){
				$Api.resp(resp,Jr2);
			}},wrap);
		}});
		$1.Win.open(wrap,{winTitle:'Hallazgo',winSize:'medium'});
	},

}

xGen.Cyp={
OLg:function(L){
	var Li=[];
	Li.push({ico:'fa fa-eye',textNode:' Visualizar', P:L, func:function(T){ $M.to('xGenCyp.view','docEntry:'+T.P.docEntry); } });
	if(L.canceled=='N'){
		Li.push({k:'statusN',ico:'fa fa_prio_high',textNode:' Anular Documento', P:L, func:function(T){ $Doc.statusDefine({reqMemo:'N',docEntry:T.P.docEntry,api:Api.xGe.b+'cyp/statusCancel',text:'Se va anular el documento.'}); } });
	}
	return $Opts.add('xGenCyp',Li,L);;
},
opts:function(P,pare){
	Li={Li:xGen.Cyp.OLg(P.L),PB:P.L,textNode:P.textNode};
	var mnu=$1.Menu.winLiRel(Li);
	if(pare){ pare.appendChild(mnu); }
	return mnu;
},
get:function(){
	var cont=$M.Ht.cont;
	$Doc.tbList({api:Api.xGe.b+'cyp',inputs:$1.G.filter(),
	OLg:xGen.Cyp.OLg,view:'Y',docBy:'ud',docByUpd:'ud',
	tbSerie:'xGenCyp',
	TD:[
		{H:'Estado',k:'docStatus',_V:'docStatusAll'},
		{H:'Fecha',k:'docDate'},
		{H:'Empleado',k:'cardName'},
		{H:'Clase',k:'docClass',_g:$JsV.xGeCypCls},
		{H:'Detalles',k:'lineMemo'},
	]
	},cont);
},
form:function(){
	var D={}; var Pa=$M.read();
	var topCont=$M.Ht.cont;
	var AJs={};
	var crdVal=(D.cardId)?D.cardName:'';
	$Api.get({f:Api.xGe.b+'cyp/form',inputs:'docEntry='+Pa.docEntry,loadVerif:!Pa.docEntry,loade:topCont,func:function(Jr){
		if(Jr){ D=Jr; }
		if(!D.docDate){ D.docDate=$2d.today; }
		var rbCode= true;//$Mdl.defCnf('ivtDotDoeBCode');
		if(rbCode){
			var tagCrd={lTag:'disabled',L:'Empleado',wxn:'wrapx3',req:'Y',I:{name:'cardId','class':'__crd'}};
			Nom.Crd.Bio.bCode(function(Da){
				var qt=$1.q('.__crd');
				qt.value=Da.cardName; qt.AJs={cardId:Da.cardId};
			},{pare:topCont});
		}
		else{
			tagCrd={lTag:'card',L:'Empleado',wxn:'wrapx3',req:'Y',I:{topPare:cont,D:D,AJsPut:['cardName'],cardType:'E'}};
		}
		var cont=$1.t('div',0,topCont); //separar
		$Doc.form({tbSerie:'xGenCyp',cont:cont,docEdit:Pa.docEntry,POST:Api.xGe.b+'cyp',func:D.func,
		HLs:[
			{lTag:'date',L:'Fecha',wxn:'wrapx8',req:'Y',I:{name:'docDate',value:D.docDate}},
			tagCrd,
			{lTag:'select',L:'Clase',wxn:'wrapx8',req:'Y',aGo:'jsv.xGeCypCls',I:{name:'docClass',opts:$JsV.xGeCypCls,selected:D.docClass}},
			{divLine:1,lTag:'textarea',L:'Detalles',wxn:'wrapx1',I:{name:'lineMemo',textNode:D.lineMemo}}
		]
		});
	}});
},
view:function(){
	var cont=$M.Ht.cont; var Pa=$M.read(); var jsF=$Api.JS.cls;
	$Api.get({f:Api.xGe.b+'cyp/view',inputs:'docEntry='+Pa.docEntry,loade:cont,func:function(Jr){
		var tP={tbSerie:'xGenCyp',D:Jr,
			topBtns:{OLg:xGen.Cyp.OLg,print:'Y',D:Jr},
			THs:[
				{sdocNum:1},{sdocTitle:1,cs:7,ln:1},
				,{t:'Estado',k:'docStatus',_V:'docStatusAll'},{middleInfo:'Y'},{logo:'Y'},
				{t:'Fecha',k:'docDate'},
				{t:'Clase',k:'docClass',_g:$JsV.ivtDotDoeCls},
				{k:'licTradType',_V:'licTradType'},{k:'licTradNum',ln:1},{k:'cardName',cs:4,ln:1},
				{k:'lineMemo',cs:8,addB:$1.t('b',{textNode:'Detalles:\u0020'}),HTML:1,Tag:{'class':'pre'}},
			]
		};
		$Doc.view(cont,tP);
	}});
}
}
xGen.B1={};
xGen.B1.Presu={
	Lg:function(L,men){
		var Li=[];
		Li.push({k:'desvia',ico:'fa fa-warning',textNode:'Desviaciones',P:L,func:function(T){  }});
		Li.push({k:'desvia',ico:'fa fa-thumbs-o-up',textNode:'Acciones',P:L,func:function(T){  }});
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ xGen.B1.Presu.form(T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'novInc',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['Año','Mes','Detalles','Creado'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			Jr.L=[{docYear:'2020',docMonth:'05',lineMemo:'Prueba de presupuesto',userId:1,dateC:'2020-05-04 10:32:01'}];
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				var td=$1.t('td',{textNode:''},tr);
				xGen.B1.Presu.Lg(L,td);
				$1.t('td',{textNode:L.docYear},tr);
				$1.t('td',{textNode:L.docMonth},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
				$1.t('td',{textNode:$Doc.by(L)},tr);
			}
		}});
	},
	form:function(){
		var cont=$M.Ht.cont;
		var _iV=[{k:1,v:'Nómina'},{k:2,v:'Horas Capacitación'},{k:3,v:'Accidentes'},{k:4,v:'Rentabilidad'},{k:5,v:'Cjs Mov/Fte'},{k:6,v:'Cjs Mov/H Maq'},{k:7,v:'% Ajuste Inv'}];
		{
			Jr={L:[]};
			var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Fecha',req:'Y',I:{tag:'input',type:'date',name:'docDate',value:Jr.docDate}},cont);
			$1.T.divL({wxn:'wrapx8',L:'Estado',req:'Y',I:{tag:'select',name:'docStatus',opts:$V.docStatusOCN,selected:Jr.docStatus,noBlank:'Y'}},divL);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea',type:'text',name:'lineMemo',value:Jr.lineMemo}},cont);
			var Tbs=['Real','Presupuesto','Concepto'];
			var tb=$1.T.table(Tbs,0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in _iV){ L=_iV[i]; 
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.t('input',{tag:'input',type:'text',name:'real',style:'width:8rem;'},td);
				var td=$1.t('td',0,tr);
				$1.t('input',{tag:'input',type:'text',name:'presu',style:'width:8rem;'},td);
				var td=$1.t('td',{textNode:L.v},tr);
			}
			$Api.send({PUT:'',textNode:'Guardar Presupuesto'},cont);
		}
	}
}

xGen.B1.Cae={
	Lg:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ xGen.B1.Cae.form(T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'b1Cae',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['','Estado','Fecha','Empleado','Horas','Detalles'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				xGen.B1.Cae.Lg(L,td);
				$1.t('td',{textNode:_g(L.docStatus,$V.docStatusOCN)},tr);
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.hrsTotal*1},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
			}
		}});
	},
	form:function(Pa){
		var Pa=(Pa)?Pa:{};
		var api1=Api.xGe.b+'b1Cae';
		var jsF=$Api.JS.cls;
		var wrap=$1.t('div');
		$Api.get({f:api1+'/form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:wrap,func:function(Jr){
			if(Pa.docEntry){ $Api.JS.addF({name:'docEntry',value:Pa.docEntry},wrap); }
			var nd=Nom.Sea.crd(Jr,wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx6_1',L:'Empleado',Inode:nd},wrap);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx6',L:'Fecha',I:{tag:'input',type:'date',name:'docDate','class':jsF,value:Jr.docDate}},wrap);
			$1.T.divL({wxn:'wrapx6',L:'Estado',I:{tag:'select',name:'docStatus','class':jsF,opts:$V.docStatusOCN,selected:Jr.docStatus,noBlank:'Y'}},divL);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea',name:'lineMemo','class':jsF,value:Jr.lineMemo}},wrap);
			var tb=$1.T.table(['Tipo','Detalle Entrenamiento','Tiempo (hr)','Resultado','Observación',''],0,wrap);
			var tBody=$1.t('tbody',0,tb);
			if(Jr.L){ for(var i in Jr.L){
				trA(Jr.L[i],tBody);
			}}
			else{ trA([{}],tBody); }
			$1.T.btnFa({faBtn:'fa_plusCircle',textNode:'Añadir Linea',P:L,func:function(T){ trA(T.P,tBody); }},wrap);
			var resp=$1.t('div',0,wrap);
			var Ps={PUT:api1,jsBody:wrap,loade:resp,func:function(Jr2,o){
				$Api.resp(resp,Jr2);
			}};
			$Api.send(Ps,wrap);
		}});
		var wrapBk=$1.Win.open(wrap,{winSize:'medium',winTitle:'Entrenamiento Recibido'});
		function trA(L,tBody){
			L=(L)?L:{};
			var tr=$1.t('tr',{'class':$Api.JS.clsL},tBody);
			var jsF2=$Api.JS.clsLN;
			var td=$1.t('td',0,tr);
			$1.T.sel({'class':jsF2,name:'lineType',opts:$JsV.xGeCae,selected:L.lineType},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF2,name:'lineText',value:L.lineText,style:'width:10rem'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF2,name:'lineHrs',value:L.lineHrs,style:'width:3rem'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF2,name:'lineText2',value:L.lineText2,style:'width:8rem'},td);
			var td=$1.t('td',0,tr);
			$1.t('input',{type:'text','class':jsF2,name:'lineMemo',value:L.lineMemo,style:'width:8rem'},td);
			var td=$1.t('td',0,tr);
			if(L.id){ 
				$1.T.check({'class':jsF2,name:'delete',AJs:{id:L.id}},td);
			}
			else{ $1.T.btnFa({faBtn:'fa-trash',title:'Quitar',P:L,func:function(T){ $1.delet(tr); }},td);
			}
		}
	},
	rep1:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'b1Cae/rep1',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['Empleado','Fecha','Tipo','Horas','Entranamiento','Resultado','Observación'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:_g(L.lineType,$JsV.xGeCae)},tr);
				$1.t('td',{textNode:L.hrsTotal*1},tr);
				$1.t('td',{textNode:L.lineText},tr);
				$1.t('td',{textNode:L.lineText2},tr);
				$1.t('td',{textNode:L.lineMemo},tr);
			}
		}});
	}
}

/* sin $m.li */
xGen.B1.Rindu={
	Lg:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ JForm.h('form','xGen33rindu',T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	LgDocs:function(L,td){
		$1.T.btnFa({faBtn:'fa-paper-plane',title:'Respuestas Registradas',P:L,func:function(T){ JForm.h('docs','xGen33rindu',T.P); }},td);
		$1.T.btnFa({faBtn:'fa-user-plus',title:'Nuevo Registro',P:L,func:function(T){ JForm.h('ans','xGen33rindu',T.P); }},td);
		$1.T.btnFa({faBtn:'fa-bolt',title:'Reporte de Respuestas',P:L,func:function(T){ JForm.h('rep','xGen33rindu',T.P); }},td);
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'b1Rindu/forms',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['','Código','Nombre','Versión'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				xGen.B1.Rindu.LgDocs(L,td);
				$1.t('td',{textNode:L.fCode},tr);
				$1.t('td',{textNode:L.fName},tr);
				$1.t('td',{textNode:L.version},tr);
			}
		}});
	},
	docs:function(){
		var cont=$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.xGe.b+'b1Rindu/docs',inputs:'fId='+Pa.fId+'&'+$1.G.filter(),loade:cont,errWrap:cont,func:function(Jr){
			$1.t('h5',{textNode:Jr.fCode+' - '+Jr.fName},cont);
			if(Jr.descrip){ $1.t('p',{textNode:Jr.descrip},cont); }
			var tb=$1.T.table(['','Fecha','Empleado','Duración (min)'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ JForm.h('ans','xGen33rindu',T.P); } },td);
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.timeTotal*1},tr);
			}
		}});
	},
	ans:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		var vPost='fId='+Pa.fId;
		if(Pa.docEntry){ vPost +='&docEntry='+Pa.docEntry; }
		$Api.get({f:Api.xGe.b+'b1Rindu/ans',inputs:vPost,loade:cont,errWrap:cont, func:function(Jr){ Jr.fId=Pa.fId;
			$1.t('h5',{textNode:Jr.fCode+' - '+Jr.fName},cont);
			var nd=Nom.Sea.crd(Jr,cont);
			var xF= new JForm.Res({Jr:Jr,api:Api.xGe.b+'b1Rindu/ans',func:function(Jr2){
			},
			F:[
				{divLine:1,wxn:'wrapx8',L:'Fecha',I:{tag:'input',type:'date',name:'docDate',value:Jr.docDate}},
				{wxn:'wrapx4',L:'Empleado',Inode:nd},
				{wxn:'wrapx8',L:'Tiempo (min)',I:{tag:'input',type:'number',name:'timeTotal',value:Jr.timeTotal}},
				{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea',name:'lineMemo',value:Jr.lineMemo}},
			]
			},
			cont);
			xF.send();
		}});
	},
	rep:function(){
		var cont=$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.xGe.b+'b1Rindu/rep',inputs:'fId='+Pa.fId+'&'+$1.G.filter(),loade:cont,func:function(Jr){
			R=JForm.repData(Jr,{Tbf:['Documento','Empleado','Fecha']});
			var tb=$1.T.table(R.Tbf);
			var tBody=$1.t('tbody',0,tb);
			for(var i in R.L){ L=R.L[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.docEntry},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.docDate},tr);
				for(var i in R.aL){
					val=(L.a[R.aL[i].aid])?L.a[R.aL[i].aid].aText:'';
					$1.t('td',{textNode:val},tr);
				}
				$1.T.tbExport(tb,{ext:'xlsx',print:'Y'},cont);
			}
		}});
	},
	forms:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'b1Rindu/forms',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['','Código','Nombre','Versión'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				xGen.B1.Rindu.Lg(L,td);
				$1.t('td',{textNode:L.fCode},tr);
				$1.t('td',{textNode:L.fName},tr);
				$1.t('td',{textNode:L.version},tr);
			}
		}});
	},
	form:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'b1Rindu/form',loadVerif:!Pa.fId,inputs:'fId='+Pa.fId,loade:cont,errWrap:cont, func:function(Jr){
			var xF=new JForm.Tpt({Jr:Jr,api:Api.xGe.b+'b1Rindu/form',func:function(Jr2){
			}},
			cont,{
				Prp:[{t:'Departamento',opts:$V.NY},{t:'Otro',opts:[]}]
			});
			xF.send();
		}});
	}
}

xGen.B1.Cdp={
	Lg:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ JForm.h('form','xGen33Cdp',T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	LgDocs:function(L,men){
		var Li=[];
		$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ xGen.B1.Cdp.pForm(T.P); }},men);
		$1.T.btnFa({faBtn:'fa-hand-o-right',title:'Firma Digital',P:L,func:function(T){ 
			JForm.Sign.win({api:Api.xGe.b+'b1Cdp/p/sign',pdocEntry:T.P.pdocEntry});
		}},men);
		Li.push({k:'attach',ico:'fa fa-paperclip',textNode:' Archivos', P:L, func:function(T){ Attach.openWin({tt:'xGenCdp',tr:T.P.pdocEntry, title:'Capacitación # '+T.P.pdocEntry}) } });
		Li.push({k:'public',ico:'fa fa-paper-plane',textNode:'Evalucaciones Registradas',P:L,func:function(T){ JForm.h('docs','xGen33Cdp',T.P); }});
		Li.push({ico:'fa fa-user-plus',textNode:'Registrar Evaluación',P:L,func:function(T){ JForm.h('ans','xGen33Cdp',T.P); }});
		Li.push({ico:'fa fa-bolt',textNode:'Reporte de Evaluaciones',P:L,func:function(T){ JForm.h('rep','xGen33Cdp',T.P); }});
		Li={Li:Li,PB:L};
		return $1.Menu.winLiRel(Li,men);
	},
	pGet:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'b1Cdp/p',inputs:$1.G.filter(),loade:cont,errWrap:cont,func:function(Jr){
			var tb=$1.T.table(['','#','Fecha','Código','Nombre','Versión','Partic.','Publico','Realizado'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				xGen.B1.Cdp.LgDocs(L,td);
				$1.t('td',{textNode:L.pdocEntry},tr);
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:L.fCode},tr);
				$1.t('td',{textNode:L.fName},tr);
				$1.t('td',{textNode:L.version},tr);
				$1.t('td',{textNode:L.peoQty},tr);
				$1.t('td',{textNode:_g(L.prp2,$V.xGeB1CdpPo)},tr);
				$1.t('td',{textNode:$Doc.by('d',L)},tr);
			}
		}});
	},
	pForm:function(Pa){
		var Pa=(Pa)?Pa:{};
		var cont=$1.t('div'); var jsF=$Api.JS.cls;
		$Api.get({f:Api.xGe.b+'b1Cdp/p/form',loadVerif:!Pa.pdocEntry,inputs:'pdocEntry='+Pa.pdocEntry,loade:cont,errWrap:cont, func:function(Jr){
			Jr.lineText=Jr.fName;
			if(Pa.pdocEntry){ $Api.JS.addF({name:'pdocEntry',value:Pa.pdocEntry},cont); }
			var divL=$1.T.divL({divLine:1,wxn:'wrapx1',L:'Formulario',Inode:JForm.Sea.one(Jr,cont,{api:Api.xGe.b+'b1Cdp/forms/sea'})},cont);
			var divL=$1.T.divL({divLine:1,wxn:'wrapx4',L:'Fecha',I:{tag:'input',type:'date','class':jsF,name:'docDate',value:Jr.docDate}},cont);
			$1.T.divL({wxn:'wrapx8',L:'Sede',I:{tag:'select','class':jsF,name:'prp1',opts:$Tb.owsu,selected:Jr.prp1}},divL);
			$1.T.divL({wxn:'wrapx8',L:'Público',I:{tag:'select','class':jsF,name:'prp2',opts:$V.xGeB1CdpPo,selected:Jr.prp2}},divL);
			$1.T.divL({divLine:1,wxn:'wrapx1',L:'Añadir Participante',Inode:Nom.Sea.crd({jsF:'N',clearInp:'Y',func:function(R){
				trA(R,rEmp);
			}})},cont);
			var rEmp=$1.t('div',0,$1.T.fieset({L:{textNode:'Participantes'}},cont));
			function trA(L,tBody){
				if(L.tr){ L.cardId=L.tr; }
				tag=$1.t('div',{textNode:L.cardName});
				var tp=$Api.JS.uniqAJs(tag,{tt:'card',tr:L.cardId},{L:'Y',uniq:L.cardId},tBody);
				$1.lineDel(L,null,tag);
			}
			if(Jr.L && !Jr.L.errNo){ for(var i in Jr.L){ trA(Jr.L[i],rEmp); } }
			var resp=$1.t('div',0,cont);
			Ps={POST:Api.xGe.b+'b1Cdp/p',jsBody:cont,loade:resp,func:function(Jr2){
				if(!Jr2.errNo){ $Api.resp(resp,Jr2); }
				else{ $Api.resp(cont,Jr2); }
			}};
			if(Pa.pdocEntry){ Ps.PUT=Ps.POST; delete(Ps.POST); }
			$Api.send(Ps,cont);
		}});
		$1.Win.open(cont,{winSize:'medium',winTitle:'Planificar Capacitación'});
	},
	docs:function(){
		var cont=$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.xGe.b+'b1Cdp/docs',inputs:'fId='+Pa.fId+'&'+$1.G.filter(),loade:cont,errWrap:cont,func:function(Jr){
			$1.t('h5',{textNode:Jr.fCode+' - '+Jr.fName},cont);
			if(Jr.descrip){ $1.t('p',{textNode:Jr.descrip},cont); }
			var tb=$1.T.table(['','Fecha','Empleado','Duración (min)'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ JForm.h('ans','xGen33Cdp',T.P); } },td);
				$1.T.btnFa({faBtn:'fa-trophy',title:'Calificar',P:L,func:function(T){ JForm.h('qua','xGen33Cdp',T.P); } },td);
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.timeTotal*1},tr);
			}
		}});
	},
	ans:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		var vPost='fId='+Pa.fId;
		if(Pa.docEntry){ vPost +='&docEntry='+Pa.docEntry; }
		$Api.get({f:Api.xGe.b+'b1Cdp/ans',inputs:vPost,loade:cont,errWrap:cont, func:function(Jr){ Jr.fId=Pa.fId;
			$1.t('h5',{textNode:Jr.fCode+' - '+Jr.fName},cont);
			var nd=Nom.Sea.crd(Jr,cont);
			var xF= new JForm.Res({Jr:Jr,api:Api.xGe.b+'b1Cdp/ans',func:function(Jr2){
			},
			F:[
				{divLine:1,wxn:'wrapx8',L:'Fecha',I:{tag:'input',type:'date',name:'docDate',value:Jr.docDate}},
				{wxn:'wrapx4',L:'Empleado',Inode:nd},
				{wxn:'wrapx8',L:'Tiempo (min)',I:{tag:'input',type:'number',name:'timeTotal',value:Jr.timeTotal}},
				{wxn:'wrapx8',L:'Calificación',I:{tag:'input',type:'number',name:'qualNum',value:Jr.qualNum}},
				{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea',name:'lineMemo',value:Jr.lineMemo}},
			]
			},
			cont);
			xF.send();
		}});
	},
	qua:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		var vPost='fId='+Pa.fId;
		if(Pa.docEntry){ vPost +='&docEntry='+Pa.docEntry; }
		$Api.get({f:Api.xGe.b+'b1Cdp/qua',inputs:vPost,loade:cont,errWrap:cont, func:function(Jr){ Jr.fId=Pa.fId;
			$1.t('h5',{textNode:Jr.fCode+' - '+Jr.fName},cont);
			var nd=Nom.Sea.crd(Jr,cont);
			var xF= new JForm.Qua({Jr:Jr,api:Api.xGe.b+'b1Cdp/qua',func:function(Jr2){
			},
			F:[
				{divLine:1,wxn:'wrapx1',L:'Detalles',I:{tag:'textarea',name:'qualMemo',value:Jr.qualMemo}},
			]
			},
			cont);
			xF.send();
		}});
	},
	rep:function(){
		var cont=$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.xGe.b+'b1Cdp/rep',inputs:'fId='+Pa.fId+'&'+$1.G.filter(),loade:cont,func:function(Jr){
			R=JForm.repData(Jr,{Tbf:['Documento','Empleado','Fecha']});
			var tb=$1.T.table(R.Tbf);
			var tBody=$1.t('tbody',0,tb);
			for(var i in R.L){ L=R.L[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.docEntry},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.docDate},tr);
				for(var i in R.aL){
					val=(L.a[R.aL[i].aid])?L.a[R.aL[i].aid].aText:'';
					$1.t('td',{textNode:val},tr);
				}
				$1.T.tbExport(tb,{ext:'xlsx',print:'Y'},cont);
			}
		}});
	},
	forms:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'b1Cdp/forms',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['','Código','Nombre','Clase','Versión','Publico Objetivo'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				xGen.B1.Cdp.Lg(L,td);
				$1.t('td',{textNode:L.fCode},tr);
				$1.t('td',{textNode:L.fName},tr);
				$1.t('td',{textNode:_g(L.docClass,$JsV.nomCapCls)},tr);
				$1.t('td',{textNode:L.version},tr);
				$1.t('td',{textNode:_g(L.prp1,$V.xGeB1CdpPo)},tr);
			}
		}});
	},
	form:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'b1Cdp/form',loadVerif:!Pa.fId,inputs:'fId='+Pa.fId,loade:cont,errWrap:cont, func:function(Jr){
			var xF=new JForm.Tpt({Jr:Jr,api:Api.xGe.b+'b1Cdp/form',
			docClass:{wxn:'wrapx8',L:'Clase',I:{lTag:'select',opts:$JsV.nomCapCls}},
			func:function(Jr2){
			}},
			cont,{
				Prp:[{t:'Publico Objetivo',opts:$V.xGeB1CdpPo}]
			});
			xF.send();
		}});
	}
}

xGen.Tor={};
xGen.Tor.Gente={
	OLg:function(L){
		var Li=[];
		Li.push({ico:'fa fa-eye',textNode:' Visualizar', P:L, func:function(T){ $Doc.go('nomAil','v',T.P,1); } });
		if(L.canceled=='N'){
			Li.push({k:'edit',ico:'fa fa-pencil',textNode:' Modificar', P:L, func:function(T){ $M.docEntry('xGenTor.gente.form',T.P.docEntry); } });
		}
		return Li;
	},
	opts:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ $M.to('xGenTor.gente.form','docEntry:'+T.P.docEntry); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'torGente',inputs:$1.G.filter(),loade:cont,errWrap:cont,func:function(Jr){
			var tb=$1.T.table(['','Fecha','Asunto','Sede','Tiempo'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				xGen.Tor.Gente.opts(L,td);
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:L.docTitle},tr);
				$1.t('td',{textNode:_g(L.workSede,$JsV.nomWorkSede)},tr);
				$1.t('td',{textNode:L.timeTotal*1+' Min'},tr);
			}
		}});
	},
	view:function(){
		var cont=$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.xGe.b+'torGente/form',inputs:'docEntry='+Pa.docEntry,loade:cont,errWrap:cont, func:function(Jr){
			Jr=$1.Bolb.sett('fieForm',Jr,['location','deals','people','rules','agenda','entrada','salida','kpis']);
			$Doc.view(cont,{tbSerie:'N',D:Jr,topBtns:{OLg:xGen.Tor.Gente.OLg,D:Jr,print:'Y'},
			THs:[
				{docEntry:1},{docTitle:'Reunión GENTE',cs:7,ln:1},
				{t:'Fecha',k:'docDate'},{middleInfo:'Y'},{logo:'Y'},
				{k:'workSede',_g:$JsV.nomWorkSede,cs:2},
				{k:'location',cs:2},
				{t:'Asunto',k:'docTitle',cs:7},
				{k:'deals',cs:8,addB:$1.t('h3',{textNode:'Objetivos','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
				{k:'people',cs:8,addB:$1.t('h3',{textNode:'Participantes','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
				{k:'rules',cs:8,addB:$1.t('h3',{textNode:'Reglas','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
				{k:'agenda',cs:8,addB:$1.t('h3',{textNode:'Agenda','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
				{k:'entrada',cs:8,addB:$1.t('h3',{textNode:'Entrada','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
				{k:'salida',cs:8,addB:$1.t('h3',{textNode:'Salida','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
				{k:'kpis',cs:8,addB:$1.t('h3',{textNode:'KPis','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
			],
			});
		}});
	},
	form:function(){
		var Pa=$M.read();
		var api1=Api.xGe.b+'torGente/';
		var cont=$M.Ht.cont;
		$Api.get({f:api1+'form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,errWrap:cont,func:function(Jr){
			var tAreaCss='resize:vertical; height:125px;';
			Jr=$1.Bolb.sett('fieForm',Jr,['location','deals','people','rules','agenda','entrada','salida','kpis']);
			$Doc.form({go:'xGenTor.gente',tbSerie:'N',cont:cont,docEdit:Pa.docEntry,POST:api1,
			HLs:[
				{L:'Fecha',divLine:1,lTag:'date',wxn:'wrapx8',req:'Y',I:{name:'docDate',value:Jr.docDate}},
				{L:'Asunto',lTag:'input',wxn:'wrapx2',req:'Y',I:{name:'docTitle',value:Jr.docTitle}},
				{L:'Sede',lTag:'select',wxn:'wrapx8',req:'Y',I:{name:'workSede',value:Jr.workSede,opts:$JsV.nomWorkSede}},
				{L:'Tiempo (Min)',lTag:'input',wxn:'wrapx8',I:{name:'timeTotal',value:Jr.timeTotal}},
				{L:'Lugar',lTag:'input',wxn:'wrapx8',I:{name:'fieForm',value:Jr.location,apijsfie:$1.Bolb.fie}},
				{L:'Objetivos',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.deals,apijsfie:$1.Bolb.fie,style:tAreaCss}},
				{L:'Participantes',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.people,apijsfie:$1.Bolb.fie,style:tAreaCss}},
				{L:'Reglas',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.rules,apijsfie:$1.Bolb.fie,style:tAreaCss}},
				{L:'Agenda',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.agenda,apijsfie:$1.Bolb.fie,style:tAreaCss}},
				{L:'Entrada',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.entrada,apijsfie:$1.Bolb.fie,style:tAreaCss}},
				{L:'Salida',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.salida,apijsfie:$1.Bolb.fie,style:tAreaCss}},
				{L:'Kpis',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.kpis,apijsfie:$1.Bolb.fie,style:tAreaCss}}
			]
			});
		}});
	},
}
xGen.Tor.Seg={
	OLg:function(L){
		var Li=[];
		if(L.canceled=='N'){
			Li.push({k:'edit',ico:'fa fa-pencil',textNode:' Modificar', P:L, func:function(T){ $M.docEntry('xGenTor.seg.form',T.P.docEntry); } });
		}
		return Li;
	},
	opts:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ $M.to('xGenTor.seg.form','docEntry:'+T.P.docEntry); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	get:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'torSeg',inputs:$1.G.filter(),loade:cont,errWrap:cont,func:function(Jr){
			var tb=$1.T.table(['','Fecha','Asunto','Sede','Tiempo'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				xGen.Tor.Seg.opts(L,td);
				$1.t('td',{textNode:L.docDate},tr);
				$1.t('td',{textNode:L.docTitle},tr);
				$1.t('td',{textNode:_g(L.workSede,$JsV.nomWorkSede)},tr);
				$1.t('td',{textNode:L.timeTotal*1+' Min'},tr);
			}
		}});
	},
	view:function(){
		var cont=$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.xGe.b+'torSeg/form',inputs:'docEntry='+Pa.docEntry,loade:cont,errWrap:cont, func:function(Jr){
			Jr=$1.Bolb.sett('fieForm',Jr,['location','deals','people','rules','agenda','entrada','salida','kpis']);
			$Doc.view(cont,{tbSerie:'N',D:Jr,topBtns:{OLg:xGen.Tor.Seg.OLg,D:Jr,print:'Y'},
			THs:[
				{docEntry:1},{docTitle:'Reunión Seguridad',cs:7,ln:1},
				{t:'Fecha',k:'docDate'},{middleInfo:'Y'},{logo:'Y'},
				{k:'workSede',_g:$JsV.nomWorkSede,cs:2},
				{k:'location',cs:2},
				{t:'Asunto',k:'docTitle',cs:7},
				{k:'deals',cs:8,addB:$1.t('h3',{textNode:'Objetivos','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
				{k:'people',cs:8,addB:$1.t('h3',{textNode:'Participantes','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
				{k:'rules',cs:8,addB:$1.t('h3',{textNode:'Reglas','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
				{k:'agenda',cs:8,addB:$1.t('h3',{textNode:'Agenda','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
				{k:'entrada',cs:8,addB:$1.t('h3',{textNode:'Entrada','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
				{k:'salida',cs:8,addB:$1.t('h3',{textNode:'Salida','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
				{k:'kpis',cs:8,addB:$1.t('h3',{textNode:'KPis','class':'head1'}),HTML:1,Tag:{'class':'pre'}},
			],
			});
		}});
	},
	form:function(){
		var Pa=$M.read();
		var api1=Api.xGe.b+'torSeg/';
		var cont=$M.Ht.cont;
		$Api.get({f:api1+'form',loadVerif:!Pa.docEntry,inputs:'docEntry='+Pa.docEntry,loade:cont,errWrap:cont,func:function(Jr){
			var tAreaCss='resize:vertical; height:125px;';
			Jr=$1.Bolb.sett('fieForm',Jr,['location','deals','people','rules','agenda','entrada','salida','kpis']);
			$Doc.form({go:'xGenTor.seg',tbSerie:'N',cont:cont,docEdit:Pa.docEntry,POST:api1,
			HLs:[
				{L:'Fecha',divLine:1,lTag:'date',wxn:'wrapx8',req:'Y',I:{name:'docDate',value:Jr.docDate}},
				{L:'Asunto',lTag:'input',wxn:'wrapx2',req:'Y',I:{name:'docTitle',value:Jr.docTitle}},
				{L:'Sede',lTag:'select',wxn:'wrapx8',req:'Y',I:{name:'workSede',value:Jr.workSede,opts:$JsV.nomWorkSede}},
				{L:'Tiempo (Min)',lTag:'input',wxn:'wrapx8',I:{name:'timeTotal',value:Jr.timeTotal}},
				{L:'Lugar',lTag:'input',wxn:'wrapx8',I:{name:'fieForm',value:Jr.location,apijsfie:$1.Bolb.fie}},
				{L:'Objetivos',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.deals,apijsfie:$1.Bolb.fie,style:tAreaCss}},
				{L:'Participantes',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.people,apijsfie:$1.Bolb.fie,style:tAreaCss}},
				{L:'Reglas',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.rules,apijsfie:$1.Bolb.fie,style:tAreaCss}},
				{L:'Agenda',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.agenda,apijsfie:$1.Bolb.fie,style:tAreaCss}},
				{L:'Entrada',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.entrada,apijsfie:$1.Bolb.fie,style:tAreaCss}},
				{L:'Salida',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.salida,apijsfie:$1.Bolb.fie,style:tAreaCss}},
				{L:'Kpis',lTag:'textarea',divLine:1,wxn:'wrapx1',I:{name:'fieForm',value:Jr.kpis,apijsfie:$1.Bolb.fie,style:tAreaCss}}
			]
			});
		}});
	},
}

$JsV._i({kObj:'xGeSuc',mdl:'xge',liK:'xGeSuc',liTxtG:'Sucursales',liTxtF:'Sucursal (Form)'});
$JsV._i({kObj:'xGeCae',mdl:'xge',liK:'xGeCae',liTxtG:'Tipos Entrenamientos',liTxtF:'Entrenamiento (Form)'});
$JsV._i({kObj:'xGeCypCls',mdl:'xge',liK:'xGeCypCls',liTxtG:'Clases Constancias',liTxtF:'Clase Constancia (Form)'});

/* Seguridad */
_Fi['xGenCkPop']=function(wrap,x){
	var Pa=$M.read('!');
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Estado',I:{tag:'select','class':jsV,name:'A.docStatus',opts:$V.docStatusCN}},wrap);
	$1.T.divL({wxn:'wrapx8', L:'Códido Formato',I:{lTag:'input','class':jsV,name:'F.fCode'}},divL);
	$1.T.divL({wxn:'wrapx4',L:'Nombre Formato',I:{lTag:'input','class':jsV,name:'F.fName(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Elemento',I:{lTag:'input','class':jsV,name:'I.itemName(E_like3)'}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Fecha',I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_mayIgual)'}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Fin',I:{tag:'input',type:'date','class':jsV,name:'A.docDate(E_menIgual)'}},divL);
	$Doc.F.ordBy({dateUpd:'Y'},divL);
	$1.T.btnSend({textNode:'Actualizar', func:()=>{ xGen.Ck.Pop.docs(); }},wrap);
};

$M.liAdd('xGen',[
{k:'JForm.forms.xGenCkPop',t:'Formatos Preoperacionales', kau:'JForm.forms', func:function(){
	$M.Ht.ini({btnGo:JForm.h('form','xGenCkPop',0,1),gyp:function(){ xGen.Ck.Pop.forms() }});
}},
{k:'JForm.form.xGenCkPop',t:'Formato de Preoperacional (Form)', kau:'JForm.forms', func:function(){
	$M.Ht.ini({g:function(){ xGen.Ck.Pop.form() }});
}},
{k:'JForm.docs.xGenCkPop',t:'Preoperacionales',d:'Documentos de revisión de preoperacionales', kau:'xGenCkPop', func:function(){
	$M.Ht.ini({f:'xGenCkPop',fNew:function(){ xGen.Ck.Pop.ansNew(); },gyp:function(){ xGen.Ck.Pop.docs() }});
}},
{k:'JForm.doc.xGenCkPop',noTitle:'Y', kau:'xGenCkPop', func:function(){
	$M.Ht.ini({g:function(){ xGen.Ck.Pop.doc() }});
}},
{k:'JForm.ans.xGenCkPop',t:'Formulario de Preoperacional', kau:'xGenCkPop', func:function(){
	$M.Ht.ini({g:function(){ xGen.Ck.Pop.ans() }});
}}
]);

xGen.Ck={}; //listas de chequeo
xGen.Ck.Pop={
	Lg:function(L,men){
		var Li=[];
		if(men){
			$1.T.btnFa({faBtn:'fa-pencil',title:'Modificar',P:L,func:function(T){ JForm.h('form','xGenCkPop',T.P); } },men);
			if(Li.length>0){ Li={Li:Li,PB:L};
			return $1.Menu.winLiRel(Li,men); }
		}
		return Li;
	},
	forms:function(){
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'ckPop/forms',inputs:$1.G.filter(),loade:cont,func:function(Jr){
			var tb=$1.T.table(['','Código','Nombre','Clase','Detalles'],0,cont);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				xGen.Ck.Pop.Lg(L,td);
				$1.t('td',{textNode:L.fCode},tr);
				$1.t('td',{textNode:L.fName},tr);
				$1.t('td',{textNode:_g(L.docClass,$JsV.xGeCkPopCls)},tr);
				$1.t('td',{textNode:L.descrip},tr);
			}
		}});
	},
	form:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		$Api.get({f:Api.xGe.b+'ckPop/form',loadVerif:!Pa.fId,inputs:'fId='+Pa.fId,loade:cont,errWrap:cont, func:function(Jr){
			var xF=new JForm.Tpt({Jr:Jr,api:Api.xGe.b+'ckPop/form',
			docType:'CL',
			docClass:{wxn:'wrapx8',L:'Clase',aGo:'jsv.xGeCkPopCls',I:{lTag:'select',opts:$JsV.xGeCkPopCls}},
			func:function(Jr2){
			}},
			cont,{
				Prp:[]
			});
			xF.send();
		}});
	},
	ansNew:function(){
		var cont=$1.t('div');
		var sea1=JForm.Sea.one({},cont,{api:Api.xGe.b+'ckPop/forms/sea',func:function(L){
			if(L.fId){ $M.to('JForm.ans.xGenCkPop','fId:'+L.fId); $1.delet(wrapBk); }
		}});
		var divL=$1.T.divL({divLine:1,wxn:'wrapx1',L:'Preoperacional',Inode:sea1},cont);
		wrapBk=$1.Win.open(cont,{winSize:'medium',winTitle:'Selección'});
	},
	ans:function(){
		var Pa=$M.read();
		var cont=$M.Ht.cont;
		var vPost='fId='+Pa.fId;
		if(Pa.docEntry){ vPost +='&docEntry='+Pa.docEntry; }
		$Api.get({f:Api.xGe.b+'ckPop/ans',inputs:vPost,loade:cont,errWrap:cont, func:function(Jr){ Jr.fId=Pa.fId;
			$1.t('h5',{textNode:Jr.fCode+' - '+Jr.fName},cont);
			var nd=Itm.Sea.get({itemType:'AF',itemId:Jr.itemId,itemName:Jr.itemName,AJsBlank:'Y',
			AJsPut:[{k:'tt',v:'itmAf'},{a:'tr',k:'itemId'}]
			},cont,Jr);
			var xF= new JForm.Res({Jr:Jr,api:Api.xGe.b+'ckPop/ans',func:function(Jr2){
			},
			F:[
				{divLine:1,wxn:'wrapx8',L:'Fecha',I:{tag:'input',type:'date',name:'docDate',value:Jr.docDate}},
				{L:'Sede',wxn:'wrapx8',req:'Y',I:{tag:'select',name:'prp1',opts:$Tb.owsu,selected:Jr.prp1}},
				{wxn:'wrapx4',L:'Elemento Revisado',Inode:nd},
				{divLine:1,wxn:'wrapx1',L:'Detalles Generales',I:{tag:'textarea',name:'lineMemo',value:Jr.lineMemo}},
			]
			},
			cont);
			xF.send();
		}});
	},
	/*Docs*/
	OLgDocs:function(L){
		var Li=[];
		Li.push({ico:'fa fa-eye',textNode:' Visualizar', P:L, func:function(T){ $Doc.go('nomAil','v',T.P,1); } });
		if(L.canceled=='N'){
			Li.push({k:'edit',ico:'fa fa-pencil',textNode:' Modificar', P:L, func:function(T){ JForm.h('ans','xGenCkPop',T.P); } });
		}
		Li.push({k:'attach',ico:'fa fa-paperclip',textNode:' Archivos', P:L, func:function(T){ Attach.openWin({tt:'xGenCkPop',tr:T.P.docEntry, title:'Doc. Preoperacional # '+T.P.docEntry}) } });
		if(L.canceled=='N'){
			Li.push({k:'statusN',ico:'fa fa_prio_high',textNode:' Anular Documento', P:L, func:function(T){ $Doc.statusDefine({reqMemo:'N',docEntry:T.P.docEntry,api:Api.xGe.b+'ckPop/statusCancel',text:'Se va anular el documento.'}); } });
		}
		return $Opts.add('xGenCkPop',Li,L);;
	},
	optsDocs:function(P,pare){
		Li={Li:xGen.Ck.Pop.OLgDocs(P.L),PB:P.L,textNode:P.textNode};
		var mnu=$1.Menu.winLiRel(Li);
		if(pare){ pare.appendChild(mnu); }
		return mnu;
	},
	docs:function(){
		var cont=$M.Ht.cont;
		$Doc.tbList({api:Api.xGe.b+'ckPop/docs',inputs:$1.G.filter(),
		fOpts:xGen.Ck.Pop.optsDocs,docBy:'userDate',docUpd:'userDate',
		docTo:(L)=>{ return JForm.a('doc','xGenCkPop',L); },
		TD:[
			{H:'Estado',k:'docStatus',_V:'docStatus'},
			{H:'Código',k:'fCode'},
			{H:'Formato',k:'fName'},
			{H:'Elemento',k:'itemName'},
			{H:'',fTag:function(D){
				return Attach.btnWin({tt:'xGenCkPop',tr:D.docEntry, title:'Preoperacional # '+D.docEntry});
			}},
			{H:'Detalles',k:'lineMemo'}
		]
		},cont);
	},
	doc:function(){
		var cont=$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.xGe.b+'ckPop/doc',inputs:'docEntry='+Pa.docEntry,loade:cont,errWrap:cont, func:function(Jr){
			Jr.txt1='Preoperacionales';
			var dWrap=$Doc.topBtns({OLg:xGen.Ck.Pop.OLgDocs,D:Jr,print:'Y'},cont);
			$Doc.view(dWrap,{tbSerie:'N',D:Jr,
			THs:[
				{docEntry:1},{docTitle:Jr.fCode+' '+Jr.fName,cs:5,ln:1},{k:'txt1',cs:2,ln:1},
				{t:'Fecha',k:'docDate'},{middleInfo:'Y'},{logo:'Y'},
				{blank:'Y',cs:2}, {blank:'Y',cs:2},
				{t:'Articulo',k:'itemName',cs:7},
				{k:'lineMemo',cs:8,addB:$1.t('b',{textNode:'Detalles del evento:\u0020'}),HTML:1,Tag:{'class':'pre'}},
			],
			});
			var xF= new JForm.Doc({Jr:Jr},dWrap);
		}});
	},
	
	rep:function(){
		var cont=$M.Ht.cont; var Pa=$M.read();
		$Api.get({f:Api.xGe.b+'b1Cdp/rep',inputs:'fId='+Pa.fId+'&'+$1.G.filter(),loade:cont,func:function(Jr){
			R=JForm.repData(Jr,{Tbf:['Documento','Empleado','Fecha']});
			var tb=$1.T.table(R.Tbf);
			var tBody=$1.t('tbody',0,tb);
			for(var i in R.L){ L=R.L[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.docEntry},tr);
				$1.t('td',{textNode:L.cardName},tr);
				$1.t('td',{textNode:L.docDate},tr);
				for(var i in R.aL){
					val=(L.a[R.aL[i].aid])?L.a[R.aL[i].aid].aText:'';
					$1.t('td',{textNode:val},tr);
				}
				$1.T.tbExport(tb,{ext:'xlsx',print:'Y'},cont);
			}
		}});
	},
}

$oB.pus('ivtBitL', $Opts, [
	{ textNode: 'Etiqueta Lote',mPars: ['bId'], func:function(T){ 
		cont=$1.t('div',{style:'text-align:center; width:100%;'});
		var L=T.P;
		var xG=Ivt.Bit.Fx.batDays(L);
		$1.t('h3',{textNode:'Tarjeta de Marcación'},cont);
		$1.t('p',0,cont);
		var tb=$1.t('table',{style:'font-size:22px; margin:0 auto;','class':'table_sepa table_x100 td-center'},cont);
		var tBody=$1.t('tbody',0,tb);
		var tr=$1.t('tr',0,tBody);
		$1.t('th',{textNode:'Producto'},tr);
		$1.t('th',{textNode:'Código'},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('th',{textNode:L.itemName},tr);
		$1.t('th',{textNode:L.itemCode,style:'width:150px'},tr);
		var div=$1.t('div',{style:'font-size:24px'},cont);
		$1.t('span',{'class':'badge',textNode:'Lote\u0020\u0020\u0020\u0020\u0020'+L.bId},div);
		var d1=$1.t('div',{style:'display:table-cell; width:48%'},cont);
		var d2=$1.t('div',{style:'display:table-cell; width:48%'},cont);
		var tb=$1.t('table',{style:'font-size:22px; margin:0 auto;','class':'table_sepa td-center'},d1);
		var trH=$1.t('tr',0,$1.t('thead',0,tb));
		$1.t('th',0,trH);
		$1.t('th',{textNode:'Dia'},trH);
		$1.t('th',{textNode:'Mes'},trH);
		$1.t('th',{textNode:'Año'},trH);
		var tBody=$1.t('tbody',0,tb);
		var tr=$1.t('tr',0,tBody);
		$1.t('th',{textNode:'Fecha Producción'},tr);
		var x1=$2d.explode(L.manDate);
		$1.t('td',{textNode:x1.d},tr);
		$1.t('td',{textNode:x1.m},tr);
		$1.t('td',{textNode:x1.Y},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('th',{textNode:'Fecha Vencimiento'},tr);
		var x1=$2d.explode(L.dueDate);
		$1.t('td',{textNode:x1.d},tr);
		$1.t('td',{textNode:x1.m},tr);
		$1.t('td',{textNode:x1.Y},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('th',{textNode:'Fecha Bloqueo'},tr);
		var x1=$2d.explode(xG.vUtil);
		$1.t('td',{textNode:x1.d},tr);
		$1.t('td',{textNode:x1.m},tr);
		$1.t('td',{textNode:x1.Y},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('th',{textNode:'Centro Origen'},tr);
		$1.t('td',{textNode:L.lineMemo,colspan:3},tr);
		var tb=$1.t('table',{style:'font-size:22px; margin:0 auto;','class':'table_sepa table_x100 td-center'},d2);
		var tBody=$1.t('tbody',0,tb);
		var tr=$1.t('tr',0,tBody);
		$1.t('th',{textNode:'Estibas'},tr);
		$1.t('th',{textNode:'Total Cajas'},tr);
		var tr=$1.t('tr',0,tBody);
		var css1='height:50px;';
		$1.t('td',{textNode:'\u0020',style:css1},tr);
		$1.t('td',{textNode:'\u0020',style:css1},tr);
		var tb=$1.t('table',{style:'font-size:22px; margin:0 auto;','class':'table_sepa table_x100 td-center'},d2);
		var tBody=$1.t('tbody',0,tb);
		var tr=$1.t('tr',0,tBody);
		$1.t('th',{textNode:'Dimensiones del arrurme',colspan:3},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('th',{textNode:'Ancho'},tr);
		$1.t('th',{textNode:'Alto'},tr);
		$1.t('th',{textNode:'Largo'},tr);
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{textNode:'\u0020',style:css1},tr);
		$1.t('td',{textNode:'\u0020',style:css1},tr);
		$1.t('td',{textNode:'\u0020',style:css1},tr);
		$1.barCode({code:L.bId,height:40},cont);
		$1.Win.print(cont);
	} }
]);

$JsV._i({kObj:'xGeCkPopCls',mdl:'xge',liK:'xGeCkPopCls',liTxtG:'Clases de CheckList',liTxtF:'Clase CheckList (Form)'});