<?php
require('tbTop.php');
_ADMS::_lb('sql/filter');
$DBo=array(
'geo'=>array('sql_db'=>'geo_app_a'),
'alpaca'=>array('sql_db'=>'app_a'),
'bat'=>array('sql_db'=>'bat_app_a'),
);
$dbocard=($_GET['dbo']=='Y');
if($dbocard){
	$DBo=$DBo[$_GET['ocard']];
}
$ModR=array(
'F'=>array('cls'=>'iBg iBg_moneyPlus','t'=>'Financiero'),
'RF'=>array('cls'=>'iBg iBg_tagPurple','t'=>'Regimen Fiscal'),
'CT'=>array('cls'=>'iBg iBg_archivador','t'=>'Contabilidad'),
'EXT'=>array('cls'=>'iBg iBg_tbRelation','t'=>'Extends on Tables'),
'REL'=>array('cls'=>'iBg iBg_tbRelation','t'=>'Relación a Otra Tabla según Objeto'),
'tax'=>array('cls'=>'iBg iBg_warning','t'=>'Impuestos,Retenciones'),
'LG'=>array('cls'=>'iBg iBg_delivery','t'=>'Logística'),
'ACS'=>array('cls'=>'iBg iBg_candado','t'=>'Acceso Controlado'),
);

if(!$_GET['tbk']){
	$wh=$inner='';
	
	switch($_GET['orderBy']){
		case 'tbk': $ord='o.tbK ASC'; break;
		default : $ord='o.module1 ASC, o.tbType ASC, o.tbk ASC'; break;
	}
	if($_GET['ocard'] && $_GET['ocard']!='B'){
		$inner=' INNER JOIN tb2 o2 ON (o2.tbId=o.tbId) ';
		$wh='AND o2.ocard=\''.$_GET['ocard'].'\' ';
	}
	if($_GET['wh']['app']){ $_GET['wh']['tags(E_like3)(W_2)']=$_GET['wh']['app']; }
	$wh .= a_sql_filtByT($_GET['wh']);
	$q = a_sql::query('SELECT o.* FROM otb o'.$inner.' WHERE o.status !=\'obsoleto\' '.$wh.' ORDER BY '.$ord.' LIMIT 300',array(1=>'Error obteniendo listado de tablas: ',2=>'No se encontraron tablas.'));
	if(a_sql::$errNoText){ echo a_sql::$errNoText; }
	else{
		echo '<table class="table_zh" style="font-size:0.7rem;">
		<thead>
		<tr> <td>tbId</td><td>tbk</td> <td title="Módulo 1">M.1</td> <td title="Módulo 2">M.2<td>Nombre</td> <td>Descripción</td> <td>ocard</td>';
		if($dbocard){
			echo '<td><span class="fa fa_eye"> dbo</span></td>';
		}
		echo '</tr>
		</thead>
		<tbody>';
	while($L=$q->fetch_assoc()){
		$view=($L['type']=='view')?'<span class="fa fa_eye" title="Vista"></span>':'';
		echo '<tr>
		<td>'.$L['tbId'].'</td>
		<td>'.$view.'<a href="?tbk='.$L['tbk'].'">'.$L['tbk'].'</td>
		<td>'.$L['module1'].'</td>
		<td>'.$L['module2'].'</td>
		<td>'.$L['tbName'].'</td>
		<td>'.$L['description'].'</td>
		<td>'.$L['ocardfie'].'</td>';
		if($dbocard){
			$qs=a_sql::query('SELECT table_name FROM information_schema.tables WHERE table_schema=\''.$DBo['sql_db'].'\' AND table_name=\''.$L['tbk'].'\' LIMIT 1');
			$YN='No'; $css='';
			if(a_sql::$errNo==-1){ $YN='Si'; $css='background-color:#0F0;'; }
			echo '<td style="'.$css.'"><span class="fa fa_eye"> '.$YN.'</span></td>';
		}
		echo '</tr>';
	}
	echo '</tbody>
	</table>';
	}
}
else{
$tbk=$_GET['tbk'];
$qT = a_sql::fetch('SELECT * FROM otb WHERE tbk=\''.$tbk.'\' LIMIT 1',array(1=>'Error obteniendo tabla: ',2=>'No se encontró la tabla ('.$tbk.')'));
if(a_sql::$errNoText){ echo a_sql::$errNoText; }
else{
	echo '<h3>'.$qT['tbName'].'</h3>
	<div>ocard: '.$qT['ocard'].'.  <br/>
	<div><b>Descripción:</b> '.$qT['description'].'</div>
	</div>
	<br/>';
if($qT['type']=='view'){
	$qut = 'CREATE VIEW `'.$qT['tbk'].'` AS 
'.$qT['sqlt'].';';
	echo '<div class="textarea" style="max-height:100%;"><pre>'.$qut.'</pre></div>';
}
else{
$Mc=array();
$Addf =array(
'ocard'=>array('fiek'=>'ocardId','_'=>'Código ocard','type'=>'mediumint','len'=>6,'unsg'=>'Y','index'=>'FK'),
'osoc'=>array('fiek'=>'osocId','_'=>'Código de Sociedad','type'=>'mediumint','len'=>6,'unsg'=>'Y','index'=>'FK'),
'userId'=>array('fiek'=>'userId','_'=>'ID de usuario','type'=>'mediumint','len'=>6,'unsg'=>'Y'),
'userName'=>array('fiek'=>'userName','_'=>'Nombre de usuario','type'=>'varchar','len'=>50,'utf8'=>'Y'),
/* fechas */
'dateC'=>array('fiek'=>'dateC','_'=>'Fecha creación sistema','type'=>'timestamp','len'=>'','utf8'=>'N','def'=>'0000-00-00 00:00:00'),
'docDate'=>array('fiek'=>'docDate','_'=>'Fecha Documento','type'=>'date','def'=>'0000-00-00'),
'tt'=>array('fiek'=>'tt','_'=>'Target Type','type'=>'varchar','len'=>20,'utf8'=>'Y'),
'tr'=>array('fiek'=>'tr','_'=>'Target Ref','type'=>'varchar','len'=>20,'utf8'=>'Y'),
//usar BS.fiek 
'docStatus'=>array('fiek'=>'docStatus','_'=>'Estado del Documento','type'=>'enum','len'=>"'O','C','N'",'utf8'=>'Y','def'=>'O'),
'docClass'=>array('fiek'=>'docClass','_'=>'Clase del Documento','type'=>'varchar','len'=>20,'utf8'=>'Y','def'=>'B'),
'invMov'=>array('fiek'=>'invMov','_'=>'Estado de transferencia del documento','type'=>'enum','len'=>"'N','Y'",'utf8'=>'Y','def'=>'N'),
'lineMemo'=>array('fiek'=>'lineMemo','_'=>'Detalles del documento','type'=>'varchar','len'=>255,'utf8'=>'Y'),
'lineMemo_100'=>array('fiek'=>'lineMemo','_'=>'Detalles del documento','type'=>'varchar','len'=>100,'utf8'=>'Y'),
'cardId'=>array('fiek'=>'cardId','_'=>'ID del socio de negocios','type'=>'bigint','len'=>12,'unsg'=>'Y'),
'cardName'=>array('fiek'=>'cardName','_'=>'Nombre del socio de negocios','type'=>'varchar','len'=>100,'utf8'=>'Y'),
'id(12)'=>array('fiek'=>'id','_'=>'ID del sistema','type'=>'bigint','len'=>12,'unsg'=>'Y','index'=>'PKA'),
);
$nr=1;
$q = a_sql::query('SELECT * FROM tb1 WHERE tbk=\''.$tbk.'\' ORDER BY orden ASC LIMIT 300',array(1=>'Error obteniendo tabla: ',2=>'No se encontró la tabla ('.$tbk.')'));
if(a_sql::$errNoText){ echo a_sql::$errNoText; }
else{ $Mx=array();
$n=0;
	while($L=$q->fetch_assoc()){
		$descrip = $L['description'];
		$orden = $L['orden']; $fiekName=$L['fiekName'];
		if(preg_match('/^BS\.(.*)$/',$L['fiek'],$rw)){
			if($Addf[$rw[1]]){ $L=$Addf[$rw[1]]; }
			if($descrip==''){ $descrip=$L['_']; }
			$L['orden']=$orden;
		}
		else if($L['tbkRel']!=''){
		$Lq = a_sql::fetch('SELECT * FROM tb1 WHERE tbk=\''.$L['tbkRel'].'\' AND fiek=\''.$L['fiek'].'\' LIMIT 1');
		$Lq['_refTable'] = 'Y';
		unset($Lq['index'],$Lq['orden'],$Lq['def'],$Lq['moduleRel']);
		foreach($Lq as $_k => $v){ $L[$_k] = $v; }
		$L['orden']=$orden;
		if($descrip=='' && $Lq['description']!=''){ $descrip=$Lq['description']; }
		if($descrip==''){ $descrip=$Lq['fiek']; }
		unset($Lq);
		}
		if($fiekName!=''){ $L['fiek']=$fiekName; }
		$k=$L['fiek'].'_'.$nr; $nr++;
		
		unset($L['description']);
		if($n==0){ $n++;
		switch($qT['ocardfie']){
			case 'card': 
				$Mx['ocardId'] = $Addf['ocard'];
			break;
			case 'osoc':
			$Mx['ocardId'] = $Addf['ocard'];
			$Mx['osocId'] = $Addf['osoc'];
			break;
		}
		switch($qT['targetFie']){
			case 'tt': $Mx['tt'] = $Addf['tt']; $Mx['tr'] = $Addf['tr']; break;
		}
		switch($qT['userFie']){
			case 'Y': 
				$Mx['userId'] = $Addf['userId'];
				$Mx['userName'] = $Addf['userName'];
			break;
			case 'id':
				$Mx['userId'] = $Addf['userId'];
			break;
			case 'name':
				$Mx['userName'] = $Addf['userName'];
			break;
		}
		switch($qT['dateFie']){
			case 'docDateC':
				$Mx['dateC'] = $Addf['dateC'];
				$Mx['docDate'] = $Addf['docDate'];
			break;
			case 'docDate':
				$Mx['docDate'] = $Addf['docDate'];
			break;
			case 'dateC':
				$Mx['dateC'] = $Addf['dateC'];
			break;
			case 'docDate':
				$Mx['docDate'] = $Addf['docDate'];
			break;
		}
		}
		if(!array_key_exists($k,$Mx)){ $Mx[$k] = $L; }
		$Mx[$k]['_'] = $descrip;
	}
}
/* extends 2 
$q2 = a_sql::query('SELECT ocard, fiek, description FROM tb2 WHERE tbId=\''.$_GET['tbId'].'\' AND tbk=\''.$tbk.'\' LIMIT 300',array(1=>'Error obteniendo extensión de campos de '.$tbk.'.'.$k.': '));
if(a_sql::$errNoText){ echo a_sql::$errNoText; }
else if(a_sql::$errNo==-1){
	while($L2=$q2->fetch_assoc()){ $k=$L2['fiek']; $k2=$L2['ocard'];
		$Mx[$k][$k2] = $L2['description'];
		$Mc[$k2] = $k2;
	}
}
*/

?>
<table class="table_zh" style="font-size:0.7rem;">
<thead>
<tr>
	<td>o</td>
	<td>Campo</td>
	<td title="Campo generado automáticamente">Au</td>
	<td title="Relación a Módulo">Mod.</td>
	<td>Descripción Base</td> 
	<td>Tipo</td>
	<td></td>
	<?php foreach($Mc as $k2){ echo '<td>'.$k2.'</td>'; } ?>
</tr>
</thead>
<tbody>
<?php
$tbSql = 'CREATE TABLE IF NOT EXISTS `'.$qT['tbk'].'` (';
$footSql = '';
foreach($Mx as $k => $L){
	$auto=($L['auto']=='Y')?'<img src="auto.png" title="Campo generado automáticamente"/>':'';
	$cssNoBase = (!$L['_'])?'style="background-color:#CC0;"':'';
	$cssNoBase = ($L['index']=='PKA')?'style="background-color:#BB0;"':$cssNoBase;
	$type = strtoupper($L['type']);
	$typeLen=!(preg_match('/^(text|mediumtext)$/is',$type));
	$type .=' ';
	$type .= ($typeLen && $L['len']!='')? ' ('.$L['len'].')':'';
	$type .= ($L['unsg']=='Y')? ' UNSIGNED':'';
	$type .= ($L['unsg']=='00')? ' UNSIGNED ZEROFILL':'';
	$typeShow=$type;
	//$type .= ($L['utf8']=='Y')? ' CHARACTER SET utf8 COLLATE \'utf8mb4_general_ci\'':'';
	$type .= ($L['canNull']=='Y')?' NULL': ' NOT NULL';
	switch($L['def']){
		case 'CURRENT_TIMESTAMP' : $type .= ' DEFAULT CURRENT_TIMESTAMP'; break;
		default : $type .= ($L['def']!='')? ' DEFAULT \''.$L['def'].'\'':''; break;
	}
	$type .= ($L['index']=='PKA')? ' AUTO_INCREMENT':'';
	$fiek=$L['fiek'];
	$tdModR=$L['moduleRel'];
	if($rM=$ModR[$L['moduleRel']]){
		$tdModR='<span class="'.$rM['cls'].'" title="'.$rM['t'].'">'.$L['moduleRel'].'</span>';
	}
	$textK = ($L['_refTable'])?'<a style="color:#F00; font-weight:bold;" href="?tbk='.$L['tbk'].'">'.$fiek.'</a>':$fiek;
	$textK = ($L['moduleRel']=='EXT')?'<a style="color:#F00; font-weight:bold;" href="?tbk='.$fiek.'">'.$fiek.'</a>':$textK;
	echo '<tr>
	<td>'.$L['orden'].'</td>
	<td '.$cssNoBase.'><a name="'.$fiek.'">'.$textK.'</a></td>
	<td '.$cssNoBase.'>'.$auto.'</td>
	<td>'.$tdModR.'</td>
	<td '.$cssNoBase.'>'.$L['_'].'</td>
	<td>'.$typeShow.'</td>
	<td style="background-color:#CCC;"></td>';
	foreach($Mc as $k2){ echo '<td '.$cssNoBase.'>'.$L[$k2].'</td>'; }
	echo '</tr>';
	if($L['moduleRel']=='EXT'){ continue; }
	$tbSql .= '
 `'.$fiek.'` '.$type.',';
	if($L['index']=='PKA'){ $footSql .= '
 ,PRIMARY KEY (`'.$fiek.'`)'; }
	if($L['index']=='PK'){ $footSql .= '
 ,PRIMARY KEY (`'.$fiek.'`)
 ,KEY `'.$fiek.'` (`'.$fiek.'`)'; }
	if($L['index']=='I'){ $footSql .= '
 ,KEY `'.$fiek.'` (`'.$fiek.'`)'; }
}
$tbSql = substr($tbSql,0,-1).$footSql.'
) ENGINE=innoDB DEFAULT CHARSET=utf8mb4 COMMENT=\''.$qT['tbName'].'\'; ';
?>
<tr>
<td colspan="5"><pre><?php echo $tbSql; ?></pre></td>
</tr>
</tbody>
</table>
<br/>
<h5>Estructura Archivo Plano</h5>
<?php
if($qT['dataTemp']){
	$t=json_decode($qT['dataTemp'],1);
	foreach($t as $n => $L){
		$tr1 .='<td>'.$L['k'].'</td>';
		$tr2 .='<td>'.$L['d'].'</td>';
	}
	echo '<table class="table_zh">
	<tr>'.$tr1.'</tr>
	<tr>'.$tr2.'</tr>
	</table>';
}
?>
<h4>Funcionamiento en la creación</h4>
<?php echo $qT['explains']; ?>
<?php
}//si es type=tb
}//si existe la tabla
}
?>
</body>