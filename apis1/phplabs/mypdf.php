<?php
if($_GET['pdf']){
	_ADMS::_lb('lb/myPdf');
$pdf=new myPdf();
$pdf->D=array(
'head_h1'=>'Calendario Tributario Año 2019',
'head_h2'=>'DyD Consultores y Asesores SAS y DCP Revisores, le informa.',
'imgWater'=>'http://static1.admsistems.com/_logos/logodyd-opa.png'
);
$pdf->AliasNbPages();
$pdf->AddPage('L','Letter',0);
$pdf->SetFont('Arial','B',10);
$wiDis=round($pdf->wPage/6,2);
$pdf->SetFont('Arial','',7);

$R=array(
array('t'=>'Enero'),
array('t'=>'Febrero'),
array('t'=>'Marzo'),
array('t'=>'Abril'),
array('t'=>'Mayo'),
array('t'=>'Junio'),
);
$R2=array(
array('t'=>"(11) IVA - Cuatrimestral\n(11) Rte. Fuente"),
array('t'=>"(11) IVA - Cuatrimestral\n(11) Rte. Fuente"),
array('t'=>"(11) IVA - Cuatrimestral\n(11) Rte. Fuente"),
array('t'=>"(11) IVA - Cuatrimestral\n(11) Rte. Fuente\n(11) IVA - Cuatrimestral\n(11) Rte. Fuente(11) IVA - Cuatrimestral\n(11) Rte. Fuente\n(11) IVA - Cuatrimestral\n(11) Rte. Fuente(11) IVA - Cuatrimestral\n(11) Rte. Fuente\n(11) IVA - Cuatrimestral"),
array('t'=>"(11) IVA - Cuatrimestral\n(11) Rte. Fuente"),
array('t'=>"(11) IVA - Cuatrimestral\n(11) Rte. Fuente"),
);
$x2=$pdf->GetX();
$y2=$pdf->GetY();
$maxY=array();
foreach($R as $n => $L){
	$x1=$pdf->GetX(); $y1=$pdf->GetY();
	$pdf->cell($wiDis,5,$L['t'],1,0,'C','255,200,20');
}
$pdf->ln();
$wiDisB=$wiDis;
$nx=0; $y2=$pdf->GetY();
$nxM=3;
$x2=$pdf->GetX();
foreach($R2 as $nn => $L){
	if($nx==0){ $newX=$x2; }
	else{
		$newX=($wiDisB*$nx)+10;
	}
	$pdf->SetX($newX); $nx++;
	$pdf->MultiCell($wiDis,5,$L['t'],1,'L');
	$y3=$pdf->GetY();
	$maxY[$y3]=$y3;
	$pdf->SetY($y2);
	if($nx==$nxM){
		$pdf->SetY(max($maxY)+5);
		$y2=$pdf->GetY();
	}
}

$pdf->Output();
}
?>