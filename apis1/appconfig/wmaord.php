<?php
a_sql::$limitDef=20;
$taxType_I=array('rteiva','rtefte','rteica','rteother');
$taxType_R=array('IVA','ICO','otro');
_0s::$cFile['svrs']=array(
'L'=>'/_files/sinDefinir/',
'files1'=>'files1.admsistems.com/app/updFile',
'ocean1'=>'ocean1.admsistems.com/app/updFile'
);

$SOC_TbKeys = array(
'cnt_addr','cnt_phone','cnt_email',
'par_ocrd','par_ocpr','par_cpr1',
'itm_oitm','itm_oits','itm_src1','itm_oitp','itm_itp1',
'itm_log2',
'itm_owhs','itm_whs1',
'itm_bar1','itm_ogrs','itm_grs1','itm_grs2',
'itm_wtr1',

'itm_oitt','itm_itt1',
'itm_itc1','itm_oiac',
'itm_oitg','chr_oitm','chr_itm1',

'ivt_ocat','ivt_cat1','ivt_cat2','ivt_doc99',
'ivt_oing','ivt_ing1','ivt_ing2',
'ivt_oegr','ivt_egr1','ivt_egr2',
'ivt_owht','ivt_wht1', 'ivt_ocph','ivt_cph1',
'ivt_oinc','ivt_inc1',
'ivt_oawh','ivt_awh1',
'ivt_omov','ivt_mov1',

'par_oslp','par_slp1',
'gvt_opvt','gvt_pvt1','gvt_doc99','gvt_ocvt','gvt_cvt1',
'gvt_opor','gvt_por1',
'gvt_odlv','gvt_dlv1',  'gvt_ordn','gvt_rdn1',
'wma_owop',
'wma_owfa','wma_ompg','wma_mpg1','wma_mpg2', 
'wma_ocif','wma_cif1','wma_cif2',
'wma_oipc',

'gfi_otax',
'app_com1','app_fil1',
/* wma3 */
'wma3_opdp','wma3_pdp1','wma3_doc99',
'wma3_oodp','wma3_odp1','wma3_odp20',
'wma3_owpp',
'wma3_ocxf','wma3_cxf1','wma3_cxf2',
'wma3_dsp1',
/* pep */
'pep_oitw','pep_wtr1','pep_owht','pep_wht1','pep_oegr','pep_egr1','pep_oing','pep_ing1',
//Vistas
'_itm_its1','_itm_ogrs',
);
foreach($SOC_TbKeys as $k =>$v){ _0s::$Tb[$v]=$v; } unset($SOC_TbKeys);
_0s::$Tb['itm_oitw']='itm_whs1';
_0s::$Tb['wma3_odp20']='wma3_odp20';
_0s::$Tb['wma3_oddf']='wma3_oddf';
_0s::$Tb['wma3_ddf1']='wma3_ddf1';
_0s::$Tb['wma3_odcf']='wma3_odcf';
_0s::$Tb['wma3_dcf1']='wma3_dcf1';
_0s::$Tb['wma3_dcf2']='wma3_dcf2'; /* emision */

Doc::$sTy=array(
'odlv'=>array('otb'=>'gvt_odlv','tb1'=>'gvt_dlv1','tb99'=>'gvt_doc99','invMov'=>'Y'),
'opvt'=>array('otb'=>'gvt_opvt','tb1'=>'gvt_pvt1','tb99'=>'gvt_doc99'),
'ocvt'=>array('otb'=>'gvt_ocvt','tb1'=>'gvt_cvt1','tb99'=>'gvt_doc99'),
'gvtRdn'=>array('otb'=>'gvt_ordn','tb1'=>'gvt_rdn1','tb99'=>'gvt_doc99','addD'=>array('daysToClose'=>'Y')),
'gvtPdn'=>array('otb'=>'gvt_opdn','tb1'=>'gvt_pdn1','tb99'=>'gvt_doc99'),
'gvtPor'=>array('otb'=>'gvt_opor','tb1'=>'gvt_por1','tb99'=>'gvt_doc99'),
/*wma */
'wmaPdp'=>array('otb'=>'wma3_opdp','tb1'=>'wma3_pdp1','tb99'=>'wma3_doc99'),
'wmaOdp'=>array('otb'=>'wma3_oodp','tb1'=>'wma3_odp1','tb99'=>'wma3_doc99'),
'wmaDdf'=>array('otb'=>'wma3_oddf','tb1'=>'wma3_ddf1','tb99'=>'wma3_doc99'),
'wmaDcf'=>array('otb'=>'wma3_odcf','tb1'=>'wma3_ddc1','tb99'=>'wma3_doc99'),

'owht'=>array('otb'=>'ivt_owht','tb1'=>'ivt_wht1'),
'ocat'=>array('otb'=>'ivt_ocat','tb1'=>'ivt_cat1','tb99'=>'ivt_doc99'),
'ivtMov'=>array('otb'=>'ivt_omov','tb1'=>'ivt_mov1','tb99'=>'ivt_doc99'),
'reproDoc'=>array('otb'=>'zes_orep','tb1'=>'zes_rep1','tb99'=>'zes_rep99'),
'oinc'=>array('otb'=>'ivt_oinc','tb1'=>'ivt_inc1'),
);

$oCtrA = 'oCtr.handNum,oCtr.docDate,oCtr.dateC';
$oCtrBase = 'oCtr.handNum,oCtr.docDate,oCtr.cardId';
_0s::$TbF =array(
'itm_oitm_*'=>'I.itemId,I.itemCode,I.itemName,I.status,I.webStatus,I.itemType,I.itemGr,I.grsId,I.handInv,I.buyItem,I.sellItem,I.buyUdm,I.buyOper,I.buyFactor,I.buyPrice,I.sellUdm,I.sellOper,I.sellFactor,I.sellPrice,I.udm,I.prdItem',
'itm_oitm_get'=>'I.itemId,I.itemCode,I.itemName,I.status,I.webStatus,I.itemType,I.itemGr,I.udm',
'ivt_ocat_*'=>'ocat.docEntry,ocat.userId,ocat.userName,ocat.dateC,ocat.docDate,ocat.whsId,ocat.docStatus,ocat.invMov,ocat.lineMemo',
'ivt_oing_*'=>'oing.docEntry,oing.userId,oing.userName,oing.dateC,oing.docDate,oing.whsId,oing.docStatus,oing.invMov,oing.lineMemo,oing.docClass',
'ivt_ocph_*'=>'ocph.docEntry,ocph.docStatus,ocph.invMov,ocph.userId,ocph.userName,ocph.dateC,ocph.docDate,ocph.whsId,ocph.lineMemo',
'ivt_oegr_*'=>'oegr.docEntry,oegr.userId,oegr.userName,oegr.dateC,oegr.docDate,oegr.whsId,oegr.docStatus,oegr.invMov,oegr.lineMemo,oegr.docClass',
'ivt_owht_*'=>'owht.docEntry,owht.userId,owht.userName,owht.dateC,owht.docDate,owht.whsFrom,owht.whsTo,owht.docStatus,owht.lineMemo,owht.docClass',
);
?>