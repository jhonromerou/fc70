/*
$o: object
$0: Variables de Sistema
	$0s: Variables de sistema de socials
$1: DOM Object
	$1h: Contenidos HTML
$2: Ajax not user
	$2a: Aax
	$2io: Ajax not user
	$2d: Fechas
$3: DB search to relation: user, supplier
$js {length}
$4: URL
	$4m:Menu
$5: Apps
	$5n: Notificaciones
	$5c: Comentarios
	$5f: Archivos
$6 Logs/History
*/

ADMS_DB = {
	get:$ps_DB.get,
	response:$ps_DB.response
}

$r = {//ps_DB
get:function(P){
	$ps_DB.get(P);
}
}

var $Svg = {
folder:function(P){
	P=(P)?P:{};
	w=(P.w)?P.w:'1.5rem'; h=(P.h)?P.h:'1.5rem'; bg=(P.bg)?P.bg:'#8F8F8F';
	var span = $1.t('sgv',{x:'0px',y:'0px',height:h,width:w,focusable:false,viewBox:'0 0 24 24',fill:bg,style:'margin:0 0.25rem -0.375rem 0; '});
	if(P.attr){
		for(var a in P.attr){ span.setAttribute(a,P.attr[a]); }
	}
	if(P.drawType == 'sharing' || (P.privacity && P.privacity !='private')){
		span.innerHTML = '<path d="M20 6h-8l-2-2H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zm-5 3c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zm4 8h-8v-1c0-1.33 2.67-2 4-2s4 .67 4 2v1z" stroke="black" stroke-width="0.5"/>';
	}
	else{
		span.innerHTML = '<path d="M10 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2h-8l-2-2z" stroke="black" stroke-width="0.5" />';
	}
	return span;
	
	var path = $1.t('path',{fill:"orange",d:'M20 6h-8l-2-2H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zm-5 3c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zm4 8h-8v-1c0-1.33 2.67-2 4-2s4 .67 4 2v1z'});
	s.appendChild(path);
	return s;
}
}

var $0 = {
Batch:{ Types:{} },
Currency:{},
AuTypes:{
	BP:{}//Tipos de permismos para BussPartner: customerData, resp
}

}

$0.Load = {
	js:function(k){
		//k
		//<script type="text/javascript" id="Jscript_VS_AuTypes.BP" src="/s/vars/A:list?VS=AuTypes.BPsss"></script>
		var srC = '/s/vars/A:list?'+k;
		var srcAll = $1.q('.jsScriptLoads',null,'all');
		var exist = false;
		var len = srcAll.length;
		for(var i=0; i<len; i++){
			if(srcAll[i].src == srC){ exist = true; }
		}
		if(exist){ return false; }
		var scri = $1.t('script',{type:'text/javascript','class':'jsScriptLoads',src:srC});
		ps_DOM.body.appendChild(scri);
	}
	,
	t:0,
	jsV:function(k,D){//cargar script maximo 5 intento
		if(k != false){ $0.Load.js(k); }
		var veris = setInterval(function(){
			if($0.Load.t>5){ clearInterval(veris); alert('Se supero el tiempo de espera.'); }
			else if(eval(D.v)){ clearInterval(veris); D.func(); }
			else{ $0.Load.js(false,D); }
			$0.Load.t++;
		},1000);
	}
	
}


$o.userMembers = {//new ene 2018
put:function(P){
	P.vPost = 'objType='+P.objType+'&objRef='+P.objRef+'&';
	P.permsOpt = ($o.perms[P.objType])?$o.perms[P.objType]:$o.perms.def;
	var wrap = $1.t('div');
	var wrapTop = $1.t('div'); wrap.appendChild(wrapTop);
	var wuserId = $1.t('input',{type:'hidden','class':'searchChange_userId jsFields',name:'userId'});
	//buscar usuario, selecciona, guarda, y lee
	var divLine = $1.T.divLinewx({divLine:1, wxn:'wrapx2',L:{textNode:'Usuario'},I:{tag:'input',type:'text','class':'jsFields searchChange_userName',name:'userName',readonly:'readonly'}, addNodes:wuserId,
		btnLeft:{func:function(){ $S.user({}); }}
	});
	divLine.appendChild($1.T.divLinewx({wxn:'wrapx2',L:{textNode:'Permisos'},I:{tag:'select',sel:{'class':'jsFields',name:'perms'},opts:P.permsOpt} }));
	wrapTop.appendChild(divLine);
	var resp = $1.t('div'); wrap.appendChild(resp);
	var btn = $1.T.btnSend({value:'Guardar'},{
		f:'PUT '+$o.apir+'userMembers',loade:resp, getInputs:function(){ return P.vPost+'&'+$1.G.inputs(wrap)}, func:function(Jq){
			$ps_DB.response(resp,Jq);
			if(!Jq.errNo){
				$1.clearInps(wrapTop);
			$o.userMembers.get(P,wList); }
		}
	});
	wrap.appendChild(btn);
	var wList = $1.t('div'); wrap.appendChild(wList);
	$o.userMembers.get(P,wList);
	$1.Win.open(wrap,{winTitle:'Usuarios y Permisos ('+P.objType+')',onBody:1,winSize:'medium',winId:'winUserMembersObj'});
}
,
get:function(P,wList){
	var tO = P.permsOpt;
	$ps_DB.get({f:'GET '+$o.apir+'userMembers', loade:wList, inputs:P.vPost, func:function(Jq){
		var tb = $1.T.table(['Usuario','Permisos','']); wList.appendChild(tb);
		var tBody = $1.t('tbody'); tb.appendChild(tBody);
		for(var i in Jq.L){ var L = Jq.L[i];
			var tr = $1.t('tr'); tBody.appendChild(tr);
			tr.appendChild($1.t('td',L.userName));
			var per = (tO[L.perms])?tO[L.perms]:L.perms;
			tr.appendChild($1.t('td',per));
			var td = $1.t('td'); tr.appendChild(td);
			var btn = $1.T.btnSend({textNode:'', 'class':'iBg iBg_trash',title:'Eliminar Usuario',confirm:{text:'El usuario perderá los permisos asignados'},
			},{f:'DELETE '+$o.apir+'userMembers', inputs:P.vPost+'&userId='+L.userId, func:function(Jq2){
					if(Jq2){ if(!Jq2.errNo){ $1.delet(tr); } }
				}});
			td.appendChild(btn);
		}
	}});
}
}

$o.Members = {
api:'/s/v1/5o/A:Members',
formTb:function(P,wrapParent){
	P.userMember = (P.userMember) ? P.userMember : P.userId;
	var wrap = $1.t('div');
	wrap.appendChild($1.t('h6',{textNode:P.userName}));
	var Opts = {canModify:{t:'Modificar',ck:false},
		ocomment:{t:'Comentar',ck:true}, ofileUpd:{t:'Subir Archivos',ck:true},
		ocheckList:{t:'CheckList',ck:true}, owboList:{t:'Compartir en Lista',ck:true},
	};
	var tb = $1.T.table(['','']);
	var tBody = $1.t('tbody');
	for(var na in Opts){
		var tr = $1.t('tr');
		var td = $1.t('td');
		var isCk = (P[na] == 'Y') ? true : Opts[na].ck;
		isCk = (P[na] == 'N') ? false : isCk;
		var ck = $1.T.ckLabel({'class':'jsFields',name:na,YN:true, checked:isCk});
		td.appendChild(ck); tr.appendChild(td);
		var td = $1.t('td',{textNode:Opts[na].t});
		tr.appendChild(td);
		tBody.appendChild(tr);
	}
	tb.appendChild(tBody);
	wrap.appendChild(tb);
	var resp =  $1.t('div');
	var btnSend = $1.T.btnSend({value:'Definir',func:function(){
		var vPost = 'objType='+P.objType+'&objRef='+P.objRef+'&userMember='+P.userMember+'&'+$1.G.inputs(tb);
		$ps_DB.get({file:'PUT '+$o.Members.api,inputs:vPost,func:function(Jq){
			if(!Jq.erroNo){ $1.delet(wrapBk); P.reload = true; $o.Members.get(P) }
		}});
	}});
	wrap.appendChild(resp);
	wrap.appendChild(btnSend);
	var wrapBk = $1.Win.open(wrap,{winTitle:'Permisos',winId:'addMembersonObject'});
	wrapParent.appendChild(wrapBk);
}
,
put:function(O,wrapParent){
	$3.user({func:function(Jsr){
		$js.push(Jsr,O);
		$o.Members.formTb(Jsr,wrapParent);
	}});
}
,
get:function(P,noLoad){
	var wrap = $1.t('div',{'class':'winMenuInLine winMembers',style:'display:none;'});
	if(noLoad){ return wrap; }
	var wrap = $1.q('.winMembers');
	var wrapExist = $1.q('.winMembers_open');
	if(!P.reload && wrapExist && wrapExist.tagName){ return true; }
	wrap.classList.add('winMembers_open');
	var vPost = 'objType='+P.objType+'&objRef='+P.objRef;
	var wrapList = $1.t('div'); $1.clear(wrap);
	wrap.appendChild($1.t('h4',{textNode:'Miembros','class':'head1'}));
	var add = $1.t('input',{type:'button','class':'btnAddText iBg_people.png',value:'Añadir Miembro a la Tarjeta'});
	add.onclick = function(){ delete(P.userMember); delete(P.userId); $o.Members.put(P,wrap); }//objType
	wrap.appendChild(add);
	$ps_DB.get({file:'GET '+$o.Members.api,inputs:vPost, loade:wrapList,
	func:function(Jq){
		if(Jq.errNo){ $ps_DB.response(wrapList,Jq); }
		else{
			var tb = $1.T.table(['',{textNode:'Usuario'},
			{'class':'iBg_ iBg_edit',title:'Modificar'},
			{'class':'iBg_ obTy_comment',title:'Comentar'},
			{'class':'iBg_ obTy_fileUpd',title:'Subir Archivos'},
			{'class':'iBg_ obTy_checklist',title:'CheckList'},
			{'class':'iBg_ obTy_workBoard',title:'Añadir a Lista de Tablero'}
			]);
			var tBody = $1.t('tbody');
			for(var i in Jq.DATA){ var L = Jq.DATA[i];
				var tr = $1.t('tr');
				var td = $1.t('td');
				var edit = $1.t('input',{type:'button','class':'iBg iBg_edit'});
				edit.P = L;
				edit.onclick = function(){ $o.Members.formTb(this.P,wrap); }
				td.appendChild(edit); tr.appendChild(td);
				tr.appendChild($1.t('td',{textNode:L.userName}));
				$1.textIs = {is:'Y',then:'Si',end:'\u00A0\u00A0\u00A0'};
				tr.appendChild($1.t('td',{textNode:L.canModify}));
				tr.appendChild($1.t('td',{textNode:L.ocomment}));
				tr.appendChild($1.t('td',{textNode:L.ofileUpd}));
				tr.appendChild($1.t('td',{textNode:L.ocheckList}));
				tr.appendChild($1.t('td',{textNode:L.owboList}));
				$1.textIs = {};
				var td = $1.t('td');
				var edit = $1.t('input',{type:'button','class':'iBg iBg_trash'});
				edit.P = L;
				edit.onclick = function(){ $o.Members.delete(this.P,wrap); }
				td.appendChild(edit); tr.appendChild(td);
				tBody.appendChild(tr);
			}
			tb.appendChild(tBody);
			wrapList.appendChild(tb);
		}
	}});
	wrap.appendChild(wrapList);
}
,
delete:function(P,wrapParent){
	var vPost = 'objType='+P.objType+'&objRef='+P.objRef+'&userMember='+P.userMember+'&delete=Y';
	$ps_DB.get({file:'PUT /s/v1/5o/A:Members',inputs:vPost,
	func:function(Jq){
		if(!Jq.errNo){ P.reload = true; $o.Members.get(P) }
	}});
}
}

$o.Ht = {
$5f:function(P,P2){P2=(P2)?P2:{};
	var win = (P2.winC)?P2.winC:'win5f';
	var winIn = $1.t('div',{'class':'winMenuInLine '+win});
	if(P2.getList=='N'){ P.getList = 'N';
		winIn.style.display ='none';
		$5f.open(P,winIn);
	}
	else{
		var winIn = $1.q('.'+win,P2.pare);
		var p=(winIn && winIn.getAttribute('getList') == 'Y')?{getList:'N'}:{};
		$5f.get(p,winIn);
		winIn.setAttribute('getList','Y');
		return true;
	}
	return winIn;
}
,
$5c:function(P,P2){P2=(P2)?P2:{};
	var win = (P2.winC)?P2.winC:'win5c';
	var winIn = $1.t('div',{'class':'winMenuInLine '+win});
	if(P2.getList=='N'){ P.getList = 'N';
		winIn.style.display ='none';
		$5c.open(P,winIn);
	}
	else{
		var winIn = $1.q('.'+win,P2.pare);
		var p=(winIn && winIn.getAttribute('getList') == 'Y')?{getList:'N'}:{};
		$5c.get(p,winIn);
		winIn.setAttribute('getList','Y');
		return true;
	}
	return winIn;
}
}

var $S = {
user:function(P,trPare){
	var A = (P.A) ? P.A : 'ocard';
	if(P.Fie){ var Fie = P.Fie; }
	else{ var Fie = {'userId':{visible:'N'},'user':{text:'Usuario'},'userName':{text:'Nombre'},'socName':{text:'Sociedad'}};
	}
	if(P.addFie){ pushO(Fie,P.addFie); }
	$ps_DB.Search.tb({A:'usr.'+A, F:Fie, func:P.func, fields:'U.userId,U.user,U.userName,ocard1.socName'},trPare);
}
}

var $1h = {//HTML
userAssg:function(D,P){var P=(P)?P:{};
	var wxn = (P.wxn) ? P.wxn : 'wrapx1';
	var userAssg = $1.t('div',{'class':'wrapDivLineSingle'});
	var addNodes = $1.t('input',{type:'hidden','class':'jsFields jsFiltVars searchChange_userId',name:'userAssg',value:D.userAssg});
	isDivL = (P.divLine==false)? false : true;
	var L = (P.L) ? P.L : false;
	userAssg.appendChild($1.T.divLinewx({divLine:isDivL,wxn:wxn,L:L, I:{tag:'input',type:'text','class':'jsFields jsFiltVars searchChange_userName',name:'userAssgName',readonly:'readonly',value:D.userAssgName,placeholder:'Usuario'},
		btnLeft:{func:function(){ $3.user({},userAssg); } },
		addNodes:addNodes
	}));
	return userAssg;
}
,
userAssg_inline:function(D,P){var P=(P)?P:{};
	var userAssg = $1.t('div',{'class':'boxiO',style:''});
	var sea = $1.t('button',{'class':'boxiO_open fa faBtn fa_search'});
	var close = $1.t('button',{'class':'boxiO_clear btn iBg_closeSmall'});
	close.onclick = function(){ if(P.func){ P.func({userId:'',userName:''}); } $1.clearInps(userAssg); }
	var userId = $1.t('input',{type:'hidden','class':'jsFields jsFiltVars searchChange_userId',name:'userAssg',value:D.userAssg});
	var userName = $1.t('input',{type:'hidden','class':'jsFields jsFiltVars searchChange_userId',name:'userAssg',value:D.userAssg});
	var text = $1.t('input',{type:'text','class':'boxiO_text clearInnerText searchChange_userName cpointer',value:D.userAssgName+'\u00A0'});
	sea.onclick = text.onclick = function(){ $S.user(P,userAssg) }
	userAssg.appendChild($1.t('div',{'class':'boxiO_label',textNode:'Responsable'}));
	userAssg.appendChild(sea); userAssg.appendChild(close);
	userAssg.appendChild(userId); userAssg.appendChild(userName);
	userAssg.appendChild(text);
	return userAssg;
}
,
card:function(D,P){ var P=(P)?P:{};
	var name1 = (P._i1) ? P._i1 : 'cardId';
	var name2 = (P._i2) ? P._i2 : 'cardName';
	var wxn = (P.wxn) ? P.wxn : 'wrapx1';
	var addNodes = $1.t('input',{type:'hidden','class':'jsFields jsFiltVars searchChange_cardId',name:name1,value:D.cardId});
	var I = {tag:'input',type:'text','class':'jsFields jsFiltVars searchChange_cardName',name:name2,readonly:'readonly',placeholder:'Clic para buscar cliente...',value:D.cardName};
	if(P.readonly == 'N'){ delete(I.readonly); }
	var divLine = $1.T.divLinewx({wxn:wxn, Label:{textNode:'Cliente'}, I:I,
	btnLeft:{func:function(){ $3.crd({A:'C', add:{Fie:'cardNameComer'} },P.wrap); } },addNodes:addNodes});
	return divLine;
}
,
wboList:function(D,P){var P=(P)?P:{};
	var wxn = (P.wxn) ? P.wxn : 'wrapx1';
	var userAssg = $1.t('div',{'class':'wrapDivLineSingle'});
	var addNodes = $1.t('input',{type:'hidden','class':'jsFields searchChange_wboListId',name:'wboListId',value:D.wboListId});
	userAssg.appendChild($1.T.divLinewx({divLine:true,wxn:wxn, I:{tag:'input',type:'text','class':'jsFields searchChange_listName',name:'listName',readonly:'readonly',value:D.listName},
		btnLeft:{func:function(){
			$3.wboList({A:'list', func:function(Jq2){}},userAssg);
		}},
		addNodes:addNodes
	}));
	return userAssg;
}
,
}

var $1f = {
O:{},//usar s0/filters?k=wbo1-10 k=wbo1-10
V:{
E_:{
	BLANK:{k:"BLANK", v:"En Blanco"},
	"in":{k:"in", v:"Alguno de estos",explain:'Utilize los valores separados por coma (,)'},
	noIn:{k:"noIn", v:"Ninguno de estos",explain:'Utilize los valores separados por coma (,)'},
	igual:{k:"igual", v:"Igual a"},
	menIgual:{k:"menIgual", v:"Menor o Igual que"},
	mayIgual:{k:"mayIgual", v:"Mayor o Igual que"},
	noIgual:{k:"noIgual", v:"Diferente / Distinto de"},
	like1:{k:"like1", v:"...Finaliza Con"},
	like2:{k:"like2", v:"Inicia Con..."},
	like3:{k:"like3", v:"...Contiene..."},
	notLike1:{k:"notLike1", v:"...No Finaliza Con"},
	notLike2:{k:"notLike2", v:"No Inicia Con..."},
	notLike3:{k:"notLike3", v:"...No Contiene..."},
}
}
,
base:function(P,pare){
	if($M.wrap){ cont = $M.wrap; }
	else{ var cont = $1.q('#_1f_wrap'); }
	var ph = (P.placeholder)?P.placeholder:'Buscar...';
	var div = $1.t('div');
	var divAdd = $1.t('div',{'class':'filter_advanced',style:'display:nones;'});
	var se = $1.t('input',{type:'text','class':'jsFiltVars iBg iBg_search2 divLine',placeholder:ph,name:'_1f_text'});
	var upd = $1.t('button',{'class':'_1f_btnUpd iBg iBg_update'});
	upd.onclick = function(){ if(P.func){ P.func(); } }
	var more = $1.t('button',{'class':'fa faBtn fa_plusSquare',textNode:' Añadir Filtro',title:'Filtro Avanzado'});
	P.wrap = divAdd;
	more.onclick = function(){ $1f.key(P.fk,P); }
	div.appendChild(se); div.appendChild(upd); div.appendChild(more);
	div.appendChild(divAdd);
	if(cont.childNodes.length>0){ }
	else{ $1.clear(cont); cont.appendChild(div); }
}
,
key:function(k,P){
	var wrap = P.wrap;
	$ps_DB.get({f:'_GET /sys/filters',inputs:'k='+k,func:function(D){
		var span = $1.t('span',{'class':'_1f_replace'});
		var li = $1.t('div',{'class':'inpLine'});
		var y = $1.T.sel({sel:{'class':'_1f__w',style:'width:3rem;'},opts:{1:'Y',2:'O'},noBlank:true});
		var fi = $1.T.sel({sel:{'class':'_1f__field'},opts:D.F, selected:D.k1,noBlank:true});
		var eO = {};
		var o = D.F[D.k1];
		for(var i in o.equals){ var te = $1f.V.E_[o.equals[i]];
			eO[i] = {k:te.k, v:te.v};
		}
		var iE = $1.T.sel({sel:{'class':'_1f__e'},opts:eO,noBlank:true});
		fi.o = y.o = iE.o = o;
		fi.onchange = function(){
			draw(D.F[this.value],this.parentNode);
			defName(this.parentNode,D.F[this.value]);
		}
		y.onchange = iE.onchange = function(){ defName(this.parentNode); }
		function defName(li,o){
			var f = $1.q('._1f__field',li).value;
			var w_ = '(W_'+$1.q('._1f__w',li).value+')';
			var e_ = '(E_'+$1.q('._1f__e',li).value+')';
			var name = (o && o.fieldName) ? o.fieldName : 'FIE[]['+f+w_+e_+']';
			var sendVal = $1.q('._1f__value',li);
			sendVal.setAttribute('name',name);
		}
		function draw(o,li){
			var span = $1.t('span',{'class':'_1f_replace'});
			var w_ = '(W_1)';
			var e_ = '(E_'+o.equals[0]+')';
			var name = (o.fieldName) ? o.fieldName : 'FIE[]['+o.k+w_+e_+']';
			switch(o.inputType){
				case 'date' : var it = $1.t('input',{type:'date','class':'jsFiltVars _1f__value',name:name,value:''}); break;
				case 'select' : var it = $1.T.sel({sel:{'class':'jsFiltVars _1f__value',name:name},opts:eval(o.opts),noBlank:true}); break;
				default : var it = $1.t('input',{type:'text','class':'jsFiltVars _1f__value',name:name,placeholder:'Texto a buscar...'});
				break;
			}
			span.appendChild(it);
			var spanOld = $1.q('._1f_replace',li);
			li.replaceChild(span,spanOld);
			var eO = {};
			for(var i in o.equals){ var te = $1f.V.E_[o.equals[i]];
				eO[i] = {k:te.k, v:te.v};
			}
			var iE = $1.T.sel({sel:{'class':'_1f__e'},opts:eO,noBlank:true});
			iE.onchange = function(){ defName(this.parentNode); }
			var spanOld = $1.q('._1f__e',li);
			li.replaceChild(iE,spanOld);
		}
		li.appendChild(y); li.appendChild(fi); li.appendChild(iE); li.appendChild(span);
		var clo = $1.t('button',{'class':'iBg iBg_closeSmall'});
		clo.onclick = function(){ 
			$1.delet(this.parentNode);
			if(P.func){ P.func(); }
		}
		li.appendChild(clo);
		draw(D.F[D.k1],li);
		wrap.appendChild(li);
	}
	});
}
,
_5a:{}
}

var $2a = {
then:false,
addGet:'',
doc:function(){
	var req = false;
	try { req = new XMLHttpRequest(); }
	catch(err1){
		try{ req = new ActiveXObject("Msxml2.XMLHTTP"); }
		catch(err2){
			try{ req = new ActiveXObject("Microsoft.XMLHTTP"); }
			catch(err3) { req = false; }
		}
	}
	return req;
}
,
get:function(PARS){
	http = $2a.doc();
	var fr = PARS.f;
	PARS.method = fr.replace(/^\_?(POST|PUT|GET|DELETE).*/,"$1");
	PARS.addGet = (PARS.addGet) ? PARS.addGet : '';
	PARS.addGet = ($2a.addGet != '') ? $2a.addGet : PARS.addGet;
	PARS.f = fr.replace(/^\_?(POST|PUT|GET|DELETE) ?/,"");
	var si = '';
	var is_Get = fr.match(/^\_(GET|POST|PUT|DELETE)/);
	if(is_Get || PARS.method == 'GET'){ si = '?'+PARS.inputs; }
	si += (PARS.addGet != '')?si+'&'+PARS.addGet : '';
	PARS.file = (PARS.f.match(/^\//)) ? PARS.f : '/'+PARS.f+'--'; 
	var formParms = (PARS.inputs) ? PARS.inputs : '';
	PARS.file = (is_Get || PARS.method == 'GET') ? PARS.file+'?'+formParms : PARS.file+'?';
	$1.delet('psAjaxError404');
	console.log(fr+si+' ('+PARS.method+') (2a)');
	http.open(PARS.method,PARS.file+'&'+PARS.addGet,true);
	if(PARS.file){
		if(PARS.loaderFull){ $ps_DB.Loader.full(); }
		if(PARS.loade){
			var loadw = $1.t('div',{'id':'psLoadAjaxPoints','class':'psLoadAjaxPoints'});
			loadw.appendChild($1.t('img',{'id':'psLoadAjaxPoints','src':ADMS.urlTop+'/_img/loaderwindows.svg', alt:' Cargando...', title:' Cargando'}));
			loadw.appendChild($1.t('i',{textNode:' Cargando...'}));
			$1.clear(PARS.loade);
			if((PARS.loade).tagName){ (PARS.loade).appendChild(loadw); }
		}
		http.onreadystatechange = function (){
		if(http.readyState === 4 && http.status === 200){
			if(PARS.funcErr){ PARS.funcErr(JS); $1.delet('psLoadAjaxPoints'); }
			$1.delet('psLoadAjaxPoints');
			$1.delet('admsDBLoaderFull');
			$1.clear(PARS.loade);  
			if(http.responseText == ''){
			var dat = $1.t('div');
				dat.appendChild($1.t('p',{'textNode':'No se obtuvieron resultados.'}));
				dat.appendChild($1.t('li',{'textNode':'FILE: '+PARS.file}));
				dat.appendChild($1.t('li',{'textNode':'ApiMethod: '+PARS.ApiMethod}));
				dat.appendChild($1.t('li',{'textNode':'Method: '+PARS.method}));
				dat.appendChild($1.t('li',{'textNode':'vPost: '+PARS.inputs}));
				$1.Win.message({'title':'Error de Respuesta','text':dat});
				return false;
			}
			var resp = http.responseText;
			if(resp.match(/^RESPONSETEXT/i)){
				resp = resp.replace(/RESPONSETEXT/i,'');
				var pr = $1.t('div',{'class':'ajusTextAll',textNode:resp});
				$1.Win.message({text:pr});
				return false;
			}
			else if(resp.match(/^ALERTJSON/i)){
				resp = resp.replace(/ALERTJSON/i,'');
				var pr = $1.t('div',{'class':'ajusTextAll',textNode:resp});
				$1.Win.message({text:pr})
			}
			var fatalErr = resp.match(/\<b\>(Fatal error)\<\/b\>\:(.*)/i);
			fatalErr = (fatalErr == null) ? resp.match(/\<b\>(Warning)\<\/b\>\:(.*)/i) : fatalErr;
			fatalErr = (fatalErr == null) ? resp.match(/\<b\>(Parse error)\<\/b\>\:(.*)/i) : fatalErr;
			if(fatalErr != null){
				$1.delet('psAjaxError500');
				var divnofound = $1.t("div",{'id':'psAjaxError500', style:'position:fixed; top:0; left:0; width:100%; height:100%; backgroundColor:rgba(255,0,0,0.7); color:#FFF; z-index:1000;'});
				var nofound = $1.t("div",{style:'position:absolute; margin:0 auto; height:100px; widht:100%; padding:30px 10px;'});
				nofound.appendChild($1.t('h1',{'textNode':fatalErr[1]}));
				nofound.appendChild($1.t('p',{'textNode':'Error en Servidor en el archivo:'+PARS.file }));
				nofound.appendChild($1.t('p',{'textNode':fatalErr[2]}));
				var reloaded = $1.t('a',{href:document.location, 'textNode':'Actualizar'});
				reloaded.onclick = document.location.reload;
				nofound.appendChild(reloaded);
				var iClose = $1.t('input',{type:'button',value:'Cerrar Ventana',style:'display:block; margin:6px 0;'});
				iClose.onclick = function(){ ps_DOM.delet('psAjaxError500'); }
				nofound.appendChild(iClose);
				divnofound.appendChild(nofound);
				document.body.appendChild(divnofound);
				iClose.focus(); iClose.blur();
			}
			else{
				try{//
					//var JS = JSON.parse(decodeURI(resp));
					var JS = JSON.parse(resp);
					var errNo = (JS.errNo) ? JS.errNo : '';
					//1-sql, 2-no rows, 3:execution server, 4:auth, 5:exuti
					var JS0 = (JSON.respChild) ? JSON.respChild[0] : {}
					if(JS.errNo || JS0.errNo){ JS.ajaxFile = PARS.file; }
					if(JS0.errNo ==1){
						title = (JS0.title) ? JS0.title : 'Error 500 DB';
						$1.Win.message({title:title, text:JS0.text });
					}
					else if(JS.error_auth == true){ $1.Win.message(JS); }
					else if(JS.errNo == 1 || JS.errNo == 4 || JS.errNo == 5){
						$1.Win.message(JS);
					}
					else{
						$5n.room.a(JS);
						if(PARS.func){
							PARS.func(JS);
							if($ps_DB.then != false){ $ps_DB.then(); $ps_DB.then = false; }
						}
						else if(PARS.VAR){ PARS.VAR = JS; }
						if(!PARS.func && $ps_DB.then != false){ $ps_DB.then(); $ps_DB.then = false; }
					}
				}
				catch(e){//
					//e = 0;
					var cot = $1.t('p');
					cot.appendChild($1.t('li',{textNode:'Nombre Error: '+e.name}));
					cot.appendChild($1.t('li',{textNode:'At: '+e.at}));
					cot.appendChild($1.t('li',{textNode:'on: '+PARS.file}));
					cot.appendChild($1.t('p',{textNode:'Text: '+e.stack}));
					if(typeof(JS) != 'object'){
						cot.appendChild($1.t('pre',{textNode:resp,style:'background:#F00; color:#FFF;'}));
					}
					ps_DOM.Tag.Win.bkFixed(cot,{onBody:true,winTitle:'Error de Ejecución',zIndex:1000});
					console.log(e);
				}
			}
		}
		else if(http.readyState !== 4){ /* errore */}
		else if(http.status === 404){
			var divnofound = $1.t("div",{'id':'psAjaxError404', style:'position:fixed; top:0; z-index:1000; left:0; width:100%; height:100%; backgroundColor:rgba(255,0,0,0.8); color:#FFF;'});
			var nofound = $1.t("div",{style:'position:absolute; margin:0 auto; height:100px; widht:100%; padding:30px 10px;'});
			nofound.appendChild($1.t('h1',{'textNode':'Error 404'}));
			nofound.appendChild($1.t('p',{'textNode':'No se ha encontrado el archivo:'+PARS.file}));
			var reloaded = $1.t('a',{href:document.location, 'textNode':'Intenta Actualizar o Comunicate con el Supersu.'});
			reloaded.onclick = document.location.reload;
			nofound.appendChild(reloaded);
			var iClose = $1.t('input',{type:'button',value:'Cerrar Ventana',style:'display:block; margin:6px 0;'});
				iClose.onclick = function(){ ps_DOM.delet('psAjaxError404'); }
			nofound.appendChild(iClose);
			divnofound.appendChild(nofound);
			document.body.appendChild(divnofound);
		}
		else if(http.status === 500){
			var divnofound = $1.t("div",{'id':'psAjaxError500', style:'position:fixed; top:0; left:0; width:100%; height:100%; backgroundColor:rgba(255,0,0,0.7); color:#FFF;'});
			var nofound = $1.t("div",{style:'position:absolute; margin:0 auto; height:100px; widht:100%; padding:30px 10px;'});
			nofound.appendChild($1.t('h1',{'textNode':'Error 500'}));
			nofound.appendChild($1.t('p',{'textNode':'Error en Servidor en el archivo:'+PARS.file}));
			var reloaded = $1.t('a',{href:document.location, 'textNode':'Intenta Actualizar o Comunicate con el Supersu.'});
			reloaded.onclick = document.location.reload;
			nofound.appendChild(reloaded);
			divnofound.appendChild(nofound);
			document.body.appendChild(divnofound);
		}
		}
		http.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		var noCache = "alet="+parseInt(Math.random()*999999999999999);
		p_ost = (formParms+"&"+noCache);
		http.send(p_ost); 
	}
	$2a.addGet = '';
}
}


$2d.Draw = {
	Wd:function(P){ P=(P)?P:{};
	var Days = $2d.Vs.Days;
	if(!P.date1){
		var ra = $2d.goRang({rang:P.rang});
		P.date1 = ra.date1;
		P.date2 = ra.date2;
	}
	var mI = $2d.g('n',P.date1);
	var date1 = $2d.weekB(P.date1);
	var date2 = $2d.weekE(P.date2); 
	var l = $2d.diff({date1:date1, date2:date2});//13=1semana
	var lastWY = ''; var lastTk = '';
	var tdWeek = {};
	var Wd = {};
	var befW = 1;
	for(var d=0; d<l+1; d++){
		var nd = $2d.add(date1,d+'days');
		var wy = $2d.g('W',nd);
		var y = nd.substring(0,4);
		var tk = y+'_'+wy;//wy
		if(l==13 && lastTk!='' && lastTk!=tk){ break; }
		var wd = $2d.g('w',nd);
		var z = $2d.g('z',nd);
		var n = $2d.g('n',nd);
		//if((wy== 1 && befW>1) || wy == 0){ wy = 53; }
		befW =  wy; lastTk = tk;
		if(!Wd[tk]){ Wd[tk] = {wy:wy}; }
		var yzId = y+'_'+z;//id para calc unico año_dia
		Wd[tk][wd] = {date:nd,z:z,n:n, wd:wd,y:y,yzId:yzId}; 
	}
	return Wd;
}
}


var socket;
var $2io = {
e:{
	error:false, recNum:0,
}
,
ini:function(hostNode,query){
	socket = io.connect(hostNode,query);
	socket.on('connect', function(e){ $2io.e.error = false;
		$2io.recNum = 0; });
	socket.on('connect_error', function(e){ $2io.e.error = e });
	socket.on('reconnecting', function(e){
		$2io.recNum = e;
		if(e>=10){
			ps_DOM.Win.errFixed({title:'Error 500 - Server 2',text:'Parece que el servidor 2 no está respondiendo , informe al área responsable o intenta actualizar la página.'});
		}
	});
}
,
emit:function(ioName,data){
	var ide = 'admsServer2Error_emit';
	ps_DOM.delet(ide);
	if($2io.e.error != false){
		ps_DOM.Win.errFixed({id:ide,title:'Error 500 - Server 2',text:'Error conectando al servidor. No se enviaron los datos.'});
	}
	else{
		$2io.e.error = false;
		$2io.recNum = 0;
		socket.emit(ioName,data);
	}
}
,
on:function(ioName,func,P){ var P = (P) ? P : {};
	socket.off(ioName);
	socket.on(ioName,function(Dres){
	if(P.loade && Dres.errNo  && Dres.errNo != 1){ $ps_DB.response(P.loade,Dres); }
	else if(Dres.errNo ==1){
		Dres.title = (Dres.title) ? Dres.title : 'Error 500 DB';
		ps_DOM.Tag.Win.message(Dres);
	}
	else{ func(Dres); }
	});
}
,
rue:function(data){//roomUserEmit
	$2io.emit('userRoom_req',data);
}
,
worksp:{
open:false,
emit:function(P){
	var rid = $0s.ocardSocId+'_'+($0s.userId*1);
	P.rid = rid;
	if(!$2io.worksp.open){ $2io.worksp.on(); $2io.worksp.open = true; }
	if(P.ini){ $2io.emit('globalRoom_obj',P); }
	else{
		var o = P.objType; var t = P.targetType;
		if((o == 'comment' && t=='activity') || 
		(o == 'fileUpd' && t=='activity') ||
		(o == 'checkList' && t=='activity') ||
		(o == 'activity' && t=='workBoard' && P.movType == 'addKard') ||
		o == 'activity'
		){
			//objType, objRef, act, from, to
			$2io.emit('globalRoom_obj',P);
		}
		else if(o == 'callFunc'){ $2io.emit('globalRoom_obj',P); }
	}
}
,
on:function(func){
	$2io.on('globalRoom_obj',function(Njs){
		$4.read();
		var o = Njs.objType; var oR = Njs.objRef;
		var t = Njs.targetType; var tR = Njs.targetRef;
		var vtype = $4.p.vtype; var mt = Njs.movType;
		if(o.match(/(comment|fileUpd|checkList)/)){
			var oWrapId = '._o_'+o+'_'+Njs.objRef;
			var oWrap = $1.q(oWrapId);
			if(mt == 'delete'){ $1.deletAll(oWrapId); }
			if(t == $o.T.activity){
				var qA = $1.q('._o_activity_'+Njs.targetRef,null,'all');
				var afie = (o=='comment')? 'commTotal' : 'nulllll';
				afie = (o=='fileUpd')?'fileTotal' : afie;
				afie = (o=='checkList')?'ckTotal' : afie;
				for(var i=0; i<qA.length; i++){
					var keyO = qA[i].getAttribute('keyo');
					var oDom = $1.q('._5afie_'+afie,qA[i]);
					var oDomText = $1.q('._5afie_'+afie+'_text',qA[i]);
					if(o == 'checkList'){
						var tck = $1.q('._5ck_isChecked',oWrap);
						var c1 = $5a.JQ.A[keyO][afie]*1; var c2 = $5a.JQ.A[keyO].ckComplet*1;
						if(mt == 'add'){ $5a.JQ.A[keyO][afie] = (c1+1)*1; }
						else if(mt == 'delete'){ $5a.JQ.A[keyO][afie] = (c1-1)*1; }
						else if(mt == 'completed'){
							if(tck){ tck.setAttribute('checked','checked'); }
							$5a.JQ.A[keyO].ckComplet = (c2+1)*1;
						}
						else if(mt == 'unCompleted'){
							if(tck){ tck.removeAttribute('checked'); }
							$5a.JQ.A[keyO].ckComplet = (c2-1)*1;
						}
						if(mt=='delete' && tck && tck.checked){
							$5a.JQ.A[keyO].ckComplet = ($5a.JQ.A[keyO].ckComplet-1)*1;
						}
						qua = $5a.JQ.A[keyO][afie]*1-$5a.JQ.A[keyO].ckComplet*1;
						qua = (qua<=0)?0:qua;
					}//ck
					else{
						var qua1 = (isNaN($5a.JQ.A[keyO][afie]*1)) ? 0 : $5a.JQ.A[keyO][afie]*1;
						var qua = (Njs.movType=='delete') ? qua1-1 : qua1;
						qua = (Njs.movType=='add' && Njs.to) ? qua1+Njs.to*1 : qua;
						qua = (Njs.movType=='add' && !Njs.to) ? qua1+1 : qua;
						$5a.JQ.A[keyO][afie] = qua;
					}
					if(oDom){
						oDom.innerText = qua;
						var disp = (qua==0)?'none':'';
						oDom.style.display = disp;
					}
					if(oDomText){ 
						quaText = (afie == 'actStatus')?'-' : '';
						oDomText.innerText = '---';
					}
				}
			}
		}
		else if(o == 'activity'){
			var keyO = Njs.objRef;
			var movType = Njs.movType;
			var qA = $1.q('._o_activity_'+Njs.objRef,null,'all');
			var mcan = movType.match(/^(archived|actType|actStatus|actPriority|userAssg|doDate)$/);
			for(var i=0; i<qA.length; i++){
				var _5afie = '_5afie_'+movType;
				var oDom = $1.q('.'+_5afie,qA[i]);
				if(mcan){
					if(movType == 'archived'){
						$1.delet(qA[i]); delete($5a.JQ.A[keyO]);
					}
					else{
					keyO = qA[i].getAttribute('keyo');
					$5a.JQ.A[keyO][movType] = Njs.to;
					if(oDom){
						display = '';
						if(movType == 'actType'){
							oDom.setAttribute('title',$5a.Vs[movType][Njs.to]);
							oDom.setAttribute('class',_5afie+' iBg iBg_'+Njs.to)
						}
						else if(movType == 'actStatus'){
							if(Njs.to == 'completed'){
								$1.delet(qA[i]); delete($5a.JQ.A[keyO]);
							}
							else{
							oDom.setAttribute('title',$5a.Vs[movType][Njs.to]);
							oDom.setAttribute('class',_5afie+' iBg iBg_'+movType+'_'+Njs.to)
							if(Njs.to == 'none'){ display = 'none';}
							}
						}
						else if(movType == 'actPriority'){
							oDom.setAttribute('title',$5a.Vs[movType][Njs.to]);
							oDom.setAttribute('class',_5afie+' fa fa_prio_'+Njs.to)
							if(Njs.to == 'none'){ display = 'none';}
						}
						else if(movType == 'userAssg'){
							oDom.setAttribute('title',Njs.to2);
							$5a.JQ.A[keyO][movType+'Name'] = Njs.to2;
						}
						else if(movType == 'doDate'){ oDom.innerText = Njs.to; }
						oDom.style.display = display;
					}
					}
				}
			}
			if(
			(movType == 'wboListId' && vtype == 'board') ||
			(movType == 'actPriority' && vtype == 'prio') ||
			(movType == 'actStatus' && vtype == 'status') ||
			(movType == 'actType' && vtype == 'type') ||
			(movType == 'doDate' && vtype.match(/^(week|month|timeline)$/))
			)
			{
				$5a.loadType({P:{}});
			}
		}
	});
}
}
,
usersp:{
open:false,
emit:function(P){
	var userTo = (P.userTo)?P.userTo*1:$0s.userId*1;
	var rid = $0s.ocardSocId+'_'+userTo;
	P.rid = rid;
	if(!$2io.usersp.open){ $2io.usersp.on(); $2io.usersp.open = true; }
	$2io.emit('userRoom',P);
}
,
on:function(Njs){
	$2io.on('userRoom',function(Njs){
		var o = Njs.objType; var oR = Njs.objRef;
		if(o == 'callFunc'){
			eval('var fu = '+Njs.callFunc);
			fu();
		}
		else if(o == 'callTemp'){
			switch(oR){
				case 'href': 
					if(confirm('Va a ser digirido a: '+Njs.href)){ location.href = Njs.href; }
				break;
				case 'message': 
					$1.Win.message(Njs.msg)
				break;
			}
		}
	});
}
}
,
wbo:{
emit:function(P){
	$4.read();
	var send = {wboId:$4.p.view, actId:P.actId, movType:P.movType, from:P.from,to:P.to, act:P.act};
	if(P.addKard){ $2io.emit('r_workBoard_put',P); }
	else{ $2io.emit('r_workBoard_put',send); }
}
,
on:function(func){
	$2io.on('r_workBoard_put',function(Njs){
		$4.read();
		var vtype = $4.p.vtype
		var movType = Njs.movType;
		movType = (movType == 'wboList') ? 'wboListId' : movType;
		var addKard = Njs.addKard;
		var ak = (vtype == 'board') ? addKard.wboListId : 'nullUndefined';
		if(addKard){
			$5a.JQ.A[addKard.keyO] = Njs.addKard;
			if(vtype == 'list'){ ak = ''; var card = $5a.Draw.cardTrList(addKard);  }
			else{ var card = $5a.Draw.cardKanvas(addKard,{}); }
			ak = (vtype == 'prio') ? addKard.actPriority : ak;
			ak = (vtype == 'status') ? addKard.actStatus : ak;
			ak = (vtype == 'type') ? addKard.actType : ak;
			if(vtype == 'week' || vtype == 'month'){
				var dk = (!$2d.is0(addKard.doDate)) ? addKard.doDate : addKard.dueDate;
				ak = dk.substring(0,4)+'_'+$2d.g('z',dk);
			}
			var adk = $1.q('.'+$5a.putKard+ak)
			if(adk){ adk.appendChild(card); }
			$5a.iniDrag();;
		}
		else{
			var qA = $1.q('._o_activity_'+Njs.actId,null,'all');
			for(var i=0; i<qA.length; i++){
				var oDom = $1.q('._5afie_'+movType,qA[i]);
				var keyO = qA[i].getAttribute('keyo');
				if(movType == 'wboListId'){
					if(!$5a.JQ.WbA[Njs.to]){ $5a.JQ.WbA[Njs.to] = {}; }
					$5a.JQ.WbA[Njs.to][Njs.actId] = Njs.actId;
					delete($5a.JQ.WbA[Njs.from][Njs.actId]);
				}
				else if(movType == 'commTotal'){ }
				else{ $5a.JQ.A[keyO][movType] = Njs.to; }
				if(movType == 'actPriority' && oDom){
					oDom.setAttribute('class','_5afie_'+movType+' fa fa_prio_'+Njs.to);
					oDom.setAttribute('title',$5a.Vs.actPriority[Njs.to]);
				}
				else if(oDom && movType == 'actType' && vtype != 'type'){
					oDom.setAttribute('class','_5afie_'+movType+' iBg iBg_'+Njs.to);
				}
				else if(oDom && movType == 'actStatus' && vtype != 'status'){
					oDom.setAttribute('class','_5afie_'+movType+' iBg iBg_actStatus_'+Njs.to);
				}
				else if(oDom && movType == 'doDate'){
					var toTime = $2d.toTime(Njs.to);
					var minTime = $2d.toTime($5a.JQ.minDate);
					var maxTime = $2d.toTime($5a.JQ.maxDate);
					if(toTime < minTime){ $5a.JQ.minDate = Njs.to; }
					else if(toTime >maxTime){ $5a.JQ.maxDate = Njs.to; }
				if(oDom){ oDom.innerText = $5a.JQ.A[keyO].doDate; }
				}
				else if(oDom && movType == 'commTotal'){
					var qua = (Njs.act=='++') ? $5a.JQ.A[keyO][movType]+1 : $5a.JQ.A[keyO][movType];
					qua = (Njs.act=='--') ? $5a.JQ.A[keyO][movType]-1 : qua;
					$5a.JQ.A[keyO][movType] = qua;
					oDom.innerText = qua;
				}
			}
			//invocar la actualización completa si...
			if(
			(movType == 'wboListId' && vtype == 'board') ||
			(movType == 'actPriority' && vtype == 'prio') ||
			(movType == 'actStatus' && vtype == 'status') ||
			(movType == 'actType' && vtype == 'type') ||
			(movType == 'doDate' && vtype.match(/^(week|month|timeline)$/))
			)
			{
				$5a.loadType({P:{}});
			}
		}
	});
}
}
}

$2io.rs ={//_roomSoc_
name:'_roomSoc_',
open:false,
emit:function(P){
	var rsid = $0s.ocardSocId;
	if(!$2io.rs.open){ $2io.rs.on(); $2io.rs.open = true; }
	if(P.ini){ $2io.emit('_roomSoc_',P); }
	else{
		$2io.emit('_roomSoc_',P);//k:activity_put objType, objRef, act, from, to
	}
},
on:function(){
	$2io.on('_roomSoc_',function(R){
		if(R.ot=='activity'){ k = R.or;
			if($Co.a__ && $Co.a__[k]){ $Co.a__[k] = $Co.a[k]; }
			$Tick.load('add',R.data,{_noEmit:'Y'});
		}
	});
}
}

$Co ={
a:{},
acr:{/*act child relation {ct-cr-actId} = A*/},
_u:{
a:function(P,D){
	var k = P.actId;
	var _noEmit = false;
	if(D && D._noEmit){ _noEmit = true; D = null;}
	P._stLine = $Tick.stLine(P);
	var coOld = ($Co.a[k])?$Co.a[k]:{};
	if(!$Co.a[k]){ $Co.a[k] = P; }
	if(D){
		if(D.actStatus){
			D.completed = (D.actStatus=='completed')?'Y':'N';
		}
		for(var f in D){ $Co.a[k][f] = D[f]; }
	}
	else{ $Co.a[k] = P; }
	var Fies = (D)?D:$Co.a[k];
	var qa = $1.q('._o_activity_'+k,0,'all');
	for(var i=0; i<qa.length; i++){
		for(var e in Fies){//todos los campos
			var val = $Co.a[k][e];
			var vt = (e=='actType')?$5a.Vs.actType[val]:val;
			vt = (e=='actStatus')?$5a.Vs.actStatus[val]:vt;
			vt = (e=='actPriority')?$5a.Vs.actPriority[val]:vt;
			var t = $1.q('._5afie_'+e,qa[i],'all');
			for(var e2=0; e2<t.length; e2++){
				var tt = t[e2];
				var nAs = ((e!='actAsunt' && !tt.classList.contains('_5afie_actAsunt')) || e=='actAsunt'); //si tiene asunto no actualizo texto
				var ttext = (tt.innerText && tt.innerText != '');
				if(nAs && ttext){ tt.innerText = vt; }
				if(nAs && tt.value && tt.value!=''){ tt.value = val; }
				var cls = tt.getAttribute('class');
				if(e=='actPriority'){
					var rp = cls.match(/fa\_prio\_(.*)\s?/);
					if(rp && !rp[0]){ continue; }
					tt.classList.remove(rp[0]);
					tt.classList.add('fa_prio_'+val);
				}
				if(e=='actStatus'){
					var rp = cls.match(/ico\_actStatus\_(.*)\s?/);
					if(rp && !rp[0]){ continue; }
					tt.classList.remove(rp[0]);
					tt.classList.add('ico_actStatus_'+val);
				}
				if(e=='actType' && rp && rp[1]){
					var rp = cls.match(/ico\_actType\_(.*)\s?/);
					if(rp && !rp[0]){ continue; }
					tt.classList.remove(rp[0]);
					tt.classList.add('ico_actType_'+val);
				}
			}
		}
	}
	$db.upd('act');
	//en rs.on se llama esta funcion con _noEmit
	if(!_noEmit){ $2io.rs.emit({ot:'activity',or:k,data:$Co.a[k]}); }
},
},
}

var $3 = {
user:function(P,trPare){
	var A = (P.A) ? P.A : 'all';
	if(P.Fie){ var Fie = P.Fie; }
	else{ var Fie = {'userId':{visible:'N'},'user':{text:'Usuario'},'userName':{text:'Nombre'},'officName':{text:'Oficina'},'dpto':{text:'Departamento'}};
		}
		if(P.addFie){ pushO(Fie,P.addFie); }
		$ps_DB.Search.tb({A:'OUSR.'+A, F:Fie, func:P.func, fields:'U.userId,U.user,U.userName,U.officName,U.dpto'},trPare);
}
,
crd:function(P,trPare){
	var addF = {cardNameComer:{n:'A.cardNameComer',text:'Nombre Comerc.'}};
	var A = (P.A) ? P.A : 'all';
	if(P.fieBase){
		var fieldsName = 'A.cardId,A.cardName';
		Fie = {cardName:{n:'A.cardName',text:'Nombre'},cardId:{n:'A.cardId',text:'cardId',cs:'in_x80'},};
		for(var i in P.fieBase){ var L = P.fieBase[i];
			Fie[i] = L;
			fieldsName += ','+L.n;
		}
	}
	else{
		if(P.fieldsName){ var fieldsName = P.fieldsName; }
		else{ var fieldsName = 'A.cardId,A.cardCode,A.cardName'; }
		if(P.Fie){ var Fie = P.Fie; }
		else{ var Fie = {cardId:{n:'A.cardId',text:'cardId',cs:'in_x80'},cardCode:{n:'A.cardCode',text:'Código',cs:'in_x80'},cardName:{n:'A.cardName',text:'Nombre'}}; }
		if(P.delFie){
			var delFies = P.delFie.split(',')
			for(var i=0; i<delFies.length;i++){ var _f = delFies[i];
				delete(Fie[_f]);
			}
		}
		if(P.add && P.add.Fie){
			var toAdd = {}; var exp = (P.add.Fie).split(',');
			for(var o in exp){//o = cardNameComer
				if(addF[exp[o]]){ 
					toAdd[exp[o]] = addF[exp[o]];
					fieldsName += ','+addF[exp[o]].n;
				}
			}
			pushO(Fie,toAdd); delete(P.add.Fie);
		}
	}
	$ps_DB.Search.tb({A:'OCRD.'+A, F:Fie,func:P.func, fields:fieldsName},trPare);
}
,
wboList:function(P,trPare){
	var addF = {wboName:{n:'WB0.wboName',text:'Tablero'}};
	var A = (P.A) ? P.A : 'all';
	if(P.fieldsName){ var fieldsName = P.fieldsName; }
	else{ var fieldsName = 'WB0.wboId,WB0.wboName,WB0.userName,WB1.wboListId,WB1.listName'; }
	if(P.Fie){ var Fie = P.Fie; }
	else{ var Fie = {userName:{n:'WB0.userName',text:'Propietario'},wboName:{n:'WB0.wboName',text:'Tablero'},listName:{n:'WB1.listName',text:'Lista'}}; }
	if(P.add && P.add.Fie){
		var toAdd = {}; var exp = (P.add.Fie).split(',');
		for(var o in exp){//o = cardNameComer
			if(addF[exp[o]]){ 
				toAdd[exp[o]] = addF[exp[o]];
				fieldsName += ','+addF[exp[o]].n;
			}
		}
		pushO(Fie,toAdd); delete(P.add.Fie);
	}
	$ps_DB.Search.tb({A:'OWBO.'+A, F:Fie,func:P.func, fields:fieldsName},trPare);
}
,
O:{//objType
T:{
	crd:'ocrd',
}
,
a:function(P,par){
	href = '';
	var tag = $1.t('a',P.textNode+'-');
	P.k = (P.k == 'profilePartner') ? 'ocrd' : P.k;
	P.k = (P.k == 'fd0_ant0') ? 'os1_oem' : P.k;
	switch(P.k){
		case 'userAssg' : var goTo = (P.useTarget) ? P.useTarget : P.cardId;
			var tag = $1.t('a',{textNode:P.userAssgName,'class':'iBg iBg_user',abbr:P.abbr});
		break;
			case 'ocrd' : var goTo = (P.useTarget) ? P.useTarget : P.cardId;
			var tag = $1.t('a',{href:'/a/bussPartner/profile/'+goTo,textNode:P.cardName,'class':'iBg iBg_customer',abbr:P.abbr});
		break;
		case 'activity' :var goTo = (P.useTarget) ? P.useTarget : P.actId;
			var tag = $1.t('a',{href:'a/forms/order/view/'+goTo,textNode:P.textNode,'class':'iBg obTy_'+P.k,abbr:P.abbr});
		break;
		case 'orderSell' :var goTo = (P.useTarget) ? P.useTarget : P.orderId;
			var tag = $1.t('a',{href:'a/forms/order/view/'+goTo,textNode:P.textNode,'class':'iBg obTy_orderSell',abbr:P.abbr});
		break;
		case 'carteraSell' :var goTo = (P.useTarget) ? P.useTarget : P.docNum;
			var tag = $1.t('a',{textNode:P.textNode,'class':'iBg obTy_'+P.k,abbr:P.abbr});
		break;
		case 'of1_drc1' : var goTo = (P.useTarget) ? P.useTarget : P.transId;
			var tag = $1.t('a',{href:'/a/busspartner/cxc/recaulist#R.transId(E_igual)(T_number)(W_1)='+goTo,textNode:P.textNode,'class':'iBg obTy_of1_drc1',abbr:P.abbr});
		break;
		case 'os1_oem' :var goTo = (P.useTarget) ? P.useTarget : P.entryId;
			var tag = $1.t('a',{href:'/a/a01/control/segfacturas/tesoreria#entryId(E_igual)(T_number)(W_1)='+goTo,textNode:P.textNode,'class':'iBg obTy_os1_oem',abbr:P.abbr});
		break;
		
	}
	if(par){ par.appendChild(tag); }
	else{
		if(P.tagParent){
			var tagParent = $1.t(P.tagParent);
			tagParent.appendChild(tag);
			return tagParent;
		}
		else{ return tag; }
	}
}
}
,
F:{//Win de Formularios sub
$5c:function(objTy,noLoad2,P){P=(P)?P:{};
	var tc = {'class':'winMenuInLine win5c'};
	delete(objTy.noLoad);
	if((typeof(noLoad2)=='string')){ noLoad = noLoad2; }else{ noLoad = false; }
	if((typeof(noLoad2)=='object')){ objTy = $js.push(objTy,noLoad2); }//getList=N
	
	if(noLoad || objTy.getList=='N'){ tc.style = 'display:none;'; objTy.noLoad = noLoad;}
	var wrap = $1.t('div',tc);
	var wrapExist = $1.q('.win5c_open',P.pare);
	if(wrapExist && wrapExist.tagName){
			$1.clear(wrapExist);
			$5c.open(objTy,wrapExist); return true;
	}
	wrap.classList.add('win5c_open');
	var wrapList = $1.t('div'); $1.clear(wrap);
	wrapList.appendChild($5c.open(objTy,'r'));
	wrap.appendChild(wrapList);
	return wrap;
}
,
$5f:function(objTy,noLoad2,P){P=(P)?P:{};
	if((typeof(noLoad2)=='string')){ noLoad = noLoad2; }else{ noLoad = false; }
	if((typeof(noLoad2)=='object')){ objTy = $js.push(objTy,noLoad2); }//getList=N
	
	var tc = {'class':'winMenuInLine win5f'};
	delete(objTy.noLoad);
	if(noLoad || objTy.getList=='N'){ tc.style = 'display:none;'; objTy.noLoad = noLoad; objTy.getList ='N'; }
	else if(typeof(noLoad2)!='object'){ objTy.getList = 'Y' }
	var wrap = $1.t('div',tc);
	var wrapExist = $1.q('.win5f_open',P.pare);

	if(wrapExist && wrapExist.tagName){
		$1.clear(wrapExist);
		$5f.open(objTy,wrapExist); return true;
	}
	wrap.classList.add('win5f_open');
	var wrapList = $1.t('div'); $1.clear(wrap);
	wrapList.appendChild($5f.open(objTy,'r'));
	wrap.appendChild(wrapList);
	return wrap;
}
,
$6Obj:function(P,P2){ var P2=(P2)?P2:{};
	if(P.wrapCont){ $1.clear(P.wrapCont);
		(P.wrapCont).appendChild($6.Obj.get(P,'r'));
		return '';
	}
	var noLoad = (P2 == 'noLoad') ? true : false;
	var wrap = $1.t('div',{'class':'winMenuInLine win6Obj',style:'display:none;'});
	if(noLoad){ return wrap; }
	var wrap = $1.q('.win6Obj');
	var wrapExist = $1.q('.win6Obj_open');
	if(!P.reload && wrapExist && wrapExist.tagName){ return true; }
	$1.clear(wrap);
	wrap.classList.add('win6Obj_open');
	var wrapList = $1.t('div'); $1.clear(wrap);
	//P.objType = 'workBoard'; P.objRef = 1;
	wrapList.appendChild($6.Obj.get(P,'r'));
	wrap.appendChild(wrapList);
}
,
$5ck:function(P,noLoad){ 
	var wrap = $1.t('div',{'class':'winMenuInLine win5ck_R',style:'display:none;'});
	if(noLoad){ return wrap; }
	var wrap = $1.q('.win5ck_R');
	var wrapExist = $1.q('.win5ck_R_open');
	if(wrapExist && wrapExist.tagName){ return true; }
	wrap.classList.add('win5ck_R_open');
	wrap.appendChild($1.t('h4','Lista de Chequeo'));
	wrap.appendChild($5a2.F.line(P))
}

}
}

var $4 = {
read:function(){ $M.read(); }
,
q:{}, p:{}
}

$4m = {
L:{},//savesAuths
viewReports:'N',
wrapList:'',
ini:function(wrap){
	$4m.Wr = {};
	wrap = $1.q('#__menuList');
	var btn = $1.t('button',{'class':'fa fabtn fa_menu',textNode:' Menú'});
	btn.onclick = function(){
		var vDis = wrapList.style.display; 
		if(vDis == 'none' || vDis == ''){ wrapList.style.display = 'block'; }
		else if(vDis == 'block'){  wrapList.style.display = 'none'; }
	}
	var wrapList = $1.t('div',{'class':'ulWrapLevel __menuListWrap'});
	var btnClose = $1.t('button',{'class':'close iBg iBg_closeSmall'});
	btnClose.onclick = function(){ wrapList.style.display = 'none'; }
	wrapList.appendChild(btnClose);
	var ul = $1.t('ul');
	wrapList.appendChild(ul);
	$4m.wrapList = wrapList;
	wrap.appendChild(btn); wrap.appendChild(wrapList);
	if($4m.viewReports == 'Y'){ $4m.L['__queryReports'] = {}; }
	$4m.L['__exit'] = {};
	$4m.folder($4m.L,ul);
}
,
Wr:{},
folder:function(R,wrap){
for(var d in R){
	if(d=='_sin_'){
		$4m.a(R[d],wrap);
	}
	else{
	$4m.Wr[d] = $1.t('li');
	var op = $1.t('button',{'class':'fa fabtn',textNode:' '+d});
	$4m.Wr[d].appendChild(op);
	if(d == '__queryReports'){ op.innerText = ' Reportes (Sys)';
		op.onclick = function(){ $4m.wrapList.style.display = 'none'; $0s.Report.get(); }
		op.classList.add('fa_archive');
		wrap.appendChild($4m.Wr[d]);
	}
	else if(d == '__exit'){
		op.innerText = ' Salir / Cerrar Sesión'; op.classList.add('fa_close');
		op.onclick = function(){ document.location = '/login?out=1'; }
		wrap.appendChild($4m.Wr[d]);
	}
	else if(typeof(R[d]) == 'object'){ op.classList.add('fa_folder');
	var ul = $1.t('ul',{'class':'__ulMenu',style:'display:none;'});
	op.onclick = function(){ var tLi = this.parentNode.childNodes[1];
		if(tLi.style.display == 'none'){ this.classList.remove('fa_folder'); this.classList.add('fa_folderOpen'); tLi.style.display = 'block'; }
		else if(tLi.style.display == 'block'){ this.classList.remove('fa_folderOpen'); this.classList.add('fa_folder');tLi.style.display = 'none'; }
	}
	$4m.Wr[d].appendChild(ul);
		if(d == '__L'){ $4m.a(R[d],wrap); }
		else{
			$4m.folder(R[d],ul);
			wrap.appendChild($4m.Wr[d]);
		}
	}
	else{ wrap.appendChild($4m.Wr[d]); }
	}
}
}
,
a:function(R,wrap){
for(var i=0; i<R.length; i++){
	var li = $1.t('li',{'class':'_link'});
	var a1 = ($0s.URL.a)? '/'+$0s.URL.a+'/' : '/a/';
	var a = $1.t('a',{href:a1+R[i].access,textNode:' '+R[i].text,'class':' fa faBtn fa_arrowCircleR _link'});
	li.appendChild(a);
	wrap.appendChild(li);
}
}

}

var $6 = {
api:'/s/history/A:',
Obj:{
	get:function(P){
		var A = (P.A) ? P.A : 'Obj';
		var wrap = $1.t('div');
		var vPost = 'objType='+P.objType+'&objRef='+P.objRef+'&'+P.vPost;
		$ps_DB.get({file:'GET '+$6.api+A, inputs:vPost, 
		func:function(Jq){
			if(Jq.errNo){ $ps_DB.response(wrap,Jq); }
			else{
				for(var i in Jq.DATA){ var D = Jq.DATA[i];
				var card = $1.t('div',{'class':'cardLog_wrap'});
				var cardTop = $1.t('div',{'class':'cLog_top'});
				var oType = $1.t('span',{'class':'iBg obTy_'+D.objType});
				var D2 = {objType:Jq.objType,objRef:Jq.objRef,nodeBeg:oType};
				var text = $LA.g(D,D2);
				cardTop.appendChild(text);
				var cardUpd = $1.t('div',{'class':'cLog_upd'});
				var lineText = D.lineMemo;
				switch(D.objType){
					case 'comment' :
						cardUpd.appendChild($1.t('div',{innerHTML:D.lineMemo}));
					break;
				}
				card.appendChild(cardTop);
				card.appendChild(cardUpd);
				card.appendChild($1.t('div',{'class':'cLog_dateC',textNode:D.dateC}));
				wrap.appendChild(card);
				}
			}
		}});
		return wrap;
	}
}
,
Obj2:{
	get:function(P){
		var A = (P.A) ? P.A : 'Obj';
		var wrap = $1.t('div');
		var vPost = 'objType='+P.objType+'&objRef='+P.objRef+'&'+P.vPost;
		$ps_DB.get({file:'GET '+$6.api+A, inputs:vPost, 
		func:function(Jq){
			if(Jq.errNo){ $ps_DB.response(wrap,Jq); }
			else{
				for(var i in Jq.DATA){ var D = Jq.DATA[i];
				var card = $1.t('div',{'class':'cardLog_wrap'});
				var cardTop = $1.t('div',{'class':'cLog_top'});
				var user = $1.t('h6',{textNode:D.userName});
				cardTop.appendChild(user);
				var cardUpd = $1.t('div',{'class':'cLog_upd'});
				var lineText = D.lineMemo;
				switch(D.targetType){
					case 'comment' : lineText = 'Comentario';
						cardUpd.appendChild($1.t('div',{textNode:D.lineMemo}));
					break;
					default :
						if(D.v1 != ''){
							var old = $1.t('div');
							old.appendChild($1.t('span',{'class':'spanLine',textNode:'Old'}));
							old.appendChild($1.t('span',{textNode:D.v1}));
							cardUpd.appendChild(old);
						}
						if(D.v2 != ''){
							var nue = $1.t('div');
							nue.appendChild($1.t('span',{'class':'spanLine',textNode:'New'}));
							nue.appendChild($1.t('span',{textNode:D.v2}));
							cardUpd.appendChild(nue);
						}
					break;
				}
				var memo = $1.t('div',{'class':'cLog_memo',textNode:lineText});
				cardTop.appendChild(memo);
				card.appendChild(cardTop);
				card.appendChild(cardUpd);
				card.appendChild($1.t('div',{'class':'cLog_dateC',textNode:D.dateC}));
				wrap.appendChild(card);
				}
			}
		}});
		return wrap;
	}
}

}

var ADMS = {
DEFAULT_CURRENCY:'COP',
Currency:{"COP":"(COP) Pesos Colombianos","USD":"(USD) Dólares Américanos","EUR":"(EUR) Euro"},

regLogList:function(Jq,cont){
	var cont = (cont == undefined) ? {} : cont;
	if(Jq.reLoad ==  true){
		var vPost = ps_DOM.G.inputs(cont.parentNode,'jsFiltVars');
		$ps_DB.get({file:'/s/adms/regLogList', inputs:vPost, loade:cont,
			func:function(Jq2){ ADMS.regLogList(Jq2,cont); }
		});
		return true;
	}
	if(Jq.error){ $ps_DB.response(cont,Jq); return true; }
	ps_DOM.clear(cont);
	for(var i in Jq){ var P = Jq[i];
		var wrap = ps_DOM.newObj('div',{'class':'logTimeLine'});
		var line = ps_DOM.newObj('div',{'class':'line','textNode':P.days});
		var content = ps_DOM.newObj('div',{'class':'content'});
		var cls = (P.fieldName == 'leadStage' && P.relType == 'leads') ? 'leadStage leadStage_'+P.fieldValue : '';
		content.appendChild(ps_DOM.newObj('div',{'class':'lineMemo '+cls,'textNode':P.lineMemo}));
		content.appendChild(ps_DOM.newObj('span',{'class':'dateC','textNode':P.date1}));
		content.appendChild(ps_DOM.newObj('span',{'class':'userName','textNode':P.userName}));
		wrap.appendChild(line);
		wrap.appendChild(content);
		cont.appendChild(wrap);
	}
}
,
regLogListForm:function(Jq,cont,HTMLOpt){
	var cont = (cont == undefined) ? {} : cont;
	var resp = ps_DOM.newObj('div');
	ps_DOM.clear(cont);
	var wrap0 = ps_DOM.newObj('div'); 
	var form = ps_DOM.newObj('form',{method:'GET'});
	form.onsubmit = function(){ Jq.reLoad = true; ADMS.regLogList(Jq,resp); return false; }
		var fieldS = ps_DOM.newObj('fieldset',{'class':'fieldset'});
		var formCont = ps_DOM.newObj('div',{'class':'noneView',style:'display:none'});
		var fieldS = ps_DOM.newObj('fieldset',{'class':'fieldset'});;
		fieldS.appendChild(ps_DOM.Tag.legendOpen(formCont,'On'));
		var divLine = ps_DOM.newObj('div',{'class':'divLine'});
		var wrapxN = ps_DOM.newObj('div',{'class':'wrapx4'});
		wrapxN.appendChild(ps_DOM.newObj('input',{type:'hidden','class':'jsFiltVars', name:'relType', value:Jq.relType}));
		wrapxN.appendChild(ps_DOM.newObj('input',{type:'hidden','class':'jsFiltVars', name:'relId', value:Jq.relId}));
		wrapxN.appendChild(ps_DOM.newObj('label',{'textNode':'Fecha Inicio'}));
		var date0 = ADMS_Date.get('Y-m-d','-3 weeks');
		var date1 = ADMS_Date.get('Y-m-d');
		wrapxN.appendChild(ps_DOM.newObj('input',{type:'date','class':'jsFiltVars', name:'wh[date1(T_time)][0]', value:date0}));
		divLine.appendChild(wrapxN);
		var wrapxN = ps_DOM.newObj('div',{'class':'wrapx4'});
		wrapxN.appendChild(ps_DOM.newObj('label',{'textNode':'Fecha Corte'}));
		wrapxN.appendChild(ps_DOM.newObj('input',{type:'date','class':'jsFiltVars', name:'wh[date1(T_time)][1]', value:date1}));
		divLine.appendChild(wrapxN);
		var wrapxN = ps_DOM.newObj('div',{'class':'wrapx4'});
		wrapxN.appendChild(ps_DOM.newObj('label',{'textNode':'Campo'}));
		var fields = (HTMLOpt) ? HTMLOpt.fields : {};
		wrapxN.appendChild(ps_DOM.Tag.select({sel:{name:'wh[fieldName]', 'class':"jsFiltVars input"},opts:fields}));
		divLine.appendChild(wrapxN);
		formCont.appendChild(divLine);
		var p = ps_DOM.newObj('p');
		p.appendChild(ps_DOM.newObj('input',{type:'submit',value:'Actualizar'}));
		fieldS.appendChild(formCont);
		fieldS.appendChild(p);
		form.appendChild(fieldS);
		wrap0.appendChild(form);
		wrap0.appendChild(resp);
	cont.appendChild(wrap0);
	ADMS.regLogList(Jq,resp);
}
}

var $Aut = {
Types:[{k:"view", v:"Ver"}, {k:"modify", v:"Modificar"}]
}

var $5a = {
apir:{a:'/xs/5a/', rel:'/xs/5a/rel'},
api:'/s/5a/A:',
JQ:{}, //almacena los datos para evitar recargarlos, al ordenar se ordenar
objType:'activity',
activityFormsVars:'activityFormsVars',
putKard:'_5awrapAddKard_',
go:function(g){
	$4.read();
	$5a.iniDrag();
	var view = (g.view) ? g.view : null;
	view = (!g.view && $4.p.view) ? $4.p.view : view ;
	var vtype = (g.vtype) ? g.vtype : 'board';
	vtype = (!g.vtype && $4.p.vtype) ? $4.p.vtype : vtype;
	var vt2 = (g.vt2) ? g.vt2 : '';
	vt2 = (!g.vt2 && $4.p.vt2) ? $4.p.vt2 : vt2;
	vt2 = (vt2 != '') ? ',vt2:'+vt2 : '';
	location.hash = $M.href({r:true,href:'boardId','!!':'{view:'+view+',vtype:'+vtype+vt2+'}'});
	return;
	location.hash = '{view:'+view+',vtype:'+vtype+vt2+'}';

}
,
Vs:{//5a.Vs.
	actType:{task:'Tarea',call:'Llamada',meeting:'Reunión',email:'Email',visit:'Visita',deadLine:'Vencimiento','event':'Evento'},
	actStatus:{none:'No Definido',doing:'En Proceso',onHold:'En Espera',completed:'Completada'},
	actPriority:{'none':'Ninguna',high:'Urgente e Importante',medium:'Urgente pero No Importante',normal:'Importante pero No Urgente',low:'Ni Importante Ni Urgente'},
	lineTime:{overDue:'Vencidos', noComplet:'Sin Realizar', 'today':'Hoy', 'tomorrow':'Mañana', next7Days:'Próximos 7 días', 'noDate':'Sin Programar', completed:'Completadas',future:'Futuros',others:'Otros no Clasificados',inProcess:'En Proceso'}
}
,
iniDrag:function (){
	$1.dnd.i('.cardCanvas_drag_item','.cardCanvas_drag_wrap',{
		onDrop:function(O){
			var keyO = (O.O.i).getAttribute('keyo');
			var froM = (O.i.fromType) ? O.i.fromType : O.i.from;
			var send = {O:O.O, keyO:keyO, actId:O.i.actId, movType:O.w.movType, from:froM, to:O.w.to};
			$5a.S.moveCard(send);
		},
		funcTouch:function(item){
			var vtype = $4.p.vtype;
			var ul = $1.t('ul',{'class':'ulList'});
			var actId = item.getAttribute('actId');
			var keyO = item.getAttribute('keyo');
			var winTitle = 'Mover..';
			switch(vtype){
				case 'prio':{ winTitle = 'Cambiar Prioridad';
					for(var ip in $5a.Vs.actPriority){
						var li = $1.t('li',{textNode:$5a.Vs.actPriority[ip],'class':'fa fa_prio_'+ip,style:'display:block; border-bottom:0.12rem solid #000; padding-top:0.25rem; margin-bottom:0.5rem;'}); li.prio = ip;
						li.onclick = function(){ $5a.S.updField({actId:actId,movType:'actPriority',to:this.prio, keyO:keyO }); 
							$1.delet(bkFixed);
						}
						ul.appendChild(li);
					}
				break;}
				case 'status':{ winTitle = 'Cambiar Estado';
					for(var ip in $5a.Vs.actStatus){
						var li = $1.t('li',{textNode:$5a.Vs.actStatus[ip],'class':'iBg iBg_actStatus_'+ip,style:'display:block; border-bottom:0.12rem solid #000; padding-top:0.25rem; margin-bottom:0.5rem;'}); li.to = ip;
						li.onclick = function(){ $5a.S.updField({actId:actId,movType:'actStatus',to:this.to, keyO:keyO }); 
							$1.delet(bkFixed);
						}
						ul.appendChild(li);
					}
				break;}
				case 'type':{ winTitle = 'Cambiar Tipo';
					for(var ip in $5a.Vs.actType){
						var li = $1.t('li',{textNode:$5a.Vs.actType[ip],'class':'iBg iBg_actType_'+ip,style:'display:block; border-bottom:0.12rem solid #000; padding-top:0.25rem; margin-bottom:0.5rem;'}); li.to = ip;
						li.onclick = function(){ $5a.S.updField({actId:actId,movType:'actType',to:this.to, keyO:keyO }); 
							$1.delet(bkFixed);
						}
						ul.appendChild(li);
					}
				break;}
				case 'board':{ winTitle = 'Cambiar de Lista';
					var ao = $5a.JQ.A[keyO];
					for(var wb_ in $5a.JQ.WBOL){
						var li = $1.t('li',{textNode:$5a.JQ.WBOL[wb_],'class':'fa fa_kanban',style:'display:block; border-bottom:0.12rem solid #000; padding-top:0.25rem; margin-bottom:0.5rem;'}); li.to = wb_;
						li.onclick = function(){
							$5a.S.moveCard({actId:actId,movType:'wboList',from:item.Odnd.from, to:this.to, keyO:keyO });
							item.Odnd.from = this.to;
							$5a.JQ.A[keyO].wboListId = this.to;
							$1.delet(bkFixed);
						}
						ul.appendChild(li);
					}
				break;}
				default:{
					if(vtype == 'week' || vtype == 'month'){
						var ul = $1.t('div');
						var winTitle = 'Cambiar Fecha';
						var doDate = $1.t('date',{'class':'divLine'});
						ul.appendChild(doDate);
						var se = $1.t('button',{'class':'fa fa_date',textNode:'Definir Fecha'});
						se.onclick = function(){
							$5a.S.moveCard({actId:actId,movType:'doDate', to:doDate.childNodes[0].value, keyO:keyO });
							$1.delet(bkFixed);
						}
						ul.appendChild(se);
					}
				break;}
			}
			var bkFixed = $1.Win.open(ul,{winTitle:winTitle});
			ps_DOM.body.appendChild(bkFixed);
		}
	});
}
,
loadType:function(Jq_P){
	//wrapContMenu = es el que continue wrapMenu, para poner dates debajo
	var wrapContMenu = $1.G.byId('_5a_wrapContMenu')//; Jq_P.wrapContMenu;
	var wrapList = $1.G.byId('_5a_wrapList'); //Jq_P.wrapList;
	$1.clear(wrapList);
	var P = Jq_P.P;
	$4.read();
	var viewType = $4.p.vtype; var ty = {};
	var vt2 = $4.p.vt2;
	P.movDateType = (P.movDateType) ? P.movDateType : '';
	var movDate = (viewType.match(/^(week|weekh|month|diary)$/)) ? true : false
	if(movDate){
		$5a.Draw.gooDate(wrapContMenu,Jq_P);//hace append
		var date1 = $1.q('.gooMovDate_1',wrapContMenu);
		var date1Text = $1.q('.gooMovDateText_1',wrapContMenu);
		var date2 =$1.q('.gooMovDate_2',wrapContMenu);
		var date2Text = $1.q('.gooMovDateText_2',wrapContMenu);
		var viewDateType = (viewType == 'week') ? 'week' : '';
		var viewDateType = (viewType == 'month') ? 'month' : viewDateType;
		var Di = $2d.goRang({date1:date1.value,date2:date2.value,rang:viewDateType,movType:P.movDateType});
		date1.value = Di.date1;
		date2.value = Di.date2;
	}
	else{ $1.clear(wrapContMenu); }
	
	switch(viewType){
		case 'prio' :{
			var _1 = ($4.p.vt2 == 'list') ? '' : 'active' ;
			var _2 = ($4.p.vt2 == 'list') ? 'active' : '';
			var menu = ps_DOM.Menu.lick([
			{li:{textNode:'Covey','class':_1,winClass:'win2_covey',
			func:function(){ $1.clear(wrapList);
				$1.append(wrapList,$5a.V.Prio.covey(P)); $5a.go({vt2:'covey'}); 
			} }},
			{li:{textNode:'Kanban','class':_2,winClass:'win3_list',
			func:function(){ $1.clear(wrapList);
				$1.append(wrapList,$5a.V.Prio.kanban(P)); $5a.go({vt2:'kanban'}); 
			} }},
			{li:{textNode:'Lista','class':_2,winClass:'win2_list',
			func:function(){ $1.clear(wrapList);
				$1.append(wrapList,$5a.V.Prio.list(P)); $5a.go({vt2:'list'}); 
			} }}
			],wrapContMenu);
			wrapContMenu.appendChild(menu);
			if(vt2 == 'list'){ $1.append(wrapList,$5a.V.Prio.list(P)); }
			else if(vt2 == 'kanban'){ $1.append(wrapList,$5a.V.Prio.kanban(P)); }
			else{ $1.append(wrapList,$5a.V.Prio.covey(P)); }
		break;}
		case 'list' : $1.append(wrapList,$5a.V.listt(P)); break;
		case 'type' : $1.append(wrapList,$5a.V.Type.list(P)); break;
		case 'status' :{
			var _1 = ($4.p.vt2 == 'list') ? '' : 'active' ;
			var _2 = ($4.p.vt2 == 'list') ? 'active' : '';
			var menu = ps_DOM.Menu.lick([
			{li:{textNode:'Kanban','class':_1,winClass:'win2_kanban',
			func:function(){ $1.clear(wrapList);
				$1.append(wrapList,$5a.V.Status.kanban(P)); $5a.go({vt2:'kanban'}); 
			} }},
			{li:{textNode:'Lista','class':_2,winClass:'win2_list',
			func:function(){ $1.clear(wrapList);
				$1.append(wrapList,$5a.V.Status.list(P)); $5a.go({vt2:'list'}); 
			} }}
			],wrapContMenu);
			wrapContMenu.appendChild(menu);
			if(vt2 == 'list'){ $1.append(wrapList,$5a.V.Status.list(P)); }
			else { $1.append(wrapList,$5a.V.Status.kanban(P)); }
		break;}
		case 'diary' :{
			date1 = ADMS_Date.get2('mmm d',Di.date1);
			date2 = ADMS_Date.get2('mmm d',Di.date2);
			$1.append(wrapList,$5a.V.status(P));
		break;}
		case 'week' :{
			date1 = ADMS_Date.get2('mmm d',Di.date1);
			date2 = ADMS_Date.get2('mmm d',Di.date2);
			$1.append(wrapList,$5a.V.week(P));
		break;}
		case 'month' :{
			date1 = ADMS_Date.get2('mes',Di.date1);
			date2 = '';
			$1.append(wrapList,$5a.V.month(P));
		break;}
		case 'timeline' :{
			date2 = '';
			$1.append(wrapList,$5a.V.timeline(P));
		break;}
		default :{
			var _1 = ($4.p.vt2 || $4.p.vt2 == 'list') ? '' : 'active' ;
			var _2 = ($4.p.vt2 == 'list') ? 'active' : '';
			var menu = ps_DOM.Menu.lick([
			{li:{textNode:'Board','class':_1,winClass:'win2_1',
			func:function(){ $1.clear(wrapList);
				$1.append(wrapList,$5a.V.Kanban.board(P)); $5a.go({vt2:'covey'});
			} }},
			{li:{textNode:'Lista','class':_2,winClass:'win2_2',
			func:function(){ $1.clear(wrapList);
				$1.append(wrapList,$5a.V.Kanban.list(P)); $5a.go({vt2:'list'});
			} }}
			],wrapContMenu);
			wrapContMenu.appendChild(menu);
			if(vt2 == 'list'){ $1.append(wrapList,$5a.V.Kanban.list(P)); break; }
			else{ $1.append(wrapList,$5a.V.Kanban.board(P)); }
		break;}
	}
	if(movDate){
	ps_DOM.clear(date1Text);  date1Text.appendChild($1.t('textNode',date1)); 
	ps_DOM.clear(date2Text);  date2Text.appendChild($1.t('textNode',date2)); 
	}
	//drap 
	$5a.iniDrag();
}
,
F:{
btnAdd:function(P,pare,D){
	var D=(D)?D:{};//D son para preconfigurar form act
	var sBy = (P.sBy) ? P.sBy : 'sBy.wboList';
	var os = $js.push({'class':'fa faBtn fa_plusCircle'},P.btn);
	var add = $1.t('button',os);
	add.onclick = function(){
		$5a.F.sBy({sBy:sBy,wboListId:P.wboListId},pare,D);
	}
	return add;
}
,
quickLine:function(P,wrapCont,D){
	var wrap = $1.t('div',{style:'border-bottom:1px solid #CCC; padding:2px 8px 2px 6px;'});
	var vPost = 'actType=task';
	var nForm = $1.t('input',{type:'button','class':'iBg iBg_uiForm'});
	nForm.onclick = function(){ $5a.F.sBy(P,wrapCont,D); }
	var asunt = $1.t('textarea',{'class':'jsFields boxi1 iBg iBg_task', name:'actAsunt',style:'width:85%; border:none; border-bottom:1px solid #CCC; padding:0.2rem; font-size:0.65rem; max-height:6rem;',placeholder:'Actividad Nueva...',O:{vPost:vPost}});
	asunt.onkeyup = function(evt){ This = this;
		var lin = this.value.length/30;
		var hei = 2+(0.25*lin);
		if(lin>1){ this.style.height = hei+'rem'; }
		ADMS_EV.isKey(evt,13,{func:function(){
			if(P.sBy == 'sBy.wboList'){
				if(P.wboListId){ vPost += '&wboListId='+P.wboListId; }
				else{ vPost += '&wboListId='+$js.afind($5a.JQ.WBOL); }
			}
			vPost += '&'+$1.G.inputs(wrap);
			ADMS_DB.get({file:'POST '+$5a.api+P.sBy, inputs:vPost,
			func:function(Jq1){
				if(Jq1.errNo){ ps_DOM.Win.message(Jq1) }
				else{ $1.clearInps(wrap);
					Jq1.wboId = $4.p.view; var A2 = Jq1.addKard;
					$5a.JQ.A[A2.actId] = A2;
					$2io.wbo.emit(Jq1); delete(Jq1.wboId);
				}
			}
			});
		}
		});
	}
	wrap.appendChild(nForm);
	wrap.appendChild(asunt);
	if(wrapCont){ wrapCont.appendChild(wrap); }
	else{ return wrap; }
}
,
sBy:function(P,wrapCont,D){ var D=(D)?D:{};
	var wrap = $1.t('div');
	var sBy = (P.sBy)?P.sBy:'sBy.wboList';//SBy.wboList
	D.actType = (D.actType) ? D.actType : 'task';
	var sel = ps_DOM.Tag.divSelect({sel:{'class':'jsFields',name:'actType'}, classBtn:'boxi1',opts:$5a.Vs.actType, selected:D.actType,iBgOpts:''});
	var selPriority = ps_DOM.Tag.divSelect({sel:{'class':'jsFields',name:'actPriority'}, classBtn:'boxi1', opts:$5a.Vs.actPriority, selected:D.actPriority,clsFa:'prio_',classWrap:'input',noBlank:'Y'});
		var selStatus = ps_DOM.Tag.divSelect({sel:{'class':'jsFields',name:'actStatus'}, classBtn:'boxi1', opts:$5a.Vs.actStatus, selected:D.actStatus,iBgOpts:'actStatus_',classWrap:'input',noBlank:'Y'});
	var userAssg = $1h.userAssg(D,{wxn:'wrapx1'});
	//$js.push(D,P.addD);
	$4.read(); var isunBoard = ($4.p.view == 'unboard' || P.wboId=='unboard') ? true : false;
	var Lis = [{},
	{L:{textNode:'Asunto:'},I:{tag:'input',type:'text','class':'jsFields divLine',name:'actAsunt'}},
	{L:{textNode:'Tipo:'},node:sel},
	{L:{textNode:'Prioridad:'},node:selPriority},
	{L:{textNode:'Estado:'},node:selStatus},
	{L:{textNode:'Fecha:'},I:{tag:'input',type:'date','class':'jsFields divLine',name:'doDate',value:D.doDate}},
	{L:{textNode:'Vencimiento:'},I:{tag:'input',type:'date','class':'jsFields  divLine iBg_deadLine',name:'dueDate',value:D.dueDate}},
	{L:{textNode:'Responsable:'},node:userAssg},
	{L:{textNode:'Detalles:'},I:{tag:'textarea',type:'text','class':'jsFields',name:'actNote'}},
	];
	if(isunBoard){ delete(Lis[0]); }
	else{
		var wboList = ps_DOM.Tag.divSelect({sel:{'class':'jsFields',name:'wboListId'}, classBtn:'boxi1', opts:$5a.JQ.WBOL, selected:P.wboListId,iBgOpts:'',classWrap:'input'});
		Lis[0] = {L:{textNode:'Lista:'},node:wboList};
	}
	var seF = (P.seF)?P.seF:{};
	seF.putResp = true;
	var no = $o.Se.form(seF);
	Lis.push({node:no,oneRow:true});
	var rows = $1.T.tbFieldsLeft(Lis);
	wrap.appendChild(rows);
	var iResp = $1.t('div');
	var iSend = $1.T.btnSend({textNode:'Guardar'},{file:'POST '+$5a.api+sBy, 
	getInputs:function(){ var vPost = $1.G.inputs(wrap); 
	vPost += (isunBoard) ? '&wboId=unboard' : '';
	return vPost; },
			func:function(Jq1){
				if(Jq1.errNo){ $ps_DB.response(iResp,Jq1) }
				else{ $1.delet(bkFull)
					if(P.func){ P.func(); }
					else{
						Jq1.wboId = $4.p.view;
						$5a.JQ.A[Jq1.addKard.keyO] = Jq1.addKard;
						$2io.wbo.emit(Jq1); delete(Jq1.wboId);
					}
				}
			}
	});
	wrap.appendChild(iResp); wrap.appendChild(iSend);
	var bkFull = $1.Win.openBk(wrap,{ret:true});
	ps_DOM.body.appendChild(bkFull);
}
,
full:function(D,P){ var P=(P)?P:{};
		D.actId = (D.actId) ? D.actId : '';
		D.actType = (D.actType) ? D.actType : '';
		D.actAsunt = (D.actAsunt) ? D.actAsunt : '';
		D.actNote = (D.actNote) ? D.actNote : '';
		var nowDate = ADMS_Date.get();
		D.doTime = (D.doDate) ? $2d.f(D.doDate,'H:i',{is0:''}) : '';
		D.doDate = (D.doDate) ? $2d.f(D.doDate,'Y-m-d',{is0:''}) : '';
		D.endTime = (D.endDate) ? $2d.f(D.endDate,'H:i',{is0:''}) : '';
		D.endDate = (D.endDate) ? D.endDate : '';
		D.actPlace = (D.actPlace) ? D.actPlace : '';
		D.timeDuration = (D.timeDuration) ? D.timeDuration : '00:10';
		D.dueDate = (D.dueDate) ? D.dueDate : '';
		var wrap = $1.t('div');
		var divLine = $1.t('div',{'class':'divLine'});
		var wxn = $1.t('div',{'class':'wrapx4'});
		wxn.appendChild($1.t('label',{textNode:'Categoria'}));
		var sel = ps_DOM.Tag.divSelect({sel:{'class':'jsFields',name:'actType'}, classBtn:'boxi1',opts:$5a.Vs.actType, selected:D.actType,iBgOpts:'',classWrap:'input',
			func:function(divClick){
				D.actType = divClick.getAttribute('k');;
				$5a.F.full(D,P);
			}
			});
		sel.D = D;
		wxn.appendChild(sel);
		divLine.appendChild(wxn);
		var wxn = $1.t('div',{'class':'wrapx4'});
		wxn.appendChild($1.t('label',{textNode:'Vencimiento'}));
		wxn.appendChild($1.t('input',{type:'date','class':'jsFields',name:'dueDate',value:D.dueDate}));
		divLine.appendChild(wxn);
		wrap.appendChild(divLine);
		var divLine = $1.t('div',{'class':'divLine'});
		var wxn = $1.t('div',{'class':'wrapx1'});
		wxn.appendChild($1.t('label',{textNode:'Asunto'}));
		var ast = $1.t('textarea',{type:'text','class':'jsFields',name:'actAsunt',value:D.actAsunt});
		ast.onkeyup  = function(evt){
			this.value = this.value.replace(/(\n|\t|\r)+/g,'');
		}
		wxn.appendChild(ast);
		divLine.appendChild(wxn);
		wrap.appendChild(divLine);
		var divLine = $1.t('div',{'class':'divLine'});
		var wxn = $1.t('div',{'class':'wrapx4'});
		wxn.appendChild($1.t('label',{textNode:'Fecha'}));
		wxn.appendChild($1.t('input',{type:'date','class':'jsFields',name:'doDate',value:D.doDate}));
		divLine.appendChild(wxn);
		var wxn = $1.t('div',{'class':'wrapx8'});
		wxn.appendChild($1.t('label',{textNode:'Hora'}));
		var doTime = $1.t('input',{type:'text','class':'in_clock jsFields',name:'doTime',value:D.doTime});
		doTime.onfocus = doTime.onclick = function(){ ADMS_Date.Sel.hour(this); }
		wxn.appendChild(doTime);
		divLine.appendChild(wxn);
		if((D.actType) && D.actType.match(/^call|visit|task|meeting$/)){
			var wxn = $1.t('div',{'class':'wrapx4'});
			wxn.appendChild($1.t('label',{textNode:'Duración'}));
			var actDurat = $1.t('input',{type:'text','class':'iBg2 iBg_chronometer jsFields',name:'timeDuration',value:D.timeDuration});
			actDurat.onfocus = function(){ ADMS_Date.Sel.duration(this); }
			wxn.appendChild(actDurat);
			divLine.appendChild(wxn);
		}
		wrap.appendChild(divLine);
		if((D.actType) && (D.actType).match(/^event$/)){
			//var divLine = $1.t('div',{'class':'divLine'});
			var wxn = $1.t('div',{'class':'wrapx4'});
			wxn.appendChild($1.t('label',{textNode:'Fecha Finalización'}));
			wxn.appendChild($1.t('input',{type:'date','class':'jsFields',name:'endDate',value:D.endDate}));
			divLine.appendChild(wxn);
				var wxn = $1.t('div',{'class':'wrapx8'});
				wxn.appendChild($1.t('label',{textNode:'Hora'}));
			var endTime = $1.t('input',{type:'text', admsitype:'hour', 'class':'in_clock jsFields',name:'endTime',value:D.endTime});
			endTime.onfocus = endTime.onclick = function(){ ADMS_Date.Sel.hour(this); }
			wxn.appendChild(endTime);
			divLine.appendChild(wxn);
			wrap.appendChild(divLine);
			var divLocat = $1.t('div',{'class':'divLine'});
			divLocat.appendChild($1.t('label',{'textNode':'Lugar, Dirección'}));
			var actLocation = $1.t('input',{type:'text','class':'inlocation jsFields',name:'actPlace','placeholder':'Plaza Bolivar, Kr 23 #52-102',value:D.actPlace});
			divLocat.appendChild(actLocation);
			wrap.appendChild(divLocat);
		}
		var divNote = $1.t('div');
		var textNote = $1.t('textarea',{'class':'textNoteWrap jsFields','name':'actNote','placeholder':'Notas...',textNode:D.actNote});
		divNote.appendChild(textNote);
		wrap.appendChild(divNote);
		var addNodes = $1.t('input',{type:'hidden','class':'jsFields searchChange_userId',name:'userAssg',value:D.userAssg});
		wrap.appendChild($1.T.divLinewx({divLine:true,wxn:'wrapx1', Label:{textNode:'Responsable'}, I:{tag:'input',type:'text','class':'jsFields searchChange_userName',name:'userAssgName',readonly:'readonly',value:D.userAssgName},
		btnLeft:{func:function(){ $3.user({},wrap); } },
		addNodes:addNodes
		}));
		var resp = $1.t('div',{style:'font-size:12px;'});
		var divBtn = $1.t('div',{style:'margin:6px 0;'});
		var iSave = $1.t('input',{type:'submit',value:'Guardar'});
		iSave.onclick = function(){
			var vPost = 'actId='+D.actId+'&'+$1.G.inputs(wrap);
			ADMS_DB.get({file:'PUT '+$5a.api+'S.full', inputs:vPost, loade:resp,
			func:function(Jq2){
				if(Jq2.errNo){ $ps_DB.response(resp,Jq2); }
				else{ $1.delet(wrapBk);
					if(P.callf =='board'){ $5b.List.get({}); }
					else{ $5a.reload(); }
				}
			}
			});
		}
		divBtn.appendChild(resp);
		divBtn.appendChild(iSave);
		wrap.appendChild(divBtn);
		var wrapBk = ps_DOM.Tag.Win.bkFixed(wrap,{winTitle:'Modificar Actividad',intMaxWidth:'600px'});
		wrapBk.id = 'activitySimpleWrapForm';
		ps_DOM.delet('activitySimpleWrapForm');
		ps_DOM.body.appendChild(wrapBk);
	}
}
,
V:{
Kanban:{
board:function(P){
	var wrap = $1.t('div',{'class':'_5bworkBoard'});
	var wrapTotal = $1.t('div');
	var wrapLis = {}; 
	Jq = $5a.JQ;
	for(var l2 in Jq.L){ var Li = Jq.L[l2];
		var l = Li.wboListId;
		wrapLis[l] = $1.t('div',{'class':'_5bwboList cardCanvas_drag_wrap'});
		wrap.appendChild(wrapLis[l]);
		var wrapTop = $1.t('div',{textNode:Li.listName+' ',style:'border-bottom:0.1875rem solid '+Li.listColor+';','class':'_5bwraptop'});
		var wrapAdd = $1.t('div');
		if(Jq.wboId == 'unboard'){ wrapAdd.style.display = 'none'; }
		var wrapCards = $1.t('div',{'class':'_5bwrapCars cardCanvas_drag_wrapReal '+$5a.putKard+l});
		wrapLis[l].appendChild(wrapTop);
		wrapLis[l].appendChild(wrapAdd);
		wrapLis[l].appendChild(wrapCards);
		wrapTop.appendChild($1.t('span',{'class':'spanLine actTotal boardListTotal_'+Li.wboListId,textNode:Li.actOpen+'/'+Li.actTotal}));
		wrapCards.Odnd = {movType:'wboList',to:Li.wboListId};
		var add = $5a.F.quickLine({sBy:'sBy.wboList',wboListId:Li.wboListId,
			func:function(){ $5b.List.get({wboId:Li.wboId}); }
			},wrapAdd,{});
	}
	var addList = $1.t('input',{type:'button','class':'btnAddText iBg_add',value:'Lista...'});
	addList.B = {wboId:P.wboId}; //wboId
	addList.onclick = function(){ $5b.List.getForm(this.B); }
	wrap.appendChild(addList);
	if(!Jq.A.errNo) for(var k1 in Jq.WbA){
	for(var a in Jq.WbA[k1]){
		var A = Jq.A[a];
		if(!A){ continue; }
		A.wboId = Jq.wboId;
		if(wrapLis[k1]){
			var li = $5a.Draw.cardKanvas(A,P);
			if(Jq.Aon&&Jq.Aon[k1]){ li.Aon = Jq.Aon[k1]; }
			var wrapOn = wrapLis[k1].childNodes[2];
			wrapOn.appendChild(li);
		}
	}}
	for(var l in Jq.L){ var k = Jq.L[l].wboListId;
		var spanLine = $1.q('.boardListTotal_'+k,wrapLis[k]);
		spanLine.innerText = wrapLis[k].childNodes[2].childNodes.length;
	}
	wrap.appendChild($1.t('clear'));
	return wrap;
}
,
list:function(P){
	var wrap = $1.t('div');
	Jq = $5a.JQ;
	if(Jq.A.errNo){ $ps_DB.response(wrap,Jq.A); return wrap; }
	var Tdiv = {};
	for(var l2 in Jq.L){ var Li = Jq.L[l2];
		var k = Li.wboListId;
		Tdiv[k] = $1.t('div',{style:'padding:0 0 1rem;'});
		var h6 = $1.t('h4');
		h6.appendChild($1.t('span',{'class':'iBg iBg_'+k}));
		h6.appendChild($1.t('span',{textNode:Li.listName}));
		var div = $1.t('div',{'class':'cardCanvas_drag_wrap '+$5a.putKard+k,style:'min-height:5rem; margin-left:2rem; border:0.1rem solid '+Li.listColor+'; padding-left:0.5rem; border-top:none; border-right:none;'});
		div.Odnd = {movType:'wboList',to:Li.wboListId};
		h6.appendChild($1.t('span',{'class':'actTotal'}));
		var add = $5a.F.btnAdd({wboListId:Li.wboListId},Tdiv[k],{});
		h6.appendChild(add);
		Tdiv[k].appendChild(h6);
		Tdiv[k].appendChild(div);
		wrap.appendChild(Tdiv[k]);
	}
	for(var k1 in Jq.WbA){
		for(var a in Jq.WbA[k1]){ A = Jq.A[a];
			var li = $5a.Draw.lineBasic(A,P);
			Tdiv[k1].childNodes[1].appendChild(li);
		}
	}
	for(var l2 in Jq.L){ var k = Jq.L[l2].wboListId;
		var tTotal = Tdiv[k].childNodes[1].childNodes.length;
		Tdiv[k].childNodes[0].childNodes[2].innerText = ' ('+tTotal+')';
	}
	return wrap;
}
}
,
Prio:{
covey:function(P){//board
	Jq = $5a.JQ;
	var wrap = $1.t('div');
	if(Jq.A.errNo){ $ps_DB.response(wrap,Jq.A); return wrap; }
	var tb = $1.t('table',{'class':'table_covey'});
	var tBody = $1.t('tbody');
	var tr1 = $1.t('tr');
	var tr2 = $1.t('tr');
	var tr3 = $1.t('tr');
	var Odnd = {movType:'actPriority',to:'high'};
	var TDs = {};
	var WRs = {};
	for(var k in $5a.Vs.actPriority){
		TDs[k] = $1.t('td',{'class':k+' cardCanvas_drag_wrap',style:'min-width:20rem;'});
		if(k == 'none'){ TDs[k].setAttribute('colspan',2); }
		var title = $1.t('div',{'class':'title'});
		var add = $5a.F.btnAdd({wboListId:null},title,{actPriority:k});
		title.appendChild(add);
		title.appendChild($1.t('textNode',' '+$5a.Vs.actPriority[k]));
		TDs[k].appendChild(title);
		WRs[k] = $1.t('div',{'class':'wrapList cardCanvas_drag_wrapReal '+$5a.putKard+k});
		WRs[k].Odnd = {movType:'actPriority',to:k}
		TDs[k].appendChild(WRs[k]);
		if(k == 'high' || k == 'medium'){ tr1.appendChild(TDs[k]); }
		else if(k == 'normal' || k == 'low'){ tr2.appendChild(TDs[k]); }
		else {tr3.appendChild(TDs[k]); }
	}
	for(var i in Jq.A){
		var k = Jq.A[i].actPriority;
		if($1.q('._5a_cardIdWrap_'+Jq.A[i].actId,WRs[k])){ continue; }
		if(!WRs[k]){ k = 'none'; }
		var li = $5a.Draw.lineBasic(Jq.A[i],P); li.title = $5a.Vs.actPriority[k];
		WRs[k].appendChild(li);
	}
	tBody.appendChild(tr1);
	tBody.appendChild(tr2);
	tBody.appendChild(tr3);
	tb.appendChild(tBody);
	wrap.appendChild(tb);
	return wrap;
}
,
list:function(P){
	Jq = $5a.JQ;
	var wrap = $1.t('div');
	if(Jq.A.errNo){ $ps_DB.response(wrap,Jq.A); return wrap; }
	var Tdiv = {};
	for(var k in $5a.Vs.actPriority){
		Tdiv[k] = $1.t('div',{'class':'table_covey',style:'padding:0 0 1rem;'});
		var h6 = $1.t('h4',{'class':k});
		h6.appendChild($1.t('span',{'class':'fa fa_prio_'+k}));
		h6.appendChild($1.t('span',{textNode:$5a.Vs.actPriority[k]}));
		var div = $1.t('div',{'class':'cardCanvas_drag_wrap '+$5a.putKard+k,style:'min-height:5rem; margin-left:2rem; padding-left:0.5rem;'});
		div.Odnd = {movType:'actPriority',to:k};
		h6.appendChild($1.t('span',{'class':'actTotal'}));
		var add = $5a.F.btnAdd({wboListId:null},h6,{actPriority:k});
		h6.appendChild(add);
		Tdiv[k].appendChild(h6);
		Tdiv[k].appendChild(div);
		wrap.appendChild(Tdiv[k]);
	}
	for(var a in Jq.A){ var A = Jq.A[a]; var k1 = A.actPriority;
		if(!Tdiv[k1]){ continue; }
		if($1.q('._5a_cardIdWrap_'+A.actId,wrap)){ continue; }
		var li = $5a.Draw.lineBasic(A,P);
		Tdiv[k1].childNodes[1].appendChild(li);
	}
	for(var k in $5a.Vs.actPriority){
		var tTotal = Tdiv[k].childNodes[1].childNodes.length;
		Tdiv[k].childNodes[0].childNodes[2].innerText = ' ('+tTotal+')';
	}
	return wrap;
}
,
kanban:function(P){
	var wrap = $1.t('div',{'class':'_5bworkBoard'});
	var wrapTotal = $1.t('div');
	var wrapLis = {};
	Jq = $5a.JQ;
	for(var k in $5a.Vs.actPriority){
		wrapLis[k] = $1.t('div',{'class':'_5bwboList'});
		wrap.appendChild(wrapLis[k]);
		var wrapTop = $1.t('div',{'class':'_5bwraptop'});
		wrapTop.appendChild($1.t('span',{'class':'fa fa_prio_'+k}));
		wrapTop.appendChild($1.t('span',$5a.Vs.actPriority[k]+' '));
		var wrapAdd = $1.t('div');
		var add = $5a.F.btnAdd({btn:{textNode:' Añadir Tarjeta'}, wboListId:null},wrapAdd,{actPriority:k});
		wrapAdd.appendChild(add);
		var wrapCards = $1.t('div',{'class':'_5bwrapCars cardCanvas_drag_wrap '+$5a.putKard+k});
		wrapLis[k].appendChild(wrapTop);
		wrapLis[k].appendChild(wrapAdd);
		wrapLis[k].appendChild(wrapCards);
		wrapTop.appendChild($1.t('span',{'class':'spanLine actTotal'}));
		wrapCards.Odnd = {movType:'actPriority',to:k};
	}
	if(!Jq.A.errNo) for(var a in Jq.A){ var A = Jq.A[a]; var k1 = A.actPriority;
		if($1.q('._5a_cardIdWrap_'+A.actId,wrap)){ continue; }
		A.wboId = Jq.wboId;
		var li = $5a.Draw.cardKanvas(A,P);
		if(wrapLis[k1]){
			var wrapOn = wrapLis[k1].childNodes[2];
			wrapOn.appendChild(li);
		}
	}
	for(var k in $5a.Vs.actPriority){
		var spanLine = $1.q('.actTotal',wrapLis[k]);
		spanLine.innerText = wrapLis[k].childNodes[2].childNodes.length;
	}
	wrap.appendChild($1.t('clear'));
	return wrap;
}
}
,
Status:{
kanban:function(P){
	Jq = $5a.JQ;
	var wrap = $1.t('div');
	var tb = $1.t('table',{'class':'tbCanvasCard'});
	var tHead = $1.t('thead');
	var tr0 = $1.t('tr');
	var tr1 = $1.t('tr');
	var TR = {};
	for(var k in $5a.Vs.actStatus){ var Li = $5a.Vs.actStatus[k];
		var tdName = $1.t('td',{textNode:Li+' '});
		tdName.appendChild($1.t('span',{'class':'spanLine actTotal actStatus_'+k,textNode:''}));
		tr0.appendChild(tdName);
		TR[k] = $1.t('td',{'class':'cardCanvas_drag_wrap '+$5a.putKard+k});
		TR[k].Odnd = {movType:'actStatus',to:k}
		tr1.appendChild(TR[k]);
	}
	tHead.appendChild(tr0);
	tb.appendChild(tHead);
	var tBody = $1.t('tbody');
	tBody.appendChild(tr1);
	if(!Jq.A.errNo) for(var a in Jq.A){ var A = Jq.A[a];
	if($1.q('._5a_cardIdWrap_'+A.actId,tBody)){ continue; }
	var k1 = A.actStatus;
		A.wboId = Jq.wboId; P.Odnd = {fromType:A.actStatus};
		var li = $5a.Draw.cardKanvas(A,P);
		if(TR[k1]){ TR[k1].appendChild(li); }
	}
	tb.appendChild(tBody);
	wrap.appendChild(tb);
	for(var k in $5a.Vs.actStatus){ 
		var spanLine = $1.q('.actStatus_'+k,tr0);
		spanLine.innerText = TR[k].childNodes.length;
	}
	return wrap;
}
,
list:function(P){
	Jq = $5a.JQ;
	var wrap = $1.t('div');
	if(Jq.A.errNo){ $ps_DB.response(wrap,Jq.A); return wrap; }
	var Tdiv = {};
	for(var k in $5a.Vs.actStatus){
		Tdiv[k] = $1.t('div',{'class':'table_covey',style:'padding:0 0 1rem;'});
		var h6 = $1.t('h4',{'class':k});
		h6.appendChild($1.t('span',''));
		h6.appendChild($1.t('span',{textNode:$5a.Vs.actStatus[k]}));
		var div = $1.t('div',{'class':'cardCanvas_drag_wrap '+$5a.putKard+k,style:'min-height:5rem; margin-left:2rem; padding-left:0.5rem;'});
		div.Odnd = {movType:'actStatus',to:k};
		h6.appendChild($1.t('span',{'class':'actTotal'}));
		var add = $5a.F.btnAdd({wboListId:null},h6,{actStatus:k});
		h6.appendChild(add);
		Tdiv[k].appendChild(h6);
		Tdiv[k].appendChild(div);
		wrap.appendChild(Tdiv[k]);
	}
	for(var a in Jq.A){ var A = Jq.A[a]; var k = A.actStatus;
		if(!Tdiv[k]){ continue; }
		if($1.q('._5a_cardIdWrap_'+A.actId,wrap)){ continue; }
		var li = $5a.Draw.lineBasic(A,P);
		li.Odnd = {actId:A.actId,from:k};
		Tdiv[k].childNodes[1].appendChild(li);
	}
	for(var k in $5a.Vs.actStatus){
		if(!Tdiv[k]){ continue; }
		var tTotal = Tdiv[k].childNodes[1].childNodes.length;
		Tdiv[k].childNodes[0].childNodes[2].innerText = ' ('+tTotal+')';
	}
	return wrap;
}
}
,
Type:{
list:function(P){
	Jq = $5a.JQ;
	var wrap = $1.t('div');
	if(Jq.A.errNo){ $ps_DB.response(wrap,Jq.A); return wrap; }
	var Tdiv = {};
	for(var k in $5a.Vs.actType){
		Tdiv[k] = $1.t('div',{style:'padding:0 0 1rem;','class':'cardCanvas_drag_wrap'});
		var h6 = $1.t('h4');
		h6.appendChild($1.t('span',{'class':'iBg iBg_'+k}));
		h6.appendChild($1.t('span',{textNode:$5a.Vs.actType[k]}));
		h6.appendChild($1.t('span',{'class':'actTotal'}));
		var add = $5a.F.btnAdd({wboListId:null},h6,{actType:k});
		h6.appendChild(add);
		var div = $1.t('div',{style:'margin-left:1rem; border-left:0.0625rem dashed #CCC;','class':'cardCanvas_drag_wrapReal '+$5a.putKard+k});
		div.Odnd = {movType:'actType',to:k};
		Tdiv[k].appendChild(h6);
		Tdiv[k].appendChild(div);
		wrap.appendChild(Tdiv[k]);
	}
	for(var a in Jq.A){ var A = Jq.A[a];
		if($1.q('._5a_cardIdWrap_'+A.actId,wrap)){ continue; }
		var li = $5a.Draw.lineBasic(A,P);
		var k1 = A.actType;
		if(Tdiv[k1]){ Tdiv[k1].childNodes[1].appendChild(li); }
	}
	if(1) for(var k in $5a.Vs.actType){
		var tTotal = Tdiv[k].childNodes[1].childNodes.length;
		if(tTotal > 0){
			Tdiv[k].childNodes[0].childNodes[2].innerText = ' ('+tTotal+')';
		}
	}
	return wrap;
}
}
,
listt:function(P){//tick ok
	Jq = $5a.JQ;
	var wrap = $1.t('div');
	if(Jq.A.errNo){ $ps_DB.response(wrap,Jq.A); return wrap; }
	var tb = ps_DOM.Tag.table([
		{textNode:'Tipo',style:'display:none',},
		{textNode:'Prioridad',style:'display:none'},
		{textNode:'','class':'expNoCol'},
		{textNode:'Asunto'},
		{textNode:'Propietario'},
		{textNode:'Responsable'},
		{textNode:'Cliente'},
		{textNode:'Vence'},
		{textNode:'Inicia'},
		{textNode:'Finaliza'},
		{textNode:'Duración'},
		{textNode:'Estado'},
		{textNode:'Creada'},
		{textNode:'Control','class':'expNoCol'},
		]
	,{tbData:{'class':'table_zh',id:'____5a_wrapList'}
	});
	var tBody = $1.t('tbody',{'class':'_5awrapAddKard_'});
	for(var a in Jq.A){ L = Jq.A[a];
		var tr1 = $5a.Draw.cardTrList(L);
		tBody.appendChild(tr1);
	}
	tb.appendChild(tBody);
	var fi = ps_DOM.Tag.tbExport(tb);
	wrap.appendChild(fi);
	$1.nullBlank = false;
	return wrap;
}
,
week:function(P){
	Jq = $5a.JQ;
	var wrap = $1.t('div');
	if(Jq.error){ $ps_DB.response(wrap,Jq.error); }
	var gooDate1 = $1.q('.gooMovDate_1');
	var gooDate2 = $1.q('.gooMovDate_2');
	var tb = $1.t('table',{'class':'table_zh',style:'min-width:119rem;'});
	var tHead = $1.t('thead');
	var tr0 = $1.t('tr');
	var tr1 = $1.t('tr');
	var Days = ['','Lunes ','Martes','Miercoles','Jueves','Viernes','Sábado','Domingo'];
	delete(Days[0]);
	var n = 0;
	var TDS = {};
	for(var dN in Days){
		var dateH = $2d.add(gooDate1.value,'+'+n+'days');
		var k = $2d.g('z',dateH); n++;
		var d_k = dateH.substring(0,4)+'_'+k;
		TDS[k] = $1.t('td',{class:'dayYear_'+d_k+' cardCanvas_drag_wrap '+$5a.putKard+d_k,style:'width:16rem;'});
		var td = $1.t('td',{style:'width:16rem;'});
		var add = $5a.F.btnAdd({btn:{textNode:' '+Days[dN]},wboListId:null},td,{doDate:dateH});
		td.appendChild(add);
		tr0.appendChild(td);
		
		TDS[k].Odnd = {movType:'doDate',to:dateH};
		tr1.appendChild(TDS[k]);
	}
	tHead.appendChild(tr0);
	tb.appendChild(tHead);
	var tBody = $1.t('tbody');
	tBody.appendChild(tr1);
	if(!Jq.A.errNo) for(var a in Jq.A){ var A = Jq.A[a];
		if($1.q('._5a_cardIdWrap_'+A.actId,tBody)){ continue; }
		var k1 = $2d.g('z',A.doDate);
		if(k1 == 'e'){ var k1 = $2d.g('z',A.dueDate); }
		else if(k1 == 'e'){ var k1 = ''; }
		A.wboId = Jq.wboId;
		P.Odnd = {fromType:A.doDate};
		var li = $5a.Draw.cardKanvas(A,P);
		li.title = 'doDate: '+A.doDate+'dueDate: '+A.dueDate+'.';
		if(TDS[k1]){ TDS[k1].appendChild(li); }
	}
	tb.appendChild(tBody);
	wrap.appendChild(tb);
	return wrap;
}
,
month:function(P){
	Jq = $5a.JQ;
	var wrap = $1.t('div');
	if(Jq.error){ $ps_DB.response(wrap,Jq.error); }
	var gooDate1 = $1.q('.gooMovDate_1');
	var gooDate2 = $1.q('.gooMovDate_2');
	var tb = $1.t('table',{'class':'table_zh',style:'min-width:119rem;'});
	var tHead = $1.t('thead');
	var tr0 = $1.t('tr');
	tr0.appendChild($1.t('td',{'textNode':'#',style:'width:1rem;'}));
	var tBody = $1.t('tbody');
	var TDS = {};
	var Days = $2d.Vs.Days;
	for(var dN in Days){ var k = Days[dN].k;
		tr0.appendChild($1.t('td',{'textNode':Days[dN].t,style:'width:16rem;','class':'weekDay_'+k}));
	}
	var mI = $2d.g('n',gooDate1.value);
	var date1 = $2d.weekB(gooDate1.value);
	var date2 = $2d.weekE(gooDate2.value);
	var lastWY = '';
	var tdWeek = {};
	var befW = 1;
	Wd = $2d.Draw.Wd({date1:date1,date2:date2});
	for(var w in Wd){
		var tr = $1.t('tr');
		var wT = (w>52) ? 1 : w;
		tr.appendChild($1.t('td',{textNode:wT}));
		for(var dN in Days){
		var tD = Days[dN]; var k = tD.k;
		var Wd_w_k = (Wd[w][k]) ? Wd[w][k] : {};
		var text = (Wd_w_k.date) ? $2d.f(Wd_w_k.date,'mmm d') : '';
		var bgColor = (mI == Wd_w_k.n) ? '': ' background-color:#e6e6e6;';
		var zk = Wd_w_k.y+'_'+Wd_w_k.z;
		var classZ = (Wd_w_k.z) ? 'yearDayTd_'+zk : '';
			var td = $1.t('td',{'class':classZ+' cardCanvas_drag_wrap '+$5a.putKard+zk,style:'height:10rem;'+bgColor});
			td.Odnd = {movType:'doDate',to:Wd_w_k.date};
			var add = $5a.F.btnAdd({btn:{textNode:' '+text},wboListId:null},td,{doDate:Wd_w_k.date});
			td.appendChild(add);
			tr.appendChild(td);
		}
		tBody.appendChild(tr);
	}
	tHead.appendChild(tr0);
	tb.appendChild(tHead);
	tb.appendChild(tBody);
	if(!Jq.A.errNo){
		for(var a in Jq.A){ var A = Jq.A[a];
			if($1.q('._5a_cardIdWrap_'+A.actId,tBody)){ continue; }
			var ky = A.doDate;
		var k1 = $2d.g('z',A.doDate);
		if(k1 == 'e'){ var k1 = $2d.g('z',A.dueDate); var ky = A.dueDate; }
		ky = (ky) ? ky.substring(0,4) : '';
		var zk = ky+'_'+k1;
		var tdDay = $1.q('.yearDayTd_'+zk,tBody);
		A.wboId = Jq.wboId;
		var li = $5a.Draw.cardKanvas(A,P);
		if(tdDay){ tdDay.appendChild(li); }
	}
	}
	wrap.appendChild(tb);
	return wrap;
}
,
timeline:function(P){
	Jq = $5a.JQ;
	var wrap = $1.t('div');
	if(Jq.error){ $ps_DB.response(wrap,Jq.error); }
	var wrap = $1.t('div',{'class':'tLine'});
	var grip = $1.t('div',{'class':'tLine_grip'});
	var gripScale = $1.t('div',{'class':'tLine_grip_scale'});
	gripScale.appendChild($1.t('div',{'class':'tLine_grip_taskName',textNode:'Actividad'}));
	gripScale.appendChild($1.t('div',{'class':'tLine_grip_perc',textNode:'%'}));
	gripScale.appendChild($1.t('div',{'class':'tLine_grip_prio',textNode:'Prioridad',abbr:2}));
	grip.appendChild(gripScale);
	var gripData = $1.t('div',{'class':'tLine_grip_data'});
	grip.appendChild(gripData);
	var task = $1.t('div',{'class':'tLine_task'});
	var taskScale = $1.t('div',{'class':'tLine_task_scale',style:'position:relative;'});
	var taskData = $1.t('div',{style:'position:relative;'});
	task.appendChild(taskScale);
	task.appendChild(taskData);
	wrap.appendChild(grip);
	wrap.appendChild(task);
	if(!Jq.A.errNo){
		var tdW = 4.33;
		var tdR = 4.375;//70px
		var minD = $2d.toTime(Jq.minDate);
		var d = $2d.diff({date1:Jq.minDate, date2:Jq.maxDate});
		if(d == 0){ $ps_DB.response(wrap,{errNo:2,text:'No se encontraron tarjetas con fechas para el timeline.'},wrap); }
		else{
			var monK = {};
			var datePosic = {};
			var fullH = (Jq.uniqueTask*1+1)*2.25;
			for(var i=0; i<d; i++){
				var date = $2d.add(minD,'+'+i+'days','object');
				var mmm_d = $2d.f(date.Ymd,'mmm d');
				if(!monK[date.mR]){ monK[date.mR] = {l:0,p:0}; }
				monK[date.mR].l += 1;
				date.posic = i;
				datePosic[date.Ymd] = date;
				var leftP = (tdR*i);
				taskScale.appendChild($1.t('div',{'class':'tLine_scale_cell',style:'left:'+leftP+'rem; height:'+fullH+'rem',textNode:mmm_d}));
			}
			var trPm = 0;
			var ntr = 0;
			var tdH = 1;
			for(var a in Jq.A){ var A = Jq.A[a];
				if($2d.is0(A.doDate) && $2d.is0(A.dueDate)){ continue; }
				if($1.q('._5a_cardIdWrap_'+A.actId,wrap)){ continue; }
				var date1 = $2d.toTime(A.doDate);
				var date2 = $2d.toTime(A.endDate);
				var days = Math.ceil((date2-date1)/$2d.Vs.day1);
				var wi = (days <= 0) ? 1 : days+1;
				wi = (wi)*tdR;
				var left2 = (datePosic[A.doDate]) ? datePosic[A.doDate].posic : 0;
				left = (left2*(tdR))+0.0625;
				var rowGrip = $1.t('div',{'class':'tLine_row _5a_cardIdWrap_'+A.actId,title:A.doDate+', '+A.endDate,keyo:A.keyO,actId:A.actId});
				var percTotal = (A.ckTotal>0) ? Math.ceil((A.ckComplet/A.ckTotal)*100) : '';
				var actName = $1.t('div',{'class':'tLine_grip_taskName',style:'cursor:pointer;'});
				var iFocus = '_5a_cardId_focus_'+A.actId;
				actName.iFocus = iFocus;
				actName.onclick = function(){ $js.focus('.'+this.iFocus); }
				actName.appendChild($1.t('span',{'class':'iBg iBg_'+A.actType}));
				actName.appendChild($1.t('span',{textNode:A.actAsunt,abbr:27}));
				rowGrip.appendChild(actName);
				var perc = $1.t('div',{textNode:percTotal,'class':'tLine_grip_perc'});
				rowGrip.appendChild(perc);
				var prio = $1.t('div',{'class':'tLine_grip_prio fa fa_prio_'+A.actPriority});
				rowGrip.appendChild(prio);
				gripData.appendChild(rowGrip);
				var rowT = $1.t('div',{'class':'tLine_task_row '});
				var div = $1.t('div',{style:' background-color:#3C9445; position:absolute; left:'+left+'rem; width:'+wi+'rem;',textNode:'\u00A0',title:A.doDate+', '+A.endDate});
				rowT.appendChild(div);
				taskData.appendChild(rowT);
				if(A.ckTotal>0){
					var wiP = wi*percTotal/100;
					var divComplet = $1.t('div',{'class':'tLine_task_row _5ck_barCkComplet_'+A.actId,style:' background-color:#65C16F; position:absolute; left:'+(left)+'rem; width:'+wiP+'rem;',textNode:'\u00A0'});
					divComplet.maxWidth = wi; //para percn
					rowT.appendChild(divComplet);
				}
				if(!$2d.is0(A.dueDate)){
					var leftDue = (datePosic[A.dueDate]) ? datePosic[A.dueDate].posic : 0;
					leftDue = leftDue*tdR+0.0625;
					var divDue = $1.t('div',{style:'position:absolute; left:'+leftDue+'rem; width:'+tdR+'rem; background-color:#E63030;',textNode:'\u00A0',title:A.dueDate});
					rowT.appendChild(divDue);
				}
				var open = $1.t('div',{'class':'tLine_task_row tLine_task_textTop '+iFocus,style:' background-color:transparent; position:absolute;  left:'+(left)+'rem; width:'+wi+'rem;',textNode:A.actAsunt,title:'Days: '+days+', '+A.doDate+', '+A.endDate});
					open.D = {reLoad:true, actId:A.actId};
					open.onclick = function(){
						$5a.O.winFloat(this.D,P);
					}
					rowT.appendChild(open);
			}
		}
	}
	
	return wrap;
}
,
weekHours:function(P){
	if(P.reLoad == true){
		var vPost = $1.G.inputs($5a.activityFormsVars,'jsFiltVars')+'ordBy='+ps_DOM.gImp.ordBys();
		$ps_DB.get({file:'GET '+$5a.api+'V.weekHours', inputs:vPost,
		func:function(Js){ $5a.V.weekHours(Js); }
		});
		return false;
	}
	var cont = document.getElementById('activityWrapList');
	ps_DOM.clear(cont);
	if(P.error){ $ps_DB.response(cont,P.error); }
	var tb = $1.t('table',{'class':'table_zh table_nopad',style:'min-width:1024px;'});
	var tHead = $1.t('thead');
	var tr0 = $1.t('tr');
	var heiC = 120;
	var heiC_2 = heiC/2;
	var minHei = 5*heiC;
	var heiWrap = heiC*24-minHei;
	var Days = ['','Lunes','Martes','Miercoles','Jueves','Viernes','Sábado'];
	delete(Days[0]);
	tr0.appendChild($1.t('td',{'textNode':''}));
	for(var dN in Days){
		tr0.appendChild($1.t('td',{'textNode':Days[dN],style:'width:15%;'}));
	}
	tHead.appendChild(tr0);
	tb.appendChild(tHead);
	var tBody = $1.t('tbody');
	var tr = $1.t('tr');
	var td = $1.t('td',{style:' width:40px;'});
	var tdDivWrap = $1.t('div',{style:'position:relative; height:'+(heiWrap)+'px;'});
	for(var h=5; h<24; h++){
		for(var m=0; m<=30; m+=30){
		var posTop = (h*heiC)+(m/60*heiC)+1-minHei;
		if(h==5 && m==0){ posTop = 0; }
		var tHour = (h<9) ? '0'+h+':' : h+':';
		tHour += (m<9) ? '0'+m : m;
		var lin = $1.t('div',{style:'position:absolute; top:'+posTop+'px; border-top:1px solid #000; width:100%; height:'+heiC_2+'px;',textNode:tHour});
			tdDivWrap.appendChild(lin);
		}
	}
	td.appendChild(tdDivWrap);;
	tr.appendChild(td);
	for(var dN in Days){
			var tdCont = $1.t('td',{'class':'calendMonthWrap'});
			var td = $1.t('div',{'class':'contList'});
			var tdDivWrap = $1.t('div',{style:'position:relative; height:'+(heiWrap)+'px'});
			tdCont.appendChild($1.t('span',{'textNode':''}));
			if(P.DATA[dN] != undefined){
				for(var li in P.DATA[dN]){
					var datA = P.DATA[dN][li];
					if(datA.actId == undefined){ continue; }
					var cls = (datA.BOL && datA.BOL.status) ? datA.BOL.status : '';
					var posTop = heiC*datA.POSIC.top-minHei;
					var hei = heiC*datA.POSIC.height;
					var div = $1.t('div',{'class':'contItem '+cls,style:'position:absolute; top:'+posTop+'px; height:'+hei+'px;'});
					var asunt = $1.t('div',{'class':'actAsunt '});
					asunt.appendChild($1.t('span',{'class':'spanLine noBorder iBg iBg_'+datA.actType,'textNode':datA.doTime}));
					asunt.actId = datA.actId;
					if(datA.actType != 'birthDay'){
					asunt.onclick = function(){ $5a.O.winFloat({reLoad:true, actId:this.actId}); }
					}
					asunt.appendChild($1.t('span',{'textNode':ps_Text.limit(datA.actAsunt,16)}))
					div.appendChild(asunt);
					tdDivWrap.appendChild(div);
					td.appendChild(tdDivWrap);
					tdCont.appendChild(td);
				}
			}
			tr.appendChild(tdCont);
		}
		tBody.appendChild(tr);
	tb.appendChild(tBody);
	cont.appendChild(tb);
}
,
daily:function(P){
	if(P.reLoad == true){
		var vPost = $1.G.inputs($5a.activityFormsVars,'jsFiltVars')+'&ordBy='+ps_DOM.gImp.ordBys();
		$ps_DB.get({file:'GET '+$5a.api+'V.daily', inputs:vPost,
		func:function(Js){ $5a.V.daily(Js); }
		});
		return false;
	}
	var cont = document.getElementById('activityWrapList');
	ps_DOM.clear(cont);
	if(P.errNo == 2== 2){ $ps_DB.response(cont,P); return false; }
	var tb = $1.t('table',{'class':'table_zh table_x100'});
	var tBody = $1.t('tbody');
	var D1 = P.DATA[0];
	var D2 = P.DATA[1];
		var tr = $1.t('tr');
		var td = $1.t('td',{'class':'calendDiaryWrap'});
			td.appendChild($1.t('div',{'class':'day iBg iBg_date','textNode':P.DAYS[0]}));
		D1 = (P.DATA[0]) ? D1 : D2;
		for(var l in D1){ var P1 = D1[l];
				var li = $1.t('div',{'class':'item'});
				var cls = (P1.BOL && P1.BOL.status) ? P1.BOL.status : '';
				li.appendChild($1.t('span',{'class':'spanLine '+cls,'textNode':P1.doTime+' - '+P1.endTime}));
				li.appendChild($1.t('span',{'class':'spanLine noBorder iBg iBg_'+P1.actType,'textNode':P1.actAsunt}));
				li.actId = P1.actId;
				li.onclick = function(){ $5a.O.winFloat({reLoad:true, actId:this.actId}); }
				td.appendChild(li);
			}
		tr.appendChild(td);
		/*var td = $1.t('td',{'class':'calendDiaryWrap'});
			td.appendChild($1.t('div',{'class':'day iBg iBg_date','textNode':P.DAYS[1]}));
			for(var l in D2){ var P1 = D2[l];
				var li = $1.t('div',{'class':'item'});
				li.appendChild($1.t('span',{'class':'spanLine','textNode':P1.doTime+' - '+P1.endTime}));
				li.appendChild($1.t('span',{'class':'spanLine noBorder iBg iBg_'+P1.actType,'textNode':P1.actAsunt}));
				td.appendChild(li);
			}
		tr.appendChild(td);
		*/
		tBody.appendChild(tr);
	tb.appendChild(tBody);
	cont.appendChild(tb);
}
}
,
drawLeyend:function(){
	var cont = ps_DOM.get('activityWrapLeyend');
	var ul = $1.t('table');
	var tr = $1.t('tr');
	var LEY = [{text:'Próximo','class':'okDate'}, 
	{text:'En Proceso','class':'onProccess'}, 
	{text:'Finalizado y No completado','class':'doDueNoCompleted'},
	{text:'Vencido','class':'dueDate'}];
	for(var i in LEY){
		tr.appendChild($1.t('td',{'class':'spanLine '+LEY[i]['class'],textNode:LEY[i].text}));
		//tr.appendChild($1.t('td',{textNode:LEY[i].text}));
	}
	ul.appendChild(tr);
	ps_DOM.clear(cont);
	cont.appendChild(ul);
}
,
S:{
updField:function(P){
	//P=movType,to, actId, callf, keyO
	var call = (P.callf);
	var vPost = '&actId='+P.actId+'&movType='+P.movType+'&to='+P.to+'&to2='+P.to2;
	$ps_DB.get({file:'PUT '+$5a.api+'S.updField', inputs:vPost, 
	func:function(Jq){
		if(P.func){ P.func(Jq); }
		$2io.worksp.emit({objType:$o.T.activity, objRef:P.actId,movType:P.movType, to:P.to, to2:P.to2});
	}
	});
}
,
moveCard:function(P){//recibido de drap(o)
	var call = (P.callf);
	var vPost = 'movType='+P.movType+'&actId='+P.actId+'&from='+P.from+'&to='+P.to;
	$ps_DB.get({file:'PUT '+$5a.api+'S.moveCard', inputs:vPost, 
	func:function(Jq){
		//P.O.i desd $1.dnd para actualizwr move
		if((P.movType).match(/^(actType|actPriority|actStatus|userAssg|doDate)$/)){
			var ch = {}; ch[P.movType] = P.to;
			$Co._u.a({actId:P.actId},ch);
			$2io.worksp.emit({objType:$o.T.activity, objRef:P.actId,movType:P.movType, from:P.from, to:P.to});
		}
		else if(1){//siempre al mover
			if(P.keyO && $5a.JQ.A[P.keyO]){
				if(P.O && P.O.i){ P.O.i.Odnd.from = P.to; }
				if(P.movType == 'wboList'){
					if(!$5a.JQ.WbA[P.to]){  $5a.JQ.WbA[P.to] = {}; }
					$5a.JQ.WbA[P.to][P.keyO] = P.keyO; 
					delete($5a.JQ.WbA[P.from][P.keyO]);
				}
				$2io.wbo.emit(P,{keyO:P.keyO,keyO_mx:Jq.keyO_mx});
			}
		}
		else if(P.func){ P.func(Jq);}
		else if(P.callf =='board'){ $5b.List.get({}); }
		else if(P.callf =='activity'){ $5a.reload({}); }
	}
	});
}
}
,
Draw:{
	actAsunt:function(D,P,C1){
		var asunt = $1.t('div',{'class':'_5afie_actAsunt cardCv_asunt actAsunt'});
		asunt.D = {reLoad:true, actId:D.actId};
		asunt.onclick = function(){ $5a.O.winFloat(this.D,{keyO:D.keyO}); }
		if(!C1 || C1.vtype != 'type'){
			asunt.appendChild($1.t('span',{'class':'_5afie_actType iBg iBg_'+D.actType}));
		}
		asunt.appendChild($1.t('span',{textNode:' '+D.actAsunt}));
		return asunt;
	}
	,
	bottomInfo:function(cardBot,D,C1){//info similar bottom
	//usando _5afie_fieldName
	var C1 = (C1)?C1:{};
		var cardBot = $1.t('div',{'class':'cardCv_bottom'});
		var wi = (C1.vtype != 'status' && D.actStatus && D.actStatus !='none') ? '' : 'display:none;';
		var cPrio = $1.t('span',{'class':'_5afie_actStatus iBg iBg_actStatus_'+D.actStatus,title:$5a.Vs.actStatus[D.actStatus],textNode:'\u00A0',style:wi});
		cardBot.appendChild(cPrio);
		var wi = (C1.vtype != 'prio' && D.actPriority && D.actPriority !='none') ? '' : 'display:none;';
		var cPrio = $1.t('span',{'class':'_5afie_actPriority fa fa_prio_'+D.actPriority,title:$5a.Vs.actPriority[D.actPriority],style:wi});
			cardBot.appendChild(cPrio);
		var wi = (D.dueDate && !$2d.is0(D.dueDate)) ? '' : 'display:none;';
		var cDue = $1.t('span',{'class':'_5afie_dueDate cardCv_dueDate fa fa_deadLine',textNode:$2d.f(D.dueDate),title:'Vencimiento de la Actividad',style:wi});
		cardBot.appendChild(cDue);
		var wi = (D.userAssgName)?'':'display:none;';
		cardBot.appendChild($1.t('span',{'class':'_5afie_userAssg fa fa_user',title:D.userAssgName,style:wi}));
		var wi = (D.actNote && D.actNote !='')?'':'display:none;';
		var span = $1.t('span',{'class':'_5afie_actNote cPointer fa fa_note',title:'Actividad con Descripción',style:wi});
		span.onclick = function(){ $1.Win.title($1.t('div',{textNode:D.actNote,style:'font-size:0.8rem;'}),{pare:cardBot});}
		cardBot.appendChild(span);
		$5a.Drawi.uMembers(D,cardBot);
		$5a.Drawi.oSrc(D,cardBot);
		$5a.Drawi.commTotal(D,cardBot);
		$5a.Drawi.fileTotal(D,cardBot);
		$5a.Drawi.ckOpen(D,cardBot);
		
		var wi =(D.actPlace && D.actPlace !='')?'':'display:none;'
		cardBot.appendChild($5a.Draw.actPlace(D,{g:'button',style:wi}));
		if((C1.vtype != 'month' && C1.vtype != 'week') && D.doDate && !$2d.is0(D.doDate)){
			cardBot.appendChild($1.t('div',{'class':'_5afie_doDate fa fa_date',textNode:$2d.f(D.doDate),title:'Fecha de Realización'}));
		}
		if(cardBot.childNodes.length ==0){ return $1.t('textNode',''); }
		return cardBot;
	}
	,
	cardKanvas:function(D,P){
		var card = $1.t('div',{'class':'_o_activity_'+D.actId+' cardCanvas cardCanvas_drag_item _5a_cardIdWrap_'+D.actId,id:'_5a_cardIdWrap_'+D.actId+'_'+D.wboListId,keyo:D.keyO,actId:D.actId});
		var froM = (P.Odnd && P.Odnd.fromType) ? P.Odnd.fromType : D.wboListId;
		card.Odnd = {actId:D.actId,from:froM}
		var cardTop = $1.t('div',{'class':'cardCv_Top',style:'float:right;'});
		var cMain = $1.t('input',{type:'button','class':'cardCv_main iBg iBg_menuList',title:'Menú',style:''}); cMain.D = D;
		var cMain = $1.t('button',{'class':'cardCv_main faBtn fa fa_3pointsv',title:'Menú',style:''}); cMain.D = D;
		cMain.onclick = function(){ $5a.O.menu(this.D,this.parentNode,{callf:'board'}); }
		cardTop.appendChild(cMain);
		card.appendChild(cardTop);
		var asunt = $5a.Draw.actAsunt(D,{vtype:$4.p.vtype});
		card.appendChild(asunt);
		var cardBot = $5a.Draw.bottomInfo(cardBot,D,{vtype:$4.p.vtype});
		card.appendChild(cardBot);
		return card;
	}
	,
	cardTrList:function(D){
		$1.nullBlank = '';
		var tr1 = $1.t('tr',{'class':'_o_activity_'+D.actId,id:'_5a_cardIdWrap_'+D.actId,keyo:D.keyO});
		var actType = $5a.Vs.actType[D.actType];
		var actPrio = $5a.Vs.actPriority[D.actPriority];
		tr1.appendChild($1.t('td',{textNode:actType,style:'display:none;'}));
		tr1.appendChild($1.t('td',{textNode:actPrio,style:'display:none;'}));
		var actType2 = actReason = '';
		var td0 = $1.t('td');
		td0.appendChild($1.t('span',{'class':'_5afie_actPriority fa fa_prio_'+D.actPriority,title:actPrio}));
		tr1.appendChild(td0);
		var tdAsu = $1.t('td');
		var aView = $5a.Draw.actAsunt(D,false,null);
		tdAsu.appendChild(aView);
		tr1.appendChild(tdAsu);
		tr1.appendChild($1.t('td',{textNode:D.userName}));
		tr1.appendChild($1.t('td',{textNode:D.userAssgName}));
		var td = $1.t('td');
		if(D.cardName){ td.D = {realText:D.cardName};
			var oTag = $M.href({href:'bussPartnerProfile','!!':D.cardId,textNode:D.cardName});
			td.appendChild(oTag);
		}
		tr1.appendChild(td);
		$2d.fdef = 'mmm d H:iam';
		tr1.appendChild($1.t('td',{textNode:$2d.f(D.dueDate,'',{is0:' '}),D:{realText:D.dueDate}}));
		tr1.appendChild($1.t('td',{textNode:$2d.f(D.doDate,'',{is0:' '}),D:{realText:D.doDate}}));
		tr1.appendChild($1.t('td',{textNode:$2d.f(D.endDate,'',{is0:' '}),D:{realText:D.dueDate}}));
		tr1.appendChild($1.t('td',{textNode:D.timeDuraText}));
		var td = $1.t('td');
		var spa = $1.t('span',{'class':'_5afie_actStatus iBg iBg_actStatus_'+D.actStatus});
		td.appendChild(spa);
		td.appendChild($1.t('span',{'class':'_5afie_actStatus_text', textNode:$5a.Vs.actStatus[D.actStatus]}));
		tr1.appendChild(td);
		tr1.appendChild($1.t('td',{textNode:$2d.f(D.dateC,'',{is0:' '}),D:{realText:D.dateC}}));
		var tdCont = $1.t('td');
		$5a.Drawi.put(D,'uMembers,oSrc,commTotal,fileTotal,ckOpen',tdCont);
		tr1.appendChild(tdCont);
		$2d.fdef = '';
		return tr1;
	}
	,
	lineBasic:function(D,P){var P=(P)?P:{};
		var li = $1.t('div',{'class':'_o_activity_'+D.actId+' _5a_cardIdWrap_'+D.actId+' kardLine_wrap cardCanvas_drag_item','id':'_5a_cardIdWrap_'+D.actId+'_'+D.wboListId,keyo:D.keyO,actId:D.actId});
		var Odnd = {};
		if($4.p.vtype == 'board'){ Odnd = {movType:'wboList', from:D.wboListId}; }
		else if($4.p.vtype == 'prio'){ Odnd = {movType:'actPriority', from:D.actPriority}; }
		else if($4.p.vtype == 'type'){ Odnd = {movType:'actType', from:D.actType}; }
		li.Odnd = $js.push({actId:D.actId},Odnd);
		var asunt = $5a.Draw.actAsunt(D,P,{vtype:$4.p.vtype});
		li.appendChild(asunt);
		var cardBot = $5a.Draw.bottomInfo(cardBot,D,{vtype:$4.p.vtype});
		li.appendChild(cardBot);
		return li;
	}
	,
	actPlace:function(D,P){P=(P)?P:{};
		if(P.g == 'button'){
			var place = $1.t('span',{'class':'iBg iBg_visit',title:'Clic para abrir ubicación aproximada...',style:P.style});
		}
		else{
			var place = $1.t('div');
			place.appendChild($1.t('span',{'class':'iBg iBg_visit'}));
			place.appendChild($1.t('span',{textNode:'Lugar: '}));
			place.appendChild($1.t('span',{textNode:D.actPlace}));
		}
		place.place = D.actPlace;
		place.onclick = function(){
			window.open('https://google.com/maps/search/'+this.place);
		}
		return place;
	}
	,
	gooDate:function(wrapCont,Jq_P){//$5a.Draw.gooDate();
	if($1.q('.gooMovDate_wrap',wrapCont)){ return true; }
	var goTo = $1.t('div',{'class':'gooMovDate_wrap'});
	var date1 = $1.t('input',{type:'hidden','class':'jsFiltVars gooMovDate_1',name:'doDate[0]'});
	var date2 = $1.t('input',{type:'hidden','class':'jsFiltVars gooMovDate_2',name:'doDate[1]'});
	var date1Text = $1.t('span',{id:'actDoDate0Text','class':'gooMovDateText_1'});
	var date2Text = $1.t('span',{id:'actDoDate1Text','class':'gooMovDateText_2'});
	
	var today = $1.t('input',{type:'button', 'class':'googBtn',value:'Hoy'});
	today.onclick = function(){ Jq_P.P.movDateType = 'today';
	$5a.loadType(Jq_P); };
	goTo.appendChild(today);
	var toBef = $1.t('input',{type:'button', 'class':'googBtn',value:'≪'});
	toBef.onclick = function(){ Jq_P.P.movDateType = '--';
	$5a.loadType(Jq_P); };
	goTo.appendChild(toBef);
	var toAft = $1.t('input',{type:'button', 'class':'googBtn',value:'≫'});
	toAft.onclick = function(){ Jq_P.P.movDateType = '++';
	$5a.loadType(Jq_P); };
	goTo.appendChild(toAft);
	goTo.appendChild(date1Text);
	goTo.appendChild($1.t('span',' – '));
	goTo.appendChild(date2Text);
	goTo.appendChild(date1); goTo.appendChild(date2);
	wrapCont.appendChild(goTo);
}
}
,
Drawi:{
	put:function(D,sels,cardBot){
		var s = sels.split(',');
		for(var i in s){
			switch(s[i]){
				case 'uMembers' : $5a.Drawi.uMembers(D,cardBot); break;
				case 'oSrc' : $5a.Drawi.oSrc(D,cardBot); break;
				case 'commTotal' : $5a.Drawi.commTotal(D,cardBot); break;
				case 'fileTotal' : $5a.Drawi.fileTotal(D,cardBot); break;
				case 'ckOpen' : $5a.Drawi.ckOpen(D,cardBot); break;
			}
		}
	}
	,
	uMembers:function(D,cardBot){
		var wi = (D.uMembers>0)?'':'display:none;';
		var span = $1.t('span',{'class':'_5afie_uMembers fa fa_ugroup',textNode:' '+D.uMembers,title:'Miembros de la Tarjeta',style:wi});
		if(cardBot){ cardBot.appendChild(span); } else{ return span; }
	}
	,
	oSrc:function(D,cardBot){
		var wi = (D.oSrc>0)?'':'display:none;';
		var oSrc = $1.t('button',{'class':'_5afie_oSrc fa faBtn fa_link',textNode:' '+D.oSrc,title:'Relacionado con la Tarjeta',style:wi});
		oSrc.onclick = function(){ $o.Src.titleGet({objpType:'activity',objpRef:D.actId},this.parentNode); }
		var span = $1.t('span'); span.appendChild(oSrc);
		if(cardBot){ cardBot.appendChild(span); } else{ return span; }
	}
	,
	commTotal:function(D,cardBot){
		var wi = (D.commTotal >0)?'':'display:none;';
		cardBot.appendChild($1.t('span',{'class':'_5afie_commTotal fa fa_comment',textNode:D.commTotal,abbr:3,style:wi}));
	}
	,
	fileTotal:function(D,cardBot){
		var wi = (D.fileTotal>0)?'':'display:none;';
		cardBot.appendChild($1.t('span',{'class':'_5afie_fileTotal fa fa_attach',textNode:D.fileTotal,abbr:3,style:wi}));
	}
	,
	ckOpen:function(D,cardBot){
		var ckOpen = D.ckTotal-D.ckComplet*1;
		var wi = (ckOpen>0)?'':'display:none;';
		//._5ck_countComplet_ para poner 3/4
		var span = $1.t('span',{'class':'_5afie_ckTotal cPointer fa fa_checkList '+D.actId,textNode:ckOpen,style:wi,title:'Puntos Pendientes'});
		cardBot.appendChild(span);
	}
}
,
O:{
	win:function(P,wrap){
		var objTy = {targetType:'activity',targetRef:P.actId,MR:{userAssg:P.userAssg, userAssgName:P.userAssgName, cardId:P.cardId, cardName:P.cardName},FR:{activity:P.actId,bussPartner:$o.T.bussPartner}};
		var objCt = {targetType:'activity',targetRef:P.actId,FR:{activity:P.actId}};
		var objTyFiles = $js.push({'ADMS_uploadFilesSubDir':'boards/'},objCt);
		var objTypC = $js.push({contextType:'wboList',contextRef:P.wboListId},objCt);
		var wrapMenu = $1.t('div');
		var P_Log = {reload:true, objType:$5a.objType, objRef:P.actId};
		var P_members = {reload:true, objType:$5a.objType, objRef:P.actId};
		var P_Src = {reload:true, objpType:$5a.objType, objpRef:P.actId};
		var liA = {};
		if(1 || $0s.ocardSocId == 2){
			liA[0] = {li:{textNode:'\u00A0','class':'fa fa_user',winClass:'winMembers', func:$o.Members.get, funcPars:P_members}};
			liA[1] = {li:{textNode:'\u00A0','class':'fa fa_link',winClass:'winoSrc', func:function(){ $o.Src.get(P_Src); } }};
		}
		var Opts = [{},{},
		{li:{textNode:'Comentarios','class':'active fa fa_comment',winClass:'win5c'}},
		{li:{textNode:'Archivos','class':'fa fa_fileUpd',winClass:'win5f', func:$3.F.$5f, funcPars:objCt}},
		{li:{textNode:'CheckList','class':'fa fa_checklist',winClass:'win5ck_R', func:$5b.R.$5a2.get, funcPars:objTyFiles}},
		{li:{textNode:'Tableros','class':'fa fa_kanban',winClass:'win5b_R', func:$5b.R.$5a.get, funcPars:P}},
		{li:{textNode:'Historial','class':'fa fa_history',winClass:'win6Obj', func:$3.F.$6Obj, funcPars:P_Log}}
		];
		if(liA[0]){ Opts = $js.push(Opts,liA); } else{ delete(Opts[0]); delete(Opts[1]); }
		var menu = ps_DOM.Menu.inLine(Opts,wrap);
		wrapMenu.appendChild(menu);
		wrapMenu.appendChild($3.F.$5c(objTypC));
		wrapMenu.appendChild($3.F.$5f(objTyFiles,'noLoad'));
		wrapMenu.appendChild($5b.R.$5a.get(P,'noLoad'));
		wrapMenu.appendChild($5b.R.$5ck.get(P,'noLoad'));
			wrapMenu.appendChild($3.F.$6Obj(P_Log,'noLoad'));
		if(1 || $0s.ocardSocId == 2){
			wrapMenu.appendChild($o.Members.get(P_members,'noLoad'));;
			wrapMenu.appendChild($o.Src.get(P_Src,'noLoad'));;
		}
		return wrapMenu;
	}
	,
	winFloat:function(Jqp,P1){ var P1 = (P1) ? P1 : {};
	var wrap = $1.t('div');
	$ps_DB.get({file:'GET '+$5a.api+'O.float', inputs:'actId='+Jqp.actId, 
		func:function(Jq){ 
	var P = (Jq.DATA)?Jq.DATA:{};
	var wrapBar1 = $1.t('div',{style:'padding:0.625rem; background:#F6F6F6; border-bottom:0.0625rem solid #DDDDDD; margin-bottom:0.375rem; font-size:0.6875rem;'});
	var cMain = $1.t('button',{'class':'faBtn fa fa_3pointsv',title:'Menú',style:'position:relative; right:0; top:0;'}); 
	cMain.D = P; cMain.D.__omit = {copy:true};
	cMain.onclick = function(){ $5a.O.menu(this.D,this.parentNode,{zIndex:1,callf:P1.callf}); }
	wrapBar1.appendChild(cMain);
	var selType = ps_DOM.Tag.divSelect({sel:{}, opts:$5a.Vs.actType, selected:P.actType,iBgOpts:'',classWrap:'input',
	func:function(This){ $5a.S.updField({keyO:P1.keyO, movType:'actType', to:This.k, actId:P.actId, callf:P1.callf}); }
	});
	wrapBar1.appendChild(selType);
	var selStatus = ps_DOM.Tag.divSelect({sel:{},classBtn:'boxi1', opts:$5a.Vs.actStatus, selected:P.actStatus,iBgOpts:'actStatus_',classWrap:'input',func:function(This){ $5a.S.updField({keyO:P1.keyO, movType:'actStatus',to:This.k, actId:P.actId, callf:P1.callf}); }});
	wrapBar1.appendChild(selStatus);
	var selPrio = ps_DOM.Tag.divSelect({sel:{'class':'jsFields',name:'actPriority'}, classBtn:'boxi1', opts:$5a.Vs.actPriority, selected:P.actPriority,clsFa:'prio_',classWrap:'input',func:function(This){ $5a.S.updField({keyO:P1.keyO, movType:'actPriority', to:This.k, actId:P.actId, callf:P1.callf}); }});
	wrapBar1.appendChild(selPrio);
	var userAssg = $1h.userAssg_inline(P,{
	func:function(JqU){ JqU =(JqU)?JqU:{};
		
		$5a.S.updField({keyO:P1.keyO, movType:'userAssg', to:JqU.userId, to2:JqU.userName, actId:P.actId, callf:P1.callf})
	}});
	wrapBar1.appendChild(userAssg);
	wrap.appendChild(wrapBar1);
	var wrapTop = $1.t('div',{style:'padding:10px; background:#F6F6F6; border-bottom:1px solid #DDDDDD;'});
	wrap.appendChild(wrapTop);
	var h5 = $1.t('h5');
	h5.appendChild($1.t('textNode',P.actAsunt));
	wrapTop.appendChild(h5);
	var wrapBar = $1.t('div',{style:'padding:0.625rem; background:#F6F6F6; border-bottom:0.0625rem solid #DDDDDD; margin-bottom:0.375rem; font-size:0.6875rem;'});
	wrapBar.appendChild($1.t('b',{textNode:'Programada: '}));
	wrapBar.appendChild($1.t('span',{textNode:P.doDate}));
	if(P.actType && !(P.actType).match(/^(email|deadLine)$/)){
		var wxn = $1.t('div',{'class':'wrapx4'});
		wrapBar.appendChild($1.t('b',{textNode:' | Duración: '}));
		wrapBar.appendChild($1.t('span',{textNode:P.timeDuraText}));
	}
	wrapBar.appendChild($1.t('b',{textNode:' | Vence: '}));
	wrapBar.appendChild($1.t('span',{textNode:P.dueDate}));
		wrap.appendChild(wrapBar);
	if(P.actPlace){
		var wrapOt = $1.t('div',{style:'padding:0.625rem; background:#F6F6F6; border-bottom:0.0625rem solid #DDDDDD; margin-bottom:0.375rem; font-size:0.6875rem;'});
		var place = $1.t('div');
		place.appendChild($1.t('span',{'class':'iBg iBg_visit',textNode:' Lugar: '}));
		place.appendChild($1.t('span',{textNode:P.actPlace}));
		place.place = P.actPlace;
		place.onclick = function(){
			window.open('https://google.com/maps/search/'+this.place);
		}
		wrapOt.appendChild(place);
		wrap.appendChild(wrapOt);
	}
	if(P.actNote != ''){
		wrap.appendChild($1.t('p',{'class':'textarea', textNode:P.actNote}));
	}
	
	var wrapMenu = $5a.O.win(P,wrap);
	wrap.appendChild(wrapMenu);
	var wrapInfo = $1.t('div',{style:'padding:0.625rem; background:#F6F6F6; border-bottom:0.0625rem solid #DDDDDD; margin-bottom:0.375rem; font-size:0.6875rem;'});
	wrapInfo.appendChild($1.t('b','# '+P.actId));
	wrapInfo.appendChild($1.t('span',{textNode:', Creado por'}));
	wrapInfo.appendChild($1.t('b',' '+P.userName));
	wrapInfo.appendChild($1.t('span',{textNode:' el '+$2d.f(P.dateC,'mmm d H:iam',{is0:'?'})+' | '}));
	wrapInfo.appendChild($1.t('span',{'class':'fa fa_checkList'}));
	wrapInfo.appendChild($1.t('span',{textNode:$2d.f(P.completedAt,'mmm d H:iam',{is0:'No'})}));
	wrap.appendChild(wrapInfo);
	}
	});
	var wrapBk = $1.Win.openBk(wrap,{ret:true,winWi:'50%'});
	wrapBk.id = 'ActivityWinFloatWrap';
	ps_DOM.body.appendChild(wrapBk);
}
	,
	menu:function(D,pare,P){
		var wrap = ps_DOM.newObj('div');
		var ul = ps_DOM.newObj('ul',{'class':'ulList'});
		var Au = (D.Au)?D.Au : {};
		if(1 || Au.canEdit == 'Y'){
			var liReceive = ps_DOM.newObj('li');
			var edt = $1.T.faBtn({'class':'fa_pencil',textNode:'Modificar Tarjeta',func:function(){ $5a.F.full(D,P); }
			});
			liReceive.appendChild(edt);
			ul.appendChild(liReceive);
		}
		var liReceive = ps_DOM.newObj('li');
		var btnEdit = $1.T.faBtn({'class':'fa_archive',textNode:(D.archived=='Y') ? 'Desarchivar' : 'Archivar'});
		btnEdit.Y = (D.archived =='Y') ? 'N' : 'Y';
		btnEdit.onclick = function(){ var This = this;
			$1.Win.confirm({text:'La actividad se archivará / desarchivará en todo los elementos donde esté relacionada.',
				func:function(){
					$5a.S.updField({movType:'archived',to:This.Y, actId:D.actId, callf:P.callf});
					if(This.Y == 'Y'){ This.innerText = 'Desarchivar'; This.Y = 'N'; }
					else if(This.Y == 'N'){ This.innerText = 'Archivar'; This.Y = 'Y'; }
				}
				
				});
		}
		liReceive.appendChild(btnEdit);
		ul.appendChild(liReceive);
		var omit = (D.__omit) ? D.__omit : {};
		if(!omit.copy){
			var liCopy = ps_DOM.newObj('li');
			var edt = $1.T.faBtn({'class':'fa_clone',textNode:'Copiar'});
			edt.onclick = function(){ $5a.C.form(D,winRel); }
			liCopy.appendChild(edt);
			ul.appendChild(liCopy);
		}
		wrap.appendChild(ul);
		var winRel = $1.Win.minRel(wrap,{winTitle:'-',winPosition:'left:0; top:1rem;'});
		//$1.Win.relative(wrap,pare,{winTitle:'-',return:true});
		pare.style.position = 'relative';
		if(P.zIndex){ pare.style.zIndex = P.zIndex; }
		pare.appendChild(winRel);
	var fo = $1.t('input',{style:'width:0; height:0; position:relative; left:'+(winRel.offsetWidth/2)+';'});
		wrap.appendChild(fo);
		fo.focus(); fo.blur(); $1.delet(fo);
	}
}
,
C:{//copy
form:function(D,wrapCont){
	var wrap = $1.t('div');
	var divLine = $1.t('div',{'class':'divLine'});
	var asunt = $1.t('input',{type:'text','class':'jsFields',name:'actAsunt',value:D.actAsunt,placeholder:'Asunto...'});
	divLine.appendChild(asunt);
	wrap.appendChild(divLine);
	var kee = $1.t('div');
	kee.appendChild($1.t('h4','Conservar...'));
	var divLine = $1.t('div',{'class':'divLine'});
	var ul = $1.t('ul');
	var lis = {
		checklist:{name:'checkList',label:'CheckList'},
		userAssg:{name:'userAssg',label:'Responsable'}
	};
	for(var i in lis){
		var li = $1.t('li');
		var ck = $1.t('input',{type:'checkbox','class':'jsFields',name:lis[i].name,id:'sss_'+i});
		var lab = $1.t('label',{textNode:lis[i].label,'for':'sss_'+i});
		li.appendChild(ck);
		li.appendChild(lab);
		ul.appendChild(li);
	}
	var li = $1.t('li','Vencimiento');
	var dueDate = $1.t('input',{type:'date','class':'jsFields',name:'dueDate',value:D.dueDate})
	dueDate.childNodes[0].classList.remove('iBg_date');
	dueDate.childNodes[0].classList.add('iBg_deadLine');
	li.appendChild(dueDate);
	ul.appendChild(li);
	divLine.appendChild(ul);
	kee.appendChild(divLine);
	kee.appendChild($1.t('div','Solo se copiará en el tablero actual.'));
	wrap.appendChild(kee);
	var ps = $1.t('div');
	var resp = $1.t('div');
	var send = $1.T.btnSend({value:'Copiar',
	func:function(){
		var vPost = $1.G.json(D)+'&actId='+D.actId+'&'+$1.G.inputs(wrap,null,{onOff:'Y'});
		$ps_DB.get({file:'POST '+$5a.api+'Copy',inputs:vPost,loade:resp,
		func:function(Jq){ $ps_DB.response(resp,Jq); }});
	}});
	ps.appendChild(resp); ps.appendChild(send);
	wrap.appendChild(ps);
	if(wrapCont){ var h5 = wrapCont.childNodes[0].childNodes[0];
	h5.innerText = 'Copiar Tarjeta';
		wrapCont = $1.q('.__wrapContent',wrapCont);
		$1.clear(wrapCont); wrapCont.appendChild(wrap);
	}
	else{ $1.Win.open(wrap,{onBody:true}); }
}
}
}

//tick
$Tick = {
apir:{last:'', a:'/xs/tick/'},
R:{stLine:''},
Gr:{due:'Vencidos', today:'Hoy', tomorrow:'Mañana', days7:'Próx. 7 días', nextt:'Próximos',sinDate:'Sin Fecha',unde:'No definido?'},
ini:function(P,cont){P=(P)?P:{};
	$M.Ht.ini({fieldset:0, func_filt:null, func_cont:function(){
		var cont = (cont)?cont:$M.Ht.cont;
		$1.t('div',{id:'_5a_wrapFormPut'},cont);
		$1.t('div',{id:'_5a_wrapCont'},cont);
		$Tick.put(P);
	} });
	$Tick.get(P);
},
iniReset:function(P,P2){
	P=(P)?P:{}; P2=(P2)?P2:{};
	var form = $1.q('#_5a_wrapFormPut');
	var wList = $1.q('#_5a_wrapCont');
	$1.clear(form); $1.clear(wList);
	$Tick.put(P,null,P2);
	if(P2.reload =='apirLast'){ $Tick.get(P,P2);}
	else if(P2.reload !='N'){ $Tick.get(P,P2); }
	else{ $Tick.load(); }
},
stLine:function(L){//due, today, sinDate
	var today = $2d.f('','Y-m-d H:i');
	var doDate = $2d.is0(L.doDate);
	var endDate = $2d.is0(L.endDate);
	var dueDate = $2d.is0(L.dueDate);
	var difDo = $2d.diff({date1:L.doDate, date2:today, g:'days{}'});
	var difEnd = $2d.diff({date1:L.endDate, date2:today, g:'days{}'});
	var difDue = $2d.diff({date1:L.dueDate, date2:today, g:'days{}'});
	if(doDate && dueDate){ r= 'sinDate'; }
	else if(!dueDate && difDue.b>0){ r= 'due'; }
	else if(!endDate && difEnd.a>0){ r= 'due'; }
	else if(!endDate && difEnd.a>0 && !doDate && difDo.a>0){ r= 'due'; }
	else if(endDate && !doDate && difDo.b>0){ r= 'due'; }
	else if(!dueDate && difDue.b==0){ r= 'today'; }
	else if(!doDate && difDo.b==0){ r= 'today'; }
	else if(!doDate && difDo.b==-1){ r= 'tomorrow'; }
	else if(!dueDate && difDue.b==-1){ r= 'tomorrow'; }
	else if(!doDate && difDo.b<=-1 && difDo.b>=-7){ r= 'days7'; }
	else if(!dueDate && difDue.b<=-1 && difDue.b>=-7){ r= 'days7'; }
	else if(!doDate && difDo.b<-7){ r= 'nextt'; }
	else if(!dueDate && difDue.b<-7){ r= 'nextt'; }
	else{ r='unde';}
	return r;
},
nis_stLine:function(L){//revisa si no es, para añadir today,tomo..
	var days7 = ($Tick.R.stLine == 'days7' && (L._stLine=='today' || L._stLine=='tomorrow'));
	if($Tick.R.stLine !='' && (!days7 && $Tick.R.stLine != L._stLine)){ return true; }
	else{ return false; }
},
put:function(P,contDef,P2){
	P=(P)?P:{}; P2=(P2)?P2:{};
	if(P.tt){ P.targetType = P.tt; P.targetRef = P.tr; }
	var cont = (contDef)?contDef:$1.q('#_5a_wrapFormPut');
	var topV = $1.t('div',{id:'_5a_wrapTop'}); cont.insertBefore(topV,cont.firstChild);
	var wTools = $1.t('div',{'class':'_5a_wrapTools'}); topV.appendChild(wTools);
	var liOpts = {
	listDate:{ico:'fa fa_clock',textNode:' Por Tiempo',P:{k:'listDate'}},
	table:{ico:'fa fa_cells',textNode:' Tabla',P:{k:'table'}},
	prioCovey:{ico:'fa fa_prio_high',textNode:' Cuadrante de Prioridades',P:{k:'prioCovey'}},
	calMonth:{ico:'fa fa_calendar',textNode:' Calendario',P:{k:'calMonth'}},
	calWeek:{ico:'fa fa_calendar',textNode:' Semana',P:{k:'calWeek'}},
	status:{ico:'ico ico_actStatus_doing',textNode:'Estado',P:{k:'statusKanban'}},
	};
	var wOpts = $1.Ul.disp(liOpts,
	{id:'_5atick_view__', funcC:function(T){
		$Tick.N.viewSel.value = T.P.k; sel.innerText = ' '+T.innerText;
		$Tick.load();
	}},null);
	var sel = $1.T.faBtn({'class':'fa_eye', value:'listDate', textNode:'Vista', func:function(){
		$1.click.disp(wOpts,sel);
	}});
	$1.clear($Tick.N.wOpts);
	$Tick.N.wOpts.appendChild(sel);
	$Tick.N.wOpts.appendChild(wOpts);
	var se = $1.t('input',{type:'text','class':'iBg iBg_search2',placeholder:'Buscar...'});
	se.onkeyup = se.onchange = function(){
		$db.find({t:this.value,e:'like3',fs:{actAsunt:'',actNote:''}, db:'act',func:function(R){ $Tick.get({dbO:R},{reload:'search'}); }});
	}
	$Tick.N.wOpts.appendChild(se);
	//ADMS_Filters.getBtn(cont,{Fik:$Fi.tick, func:function(){ $Tick.get({},{reload:'Y'}); }});
	if(P.childType && P.childRef){
	cont.appendChild($1.t('input',{type:'hidden','class':'jsFiltVars',name:'childType',value:P.childType}));
	cont.appendChild($1.t('input',{type:'hidden','class':'jsFiltVars',name:'childRef',value:P.childRef}));
	}
	var wrapSend = $1.t('div',{'class':'_5a_wrapCreater'},cont);
		var wrap = $1.t('div',{style:'border-bottom:1px solid #CCC; padding:2px 8px 2px 6px;'});
	var vPost = 'actType=task&';
	vPost += (P.targetType)?'targetType='+P.targetType+'&targetRef='+P.targetRef+'&':'';
	if(P.FR){
		for(var i in P.FR){ vPost += '&FR['+i+']='+P.FR[i]; }
	}
	for(var i in P2.defFields){ vPost += i+'='+P2.defFields[i]+'&'; }
	var pht = (P2.defFields)?'Añadir actividad según...' :'Actividad Nueva...';
	var nForm = $1.t('button',{'class':'iBg iBg_uiForm'});
	nForm.onclick = function(){ $Tick.Ad.form({vPost:P.vPost,func:function(Jqr){
		$Tick.load('add',Jqr.nA);
	}},P2.defFields); }
	var asunt = $1.t('input',{type:'text','class':'jsFields boxi1 iBg iBg_task _5a_wrapFormLine', name:'actAsunt',placeholder:pht,O:{vPost:vPost}});
	asunt.onkeyup = function(evt){ This = this;
		ADMS_EV.isKey(evt,13,{func:function(){
			This.value = This.value.substring(0,This.value.length-1);
			vPost = '&'+$1.G.inputs(wrapSend);
			$r.get({f:'POST '+$Tick.apir.a, inputs:vPost,
				func:function(Jq1){
				if(Jq1.errNo){ $1.Win.message(Jq1) }
				else{
					$Tick.load('add',Jq1.nA);
					$1.clearInps(wrapSend); }
			}
			});
		}});
	}
	wrapSend.appendChild(nForm);
	wrapSend.appendChild(asunt);
	if(contDef){ $1.t('div',{id:'_5a_wrapCont',textNode:''},contDef); }
}
,
get:function(P,P2){P2=(P2)?P2:{};
	var wrap = $1.q('#_5a_wrapCont');
	var apiA = (P2.apiA)?P2.apiA:'';
	var apir = $Tick.apir.a+apiA;
	if(P.dbO){
		$Co.a= P.dbO; delete(P.dbO);
		if(P2.reload=='search'){ 
			if(P2.func){ P2.func();}
			else{ $Tick.load(); }
			return true;
		}
	}
	//if es apirLast, verifica que la apir no se igual
	if(P2.reload!='Y' && apir==$Tick.apir.last){
		if(P2.func){ P2.func();}
		else{ $Tick.load(); }
		return true;
	}
	$Co.a = {};
	$Tick.apir.last = apir;
	var vPost = $1.G.inputs($1.q('#_5a_wrapFormPut'),'jsFiltVars');
	$r.get({f:'GET '+apir, inputs:vPost, loade:wrap, func:function(Jq){
		$Co.a = {}; 
		if(!Jq.errNo){
			for(var i in Jq.L){ L = Jq.L[i];
				L._stLine = $Tick.stLine(L);
				$Co.a[L.actId] = L;
			}
		}
		if(P2.func){ P2.func();}
		else{ $Tick.load(); }
	}});
}
}

$Tick.N = {
mTi:'_5a_wrapMainTitle', wOpts:'_5a_wrapOptions', wLists:'_5a_wrapLists', viewSel:'listDate', wInfo:'_5a_wrapInfoAlways'
};

$Tick.I = {
inte:function(P,cont){P=(P)?P:{};
	var cont = (cont)?cont:$M.Ht.cont;
	cont.appendChild($1.t('div',{style:'display:none;',id:'_5a_wrapInfoAlways'}));
	var mTi = $1.t('div',{id:'_5a_wrapMainTitle'},cont);
		var opts = $1.t('div',{id:'_5a_wrapOptions'},cont);
		var formWrap = $1.t('div',{id:'_5a_wrapFormPut'},cont);
		$1.t('div',{id:'_5a_wrapCont'},cont);
		//define
		$M.read();
		$Tick.N.wInfo = $1.q('#_5a_wrapInfoAlways',cont);
		$Tick.N.mTi = $1.q('#_5a_wrapMainTitle',cont);
		$Tick.N.wOpts = $1.q('#_5a_wrapOptions',cont);
		$Tick.N.wLists = $1.q('#_5a_wrapLists',cont);
		$Tick.N.wInfo.appendChild($1.t('input',{type:'hidden',id:'_5atick_viewSel',value:'listDate'}));
		$Tick.N.viewSel = $1.q('#_5atick_viewSel',cont);
		$Tick.put(P,formWrap);
},
main:function(P,cont){P=(P)?P:{};
	$M.Ht.ini({fieldset:0, func_filt:null, func_cont:function(){
		var cont = (cont)?cont:$M.Ht.cont;
		var mTi = $1.t('div',{id:'_5a_wrapMainTitle','class':'_5a_wrapBar_left'},cont);
		var btn = $1.T.faBtn({'class':'fa_menu _5a__listName winUl_hide_btnOpen',textNode:'Todas', style:'font-size:1.1rem; border-bottom:0.0625rem solid #000;', func:function(This){
			$1.click.hide(This,lis);
		}})
		mTi.appendChild(btn);
		var wmid = $1.t('div',{'class':'_5a_wrapBar_middle'},cont)
		wmid.appendChild($1.t('div',{style:'display:none;',id:'_5a_wrapInfoAlways'}));
		var opts = $1.t('div',{id:'_5a_wrapOptions'},wmid);
		$1.t('div',{id:'_5a_wrapFormPut'},wmid);
		$1.t('div',{id:'_5a_wrapCont'},wmid);
		//define
		$M.read();
		$Tick.N.wInfo = $1.q('#_5a_wrapInfoAlways');
		$Tick.N.mTi = $1.q('#_5a_wrapMainTitle');
		$Tick.N.wOpts = $1.q('#_5a_wrapOptions');
		$Tick.N.wLists = $1.q('#_5a_wrapLists');
		$Tick.N.wInfo.appendChild($1.t('input',{type:'hidden',id:'_5atick_viewSel',value:'listDate'}));
		$Tick.N.viewSel = $1.q('#_5atick_viewSel');
		var lis = $1.Ul.hide([
		{icoT:'fa fa_eye',textNode:'Todas', func:function(){ $Tick.I.listOpen('all',P); } },
		{ico:'',textNode:'Próximos 7 días', func:function(){ $Tick.I.listOpen('days7',P); } },
		{icoT:'fa fa_priv_members',textNode:'Socios', func:function(){ $Tick.I.listOpen('children',P); } },
		{icoT:'fa fa_archive',textNode:'Archivadas', func:function(){ $Tick.I.listOpen('archived',P); } },
		{ico:'',textNode:'Listas', func:function(){ $Tick.I.listOpen('actListId',P); } }
		],
		{id:'_5a_wrapLists'});
		mTi.appendChild(lis);
		$Tick.put(P);
	} });
	if($M.p.list){ $Tick.I.listOpen($M.p.list,P); }
	else{ $Tick.get(P); }
},
listOpen:function(list,P){//inicia contenido segun la lista seleccionada
	var form = $1.q('#_5a_wrapFormPut');
	var btnCreater = $1.q('._5a_wrapCreater',form);
	btnCreater.style.display = 'block';
	$Tick.R.stLine = '';
	var lname = $1.q('._5a__listName',$Tick.N.mTi);
	switch(list){
		case 'days7':{ $Tick.R.stLine = 'days7'; $Tick.N.viewSel.value = 'listDate'; $Tick.iniReset(P,{reload:'apirLast',defFields:{doDate:$2d.days7}});
		lname.innerText = ' Próximos 7 días'; break;}
		case 'children':{
			$Tick.N.viewSel.value = 'list_Children';
				btnCreater.style.display = 'none';
			lname.innerText = ' Actividades Relacionadas a Socios';
			$Tick.get(P,{apiA:list});
		break;}
		case 'archived':{
			btnCreater.style.display = 'none';
			$Tick.N.viewSel.value = 'table';
			lname.innerText = ' Archivadas';
			$Tick.get(P,{apiA:list});
		break;}
		case 'actListId':{
			$Tick.R.stLine = ''; $Tick.iniReset(P,{apiA:list});
			lname.innerText = ' Nombre Lista';
		break;}
		default :{ $Tick.R.stLine = ''; $Tick.N.viewSel.value = 'listDate'; $Tick.iniReset(P); list = 'all';
		lname.innerText = ' Todas'; break;}//all
	}
	$M.hash2({list:list});
},
}

$Tick.load = function(act,oAct,P){ var oAct=(oAct)?oAct:{};
	var v = $Tick.N.viewSel.value;
	//select en todas, input-hidden en socios
	if(act && act=='add'){
	if(P && P.actStatus=='completed'){
		var qa = $1.q('._o_activity_'+oAct.actId,0,'all');
		for(var i=0; i<qa.length; i++){ $1.delet(qa[i]); }
		delete($Co.a[oAct.actId]);
		return 1;
	}
	$Co._u.a(oAct,P);
	var wrapA = $1.q('#_5aviewWrap_'+v);
	switch(v){
		case 'listDate' : $Tick.Va.listDate(oAct);; break;
		case 'table' : $Tick.Va.table(oAct,wrapA); break;
		case 'calWeek' : $Tick.Va.calWrap(oAct,wrapA); break;
		case 'calMonth' : $Tick.Va.calWrap(oAct,wrapA); break;
		case 'statusKanban': $Tick.Va.kanban1(oAct,wrapA,{k:$Co.a[oAct.actId].actStatus}); break;
		case 'prioCovey': $Tick.Va.prioCovey(oAct,wrapA); break;
		//List
		case 'list_Children' : $Tick.Va.children(oAct,wrapA,{});
	}
	}
	else{
	var wList = $1.q('#_5a_wrapCont'); $1.clear(wList);
	var tmov = $1.q('.gooMovDate_wrap',wList.parentNode);
	if(tmov){ tmov.style.display = 'none'; }
	switch(v){
		case 'listDate' : $Tick.V.listDate(); break;
		case 'table' : $Tick.V.table(); break;
		case 'calMonth' :{
			$1.T.gooDate($1.q('._5a_wrapTools',wList.parentNode),{rang:'month',func:function(){$1.clear(wList); $Tick.V.calMonth({}); 
			}});
			$Tick.V.calMonth({});
		}break;
		case 'calWeek' :{
			$1.T.gooDate($1.q('._5a_wrapTools',wList.parentNode),{rang:'week',func:function(){$1.clear(wList); $Tick.V.calWeek({}); 
			}});
			$Tick.V.calWeek({});
		}break;
		case 'prioCovey' : $Tick.V.prioCovey(); break;
		case 'statusKanban' : $Tick.V.statusKanban({}); break;
		//List
		case 'list_Children' : $Tick.V.children({}); break;
		case 'archived' :$Tick.V.table(); break;
	}
	}
	$5a.iniDrag();
}

$Tick.V ={
listDate:function(P,cont){
	var wrap= (cont)?cont:$1.q('#_5a_wrapCont');
	var ul = {};
	for(var i in $Tick.Gr){
		ul[i] = $1.t('ul',{'class':'_5a_wrapCont_ul _5a_uls_'+i});
		ul[i].appendChild($1.t('b',$Tick.Gr[i]));
		wrap.appendChild(ul[i]);
	}
	for(var i in $Co.a){ $Tick.Va.listDate($Co.a[i]); }
},
table:function(P,cont){
	var wrap= (cont)?cont:$1.q('#_5a_wrapCont');
	var tb = $1.T.table([
		{textNode:''},
		{textNode:'Pr.'},
		{textNode:'Asunto'},
		{textNode:'Responsable'},
		{textNode:'Vence'},
		{textNode:'Inicia'},
		{textNode:'Finaliza'},
		{textNode:'Duración'},
		{textNode:'Estado'},
		{textNode:'Propietario'},
		{textNode:'Creada'},
		]
	,{tbData:{'class':'table_zh',id:'____5a_wrapList'}
	});
	var tBody = $1.t('tbody',{id:'_5aviewWrap_listSimple'});
	for(var a in $Co.a){ $Tick.Va.table($Co.a[a],tBody); }
	tb.appendChild(tBody);
	//var tb = ps_DOM.Tag.tbExport(tb);
	wrap.appendChild(tb);
	$1.nullBlank = false;
},
calMonth:function(P,cont){
	var wrap= (cont)?cont:$1.q('#_5a_wrapCont');
	var gooDate1 = $1.q('.gooMovDate_1');
	var gooDate2 = $1.q('.gooMovDate_2');
	var tb = $1.t('table',{'class':'table_zh',style:'min-width:119rem;'});
	var tHead = $1.t('thead');
	var tr0 = $1.t('tr');
	tr0.appendChild($1.t('td',{'textNode':'#',style:'width:1rem;'}));
	var tBody = $1.t('tbody',{id:'#_5aviewWrap_calMonth'});
	var TDS = {};
	var Days = $2d.Vs.Days;
	for(var dN in Days){ var k = Days[dN].k;
		tr0.appendChild($1.t('td',{'textNode':Days[dN].t,style:'width:16rem;','class':'weekDay_'+k}));
	}
	var mI = $2d.g('n',gooDate1.value);
	var date1 = $2d.weekB(gooDate1.value);
	var date2 = $2d.weekE(gooDate2.value);
	var lastWY = '';
	var tdWeek = {};
	var befW = 1;
	Wd = $2d.Draw.Wd({date1:date1,date2:date2});
	for(var w in Wd){
		var tr = $1.t('tr');
		var wT = (w>52) ? 1 : w;
		tr.appendChild($1.t('td',{textNode:wT}));
		for(var dN in Days){
		var tD = Days[dN]; var k = tD.k;
		var Wd_w_k = (Wd[w][k]) ? Wd[w][k] : {};
		var text = (Wd_w_k.date) ? $2d.f(Wd_w_k.date,'mmm d') : '';
		var bgColor = (mI == Wd_w_k.n) ? '': ' background-color:#e6e6e6;';
		var zk = Wd_w_k.y+'_'+Wd_w_k.z;
		var classZ = (Wd_w_k.z) ? 'yearDayTd_'+zk : '';
			var td = $1.t('td',{'class':classZ+' cardCanvas_drag_wrap '+$5a.putKard+zk,style:'height:10rem;'+bgColor});
			td.Odnd = {movType:'doDate',to:Wd_w_k.date};
			var add = $Tick.Ht.btnAdd({btn:{textNode:' '+text}},{doDate:Wd_w_k.date});
			td.appendChild(add);
			tr.appendChild(td);
		}
		tBody.appendChild(tr);
	}
	tHead.appendChild(tr0);
	tb.appendChild(tHead);
	tb.appendChild(tBody);
	for(var a in $Co.a){ $Tick.Va.calWrap($Co.a[a],tBody); }
	wrap.appendChild(tb);
	return wrap;
},
calWeek:function(P,cont){
	var wrap= (cont)?cont:$1.q('#_5a_wrapCont');
	var gooDate1 = $1.q('.gooMovDate_1');
	var gooDate2 = $1.q('.gooMovDate_2');
	var tb = $1.t('table',{'class':'table_zh',style:'min-width:119rem;'});
	var tHead = $1.t('thead');
	var tr0 = $1.t('tr');
	tr0.appendChild($1.t('td',{'textNode':'#',style:'width:1rem;'}));
	var tBody = $1.t('tbody',{id:'#_5aviewWrap_calMonth'});
	var TDS = {};
	var Days = $2d.Vs.Days;
	for(var dN in Days){ var k = Days[dN].k;
		tr0.appendChild($1.t('td',{'textNode':Days[dN].t,style:'width:16rem;','class':'weekDay_'+k}));
	}
	var mI = $2d.g('n',gooDate1.value);
	var date1 = $2d.weekB(gooDate1.value);
	var date2 = $2d.weekE(gooDate2.value);
	var lastWY = '';
	var tdWeek = {};
	var befW = 1;
	Wd = $2d.Draw.Wd({date1:date1,date2:date2});
	for(var w in Wd){
		var tr = $1.t('tr');
		var wT = (w>52) ? 1 : w;
		tr.appendChild($1.t('td',{textNode:wT}));
		for(var dN in Days){
		var tD = Days[dN]; var k = tD.k;
		var Wd_w_k = (Wd[w][k]) ? Wd[w][k] : {};
		var text = (Wd_w_k.date) ? $2d.f(Wd_w_k.date,'mmm d') : '';
		var bgColor = (mI == Wd_w_k.n) ? '': ' background-color:#e6e6e6;';
		var zk = Wd_w_k.y+'_'+Wd_w_k.z;
		var classZ = (Wd_w_k.z) ? 'yearDayTd_'+zk : '';
			var td = $1.t('td',{'class':classZ+' cardCanvas_drag_wrap '+$5a.putKard+zk,style:'height:10rem;'+bgColor});
			td.Odnd = {movType:'doDate',to:Wd_w_k.date};
			var add = $Tick.Ht.btnAdd({btn:{textNode:' '+text}},{doDate:Wd_w_k.date});
			td.appendChild(add);
			tr.appendChild(td);
		}
		tBody.appendChild(tr);
	}
	tHead.appendChild(tr0);
	tb.appendChild(tHead);
	tb.appendChild(tBody);
	for(var a in $Co.a){ $Tick.Va.calWrap($Co.a[a],tBody); }
	wrap.appendChild(tb);
	return wrap;
},
prioCovey:function(P,cont){//board
	var wrap= (cont)?cont:$1.q('#_5a_wrapCont');
	var tb = $1.t('table',{'class':'table_covey'});
	var tBody = $1.t('tbody',{id:'_5aviewWrap_prioCovey'});
	var tr1 = $1.t('tr');
	var tr2 = $1.t('tr');
	var tr3 = $1.t('tr');
	var Odnd = {movType:'actPriority',to:'high'};
	var TDs = {};
	var WRs = {};
	for(var k in $5a.Vs.actPriority){
		TDs[k] = $1.t('td',{'class':k+' cardCanvas_drag_wrap',style:'min-width:20rem;'});
		if(k == 'none'){ TDs[k].setAttribute('colspan',2); }
		var title = $1.t('div',{'class':'title'});
		var add = $Tick.Ht.btnAdd({wboListId:null},{actPriority:k});
		title.appendChild(add);
		title.appendChild($1.t('textNode',' '+$5a.Vs.actPriority[k]));
		TDs[k].appendChild(title);
		WRs[k] = $1.t('div',{'class':'wrapList cardCanvas_drag_wrapReal '+$5a.putKard+k+' _5aviewWrap_prioCovey_'+k});
		WRs[k].Odnd = {movType:'actPriority',to:k}
		TDs[k].appendChild(WRs[k]);
		if(k == 'high' || k == 'medium'){ tr1.appendChild(TDs[k]); }
		else if(k == 'normal' || k == 'low'){ tr2.appendChild(TDs[k]); }
		else {tr3.appendChild(TDs[k]); }
	}
	tBody.appendChild(tr1);
	tBody.appendChild(tr2);
	tBody.appendChild(tr3);
	tb.appendChild(tBody);
	for(var a in $Co.a){ $Tick.Va.prioCovey($Co.a[a],tBody); }
	wrap.appendChild(tb);
},
statusKanban:function(P,cont){
	var wrap= (cont)?cont:$1.q('#_5a_wrapCont');
	var tb = $1.t('table',{'class':'tbCanvasCard',id:'_5aviewWrap_statusKanban'});
	var tHead = $1.t('thead');
	var tr0 = $1.t('tr');
	var tr1 = $1.t('tr');
	var TR = {};
	for(var k in $5a.Vs.actStatus){ var Li = $5a.Vs.actStatus[k];
		var tdName = $1.t('td',{textNode:Li+' '});
		tdName.appendChild($1.t('span',{'class':'spanLine actTotal actStatus_'+k,textNode:''}));
		tr0.appendChild(tdName);
		TR[k] = $1.t('td',{'class':'cardCanvas_drag_wrap '+$5a.putKard+k+' _5aviewWrap_kanban1_'+k});
		TR[k].Odnd = {movType:'actStatus',to:k}
		tr1.appendChild(TR[k]);
	}
	tHead.appendChild(tr0);
	tb.appendChild(tHead);
	var tBody = $1.t('tbody');
	tBody.appendChild(tr1);
	for(var a in $Co.a){ $Tick.Va.kanban1($Co.a[a],tBody,{k:$Co.a[a].actStatus});
	}
	tb.appendChild(tBody);
	wrap.appendChild(tb);
	for(var k in $5a.Vs.actStatus){
		var spanLine = $1.q('.actStatus_'+k,tr0);
		spanLine.innerText = TR[k].childNodes.length;
	}
},
children:function(P,cont){
	var wrap= (cont)?cont:$1.q('#_5a_wrapCont');
	var wrapId = $1.t('div',{id:'_5aviewWrap_children'},wrap);
	for(var i in $Co.a){
		L=$Co.a[i];
		var or = '_5a_childsWrap_'+L.childType+'_'+L.childRef;
		var w = $1.q('.'+or,wrapId);
		if(!w){
			w = $1.t('div',{'class':or},wrapId);
			w.appendChild($1.t('h5',{'class':'head1',textNode:L.childName}));
		}
		var cw = $Tick.Va.children(L,wrapId);
	}
},
}

$Tick.Va = { //añade segun vista
listDate:function(L,P){ P=(P)?P:{};
	L = $Co.a[L.actId];
	if($Tick.nis_stLine(L)){ return true; }
	var oldN = $1.q('._5af_liw_'+L.actId);
	var li = $1.t('li',{'class':'_5af_liw _5af_liw_'+L.actId+' _o_activity_'+L.actId});
	var div = $1.t('div',{'class':'l-task'}); li.appendChild(div);
	var ck2 = $Tick.f.ckStatus(L); div.appendChild(ck2);
	var span = $1.t('div',{'class':'_5af_asunt'}); div.appendChild(span);
	var wrapDates = $1.t('div',{'class':'_5a_wrapDates',style:'font-size:0.75rem;'}); span.appendChild(wrapDates);
	span.appendChild($Tick.f.asuntTy(L));
	span.onclick = function(){ $Tick.Ht.winTick(L); }
	wrapDates.appendChild($Tick.f.status(L));
	wrapDates.appendChild($Tick.f.prio(L));
	wrapDates.appendChild($Tick.f.note(L));
	$Tick.f.dueDate(L,wrapDates);
	$Tick.f.doDate(L,wrapDates);
	span.appendChild($Tick.f.userAssg(L));
	ul = $1.q('._5a_uls_'+L._stLine);
	ul.style.display = 'block';
	$1.appendr(li,ul,oldN);
	$Tick.updF.uls(); //eliminar los vacios
},
table:function(L,tBody){
	var tBody = (tBody)?tBody:$1.q('#____5a_wrapList tbody');
	L = $Co.a[L.actId];
	if($Tick.R.stLine !='' && $Tick.R.stLine != L._stLine){ return true; }
	var oldN = $1.q('._o_activity_'+L.actId,tBody);
	var tr = $1.t('tr',{'class':'_o_activity_'+L.actId});
	$1.t('td',{node:$Tick.f.ckStatus(L)},tr);
	$1.t('td',{node:$Tick.f.prio(L)},tr);
	$1.t('td',{node:$Tick.f.asuntTy(L,{open:'Y'})},tr);
	$1.t('td',{node:$Tick.f.userAssg(L,'text')},tr);
	$1.t('td',{node:$Tick.f.dueDate(L)},tr);
	$1.t('td',{node:$Tick.f.doDate(L)},tr);
	$1.t('td',{node:$Tick.f.endDate(L)},tr);
	$1.t('td',{textNode:L.timeDuraText},tr);
	$1.t('td',{node:$Tick.f.status(L,'text')},tr);
	$1.t('td',{textNode:L.userName},tr);
	$1.t('td',{textNode:$2d.f(L.dateC,'mmm d H:iam',{is0:''})},tr);
	$1.appendr(tr,tBody,oldN);
},
calWrap:function(D,tBody,P){P=(P)?P:{};
	var old = $1.q('._o_activity_'+D.actId,tBody);
	var D = $Co.a[D.actId];
	var card = $Tick.f.wrap(D);
	card.appendChild($Tick.f.ckStatus(D));
	card.appendChild($Tick.f.asuntT(D,{open:'Y'}));
	var top = $1.t('div',{style:'font-size:0.75rem;'}); card.appendChild(top);
	top.appendChild($Tick.f.prio(D));
	top.appendChild($Tick.f.type(D));
	top.appendChild($Tick.f.status(D));
	top.appendChild($Tick.f.dueDate(D));
	var ky = D.doDate;
	var k1 = $2d.g('z',D.doDate);
	if(k1 == 'e'){ var k1 = $2d.g('z',D.dueDate); 
	var ky = D.dueDate; 
	}
	ky = (ky) ? ky.substring(0,4) : '';
	var zk = ky+'_'+k1;
	var tdDay = $1.q('.yearDayTd_'+zk,tBody);
	$1.appendr(card,tdDay,old,{exists:'if'});
},
prioCovey:function(D,wrap,P){P=(P)?P:{};
	var L = $Co.a[D.actId]; var k = D.actPriority;
	if($Tick.R.stLine !='' && $Tick.R.stLine != L._stLine){ return true; }
	var old = $1.q('._o_activity_'+L.actId,wrap);
	var tdDay = $1.q('._5aviewWrap_prioCovey_'+k,wrap);
	var card = $Tick.f.wrap(L);
	card.appendChild($Tick.f.ckStatus(L));
	card.appendChild($Tick.f.asuntTy(L,{open:'Y'}));
	card.appendChild($Tick.f.calBot(L,{omit:'prio'}));
	$1.appendr(card,tdDay,old)
},
kanban1:function(D,wrap,P){//status
	var L = $Co.a[D.actId];
	if($Tick.R.stLine !='' && $Tick.R.stLine != L._stLine){ return true; }
	var old = $1.q('._o_activity_'+L.actId,wrap);
	var tdDay = $1.q('._5aviewWrap_kanban1_'+P.k,wrap);
	var card = $Tick.f.wrap(L);
	card.appendChild($Tick.f.ckStatus(L));
	card.appendChild($Tick.f.asuntTy(L,{open:'Y'}));
	$1.appendr(card,tdDay,old);
},
children:function(D,wrap,P){//status
	P=(P)?P:{};
	var L = $Co.a[D.actId];
	var or = '_5a_childsWrap_'+L.childType+'_'+L.childRef;
	var tdDay = $1.q('.'+or,wrap);
	var old = $1.q('._o_activity_'+L.actId,tdDay);
	var card = $Tick.f.wrap(L);
	card.appendChild($Tick.f.ckStatus(L));
	card.appendChild($Tick.f.asuntTy(D,{open:'Y'}));
	var cardBot = $Tick.f.calBot(L);
	card.appendChild(cardBot);
	$1.appendr(card,tdDay,old);
},
}

$Tick.Ad ={
form:function(P,Df){
	var D=(P.actId && $Co.a[P.actId])?$Co.a[P.actId]:{};
	D=(Df)?Df:D;
	var wrapF = $1.q('._5a_wrapFormLine');
	var vPost = (wrapF && wrapF.O) ? wrapF.O.vPost+'&' :'';
	vPost += $1.G.inputs($1.q('#_5a_wrapTop'),'jsFiltVars');
	vPost += (P.vPost)?'&'+P.vPost:'';
	var wrap = $1.t('div',{style:'padding-top:1rem;'});
	D.actType = (D.actType) ? D.actType : 'task';
	var sel = ps_DOM.Tag.divSelect({sel:{'class':'jsFields',name:'actType'}, classBtn:'boxi1',opts:$5a.Vs.actType, selected:D.actType,iBgOpts:''});
	var selPriority = ps_DOM.Tag.divSelect({sel:{'class':'jsFields',name:'actPriority'}, classBtn:'boxi1', opts:$5a.Vs.actPriority, selected:D.actPriority,clsFa:'prio_',classWrap:'input',noBlank:'Y'});
	var selStatus = ps_DOM.Tag.divSelect({sel:{'class':'jsFields',name:'actStatus'}, classBtn:'boxi1', opts:$5a.Vs.actStatus, selected:D.actStatus,iBgOpts:'actStatus_',classWrap:'input',noBlank:'Y'});
	D.doDate = ($2d.is0(D.doDate))?'':D.doDate;
	D.endDate = ($2d.is0(D.endDate))?'':D.endDate;
	var userAssg = $1h.userAssg(D,{wxn:'wrapx1'});
	var Lis = [
	{L:{textNode:'Asunto:'},I:{tag:'input',type:'text','class':'jsFields divLine',name:'actAsunt',value:D.actAsunt}},
	{L:{textNode:'Tipo:'},node:sel},
	{L:{textNode:'Prioridad:'},node:selPriority},
	{L:{textNode:'Estado:'},node:selStatus},
	{L:{textNode:'Fecha:'},I:{tag:'input',type:'date','class':'jsFields divLine',name:'doDate',value:D.doDate, HH:{value:D.doTime}}},
	{L:{textNode:'Finaliza:'},I:{tag:'input',type:'date','class':'jsFields divLine',name:'endDate',value:D.endDate,HH:{value:D.endTime}}},
	{L:{textNode:'Vencimiento:'},I:{tag:'input',type:'date','class':'jsFields  divLine iBg_deadLine',name:'dueDate',value:D.dueDate}},
	{L:{textNode:'Responsable:'},node:userAssg},
	{L:{textNode:'Detalles:'},I:{tag:'textarea',type:'text','class':'jsFields',name:'actNote',textNode:D.actNote}},
	];
	var rows = $1.T.tbFieldsLeft(Lis);
	wrap.appendChild(rows);
	var iResp = $1.t('div');
	var iSend = $1.T.btnSend({textNode:'Guardar'},{f:'PUT '+$Tick.apir.a+'add.form', getInputs:function(){ return vPost+'&'+$1.G.inputs(wrap); },
		func:function(Jq1){
			if(Jq1.errNo){ $ps_DB.response(iResp,Jq1) }
			else{ $1.delet(bkFull)
				if(P.func){ P.func(Jq1); }
				else{
					//Jq1.wboId = $4.p.view;
					//$5a.JQ.A[Jq1.addKard.keyO] = Jq1.addKard;
					//$2io.wbo.emit(Jq1); delete(Jq1.wboId);
				}
			}
		}
	});
	wrap.appendChild(iResp); wrap.appendChild(iSend);
	var bkFull = $1.Win.openBk(wrap,{ret:true});
	ps_DOM.body.appendChild(bkFull);
}
}

$Tick.updF = {
uls:function(){
	for(var i in $Tick.Gr){
		var ch = $1.q('._5a_uls_'+i);
		if(!$1.q('li',ch)){ ch.style.display = 'none'; }
	}
},
status:function(P,P1){
	P1=(P1)?P1:{};
	return ps_DOM.Tag.divSelect({sel:{},classBtn:'boxi1', opts:$5a.Vs.actStatus, selected:P.actStatus,iBgOpts:'actStatus_',classWrap:'input',
	func:function(This){ $5a.S.updField({keyO:P1.keyO, movType:'actStatus',to:This.k, actId:P.actId, 
	func:function(){ $Tick.load('add',P,{actStatus:This.k}); }, callf:P1.callf}); }});
},
type:function(P,P1){
	P1=(P1)?P1:{};
	return ps_DOM.Tag.divSelect({sel:{}, opts:$5a.Vs.actType, selected:P.actType,iBgOpts:'',classWrap:'input',
	func:function(This){ $5a.S.updField({keyO:P1.keyO, movType:'actType', to:This.k, actId:P.actId, func:function(){
		$Tick.load('add',P,{actType:This.k});
	}, callf:P1.callf}); }
	});;
},
prio:function(P,P1){
	P1=(P1)?P1:{};
	return ps_DOM.Tag.divSelect({sel:{'class':'jsFields',name:'actPriority'}, classBtn:'boxi1', opts:$5a.Vs.actPriority, selected:P.actPriority,clsFa:'prio_',classWrap:'input',func:function(This){ $5a.S.updField({keyO:P1.keyO, movType:'actPriority', to:This.k, actId:P.actId, func:function(){ $Tick.load('add',P,{actPriority:This.k}); }, callf:P1.callf}); }});;
},
userAssg:function(P,P1){
	P1=(P1)?P1:{};
	return $1h.userAssg_inline(P,{
	func:function(JqU){ JqU =(JqU)?JqU:{};
		$5a.S.updField({keyO:P1.keyO, movType:'userAssg', to:JqU.userId, to2:JqU.userName, actId:P.actId, func:function(){ $Tick.load('add',P,{userAssg:JqU.userId, userAssgName:JqU.userName}); }, callf:P1.callf})
	}});
}
}

$Tick.f = {//fields
ckStatus:function(D){
	var ck2 = $1.T.ckLabel({'class':'',name:'ck',checked:(D.completed=='Y')});
	var ck = ck2.childNodes[0];
	ck.onclick = function(){
		var stat = (this.checked)?'completed':'none';
		$r.get({f:'PUT '+$Tick.apir.a+'updF', inputs:'actId='+D.actId+'&movType=actStatus&to='+stat, func:function(){ $Tick.load('add',D,{actStatus:stat}); }});
	}
	return ck2;
},
wrap:function(D){
	var wrap = $1.t('div',{'class':'_o_activity_'+D.actId+' cardCanvas cardCanvas_drag_item _5a_cardIdWrap_'+D.actId+' _prio_bl_'+D.actPriority,id:'_5a_cardIdWrap_'+D.actId+'_'+D.wboListId,keyo:D.keyO,actId:D.actId});
	var froM = (D.Odnd && D.Odnd.fromType) ? D.Odnd.fromType : D.wboListId;
	wrap.Odnd = {actId:D.actId,from:froM}
	return wrap;
},
asunt:function(D,P){P=(P)?P:{};
	var wr = $1.t('div',{'class':'cardCv_asunt actAsunt'});
	var t = $5a.Vs.actType[D.actType];
	var span = $1.t('span',{'class':'_5afie_actAsunt', title:t,textNode:D.actAsunt});
	wr.appendChild(span);
	if(P.open){ wr.onclick = function(){ $Tick.Ht.winTick(D); } }
	return wr;
},
asuntT:function(D,P){P=(P)?P:{};
	var wr = $1.t('span',{'class':'cardCv_asunt actAsunt',style:'margin-right:0.5rem;'});
	var t = $5a.Vs.actType[D.actType];
	var span = $1.t('span',{'class':'_5afie_actAsunt', title:t,textNode:D.actAsunt});
	wr.appendChild(span);
	if(P.open){ wr.onclick = function(){ $Tick.Ht.winTick(D); } }
	return wr;
},
asuntTy:function(D,P){
	wr = $Tick.f.asuntT(D,P);
	var t = $5a.Vs.actType[D.actType];
	var span = $1.t('span',{'class':'_5afie_actType ico ico_actType_'+D.actType, title:t,style:'margin-right:0.5rem;'});
	wr.insertBefore(span,wr.firstChild);
	return wr;
},
prio:function(D){
	var actPrio = $5a.Vs.actPriority[D.actPriority];
	var span = $1.t('span',{'class':'_5afie_actPriority fa fa_prio_'+D.actPriority, title:actPrio,style:'margin-right:0.25rem;'});
	return span;
},
status:function(D,text){
	var t = $5a.Vs.actStatus[D.actStatus];
	var te = (text)?t:' ';
	var span = $1.t('span',{'class':'_5afie_actStatus ico ico_actStatus_'+D.actStatus, title:t,textNode:te});
	if(D.actStatus=='none'){ span.style.display = 'none'; }
	return span;
}
,
dueDate:function(D,cont){
	var d = D.dueDate;
	var te = $2d.f(D.dueDate,'mmm d',{is0:''});
	var i0 = $2d.is0(d);
	var cssdue = (!i0 && d<$2d.today)?'color:#F00; font-weight:bold;':'';
	cssdue +=(i0)?'display:none;':'';
	var span = $1.t('span',{'class':'_5afie_dueDate', title:'Vencimiento',textNode:te,style:'padding-right:0.25rem;'+cssdue});
	if(cont){ cont.appendChild(span); }
	else{ return span; }
},
doDate:function(D,cont){
	var d = D.doDate;
	var te = $2d.f(D.doDate,'mmm d H:iam',{is0:''});
	var i0 = $2d.is0(d);
	var ms = $2d.f(d,'ms');
	var cssdue = (!i0 && ms<$2d.todayMs)?'color:#F00; font-weight:bold;':'';
	cssdue +=(i0)?'display:none;':'';
	var span = $1.t('span',{'class':'_5afie_doDate', title:'Programado para',textNode:te,style:'padding-right:0.25rem;'+cssdue});
	if(cont){ cont.appendChild(span); }
	else{ return span; }
},
endDate:function(D,cont){
	var d = D.endDate;
	var te = $2d.f(D.endDate,'mmm d H:iam',{is0:''});
	var i0 = $2d.is0(d);
	var ms = $2d.f(d,'ms');
	var cssdue = (!i0 && ms<$2d.todayMs)?'color:#F00; font-weight:bold;':'';
	cssdue +=(i0)?'display:none;':'';
	var span = $1.t('span',{'class':'_5afie_doDate', title:'Finaliza en',textNode:te,style:'padding-right:0.25rem;'+cssdue});
	if(cont){ cont.appendChild(span); }
	else{ return span; }
},
type:function(D,text){
	var t = $5a.Vs.actType[D.actType];
	var te = (text)?t:' ';
	var span = $1.t('span',{'class':'_5afie_actType ico ico_actType_'+D.actType, title:t,textNode:te});
	return span;
},
note:function(D){
	var span =$1.t('span',{'class':'fa fa_note', title:'Incluye descripción...'});
	if(D.actNote==''){ span.style.display = 'none'; }
	return span;
},
calBot:function(D,Pc){Pc=(Pc)?Pc:{};
	var omit = (Pc.omit)?Pc.omit:'';
	var wrap = $1.t('div',{'class':'cardCv_bottom'});
	wrap.appendChild($Tick.f.status(D));
	if(!omit.match(/prio/)){ wrap.appendChild($Tick.f.prio(D)); }
	wrap.appendChild($Tick.f.note(D));
	return wrap;
},
userAssg:function(D,text){
	var span = $1.t('span',{'class':'fa fa_user',textNode:' '+D.userAssgName,title:'Usuario Responsable'},span);
	if(D.userAssgName==''){ span.style.display = 'none'; }
	return span;
}
}

$Tick.Ht = {
winTick:function(Jqp){
	var wrap = $1.t('div',{'class':'_o_activity_'+Jqp.actId});
	$ps_DB.get({file:'GET '+$5a.api+'O.float', inputs:'actId='+Jqp.actId, 
		func:function(Jq){
	var P = (Jq.DATA)?Jq.DATA:{};
	var wrapTop = $1.t('div',{style:'padding:10px; background:#F6F6F6; border-bottom:1px solid #DDDDDD;'});
	wrap.appendChild(wrapTop);
	var h5 = $1.t('h5',{'class':'_5afie_actAsunt', textNode:P.actAsunt});
	wrapTop.appendChild(h5);
		var wrapBar = $1.t('div',{style:'padding:0.625rem; background:#F6F6F6; border-bottom:0.0625rem solid #DDDDDD; margin-bottom:0.375rem; font-size:0.6875rem;'});
	wrapBar.appendChild($1.t('b',{textNode:'Programada: '}));
	wrapBar.appendChild($1.t('span',{textNode:$2d.f(P.doDate,'mmm d H:iam',{is0:''})}));
	if(P.doDate!=P.endDate){
		wrapBar.appendChild($1.t('b',{textNode:' | Finaliza: '}));
		wrapBar.appendChild($1.t('span',{textNode:$2d.f(P.endDate,'mmm d H:iam',{is0:''})}));
	}
	wrapBar.appendChild($1.t('b',{textNode:' | Duración: '}));
	wrapBar.appendChild($1.t('span',{textNode:P.timeDuraText,'class':'_5afie_timeDuraText'}));
	wrapBar.appendChild($1.t('b',{textNode:' | Vence: '}));
	wrapBar.appendChild($1.t('span',{'class':'_5afie_dueDate',textNode:$2d.f(P.dueDate,'mmm d',{is0:''})}));
		wrap.appendChild(wrapBar);
	if(P.actPlace){
		var wrapOt = $1.t('div',{style:'padding:0.625rem; background:#F6F6F6; border-bottom:0.0625rem solid #DDDDDD; margin-bottom:0.375rem; font-size:0.6875rem;'});
		var place = $1.t('div');
		place.appendChild($1.t('span',{'class':'iBg iBg_visit',textNode:' Lugar: '}));
		place.appendChild($1.t('span',{textNode:P.actPlace}));
		place.place = P.actPlace;
		place.onclick = function(){
			window.open('https://google.com/maps/search/'+this.place);
		}
		wrapOt.appendChild(place);
		wrap.appendChild(wrapOt);
	}
	if(P.actNote != ''){
		wrap.appendChild($1.t('p',{'class':'textarea', textNode:P.actNote}));
	}
	var wrapBar1 = $1.t('div',{style:'padding:0.625rem; background:#F6F6F6; border-bottom:0.0625rem solid #DDDDDD; margin-bottom:0.375rem; font-size:0.6875rem;'});
		var edit = $1.T.btnFa({fa:'fa_pencil',textNode:' ',title:'Modificar', P:P, func:function(T){ $Tick.Ad.form({vPost:'targetType='+T.P.targetType+'&targetRef='+T.P.targetRef+'&actId='+T.P.actId, actId:T.P.actId,
		func:function(Jqr){ $Tick.load('add',Jqr.nA); }})
		}}); wrapBar1.appendChild(edit);
	var selType = $Tick.updF.type(P);
	wrapBar1.appendChild(selType);
	var selStatus = $Tick.updF.status(P);
	wrapBar1.appendChild(selStatus);
	var selPrio = $Tick.updF.prio(P);
	wrapBar1.appendChild(selPrio);
	var userAssg = $Tick.updF.userAssg(P);
	wrapBar1.appendChild(userAssg);
	wrap.appendChild(wrapBar1);
	P.opt = {relations:'Y'};
	var wrapMenu = $Tick.Ht.wins(P,wrap);
	wrap.appendChild(wrapMenu);
	}
	});
	var wrapBk = $1.Win.openBk(wrap,{ret:true,winWi:'50%'});
	wrapBk.id = 'ActivityWinFloatWrap';
	ps_DOM.body.appendChild(wrapBk);
}
,
wins:function(P,wrap){
		var opt = (P.opt)?P.opt:{comment:'Y',filedUpd:'Y',checkList:'Y'};
		var objTy = {targetType:'activity',targetRef:P.actId,
		MR:{userAssg:P.userAssg, userAssgName:P.userAssgName, cardId:P.cardId, cardName:P.cardName},
		FR:{activity:P.actId,bussPartner:$o.T.bussPartner}
	};
		var objCt = {targetType:'activity',targetRef:P.actId,FR:{activity:P.actId}};
		var objTyFiles = $js.push({'ADMS_uploadFilesSubDir':'boards/'},objCt);
		var objTypC = $js.push({contextType:'wboList',contextRef:P.wboListId},objCt);
		var wrapMenu = $1.t('div');
		var P_Log = {reload:true, objType:$5a.objType, objRef:P.actId};
		var P_members = {reload:true, objType:$5a.objType, objRef:P.actId};
		var P_Src = {reload:true, objpType:$5a.objType, objpRef:P.actId, relations:'bussPartner'};
		var liA = {}; 
		var Opts = {};
		Opts['comment'] = {li:{textNode:'Comentarios','class':'active fa fa_comment',winClass:'win5c'}};
		wrapMenu.appendChild($3.F.$5c(objTypC,0,{pare:wrapMenu}));
		Opts['fileUpd'] = {li:{textNode:'Archivos','class':'fa fa_fileUpd',winClass:'win5f', func:function(){ $o.Ht.$5f({},{pare:wrapMenu}); } }};
		wrapMenu.appendChild($o.Ht.$5f(objTyFiles,{getList:'N'}))
		if(opt.checkList){
		Opts['checkList'] = {li:{textNode:'CheckList','class':'fa fa_checklist',winClass:'win5ck_R', func:$5b.R.$5a2.get, funcPars:objTyFiles}};
			wrapMenu.appendChild($5b.R.$5ck.get(P,'noLoad'));
		}
		if(opt.members){
			Opts['members'] = {li:{textNode:'\u00A0','class':'fa fa_user',winClass:'winMembers', func:$o.Members.get, funcPars:P_members}};
			wrapMenu.appendChild($o.Members.get(P_members,'noLoad'));;
		}
		if(opt.relations){
			Opts['relations'] = {li:{textNode:'\u00A0','class':'fa fa_link',winClass:'winoSrc', func:function(){ $o.Src.get(P_Src); } }};
			wrapMenu.appendChild($o.Src.get(P_Src,'noLoad'));;
		}
		if(opt.boards){
		Opts['boards'] = {li:{textNode:'Tableros','class':'fa fa_kanban',winClass:'win5b_R', func:$5b.R.$5a.get, funcPars:P}};
		wrapMenu.appendChild($5b.R.$5a.get(P,'noLoad'));
		}
		Opts['history'] = {li:{textNode:'Historial','class':'fa fa_history',winClass:'win6Obj', func:$3.F.$6Obj, funcPars:P_Log}};
		var menu = ps_DOM.Menu.inLine(Opts,wrap);
		wrapMenu.insertBefore(menu,wrapMenu.firstChild);
		wrapMenu.appendChild($3.F.$6Obj(P_Log,'noLoad'));
		return wrapMenu;
	},
btnAdd:function(P,D){
	var D=(D)?D:{};//D son para preconfigurar form act
	var os = $js.push({'class':'fa faBtn fa_plusCircle'},P.btn);
	var add = $1.t('button',os);
	add.onclick = function(){
	$Tick.Ad.form({func:function(Jqr){ $Tick.load('add',Jqr.nA); }},D)
	}
	return add;
},
}

var $5a2 = {
api:'/xs/5a2',
F:{
	line:function(P){
	var P = (P) ? P : {};
	var wrapGen = $1.t('div',{style:'padding:3px 0;'});
	var wrapBarPerc = $1.t('div');
	var wrapList = $1.t('div',{'class':'checkListSimpleWrapList',id:'checkListSimpleWrapList'});
	var wrapForm = $1.t('div',{'class':'checkListSimple_addWrap'});
	var iSave = $1.t('input',{type:'button','class':'btn iBg_save btnOnRight'});
	iSave.onclick = function(){ $5a2.S.line(this,wrapForm,P,wrapList); }
	var vPost = 'targetType='+P.targetType+'&targetRef='+P.targetRef;
	vPost += (P.relType)?'&relType='+P.relType:'';
	vPost += (P.relRef)?'&relRef='+P.relRef:'';
	vPost += (P.relRef2)?'&relRef2='+P.relRef2:'';
	var iText = $1.t('input',{type:'text','class':'jsFields boxi1 sactAsunt',name:'sactAsunt',placeholder:'Añadir CheckList...',style:'width:98%;',O:{vPost:vPost}});
	iText.onkeyup = function(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode;
		if(charCode ==13){ $5a2.S.line(this,wrapForm,P,wrapList); }
	}
	wrapForm.appendChild(iText); wrapForm.appendChild(iSave);
	wrapGen.appendChild(wrapForm);
	wrapGen.appendChild(wrapBarPerc); //0
	wrapGen.appendChild(wrapList);
	//wrapGen.appendChild(vars);
	$5a2.V.line(P,wrapList);
	return wrapGen;
}
}
,
S:{
line:function(obj,objWrap,P,wraList){
	var This = obj;
	var vPost = $1.G.inputs(objWrap);
	ADMS_DB.get({f:'POST '+$5a2.api, inputs:vPost,
	func:function(Jq1){
		if(Jq1.errNo){ $1.Win.message(Jq1); }
		else{
			var globRoom = {objType:$o.T.checkList, objRef:'newS', targetType:P.targetType, targetRef:P.targetRef, movType:'add'};
			$2io.worksp.emit(globRoom);
			ps_DOM.clearInps(objWrap); $5a2.V.line(P,wraList);
		}
	}
	});
}
}
,
V:{
line:function(P,wrapList){
	$1.clear(wrapList);
	var vPost = 'targetType='+P.targetType+'&targetRef='+P.targetRef;
	vPost += (P.relType)?'&relType='+P.relType:'';
	vPost += (P.relRef)?'&relRef='+P.relRef:'';
	vPost += (P.relRef2)?'&relRef2='+P.relRef2:'';
	if(P.getPars){ for(var m in P.getPars){ vPost+='&'+m+'='+P.getPars[m]; } }
	ADMS_DB.get({f:'GET '+$5a2.api, inputs:vPost,
		func:function(Jq){
		if(Jq.errNo){ $ps_DB.response(wrapList,Jq); }
		else{
			for(var i in Jq.DATA){ var D = Jq.DATA[i];
				var ideWr = 'ckListFloatSimpleIdWrap'+D.sactId;
				var ide = 'ckListFloatSimpleId'+D.sactId;
				var ck = $1.t('div',{'class':'_o_'+$o.T.checkList+'_'+D.sactId+' item',id:ideWr});
				var iCk = $1.t('input',{type:'checkbox','class':'_5ck_isChecked _5ck_checkStatus_'+D.targetRef,id:ide,checked:(D.isCompleted=='Y')});
				iCk.vPost = 'sactId='+D.sactId+'&targetType='+D.targetType+'&targetRef='+D.targetRef;
				iCk.globRoom = {objType:$o.T.checkList,objRef:D.sactId,targetType:D.targetType,targetRef:D.targetRef,movType:'-'}
				iCk.onclick = function(){
					var globRoom = this.globRoom;
					if(this.checked){ globRoom.movType='completed';}
					else {globRoom.movType='unCompleted'; }
					var vPost = (this.checked) ? 'isCompleted=Y' : 'isCompleted=N';
					vPost += '&'+this.vPost;
					$ps_DB.get({f:'PUT '+$5a2.api+'/mark',inputs:vPost,func:function(Jq2){
						if(!Jq2.errNo){ $2io.worksp.emit(globRoom); }
					}});
				}
				var iLabel = $1.t('label',{textNode:D.sactAsunt,'for':ide});
				ck.appendChild(iCk);
				ck.appendChild(iLabel);
				var idate = $1.t('input',{type:'date','class':'jsFields',name:'doDate'});
				idate.vPost = 'sactId='+D.sactId;
				idate.onclick = function(){ var pare = this.parentNode;
					var vPost = this.vPost+'&doDate='+this.value;
					$ps_DB.get({f:'DELETE '+$5a2.api,inputs:vPost,func:function(Jq2){
						if(!Jq2.errNo){ $1.delet(pare); $5a2.Sta.upd(D); }
					}});
				}
				//ck.appendChild(idate);
				var iCl = $1.t('input',{type:'button','class':'btn iBg_closeSmall btn2Right'});
				iCl.vPost = 'sactId='+D.sactId+'&targetType='+D.targetType+'&targetRef='+D.targetRef;
				iCl.globRoom ={objType:$o.T.checkList,objRef:D.sactId,targetType:D.targetType,targetRef:D.targetRef,movType:'delete'};
				iCl.onclick = function(){ var pare = this.parentNode; var globRoom = this.globRoom;
					var vPost = this.vPost; 
					$ps_DB.get({f:'DELETE '+$5a2.api,inputs:vPost,func:function(Jq2){
						if(!Jq2.errNo){
							$2io.worksp.emit(globRoom);
							$1.delet(pare);
						}
					}});
				}
				ck.appendChild(iCl);
				wrapList.appendChild(ck);
			}
			$5a2.Sta.upd(P);
		}
	}
	});
}
}
,
Sta:{
upd:function(P){
	//ck son los items checke
	var ck = $1.q('._5ck_checkStatus_'+P.targetRef,null,'all');
	var spanCk = $1.q('._5ck_countComplet_'+P.targetRef,null,'all');
	var bar = $1.q('._5ck_barCkComplet_'+P.targetRef,null,'all');
	var complet = total = 0;
	for(var i=0; i<ck.length; i++){
		total++;
		if(ck[i].checked){ complet ++; }
	}
	for(var i=0; i<bar.length; i++){
		var perc = (bar[i].maxWidth) ? bar[i].maxWidth : 1;
		perc = perc*((complet/total));
		bar[i].style.width = perc+'rem';
	}
	for(var i=0; i<spanCk.length; i++){
		spanCk[i].innerText = complet+' / '+total;
	}
}
}
}

var $5b = {
api:'/s/5b/A:',
V:{//$V
wboListPriv:{owner:'Solo Propietario',members:'Miembros del Tablero',share:'Compartido'}
,
Perms:{
		_t:{b:'Puede Ver Tablero',l:'Acción en Lista'},
	'owner-owner':{
		b:{a:{owner:'Sí',members:'',share:''}},
		l:{a:{owner:'Agregar',members:'',share:''}}
	},
	'owner-members':{
		b:{a:{owner:'Sí',members:'',share:''}},
		l:{a:{owner:'Agregar',members:'Enviar',share:''}}
	},
	'owner-share':{
		b:{a:{owner:'Sí',members:'',share:''}},
		l:{a:{owner:'Agregar',members:'Enviar',share:'Enviar'}}
	},
	'members-owner':{
		b:{a:{owner:'Sí',members:'Sí',share:''}},
		l:{a:{owner:'Agregar',members:'Ver',share:''}}
	},
	'members-members':{
		b:{a:{owner:'Sí',members:'Sí',share:''}},
		l:{a:{owner:'Agregar',members:'Agregar',share:''}}
	},
	'members-share':{
		b:{a:{owner:'Sí',members:'Sí',share:''}},
		l:{a:{owner:'Agregar',members:'Agregar',share:'Envíar'}}
	},
	'share-owner':{
		b:{a:{owner:'Sí',members:'Sí',share:'Sí'}},
		l:{a:{owner:'Agregar',members:'Ver',share:'Ver'}}
	},
	'share-members':{
		b:{a:{owner:'Sí',members:'Sí',share:'Sí'}},
		l:{a:{owner:'Agregar',members:'Agregar',share:'Ver'}}
	},
	'share-share':{
		b:{a:{owner:'Sí',members:'Sí',share:'Sí'}},
		l:{a:{owner:'Agregar',members:'Agregar',share:'Agregar'}}
	}
}
}
,
ini:function(P){
	if($M.wrap){ cont = $M.wrap; }
	else{ cont = $1.t('div'); }
	$1.clear(cont);
	var p = $1.t('p');
	var goToWbo = $M.href({href:'boards.open','class':'fa fabtn fa_kanban',textNode:'Tableros'});
	var filt = $1.t('div',{id:'_1f_wrap','class':'_1f_wrap'});
	var wrapDraw = $1.t('div',{'id':'_5b_drawer'});
	p.appendChild(goToWbo); cont.appendChild(p);
	cont.appendChild(filt);
	cont.appendChild(wrapDraw);
	$5b.o.get({},wrapDraw);
}
,
numLine_member:1,
o:{
	get:function(P,cont){ var P =(P)?P:{};
		$4.read();
		if($4.p.view){
			$1f.base({placeholder:'Buscar en Asunto y Socio',fk:'wbo1-10',func:$5b.List.get});
			$5b.List.get();
			return '';
		}
		else{ $1.clear($1.q('._1f_wrap')); }
		if(cont){ var cont = cont; }
		else{ var cont = $1.q('.__wrapContent'); }
		$1.clear(cont); 
		var h4 = $1.t('h2',{'class':'head1',textNode:'Tableros'});
		cont.appendChild(h4);
		var wrapList = $1.t('div',{'class':'_5b_wrapBoards'});
		var wTypes = {};
		var vPost = $1.G.inputs(null,'jsFiltVars');
		$ps_DB.get({file:'GET '+$5b.api+'o',inputs:vPost,loade:wrapList,
		func:function(Jq){
			if(Jq.errNo && Jq.errNo != 2){ $ps_DB.response(wrapList,Jq); }
			else{
				var boar = $1.t('div',{'class':'_5b_wrapBoard',style:'display:block; clear:both;'});
				var boarW = $1.t('div');
				boarW.appendChild($1.t('h5',{textNode:'Crear nuevo tablero...'}));
				boarW.onclick = function(){ $5b.o.getForm({}); }
				boar.appendChild(boarW);
				wrapList.appendChild(boar);
				for(var b in $V._5b.wboPriv){
					wTypes[b] = $1.t('div',{'class':'_5b_wrapPrivType'});
					wTypes[b].appendChild($1.t('h4',{'class':'head1',textNode:$V._5b.wboPriv[b]}));
					wrapList.appendChild(wTypes[b]);
				}
				{//assg un board
					var boar = $5b.Draw.board({wboId:'unboard',wboColor:'#FFF',wboName:'Mis Actividades', descrip:'Tablero Automático del Sistema que agrupa todas las actividades.',userName:$0s.userName});
					wTypes['owner'].appendChild(boar);
					var boar = $5b.Draw.board({wboId:'admin',wboColor:'#CCC',wboName:'Administrador (Sys)', descrip:'Actividades de los Usuarios de los grupos donde soy administrador.',userName:$0s.userName});
					wTypes['owner'].appendChild(boar);
				}
				for(var b in Jq.DATA){ var B = Jq.DATA[b];
					boar = $5b.Draw.board(B);
					wTypes[B.wboPriv].appendChild(boar);
				}
			}
		}
		});
		$1.append(cont,wrapList);
	}
	,
	getForm:function(D){ var D=(D)?D:{};
		$5b.numLine_member = 1;
		D.wboId = (D.wboId) ? D.wboId : 'newS'; //wboName:O{vPost}
		$1.delet('_5bFormBoardDefine');
		var wrap = $1.t('div');
		wrap.appendChild($5b.o.F.topInfo(D));
		var wrapMenu = $1.t('div');
		var menu = ps_DOM.Menu.inLine([
		{li:{textNode:'Listas','class':'active',winClass:'winList'}},
		{li:{textNode:'Miembros','class':'',winClass:'winMembers', func:$5b.o.F.members, funcPars:D}}
		],wrap);
			wrapMenu.appendChild(menu);
			wrapMenu.appendChild($5b.o.F.list(D));
			wrapMenu.appendChild($5b.o.F.members(D,{noLoad:true}));
			wrap.appendChild(wrapMenu);
		var resp = $1.t('div');
		var iSend = $1.T.btnSend({func:function(){
		 var vPost = $1.G.inputs(wrap);
			$ps_DB.get({file:'PUT '+$5b.api+'o',inputs:vPost, loade:resp,
			func:function(Jq){ var Jq_0 = (Jq.respChild) ? Jq.respChild[0] : Jq;
				if(Jq.errNo){ $ps_DB.response(resp,Jq); }
				else{ $1.delet(wrapBk); $5b.List.get({wboId:Jq_0.wboId}); }
			}});
		}});
		wrap.appendChild(resp);
		wrap.appendChild(iSend);
		var wrapBk = $1.Win.openBk(wrap,{ret:true,winTitle:'Tablero',winId:'_5bFormBoardDefine',winSize:'medium-f'});
		ps_DOM.body.appendChild(wrapBk);
	}
	,
	F:{
		topInfo:function(D){
			var wrap = $1.t('div');
			var divLine = $1.T.divLinewx({divLine:true, wxn:'wrapx2',L:{textNode:'Nombre Tablero'},I:{tag:'input',type:'text','class':'jsFields',name:'wboName',value:D.wboName,
			O:{vPost:'wboId='+D.wboId}}});
			divLine.appendChild($1.T.divLinewx({wxn:'wrapx8',L:{textNode:'Color'},I:{tag:'input',type:'color','class':'jsFields',name:'wboColor',value:D.wboColor}}));
			var wboPriv = (D.wboPriv) ? D.wboPriv : 'owner';
			var privac = ps_DOM.Tag.divSelect({sel:{'class':'jsFields wboPriv_define',name:'wboPriv'}, classBtn:'boxi1', opts:$V._5b.wboPriv, selected:wboPriv,clsFa:'priv_',classWrap:'input'});
			var privac = $1.T.divLinewx({wxn:'wrapx4',L:{textNode:'Privacidad'},Inode:privac});
			divLine.appendChild(privac);
			wrap.appendChild(divLine);
			var divLine = $1.T.divLinewx({divLine:true, wxn:'wrapx1',L:{textNode:'Descripción'},I:{tag:'textarea','class':'jsFields',name:'descrip',value:D.descrip}});
			wrap.appendChild(divLine);
			return wrap;
		}
		,
		members:function(D,P){ var P=(P)?P:{};
			if(P.noLoad){
				var wrap = $1.t('div',{'class':'winMenuInLine winMembers',style:'display:none;'});
				return wrap;
			}
			var wrap = $1.q('.winMembers');
			var wrapExist = $1.q('.winMembers_board');
			if(wrapExist && wrapExist.tagName){ return true; }
			wrap.classList.add('winMembers_board');
			var numLine = 0;
			function memberList(D2,tBody){
				var mId = 'memberBoard_'+D2.userId;
				var iName = 'U['+$5b.numLine_member+']';
				if(!$1.q('.'+mId,tBody)){//sino existe
					var tr = $1.t('tr');
					if(D2.isList){ var del = $1.T.ckLabel({'class':'jsFields checkSel_trash',name:iName+'[delete]',id:mId}); }
					else{
						var del = $1.t('input',{type:'button','class':'iBg iBg_closeSmall'});
						del.onclick = function(){ $1.delet(tr); }
					}
					var tdDel = $1.t('td');
					tdDel.appendChild(del);
					tr.appendChild(tdDel);
					tr.appendChild($1.t('td',$5b.numLine_member));
				tr.appendChild($1.t('td',{textNode:D2.userName,'class':'jsFields '+mId,fieldData:'Y',O:{vPost:iName+'[userId]='+D2.userId}}));
					tBody.appendChild(tr);
					$5b.numLine_member++;
				}
			}
			$ps_DB.get({file:'GET '+$5b.api+'o.members', inputs:'wboId='+D.wboId,
			func:function(Jq){
				if(Jq.errNo){ $ps_DB.response(wrap,Jq); }
				var tBody = $1.t('tbody');
				var users = $1.t('div');
				var add = $1.t('input',{type:'button','class':'btnAddText iBg_tbaddrow',value:'Añadir Usuario'});
				var l = 1;
				add.onclick = function(){
					$3.user({A:'', func:function(Jq){ Jq.wboId = D.wboId; memberList(Jq,tBody); }});
				}
				users.appendChild(add);
				wrap.appendChild(users);
				var tb = $1.T.table(['','#','Usuario']);
				tb.appendChild(tBody);
				for(var u in Jq.DATA){ Jq.DATA[u].isList = true;
					memberList(Jq.DATA[u],tBody);
				}
				wrap.appendChild(tb);
			}
			});
			
		}
		,
		list:function(D){
			var wrap = $1.t('div',{'class':'winMenuInLine winList winList_board',style:''});
			var wrapExist = $1.q('.winList_board');
			if(wrapExist && wrapExist.tagName){ return true; }
			var wrapList = $1.t('div'); $1.clear(wrap);
			var tb = $1.T.table(['','#','Posic.','Nombre Lista','Puede Añadir Actividades','Color']);
			var tBody = $1.t('tbody');
			tb.appendChild(tBody);
			function addList(D2,tBody){
				var mId = 'listBoard_'+D2.wboListId;
				var iName = 'LIST['+$5b.numLine_member+']';
				if(1){//sino existe
					var tr = $1.t('tr');
					var tdDel = $1.t('td');
					if(!D2.isList){
						var del = $1.t('button',{'class':'iBg iBg_closeSmall'});
						del.onclick = function(){ $1.delet(tr); }
						tdDel.appendChild(del);
					}
					tr.appendChild(tdDel);
					tr.appendChild($1.t('td',$5b.numLine_member));
					var tdMove =  $1.t('td');
					$1.T.upDown(tdMove,tr);
					tr.appendChild(tdMove);
					var td = $1.t('td');
					var liName = $1.t('input',{type:'text','class':'jsFields',name:iName+'[listName]',value:D2.listName,O:{vPost:iName+'[wboListId]='+D2.wboListId}});
					td.appendChild(liName);
					tr.appendChild(td);
					var td = $1.t('td');
					var btn = $1.t('button',{'class':'fa faBtn fa_info'});
					btn.onclick = function(){
						var boardPriv = $1.q('.wboPriv_define');
						var listPriv = $1.q('.listPriv_define',this.parentNode.parentNode);
						$5b.Draw.permsInfo({board:boardPriv,list:listPriv},this.parentNode.parentNode);
					}
					td.appendChild(btn);
					D2.authRel = (D2.authRel && D2.authRel != '') ? D2.authRel : 'owner';
					var sel = ps_DOM.Tag.divSelect({sel:{'class':'jsFields listPriv_define',name:iName+'[authRel]'}, classBtn:'boxi1', opts:$5b.V.wboListPriv,selected:D2.authRel,clsFa:'priv_',classWrap:'input'});
					$1.T.sel({sel:{'class':'jsFields listPriv_define',name:iName+'[authRel]'},opts:$5b.V.wboListPriv,selected:D2.authRel,noBlank:true});
					td.appendChild(sel); tr.appendChild(td);
					var td = $1.t('td');
					D2.listColor = (D2.listColor) ? D2.listColor : '#FFF';
					var color = $1.t('input',{type:'color','class':'jsFields',name:iName+'[listColor]',value:D2.listColor});
					td.appendChild(color); tr.appendChild(td);
					tBody.appendChild(tr);
					$5b.numLine_member++;
				}
			}
			$ps_DB.get({file:'GET '+$5b.api+'o.list', inputs:'wboId='+D.wboId,loade:wrapList,
			func:function(Jq){
				if(D.wboId != 'newS' && Jq.errNo){ $ps_DB.response(wrapList,Jq); }
				if(!Jq.DATA){
					var def = [{listName:'Lista 1'},{listName:'Lista 2'}];
					//var def = [{listName:'Lista 1'},{listName:'Compartido',authRel:'share'}];
					for(var i in def){ addList(def[i],tBody); }
				}
				else for(var u in Jq.DATA){ Jq.DATA[u].isList = true;
					addList(Jq.DATA[u],tBody);
				}
			}
			});
			wrap.appendChild(wrapList);
			wrap.appendChild(tb);
			var users = $1.t('div',{style:'padding:0.5rem 0;'});
			wrap.appendChild(users);
			var add = $1.t('button',{'class':'btnAddText iBg_tbaddrow',textNode:'Añadir Otra Lista '});
				add.onclick = function(){ addList(D,tBody); }
				users.appendChild(add);
			return wrap;
		}
	}
}
,
List:{
	get:function(P){ P=(P)?P:{};
		$4.read();
		P.wboId = (P.wboId) ? P.wboId : $4.p.view;
		$2io.emit('rOpen_workBoard',{wboId:P.wboId});
		P.callf = 'board';
		if($M.wrap){ cont = $M.wrap; }
		else{ var cont = $1.q('.__wrapContent'); }
		$1.clear(cont);
		var wrapContMenu = $1.t('div',{id:'_5a_wrapContMenu'});
		var wrapList = $1.t('div',{id:'_5a_wrapList'});
		function go(g){
				$5a.go(g); $1.clear(wrapList);
				$5a.loadType({Jq:$5a.JQ,P:P});
		}
		$2io.wbo.on(function(Dres){ $5a.JQ = Dres; $5a.loadType({P:P}); });
		$2io.worksp.on();
		var vPost = $1.G.inputs($1.q('#_1f_wrap'),'jsFiltVars');
		var vPost = 'wboId='+P.wboId+'&'+vPost;
		$ps_DB.get({file:'GET '+$5b.api+'List',inputs:vPost,loade:wrapList,
		func:function(Jq){
			Jq.WBOL = {};
			for(var l2 in Jq.L){ var Li = Jq.L[l2];
				Jq.WBOL[Li.wboListId] = Li.listName;
			}
			$5a.JQ = Jq;
			cont.appendChild($1.t('br'));
			var h4 = $1.t('h4',{textNode:Jq.wboName});
			cont.appendChild(h4);
			var vtype = $4.p.vtype;
			var opts = $1.t('div',{style:'padding:0.5rem 0;'});
			var adminUnb = P.wboId.match(/(unboard|admin)/);
			var _3 = (adminUnb && (!vtype || vtype == 'list')) ? 'active' : '';
			var _1 = (!adminUnb && (!vtype || vtype == 'board')) ? 'active' : '';
			var _2 = (vtype == 'prio') ? 'active' : '';
			var _4 = (vtype == 'status') ? 'active' : '';
			var _5 = (vtype == 'week') ? 'active' : '';
			var _6 = (vtype == 'month') ? 'active' : '';
			var _7 = (vtype == 'timeline') ? 'active' : '';
			var _8 = (vtype == 'type') ? 'active' : '';
			var _log = (vtype == 'log') ? 'active' : '';
			var P_Log = {wrapCont:wrapList, objType:'workBoard',objRef:P.wboId};
			var add = $5a.F.btnAdd({wboListId:null},ps_DOM.body,{});
			if($4.p.view == 'admin' || P.wboId == 'admin'){
				vtype = (vtype)?vtype:'list';
				if(!$1.q('#usgGr_wrap')){
				$1.q('#_1f_wrap').appendChild($1.t('b','Grupo: '));
				var gr = $1.T.sel({sel:{'id':'usgGr_wrap','class':'jsFiltVars',name:'uGroupId'},opts:Jq.usgGr});
				$1.q('#_1f_wrap').appendChild(gr);
				}
				var Lis = [
				{li:{textNode:'Lista','class':_3+' fa fa_list', func:go, funcPars:{view:P.wboId,vtype:'list'}}},
				{li:{textNode:'Prioridad','class':_2+' fa fa_prio', func:go, funcPars:{view:P.wboId,vtype:'prio',vt2:'covey'}}},
				{li:{textNode:'Estado','class':_4+' fa fa_checkList', func:go, funcPars:{view:P.wboId,vtype:'status',vt2:'kanban'}}},
				{li:{textNode:'Tipo','class':_8+'', func:go, funcPars:{view:P.wboId,vtype:'type'}}},
				{li:{textNode:'Mes','class':_6+'', func:go, funcPars:{view:P.wboId,vtype:'month'}}},
				{li:{textNode:'Semana','class':_5+'', func:go, funcPars:{view:P.wboId,vtype:'week'}}},
				{li:{textNode:'Timeline','class':_7+'', func:go, funcPars:{view:P.wboId,vtype:'timeline'}}},
				{li:{textNode:'\u00A0','class':_8+' iBg iBg_history1', func:$3.F.$6Obj, funcPars:P_Log}}
				];
			}
			else if($4.p.view == 'unboard' || P.wboId == 'unboard'){
				vtype = (vtype && vtype != 'board')?vtype:'list';
				var Lis = [{li:{node:add}},
				{li:{textNode:'Lista','class':_3+' fa fa_list', func:go, funcPars:{view:P.wboId,vtype:'list'}}},
				{li:{textNode:'Prioridad','class':_2+' fa fa_prio', func:go, funcPars:{view:P.wboId,vtype:'prio',vt2:'covey'}}},
				{li:{textNode:'Estado','class':_4+' fa fa_checkList', func:go, funcPars:{view:P.wboId,vtype:'status',vt2:'kanban'}}},
				{li:{textNode:'Tipo','class':_8+'', func:go, funcPars:{view:P.wboId,vtype:'type'}}},
				{li:{textNode:'Mes','class':_6+'', func:go, funcPars:{view:P.wboId,vtype:'month'}}},
				{li:{textNode:'Semana','class':_5+'', func:go, funcPars:{view:P.wboId,vtype:'week'}}},
				{li:{textNode:'Timeline','class':_7+'', func:go, funcPars:{view:P.wboId,vtype:'timeline'}}},
				{li:{textNode:'\u00A0','class':_8+' iBg iBg_history1', func:$3.F.$6Obj, funcPars:P_Log}}
				];
			}
			else{
			vtype = (vtype && vtype != 'board')?vtype:'board';
			var Lis = [{li:{node:add}},
			{li:{textNode:'Kanban','class':_1+' fa fa_kanban',winClass:'winList',func:go, funcPars:{view:P.wboId,vtype:'board',vt2:'board'} }},
			{li:{textNode:'Lista','class':_3+' fa fa_list', func:go, funcPars:{view:P.wboId,vtype:'list'}}},
			{li:{textNode:'Prioridad','class':_2+' fa fa_prio', func:go, funcPars:{view:P.wboId,vtype:'prio',vt2:'covey'}}},
			{li:{textNode:'Estado','class':_4+' fa fa_checkList', func:go, funcPars:{view:P.wboId,vtype:'status',vt2:'kanban'}}},
			{li:{textNode:'Tipo','class':_8+'', func:go, funcPars:{view:P.wboId,vtype:'type'}}},
			{li:{textNode:'Semana','class':_5+'', func:go, funcPars:{view:P.wboId,vtype:'week'}}},
			{li:{textNode:'Mes','class':_6+'', func:go, funcPars:{view:P.wboId,vtype:'month'}}},
			{li:{textNode:'Timeline','class':_7+'', func:go, funcPars:{view:P.wboId,vtype:'timeline'}}},
			{li:{textNode:'\u00A0','class':_8+' iBg iBg_history1', func:$3.F.$6Obj, funcPars:P_Log}}
			];
			}
			var menu = ps_DOM.Menu.inLine(Lis,cont);
			opts.appendChild(menu);
			opts.appendChild(wrapContMenu);
			cont.appendChild(opts);
			cont.appendChild(wrapList);
			if(Jq.errNo){ $ps_DB.response(wrapList,Jq); }
			else if(Jq.A && Jq.A.errNo){ $ps_DB.response(wrapList,Jq.A); }
			else{
				go({view:P.wboId,vtype:vtype});
			}
			//call to drag callf
		}//func
		});
	}
	,
	getForm:function(D){
		var wboId = (D.wboId) ? D.wboId : ''; //wboName:O{vPost}
		var wboListId = (D.wboListId) ? D.wboListId : 'newS'; //wboName:O{vPost}
		var wrap = $1.t('div');
		var divLine = $1.T.divLinewx({divLine:true, wxn:'wrapx2',L:{textNode:'Nombre Lista'},I:{tag:'input',type:'text','class':'jsFields',name:'listName',value:D.listName,
			O:{vPost:'wboId='+wboId+'&wboListId='+wboListId}}});
		D.authRel = (D.authRel && D.authRel !='') ? D.authRel : 'owner';
		divLine.appendChild($1.T.divLinewx({wxn:'wrapx3',L:{textNode:'Puede Añadir Actividades',abbr:20},I:{tag:'select',sel:{'class':'jsFields',name:'authRel'},opts:$5b.V.wboListPriv,selected:D.authRel}}));
		wrap.appendChild(divLine);
		var resp = $1.t('div');
		var iSend = $1.T.btnSend({func:function(){
			$ps_DB.get({file:'PUT '+$5b.api+'List',inputs:$1.G.inputs(wrap), loade:resp,
			func:function(Jq){
				if(Jq.errNo){ $ps_DB.response(resp,Jq); }
				else{ $1.delet(wrapBk); $5b.List.get({wboId:Jq.wboId}); }
			}});
		}});
		wrap.appendChild(resp);
		wrap.appendChild(iSend);
		var wrapBk = $1.Win.open(wrap,{winTitle:'Lista de Tablero',winId:'_5bFormBoardDefineList',winSize:'medium'});
		ps_DOM.body.appendChild(wrapBk);
	}
}
,
Draw:{
	board:function(B){//dibujar board en tablero
		var styBg = (B.wboColor == '#000000') ? 'background-color:'+B.wboColor+'; color:#FFF;' : 'background-color:'+B.wboColor+';';
		styBg = (B.wboColor == '#FFFFFF') ? 'background-color:'+B.wboColor+'; color:#000;' : styBg;
		var boar = $1.t('div',{'class':'_5b_wrapBoard',style:styBg});
		var boarW = $1.t('div',{style:'position:relative;'});
		var h3 =  $1.t('a',{textNode:B.wboName});
		boarW.onclick = function(){ $5a.go({view:B.wboId}); }
		boarW.appendChild(h3);
		boar.appendChild(boarW);
		boarW.appendChild($1.t('div',{textNode:B.descrip,style:'font-size:0.7rem; padding:0.25rem 0;'}));
		boarW.appendChild($1.t('div',{'class':'fa fa_priv_'+B.wboPriv,textNode:B.userName,style:'position:absolute; bottom:0; font-size:0.6rem;'}));
		if(B.wboId != 'unboard'){
			var mod = $1.t('input',{type:'button','class':'iBg iBg_edit',style:'position:absolute; top:0px; right:0px;'});
			mod.onclick = function(){ $5b.o.getForm(B); }
			boar.appendChild(mod);
		}
		return boar;
	}
	,
	permsInfo:function(P,trPare){
		var wrap = $1.t('div');
		var nameList = trPare.childNodes[3].childNodes[0].value;
		wrap.appendChild($1.t('h3',{textNode:nameList,'class':'head1'}));
		wrap.appendChild($1.t('span',{textNode:'Tablero:'}));
		wrap.appendChild($1.t('span',{'class':'spanLine',textNode:P.board.getAttribute('inerText')}));
		wrap.appendChild($1.t('span',{textNode:' | Lista:'}));
		wrap.appendChild($1.t('span',{'class':'spanLine',textNode:P.list.getAttribute('inerText')}));
		var tb = $1.t('table',{'class':'table_zh'});
		var tHead = $1.t('thead');
		var tr0 = $1.t('tr');
		tr0.appendChild($1.t('td',{textNode:''}));
		tr0.appendChild($1.t('td',{textNode:'Propietario'}));
		tr0.appendChild($1.t('td',{textNode:'Miembro'}));
		tr0.appendChild($1.t('td',{textNode:'Usuario'}));
		tHead.appendChild(tr0);
		tb.appendChild(tHead);
		var tBody = $1.t('tbody');
		var O = $5b.V.Perms[P.board.value+'-'+P.list.value];
		var tT = $5b.V.Perms._t;
		for(var e in O){ 
			var tr = $1.T.tbtr([tT[e], O[e].a.owner, O[e].a.members,O[e].a.share]);
			tBody.appendChild(tr);
		}
		
		tb.appendChild(tBody);
		wrap.appendChild(tb);
		var ul = $1.t('ul',{style:'padding:0.25rem; font-size:0.75rem;'});
		ul.appendChild($1.t('li','El campo en blanco equivale a ningúna acción.'));
		ul.appendChild($1.t('li','Enviar es la opción que permite a un usuario ponerte una actividad en la lista, sin que este vea el contenido.'));
		wrap.appendChild(ul);
		$1.Win.openBk(wrap,{onBody:true,winSize:'auto',winId:'_5bPermsInfo',winTitle:'Permiso Tablero y Lista'});
	}
}
,
R:{
$5a:{
	get:function(P,noLoad){
		var wrap = $1.t('div',{'class':'winMenuInLine win5b_R',style:'display:none;'});
		if(noLoad){ return wrap; }
		var wrap = $1.q('.win5b_R');
		var wrapExist = $1.q('.win5b_R_open');
		if(wrapExist && wrapExist.tagName){ return true; }
		wrap.classList.add('win5b_R_open');
		wrap.appendChild($1.t('br'));
		wrap.appendChild($1.t('h4',{textNode:'Tableros donde está la Actividad','class':'head1'}));
		wrap.appendChild($1.t('div',{textNode:'Una actividad o tarjeta puede estar en 1 o más tableros para que sea visible para diferentes personas.'}));
		var p = $1.t('p');
		var tBody = $1.t('tbody');
		function addLine(L,tBody){
			var wid = 'wboList_'+L.wboListId;
			if(!$1.q('.'+wid,tBody)){
				var tr = $1.t('tr',{'class':wid});
				var tdDel = $1.t('td');
				var del = $1.t('input',{type:'button','class':'iBg iBg_closeSmall',title:'Eliminar'});
				del.onclick = function(){ var trP = this.parentNode.parentNode;
					$1.Win.confirm({text:'Se va a eliminar la actividad de la Lista del Tablero',func:function(){
					$ps_DB.get({file:'PUT '+$5b.api+'R',inputs:'delete=Y&wboListId='+L.wboListId+'&actId='+P.actId, func:function(Jq3){
						if(!Jq3.errNo){ $1.delet(trP); }
					}}); }
					});
				}
				tdDel.appendChild(del);
				tr.appendChild(tdDel);
				var td = $1.t('td');
				td.appendChild($1.t('b',{textNode:L.wboName}));
				td.appendChild($1.t('span',' ⤏ '));
				td.appendChild($1.t('b',{textNode:L.listName}));
				tr.appendChild(td);
				tBody.appendChild(tr);
			}
		}
		var ope = $1.t('input',{type:'button','class':'btnAddText iBg_search2',value:'Añadir a Lista de Tablero...'});
		ope.onclick = function(){
			$3.wboList({A:'list', func:function(Jq2){
				if(!$1.q('.wboList_'+Jq2.wboListId,tBody)){
					$ps_DB.get({file:'PUT '+$5b.api+'R',inputs:'wboListId='+Jq2.wboListId+'&actId='+P.actId, func:function(Jq3){
						addLine(Jq2,tBody);
				}});
				}
			}},wrap);
		}
		p.appendChild(ope);
		wrap.appendChild(p);
		var vPost = 'actId='+P.actId;
		$ps_DB.get({file:'GET '+$5b.api+'R', inputs:vPost, func:function(Jq){
			var tb = $1.T.table(['#','Lista de Tablero']);
			for(var r in Jq.DATA){ var L = Jq.DATA[r];
				addLine(L,tBody);
			}
			tb.appendChild(tBody);
			wrap.appendChild(tb);
		}});
	}
}
,
$5ck:{
	get:function(P,noLoad){
		var wrap = $1.t('div',{'class':'winMenuInLine win5ck_R',style:'display:none;'});
		if(noLoad){ return wrap; }
		var wrap = $1.q('.win5ck_R');
		var wrapExist = $1.q('.win5ck_R_open');
		if(wrapExist && wrapExist.tagName){ return true; }
		wrap.classList.add('win5ck_R_open');
		wrap.appendChild($1.t('h4','Lista de Chequeo'));
		wrap.appendChild($5ck.F.line(P))
		var tBody = $1.t('tbody');
		function addLine(L,tBody){
			var wid = 'wboList_'+L.wboListId;
			if(!$1.q('.'+wid,tBody)){
				var tr = $1.t('tr',{'class':wid});
				var tdDel = $1.t('td');
				var del = $1.t('input',{type:'button','class':'iBg iBg_closeSmall',title:'Eliminar'});
				del.onclick = function(){ var trP = this.parentNode.parentNode;
					$1.Win.confirm({text:'Se va a eliminar la actividad de la Lista del Tablero',func:function(){
					$ps_DB.get({file:'PUT '+$5b.api+'R',inputs:'delete=Y&wboListId='+L.wboListId+'&actId='+P.actId, func:function(Jq3){
						if(!Jq3.errNo){ $1.delet(trP); }
					}}); }
					});
				}
				tdDel.appendChild(del);
				tr.appendChild(tdDel);
				var td = $1.t('td');
				td.appendChild($1.t('b',{textNode:L.wboName}));
				td.appendChild($1.t('span',' ⤏ '));
				td.appendChild($1.t('b',{textNode:L.listName}));
				tr.appendChild(td);
				tBody.appendChild(tr);
			}
		}
	}
}
,
$5a2:{
	get:function(P,noLoad){
		var wrap = $1.t('div',{'class':'winMenuInLine win5ck_R',style:'display:none;'});
		if(noLoad){ return wrap; }
		var wrap = $1.q('.win5ck_R');
		var wrapExist = $1.q('.win5ck_R_open');
		if(wrapExist && wrapExist.tagName){ return true; }
		wrap.classList.add('win5ck_R_open');
		wrap.appendChild($1.t('h4','Lista de Chequeo'));
		wrap.appendChild($5a2.F.line(P))
		var tBody = $1.t('tbody');
		function addLine(L,tBody){
			var wid = 'wboList_'+L.wboListId;
			if(!$1.q('.'+wid,tBody)){
				var tr = $1.t('tr',{'class':wid});
				var tdDel = $1.t('td');
				var del = $1.t('input',{type:'button','class':'iBg iBg_closeSmall',title:'Eliminar'});
				del.onclick = function(){ var trP = this.parentNode.parentNode;
					$1.Win.confirm({text:'Se va a eliminar la actividad de la Lista del Tablero',func:function(){
					$ps_DB.get({file:'PUT '+$5a2.api+'R',inputs:'delete=Y&wboListId='+L.wboListId+'&actId='+P.actId, func:function(Jq3){
						if(!Jq3.errNo){ $1.delet(trP); }
					}}); }
					});
				}
				tdDel.appendChild(del);
				tr.appendChild(tdDel);
				var td = $1.t('td');
				td.appendChild($1.t('b',{textNode:L.wboName}));
				td.appendChild($1.t('span',' ⤏ '));
				td.appendChild($1.t('b',{textNode:L.listName}));
				tr.appendChild(td);
				tBody.appendChild(tr);
			}
		}
	}
}
}
}

var $5ck = {
api:'/s/v1/5ck/A:',
F:{
	line:function(P){
	var P = (P) ? P : {};
	var Pars = {targetType:'activity',targetRef:P.actId};
	//$5ck.Simple.timeWaitOnMove = (P.timeWaitOnMove >=0) ? P.timeWaitOnMove : $5ck.Simple.timeWaitOnMove;
	var wrapGen = $1.t('div',{style:'padding:3px 0;'});
	var wrapBarPerc = $1.t('div');
	var wrapList = $1.t('div',{'class':'checkListSimpleWrapList',id:'checkListSimpleWrapList'});
	var wrapForm = $1.t('div',{'class':'checkListSimple_addWrap'});
	var iSave = $1.t('input',{type:'button','class':'btn iBg_save btnOnRight'});
	var iText = $1.t('input',{type:'text','class':'jsFields boxi1 ckAsunt',name:'ckAsunt',placeholder:'Añadir Elemento...',style:'width:98%;',O:{vPost:'targetType=activity&targetRef='+P.actId}});
	iSave.onclick = function(){ $5ck.S.line(this,wrapForm,Pars,wrapList); }
	iText.onkeyup = function(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode;
		if(charCode ==13){ $5ck.S.line(this,wrapForm,Pars,wrapList); }
	}
	wrapForm.appendChild(iText); wrapForm.appendChild(iSave);
	wrapGen.appendChild(wrapForm);
	wrapGen.appendChild(wrapBarPerc); //0
	wrapGen.appendChild(wrapList);
	//wrapGen.appendChild(vars);
	$5ck.V.line(Pars,wrapList);
	return wrapGen;
}
}
,
S:{
line:function(obj,objWrap,P,wraList){
	var This = obj;
	var vPost = $1.G.inputs(objWrap);
	ADMS_DB.get({file:'POST '+$5ck.api+'S.line', inputs:vPost,
	func:function(Jq1){
		if(Jq1.errNo){ ps_DOM.Tag.Win.relResp(Jq1,objWrap,This); }
		else{ ps_DOM.clearInps(objWrap); $5ck.V.line(P,wraList); }
	}
	});
}
}
,
V:{
line:function(P,wrapList){
	$1.clear(wrapList);
	var vPost = 'targetType='+P.targetType+'&targetRef='+P.targetRef;
	ADMS_DB.get({file:'GET '+$5ck.api+'V.line', inputs:vPost,
		func:function(Jq){
		if(Jq.errNo){ $ps_DB.response(wrapList,Jq); }
		else{
			for(var i in Jq.DATA){ var D = Jq.DATA[i];
				var ideWr = 'ckListFloatSimpleIdWrap'+D.ckId;
				var ide = 'ckListFloatSimpleId'+D.ckId;
				var ck = $1.t('div',{'class':'item',id:ideWr});
				var iCk = $1.t('input',{type:'checkbox','class':'_5ck_checkStatus_'+D.targetRef,id:ide,checked:(D.isCompleted=='Y')});
				iCk.vPost = 'ckId='+D.ckId+'&targetType='+D.targetType+'&targetRef='+D.targetRef;
				iCk.onclick = function(){
					var vPost = (this.checked) ? 'isCompleted=Y' : 'isCompleted=N';
					vPost += '&'+this.vPost;
					$ps_DB.get({file:'PUT '+$5ck.api+'S.mark',inputs:vPost,func:function(Jq2){
						if(!Jq2.errNo){ $5ck.Sta.upd(D); }
					}});
				}
				var iLabel = $1.t('label',{textNode:D.ckAsunt,'for':ide});
				ck.appendChild(iCk);
				ck.appendChild(iLabel);
				var iCl = $1.t('input',{type:'button','class':'btn iBg_closeSmall btn2Right'});
				iCl.vPost = 'ckId='+D.ckId+'&targetType='+D.targetType+'&targetRef='+D.targetRef;
				iCl.onclick = function(){ var pare = this.parentNode;
					var vPost = this.vPost;
					$ps_DB.get({f:'DELETE '+$5ck.api,inputs:vPost,func:function(Jq2){
						if(!Jq2.errNo){ $1.delet(pare); $5ck.Sta.upd(D); }
					}});
				}
				ck.appendChild(iCl);
				wrapList.appendChild(ck);
			}
			$5ck.Sta.upd(P);
		}
	}
	});
}
}
,
Sta:{
upd:function(P){
	//ck son los items checke
	var ck = $1.q('._5ck_checkStatus_'+P.targetRef,null,'all');
	var spanCk = $1.q('._5ck_countComplet_'+P.targetRef,null,'all');
	var bar = $1.q('._5ck_barCkComplet_'+P.targetRef,null,'all');
	var complet = total = 0;
	for(var i=0; i<ck.length; i++){
		total++;
		if(ck[i].checked){ complet ++; }
	}
	for(var i=0; i<bar.length; i++){
		var perc = (bar[i].maxWidth) ? bar[i].maxWidth : 1;
		perc = perc*((complet/total));
		bar[i].style.width = perc+'rem';
	}
	for(var i=0; i<spanCk.length; i++){
		spanCk[i].innerText = complet+' / '+total;
	}
}
}
}

var $5n = {
api:'/s/v1/5n/A:',
get:function(P,cont){
	var wrap = $1.t('div',{'class':'_5n_wrapFloat'});
	var wrapList = $1.t('div');
	wrap.appendChild(wrapList);
	var vPost = 'noTypeApp=activity';
	$ps_DB.get({file:'GET '+$5n.api+'o',inputs:vPost, loade:wrapList, func:function(Jq){
		if(Jq.errNo){ $ps_DB.response(wrapList,Jq); }
		else{
			for(var i in Jq.DATA){
				wrapList.appendChild($5n.Draw.line(Jq.DATA[i]));
			}
		}
	}});
	//$1.Win.open(wrap,{onBody:true});
	ps_DOM.Tag.Win.relative(wrap,cont,null,{winX:'rightBot'});
}
,
Draw:{
line:function(D){//D= Jq[i]
	var wrap = $1.t('div',{class:'_5n_line'});
	var LAo = $LA.$5n.O[D.akey];
	wrap.D = D;
	wrap.appendChild($1.t('span',{'class':'iBg obTy_'+D.objType}));
	var line = $1.t('span',{textNode:D.userFromName});
	wrap.appendChild(line);
	switch(D.akey){
		case 'activityOnwboList':{
			line.appendChild($1.t('span',{textNode:' '+LAo.a1}));
			var sa = $1.t('b',{textNode:' '+LAo.a2+' ','class':'obj'});
			sa.onclick = function(){
				$o.open({objType:'activity',objRef:D.objRef},{callf:null});
			}
			line.appendChild(sa);
			line.appendChild($1.t('span',{textNode:' '+LAo.a3}));
			var s2 = $1.t('b',{textNode:' '+LAo.a4,'class':'obj'});
			s2.onclick = function(){
				$o.open({objType:'workBoard',objRef:D.wboId});
			}
			line.appendChild(s2);
		break;}
		case 'activityAssg':{
			line.appendChild($1.t('span',{textNode:' '+LAo.a1}));
			line.appendChild($1.t('textNode',' '));
			var sa = $1.t('b',{textNode:LAo.a2,'class':'obj'});
			sa.onclick = function(){
				D.actId = D.objRef; D.reLoad = true;
				$5a.O.winFloat(D); delete(D.actId); delete(D.reLoad);
			}
			line.appendChild(sa); 
		break;}
		case 'activityCompleted':{
			line.appendChild($1.t('span',{textNode:' '+LAo.a1}));
			line.appendChild($1.t('textNode',' '));
			var sa = $1.t('b',{textNode:LAo.a2,'class':'obj'});
			sa.onclick = function(){
				D.actId = D.objRef; D.reLoad = true;
				$5a.O.winFloat(D); delete(D.actId); delete(D.reLoad);
			}
			line.appendChild(sa);
		break;}
		case '' : break;
		default :{
			line.appendChild($1.t('span',{textNode:' '+LAo.a1}));
			line.appendChild($1.t('textNode',' '));
			if(D.objType == 'activity'){
				var sa = $1.t('b',{textNode:LAo.a2,'class':'obj'});
				sa.onclick = function(){
				D.actId = D.objRef; D.reLoad = true;
				$5a.O.winFloat(D); delete(D.actId); delete(D.reLoad);
			}
				line.appendChild(sa);
			}
			else{
				line.appendChild($1.t('textNode',LAo.a1));
			}
		break;}
	}
	var timeLog = $1.t('div',{'class':'timeLog'});
	timeLog.appendChild($1.t('span',{textNode:D.dateC}));
	wrap.appendChild(timeLog);
	return wrap;
}
}
/* notifi push */
,
verify:function(){
	if(Notification){
		if(Notification.permission == "granted"){}
		else if(Notification.permission == "default") {
			Notification.requestPermission();
		}
		else{
			alert("Bloqueaste los permisos de notificación, debes activarlos.");
		}
	}
	else {
		alert("Tu navegador no es compatible con API Notification");
	}
}
,
room:{//simple
a:function(Jq){
	if(Jq.__toNode){//userId, lineMemo
		for(var i in Jq.__toNode){ var L = Jq.__toNode[i];
			$2io.emit('userRoom_req',L);
		}
	}
}
,
on:function(Dr){//node to client resp on()
	var bell = $1.q('.wbo_notify .notifyCount');
	if(bell){
		var n = bell.innerText*1;
		bell.innerText = n+1;
	}
	if(Notification.permission != "granted"){ $5n.verify(); }
	else{
		var n = {};
		var title = Dr.lineMemo;
		var opts = {};
		if(Dr.objType == 'activity'){
			title = 'Actividad';
			opts = {icon:'http://192.168.0.104:8081/_img/task.png',body:Dr.lineMemo}
		}
		var n = new Notification(title,opts);
		n.onclick = function(){
			window.focus();
			$o.open(Dr);
			n.close();
		}
		setTimeout(function(){ n.close();},15000);
	}
}
}
}


/* adm api */
var ADMS_Filters = {
jsFields:'', //aqui se almacenan los filtros segun el kTb
formu:'admsFiltersForm',
Condic:{"1":"Y", "2":"O"}
,
Equals:{
	"BLANK":{k:"BLANK", v:"En Blanco"},
	"in":{k:"in", v:"Alguno de estos",explain:'Utilize los valores separados por coma (,)'},
	"noIn":{k:"noIn", v:"Ninguno de estos",explain:'Utilize los valores separados por coma (,)'},
	"igual":{k:"igual", v:"Igual a"},
	"menIgual":{k:"menIgual", v:"Menor o Igual que"},
	"mayIgual":{k:"mayIgual", v:"Mayor o Igual que"},
	"noIgual":{k:"noIgual", v:"Diferente / Distinto de"},
	"like1":{k:"like1", v:"...Finaliza Con"},
	"like2":{k:"like2", v:"Inicia Con..."},
	"like3":{k:"like3", v:"...Contiene..."},
	"notLike1":{k:"notLike1", v:"...No Finaliza Con"},
	"notLike2":{k:"notLike2", v:"No Inicia Con..."},
	"notLike3":{k:"notLike3", v:"...No Contiene..."},
}
,
i:0,
updateForm:function(func,formuWrap){
	if(formuWrap){ ADMS_Filters.updateHash(formuWrap); }
	func();
}
,
getBtn:function(cont,CONF){
	var CONF = (CONF) ? CONF : {};
	var __formu = $1.q('#__formuWrap');
	if(CONF.onsubmit){
		__formu.onsubmit = function(){ return false; }
	}
	var cont = (cont && cont.tagName) ? cont : $1.q('.__filtersWrap',__formu);
	if(!cont){ return; }
	$1.clear(cont);
	//formuForm = cont.parentNode form -> div.id
	/* kTb: Key de la tabla para obtener los filtros predeterminados.
	*/
	var superWrap = $1.t('div');
	var divBtn = $1.t('div',{style:'position:relative;'});
	var iBtn = $1.t('input',{type:'submit', value:'', 'class':'submitBtn btnBor iBg iBg_update'});
	divBtn.appendChild(iBtn);
	var btn = $1.t('input',{type:'button','class':'btnAddText iBg_filterOn',value:'Filtros'});
	divBtn.appendChild(btn);
	superWrap.appendChild(divBtn);
	var subWrap = $1.t('div');
	var wrapLines = $1.t('div',{'class':'admsFiltersWrapLines',style:'margin:6px 0;'});
	var wrapInputs = $1.t('div',{'class':'admsFiltersWrapInputs'});
	btn.onclick = function(){ open(this); }
	var wrap = $1.t('div');
	function open(This){
		var win = ps_DOM.Tag.Win.minRel(wrap,{winTitle:'Opciones de Filtro',
		winPosition:'left:'+This.offsetWidth+'px; z-index:1;',winId:'__filterWinOptions'});
		(This.parentNode).appendChild(win);
		ADMS_Filters.winOptions(wrap,superWrap,CONF);
	}
	superWrap.appendChild(wrapLines);
	superWrap.appendChild(wrapInputs);
	cont.appendChild(superWrap);
	if(CONF.hash == true || typeof(CONF.hash) == 'object'){
		ADMS_Filters.drawLineHash(cont,wrapLines);
	}
	if(CONF.func){ ADMS_Filters.updateHash(cont); iBtn.onclick = CONF.func; }
	if(CONF.open){ open(btn); }
}
,
winOptions:function(wrapCont,superWrap,CONF){
	var wrapLines = ps_DOM.G.qSel('.admsFiltersWrapLines',superWrap);
	var btnSubmit = ps_DOM.G.qSel('.submitBtn',superWrap);
	var wrapInputs = ps_DOM.G.qSel('.admsFiltersWrapInputs',superWrap);
	function draww(Jq2){
		if(!Jq2.FiltStand){
			$1.delet($1.q('#__filterWinOptions'));
			ADMS_Filters.getForm(superWrap,Jq2);
			return false;
		}
		var wrap = $1.t('ul',{'class':'clicList'});
		var li = $1.t('li',{textNode:'Filtro Personalizado','class':'iBg iBg_filterOn',style:'display:block;'});
		li.onclick = function(){ ps_DOM.delet(this.parentNode.parentNode.parentNode.parentNode.parentNode);
		ADMS_Filters.getForm(superWrap,Jq2); }
		wrap.appendChild(li);
		for(var f in Jq2.FiltStand){ var FE = Jq2.FiltStand[f];
			var li = $1.t('li',{textNode:FE.text});
			li.D = FE;
			li.onclick = function(){ ps_DOM.delet(this.parentNode.parentNode.parentNode.parentNode.parentNode);
				ADMS_Filters.drawByKey(this.D,wrapLines,wrapInputs);
				btnSubmit.click();
			}
			wrap.appendChild(li);
		}
		ps_DOM.clear(wrapCont);
		wrapCont.appendChild(wrap);
	}
	if(ADMS_Filters.jsFilters != undefined && ADMS_Filters.jsFilters != ''){ draww(ADMS_Filters.jsFilters); }
	else if(CONF.Fik){ draww(CONF.Fik); }
	else{
		ADMS_DB.get({file:'GET /s/filters/tb',ApiMethod:'GET', inputs:'kTb='+CONF.kTb,
	func:function(Jq2){ draww(Jq2); } });
	}
}
,
drawByKey(P,wrapLines,wrapInputs){ //Predeterminados
	ps_DOM.clear(wrapLines);
	ps_DOM.clear(wrapInputs);
	var formuWrap = wrapLines.parentNode;
	var btnSubmit = ps_DOM.G.qSel('.submitBtn',formuWrap);
	var classInput = (P && P.classInput) ? P.classInput : 'jsFiltVars';
	var tName = 'filterKey_'+P.filterKey;
	var div = $1.t('div',{'class':'equalLine'});
	div.appendChild($1.t('span',P.text+' '));
	var iClear = $1.t('input',{type:'button','class':'iBg iBg_clear'});
	iClear.onclick = function(){
		ps_DOM.clear(wrapLines);
		ps_DOM.clear(wrapInputs);
		btnSubmit.click();
	}
	div.appendChild(iClear);
	wrapLines.appendChild(div);
	var div = $1.t('div',{id:tName});
	if(P.filterKey == 'personal'){;
		div.appendChild($1.t('input',{type:'hidden','class':classInput, name:P.fieldName,value:P.fieldValue}));
	}
	else{
	div.appendChild($1.t('input',{type:'hidden','class':classInput, name:'filterKey',value:P.filterKey}));
	div.appendChild($1.t('input',{type:'hidden','class':classInput, name:'fieldName',value:P.fieldName}));
	}
	wrapInputs.appendChild(div);
}
,
getForm:function(formuWrap,D){
	var k1 = D.k1;
	var FIELDS = D.FIELDS;
	var cont = formuWrap;
	var wrapLines = ps_DOM.G.qSel('.admsFiltersWrapLines',formuWrap);
	var btnSubmit = ps_DOM.G.qSel('.submitBtn',formuWrap);
	var wrapInputs = ps_DOM.G.qSel('.admsFiltersWrapInputs',formuWrap);
	var equalLines = ps_DOM.G.qSel('.equalLine',wrapLines);
	if(!equalLines){ ps_DOM.clear(wrapLines); }
	ps_DOM.clear(wrapInputs);
	var wrap = $1.t('fieldset');
	var leg = $1.t('legend');
	var iCl = $1.t('input',{type:'button', 'class':'iBg iBg_closeSmall'});
	iCl.onclick = function(){ ps_DOM.clear(wrapInputs); }
	leg.appendChild(iCl);
	leg.appendChild($1.t('textNode',' Campos de Búsqueda'));
	wrap.appendChild(leg);
	{var formAdd = $1.t('div');
	var tb = $1.t('table',{'class':'table_zh'});
	var tr0 = $1.t('tr');
	tr0.appendChild($1.t('td',{textNode:'Y/O'}));
	tr0.appendChild($1.t('td',{textNode:'Campo'}));
	tr0.appendChild($1.t('td',{textNode:'Condición'}));
	tr0.appendChild($1.t('td',{textNode:'Valor'}));
	tr0.appendChild($1.t('td'));
	tb.appendChild(tr0);
	var tr = $1.t('tr');
	var td = $1.t('td');
	var opts = [{k:1,v:'Y'}, {k:2,v:'O'}];
	var eq = ps_DOM.Tag.select({sel:{'class':'fieldCondic'},opts:opts,noBlank:1});
	td.appendChild(eq);
	tr.appendChild(td);
	var tbAlias = (D.tbAlias) ? D.tbAlias+'.': '';
	for(var i in FIELDS){ if(FIELDS[i].hidden == 'Y'){ delete(FIELDS[i]); } }
	var fiSel = ps_DOM.Tag.select({sel:{'class':'fieldName'},opts:FIELDS, kR:true, addValBegin:tbAlias,selected:k1});
	fiSel.onchange = function(){
		var tS = ps_DOM.G.sel(this); kr = tS.getAttribute('kr');
		var eSel = ps_DOM.G.byId('fieldEqual');
		var FI = FIELDS[kr].equals;
		for(var fn in FI){
			if(ADMS_Filters.Equals[FI[fn]]){ FI[fn] = ADMS_Filters.Equals[FI[fn]]; }
		}
		var nSel = ps_DOM.Tag.select({sel:{id:'fieldEqual','class':'fieldEqual'},noBlank:true,opts:FI});
		(eSel.parentNode).replaceChild(nSel,eSel);
		var iVal = ps_DOM.G.byId('fieldValue');
		if(FIELDS[kr].inputType == 'select'){
			var opts = FIELDS[kr].opts;
			opts = (typeof(opts) == 'string') ? eval(opts) : opts;
			var nInp = ps_DOM.Tag.select({sel:{id:'fieldValue','class':'fieldValue'},opts:opts});
			nInp.opts = opts;
		}
		else if(FIELDS[kr].inputType == 'date'){
		nInp = $1.t('input',{id:'fieldValue',type:'date','class':'fieldValue'});
		}
		else if(FIELDS[kr].inputType == 'number'){
		nInp = $1.t('input',{id:'fieldValue',type:'number','class':'fieldValue'});
		}
		else{ nInp = $1.t('input',{id:'fieldValue',type:'text','class':'fieldValue'}); }
		(iVal.parentNode).replaceChild(nInp,iVal);
	}
	var tdFields = $1.t('td');
	tdFields.appendChild(fiSel);
	tr.appendChild(tdFields);
	var equ = (FIELDS[k1]) ?FIELDS[k1].equals : {};
	var fiEqual = ps_DOM.Tag.select({sel:{id:'fieldEqual','class':'fieldEqual'},opts:equ,noBlank:true});
	var tdFields = $1.t('td');
	tdFields.appendChild(fiEqual);
	tr.appendChild(tdFields);
	var tdVal = $1.t('input',{id:'fieldValue',type:'text','class':'fieldValue'});
	var td = $1.t('td'); td.appendChild(tdVal);
	tr.appendChild(td);
	var iAdd = $1.t('input',{type:'button','class':'btnAddText iBg iBg_add',value:'Añadir Filtro'});
	iAdd.onclick = function(){
		ADMS_Filters.drawLine(formAdd,wrapLines,wrapInputs,{tbAlias:D.tbAlias});
		btnSubmit.click();
		ps_DOM.clearInps(formAdd);
	}
	var td = $1.t('td');
	td.appendChild(iAdd);
	tr.appendChild(td);
	tb.appendChild(tr);
	formAdd.appendChild(tb);
	}
	wrap.appendChild(formAdd);
	wrapInputs.appendChild(wrap);
}
,
drawLineHash:function(formuWrap,wrapLines,P){
	/*
		condic:{value:'1|2'},
		fie:{text:'Código MP', name:'mpCode', value:'TEX*'},
		equ:{text:'Inicia con...', value:'like2'},
		FIE[0][fie.value(E_equ.value)(W_condic.value)(T_type)]
	*/
	var P = (P) ? P : {};
	var namesSave = {'x_x_x':'prueba'};
	var hashIs = '';
	if(P.addLines){
		var wrapLines = ps_DOM.G.qSel('.admsFiltersWrapLines',formuWrap);
		var D = P.addLines;
		hashIs = ADMS_js.hash();
	}
	else{ var D = ADMS_js.hash({get:'url'}); }
	if(P.addLines){
		if(hashIs == ''){}
		else{ return ''; }
	}
	var classInput = (P && P.classInput) ? P.classInput : 'jsFiltVars';
	var btnSubmit = ps_DOM.G.qSel('.submitBtn',formuWrap);
	var hash = '';
	for(var i in D){ for(var i2 in D[i]){
		var tObj = D[i];
		var textt = i2; var valor = D[i][i2];
		var fName = textt.match(/^([a-z0-9\_\.]+)\(/i); fName = (fName && fName[1]) ? fName[1] : textt;
		var tOpts = (ADMS_Filters.jsFilters && ADMS_Filters.jsFilters.FIELDS && ADMS_Filters.jsFilters.FIELDS[fName] && ADMS_Filters.jsFilters.FIELDS[fName].opts) ? ADMS_Filters.jsFilters.FIELDS[fName].opts : {};
		tOpts = (typeof(tOpts) == 'string') ? eval(tOpts) : tOpts;
	var textName = (ADMS_Filters.jsFilters && ADMS_Filters.jsFilters.FIELDS[fName]) ? ADMS_Filters.jsFilters.FIELDS[fName].v : fName;
		hash += i2+'='+valor;
		var E = textt.match(/\(E\_([a-z0-9\_]+)\)/i); E = (E && E[1]) ? E[1] : 'like1';
		var T = textt.match(/\(T\_([a-z0-9\_\-]+)\)/i); T = (T && [1]) ? T[1] : '';
		var W = textt.match(/\(W\_(1|2)\)/i); W = (W && W[1]) ? W[1] : '1';
		var condicText = ADMS_Filters.Condic[W];
		var fieVal = ''; var val = '';
		var equText = ADMS_Filters.Equals[E].v;
		var E_ = '(E_'+E+')'; //igual,BLANK
		var T_ = '(T_text)';
		var W_ = '(W_'+W+')';
		var hasName = fName+E_+T_+W_
		var FiJSON = {name:fName, E:E, T:T, W:W};//Esto se enviar para G.JSONin
		if(fieVal.match(/^FILCONF/)){ var tName = fieVal; }
		else{ var tName = 'FIE['+ADMS_Filters.i+']['+hasName+']';  }
		ADMS_Filters.i++;
		var div = $1.t('div',{'class':'equalLine'});
		if(tObj.canDelete == 'N' || P.canDelete == 'N'){ }
		else{
			var iClear = $1.t('input',{type:'button','class':'iBg iBg_clear'});
			iClear.onclick = function(){
				location.hash = (location.hash).replace(hasName+'='+valor+'&','');
				$1.delet(this.parentNode); ps_DOM.delet(ps_DOM.G.byId(tName));
				btnSubmit.click();
			}
			div.appendChild(iClear);
		}
		div.appendChild($1.t('span',{'class':'spanLine',textNode:condicText}));
		div.appendChild($1.t('u',' '+textName));
		div.appendChild($1.t('span',' \u0020 \u0020'));
		div.appendChild($1.t('span',{'class':'spanLine',textNode:equText,title:E}));
		if(val.tagName == 'SELECT' || T == 'select-one'){
			//val.opts = (T == 'select-one') ? ADMS_Filters.jsFilters.FIELDS[] : val.opts;
			var valS = ps_DOM.Tag.select({sel:{'class':classInput, name:tName, FiJSON:FiJSON},opts:tOpts, selected:val.value});
			div.appendChild(valS);
		}
		else if(E == 'BLANK'){
			div.appendChild($1.t('input',{FiJSON:FiJSON, id:tName, type:'hiddens','class':classInput, name:tName,value:valor}));
		}
		else{
			div.appendChild($1.t('input',{FiJSON:FiJSON, id:tName, type:'text','class':classInput, name:tName,value:valor}));
		};
		if(ADMS_Filters.Equals[E] && ADMS_Filters.Equals[E].explain){
			div.appendChild($1.t('span',' \u0020 \u0020'+ADMS_Filters.Equals[E].explain));
		}
		//document.location.href = '#'+hash;
		wrapLines.appendChild(div);
	}
	}
}
,
updateHash:function(formuWrap){
	return true;
	var btnHash = ps_DOM.G.qSel('.btnHash',formuWrap);
	var hash1 = document.location.hash;
	has1 = hash1.match(/\#(\{.*\})/);
	has1 = (has1 && has1[1]) ? has1[1] : '';
	if(btnHash && btnHash.checked){
		var ge = ps_DOM.G.qSel('.jsFiltVars',formuWrap,'all');
		var h = '';
		for(var i=0; i<ge.length; i++){
			if(ge[i].classList.contains('filterNoHash')){ continue; }
			else{ var name = ge[i].name; name = name.replace(/^FIE\[[0-9]+\]\[/,'');
			if($0s.user == 'supersu'){ alert(name+'='+ge[i].value+'&'); }
			name = name.replace(/\]$/,'');
			h += name+'='+ge[i].value+'&';
			}
		}
		document.location.hash = has1+h; 
	}
	else{ document.location.hash = '#'+has1; }
}
,
drawLine:function(objPare,wrapLines,wrapInputs,P){
	var P = (P) ? P : {};
	var classInput = (P && P.classInput) ? P.classInput : 'jsFiltVars';
	var formuWrap = wrapLines.parentNode;
	var btnSubmit = ps_DOM.G.qSel('.submitBtn',formuWrap);
	var fie = ps_DOM.G.sel(ps_DOM.G.qSel('.fieldName',objPare));
	var fieVal = fie.value;
	equ = ps_DOM.G.sel(ps_DOM.G.qSel('.fieldEqual',objPare));
	var E = equ.value;
	condic = ps_DOM.G.sel(ps_DOM.G.qSel('.fieldCondic',objPare));
	var val = ps_DOM.G.qSel('.fieldValue',objPare);
	var condicText = ADMS_Filters.Condic[condic.value];
	var E_ = '(E_'+E+')'; //igual,BLANK
	var T_ = '(T_'+val.type+')';
	var W_ = '(W_'+condic.value+')';
	var FiJSON = {name:fieVal, E:E, T:val.type, W:condic.value};//Esto se enviar para G.JSONin
	if(fieVal.match(/^FILCONF/)){ var tName = fieVal; }
	else{ var tName = 'FIE['+ADMS_Filters.i+']['+fieVal+E_+T_+W_+']';  }
	ADMS_Filters.i++;
	//text
	var div = $1.t('div',{'class':'equalLine'});
	if(fie.canDelete == 'N'){ }
	else{ var iClear = $1.t('input',{type:'button','class':'iBg iBg_clear'});
	iClear.onclick = function(){
		ps_DOM.delet(this.parentNode); ps_DOM.delet(ps_DOM.G.byId(tName));
		btnSubmit.click();
	}
	}
	div.appendChild(iClear);
	div.appendChild($1.t('span',{'class':'spanLine',textNode:condicText}));
	div.appendChild($1.t('u',' '+fie.text));
	div.appendChild($1.t('span',' \u0020 \u0020'));
	div.appendChild($1.t('span',{'class':'spanLine',textNode:equ.text,title:E}));
	if(val.tagName == 'SELECT'){
		var valS = ps_DOM.Tag.select({sel:{'class':classInput, name:tName, FiJSON:FiJSON},opts:val.opts, selected:val.value});
		div.appendChild(valS);
	}
	else if(E == 'BLANK'){ 
		div.appendChild($1.t('input',{FiJSON:FiJSON, id:tName, type:'hiddens','class':classInput, name:tName,value:val.value}));
	}
	else{
		div.appendChild($1.t('input',{FiJSON:FiJSON, id:tName, type:'text','class':classInput, name:tName,value:val.value}));
	}
	if(ADMS_Filters.Equals[E] && ADMS_Filters.Equals[E].explain){
			div.appendChild($1.t('span',' \u0020 \u0020'+ADMS_Filters.Equals[E].explain));
		}
	wrapLines.appendChild(div);
	ADMS_Filters.updateHash(formuWrap);
}

}

var ADMS_SQL = {};

ADMS_SQL.ordBy = {
	putBtn:function(objTr,P){ var P = (P) ? P : {};
		//P.filtWrapOrdBy: Contenedor de form, que tiene .admsFiltWrapOrdBy
		var filtersWrap = (P.filtersWrap) ? ps_DOM.G.byId(P.filtersWrap) : ps_DOM.G.byId('filtersWrap');
		var tds = ps_DOM.G.byClass('fields2Order',objTr);
		for(var i=0;i<tds.length; i++){
			var td = tds[i]; //td debe tener objeto "O" definido en newObj
			var O = (td.O) ? td.O : {};
			var filt = $1.t('div',{style:'float:right;'});
			var btn = $1.t('input',{type:'button', 'class':'iBg iBg_arrowMinDown'});
			var sType = (td.classList.contains('fieldOrderType_number')) ? 'number' : 'text';
			btn.D = {filtWrapOrdBy:P.filtWrap, sType:sType, fieldName:O.fieldName, fieldDescrip:O.fieldDescrip, tdText:td.innerText};
			btn.onclick = function(){
				var wrap = $1.t('div',{'class':'divUL'});
				if(!this.D.fieldName){
					wrap.appendChild( $1.t('div',{'class':'input_error',textNode:'No se encontró fieldName para el filtro. Actualice e intente nuevamente, o Informe al administrador.'}));
					var wrapBk = ps_DOM.Tag.Win.relative(wrap,this.parentNode);
					return false;
				}
				if(this.D.sType == 'number'){
					var li_az = $1.t('div',{'class':'item iBgBlock iBg_sortNumber',textNode:'Ordenar de Menor a Mayor.'});
					var li_za = $1.t('div',{'class':'item iBgBlock iBg_ksortNumber',textNode:'Ordenar de Mayor a Menor.'});
				}
				else{
					var li_az = $1.t('div',{'class':'item iBgBlock iBg_sort',textNode:'Ordenar de A a Z'});
					var li_za = $1.t('div',{'class':'item iBgBlock iBg_ksort',textNode:'Ordenar de Z a A'});
				}
				li_az.D = this.D;
				li_az.onclick = function(){ 
					this.D.ordType = 'ASC';
					if(P.func){ this.D.func = P.func; }
					ADMS_SQL.ordBy.drawItem(this,filtersWrap);
					ps_DOM.delet(ps_DOM.G.qSel('.admsWinRelative'));
					if(P.func){ P.func(); }
				}
				li_za.D = this.D;
				li_za.onclick = function(){
					this.D.ordType = 'DESC';
					if(P.func){ this.D.func = P.func; }
					ADMS_SQL.ordBy.drawItem(this,filtersWrap);
					ps_DOM.delet(ps_DOM.G.qSel('.admsWinRelative'));
					if(P.func){ P.func(); }
				}
				wrap.appendChild(li_az); wrap.appendChild(li_za);
				var wrapBk = ps_DOM.Tag.Win.relative(wrap,this.parentNode);
			}
			filt.appendChild(btn);
			td.appendChild(filt);
		}
	}
	,
	drawItem:function(obj,wrapCont){
		var P = (obj.D) ? obj.D : {};
		var wrapCont = (wrapCont) ? wrapCont : ps_DOM.G.byId(wrapCont);
		var wrapFilt = ps_DOM.G.qSel('.admsfiltWrapOrdBy',wrapCont);
		if(!wrapFilt){ 
			wrapFilt = $1.t('div',{'class':'admsfiltWrapOrdBy'});
			ps_DOM.I.befNode(1,wrapFilt,wrapCont);
		}
		ps_DOM.DropG.wrap(wrapFilt);
		var fieldName = P.fieldName;
		var fieldDescrip = (P.fieldDescrip) ? P.fieldDescrip : ps_Text.limit(P.tdText,16); 
		var ordType = (P.ordType) ? P.ordType : 'ASC';
		var cl_sort = (P.sType == 'number') ? 'iBg_sortNumber' : 'iBg_sort';
		var cl_ksort = (P.sType == 'number') ? 'iBg_ksortNumber' : 'iBg_ksort';
		var cl_def = (ordType == 'ASC') ? cl_sort : cl_ksort;
		var ordText = fieldName+'.'+ordType;
		var id = 'orderIdDrap_'+fieldName;
		ps_DOM.delet(id);
		var iFilt = $1.t('span',{'class':'admsFiltOrder_item iBg '+cl_def,'ordText':ordText,id:id});
			ps_DOM.DropG.iteM(iFilt,{funcEnd:P.func});
			iFilt.onclick = function(){
				var d = this.getAttribute('ordText');
				if(d.match(/\.ASC$/)){ 
					this.setAttribute('ordText',d.replace(/\.ASC$/,'.DESC')); 
					this.classList.remove(cl_sort); this.classList.add(cl_ksort); 
				}
				else if(d.match(/\.DESC$/)){
					this.setAttribute('ordText',d.replace(/\.DESC$/,'.ASC'));
					this.classList.remove(cl_ksort); this.classList.add(cl_sort); 
				}
				var itemsL = ps_DOM.G.byClass('admsFiltOrder_item',wrapFilt);
				if(itemsL.length == 0){ ps_DOM.delet(wrapFilt); }
				if(P.func){ P.func(); }
			}
		var iClose = $1.t('input',{type:'button','class':'btn iBg iBg_closeSmall',
		style:'position:absolute; top:-12px; left:-12px;'});
		iClose.onclick = function(){ ps_DOM.delet(iFilt); if(P.func){ P.func(); } }
		iFilt.appendChild($1.t('textNode',fieldDescrip));
		iFilt.appendChild(iClose);
		wrapFilt.appendChild(iFilt);
	}
}

var $Fi ={};
$Fi.ocrd = {
FiltStand:{
	inactive:{filterKey:"personal", fieldName:"FILCONF[inactive]", fieldValue:"Y",text:"Clientes Inactivos"},
	withOut:{filterKey:"withOut", fieldName:null, text:"Todos los clientes."},
	updateLast24Hours:{filterKey:"updateLast24Hours", fieldName:"A.dateUpd", text:"Actualizados en las últimas 24 horas"},
	last24Hours:{filterKey:"last24Hours", fieldName:"A.dateC", text:"Añadidos en las últimas 24 horas"},
	yesterdayToday:{filterKey:"yesterdayToday", fieldName:"A.dateC", text:"Añadidos ayer y hoy."},
	last7days:{filterKey:"last7days", fieldName:"A.dateC", text:"Añadidos en los últimos 7 días."}
},
tbAlias:"",
FIELDS:{
	"A.cardId":{k:"A.cardId", v:"ID Cliente", inputType:"number", equals:["igual","noIgual","mayIgual","minIgual"]},
	"A.cardCode":{k:"A.cardCode", v:"Código Cliente", inputType:"text", equals:["like3","igual","noIgual","mayIgual","minIgual"]},
	"A.cardName":{k:"A.cardName", v:"Nombre del Cliente", inputType:"text", equals:["like3","igual","noIgual","like1","like2","notLike1","notLike2","notLike3"]},
	"A.addId":{k:"A.addId", v:"Id Adicional", inputType:"number", equals:["igual","noIgual","mayIgual","minIgual"]},
	"A.userAssgName":{k:"A.userAssgName", v:"Usuario Responsable", inputType:"text", equals:["igual","noIgual","like1","like2","like3","notLike1","notLike2","notLike3","BLANK"]},
	"AUR.auth":{k:"AUR.auth", v:"Tipo de Permiso", inputType:"select", equals:["igual","noIgual"], "opts":"$Aut.Types"},
	"AUR.auTyCode":{k:"AUR.auTyCode", v:"Permiso Para", inputType:"select", equals:["igual","noIgual"], "opts":"$0.AuTypes.BP"},
	"A.isEmp":{k:"A.isEmp", v:"Es Empleado", inputType:"select", "opts":[{k:"N", v:"NO"},{k:"Y", v:"SI"}], equals:["igual"]},
	"A.licTradNum":{k:"A.licTradNum", v:"NIT / C.C", inputType:"text", equals:["igual","noIgual","like1","like2","like3"]},
	"A.RF_RegTrib":{k:"A.RF_RegTrib", v:"Regimen", inputType:"select", "opts":[{k:"GC",v:"GC"},{k:"RC", v:"RC"},{k:"RS",v:"RS"}], 
	equals:["igual","noIgual"]},
	"A.RF_tipEnt":{k:"A.RF_tipEnt", v:"Tipo Entidad", inputType:"select",
	"opts":[{k:"PN",v:"Persona Natural"},{k:"PJ", v:"Persona Jurídica"}], 
	equals:["igual"]}
}
}
$Fi.tick = {
FIELDS:{
actAsunt:{k:'oact.actAsunt', v:'Asunto de Actividad', equals:['like3','like2','like1','notLike3','notLike2','notLike1']},
actPrio:{k:'oact.actPriority',v:'Prioridad', inputType:'select', equals:['igual','noIgual'], opts:"$5a.Vs.actPriority"}
}
}

$db = {
defSe:function(db){//define solo para busquedad
	var dbs = {};
	switch(db){
		case 'act': if(!$Co.a__){ $Co.a__ = $Co.a; } dbs = $Co.a__;  break;//saves
	}
	return dbs;
},
upd:function(db){
	$Co.a__ = ($Co.a__)?$Co.a__:{};
	switch(db){
		case 'act': for(var k in $Co.a){ $Co.a__[k] = $Co.a[k]; } break;
	}
}
,
find:function(P){
	var text = P.t; var e = P.e;
	db = $db.defSe(P.db);;
	var fs = P.fs;//fields{ actAsunt:'esto',}
	var r = {};
	for(var i in db){
		if(e=='like3'){ var reg = new RegExp(''+text+'','i');
			for(var f in fs){
				if(db[i][f] && db[i][f].match(reg)){ r[i] = db[i]; }
			}
		}
	}
	if(P.func){ P.func(r); }
	else{ return r; }
}
}
//$db.act = {};// usar $Co.a;

var ps_Text = {
gNum:function(texT){
	texT = ''+texT+'';
	var sign = texT.substring(0,1);
	var conv = (sign =='-') ? -1 : 1;
	texT = ''+texT.replace('\u0001','')+'';
	var deci =  texT.match(/(\..*)$/);
	var dec = (deci != null) ? deci[0] : '';
	var num = (texT.replace(dec,'').replace(/[^\d]/g,''))*conv;
	var fNum = parseFloat(num+dec);
	fNum = (isNaN(fNum)) ? 0 : fNum;
	return fNum;
},

money:function(obj){ return $Str.money(obj); }
,
limit:function(text,limit){ return $Str.limit(text,limit); }
}
