
Api.Stor='/api/5f';
Api.Attach={a:'/1/attach/'}
var Attach={
ni:1,
svr:'http://api0.admsistems.com/1/attach/upload',
//svr:'http://192.168.0.104:8051/1/attach/upload',
uriPost:'',
formLine:function(P,pare){
	delete(P.getList);
	P.apiG='updAndTable';
	return Attach.btn(P,pare);
},
btn:function(P,pare){P=(P)?P:{};
	P.wT=(P.wT)?P.wT:{};
	var wid=$5fi.wid+P.tt+'_'+P.tr;
	P.wT.id='__btnUpload';
	var wrap = $1.t('div',P.wT,pare);
		var vPostBase=gPost='tt='+P.tt+'&tr='+P.tr+'&';
		var vPost=vPostBase;
		if(!P.vP){ P.vP={}; }
		P.vP['tt']=P.tt; P.vP['tr']=P.tr;
		for(var i in P.gP){ gPost +=i+'='+P.vP[i]+'&'; }
		delete(P.noLoad);
		var lid='fileBtn_'+Attach.ni; Attach.ni++;
		var formu = $1.t('form',{name:'psUploadFileForm','style':'padding:5px 0;','server':$s.storage,'hreff':$5f.updReal},wrap);
		var dFile = $1.t('div');
		var iFile = $1.t('input',{type:'file', id:lid, 'class':'psUFile psUFile_x32 _fileInput', name:'file'});
		var iLabel = $1.t('label',{'for':lid,textNode:'Selección de Archivo'});
		iFile.setAttribute('multiple',true);
	iFile.onchange = function(){
			if(this.files.length>3){ $1.Win.message({text:'No puede seleccionar más de 3 archivos.'}); }
			else{ Attach.revBef(this,P,wrap); $1.clearInps(this.parentNode); }
	}
	formu.appendChild(iFile); formu.appendChild(iLabel);
	var wList = $1.t('div',{id:wid,'class':'_5f_wrapList',style:'margin-top:15px;'},wrap);
	var inpS=$1.t('input',{type:'hidden','class':'jsFiltVars',name:'searchFie',value:'',O:{vPost:vPostBase}},wrap);
	if(P.open=='Y'){ iFile.click(); }
	if(P.winTitle){
		$1.delet(wid+'_');
		$1.Win.open(wrap,{winTitle:P.winTitle,onBody:1,winSize:'medium',winId:wid+'_'});
	}
	return wrap;
},
revBef:function(Tfile,P,wrapPar){ /* 1. revisión antes de */
	var r = $5fi.r_size(Tfile.files);
	P.wList = $1.q('._5f_wrapList',wrapPar);
	if(r){ $1.Win.message(r); }
	else{ Attach.uplSvr(Tfile,P,wrapPar); }
},
storGet:false,/* obtener espacio */
revStor:function(Tfile,P,wrapPar){
	var total=0; var r=false;
	var tFile=Tfile.files; var maxFi=0;
	for(var i=0; i<tFile.length; i++){
		var fiSize=tFile[i].size;
		if(maxFi<fiSize){ maxFi=fiSize; }
		total += fiSize;
	}
	var vPost='storGet=Y&ocardCode='+$0s.ocardCode+'&maxFile='+maxFi+'&upTotal='+total
	$Api.get({url:Attach.svr,f:'',inputs:vPost,func:function(Jr){
		if(Jr.errNo){ $1.Win.message(Jr); return false; }
		P.wList = $1.q('._5f_wrapList',wrapPar);
		Attach.uplSvr(Tfile,P,wrapPar)
	}});
},
revSize:function(tFile){
	var total=0
	for(var i=0; i<tFile.length; i++){
		var fileSize= Math.round(tFile[i].size/1024/1024).toFixed(2);
		if($0s.cFile.maxSize<fileSize){ return {errNo:3,text:'El archivo supera el tamaño máximo por archivo a subir de '+$0s.cFile.maxSize+'Mb. ('+fileSize+')'}; }
		total+= fileSize;
	}
	return false;
},
uplSvr:function(Tfile,Po,wrapPar){
	var Po = (Po) ? Po : {};
	var Func = (Po.func)?Po.func: null;
	var formData = new FormData();
	var wResp = $1.q('.__fileUpdwrapResp',wrapPar);
	if(!wResp){  wResp = $1.t('div',{'class':'__fileUpdwrapResp'},wrapPar); }
	for(var f=0; f<Tfile.files.length; f++){
		var file = Tfile.files[f];
		var fName=(Po.fileNum)?f:'file['+f+']';
		formData.append(fName, file, file.name);
	}
	var MR = Po.MR;
	for(var k in Po.vP){ formData.append(k,Po.vP[k]);
	}
	if(window.XMLHttpRequest){ var xhr = new XMLHttpRequest();}
	else if(window.ActiveXObject){
		var xhr = new ActiveXObject("Microsoft.XMLHTTP"); }
	var url = Attach.svr;
	xhr.open('POST', url, true);
	if($0s.stor('ocardtooken')){ xhr.setRequestHeader('ocardtooken',$0s.stor('ocardtooken')); }
	if($V.AJAXHeaders){
		for(var h in $V.AJAXHeaders){ xhr.setRequestHeader(h,$V.AJAXHeaders[h]); }
	}
	var abt = $1.t('button',{'class':'iBg iBg_closeSmall',title:'Cancelar Operación'},wResp);
	abt.onclick = function(){
		xhr.abort(); $1.clear(wResp);
	}
	var progress = $1.t('progress',{value:0,max:100,style:'width:90%; background-color:#0F0;'},wResp);
	xhr.upload.addEventListener("progress", function(e){
		progress.value = ((e.loaded/e.total)*100);
	}, false);
	xhr.onload = function(Event){
		if(xhr.status == 200){
			$1.clear(wResp);
			var Jq = JSON.parse(xhr.responseText);
			if(Jq.errNo || Jq.errs){ $1.Win.message(Jq); }
			else{ if(Func){ Func(Jq); } }
		}
		else{
			$Api.resp(wResp,{errNo:5,text:'Err Status: '+xhr.status});
		}
	};
	xhr.send(formData);
	if($0s.consLogApi){ console.log('POST-FILE '+url); }
	Tfile.value = '';
},
};

Attach.view=function(P,pare,P2){
	P=(P)?P:{};
	P2=(P2)?P2:{};
	var wrap=$1.t('div',0,pare);
	tT={tt:'geDocFile',tr:P.fileId};
	var vPost='fileId='+P.fileId;
	$Api.get({f:Api.GeDoc.a+'file',loade:wrap, inputs:vPost, func:function(Jr){
		wrap.style.display='block';
		if(Jr.errNo){ $Api.resp(wrap,Jr); return false; }
	 var top=$1.t('div',0,wrap);
	 var h3=$1.t('h3',0,top);
	 var spa=$1.t('span',{textNode:Jr.fileName},h3);
		$1.T.btnFa({fa:'fa-pencil',title:'Editar nombre',P:Jr,func:function(T){
			h3.style.display='none';
			var wEd=$1.t('div',0,top);
			var inp=$1.t('input',{type:'text',name:'fileName',value:spa.innerText,'class':$Api.JS.cls,AJs:{fileId:T.P.fileId},},wEd);
			$Api.send({PUT:Api.GeDoc.a+'file/name',jsBody:top,winErr:1,func:function(R){
				spa.innerText=inp.value; $1.delet(wEd); h3.style.display='';
			}},wEd);
		}},h3);
		var wrapMenu = $1.t('div',0,wrap);
		var Pm={w:{style:'margin-top:0.5rem;'},Li:[
		{textNode:'','class':'fa fa_info active',winClass:'winInfo' },
		{textNode:'','class':'fa fa_comment',winClass:'win5c', func:function(){ $5c.openOne(tT); } }
		]};
		var menu = $1.Menu.inLine(Pm,wrap);
		wrapMenu.appendChild(menu);
		var _inf=$1.t('div',{'class':'winMenuInLine winInfo'},wrapMenu);
		var _5c=$1.t('div',{'class':'winMenuInLine win5c',style:'display:none;'},wrapMenu);
		$5c.formLine(tT,_5c);
		var enlace=$1.t('div',0);
		$1.t('a',{'class':'fa fa_attach',title:'Descargar Archivo',href:Jr.url},enlace);
		$1.t('input',{type:'text',readonly:'readonly',value:Jr.url,style:'width:88%;'},enlace);
		var descrip=$1.t('div',{'class':'textarea',textNode:((Jr.descrip)?Jr.descrip:'')});
		$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'Tamaño',v:Jr.fileSizeText},
		{wxn:'tbf_x4',t:'Creada por',v:_g(Jr.userId,$Tb.ousr)},
		{wxn:'tbf_x2',t:'Creado',v:Jr.dateC},
		{line:1,wxn:'tbf_x1',t:'Enlace',node:enlace},
		{line:1,wxn:'tbf_x1',t:'Descripción',node:descrip},
		],_inf);
		if(P2.btnDelete!='N' && Jr.canDelete=='Y'){
			$Api.send({DELETE:Api.GeDoc.attach+'file',inputs:'fileId='+Jr.fileId+'&svrFileId='+Jr.svrFileId,winErr3:'Y',textNode:'Eliminar Archivo',Conf:{text:'El archivo será eliminado, no se puede revertir esta acción.'},func:function(JrD){
				
			}},_inf);
		}
		GeDoc.Fi.preview(Jr,_inf);
	}});
	if(!pare){ $1.Win.open(wrap,{winSize:'medium',winTitle:'Visualización de Archivo'}); }
}

Attach.btnView=function(L,pare,P2){
	return $1.t('span',{'class':'fa fa-eye',title:'Ver Archivo',L:L,textNode:L.fileName},pare).onclick=function(){ Attach.view(this.L,null,P2); }
}

Attach.Tt={
form:function(D,pare){
	Attach.formLine({func:function(Jr3){
		if(!Jr3.errNo){ D.L=Jr3.L;
			Attach.Tt.up(D, function(Jrr,o){
				if(D.addLine && o){
					var wList=$1.q('._5f_wrapList',pare); /* pare=_5f */
					for(var i in o){
						Attach.Dw.line(o[i],wList);
					}
				}
				if(D.func){ D.func(Jrr,o); }
			});
		}
	}},pare);
},
up:function(D,func){
	var vPost ='&tt='+D.tt+'&tr='+D.tr;
	for(var i in D.L){
		for(var i2 in D.L[i]){ vPost += '&L['+i+']['+i2+']='+D.L[i][i2]; }
	}
	$Api.post({f:Api.Attach.a+'tt/up', inputs:vPost, winErr3:'Y', func:func});
},
get:function(P,wList){
	if(wList && wList.classList.contains($5fi.wid+'opened')){
		return false;
	}
	var vPost='tt='+P.tt+'&tr='+P.tr;
	$Api.get({f:Api.Attach.a+'tt/up', inputs:vPost, loade:wList,
	func:function(Jq){
		wList.classList.add($5fi.wid+'opened');
		if(Jq.errNo){ $Api.resp(wList,Jq); }
		for(var c in Jq.L){ Attach.Dw.line(Jq.L[c],wList); }
	}
	});
},
}

Attach.Dw={
line:function(J,wList){
	var wrap = $1.t('div',{'class':'gid_file_'+$o.T.fileUpd+'_'+J.fileId+' psUpdList_Item',style:'position:relative;'},wList);
	var fileName = $1.t('a',{'target':'_BLANK','class':'iName'},wrap);
	$1.t('span',{'class':'iBg iBg_ico'+J.fileType},fileName);
	$1.t('span',{textNode:J.fileName},fileName);
	$1.T.btnFa({fa:'fa-eye',func:function(){
		var w3=$1.t('div',{'class':''});
		$1.Win.open(w3,{winSize:'medium',onBody:1});
		GeDoc.Fi.view({pare:w3,fileId:J.fileId});
	}},fileName);
	/* if(J.canDelete == 'Y'){ 
		btnDel = $5f.btnDelete(J); wrap.appendChild(btnDel);
	} */
	var docInf= $1.t('div',{'class':'docInfo'},wrap);
	docInf.appendChild($1.t('span',{'class':'userName','textNode':$Tb._g('ousr',J.userId)}));
	docInf.appendChild($1.t('span',{'class':'dateC','textNode':J.dateC}));
	docInf.appendChild($1.t('span',{'class':'dateC','textNode':J.fileSizeText}));
	wrap.appendChild(docInf);
},
}

var $5f={
urlBase:'/_files/uploadFile.php',
updLocalFrom:'N',//definir path para usar ruta y cargar
updReal:'/api/5f',//ruta de carga real
apir:{a:'/api/5f',aUpd:'/api/5f', fire:'/api/5f/fireb'},
Vs:{
typeSea:{'xls,xls':'Hoja de Cálculo','doc,docs':'Documento de Word','jpeg,jpg,png,gif':'Imagen','pdf':'PDF'}
},
btnDelete:function(P){
	var btnDel = $1.t('input',{type:'button','class':'btn iBg_trash btn2Right',title:'Eliminar este archivo'});
	btnDel.fileName = P.fileName; btnDel.fileId = P.id;
	btnDel.globRoom = {objType:$o.T.fileUpd, objRef:P.id, targetType:P.targetType, targetRef:P.targetRef, movType:'delete'}
	btnDel.onclick = function(){ var globRoom = this.globRoom;
		var objDel = this.parentNode;
		$1.Win.confirm({text:'Se va eliminar el archivo, no se podrá recuperar esta información', func:function(){
			if($0s.fireb && $0s.fireb.apps>0){
				var refDel = firebase.storage().refFromURL(P.url);
				if(refDel){ refDel.delete(); }
			}
			$ps_DB.get({f:'DELETE '+$5f.apir.a,inputs:'fileId='+P.id,
			func:function(J2){ if(!J2.errNo){ $1.delet(objDel);
			} }
			});
		}});
	}
	return btnDel;
}
}
$5f.fileType = function(file){
	var type = '';
	var ext = file.name.match(/\.(.*)$/); ext = (ext && ext[1])?ext[1]:'';
	if(ext =='pdf' || file.type == 'application/pdf'){ type = 'pdf'; }
	else if(ext.match(/(jpeg|jpg|png|gif|ico)/i) || file.type.match(/^image/i)){ type = 'img'; }
	else if(ext.match(/(rar|tar|zip|7|bz|bz2)/i)){ type = 'rar'; }
	else if(ext.match(/(doc|docx|rtf)/i) || file.type.match(/^application\/msword/i)){ type = 'doc'; }
	else if(ext.match(/(xls|xlsx|csv|tsv)/i)){ type = 'xls'; }
	return type;
}
$5f.Svr={
L:'/_files/uploadFile.php', svr1:'http://apis1.admsistems.com/app/updFile', fireb:'/firebase'
}

var $5fi ={
/*new ocean */
wid:'winFileUpd_',
saveJS:function(Js,P,P2){
	P2=(P2)?P2:{};
	var vPost='tt='+P.tt+'&tr='+P.tr;
	for(var i in P.MR){ vPost += '&'+i+'='+P.MR[i]; };
	vPost += '&L='+encodeURIComponent(JSON.stringify(Js));
	$ps_DB.get({f:'POST '+$5f.apir.a+'/saveJs',inputs:vPost,func:function(Jr){
		$ps_DB.response(P2.cont,Jr);
		if(P.func){ P.func(Js,Jr); }
		$5fi.getL(P,P.wList);
	}});
},
/* end ocean */
btnOnTb:function(P,pare){
	P.apiG='updAndTable';
	return $5fi.btn(P,pare); 
},
btn:function(P,pare){P=(P)?P:{};
	P.wT=(P.wT)?P.wT:{};
	var wid=$5fi.wid+P.tt+'_'+P.tr;
	P.wT.id='__btnUpload';
	var wrap = $1.t('div',P.wT,pare);
	if($s.storage=='firebase'){ }
	//else if(Attach.svr){ $5f.updReal=Attach.svr; }
	else if($s.storage=='L'){  $5f.updReal='/_files/uploadFile.php'; }
	else if($s.storage=='' || $s.storage==null){
		$1.Win.message({text:'El servidor definido ('+$s.storage+') no existe en $5f.Svr{}.'});
		return wrap;
	}
	else{ $5f.updReal= $s.storage; }
		var vPostBase=gPost='tt='+P.tt+'&tr='+P.tr+'&';
		var vPost=vPostBase;
		if(!P.vP){ P.vP={}; }
		P.vP['tt']=P.tt; P.vP['tr']=P.tr;
		for(var i in P.gP){ gPost +=i+'='+P.vP[i]+'&'; }
		delete(P.noLoad);
		//$5f2.updTask.vPost = vPost+'&';
		var formu = $1.t('form',{name:'psUploadFileForm','style':'padding:5px 0;','server':$s.storage,'hreff':$5f.updReal},wrap);
		var dFile = $1.t('div');
		var iFile = $1.t('input',{type:'file', id:'file', 'class':'psUFile psUFile_x32', name:'file'});
		var iLabel = $1.t('label',{'for':'file',textNode:'Selección de Archivo'});
		iFile.setAttribute('multiple',true);
		iFile.onchange = function(){
			if(this.files.length>3){ $1.Win.message({text:'No puede seleccionar más de 3 archivos.'}); }
			else{ $5fi._updOn(this,P,wrap); $1.clearInps(this.parentNode); }
	}
	formu.appendChild(iFile); formu.appendChild(iLabel);
	var wList = $1.t('div',{id:wid,'class':'_5f_wrapList',style:'margin-top:15px;'},wrap);
	var inpS=$1.t('input',{type:'hidden','class':'jsFiltVars',name:'searchFie',value:'',O:{vPost:vPostBase}},wrap);
	if(P.open=='Y'){ iFile.click(); }
	if(P.getList=='Y'){ $5fi.getL({gP:gPost},wList); }
	if(P.winTitle){
		$1.delet(wid+'_');
		$1.Win.open(wrap,{winTitle:P.winTitle,onBody:1,winSize:'medium',winId:wid+'_'});
	}
	return wrap;
},
_updOn:function(Tfile,P,wrapPar){
	var r = $5fi.r_size(Tfile.files);
	P.wList = $1.q('._5f_wrapList',wrapPar);
	if(r){ $1.Win.message(r); }
	//else if($0s.cFile.storage == 'firebase'){ $5f2.putReview(Tfile.files,P,wrapPar); }
	else{ $5fi._updLocal(Tfile,P,wrapPar); }
},
_updLocal:function(Tfile,Po,wrapPar){
	var Po = (Po) ? Po : {};
	var Func = (Po.func)?Po.func: null;
	var formData = new FormData();
	var wResp = $1.q('.__fileUpdwrapResp',wrapPar);
	if(!wResp){  wResp = $1.t('div',{'class':'__fileUpdwrapResp'},wrapPar); }
	for(var f=0; f<Tfile.files.length; f++){
		var file = Tfile.files[f];
		formData.append('file['+f+']', file, file.name);
	}
	var MR = Po.MR;
	for(var k in Po.vP){ formData.append(k,Po.vP[k]);
	}
	if(window.XMLHttpRequest){ var xhr = new XMLHttpRequest();}
	else if(window.ActiveXObject){
		var xhr = new ActiveXObject("Microsoft.XMLHTTP"); }
	var url = $5f.updReal;
	xhr.open('POST', url, true);
	if($0s.stor('ocardtooken')){ xhr.setRequestHeader('ocardtooken',$0s.stor('ocardtooken')); }
	if($V.AJAXHeaders){
		for(var h in $V.AJAXHeaders){ xhr.setRequestHeader(h,$V.AJAXHeaders[h]); }
	}
	var abt = $1.t('button',{'class':'iBg iBg_closeSmall',title:'Cancelar Operación'});
	abt.onclick = function(){
		xhr.abort(); $1.clear(wResp);
	}
	var progress = $1.t('progress',{value:0,max:100,style:'width:90%; background-color:#0F0;'});
	wResp.appendChild(abt);
	wResp.appendChild(progress);
	xhr.upload.addEventListener("progress", function(e){
		progress.value = ((e.loaded/e.total)*100);
	}, false);
	xhr.onload = function(Event){
		if(xhr.status == 200){
			var Jq = JSON.parse(xhr.responseText);
			if(Jq.errNo || Jq.errs){ if(Func){ Func(Jq); } $ps_DB.response(wResp,Jq); }
			else{
				$ps_DB.response(wResp,Jq);
				$5fi.saveJS(Jq,Po,{cont:wResp});
				//if(Po.getList=='Y'){ $5fi.getL({gP:Po.gP},Po.wList); }
			}
		}else{
			$ps_DB.response(wResp,{errNo:5,text:'Err Status: '+xhr.status});
		}
	};
	xhr.send(formData);
	if($0s.consLogApi){ console.log('POST-FILE '+url); }
	Tfile.value = '';
}
,
drawL:function(J,wList){
	var wrap = $1.t('div',{'class':'_o_'+$o.T.fileUpd+'_'+J.id+' psUpdList_Item',style:'position:relative;'},wList);
	var fileName = $1.t('a',{'href':J.url,'target':'_BLANK','class':'iName'},wrap);
	$1.t('span',{'class':'iBg iBg_ico'+J.fileType},fileName);
	$1.t('span',{textNode:J.fileName},fileName);
	if(J.canDelete == 'Y'){ btnDel = $5f.btnDelete(J); wrap.appendChild(btnDel) }
	var docInf= $1.t('div',{'class':'docInfo'},wrap);
	docInf.appendChild($1.t('span',{'class':'userName','textNode':J.userName}));
	docInf.appendChild($1.t('span',{'class':'dateC','textNode':J.dateC}));
	docInf.appendChild($1.t('span',{'class':'dateC','textNode':J.fileSizeText}));
	wrap.appendChild(docInf);
},
getL:function(P,wList){
	var vPost=(P.gP)?P.gP:'tt='+P.tt+'&tr='+P.tr;
	$Api.get({f:$5f.apir.a, inputs:vPost, loade:wList,
	func:function(Jq){
		wList.classList.add($5fi.wid+'opened');
		if(Jq.errNo){ $ps_DB.response(wList,Jq); }
		for(var c in Jq.L){ $5fi.drawL(Jq.L[c],wList); }
	}
	});
},
r_size:function(tFile){
	var total=0
	for(var i=0; i<tFile.length; i++){
		var fileSize= Math.round(tFile[i].size/1024/1024).toFixed(2);
		if($0s.cFile.maxSize<fileSize){ return {errNo:3,text:'El archivo supera el tamaño máximo por archivo a subir de '+$0s.cFile.maxSize+'Mb. ('+fileSize+')'}; }
		total+= fileSize;
	}
	return false;
}
,
deleteBySrc:function(src){
	$Api.post({f:Api.Stor+'/deleteBySrc', inputs:'src='+src});
}
}

$5fi.formLine=function(P,pare){
	delete(P.getList);
	P.apiG='updAndTable';
	return $5fi.btn(P,pare);
};
$5fi.openOne=function(P){
	var wList=$1.q('#'+$5fi.wid+P.tt+'_'+P.tr);
	if(wList && wList.classList && wList.classList.contains($5fi.wid+'opened')){}
	else{ $5fi.getL(P,wList); }
}
