
$1.q('.inputSea').keyPresi('enter',(T)=>{
	cont=$1.q('#Jhelp_cont');
	JHelp.tutos(cont,'buscar='+T.value);
});
$M.li['home'] ={t:'Centro de Ayuda',noTitle:'Y',func:function(){
	$M.Ht.ini({g:JHelp.ini });
}};
$M.li['tags'] ={t:'Etiqueta',noTitle:'Y',func:function(){
	$M.Ht.ini({clear:'N',g:JHelp.ini });
}};

Api.Help='/help/';
JHelp={
	ini:function(Pa){
		var yaDraw=$1.q('#Jhelp_main');
		wrap=$M.Ht.cont;
		if(yaDraw){
			leff=yaDraw;
			cont=$1.q('#Jhelp_cont');
		}
		else{
			var leff=$1.t('div',{id:'Jhelp_main'},wrap);
			var cont=$1.t('div',{id:'Jhelp_cont'},wrap);
			$1.t('b',{textNode:'Utiliza el buscador y presiona enter para obtener posibles manuales. Tambien puede utilizar la barra de categorias.'},cont);
		}
		if(!Pa.vid && !Pa.cId){ Pa=$M.read(); }
		if(!yaDraw){ JHelp.categories(leff,Pa); }
		if(Pa.vid){ JHelp.tuto(cont,Pa); }
		if(Pa.tag){ JHelp.tutos(cont); }
		else if(Pa.cId){ JHelp.category(cont,Pa); }
	},
	categories:function(leff){
		$Api.get({f:Api.Help+'categories',loade:leff,func:function(Jr){
			ul=$1.t('ul',0,leff);
			$1.t('h3',{textNode:'Categorias'},ul);
			for(var i in Jr.L){ L=Jr.L[i];
				L.tagAttr=(L.tagAttr)?JSON.parse(L.tagAttr):{};
				var li=$1.t('li',0,ul);
				$1.t('span',L.tagAttr,li);
				L.tagAttr={};
				L.tagAttr.href='#home!!{cId:'+L.cId+'}';
				L.tagAttr.textNode=L.cName
				a=$1.t('a',L.tagAttr,li);
				a.style.color='#000';
				a.L=L;
				a.onclick=function(){ JHelp.ini({cId:this.L.cId}); }
			}
		}});
	},
	category:function(cont,Pa){
		$Api.get({f:Api.Help+'category',inputs:'cId='+Pa.cId,loade:cont,func:function(Jr){
			if(Jr.errNo){ return $Api.resp(cont,Jr); }
			for(var i in Jr.L){ L=Jr.L[i];
				var li=$1.t('li',0,cont);
				a=$1.t('a',{href:'#home!!{vid:'+L.vid+'}',textNode:L.title},li);
				a.L=L;
				a.onclick=function(){ JHelp.ini({vid:this.L.vid}); }
			}
		}});
	},
	tutos:function(cont,vPost){
		var Pa=$M.read();
		if(Pa.tag){ vPost='tag=Y&buscar='+Pa.tag; }
		$Api.get({f:Api.Help+'tutos',inputs:vPost,loade:cont,func:function(Jr){
			for(var i in Jr.L){ L=Jr.L[i];
				var li=$1.t('li',0,cont);
				a=$1.t('a',{href:'#help!!{vid:'+L.vid+'}',textNode:L.title},li);
				a.L=L;
				a.onclick=function(){ JHelp.ini({vid:this.L.vid}); }
			}
		}});
	},
	tuto:function(cont,Pa){
		$Api.get({f:Api.Help+'tuto',inputs:'vid='+Pa.vid,loade:cont,func:function(Jr){
			$1.t('h1',{'class':'tuto-title',textNode:Jr.title},cont);
			if(Jr.murl){
				var div=$1.t('div',0,cont);
				$1.t('span',{'class':'fa fa-arrow-circle-o-right',textNode:'Acceso: '},div);
				var sep=Jr.murl.split('/');
				var tt=$V.helpRoutes[sep[0]];
				if(tt){
					$1.t('span',{textNode:tt.path},div);
				}
				if(sep[1]){ $1.t('span',{textNode:' / '+sep[1]},div);}
			}
			var wrap=$1.t('div',{'class':'contentHtml'},cont); 
			var div=$1.t('div',0,wrap);
			div.innerHTML=Jr.html;
			//tags
			if(Jr.tags){
				var tags=$1.t('div',{'class':'JHelp_tags'},cont);
				$1.t('b',{textNode:'Etiquetas ','class':'fa fa-tags'},tags);
				sep=Jr.tags.split(',');
				for(var i in sep){
					var a=$1.t('a',{href:'#tags!!{tag:'+sep[i]+'}',textNode:sep[i]},tags);
				}
			}
			//recargar contenido html
			/* indices */
			var ele=$1.q('[class="item-title"]',div,'all');
			if(ele && ele.length){
				var ul2=$1.t('ul');
				$1.t('b',{textNode:'Indice'},ul2);
				wrap.insertBefore(ul2,div);
			for(var i=0; i<ele.length; i++){
				var txt=ele[i].innerText;
				li=$1.t('li',0,ul2);
				a=$1.t('a',{textNode:txt,'class':'c-pointer'},li);
				a.ele=ele[i];
				a.onclick=function(){ $js.focus(this.ele); }
			}}
			var leyends=[];
			/* Campos obligatorios */
			var reqs=$1.q('.JHelp-btnGo',div,'all');
			if(reqs){ for(var i=0; i<reqs.length; i++){
				reqs[i].setAttribute('class','btnB btn-drop fa fa-plus-circle');
				reqs[i].innerText='Nuevo';
				reqs[i].parentNode.insertBefore($1.t('textNode','haga clic en '),reqs[i]);
			}}
			var reqs=$1.q('[req="Y"]',div,'all');
			for(var i=0; i<reqs.length; i++){
				var txt=reqs[i].innerText;
				reqs[i].innerHTML='';
				var re=$1.t('span',{'class':'fa fa-exclamation-circle',title:'Este campo es obligatorio',textNode:' ',style:'color:red'},reqs[i]);
				$1.t('span',{textNode:txt},reqs[i]);
				if(i==0){
					leyends.push({tag:re.cloneNode(),t:'Campo obligatorio'});
				}
			}
			var reqs=$1.q('[req="R"]',div,'all');
			for(var i=0; i<reqs.length; i++){
				var txt=reqs[i].innerText;
				reqs[i].innerHTML='';
				var re=$1.t('span',{'class':'fa fa-exclamation-circle fc-blue',title:'Es recomendado definirlo',textNode:' '},reqs[i]);
				$1.t('span',{textNode:txt},reqs[i]);
				if(i==0){
					leyends.push({tag:re.cloneNode(),t:'Campo recomendado para agilizar procesos'});
				}
			}
			/* Texto ayuda imagenes */
			var reqs=$1.q('img[alt]',div,'all');
			for(var i=0; i<reqs.length; i++){
				var div=$1.t('div',{style:'fontSize:11px; border:1px solid #000'});
				$1.t('img',{src:reqs[i].getAttribute('src')},div);
				$1.t('div',{textNode:reqs[i].getAttribute('alt'),style:'textAlign:right'},div);
				reqs[i].parentNode.insertBefore(div,reqs[i]);
				$1.delet(reqs[i]);
			}
			if(leyends.length){
				childLeyend=wrap.childNodes.length;
				var div=$1.t('p',{style:'fontSize:11px'});
				wrap.insertBefore(div,wrap.childNodes[childLeyend]);
				$1.t('b',{textNode:'Leyenda: '},div);
				for(var i in leyends){ 
					div.appendChild(leyends[i].tag);
					$1.t('span',{textNode:leyends[i].t+' '},div);
				}
			}
		}});
	}
}