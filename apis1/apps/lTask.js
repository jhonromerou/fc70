/* lisTask */
$Mdl.lisTask='Y';
$V.listNames=[];
Api.Task={a:'/1/task/',list:'/1/task/list/',rel:'/1/task/rel'};
$Doc.a['gtdTask']={a:'lisTask.opener',kl:'taskId'};
$V.listPrivacity={'private':'Privada',shared:'Compartida'};
$V.taskPriority=[{k:'high',v:'Alta'},{k:'medium',v:'Media'},{k:'normal',v:'Normal'},{k:'low',v:'Baja'},{k:'none',v:'Ninguna'}];
$V.taskType={task:'Tarea','call':'Llamada',email:'Correo',meeting:'Reunión',visit:'Visita',deadLine:'Vencimiento','event':'Evento'};
$V.lisTaskSort=[{k:'def',v:'Defecto'},{k:'name',v:'Asunto',ico:'fa fa_sortasc'},{k:'priority',v:'Prioridad',ico:'fa fa_prio'},{k:'tType',v:'Tipo',ico:'iBg iBg_task'},{k:'date',v:'Fecha',ico:'iBg iBg_calendar'},{k:'userAssg',v:'Responsable',ico:'fa fa_user'}];
$M.sAdd([
{L:[{folId:'lisTask',folName:'Actividades',ico:'fa fa-list-ul',folColor:'#4DD698'}]},
{fatherId:'lisTask',MLis:['lisTask']}
]);
$M.add([
{liA:'lisTask',rootFolder:'Actividades',
	L:[{k:'lisTask.list',t:'Organizador Actividades'}]
},
]);

$M.li['lisTask']={t:'Organizador Actividades', kau:'lisTask.list',func:function(){
	$M.Ht.ini({func_cont:Task.List.get });
}};
$M.li['lisTask.task']={t:'Tareas', kau:'lisTask.list',func:function(){
	$M.Ht.ini({func_cont:Task.get });
}};

Task={
openAt:'uri',
Cls:{
wrapper:'lisTask_wrapTasks',
top:'__lisTask_topWrap',
topTitle:'__lisTask_topTitle',
topFilter:'__lisTask_topFilter',
topFilter:'__lisTask_topDescrip',
lisWrap:'__lisTask_ulWrap',
ulWrap:'gtdTaskUL',
wrapView:'lisTask_wrapViewTask',
},
c:[],//cache
wrap:'gtdListTaskWrap',
wrapView:'gtdListTaskWrapView',
onOpen:function(P){
	var P=(P)?P:{};
	if(Task.openFunc=='uri'){
		var Pa=$M.read();
		$M.uriFunc=function(){ Task.view(); };
		$M.to('lisTask','listId:'+Pa.listId+',taskId:'+P.taskId);
	}
	else if(Task.openFunc=='open'){ Task.view(P); }
},
list:function(P,cont){ P=(P)?P:{};
	var cont=(cont)?cont:$1.q('.'+Task.Cls.wrapper);
	var Pa=$M.read();
	var compView=$1.q('.__lisTask_compView',cont);
	compView=(compView)?compView.value:'uncompletedTask';
	var wTaskLis=$1.q('.'+Task.Cls.ulWrap,cont);
	if(P.listId){ Pa.listId=P.listId; }
	if(!Pa.listId){ Pa.listId='inbox'; }
	var uri= (Pa.listId && Pa.listId.match(/inbox|assg|delegated|relAt/))?Api.Task.a+Pa.listId:Api.Task.list;
	var vPost='listId='+Pa.listId+'&completedView='+compView;
	if(P.vPost){ vPost +='&'+P.vPost; }
	$Api.get({f:uri, inputs:vPost, loade:wTaskLis, func:function(Jr){
		$Ch.Task=[];//reset
		if(Task.openAt=='uri' && P.parcialLoad!='Y'){ $1.clear(cont); }
		var top=$1.q('.'+Task.Cls.top,cont);
		if(!top){
			Jr.vPost=vPost;
			Task.Ht.iniWrap(Jr,cont);
			var top=$1.q('.'+Task.Cls.top,cont);
		}
		top.classList.remove('isOpened'); top.classList.add('isOpened');
		var wTaskLis=$1.q('.'+Task.Cls.ulWrap,cont);
		if(Jr.errNo){ $Api.resp(wTaskLis,Jr);  }
		var tit=$1.q('.'+Task.Cls.topTitle,top);
		var descr=$1.q('.'+Task.Cls.topDescrip,top);
		tit.innerHTML=Jr.listName;
		tit.style.borderBottom='0.0625rem solid '+Jr.color;
		if(Jr.description){
			descr.innerHTML=Jr.description;
		}
		if(Jr.L && !Jr.L.errNo){
			$Ch._ini('Task',Jr.L);
			Task.views.get();
		}
	}});
},
post:function(wrap){
	$1.delet($1.q('.gtdTask_respWrap'));
	resp=$1.t('div',{'class':'gtdTask_respWrap'},wrap);
	$Api.post({f:Api.Task.a, loade:resp, inputs:$1.G.inputs(wrap), func:function(Jr,o){
		$Api.resp(resp,Jr);
		if(!Jr.errNo){ $1.clearInps(wrap,{vPost:'N'}); }
		if(o){
			o=$Ch._push('Task',o); Task.views.addTo(o);
		}
	}});
},
view:function(P){ var P=(P)?P:{};
	var wrap=$1.q('.'+Task.Cls.wrapView);
	var Pa=$M.read();
	if(P.taskId){ Pa.taskId=P.taskId; }
	$Api.get({f:Api.Task.a+Pa.taskId, loade:wrap, func:function(Jr){
		if(Jr.errNo){ $Api.resp(wrap,Jr); }
		else{
			var top=$1.t('div',{style:'padding:1rem 0.25rem 0.875rem; line-height:1.5rem; border-bottom:0.0625rem solid #DDD; margin-bottom:0.25rem;'},wrap);
		Task.T.ck(Jr,top,{view:'Y'});
		Task.Ht.subLOpen(Jr,wrap,{update:'Y'});
		}
		// Task.Ht.neW(wrap,{listName:'Subtarea',parentTask:Jr.taskId,grandTask:Jr.parentTask},{prio:'N'});
	}});
}
};

Task.get=function(P,cont){ P=(P)?P:{};
	Task.openFunc='uri'; //usar uris
	var cont=$M.Ht.cont; 
	cont=$1.t('div',{'class':'lisTask_wrapper'},cont);
	var mid=$1.t('div',{'class':Task.Cls.wrapper},cont);
	var rig=$1.t('div',{id:Task.wrapView,'class':Task.Cls.wrapView,textNode:'Vista'},cont);
	$1.t('div',{'class':'clear'},cont);
	var cont=(cont)?cont:$1.q('.'+Task.Cls.wrapper);
	var Pa=$M.read();
	var compView=$1.q('.__lisTask_compView',cont);
	compView=(compView)?compView.value:'uncompletedTask';
	if(P.listId){ Pa.listId=P.listId; }
	if(!Pa.listId){ Pa.listId='inbox'; }
	var uri= (Pa.listId && Pa.listId.match(/inbox|assg|delegated|relAt/))?Api.Task.a+Pa.listId:Api.Task.list;
	var vPost='listId='+Pa.listId+'&completedView='+compView;
	if(P.vPost){ vPost +='&'+P.vPost; }
	$Api.get({f:uri, inputs:vPost, loade:rig, func:function(Jr){
		$Ch.Task=[];//reset
		if(Task.openAt=='uri' && P.parcialLoad!='Y'){ $1.clear(cont); }
		Jr.vPost=vPost;
			Task.Ht.iniWrap(Jr,cont);
		var wTaskLis=$1.q('.'+Task.Cls.ulWrap,cont);
		if(Jr.errNo){ $Api.resp(wTaskLis,Jr);  }
		if(Jr.L && !Jr.L.errNo){
			$Ch._ini('Task',Jr.L);
			Task.views.get({view:'table'});
		}
	}});
}

Task.gcsK=function(L,k){
	var r='__listTask';
	if(k){ r+='_'+k; }
	r += '_'+L.taskId;
	return r;
}

Task.relAt=function(cont,P){ //tareas relacionadas en card
	var P=(P)?P:{};
	Task.openFunc='open'; //evitar uris
	var Pa=$M.read();
	var D={listId:'relAt',listName:'Tareas Relacionadas'};
	if(Pa.cardId){ D.vPost= 'tt=card&tr='+Pa.cardId; D.listName='Tareas del Socio'; }
	if(P.vP){ for(var k in P.vP){ D.vPost +='&'+k+'='+P.vP[k]; } }
	var inp=$1.q('.addTask',cont);
	inp.O={vPost:D.vPost};
	var wp=$1.q('.'+Task.Cls.top,cont);
	if(wp && wp.classList.contains('isOpened')){}
	else{ Task.list(D,cont); }
}

Task.views={
get:function(D){ D=(D)?D:{};
	var vie=$1.q('.__lisTask_orderBy');
	var vieV=null;
	if(D.view){ vieV=D.view }
	else{ vieV=(vie)?vie.value:vieV; }
	switch(vieV){
		case 'name': Tl=$js.sortNum($Ch.Task,{k:'name',ksort:1}); break;
		case 'userAssg': Tl=$js.sortNum($Ch.Task,{k:'userAssg'}); break;
		default: Tl=$js.sortNum($Ch.Task,{k:'n',ksort:1}); break;
	}
	var hayL=(Tl && Tl.length>0);
	var w=$1.q('#'+Task.Cls.lisWrap);
	if(hayL){ $1.clear(w); }
	switch(vieV){
		case 'date': Task.views.date(w); break;
		case 'priority': Task.views.prio(w); break;
		case 'table': Task.views.table(w); break;
	}
	if(hayL){
		for(var i in Tl){ Task.views.addTo(Tl[i],w,vie); }
	}
},
addTo:function(T,P){
	var P=(P)?P:{};
	var csKw=Task.gcsK(T,'wrap');
	if(P.del){
		$Dom.upd([{csk:csKw,del:'Y'}]);
		if(P.del=='all'){ $Ch._d('Task','taskId',T.taskId); return true; }
	}
	var pare=null;
	var w=(w)?w:$1.q('#'+Task.Cls.lisWrap);
	var vie=(vie)?vie:$1.q('.__lisTask_orderBy');
	vie=(vie)?vie.value:'';
	var tod=$2d.D.today;
	var tom=$2d.D.tom;
	var next7=$2d.D.next7;
	switch(vie){
		default:{
			var ul=$1.t('ul',{'class':Task.Cls.ulWrap},w);
			pare=w;
		}break;
		case 'priority' :{ var kp='___prio_'+T.priority;
			pare=$1.q('.'+kp,w);
			if(!pare){
				var div=$1.t('div',{'class':kp},w);
				pare=div;
			$1.t('b',{textNode:$js.k($V.taskPriority,T.priority),'class':'fa fa_prio_'+T.priority},div);
				$1.t('ul',{'class':Task.Cls.ulWrap},div);
			}
		}break;
		case 'name':{
			var ul=$1.t('ul',{'class':Task.Cls.ulWrap},w);
			pare=w;
		}break;
		case 'tType':{ var kp='___type_'+T.tType;
			pare=$1.q('.'+kp,w);
			if(!pare){
				var div=$1.t('div',{'class':kp},w);
				pare=div;
				$1.t('b',{textNode:$V.taskType[T.tType],'class':'iBg iBg_'+T.tType},div);
				$1.t('ul',{'class':Task.Cls.ulWrap},div);
			}
		}break;
		case 'userAssg':{ var kp='___userAssg_'+T.userAssg;
			pare=$1.q('.'+kp,w);
			if(!pare){
				var div=$1.t('div',{'class':kp},w);
				pare=div;
				$1.t('b',{textNode:$Tb._g('ousr',T.userAssg),'class':'fa fa_user'},div);
				$1.t('ul',{'class':Task.Cls.ulWrap},div);
			}
		}break;
		case 'date':{
			var kd=$2d.isD(T.dueDate);
			var kd='___date_'+kd+'_';
			pare=$1.q('.'+kd,w);
			if(!pare){
				var div=$1.t('div',{'class':kd},w);
				pare=div;
			}
		}break;
	}
	var ul=$1.q('.gtdTaskUL',pare);
	Task.Ht.ckLine(T,ul);
},
date:function(wrap){
	$2d.dates();
	var tod=$2d.D.today;
	var tom=$2d.D.tom;
	var next7=$2d.D.next7;
	Ds=[{k:'due',v:'Vencidos'},{k:'today',v:'Hoy '},{k:'tomorrow',v:'Mañana '},{k:'next7',v:'Próximos 7 días'},{k:'future',v:'Próximas'},{k:'noDate',v:'Sin Fecha'}]
	for(var i in Ds){ var K=Ds[i];
		var div=$1.t('div',{'class':'___date_'+K.k+'_'},wrap);
		$1.t('b',{textNode:K.v,'class':''},div);
		$1.t('ul',{'class':Task.Cls.ulWrap},div);
	}
},
prio:function(wrap){
	var Ds=$V.taskPriority;
	for(var i in Ds){ var K=Ds[i];
		var div=$1.t('div',{'class':'lisTask_ulGroup ___prio_'+K.k+' fa fa_prio_'+K.k},wrap);
		$1.t('b',{textNode:K.v,'class':''},div);
		$1.t('ul',{'class':Task.Cls.ulWrap},div);
	}
},
table:function(wrap){
	var tb=$1.T.table(['','Tipo','Asunto','Prioridad','Vencimiento','Responsable']);
	var tBody=$1.t('tbody',0,tb);
	for(var i in $Ch.Task){ var L=$Ch.Task[i];
		var tr=$1.t('tr',0,tBody);
		var td=$1.t('td',0,tr);
		$1.t('td',{textNode:$V._g('taskType',L.taskType)},tr);
		$1.t('td',{textNode:L.asunt},tr);
		$1.t('td',{textNode:$V._g('taskPriority',L.taskPriority)},tr);
		$1.t('td',{textNode:L.dueDate},tr);
		$1.t('td',{textNode:$Tb._g('ousr',L.userId)},tr);
	}
	wrap.appendChild(tb);
}
}

Task.Hry=function(wrap,P){ P=(P)?P:{};
	$Api.get({f:Api.Task.a+'history', loade:wrap, inputs:'taskId='+P.taskId, func:function(Jr){
		if(Jr.errNo){ $Api.resp(wrap,Jr); }
		else{ $Hry._g(Jr.L,wrap,{type:$V.taskType,priority:$V.taskPriority}); }
	}});
}

Task.List={
onOpen:function(Li,P){
	var P=(P)?P:{};
	var tf= {mFunc:Task.list,mTo:function(T1){ $M.to('lisTask','listId:'+T1.folId); },Li:[]};
	if(Li){
		for(var i in Li){ var L=Li[i];
			var ao={folName:L.listName,folId:L.listId,folColor:L.color}
			ao.rBtn='Y';
			L.listId =''+L.listId;
			if(L.listId.match(/inbox|assg|delegated/)){ ao.rBtn='N'; }
			if(L.news>0){ ao.Ico={'class':'fa fa-certificate',color:'red',title:'Novedades: '+L.news}; }
			tf.Li.push(ao);
		}
	}
	if(P.n0=='Y'){ tf.n=0; }
	tf.rFunc=function(Lx,pare){
		var Li=[]; n=0;
		Li[n]={ico:'fa fa_pencil',textNode:' Modificar', P:Lx, func:function(T){ Task.List.form({listId:T.P.folId}); } }; n++;
		Li[n]={ico:'fa fa_priv_members',textNode:' Miembros', P:Lx, func:function(T){ Task.List.members({listId:T.P.folId}); } }; n++;
		var menu=$1.Menu.winLiRel({tagBtn:pare,Li:Li,posi:'rCal'});
	}
	return tf;
},
get:function(){
	Task.openFunc='uri'; //usar uris
	var cont=$M.Ht.cont; var Pa=$M.read();
	cont=$1.t('div',{'class':'lisTask_wrapper'},cont);
	var lef=$1.t('div',{'class':'lisTask_wrapLists'},cont);
	var mid=$1.t('div',{'class':Task.Cls.wrapper},cont);
	var rig=$1.t('div',{id:Task.wrapView,'class':Task.Cls.wrapView,textNode:'Vista'},cont);
	$1.t('div',{'class':'clear'},cont);
	$Api.get({f:Api.Task.list+'all', loade:lef, inputs:$1.G.filter(), func:function(Jr){
		var liWrap=$1.t('div',{id:'gtdTask_listWrap'});
		$1.T.btnFa({fa:'fa_plusCircle',textNode:'Nueva Lista',P:{liWrap:liWrap}, func:function(T){
		Task.List.form(T.P); }},lef);
		lef.appendChild(liWrap);
		$Ch._ini('List',Jr.L,{omit:'listId',m:/(assg|delegated)/});
		$V.listNames=[];
		for(var i in $Ch.List){
			$V.listNames.push({k:$Ch.List[i].listId,v:$Ch.List[i].listName});
		}
		var Lb=Task.List.onOpen(Jr.L,{n0:'Y'});
		Uli.ini(Lb,liWrap);
	}});
	if(!Pa.listId){ Pa.listId='inbox'; }
	if(Pa.listId){ Task.list(); }
	if(Pa.taskId){ Task.view(rig); }
	
},
form:function(P){ P=(P)?P:{};
	var cont=$1.t('div');jsF='jsFields';
	var vPost=(P.listId)?'listId='+P.listId+'&':'';
	$Api.get({f:Api.Task.list, cont:cont, inputs:vPost, loadVerif:!P.listId, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		var divL=$1.T.divL({divLine:1,wxn:'wrapx2',req:'Y',L:'Nombre Lista',I:{tag:'input',type:'text',name:'listName','class':jsF,value:Jr.listName}},cont);
		$1.T.divL({wxn:'wrapx4',req:'Y',L:'Privacidad',I:{tag:'divSelect',sel:{name:'privacity','class':jsF},opts:$V.listPrivacity,noBlank:1,selected:Jr.privacity,arrow:'Y'}},divL);
		$1.T.divL({wxn:'wrapx4',subText:'Orden por defecto',L:'Ordenar',I:{tag:'divSelect',sel:{name:'sortType','class':jsF},opts:$V.lisTaskSort,noBlank:1,selected:Jr.sortType,arrow:'Y'}},divL);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción',I:{tag:'input',type:'text',name:'description','class':jsF,value:Jr.description}},cont);
		var resp=$1.t('div',0,cont);
		var iL={textNode:'Guardar',POST:Api.Task.list, getInputs:function(){ return vPost+$1.G.inputs(cont); }, loade:resp, func:function(Jr2,o){
			$Api.resp(resp,Jr2);
			if(o){
				$1.delet($1.q('.__GtdfolId_'+o.listId));
				var Lb=Task.List.onOpen([o]);
				Lb.open='Y';
				Uli.ini(Lb,P.liWrap);
				$1.delet($1.q('#gtdListWin'));
			}
		}};
		if(P.listId){ iL.PUT=iL.POST; delete(iL.POST); }
		$Api.send(iL,cont);
	}});
	$1.Win.open(cont,{winTitle:'Lista',winId:'gtdListWin',winSize:'medium',onBody:1});
},
members:function(P){
	var cont=$1.t('div');jsF='jsFields';
	var vPost=(P.listId)?'listId='+P.listId+'&':'';
	$Api.get({f:Api.Task.list+'members', cont:cont, inputs:vPost,func:function(Jr){
		var divL=$1.T.divL({divLine:1,wxn:'wrapx4',req:'Y',L:'Usuario',I:{tag:'select',sel:{'class':jsF,name:'userId'},opts:$Tb.ousr}},cont);
		var ops={M:'Miembro'};
		$1.T.divL({wxn:'wrapx4',req:'Y',L:'Tipo',I:{tag:'select',sel:{'class':jsF,name:'userType'},opts:ops,noBlank:1}},divL);
		var resp=$1.t('div',0,cont);
		var nd=$Api.send({POST:Api.Task.list+'members',textNode:'Añadir', loade:resp, getInputs:function(){ return vPost+$1.G.inputs(cont); }, func:function(Jr2,o){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ $1.clearInps(divL); trA(o); }
		}});
		$1.T.divL({wxn:'wrapx4',Inode:nd},divL);
		var lis=$1.t('div',0,cont);
		var tb=$1.T.table(['Usuario','Tipo','']); cont.appendChild(tb);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){ trA(Jr.L[i]); }
		function trA(L2){
			var tr=$1.t('tr',0,tBody);
			$1.t('td',{textNode:$Tb._g('ousr',L2.userId)},tr);
			var td=$1.t('td',{textNode:ops[L2.userType]},tr);
			var td=$1.t('td',{},tr);
			$1.T.btnFa({fa:'fa-close',textNode:'Eliminar Miembro', P:L2, func:function(T){ $Api.delete({f:Api.Task.list+'members',btnDisabled:T,inputs:vPost+'id='+T.P.id,winErr3:'Y', func:function(Jr2){
				if(!Jr2.errNo){ $1.delet(tr); }
			}}); }},td);
		}
	}});
	$1.Win.open(cont,{winTitle:'Miembros de la Lista',winId:'gtdListWin',winSize:'medium',onBody:1});
}
}

Task.Upd={
name:function(P){
	var vPost='taskId='+P.taskId+'&name='+P.name;
	$Api.req({PUT:Api.Task.a+'name', inputs:vPost, winErr1:1, func:function(Jr){
		if(P.func){ P.func(Jr); }
	}});
}
}

Task.iv={}; //intervals
Task.T={
ck:function(L,pare,P){ var P=(P)?P:{};
	var clsId='taskId_'+L.taskId;
	var csKw=Task.gcsK(L,'wrap');
	var idd=(P.view=='Y')?'gtdTaskOpen_'+L.taskId:'gtdTask_'+L.taskId;
	var Pf={name:'completed','class':'jsFields',P:L, id:idd,
	func:function(T){
	var vPost='taskId='+T.P.taskId+'&'+$1.G.inputs(T.parentNode);
	$Api.req({PUT:Api.Task.a+'markCompleted', inputs:vPost,winErr3:'Y', func:function(Jr){
		if(T.checked){ $Dom.upd([{csk:csKw,del:'Y'}]); }
		else{ Task.views.get(L); }
		var ea=$1.qClsId(clsId,null,'all');
		for(var e=0; e<ea.length; e++){
			if(T.checked){ ea[e].checked=true; }
			else{ ea[e].checked=false; }
		}
	}});
	}};
	Pf.clsId=clsId;
	if(L.completed=='Y'){ Pf.checked='checked'; }
	$1.T.check(Pf,pare);
	var csK=Task.gcsK(L,'name');
	var ck=$1.t('div',{'class':csK+' lisTask_ckLineText',textNode:L.name},pare);
	ck.onclick=function(){ var oldval=this.innerText;
		var inpt=$1.t('input',{type:'text',value:oldval},pare);
		inpt.focus();
		inpt.onblur=function(){ var T=this;
			if(T.value==oldval){ ck.style.width='auto'; $1.delet(T); }
			else{
				Task.Upd.name({taskId:L.taskId,name:T.value,func:function(){
					ck.style.width='auto'; 
					$Dom.upd([{csk:csK,nv:T.value,on:'innerText'}]);
					$1.delet(T);
				}});
			}
		}
		ck.style.width=0; 
	}
	return ck;
},
prio:function(L,pare,P2){ var P2=(P2)?P2:{};
	if(P2.edit=='N'){
		var t=$1.t('span',{'class':' fa fa_prio_'+L.priority,title:'Prioridad: ' +$js.k($V.taskPriority,L.priority)},pare);
	}
	else{
		if(!L.priority){ L.priority='none'; }
		var fiecs='taskFie_prio';
		var jsF=(L.jsF)?L.jsF:'jsFields';
		var Ps={sel:{'class':jsF+' '+fiecs,name:'priority'},selected:L.priority,opts:$V.taskPriority,noBlank:1,icok:'fa fa_prio'};
		Ps.ico=(P2.ico)?P2.ico:'Y';
		if(P2.update=='Y'){
			Ps.onchange=function(T,Dr,fch){
			$Api.put({f:Api.Task.a+'priority', inputs:'taskId='+L.taskId+'&priority='+Dr.value,winErr3:'Y', func:function(Jr2){
				if(!Jr2.errNo){
					fch(); L.priority=Dr.value;
					Task.views.addTo(L,{del:'Y'});
				}
			}});
			}
		}
		if(P2.width){ Ps.width=P2.width; }
		Ps.reset='N';
		var t=$1.T.divSelect(Ps,pare);
	}
	return t;
},
type:function(L,pare,P2){ var P2=(P2)?P2:{};
	if(P2.edit=='N'){
		var t=$1.t('span',{'class':' iBg iBg_'+L.tType,title:'Tipo Tarea: ' +$js.k($V.taskType,L.tType)},pare);
	}
	else{
		var jsF=(L.jsF)?L.jsF:'jsFields';
		var Ps={sel:{'class':jsF,name:'tType'},selected:L.tType,opts:$V.taskType,noBlank:1,icok:'iBg iBg'};
		Ps.ico=(P2.ico)?P2.ico:'Y';
		if(P2.update=='Y'){
			Ps.onchange=function(T,Dr,fch){
			$Api.put({f:Api.Task.a+'tType', inputs:'taskId='+L.taskId+'&tType='+Dr.value, winErr3:'Y', func:function(Jr2){
				if(!Jr2.errNo){
					fch(); L.tType=Dr.value;
					Task.views.addTo(L,{del:'Y'});
				}
			}});
			}
		}
		if(P2.width){ Ps.width=P2.width; }
		Ps.reset='N';
		var t=$1.T.divSelect(Ps,pare);
	}
	return t;
},
assg:function(L,pare,P2){ var P2=(P2)?P2:{};
	var jsF=(L.jsF)?L.jsF:'jsFields';
	if(L.userAssg==0){ delete(L.userAssg); }
	var Ps={sel:{'class':jsF,name:'userAssg'},selected:L.userAssg,opts:$Tb.ousr,kt:'user',width:'5',icokNull:'fa fa_userO', icokDefine:'fa fa_user'};
	Ps.ico=(P2.ico)?P2.ico:'Y';
	if(P2.update=='Y'){
		Ps.onchange=function(T,Dr,fch){
		$Api.put({f:Api.Task.a+'assg', inputs:'taskId='+L.taskId+'&userAssg='+Dr.value,winErr3:'Y', func:function(Jr2){ L.userAssg=Dr.value;
			if(!Jr2.errNo){ fch(); Task.views.addTo(L,{del:'Y'}); }
		}});
		}
	}
	var t=$1.T.divSelect(Ps,pare);
},
dueDate:function(L,pare,P2){ var P2=(P2)?P2:{};
	if(P2.edit=='N'){
		var t=$1.t('span',{'class':'iBg iBg_deadLine',title:'Vencimiento: '+L.dueAt,textNode:$2d.f(L.dueAt,'mmm d')},pare);
	}
	else{
		var jsF=(L.jsF)?L.jsF:'jsFields';
		var Ps={'class':jsF,name:'dueDate',value:L.dueAt};
		if(P2.update=='Y'){
			Ps.func=function(Dt){
			$Api.put({f:Api.Task.a+'dueDate', inputs:'taskId='+L.taskId+'&dueDate='+Dt.d,winErr3:'Y', func:function(Jr2){ L.dueDate=Dt.d; L.dueAt=Dt.d;
				Task.views.addTo(L,{del:'Y'});
			}});
			}
		}
		if(P2.width){ Ps.w=P2.width; }
		var t=$2dW.btn(Ps,pare,{time:'Y'});
	}
	return t;
},
onList:function(L,pare,P2){ var P2=(P2)?P2:{};
	if(L.listId==0){ L.listId='inbox'; }
	var inilist=L.listId;
	if(0 && P2.edit=='N'){
		var t=$1.t('span',{'class':' fa fa_prio_'+L.priority,title:'Prioridad: ' +$js.k($V.taskPriority,L.priority)},pare);
	}
	else{
		var jsF=(L.jsF)?L.jsF:'jsFields';
		var Ps={sel:{'class':jsF+' ',name:'listId'},selected:L.listId,opts:$V.listNames,noBlank:1};
		Ps.ico=(P2.ico)?P2.ico:'Y';
		if(P2.update=='Y'){
			Ps.onchange=function(T,Dr,fch){
			if(L.listId==Dr.value){ return false; }
			$Api.put({f:Api.Task.a+'moveList', inputs:'taskId='+L.taskId+'&listId='+Dr.value,winErr3:'Y', func:function(Jr2){
				if(!Jr2.errNo){
					fch();
					del='Y';
					if(L.listId!=Dr.value){ del='all'; }
					if(inilist==Dr.value){ del='Y'; $Ch._push('Task',L); }
					L.listId=Dr.value;
					Task.views.addTo(L,{del:del});
				}
			}});
			}
		}
		if(P2.width){ Ps.width=P2.width; }
		Ps.reset='N';
		var t=$1.T.divSelect(Ps,pare);
	}
	return t;
},
notes:function(L,pare,P2){
	if(P2.edit=='N'){
		var t=$1.t('span',{'class':'iBg iBg_notes',title:'Nota de tarea'},pare);
	}
	else{
	var jsF=(L.jsF)?L.jsF:'jsFields';
	var wrapper=$1.t('div',{style:'max-height:15rem;'},pare);
	var T=ta=$1.t('div',{'class':myWys.clsHtml},wrapper);
	if(P2.update=='Y'){
		var whtml=$1.t('div',{},wrapper);
	ta.innerHTML=L.notes;
	T.value=T.oldv=L.notes;
	ta.onclick=function(){
		myWys.ini(whtml,{value:L.notes, save:function(html,close){
			ta.style.display='';
			if(!close){
				T.value=html;
				_upd(L,T);
				T.oldv=T.value;
			}
			$1.clear(whtml);
		}});
		ta.style.display='none';
	}
		function _upd(L,T){
			if(T.oldv!=T.value){
				$Api.put({f:Api.Task.a+'notes',winErr3:'Y', inputs:'taskId='+L.taskId+'&notes='+$js.uriT(T.value),func:function(){ 
					L.notes=T.value;
					ta.innerHTML=T.value;
					Task.views.addTo(L,{del:'Y'});
				}});
			}
		}
	}
	}
},
};

Task.Ht={
ckLine:function(L,ul){ /* [ ] actividad */;
	var ul=(ul)?ul:$1.q('.'+Task.Cls.ulWrap);
	var csKw=Task.gcsK(L,'wrap');
	var li=$1.t('li',{'class':csKw},0);
	if(ul.firstChild){ $1.I.before(li,ul.firstChild); }
	else{ ul.appendChild(li); }
	var ck=Task.T.ck(L,li);
	ck.onclick=function(){ Task.onOpen(L); }
	Task.Ht.subL(L,li);
	if(L.news>0){
		$1.t('span',{style:'position:absolute; right:0; color:red;','class':'fa fa-certificate',title:'Novedades: '+L.news},li);
	}
	return li;
},
subL:function(L,pare){ /*[ ] xx */
	var div=$1.t('div',{'class':'lisTask_ckLineData'},pare);
	Task.T.type(L,div,{edit:'N'});
	Task.T.prio(L,div,{edit:'N'});
	if(L.userAssg && L.userAssg!=0){ $1.t('span',{'class':'fa fa_user',title:'Responsable: '+$Tb._g('ousr',L.userAssg)},div); }
	if(L.notes!=''){ Task.T.notes(L,div,{edit:'N'}); }
	return div;
},
subLOpen:function(L,pare,P2){
	var P2=(P2)?P2:{};
	var div=$1.t('div',0,pare);
	L.ico='N'; P2.ico='N'; P2.width=4;
	Task.T.dueDate(L,div,P2);
	Task.T.type(L,div,P2);
	Task.T.prio(L,div,P2);
	Task.T.assg(L,div,P2);
	Task.T.onList(L,div,P2);
	Task.T.notes(L,div,P2);
	/* wins */
	var wrap = $1.t('div',0,div);
	var tT={tt:'gtdTask',tr:L.taskId,addLine:'Y'};
	var wrapMenu = $1.t('div',0,wrap);
	var Pm={w:{style:'margin-top:0.5rem;'},Li:[
	{li:{textNode:'','class':'fa fa_info active',winClass:'winInfo' }},
	{li:{textNode:'','class':'fa fa_comment',winClass:'win5c', func:function(){ Commt.openOne(tT); } }},
	{li:{textNode:'','class':'fa fa_fileUpd',winClass:'win5f', func:function(){ Attach.Tt.get(Tts,_5fw); } }},
	{li:{textNode:'','class':'fa fa_link',winClass:'winRel', func:function(){ $Rel.Card.openOne(L); } }}
	]};
	var menu = $1.Menu.inLine(Pm,wrap);
	wrapMenu.appendChild(menu);
	var Tts={tt:'gtdTask',tr:L.taskId};
	var _inf=$1.t('div',{'class':'winMenuInLine winInfo'},wrapMenu);
	Task.Hry(_inf,L);
	var _5c=$1.t('div',{'class':'winMenuInLine win5c',style:'display:none;'},wrapMenu);
	var _5f=$1.t('div',{'class':'winMenuInLine win5f',style:'display:none;'},wrapMenu);
	var rel=$1.t('div',{'class':'winMenuInLine winRel',style:'display:none;'},wrapMenu);
	tT.wT={'class':'win5c'};
	Commt.formLine(tT,_5c);
	tT.wT={'class':'win5f'};
	Attach.Tt.form({tt:'gtdTask',tr:L.taskId, addLine:'Y',func:function(Jrr,o){
	}},_5f);
	var _5fw=$1.t('div',0,_5f);
	var tRel={func:function(L2){
		var vPost='taskId='+L.taskId+'&tt=card&tr='+L2.cardId+'&trMemo='+L2.cardName;
		$Api.post({f:Api.Task.rel, inputs:vPost, func:function(Jr2,o){
			if(Jr2.errNo){ $1.Win.message(Jr2); }
			else if(o){ $Rel.Card.add(o); }
		}});
	}};
	$Rel.Card.formLine(rel,tRel);
	return div;
},
neW:function(cont,P,P2){
	if(!isNullo(P.grandTask,0)){ //No permite abuelo->padre->hijo
		return $1.t('div',{textNode:'Una subtarea no puede tener otras subtareas.'},cont);
	}
	var P=(P)?P:{};
	var P2=(P2)?P2:{};
	var div=$1.t('div',{'class':'gtdTask_addWrap'},cont); var jsF='jsFields';
	var taskInp=$1.t('div',{'class':'gtdTask_input'},div);
	var vPost=(P.listId)?'listId='+P.listId:'';
	if(P.parentTask){ vPost += '&parentTask='+P.parentTask; }
	if(P.vPost){ vPost += '&'+P.vPost; }
	var divL=$1.T.divL({divLine:1,wxn:'wrapx1',req:'Y',I:{tag:'input',type:'text','class':jsF+' addTask',name:'name',placeholder:'Añadir a '+P.listName+'. + Enter',O:{vPost:vPost}}},taskInp);
	divL.classList.add('divLineTaskInput');
	divL.setAttribute('tabindex',0);
	var tols=$1.t('div',{'class':'taskInputRight'},divL);
	if(P2.dueDate!='N'){ Task.T.dueDate({f:'d'},tols); }
	if(P2.prio!='N'){ Task.T.prio({},tols); }
	if(P2.assg!='N'){ Task.T.assg({},tols); }
	if(P2.type!='N'){ Task.T.type({},tols); }
	var inp=$1.q('.addTask',div);
	inp.onkeypress=function(ev){
		$js.isKey(ev,'enter',{func:function(){
			Task.post(div);
		}});
	}
	return div;
},
}

Task.Ht.iniWrap=function(P,cont){ /* contenedor */
	var P=(P)?P:{};
	var cont=(cont)?cont:$1.q('.'+Task.Cls.wrapper);
	var tit=$1.t('div',{'class':Task.Cls.top},cont);
	$1.t('div',{textNode:P.listName,style:'display:inline-block;','class':Task.Cls.topTitle},tit);
	$1.t('p',{'class':Task.Cls.topDescrip},tit);
	var ops=$1.t('div',{style:'float:right; position:relative;','class':Task.Cls.topFilter},tit);
	var viC=[{k:'uncumpletedTask',v:'No Completas',ico:'fa fa_uncompleted'},{k:'completedTask',v:'Completas',ico:'fa fa_completed'}];
	var sP={change:'always',sel:{'class':'__lisTask_compView'},opts:viC,noBlank:1,reset:'N',width:'2'};
	sP.onchange=function(){ Task.list({parcialLoad:'Y'}); }
	$1.T.divSelect(sP,ops);
	var sP={change:'always',sel:{'class':'__lisTask_orderBy'},opts:$V.lisTaskSort,selected:P.sortType,noBlank:1,reset:'N',width:'5'};
	sP.onchange=function(){ Task.views.get(); }
	var orde=$1.T.divSelect(sP,ops);
	$1.t('div',{'class':'clear'},tit);
	Task.Ht.neW(cont,P);
	var ulT=$1.t('div',{id:Task.Cls.lisWrap},cont);
	$1.t('ul',{'class':Task.Cls.ulWrap},ulT);
}

/* end task */