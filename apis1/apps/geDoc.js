$Mdl.geDoc='Y' /* Version DCP oct 2020 */
Api.GeDoc={a:'/1/gtd/ffc/',attach:'/1/attach/'};
Api.GeDoc.b='/1/gtd/';
$Doc.a['geDocFol']={a:'geDoc.fol',kl:'folId'};

$oB.pus('crd',$Opts,[
{ico:'fa fa-folder',textNode:' Carpeta Cliente',mTo:'geDoc.card', mPars:['cardId']}
]);
$oB.pus('crd',$Mdl.itemLi,[
{fa:'fa-folder',textNode:' Carpeta',hrefTo:['geDoc.card','cardId']}
]);
$oB.push($Mdl.Cnf.crdFichaProf,[
{textNode:' Requeridos',winClass:'_gtdRfi_win','class':'fa fa-file-o', func:function(T){
	var Pa=$M.read();
	GeDoc.Rfi.oneLoad({tt:'card',tr:Pa.cardId},T.win);
}}
]);

$M.add([
{liA:'geDoc',rootFolder:'Gestión Documental',
	L:[{k:'geDoc.card',t:'Clientes'},{k:'geDoc.pwork',t:'Papeles de Trabajo'}]
}
]);

$M.li['geDoc.cards']={t:'Clientes', kau:'geDoc.card', func:function(){ $M.Ht.ini({fieldset:'Y',func_filt:'ocrd', func_filtCall:GeDoc.Card.getCards,func_pageAndCont:GeDoc.Card.getCards}); }};
$M.li['geDoc.card']={noTitle:'Y', kau:'geDoc.card',func:function(){ $M.Ht.ini({func_cont:GeDoc.Card.get}); }};
$M.li['geDoc.pwork']={t:'Papeles de Trabajo', mdl:'geDoc', kau:'geDoc.card', func:function(){ $M.Ht.ini({func_cont:GeDoc.Pwork.get}); }};

/* revision... */
$M.li['geDoc.oTy']={noTitle:'Y',t:'Carpetas de Sistema', kau:'geDoc.card',func:function(){ $M.Ht.ini({func_cont:GeDoc.oTy.get}); }};

GeDoc={
openFunc:'uri',
Cls:{
wrapper:'JDrive_wrapper',
wrapFol:'JDrive_wFolders',
wrapFiles:'JDrive_wFiles',
wrapViewFile:'JDrive_wViewFile',
},
get:function(){
	var contA=$M.Ht.cont;
	var Pa=$M.read();
	var vPost='cardId='+Pa.cardId;
	var cont=$1.t('div',{'class':GeDoc.Cls.wrapper},contA);
	var lef=$1.t('div',{'class':GeDoc.Cls.wrapFol},cont);
	var mid=$1.t('div',{'class':GeDoc.Cls.wrapFiles},cont);
	var rig=$1.t('div',{'class':GeDoc.Cls.wrapViewFile,textNode:'Vista'},cont);
	$1.t('div',{'class':'clear'},cont);
	$Api.get({f:Api.GeDoc.a+'card',inputs:vPost, loader:mid, func:function(Jr){
		if(Jr.errNo){ $Api.resp(lef,Jr); }
		else if(Jr.L && Jr.L.errNo==1){ $Api.resp(lef,Jr.L); }
		else{
			Jr.L=$js.sortNum(Jr.L,{k:'lv'});
			for(var i in Jr.L){ var L=Jr.L[i];
				Jr.L[i].cardId=Pa.cardId;
				Uli.fold({L:L,n:0,
					mFunc:function(T1){ GeDoc.getFiles(); },
					mTo:function(T1){ $M.to('geDoc.card','cardId:'+T1.cardId+',folId:'+T1.folId); }
				},lef);
			}
		}
	}});
	if(Pa.folId){ GeDoc.getFiles(); }
	if(Pa.fileId){ GeDoc.Fi.onOpen(Pa); }
},
getFiles:function(){
	var Pa=$M.read();
	var cont=$1.q('.'+GeDoc.Cls.wrapFiles);
	var vPost='cardId='+Pa.cardId+'&folId='+Pa.folId;
	$Api.get({f:Api.GeDoc.a+'card.open',inputs:vPost, loade:cont, func:function(Jr){
		if(Jr.errNo && Jr.errNo==1){ $Api.resp(cont,Jr); }
		else{
			$1.t('h5',{'class':'fa fa_folder',textNode:Jr.folName},cont);
			var tb=$1.T.table(['Tipo','Nombre Archivo.','Tamaño','Versión','Creado']);
			var tBody=$1.t('tbody',{'class':'JDrive_tbFiles'},tb); cont.appendChild(tb);
			if(Jr.L && !Jr.L.errNo){ GeDoc.trLine(tBody,Jr.L); }
			var upd=$1.t('div',{id:'fileUpd'},cont);
			Attach.btnUp({func:function(Jr3){
				if(!Jr3.errNo){
					GeDoc.fileUp({folId:Pa.folId,tt:'card',tr:Pa.cardId,L:Jr3.L}, function(Jrr,o){
						GeDoc.trLine(tBody,o);
					});
				}
			}},upd);
		}
	}});
},
trLine:function(tBody,JrL,P){
	var P=(P)?P:{};
	var view=P.view; delete(P.view);//card,oTy
	for(var i in JrL){ var L=JrL[i];
		var tr=$1.t('tr',0,tBody);
		$1.t('td',{node:$1.t('span',{'class':'iBg iBg_ico'+L.fileType}),title:L.fileType},tr);
		var tddiv=$1.t('div',{'class':'_fileName'},$1.t('td',0,tr));
		var btn=$1.t('span',{textNode:L.fileName+' ',P:L},tddiv);
		btn.onclick=function(){
			switch(view){
				case 'card': GeDoc.Card.onOpen(this.P); break;
				case 'pwork': GeDoc.Pwork.onOpen(this.P); break;
				case 'oTy': GeDoc.oTy.onOpen(this.P); break;
				case 'free': GeDoc.Fi.view(this.P); break; //abrir directamente visualizacion info
			}
		}
		$1.t('td',{textNode:L.fileSizeText},tr);
		$1.t('td',{textNode:L.versNum},tr);
		var td= $1.t('td',{textNode:$Doc.by('userDate',L)+' '},tr);
		$1.t('a',{href:L.purl,title:'Clic para descargar','class':'fa fa_attach'},td);
		var td= $1.t('td',{'class':$Api.JS.cls,AJs:{tt:L.tt,tr:L.tr,fileId:L.fileId}},tr);
		/* var btn=$1.T.btnFa({fa:'fa-thumb-tack',title:'Fijar en Perfil de Cliente',P:td,func:function(T){ 
			$Api.put({f:Api.GeDoc.b+'fix',btnDisabled:T,winMsg:'Y',jsAdd:T.P.AJs});
		}},td); */
		for(var ix in P.tds){
			tr.appendChild(P.tds[ix]);
		}
	}
},
perms:function(p,D){
	var D=(D)?D:{};
	if(p=='R' && (D.perms=='R' || D.perms=='W' || D.perms=='P')){ return true; }
	else if(p=='W' && (D.perms=='W' || D.perms=='P')){ return true; }
	else if(p=='P' && D.perms=='P'){ return true; }
	else{ return false; }
}
}
GeDoc.Fi={
onOpen:function(P){
	var P=(P)?P:{};
	if(GeDoc.openFunc=='uri'){
		var Pa=$M.read();
		$M.uriFunc=function(){ GeDoc.Fi.view(); };
		$M.to('geDoc.card','cardId:'+Pa.cardId+',folId:'+Pa.folId+',fileId:'+P.fileId);
	}
	else if(GeDoc.openFunc=='open'){ GeDoc.Fi.view(P); }
},
view:function(P){ P=(P)?P:{}; //visualizar datos archivo
	var wrap=(P.pare)?P.pare:$1.q('.'+GeDoc.Cls.wrapViewFile);
	var Pa=$M.read();
	if(Pa.fileId){ P.fileId=Pa.fileId; }
	P.display='Y';
	return Attach.view(P,wrap);
	tT={tt:'geDocFile',tr:Pa.fileId};
	var vPost='fileId='+Pa.fileId;
	$Api.get({f:Api.Attach.prA+'view',loade:wrap, inputs:vPost, func:function(Jr){
		$1.T.btnFa({fa:'fa fa_close',title:'Cerrar',style:'position:absolute; right:0; top:0;', func:function(){ wrap.style.display='none'; }},wrap);
		wrap.style.display='block';
		if(Jr.errNo){ $Api.resp(wrap,Jr); return false; }
	 var top=$1.t('h3',{textNode:Jr.fileName},wrap);
		var wrapMenu = $1.t('div',0,wrap);
		var Pm={w:{style:'margin-top:0.5rem;'},Li:[
		{li:{textNode:'','class':'fa fa_info active',winClass:'winInfo' }},
		{li:{textNode:'','class':'fa fa_comment',winClass:'win5c', func:function(){ $5c.openOne(tT); } }}
		]};
		var menu = $1.Menu.inLine(Pm,wrap);
		wrapMenu.appendChild(menu);
		var _inf=$1.t('div',{'class':'winMenuInLine winInfo'},wrapMenu);
		var _5c=$1.t('div',{'class':'winMenuInLine win5c',style:'display:none;'},wrapMenu);
		GeDoc.Fi.preview(Jr,_inf);
		$5c.formLine(tT,_5c);
		var enlace=$1.t('div',0);
		$1.t('a',{'class':'fa fa_attach',title:'Descargar Archivo',href:Jr.purl},enlace);
		$1.t('input',{type:'text',readonly:'readonly',value:Jr.purl,style:'width:88%;'},enlace);
		var descrip=$1.t('div',{'class':'textarea',textNode:((Jr.descrip)?Jr.descrip:'')});
		$1.T.tbf([
		{line:1,wxn:'tbf_x4',t:'Tamaño',v:Jr.fileSizeText},
		{wxn:'tbf_x4',t:'Creada por',v:_g(Jr.userId,$Tb.ousr)},
		{wxn:'tbf_x2',t:'Creado',v:Jr.dateC},
		{line:1,wxn:'tbf_x1',t:'Enlace',node:enlace},
		{line:1,wxn:'tbf_x1',t:'Descripción',node:descrip},
		],_inf);
		if(Jr.canDelete=='Y'){
			$1.T.btnNew({textNode:'Eliminar Archivo.',func:function(){ Attach.delete(Jr); }},_inf);
		}
	}});
},
preview:function(L,pare){
	var ret=false;
	var div=$1.t('div',{style:'border:0.0625rem solid #EEE; padding:0.25rem; border-radius:0.25rem; margin-top:1rem;'});
	$1.t('h5',{textNode:'Vista Previa'},div);
	if(L.svr!='FB'){ L.purl +='&embed=Y'; }
	var gview='https://docs.google.com/gview?url=';
	if(L.fileType=='img'){ $1.t('img',{src:L.purl,alt:'Imagen',title:L.fileName},div); ret=true; }
	else if(L.fileType=='pdf'){ $1.t('embed',{src:L.purl,frameborder:0,style:'width:100%; height:100%;'},div); ret=true; }
	else if((''+L.fileType).match(/^(txt|csv|tsv)$/)){ 
		$1.t('embed',{src:L.purl,frameborder:0,style:'width:100%; height:100%;'},div); ret=true; 
	}
	if(ret && pare){ pare.appendChild(div); return div; }
}
}
GeDoc.fileUp=function(D,func){
	var jsAdd={folId:D.folId};
	if(D.tt && D.tr){ jsAdd.tt=D.tt; jsAdd.tr=D.tr; }
	jsAdd.L=D.L;
	$Api.post({f:Api.GeDoc.a+'fileUp', jsAdd:jsAdd, winErr3:'Y', func:func});
}

GeDoc.Card={
onOpen:function(P){
	var P=(P)?P:{};
	if(GeDoc.openFunc=='uri'){
		var Pa=$M.read();
		$M.uriFunc=function(){ GeDoc.Fi.view(); };
		$M.to('geDoc.card','cardId:'+Pa.cardId+',folId:'+Pa.folId+',fileId:'+P.fileId);
	}
	else if(GeDoc.openFunc=='open'){ GeDoc.Fi.view(P); }
},
getCards:function(){
	var Pa=$M.read();
	var cont=$M.Ht.cont;
	$Api.get({f:Api.GeDoc.a+'cards',inputs:$1.G.filter(), loaderFull:'Y', loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['','Código','Nombre']); cont.appendChild(tb);
			var tBody=$1.t('tbody',0,tb);
			for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				var td=$1.t('td',0,tr);
				var a=$1.t('a',{href:$M.to('geDoc.card','cardId:'+L.cardId,'r'),'class':'fa fa_folder',title:'Ver Carpeta'},td);
				$1.t('td',{textNode:L.cardCode},tr);
				$1.t('td',{textNode:L.cardName},tr);
			}
		}
	}});
},
get:function(){
	var contA=$M.Ht.cont;
	var Pa=$M.read();
	var vPost='cardId='+Pa.cardId;
	
	var cont=$1.t('div',{'class':GeDoc.Cls.wrapper},contA);
	var lef=$1.t('div',{'class':GeDoc.Cls.wrapFol},cont);
	var mid=$1.t('div',{'class':GeDoc.Cls.wrapFiles},cont);
	var rig=$1.t('div',{'class':GeDoc.Cls.wrapViewFile,textNode:'Vista'},cont);
	$1.t('div',{'class':'clear'},cont);
	$Api.get({f:Api.GeDoc.a+'card',inputs:vPost, loaderFull:'Y', loade:lef, func:function(Jr){
		if(Jr.errNo){ $Api.resp(lef,Jr); }
		else{
			$1.I.before($1.t('h4',{textNode:Jr.cardName}),cont);
			if(Jr.L && Jr.L.errNo){ $Api.resp(lef,Jr.L); }
			else{
				Jr.L=$js.sortNum(Jr.L,{k:'lv'});
				var Li=[];
				for(var i in Jr.L){ var L=Jr.L[i];
					L.cardId=Pa.cardId;
					Li.push(L);
				}
				Uli.ini({Li:Li,
					mFunc:function(){ GeDoc.Card.getFiles(); },
					mTo:function(L2){ $M.to('geDoc.card','cardId:'+Pa.cardId+',folId:'+L2.folId); }
				},lef);
			}
		}
	}});
	if(Pa.folId){ GeDoc.Card.getFiles(); }
	if(Pa.fileId){ GeDoc.Card.onOpen(Pa); }
},
getFiles:function(){
	var Pa=$M.read();
	var cont=$1.q('.'+GeDoc.Cls.wrapFiles);
	var vPost='cardId='+Pa.cardId+'&folId='+Pa.folId;
	$Api.get({f:Api.GeDoc.a+'card.open',inputs:vPost, loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			$1.t('h5',{'class':'fa fa_folder',textNode:Jr.folName},cont);
			var tb=$1.T.table(['Tipo','Nombre Archivo...','Tamaño','Versión','Creado']);
			var tBody=$1.t('tbody',{'class':'JDrive_tbFiles'},tb); cont.appendChild(tb);
			if(Jr.L && !Jr.L.errNo){ GeDoc.trLine(tBody,Jr.L,{view:'card'}); }
			var upd=$1.t('div',{id:'fileUpd'},cont);
			Attach.btnUp({func:function(Jr3){
				if(!Jr3.errNo){
					GeDoc.fileUp({folId:Pa.folId,tt:'card',tr:Pa.cardId,L:Jr3}, function(Jrr,o){
						GeDoc.trLine(tBody,o,{view:'card'});
					});
				}
			}},upd);
		}
	}});
},
}
GeDoc.Pwork={
onOpen:function(P){
	var P=(P)?P:{};
	if(GeDoc.openFunc=='uri'){
		var Pa=$M.read();
		$M.uriFunc=function(){ GeDoc.Fi.view(); };
		$M.to('geDoc.pwork','folId:'+Pa.folId+',fileId:'+P.fileId);
	}
	else if(GeDoc.openFunc=='open'){ GeDoc.Fi.view(P); }
},
get:function(){
	var contA=$M.Ht.cont;
	var Pa=$M.read();
	var vPost='';
	var cont=$1.t('div',{'class':GeDoc.Cls.wrapper},contA);
	var lef=$1.t('div',{'class':GeDoc.Cls.wrapFol},cont);
	var mid=$1.t('div',{'class':GeDoc.Cls.wrapFiles},cont);
	var rig=$1.t('div',{'class':GeDoc.Cls.wrapViewFile,textNode:'Vista'},cont);
	$1.t('div',{'class':'clear'},cont);
	$Api.get({f:Api.GeDoc.a+'pwork',inputs:vPost, loaderFull:'Y', loade:lef, func:function(Jr){
		if(Jr.errNo){ $Api.resp(lef,Jr); }
		else{
			//$1.I.before($1.t('h4',{textNode:Jr.cardName}),cont);
			if(Jr.L && Jr.L.errNo){ $Api.resp(lef,Jr.L); }
			else{
				Jr.L=$js.sortNum(Jr.L,{k:'lv'});
				var Li=[];
				for(var i in Jr.L){ var L=Jr.L[i];
					L.cardId=Pa.cardId;
					Li.push(L);
				}
				Uli.ini({Li:Li,
					mFunc:function(){ GeDoc.Pwork.getFiles(); },
					mTo:function(L2){ $M.to('geDoc.pwork','folId:'+L2.folId); }
				},lef);
			}
		}
	}});
	if(Pa.folId){ GeDoc.Pwork.getFiles(); }
	if(Pa.fileId){ GeDoc.Pwork.onOpen(Pa); }
},
getFiles:function(){
	var Pa=$M.read();
	var cont=$1.q('.'+GeDoc.Cls.wrapFiles);
	var vPost='folId='+Pa.folId;
	$Api.get({f:Api.GeDoc.a+'pwork.open',inputs:vPost, loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			$1.t('h5',{'class':'fa fa_folder',textNode:Jr.folName},cont);
			var tb=$1.T.table(['Tipo','Nombre Archivo','Tamaño','Versión','Creado']);
			var tBody=$1.t('tbody',{'class':'JDrive_tbFiles'},tb); cont.appendChild(tb);
			if(Jr.L && !Jr.L.errNo){ GeDoc.trLine(tBody,Jr.L,{view:'pwork'}); }
			var upd=$1.t('div',{id:'fileUpd'},cont);
			if(GeDoc.perms('W',Jr)){
				Attach.btnUp({func:function(Jr3){
					if(!Jr3.errNo){
						GeDoc.fileUp({folId:Pa.folId,tt:'pwork',tr:1,L:Jr3.L}, function(Jrr,o){
							GeDoc.trLine(tBody,o,{view:'pwork'});
						});
					}
				}},upd);
			}
		}
	}});
},
}

GeDoc.wrapTemp=function(){
	var contA=$M.Ht.cont; contA.innerHTML='';
	var Pa=$M.read();
	var vPost='';
	var cont=$1.t('div',{'class':GeDoc.Cls.wrapper},contA);
	var lef=$1.t('div',{'class':GeDoc.Cls.wrapFol},cont);
	var mid=$1.t('div',{'class':GeDoc.Cls.wrapFiles},cont);
	var rig=$1.t('div',{'class':GeDoc.Cls.wrapViewFile,textNode:'Vista'},cont);
	$1.t('div',{'class':'clear'},cont);
}
GeDoc.oTy={
onOpen:function(P){
	var P=(P)?P:{};
	if(GeDoc.openFunc=='uri'){
		var Pa=$M.read();
		$M.uriFunc=function(){ GeDoc.Fi.view(); };
		$M.to('geDoc.oTy','listId:'+Pa.folId+',fileId:'+P.fileId);
	}
	else if(GeDoc.openFunc=='open'){ GeDoc.Fi.view(P); }
},
get:function(){
	var contA=$M.Ht.cont;
	var Pa=$M.read();
	var vPost='cardId='+Pa.cardId;
	var cont=$1.t('div',{'class':GeDoc.Cls.wrapper},contA);
	var lef=$1.t('div',{'class':GeDoc.Cls.wrapFol},cont);
	var mid=$1.t('div',{'class':GeDoc.Cls.wrapFiles},cont);
	var rig=$1.t('div',{'class':GeDoc.Cls.wrapViewFile,textNode:'Vista'},cont);
	var Jr={};
	Jr.L=($Mdl.GeDoc_oTy)?$Mdl.GeDoc_oTy:{errNo:3,text:'No hay definido ningun tipo.'};
	$1.t('div',{'class':'clear'},cont);
		if(Jr.errNo){ $Api.resp(lef,Jr); }
		else{
			if(Jr.L && Jr.L.errNo){ $Api.resp(lef,Jr.L); }
			else{
			Jr.L=$js.sortNum(Jr.L,{k:'lv'});
			var Li=[];
			for(var i in Jr.L){ var L=Jr.L[i];
				L.cardId=Pa.cardId;
				Li.push(L);
			}
			Uli.ini({Li:Li,
				mFunc:function(){ GeDoc.oTy.getFiles(); },
				mTo:function(L2){ $M.to('geDoc.oTy','folId:'+L2.folId); }
			},lef);
			}
		}
	
	if(Pa.folId){ GeDoc.oTy.getFiles(); }
	if(Pa.fileId){ GeDoc.oTy.onOpen(Pa); }
},
getFiles:function(){
	var Pa=$M.read();
	var cont=$1.q('.'+GeDoc.Cls.wrapFiles);
	var vPost='folId='+Pa.folId;
	$Api.get({f:Api.GeDoc.a+'oTy.open',inputs:vPost, loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			$1.t('h5',{'class':'fa fa_folder',textNode:Jr.folName},cont);
			var tb=$1.T.table(['Tipo','Nombre Archivo','Tamaño','Versión','Creado','Pedido']);
			var tBody=$1.t('tbody',{'class':'JDrive_tbFiles'},tb); cont.appendChild(tb);
			var tds=[];
			for(var i in Jr.L){
				Jr.L[i].folId='oTy';
				var td=$1.t('td');
				td.appendChild($Doc.href(Jr.L[i].tt,{docEntry:Jr.L[i].tr}));
				tds.push(td);
			}
			if(Jr.L && !Jr.L.errNo){ GeDoc.trLine(tBody,Jr.L,{tds:tds,view:'oTy'}); }
		}
	}});
},
}

GeDoc.to=function(P,add){
	var add=(add)?','+add:'';
	var r=''; var r2='';
	if(P.tt=='card'){ r='geDoc.card'; r2='cardId:'+P.tr; }
	return $M.to(r,r2+add,'r');
}

GeDoc.Rfi={/* Req Files */
oneLoad:function(P,pare){
	var ex=$1.q('._oneLoad',pare);
	if(!ex){
		var win=$1.t('div',{'class':'_oneLoad'},pare);
		GeDoc.Rfi.get(P,pare);
	}
},
get:function(P,cont){
	var cont=(cont)?cont:$M.Ht.cont;
	var vPost='tt='+P.tt+'&tr='+P.tr;
	$Api.get({f:Api.GeDoc.b+'rfi',inputs:vPost, loade:cont, func:function(Jr){
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['Documento','Tipo','Nombre Archivo..','Tamaño','Requerido',''],0,cont);
			tb.classList.add('_oneLoad');
			var tBody=$1.t('tbody',{'class':'geDoc_tableFiles'},tb);
			if(Jr.L && !Jr.L.errNo){ for(var i in Jr.L){ L=Jr.L[i];
				L.tt=P.tt; L.tr=P.tr;
				if(L.fileName){ GeDoc.Rfi.inpTxt(L,tBody); }
				else{ GeDoc.Rfi.inpUp(L,tBody); }
			}}
		}
	}});
},
inpUp:function(L,tBody){
	var tr=$1.t('tr',{'class':'_gtdRfiId_'+L.rfiId});
	var trOld=$1.q('._gtdRfiId_'+L.rfiId,tBody);
	if(trOld){ tBody.replaceChild(tr,trOld); }
	else{ tBody.appendChild(tr); }
	$1.t('td',{textNode:L.rfiName},tr);
	var td=$1.t('td',{colspan:3},tr);
	var inp=$1.t('input',{type:'file',name:'file','class':$Api.xFields},td);
	inp.AJs={rfiId:L.rfiId,tt:L.tt,tr:L.tr};
	inp.onchange=function(){ This=this.parentNode;
		$Api.post({f:Api.GeDoc.b+'rfi',formData:This,winErr:true,func:function(Jr2,_o){
			GeDoc.Rfi.inpTxt(_o,tBody);
		},upFiles:'Y'
		});
	}
	$1.t('td',{textNode:((L.isOpt=='N')?'Si':'No')},tr);
	$1.t('td',0,tr);
	return tr;
},
inpTxt:function(L,tBody){
	var tr=$1.t('tr',{'class':'_gtdRfiId_'+L.rfiId});
	var trOld=$1.q('._gtdRfiId_'+L.rfiId,tBody);
	if(trOld){ tBody.replaceChild(tr,trOld); }
	else{ tBody.appendChild(tr); }
	$1.t('td',{textNode:L.rfiName},tr);
	$1.t('td',{textNode:L.fileType},tr);
	var td=$1.t('td',0,tr);
	$1.t('span',{'class':'fa fa-eye',title:'Ver Archivo',L:L,textNode:L.fileName},td).onclick=function(){ Attach.view(this.L); }
	$1.t('td',{textNode:L.fileSizeText},tr);
	$1.t('td',{textNode:((L.isOpt=='N')?'Si':'No')},tr);
	var td=$1.t('td',0,tr);
	$1.T.btnFa({fa:'fa_close',title:'Eliminar',P:L,func:function(T){
		$Api.delete({f:Api.GeDoc.b+'rfi',inputs:'id='+T.P.id,winErr:true,func:function(Jr2){ GeDoc.Rfi.inpUp(L,tBody);
		}});
	}},td);
}
}

GeDoc.Rfi.define=function(Px){
	//api,gData,tt,
	var gData='wh[tt]='+Px.tt+'&';
	if(Px.gData){ gData+=Px.gData+'&'; }
	$Filt.filtFunc=function(){ GeDoc.Rfi.define(Px); }
	$Filt.form({cont:$M.Ht.filt,whs:'Y',active:'Y',Li:[
	{t:'Opcional',tag:'select',name:'isOpt',opts:$V.YN},
	{t:'Nombre del Requerido',tag:'input',name:'rfiName(E_like3)'}
	]});
	$Tb._Massi.form({api:Api.GeDoc.b+'rfi/tt',vPost:gData+$Filt.get($M.Ht.filt),filter:'Y',
	L:[
	[{textNode:'Opcional'},{tag:'select',k:'isOpt',name:'isOpt',opts:$V.YN,noBlank:'Y',AJsBase:{tt:Px.tt},AJs:['rfiId']}],
	[{textNode:'Nombre del Requerido'},{tag:'input',k:'rfiName',name:'rfiName'}]
	]
	});
}


GeDoc.Fix={/*archivos fijos */
get:function(P,cont){
	// to:geDoc.card, cardId, 
	//geDoc.card!!{cardId:1,folId:1,fileId:8}
	var cont=(cont)?cont:$M.Ht.cont;
	var vPost='tt='+P.tt+'&tr='+P.tr;
	$Api.get({f:Api.GeDoc.b+'fix',inputs:vPost, loade:cont, func:function(Jr){
			if(P.divTop){
				cont.appendChild(P.divTop);
			}
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var tb=$1.T.table(['Tipo','Nombre Archivo..','Tamaño','Directorio'],0,cont);
			tb.classList.add('_oneLoad');
			var tBody=$1.t('tbody',{'class':'geDoc_tableFiles'},tb);
			if(Jr.L && !Jr.L.errNo){ for(var i in Jr.L){ L=Jr.L[i];
				var tr=$1.t('tr',0,tBody);
				$1.t('td',{textNode:L.fileType},tr);
				$1.t('td',{textNode:L.fileName},tr);
				$1.t('td',{textNode:L.fileSizeText},tr);
				var td=$1.t('td',0,tr);
				$1.t('a',{textNode:L.folName,'class':'fa fa-folder',href:GeDoc.to(L,'folId:'+L.folId+',fileId:'+L.fileId)},td);
			}}
		}
	}});
},
}