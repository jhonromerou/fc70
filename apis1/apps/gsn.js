/* GeSN! */
Api.Gsn = {a:'/1/gsn/'};
$Mdl.GeSN='Y';
$M.sAdd([
{fatherId:'crd',L:[{folId:'crdGsn',folName:'Gestión'}]},
{fatherId:'crdGsn',MLis:['Gsn.crs']}
]);
$M.add([
{liA:'Gsn',rootFolder:'Gestión Socios Negocios',
	L:[{k:'Gsn.crs',t:'Seguimiento'}]
},
{s:'crd',folder:'Gestión',L:['Gsn.crs']}
]);

$TbV['Gsn.crdStatus']=[{k:1,v:'Contactado'},{k:2,v:'Cotización Enviada'},{k:3,v:'No interesado'},{k:4,v:'Cotización en estudio'},{k:5,v:'Cambio de comprador'},{k:6,v:'Firma de Documentos'},{k:7,v:'Otros'}];
$TbV['Gsn.actType']=[{k:1,v:'Otros'},{k:2,v:'Llamada'},{k:3,v:'Correo'},{k:4,v:'Reunión'},{k:5,v:'Visita'}];
$TbV['Gsn.actType_aa']=$TbV['Gsn.actType'];
$TbV['Gsn.actType_aa'].push({k:0,v:'Estado'});

_Fi['Gsn.crs.history']=function(wrap,func){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8',L:'Gestión',I:{tag:'select',sel:{'class':jsV,name:'GA.actType(E_igual)'},opts:$TbV['Gsn.actType_aa']}},wrap);
	$1.T.divL({wxn:'wrapx8',L:'Fecha',I:{tag:'input',type:'date','class':jsV,name:'GA.dateC(E_mayIgual)'}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Corte',I:{tag:'input',type:'date','class':jsV,name:'GA.dateC(E_menIgual)'}},divL);
	$1.T.divL({wxn:'wrapx4',L:'Detalles de la gestión',I:{tag:'input',type:'text','class':jsV,name:'GA.lineMemo(E_like3)'}},divL);
	var bR={'':'Parcial','max1000':'Hasta 1000'};
	$1.T.divL({wxn:'wrapx8', L:'Reporte',I:{tag:'select',sel:{'class':jsV,name:'__dbReportLen'},opts:bR,noBlank:1}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Gsn.Crs.history});
	wrap.appendChild(btnSend);
};
_Fi['Gsn.crs']=function(wrap,func){
	var jsV = 'jsFiltVars';
	var divL=$1.T.divL({divLine:1, wxn:'wrapx8',L:{textNode:'Código'},I:{tag:'input',type:'text','class':jsV,name:'A.cardCode(E_like3)'}},wrap);
	$1.T.divL({wxn:'wrapx4',L:{textNode:'Nombre'},I:{tag:'input',type:'text','class':jsV,name:'A.cardName(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx4',L:'Última gestión',I:{tag:'input',type:'text','class':jsV,name:'GA.lastAction(E_like3)'}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Responsable Venta'},I:{tag:'select',sel:{'class':jsV,name:'A.slpId(E_igual)'},opts:$Tb.oslp}},divL);
	$1.T.divL({wxn:'wrapx8', L:{textNode:'Tipo Socio'},I:{tag:'select',sel:{'class':jsV,name:'A.cardType(E_igual)'},opts:$V.crdTypeSell}},divL);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:{textNode:'Estado'},I:{tag:'select',sel:{'class':jsV,name:'GA.statusId(E_igual)'},opts:$TbV['Gsn.crdStatus']}},wrap);
	var df=$2d.rang({rang:'weeks',n:1});
	$1.T.divL({wxn:'wrapx8',subText:'Actualización',L:'Fecha',I:{tag:'input',type:'date','class':jsV,name:'GA.dateUpd(E_mayIgual)(T_time)',value:df.date1}},divL);
	$1.T.divL({wxn:'wrapx8',L:'Fecha Corte',I:{tag:'input',type:'date','class':jsV,name:'GA.dateUpd(E_menIgual)(T_time)'}},divL);
	var btnSend = $1.T.btnSend({textNode:'Actualizar', func:Gsn.Crs.get});
	wrap.appendChild(btnSend);
};

$M.li['Gsn.crs']={t:'Listado de Socios', kau:'Gsn.crs', func:function(){
	$M.Ht.ini({fieldset:'Y',func_filt:'Gsn.crs',func_pageAndCont:Gsn.Crs.get}); }};
$M.li['Gsn.crs.history']={t:'Historial de Socio', kau:'Gsn.crs', func:function(){
	$M.Ht.ini({fieldset:'Y',func_filt:'Gsn.crs.history',func_pageAndCont:Gsn.Crs.history}); }};
	
	var Gsn={};
Gsn.Crs={
opts:function(P,pare){
	var L=P.L; var Jr=P.Jr;
	Li=[]; var n=0;
	Li[n]={ico:'fa fa-exchange',textNode:' Cambiar Estado', func:function(){
		Gsn.Crs.btnStatus(L,pare);
	} }; n++;
	Li[n]={ico:'fa fa-flash',textNode:' Registrar Gestión', func:function(){
		Gsn.Crs.btnAct(L,pare);
	} }; n++;
	Li[n]={ico:'fa fa-history',textNode:' Historial', href:$M.to('Gsn.crs.history','cardId:'+L.cardId,'r') }; n++;
	Li=$Opts.add('Gsn.crs',Li);
	return Li={L:L,Li:Li,textNode:P.textNode};
},
get:function(){
	var cont=$M.Ht.cont;
	$Api.get({f:Api.Gsn.a+'crs', inputs:$1.G.filter(), loade:cont, func:function(Jr){
		var tb=$1.T.table(['','Estado','Cliente','Responsable','Actualización','Ult. Tipo Gestión','Ult. Actualización Gestión']);
		var tBody=$1.t('tbody',0,tb);
		for(var i in Jr.L){ L=Jr.L[i];
			var tr=$1.t('tr',0,tBody);
			var td=$1.t('td',0,tr);
			var div=$1.t('div');
			var menu=$1.Menu.winLiRel(Gsn.Crs.opts({L:L,textNode:'Opciones'},div));
			td.appendChild(menu);
			td.appendChild(div);
			$1.t('td',{textNode:$TbV._g('Gsn.crdStatus',L.statusId)},tr);
			var td=$1.t('td',0,tr);
			$1.t('a',{textNode:L.cardName,href:$M.to('Gsn.crs.history','cardId:'+L.cardId,'r'),'class':'fa fa-history'},td);
			$1.t('td',{textNode:$Tb._g('ousr',L.userUpd)},tr);
			$1.t('td',{textNode:L.dateUpd},tr);
			$1.t('td',{textNode:$TbV._g('Gsn.actType_aa',L.lastActType)},tr);
			$1.t('td',{textNode:$Str.limit(L.lastAction,40),xls:{t:L.lastAction}},tr);
		}
		tb=$1.T.tbExport(tb,{ext:'xlsx',fileName:'Reporte Ultima Gestión '+$2d.today});
		cont.appendChild(tb);
	}});
},
btnStatus:function(L,wrap){
	var wid='_GsnCrsWin_act';
	var jsF='jsFields';
	wrap.style.position='relative';
	$1.clear(wrap);
	var wcont=$1.t('div',{'class':wid,style:'position:absolute; top:100%; left:0; width:16.5rem; backgroundColor:#FFF; padding:0.5rem; border:0.0625rem solid #DDD;'},wrap);
	$1.t('h4',{textNode:'Cambio de Estado'},wcont)
	var oldS=L.statusId;
	var sel=$1.T.divSelect({sel:{'class':jsF,name:'statusId'},selected:oldS,opts:$TbV['Gsn.crdStatus']},wcont);
	var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Estado',Inode:sel},wcont);
	$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción del cambio',I:{tag:'textarea','class':jsF,name:'lastAction'}},wcont);
	var resp=$1.t('div',0,wcont);
	$Api.send({textNode:'Actualizar Estado',PUT:Api.Gsn.a+'crs/status', loade:resp, getInputs:function(){ return 'cardId='+L.cardId+'&'+$1.G.inputs(wrap); }, func:function(Jr2){
		$Api.resp(resp,Jr2);
		if(!Jr2.errNo){ $1.clearInps(wcont); }
	}},wcont);
	$1.T.btnFa({fa:'fa_close',textNode:'Cerrar',style:'position:absolute; top:0; right:0;',func:function(){
		wrap.style.position=''; $1.clear(wrap);
	}},wcont)
	return wcont;
},
btnAct:function(L,wrap){
	var wid='_GsnCrsWin_act';
	var jsF='jsFields';
	wrap.style.position='relative'; $1.clear(wrap);
	var wcont=$1.t('div',{'class':wid,style:'position:absolute; top:100%; left:0; width:16.5rem; backgroundColor:#FFF; padding:0.5rem; border:0.0625rem solid #DDD; '},wrap);
	$1.t('h4',{textNode:'Registrar Gestión'},wcont);
	wrap.style.position='relative';
		var sel=$1.T.divSelect({sel:{'class':jsF,name:'actType'},opts:$TbV['Gsn.actType'],noBlank:1},wcont);
		var divL=$1.T.divL({divLine:1,wxn:'wrapx8',L:'Tipo',Inode:sel},wcont);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Fecha',I:{tag:'input',type:'date','class':jsF,name:'docDate'}},wcont);
		var sea=$crd.Fx.inpSeaPrs({});
		$1.T.divL({divLine:1,L:'Contacto',wxn:'wrapx2',Inode:sea},wcont);
		$1.T.divL({divLine:1,wxn:'wrapx1',L:'Descripción de la gestión',I:{tag:'textarea','class':jsF,name:'lastAction'}},wcont);
		var resp=$1.t('div',0,wcont);
		$Api.send({textNode:'Registrar',POST:Api.Gsn.a+'crs/action', loade:resp, getInputs:function(){ return 'cardId='+L.cardId+'&'+$1.G.inputs(wrap); }, func:function(Jr2){
			$Api.resp(resp,Jr2);
			if(!Jr2.errNo){ $1.clearInps(wcont); }
		}},wcont);
		$1.T.btnFa({fa:'fa_close',textNode:'Cerrar',style:'position:absolute; top:0; right:0;',func:function(){
		wrap.style.position=''; $1.clear(wrap);
	}},wcont);
	return wcont;
},
history:function(){
	var cont =$M.Ht.cont; var Pa=$M.read();
	$Api.get({f:Api.Gsn.a+'crs/history', inputs:'cardId='+Pa.cardId+'&'+$1.G.filter(), loade:cont, func:function(Jr){
			$1.t('h3',{textNode:Jr.cardName},cont);
		if(Jr.errNo){ $Api.resp(cont,Jr); }
		else{
			var wList=$1.t('div');
			for(var i in Jr.L){ var L=Jr.L[i];
				L.cardName=Jr.cardName;
				Gsn.Ht.histCrs(L,wList);
			}
			wList=$Xls.downFrom(wList,{ext:'xlsx',fileName:'Reporte gestión Socio de Negocios',
			Tb:['Tipo Gestión','Usuario','Fecha Gestión','Fecha Sistema','Detalle de gestión','Socio de Negocios']});
			cont.appendChild(wList);
		}
	}});
}
}
Gsn.Ht={
histCrs:function(L,pare){
	var actType=$TbV._g('Gsn.actType_aa',L.actType);
	var userName=$Tb._g('ousr',L.userId);
	var div=$1.t('div',{'class':'_GsnCrsHistoryWrap '+$Xls.Cls.trs,style:'margin:0 0 0.75rem 0; padding:0.5rem; border:0.0625rem solid #DDD;'},pare);
	var divTop=$1.t('div',{style:'font-size:0.8rem'},div);
	$1.t('span',{'class':'fa fa-flash '+$Xls.Cls.tds,textNode:actType+'\u00A0\u00A0\u00A0\u00A0',xls:{textNode:actType}},divTop);
	$1.t('b',{'class':'fa fa-user '+$Xls.Cls.tds,textNode:userName+'\u00A0\u00A0\u00A0\u00A0',xls:{textNode:userName}},divTop);
	$1.t('span',{textNode:'Fecha: '},divTop);
	$1.t('span',{'class':$Xls.Cls.tds,textNode:L.docDate},divTop);
	$1.t('i',{'class':'iBg iBg_date '+$Xls.Cls.tds,textNode:L.dateC},divTop);
	$1.t('b',{textNode:'\u00A0\u00A0\u00A0\u00A0 Contacto: '},divTop);
	if(L.name){ $1.t('span',{'class':$Xls.Cls.tds,textNode:L.name},divTop); }
	var tare=$1.t('div',{style:'padding:0.5rem 0;','class':$Xls.Cls.tds,xls:{textNode:L.lineMemo}},div);
	tare.innerHTML = L.lineMemo;
	$1.t('div',{style:'display:none;','class':$Xls.Cls.tds,textNode:L.cardName},div);
	return tare;
}
}
