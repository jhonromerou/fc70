$M.sAdd([
{fatherId:'mast',L:[{folId:'wmaMast',folName:'Producción'}]},
{fatherId:'wmaMast',MLis:['wmaFas','wmaWop','wmaMaq','jsv.wmaWopGr','sysd.massData.wmaWfa']},
{L:[{folId:'wma3',folName:'Producción',ico:'iBg iBg_produccion'}]},
/* */
{fatherId:'wma3',L:[
{folId:'wma3Plan',folName:'Planificación'},
{folId:'wma3Odp',folName:'Orden de Producción'},
{folId:'wma3GestFases',folName:'Gestión de Fases'},
{folId:'wma3Escandallo',folName:'Escandallos'},
{folId:'wma3Mrp',folName:'MRP',ico:'fa fa fa_cubes'}
]},
{fatherId:'wma3Plan',
MLis:['wma3.opdp','wma3.opdp.consol','wma3.opdp.consolGroup','wma3.opdp.auxCumpProd','wma3.opdp.corteProg']
},
{fatherId:'wma3Odp',MLis:['wma3.oodp','wmaOdp.docHistory','wma3.odp.tbFase']},
{fatherId:'wma3GestFases',MLis:['wma3.ddf','wma3.dcf']},
{fatherId:'wma3Escandallo',MLis:['wmaMpg','wmaBom']},
{fatherId:'wma3Mrp',MLis:['wma3.mrp.fromOpdp','wma3.mrp.fromPep']},

{L:[
{fatherId:'wma3',folId:'wma3PeP',folName:'Producto en Proceso',ico:'fa fa_cubes'}
]},
{fatherId:'wma3PeP',MLis:['pepWhs','pep.lopCat','pepWht','pepIng','pepEgr','pepAwh','pepMov']},
{fatherId:'wma3PeP',L:[
	{folId:'pepRep',folName:'Reportes',ico:'fa fa_bolt'},
]},
{fatherId:'pepRep',MLis:['pepRep.handAt']}
]);