<?php
a_sql::$limitDefBef=20;
$SOC_TbKeys = array(
'__pTriNty',

'par_ocrd','par_odry', 'par_par99','par_oslp',
'par_ocpr','par_cpr1',

'app_com1','app_fil1',
'fqu_oitf','fqu_itf1','fqu_ofre','fqu_fre1',

'a03_ofvc',
'gt1_ogrt',
'gt1_grt1',
'gt1_ortr',
'gt1_rtr1',
'gt1_rtr2',
'gt1_rtr99',
'gt1_rtr3',
'gt1_onot',
'gt1_ogdv',
'par_03_vtr',
'_par_ficha'
);
foreach($SOC_TbKeys as $n =>$k){ _0s::$Tb[$k]=$k; } unset($SOC_TbKeys);

Doc::$owh=false; #no usar ocard

_0s::$jsL['$Tb.GRT']=array('tbk'=>'gt1_ogrt','k'=>'grId','v'=>'grName','inisys'=>'0s');
_0s::$jsL['$V.PtGRT']=array('tbk'=>'gt1_ogrt','type'=>'k-v','k'=>'grId','v'=>'grName','inisys'=>'0s');
require_once(_0s::$Path_static.'/_restcards/dcp/dian_dv.php');
_0s::$jsL['$Tb.GDV']=array('type'=>'data','data'=>json_encode($DIAN_SUBGR),'inisys'=>'0s');

Doc::$sTy=array(
'crdDry'=>array('otb'=>'par_odry','tb1'=>'par_dry1','tb99'=>'par_par99'),
'dcpVis'=>array('otb'=>'a03_ofvc'),
);

class pTri{
static $err=false;
static $errText='';
static public function _r(){
	self::$err=false; self::$errText='';
}
static public function resp_qu($D=array(),$P=array()){
	$date1 = $D['FIE'][0]['RT1.dueDate(E_mayIgual)'];
	$date2 = $D['FIE'][1]['RT1.dueDate(E_menIgual)'];
	$days = 30;
	$mes = 86400*$days;
	$notDays = 15;
	$yearDays = 396;
	$dif = (strtotime($date2)-strtotime($date1));
	if($P['view']=='calendar' && $dif>$mes){
		echo _js::e(3,'En la vista calendario no se pueden exceder los '.$days.' días.'); die();
	}
	else if($P['view']=='viewDCP' && $dif>(86400*$yearDays)){
		echo _js::e(3,'Ninguna vista pueden exceder los '.$yearDays.' días (13 meses).'); die();
	}
	else if($P['view']=='notify' && $dif>(86400*$notDays)){
		echo _js::e(3,'El rango de fechas no puede exceder '.$notDays.' días.'); die();
	}
	$fiAdd = '';
	$groupId = $D['groupId']; unset($D['groupId']);
	$wh = a_sql_filtByT($D,array('FIE_EXTs'=>array(3=>1)));
	if($D['respStatus']=='pendiente'){
		$wh .= ' AND (RT2.respStatus=\'pendiente\' OR RT2.respStatus IS NULL) ';
	}
	else{ $wh .= ' AND RT2.respStatus=\''.$D['respStatus'].'\' '; }
	$_nit = 'C.licTradNum';
	if(_0s::$bussPartnerPrivacity=='Public_Soc'){}
	$whU=a_ses::U_slpIds(array('f'=>'C.slpId','r'=>'in'));
	$twhere = 'AND (C.actived=\'Y\' '.$whU.') ';
	$twhere .= $wh.' ';
	a_sql::$DB->query("SET SESSION sql_mode = ''");
	$qt = ('SELECT RT2.respStatus, RA.year, RA.cardId, C.licTradNum, GR0.grName, RT0.*, '.$_nit.' nit, RT1.dueDate, C.cardName, C.licTradNum, C.RF_regTrib, C.RF_tipEnt, C.slpId,
	IF(RT0.gdvCode =\'0-9\',SUBSTRING('.$_nit.',-1),SUBSTRING('.$_nit.',-2)) dvSys 
	FROM 
	'._0s::$Tb['par_ocrd'].' C
	'.$inerGr.'
	JOIN gt1_grt1 RA ON (RA.cardId = C.cardId)
	JOIN gt1_ogrt GR0 ON (GR0.grId = RA.grId)
	JOIN gt1_ortr RT0 ON (RT0.grId = RA.grId AND RT0.respYear = RA.year 
	AND ((C.RF_regTrib =\'GC\' AND RT0.respTo=\'GC\') 
	OR (C.RF_regTrib!=\'GC\' AND RT0.respTo=C.RF_tipEnt)
	OR (RT0.respTo=\'general\')
	))
	JOIN gt1_rtr1 RT1 ON (RT1.respId = RT0.respId AND 
	(RT1.verif =\'uni\' 
	OR RT1.verif = IF((RT0.gdvCode =\'0-9\' OR RT0.gdvCode =\'1-2\'),SUBSTRING('.$_nit.',-1),SUBSTRING('.$_nit.',-2)) 
	)
	) 
	LEFT JOIN gt1_rtr2 RT2 ON (RT2.respId = RT0.respId AND RT2.cardId = RA.cardId) 
	WHERE 1 '.$twhere.'
	GROUP BY RT0.respId, RT1.respId, RA.cardId
	ORDER BY RT1.dueDate ASC 
	');
	$q = a_sql::query($qt);
	a_sql::$DB->query("SET SESSION sql_mode = '"._0s::$sqlMode."'");
	if(a_sql::$errNo == 1){ self::$err=true; self::$errText=(_js::e(1,'Error obteniendo responsabilidades: '.$q['error_sql'])); }
	else if(a_sql::$errNo == 2){ self::$err=true; self::$errText=(_js::e(2,'No se encontraron vencimientos.'));}
	return $q;
}
static public function due_qu($P=array()){
	if($js=_js::ise($P['date1'],'Se debe definir la fecha de inicio para consulta.')){ self::$err=true; self::$errText=$js; }
	else if($js=_js::ise($P['date2'],'Se debe definir la fecha de fin para consulta.')){ self::$err=true; self::$errText=$js; }
	else{
		$wh=($P['wh'])?' AND '.$P['wh']:'';
		if($P['respStatus']=='pendiente'){
			$wh .= ' AND (RT2.respStatus=\'pendiente\' OR RT2.respStatus IS NULL) ';
		}
		else{ $wh .= ' AND RT2.respStatus=\''.$P['respStatus'].'\' '; }
		$ordBy=($P['ordBy'])?' ORDER BY '.$P['ordBy'].' ':'';
		$_nit='C.licTradNum';
		$fies='gC.respId,C.cardId,C.cardName,gA.grName,gC.respName,gD.dueDate';
		if($P['fie']){ $fies .=','.$P['fie']; }
		$qu='SELECT '.$fies.' FROM 
		par_ocrd C
		JOIN gt1_grt1 gB ON (gB.cardID=C.cardId)
		JOIN gt1_ogrt gA ON (gA.grId=gB.grId)
		JOIN gt1_ortr gC ON (gC.grId=gA.grId AND gC.respYear = gB.year 
		AND ((C.RF_regTrib =\'GC\' AND gC.respTo=\'GC\') 
			OR (C.RF_regTrib!=\'GC\' AND gC.respTo=C.RF_tipEnt)
			OR (gC.respTo=\'general\')
		))
		JOIN gt1_rtr1 gD ON (gD.respId=gC.respId AND 
			(gD.verif =\'uni\' 
			OR gD.verif = IF((gC.gdvCode =\'0-9\' OR  gC.gdvCode =\'1-2\'),SUBSTRING('.$_nit.',-1),SUBSTRING('.$_nit.',-2))
			)
		)
		LEFT JOIN gt1_rtr2 RT2 ON (RT2.respId = gD.respId AND RT2.cardId = gB.cardId) 
		WHERE C.actived=\'Y\' AND gD.dueDate BETWEEN \''.$P['date1'].'\'  AND \''.$P['date2'].'\'
		'.$wh.' 
		GROUP BY '.$fies.$ordBy;
		$q=a_sql::query($qu,array(1=>'Error consultando vencimientos.',2=>'No se encontraron resultados de vencimientos.'));
		if(a_sql::$err){ self::$err=true; self::$errText=a_sql::$errNoText; }
		return $q; 
	}
}
static public function due_quMalo($P=array()){
	if($js=_js::ise($P['date1'],'Se debe definir la fecha de inicio para consulta.')){ self::$err=true; self::$errText=$js; }
	else if($js=_js::ise($P['date2'],'Se debe definir la fecha de fin para consulta.')){ self::$err=true; self::$errText=$js; }
	else{
		$wh=($P['wh'])?' AND '.$P['wh']:'';
		$ordBy=($P['ordBy'])?' ORDER BY '.$P['ordBy'].' ':'';
		$_nit='C.licTradNum';
		$fies='gC.respId,C.cardId,C.cardName,gA.grName,gC.respName,gD.dueDate';
		if($P['fie']){ $fies .=','.$P['fie']; }
		$qu='SELECT '.$fies.' FROM 
		'._0s::$Tb['par_ocrd'].' C
		JOIN gt1_grt1 gB ON (gB.cardID=C.cardId)
		JOIN gt1_ogrt gA ON (gA.grId=gB.grId)
		JOIN gt1_ortr gC ON (gC.grId=gA.grId AND 
			((C.RF_regTrib =\'GC\' AND gC.respTo=\'GC\') 
			OR (C.RF_regTrib!=\'GC\' AND gC.respTo=C.RF_tipEnt)
			OR (gC.respTo=\'general\')
			)
		)
		JOIN gt1_rtr1 gD ON (gD.respId=gC.respId AND 
			(gD.verif =\'uni\' 
			OR gD.verif = IF(gC.gdvCode =\'0-9\',SUBSTRING('.$_nit.',-1),SUBSTRING('.$_nit.',-2))
			)
		)
		WHERE C.actived=\'Y\' AND gD.dueDate BETWEEN \''.$P['date1'].'\'  AND \''.$P['date2'].'\'
		'.$wh.' 
		GROUP BY '.$fies.$ordBy;
		echo 'ALERTJSON '.$qu;
		$q=a_sql::query($qu,array(1=>'Error consultando vencimientos.',2=>'No se encontraron resultados de vencimientos.'));
		if(a_sql::$err){ self::$err=true; self::$errText=a_sql::$errNoText; }
		else{ return $q; } 
	}
}
}
?>