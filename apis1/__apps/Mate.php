<?php
class Mate{
static public function stockValue($L=array(),$P=array()){
	if($P['in']){
		if(!$L['lineTotal']){ $L['lineTotal']=0; }
		$L['._.stockValue']='+-+stockValue+'.$L['lineTotal'];
		$L['._.avgPrice']='+-+stockValue/onHand';
		$L['avgPrice']=$L['lineTotal']/$L['quantity'];
		$L['stockValue']=$L['lineTotal'];
	}
	else if($P['out']){/* sale costo estandar */
		if($L['lineTotal']){
			$L['._.stockValue']='+-+IF(onHand>0,stockValue-('.$L['lineTotal'].'),0)';
		}
		else{ $L['._.stockValue']='+-+stockValue-(avgPrice*'.$L['quantity'].')'; }
		$L['._.avgPrice']='+-+IF(onHand>0,stockValue/onHand,avgPrice)';
		$L['avgPrice']=$L['lineTotal']/$L['quantity']; /* ultimo costo salida */
		$L['stockValue']=$L['lineTotal'];
	}
	return $L;
}
}

?>