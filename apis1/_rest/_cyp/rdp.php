<?php
if(_0s::$router=='POST rdp'){
	if($js=_js::ise($___D['cuoId'],'Se debe definir el ID de la cuota','numeric>0')){}
	else if($js=_js::ise($___D['docDate'],'Se debe definir la fecha del pago')){}
	else if($js=_js::ise($___D['bal'],'La cantidad del pago debe ser un número mayor a 0','numeric>0')){}
	else if($js=_js::textLen($___D['lineMemo'],255,'Los detalles no pueden exceder los 255 caracteres.')){}
	else{
		$qC=a_sql::fetch('SELECT lineStatus,cuoV,dueBal FROM '._0s::$Tb['pad_ddc1'].' WHERE cuoId=\''.$___D['cuoId'].'\' LIMIT 1',array(1=>'Error consultando la cuota en el sistema.',2=>'La cuota a pagar no existe.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else if($qC['lineStatus']=='C'){ $js=_js::e(3,'La cuota que intenta pagar se encuentra cerrada.'); }
		else if($___D['bal']>$qC['cuoV']){ $js=_js::e(3,'El valor ingresado es mayor al valor de la cuota.'); }
		else if($___D['bal']>$qC['dueBal']){ $js=_js::e(3,'El valor ingresado es mayor al valor pendiente por pagar.'); }
		else{
			a_sql::transaction(); $cmt=false;
			a_sql::quTrans(array(
			array('q'=>'UPDATE '._0s::$Tb['pad_ddc1'].' SET dueBal=dueBal-'.$___D['bal'].', lineStatus=IF(dueBal=0,\'C\',lineStatus) WHERE cuoId=\''.$___D['cuoId'].'\' LIMIT 1',1=>'Error actualizado saldo de la cuota: '),
			array('q'=>'::insert','D'=>$___D,'D2'=>array('tbk'=>'pad_rdp1','qDo'=>'insert'))
			));
		if(_err::$err){ $js=_err::$errText; }
		else{ $cmt=true; $js=_js::r('Pago recibido correctamente.'); }
		a_sql::transaction($cmt);
		}
	}
	echo $js;
}
?>