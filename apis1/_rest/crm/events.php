<?php
$fieCom='A.calId,A.doDate,A.doDateAt,A.endDate,A.endDateAt,A.priority,A.wLocation,A.shortDesc';
if(_0s::$router=='GET events'){ a_ses::hashKey('crmBasic');
	_ADMS::_lb('sql/filter');
	_ADMS::lib('_2d');
	$___D['wh']['A.obj']='event';
	if($___D['window']=='calendar'){
		$___D['profile']='event';
		$___D['wh']['A.onCal']='Y';
		$___D['whText']='('._2d::rangFields(array('A.doDate',$___D['date1'],'A.endDate',$___D['date2'])).')';
		unset($___D['wh']['A.obj']);
	}
	$___D['_fie']=$fieCom;
	echo crm::nov_get($___D,array('prsLeft'=>'Y'));
}
else if(_0s::$router=='GET events/view'){ a_ses::hashKey('crmBasic');
	$___D['_fie']=$fieCom;
	echo crm::nov_one($___D);
}
else if(_0s::$router=='GET events/form'){ a_ses::hashKey('crmBasic');
	$___D['_fie']=$fieCom;
	echo crm::nov_one($___D);
}
else if(_0s::$router=='POST events'){ a_ses::hashKey('crmBasic');
	//if($js=_js::ise($___D['oType'],'El tipo debe estar definidido','numeric>0')){}
	if($js=_js::ise($___D['title'],'Se debe definir un nombre para el evento.')){}
	else if($js=_js::textLen($___D['title'],200,'El nombre no puede exceder los 200 caracteres.')){}
	else if($js=_js::ise($___D['doDate'],'Se debe definir la fecha de inicio.')){}
	else if($js=_js::ise($___D['endDate'],'Se debe definir la fecha de fin.')){}
	else if(strtotime($___D['endDate']) <strtotime($___D['doDate'])){ $js=_js::e(3,'Fecha de fin es menor a fecha inicial.'); }
	else if($js=_js::textLen($___D['shortDesc'],340,'La descripción no puede exceder 340 caracteres.')){}
	else{
		_ADMS::_lb('com/_2d');
		$___D['timeDuration']=_2d::relTime($___D['doDate'],$___D['endDate']);
		$___D['obj']='event';
		$INV=$___D['INV']; unset($___D['INV']);
		$___D=crm::nov_post($___D);
		if(_err::$err){ $js=_err::$errText; }
		else{
			crm::events_invSave($INV,array('gid'=>$___D['gid']));
			if(_err::$err){ $js=_err::$errText; }
			else{
				$___D['doDate'] .= ' '.$___D['doDateAt'];
				$___D['endDate'] .= ' '.$___D['endDateAt'];
				crm::events_invSend($___D);
				if(_err::$err){ $js=_err::$errText; }
				else{
					$js=_js::r('Evento programado correctamente.',$___D);
					a_sql::transaction(true);
				}
			}
		}
	}
	echo $js;
}
else if(_0s::$router=='PUT events'){ a_ses::hashKey('crmBasic');
	if($js=_js::ise($___D['gid'],'Debe definir ID para actualizar.','numeric>0')){}
	else if($js=_js::ise($___D['oType'],'El tipo debe estar definidido','numeric>0')){}
	else if($js=_js::ise($___D['priority'],'Se debe definir la prioridad del evento.')){}
	else if($js=_js::ise($___D['title'],'Se debe definir un asunto para el evento.')){}
	else if($js=_js::textLen($___D['title'],200,'El asunto no puede exceder los 200 caracteres.')){}
	else if($js=_js::textLen($___D['shortDesc'],340,'La descripción no puede exceder 340 caracteres.')){}
	else if($js=_js::ise($___D['doDate'],'Se debe definir la fecha de inicio.')){}
	else if($js=_js::ise($___D['endDate'],'Se debe definir la fecha de fin.')){}
	else if(strtotime($___D['endDate']) <strtotime($___D['doDate'])){ $js=_js::e(3,'Fecha de fin es menor a fecha inicial.'); }
	else{
		_ADMS::_lb('com/_2d');
		$___D['timeDuration']=_2d::relTime($___D['doDate'],$___D['endDate'],array('format'=>'hours'));
		$INV=$___D['INV']; unset($___D['INV']);
		a_sql::transaction();
		$___D=crm::nov_put($___D);
		if(_err::$err){ $js=_err::$errText; }
		else{
			crm::events_invSave($INV,array('gid'=>$___D['gid']));
			if(_err::$err){ $js=_err::$errText; }
			else{
				$___D['doDate'] .=' '.$___D['doDateAt'];
				$___D['endDate'] .= ' '.$___D['endDateAt'];
				crm::events_invSend($___D);
				if(_err::$err){ $js=_err::$errText; }
				else{
					$js=_js::r('Evento actualizado correctamente.',$___D);
					a_sql::transaction(true);
				}
			}
		}
	}
	echo $js;
}

/* Send Reminder */
else if(_0s::$router=='GET events/daily'){ a_ses::hashKey('crmBasic');
	_ADMS::_lb('sql/filter');
	a_sql::$limitDefBef=50;
	$___D['wh']['A.onCal']='Y';
	//$___D['wh']['A.endDate']=date('Y-m-d');
	$___D['_fie']='A.repeatKey,A.endDate,A.endDateAt';
	$___D['repeatKey']='Y';
	_ADMS::lib('dateCicle');
	echo crm::nov_get($___D,array('prsLeft'=>'Y'));
}
?>