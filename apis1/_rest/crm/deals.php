<?php
if(_0s::$router=='GET deals'){ //a_ses::hashKey('wma3.ddf.basic');
	_ADMS::_lb('sql/filter');
	$___D['wh']['A.obj']='deal';
	$___D['_fie']='A.doDate,A.dueDate';
	echo crm::nov_get($___D,array('prsLeft'=>'Y'));
}
else if(_0s::$router=='GET deals/form'){ //a_ses::hashKey('wma3.ddf.basic');
		echo crm::nov_one($___D);
}
else if(_0s::$router=='POST deals'){
	if($js=_js::ise($___D['oType'],'El tipo debe estar definidido','numeric>0')){}
	else if($js=_js::ise($___D['oClass'],'Se debe definir la fase de la oportunidad.','numeric>0')){}
	else if($js=_js::ise($___D['title'],'Se debe definir un nombre para la oportunidad.')){}
	else if($js=_js::textLen($___D['title'],200,'El nombre no puede exceder los 200 caracteres.')){}
	else{
		$___D['obj']='deal';
		$___D=crm::nov_post($___D);
		if(_err::$err){ $js=_err::$errText; }
		else{$js=_js::r('Oportunidad generada correctamente.',$___D); }
	}
	echo $js;
}
else if(_0s::$router=='PUT deals'){
	if($js=_js::ise($___D['gid'],'Debe definir ID para actualizar.','numeric>0')){}
	else if($js=_js::ise($___D['oType'],'El tipo debe estar definidido','numeric>0')){}
	else if($js=_js::ise($___D['oClass'],'Se debe definir la fase','numeric>0')){}
	else if($js=_js::ise($___D['title'],'Se debe definir el asunto.')){}
	else if($js=_js::textLen($___D['title'],200,'EL asunto no puede exceder los 200 caracteres.')){}
	else{
		$ins=a_sql::insert($___D,array('table'=>'crm_onov','qDo'=>'update','updater'=>'uid_dateC','wh_change'=>'WHERE gid=\''.$___D['gid'].'\' LIMIT 1'));
		if($ins['err']){ $js=_js::e(3,'Error actualizando oportunidad: '.$ins['err']['error_sql']); }
		else{
			$q=a_sql::fetch('SELECT * FROM crm_onov WHERE gid=\''.$___D['gid'].'\' LIMIT 1',array(1=>'Error obteniendo información guardada',2=>'No se guardo..'));
			if(!a_sql::$err){ $___D=$q;}
			$js=_js::r('Oportunidad actualizada correctamente.',$___D);
		}
	}
	echo $js;
}
?>