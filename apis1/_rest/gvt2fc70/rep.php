<?php
if(_0s::$router==('GET rep/dlv1')){
	$D=$_GET;
	$datep1=$D['datep1']; $datep2=$D['datep2'];
	$date1=$D['date1'];
	$date2=$D['date2'];
  if(_js::iseErr($datep1,'Fecha inicio para pedidos debe definirse')){}
  else if(_js::iseErr($datep1,'Fecha fin para pedidos debe definirse')){}
  else if(_js::iseErr($date1,'Fecha inicio debe estar definida')){}
	else if(_js::iseErr($date2,'Fecha cortre debe estar definida')){}
  _err::errDie();
	_ADMS::lib('sql/filter');
	$vt=$D['viewType'];
	unset($D['datep1'],$D['datep2'],$D['date1'],$D['date2'],$D['viewType']);
	$whB='P.docEntry=15535 AND I.itemCode=103 AND B2.itemSzId=41 AND '; //'A.canceled=\'N\'';
	$whB='P.canceled=\'N\' AND P.docDate>=\''.$datep1.'\' AND P.docDate<=\''.$datep2.'\' '.a_sql_filter($D).' '; //'A.canceled=\'N\'';
	if($vt=='C'){
		$Mx=[];
		$gby1='P.docEntry,P.docStatus,P.dlvStatus,P.cardName,P.slpId,I.itemId, I.itemCode, I.itemName, P1.itemSzId,P.docDate,P.dueDate, P1.quantity';
		$gby2='P.docEntry,P.docStatus,P.dlvStatus,P.cardName,P.slpId,I.itemId, I.itemCode, I.itemName, P1.itemSzId, P.docDate,P.dueDate, P1.quantity';
		$q1=a_sql::query('SELECT '.$gby1.',SUM(B2.quantity) dlvQtyBef,0 dlvQty 
		FROM gvt_opvt P
		JOIN gvt_pvt1 P1 ON (P1.docEntry=P.docEntry)
		LEFT JOIN itm_oitm I ON (I.itemId=P1.itemId)
		LEFT JOIN gvt_odlv A ON (A.tr=P.docEntry AND A.canceled=\'N\' AND A.docDate<\''.$date1.'\') 
		LEFT JOIN gvt_dlv1 B2 ON (B2.docEntry=A.docEntry AND B2.itemId=P1.itemId AND B2.itemSzId=P1.itemSzId)
		WHERE '.$whB.'  GROUP BY '.$gby2);
		if(a_sql::$errNo==-1) while($L=$q1->fetch_assoc()){
			$k=$L['docEntry'].$L['itemId'].$L['itemSzId'];
			if(!$Mx[$k]){ $Mx[$k]=$L; }
		}
		$q1=a_sql::query('SELECT '.$gby1.',0 dlvQtyBef,SUM(B2.quantity) dlvQty 
		FROM gvt_opvt P
		JOIN gvt_pvt1 P1 ON (P1.docEntry=P.docEntry)
		LEFT JOIN itm_oitm I ON (I.itemId=P1.itemId)
		LEFT JOIN gvt_odlv A ON (A.tr=P.docEntry AND A.canceled=\'N\' AND A.docDate>=\''.$date1.'\' AND A.docDate<=\''.$date2.'\') 
		LEFT JOIN gvt_dlv1 B2 ON (B2.docEntry=A.docEntry AND B2.itemId=P1.itemId AND B2.itemSzId=P1.itemSzId)
		WHERE '.$whB.'  GROUP BY '.$gby2);
		if(a_sql::$errNo==-1) while($L=$q1->fetch_assoc()){
			$k=$L['docEntry'].$L['itemId'].$L['itemSzId'];
			if(!$Mx[$k]){ $L['dlvQtyBef']=0; $Mx[$k]=$L; }
			if($L['dlvQty']){ 
				$Mx[$k]['dlvQty']=$L['dlvQty'];
			}
		}
		die(_js::enc2(['_view'=>$vt,'L'=>$Mx]));
  }

	echo a_sql::fetchL($query,
	['k'=>'L','D'=>['_view'=>$vt]],true);
}
else if(_0s::$router==('GET rep/ger')){
	$D=$_GET; $wh='';
	if($D['viewType']=='P' && _js::iseErr($D['whsId'],'Este reporte requiere definir el almacen','numeric>0')){}
	else if($D['viewType']=='P' && _js::iseErr($D['docClass'],'Este reporte requiere definir la clasificación')){};
	_err::errDie();
	$EX=['itm'=>[],'card'=>[],'slp'=>[]];
	$M=['_view'=>$D['viewType'],'docTotal'=>0,'reqQtyTotal'=>0,
	'itm'=>[],'card'=>[],'slp'=>[],'log'=>[],
	'L'=>[],
	'D1'=>[],'D2'=>[],'D3'=>[]];
	if($D['whsId']){ $wh .=' AND A.whsId=\''.$D['whsId'].'\' '; }
	if($_GET['viewRang']=='M'){
		$date1=$_GET['year2'].'-'.$D['month1'].'-01';
		$date2=date('Y-m-t',strtotime($D['year2'].'-'.$D['month2'].'-28'));
	}
	else{ $date1=$D['date1']; $date2=$D['date2']; }
	$date2Dlv=date('Y-m-d',strtotime($date2)+($D['dlvTol']*86400)); //dias adicionales
	$datebet='BETWEEN \''.$date1.'\' AND \''.$date2.'\' ';
	$datebet2='BETWEEN \''.$date1.'\' AND \''.$date2Dlv.'\' ';
	if($D['viewType']=='O'){#Pedidos Recibidos y despachos en el mismo mes, por cliente, por articulo 
	$qu='SELECT A.docEntry,A.docDate,A.dueDate,A.cardId,A.cardName,A.slpId,I.itemCode,I.itemName,B.itemId,B.itemSzId,B.quantity,B.quantity reqQty, B.price,B.priceLine,B.priceList,B.disc
	FROM gvt_opvt A
	JOIN gvt_pvt1 B ON (B.docEntry=A.docEntry)
	JOIN itm_oitm I ON (I.itemId=B.itemId)
	WHERE A.canceled=\'N\' '.$wh.' AND A.docDate  '.$datebet.$wh;
	$q=a_sql::query($qu,[1=>'Error obteniendo reporte de ordenes',2=>'El reporte de orden no encontró resultados']);
	$n1=$n2=$n3=$nlog=$n4=0; $docs=[];
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else while($L=$q->fetch_assoc()){
		$M['reqQtyTotal'] +=$L['reqQty'];
		$M['docTotal'] +=$L['priceLine'];
		$kx=$L['itemId'];
		if(!$EX['itm'][$kx]){
			$M['itm'][$n1]=['itemCode'=>$L['itemCode'],'itemName'=>$L['itemName'],'itemSzId'=>$L['itemSzId'],'reqQty'=>0,'priceLine'=>0,'priceList'=>0];
			$EX['itm'][$kx]=$n1; $n1++;
		} $kx=$EX['itm'][$kx];
		$M['itm'][$kx]['reqQty'] += $L['reqQty'];
		$M['itm'][$kx]['priceLine'] += $L['priceLine'];
		$M['itm'][$kx]['priceList'] += $L['priceList']*$L['reqQty'];
		$kx=$L['cardId'];
		if(!$EX['card'][$kx]){
			$M['card'][$n2]=['cardName'=>$L['cardName'],'reqQty'=>0,'priceLine'=>0,'priceList'=>0];
			$EX['card'][$kx]=$n2; $n2++;
		}$kx=$EX['card'][$kx];
		$M['card'][$kx]['reqQty'] += $L['reqQty'];
		$M['card'][$kx]['priceLine'] += $L['priceLine'];
		$M['card'][$kx]['priceList'] += $L['priceList']*$L['reqQty'];
		$kx=$L['slpId'];
		if(!$EX['slp'][$kx]){
			$M['slp'][$n3]=['slpId'=>$L['slpId'],'reqQty'=>0,'priceLine'=>0,'priceList'=>0];
			$EX['slp'][$kx]=$n3; $n3++;
		}$kx=$EX['slp'][$kx];
		$M['slp'][$kx]['reqQty'] += $L['reqQty'];
		$M['slp'][$kx]['priceLine'] += $L['priceLine'];
		$M['slp'][$kx]['priceList'] += $L['priceList']*$L['reqQty'];
		$kx=$L['cardId'];
		if(!$EX['log'][$kx]){
			$M['log'][$nlog]=['cardName'=>$L['cardName'],'reqQty'=>0,'docsQty'=>[]];
			$EX['log'][$kx]=$nlog; $nlog++;
		}$kx=$EX['log'][$kx];
		$M['log'][$kx]['reqQty'] += $L['reqQty'];
		$doc=$L['docEntry'];
		if(!$M['log'][$kx]['docsQty'][$doc]){ $M['log'][$kx]['docsQty'][$doc]=1; }
		$M['L'][]=$L;
	}}
	if($D['viewType']=='D'){#despachos del mes y cuales con atraso
	$qb='FROM gvt_odlv A
	JOIN gvt_dlv1 B ON (B.docEntry=A.docEntry)
	JOIN itm_oitm I ON (I.itemId=B.itemId)
	JOIN gvt_opvt D ON (A.tr=D.docEntry AND D.canceled=\'N\')
	JOIN gvt_pvt1 D1 ON (D1.docEntry=D.docEntry AND D1.itemId=B.itemId AND D1.itemSzId=B.itemSzId)
	WHERE A.canceled=\'N\' '.$wh.' AND A.docDate '.$datebet.'';
	$gb='D.docEntry,D.cardId,D.cardName,D.slpId,D.docDate,D.dueDate,I.itemCode,I.itemName,B.itemId,B.itemSzId,B.priceList,B.priceLine,B.disc';
	$q=a_sql::query('SELECT A.docEntry dlvEntry,'.$gb.',A.docDate dlvDate,SUM(B.quantity) reqQty '.$qb.' GROUP BY A.docEntry,'.$gb.',A.docDate');
	$n1=$n2=$n3=$n4=0;
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else while($L=$q->fetch_assoc()){
		$M['reqQtyTotal'] +=$L['reqQty'];
		$M['docTotal'] +=$L['priceLine'];
		$kx=$L['itemId'];
		if(!$EX['itm'][$kx]){
			$M['itm'][$n1]=['itemCode'=>$L['itemCode'],'itemName'=>$L['itemName'],'itemSzId'=>$L['itemSzId'],'reqQty'=>0,'priceLine'=>0,'priceList'=>0];
			$EX['itm'][$kx]=$n1; $n1++;
		} $kx=$EX['itm'][$kx];
		$M['itm'][$kx]['reqQty'] += $L['reqQty'];
		$M['itm'][$kx]['priceLine'] += $L['priceLine'];
		$M['itm'][$kx]['priceList'] += $L['priceList']*$L['reqQty'];
		$kx=$L['cardId'];
		if(!$EX['card'][$kx]){
			$M['card'][$n2]=['cardName'=>$L['cardName'],'reqQty'=>0,'priceLine'=>0,'priceList'=>0];
			$EX['card'][$kx]=$n2; $n2++;
		}$kx=$EX['card'][$kx];
		$M['card'][$kx]['reqQty'] += $L['reqQty'];
		$M['card'][$kx]['priceLine'] += $L['priceLine'];
		$M['card'][$kx]['priceList'] += $L['priceList']*$L['reqQty'];
		$kx=$L['slpId'];
		if(!$EX['slp'][$kx]){
			$M['slp'][$n3]=['slpId'=>$L['slpId'],'reqQty'=>0,'priceLine'=>0,'priceList'=>0];
			$EX['slp'][$kx]=$n3; $n3++;
		}$kx=$EX['slp'][$kx];
		$M['slp'][$kx]['reqQty'] += $L['reqQty'];
		$M['slp'][$kx]['priceLine'] += $L['priceLine'];
		$M['slp'][$kx]['priceList'] += $L['priceList']*$L['reqQty'];
		$M['L'][]=$L;
	}
	}
	if($D['viewType']=='P'){#produccion
	$gb='B.itemId,I.itemCode,I.itemName,B.itemSzId';
	$q=a_sql::query('SELECT '.$gb.',SUM(B.quantity) reqQty FROM ivt_ocat A 
	JOIN ivt_cat1 B ON (B.docEntry=A.docEntry)
	LEFT JOIN itm_oitm I ON (I.itemId=B.itemId) 
	WHERE A.docStatus !=\'N\' '.$wh.' AND A.docDate '.$datebet.' GROUP BY '.$gb,[1=>'Error obteniendo produccion',2=>'No se encontraron resultados']);
	$n1=$n2=$n3=$n4=0;
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else while($L=$q->fetch_assoc()){
		$M['reqQtyTotal'] +=$L['reqQty'];
		$kx=$L['itemId'];
		if(!$EX['itm'][$kx]){
			$M['itm'][$n1]=['itemCode'=>$L['itemCode'],'itemName'=>$L['itemName'],'itemSzId'=>$L['itemSzId'],'reqQty'=>0];
			$EX['itm'][$kx]=$n1; $n1++;
		} $kx=$EX['itm'][$kx];
		$M['itm'][$kx]['reqQty'] += $L['reqQty'];
		$M['L'][]=$L;
	}
	}
	
	
	echo _js::enc2($M);
}
?>