<?php
if(_0s::$router== 'GET reproDoc'){
	$_J = $_GET;
	_ADMS::lib('sql/filter,_2d');
    $Ra=_2d::viewRang('A.docDate',$_J);
    unset($_J['viewRang'], $_J['date1'], $_J['date2'], $_J['month1'], $_J['month2'], $_J['year1'], $_J['year2']);
	$wh=a_sql_filter($_J);
	echo Doc::get(array('fromA'=>'A.docEntry,A.dateC,A.userId,A.area,A.docDate,A.docTotal,A.docStatus,A.canceled FROM zes_orep A ','whA'=>$wh.' '.$Ra['wh']));
}
else if(_0s::$router==('GET reproDoc/view')){
	echo Doc::getOne(array('docEntry'=>$_GET['docEntry'],
	'fromA'=>'A.* FROM zes_orep A',
	'fromB'=>'B.*,I.itemCode,I.itemName,I.grsId,I.src1 FROM zes_rep1 B JOIN itm_oitm I ON (I.itemId=B.itemId) ','whB'=>'ORDER BY B.lineNum ASC'));
}
else if(_0s::$router==('GET reproDoc/form')){
	echo Doc::getOne(array('docEntry'=>$_GET['docEntry'],
	'fromA'=>'A.* FROM zes_orep A',
	'fromB'=>'B.*,I.itemCode,I.itemName,I.grsId,I.src1 FROM zes_rep1 B JOIN itm_oitm I ON (I.itemId=B.itemId) ','whB'=>'ORDER BY B.lineNum ASC'));
}
else if(_0s::$router==('POST reproDoc') || _0s::$router==('PUT reproDoc')){
	save($_J);
}

else if(_0s::$router==('PUT reproDoc/statusCancel')){
	_ADMS::lib('iDoc');
	echo iDoc::putStatus(array('closeOmit'=>'Y','t'=>'N','tbk'=>'zes_orep','docEntry'=>$___D['docEntry'],'serieType'=>'reproDoc','log'=>'zes_doc99','reqMemo'=>'Y','lineMemo'=>$___D['lineMemo']));
}
else if(_0s::$router==('PUT reproDoc/statusC')){
	_ADMS::lib('iDoc');
	echo iDoc::putStatus(array('t'=>'C','tbk'=>'zes_orep','docEntry'=>$___D['docEntry'],'serieType'=>'reproDoc','log'=>'zes_doc99'));
}

else if(_0s::$router=='GET reproDoc/tb99'){
	_ADMS::lib('JLog');
	echo JLog::get(array('tbk'=>'zes_doc99','serieType'=>'reproDoc','docEntry'=>$_GET['docEntry']));
}

else if(_0s::$router=='GET reproDoc/rep'){
	_ADMS::lib('sql/filter,_2d');
    $D = $___D;
    $Ra=_2d::viewRang('A.docDate',$D);
	$vt=$D['viewType'];
    unset($D['viewType'], $D['viewRang'], $D['date1'], $D['date2'], $D['month1'], $D['month2'], $D['year1'], $D['year2']);
	$whB='A.canceled=\'N\' '.$Ra['wh'];
	if($vt=='G'){
		$fie =' A.area,I.itemCode,I.itemName,B.itemSzId,B.parte,B.causa,B.solucion';
		$query = 'SELECT '.$fie.', SUM(B.quantity) quantity, SUM(B.price) price
		FROM zes_orep A
		JOIN zes_rep1 B ON (B.docEntry=A.docEntry)
        JOIN itm_oitm I ON (I.itemId=B.itemId)
		WHERE '.$whB.' '.a_sql_filtByT($D).' 
		GROUP BY '.$fie;
	}
	else {
		$gb='';
		$query='SELECT A.docEntry,A.docDate,A.area,A.docTotal,B.quantity,I.itemCode,I.itemName,B.itemSzId,B.parte,B.causa,B.solucion,B.price
		FROM zes_orep A
		JOIN zes_rep1 B ON (B.docEntry=A.docEntry)
        JOIN itm_oitm I ON (I.itemId=B.itemId)
		WHERE '.$whB.' '.a_sql_filtByT($D).' ';
	}
	echo a_sql::fetchL($query,
	['k'=>'L','D'=>['_view'=>$vt]],true);
}

function save($_J) {
    _ADMS::lib('iDoc,JLog');
    if ($_J['docEntry']) {
        iDoc::vStatus(array('docEntry'=>$_J['docEntry'],'tbk'=>'zes_orep'));
        if(_err::$err){  die(_err::$errText); }
    }

	_ADMS::_lb('com/_2d');
    $Lx = $_J['L']; unset($_J['L']);
	if($js=_js::ise($_J['area'],'Se debe definir el area.')){}
	else if($js=_js::ise($_J['docDate'],'La fecha del documento debe estar definida.','Y-m-d')){}
	else if(!_js::textLimit($_J['lineMemo'])){ $js= _js::e(3,'Los detalles no pueden exceder los 255 caracteres.'); }
	else if(!is_array($Lx)){ $js=_js::e(3,'No se han enviado lineas para el documento.'); }
	else{
        $_J['docTotal'] = 0;
		foreach($Lx as $nk=>$L){
			$ln ='Linea '.$n.': ';
			if($js=_js::ise($L['itemId'],$ln.'No se ha encontrado el ID del artículo.','numeric>0')){ $errs++; break; }

            if($js=_js::ise($L['itemSzId'],$ln.'No se ha encontrado el ID del subproducto.','numeric>0')){ $errs++; break; }

            if($js=_js::ise($L['quantity'],$ln.'La cantidad debe ser mayor a 0','numeric>0')){ $errs++; break; }

            if($L['price']>0){
                $_J['docTotal'] += $L['price']*1;
            }
		}
		if($errs==0 && count($Lx)==0){ die(_js::e(3,'No se han definido lineas a guardar luego de la verificación.')); }
		if($errs==0){
		a_sql::transaction(); $comit=false;
        $L = $__D['L']; unset($__D['L']);
        $_J['docStatus'] = 'O';
		$ins=a_sql::insert($_J,array('kui'=>'uid_dateC','table'=>'zes_orep','wh_change'=>'WHERE docEntry=\''.$_J['docEntry'].'\' LIMIT 1'));
		if($ins['err']){ $js=_js::e(1,$ins['text']); }
		else{
			$docEntry=($ins['insertId'])?$ins['insertId']:$_J['docEntry'];
			$jsEntry='"docEntry":"'.$docEntry.'"';
			$Wh=array();
            $ln = 1;
			foreach($Lx as $nk=>$L2){
                $L2['lineNum'] = $ln; $ln++;
				$whc='WHERE docEntry=\''.$docEntry.'\' AND lineNum=\''.$L2['lineNum'].'\' LIMIT 1';
				$L2['docEntry']=$docEntry;
				$ins2=a_sql::insert($L2,array('tbk'=>'zes_rep1','wh_change'=>$whc));
				if($ins2['err']){
					$js=_js::e(1,'Linea '.$n.': '.$ins2['text'],$jsEntry);
					$errs++; break;
				}
			}
			if($errs==0){ $comit=true;
				$js=_js::r('Información guardada correctamente.',$jsEntry);
				if($ins['insertId']){
                    JLog::post(array('tbk'=>'zes_doc99','serieType'=>'reproDoc','docEntry'=>$docEntry,'dateC'=>1));
			    }
                else{
                    JLog::post(array('tbk'=>'zes_doc99','serieType'=>'reproDoc','docEntry'=>$docEntry,'update'=>'Y'));
                }
			}
			a_sql::transaction($comit);
		}
		}
	}
	echo $js;
}
?>