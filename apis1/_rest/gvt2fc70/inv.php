<?php 
$serieType='gvtInv';
Doc::$owh=false;
class docSerie{
static public function revi($P=array()){
	if($js=_js::ise($P['serieId'],'Se debe definir el Id de la serie.','numeric>0')){ return $js; }
	return false;
}
static public function nextNum($P=array(),$RD=false){
	$num=0;
	if($js=_js::ise($P['serieId'],'Se debe definir el Id de la serie.','numeric>0')){ _err::err($js); }
	else{
		$q=a_sql::fetch('SELECT nextNum FROM doc_oser WHERE serieId=\''.$P['serieId'].'\' LIMIT 1',array(1=>'Error obteniendo consecutivo de serie.',2=>'La serie no tiene definida información.'));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			if($q['nextNum']==0){ $num=1; }
			else{ $num=$q['nextNum']; }
			$q=a_sql::query('UPDATE doc_oser SET nextNum=nextNum+1 WHERE serieId=\''.$P['serieId'].'\' LIMIT 1',array(1=>'Error actualizando consecutivo de serie.'));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		}
	}
	if(is_array($RD)){
		$RD['serieId']=$P['serieId'];
		$RD['docNum']=$num;
		return $RD;
	}
	return array('docNum'=>$num);
}
static public function form($P=array()){
	$num=0;
	if($js=_js::ise($P['serieId'],'Se debe definir el Id de la serie.','numeric>0')){ _err::err($js); }
	else{
		$q=a_sql::fetch('SELECT THs,TLs FROM doc_oser WHERE serieId=\''.$P['serieId'].'\' LIMIT 1',array(1=>'Error obteniendo consecutivo de serie.',2=>'La serie no tiene definida información.'));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		$P['THs']=$q['THs'];
		$P['TLs']=$q['TLs'];
		return $P;
	}
}
}

if(_0s::$router=='GET inv'){
	$___D['fromA']='A.docEntry,A.cardId,A.cardName,A.docDate,A.dueDate,A.slpId,P.name prsName,A.fdpId,A.docTotal,A.dateC,A.userId
	FROM gvt_oinv A
	LEFT JOIN par_ocpr P ON (P.prsId=A.prsId)';
	echo Doc::get($___D);
}
else if(_0s::$router=='GET inv/tb99'){
	echo Doc::tb99($serieType,$___D);
}
else if(_0s::$router=='POST inv'){ //a_ses::hashKey('ivt.wht.basic');
	unset($_J['prsName']);
	if($js=docSerie::revi($_J)){ return $js; }
	else if($js=_js::ise($_J['docDate'],'Se debe definir la fecha.')){}
	else if($js=_js::ise($_J['dueDate'],'Se debe definir la fecha de vencimiento.')){}
	else if($js=_js::ise($_J['cardId'],'Se debe definir ID del socio de negocios.','numeric>0')){}
	else if($js=_js::ise($_J['cardName'],'Se debe definir socio de negocios.')){}
	else if($js=_js::ise($_J['slpId'],'Se debe definir responsable.','numeric>0')){}
	else if($js=_js::ise($_J['prsId'],'Se debe definir persona de contacto.','numeric>0')){}
	else if($js=_js::ise($_J['fdpId'],'Se debe definir forma de pago.','numeric>0')){}
	else if(!_js::textLimit($_J['lineMemo'],200)){ $js=_js::e(3,'Los detalles no pueden exceder 200 caracteres.'); }
	else if(!is_array($_J['L']) || count($_J['L'])==0){ die(_js::e(3,'No se han enviado lineas a guardar.')); }
	else{
		$Ld=$_J['L'];
		$Rtes=$_J['Rtes']; $Vats=$_J['Vats'];
		unset($_J['L'],$_J['Rtes'],$_J['Vats']);
		$errs=0; $nl=0;
		$_J['baseAmnt']=$_J['vatSum']=$_J['rteSum']=$_J['docTotal']=$_J['discSum']=0;
		foreach($Ld as $n => $L){ $nl++;
			$ln='Linea '.$nl.': '; 
			if($js=_js::ise($L['itemId'],$ln.'Se debe definir el ID del artículo.')){ $errs++; break; }
			else if($js=_js::ise($L['itemSzId'],$ln.'Se debe definir el ID de la talla del artículo.')){ $errs++; break; }
			else if($js=_js::ise($L['price'],$ln.'Se debe definir el precio del artículo.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'Se debe definir la cantidad.','numeric>0')){ $errs++; break; }
			else if($L['handInv']=='Y' && $js=_js::ise($L['whsId'],$ln.'Se debe definir la bodega.')){ $errs++; break; }
			else if(!_js::textLimit($L['lineText'],200)){ $js=_js::e(3,'La descripción de linea no puede exceder 200 caracteres.'); }
			else{
				$Ld[$n]['lineNum']=$nl;
				$_J['baseAmnt']+=$L['lineTotal'];
				$_J['vatSum']+=$L['vatSum'];
				$_J['rteSum']+=$L['rteSum'];
				$_J['discSum']+=$L['discSum'];
			}
		}
		if($errs==0){ unset($_J['undefined']);
			$_J['docTotal']=$_J['baseAmnt']+$_J['vatSum']-$_J['rteSum']-($_J['rteIvaSum']*1)-($_J['rteIcaSum']*1);
			a_sql::transaction(); $cmt=false;
			$_J=docSerie::nextNum($_J,$_J);
			if(_err::$err){ die(_err::$errText); }
			$ins=a_sql::insert($_J,array('table'=>'gvt_oinv','qDo'=>'insert','kui'=>'uid_dateC'));
			if($ins['err']){ $js=_js::e(3,'Error guardando documento: '.$ins['text']); $errs++; }
			else{ $Liv=array();
			$docEntry=($ins['insertId'])?$ins['insertId']:$_J['docEntry'];
			foreach($Ld as $n=>$L){
				$L['docEntry']=$docEntry;
				$ins2=a_sql::insert($L,array('table'=>'gvt_inv1','qDo'=>'insert'));
				if($ins2['err']){ $js=_js::e(3,'Error guardando linea: '.$ins2['text'],$jsA); $errs++; break; }
				else{
					if($L['handInv']=='Y'){
						$Liv[]=array('itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'whsId'=>$L['whsId'],'onOrder'=>$L['quantity'],'buyFactor'=>$L['buyFactor']);
					}
				}
			}
			}
		}
		if($errs==0){
			$js=_js::r('Documento guardado correctamente.','"docEntry":"'.$docEntry.'"'); $cmt=true;
			a_sql::transaction($cmt);
			Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$docEntry,'dateC'=>1));
		}
	}
	echo $js;
}
else if(_0s::$router=='GET inv/view'){ //a_ses::hashKey('.basic');
	if($js=_js::ise($___D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	$Mx= Doc::getOne(array('r'=>'Mx','docEntry'=>$___D['docEntry'],'fromA'=>'C.licTradType,C.licTradNum,P.name prsName,P.tel1,A.* 
	FROM gvt_oinv A 
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
	LEFT JOIN par_ocpr P ON (P.prsId=A.prsId)
	',
	'fromB'=>'I.itemCode, I.itemName,I.udm, B.price,B.quantity,B.priceLine FROM gvt_inv1 B 
	LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)'));
	echo _js::enc(docSerie::form($Mx));
}
else if(_0s::$router=='POST inv/receive'){ //pendiente
	if($js=_js::ise($___D['docEntry'],'Se debe definir Id de documento.')){}
	else if(!is_array($___D['L']) || count($___D['L'])==0){ die(_js::e(3,'No se han enviado lineas a guardar.')); }
	else{
		$Ld=$___D['L']; unset($___D['L']);
		$errs=0; $nl=0;
		$_docTotalLine=$_docTotalList=0;
		foreach($Ld as $n => $L){ $nl++;
			$ln='Linea '.$nl.': '; 
			if($js=_js::ise($L['id'],$ln.'Se debe definir el ID de linea a recibir.')){ $errs++; break; }
			else if($js=_js::ise($L['itemId'],$ln.'Se debe definir el ID del artículo.')){ $errs++; break; }
			else if($js=_js::ise($L['itemSzId'],$ln.'Se debe definir el ID de la talla del artículo.')){ $errs++; break; }
			else if($js=_js::ise($L['whsId'],$ln.'Se debe definir la bodega de ingreso.')){ $errs++; break; }
			else if($js=_js::ise($L['price'],$ln.'Se debe definir el precio del artículo.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'Se debe definir la cantidad.','numeric>0')){ $errs++; break; }
			else{
				$Ld[$n]['lineNum']=$nl;
				$Ld[$n]=Doc::lineTotal($L,array('discPf'=>$___D['discPf'],'curr'=>$___D['curr']));
				$_docTotalList+=$Ld[$n]['lineTotalList'];
				$_docTotalLine+=$Ld[$n]['priceLine'];
			}
		}
		$___D['docTotalList']=$_docTotalList;
		$___D['docTotalLine']=$_docTotalLine;
		$___D=Doc::docTotal($___D);
		if($errs==0){ unset($___D['undefined']);
			a_sql::transaction(); $cmt=false;
			$ins=a_sql::insert($___D,array('table'=>'gvt_opor','qDo'=>'insert','kui'=>'uid_dateC'));
			if($ins['err']){ $js=_js::e(3,'Error guardando documento: '.$ins['text']); $errs++; }
			else{ $Liv=array();
			$docEntry=($ins['insertId'])?$ins['insertId']:$___D['docEntry'];
			foreach($Ld as $n=>$L){
				$L['docEntry']=$docEntry;
				$L['openQty']=$L['quantity'];
				$ins2=a_sql::insert($L,array('table'=>'gvt_por1','qDo'=>'insert'));
				if($ins2['err']){ $js=_js::e(3,'Error guardando linea: '.$ins2['text'],$jsA); $errs++; break; }
				else{
					$Liv[]=array('itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'whsId'=>$L['whsId'],'onOrder'=>$L['quantity'],'buyFactor'=>$L['buyFactor']);
				}
			}
			}
		}
		if($errs==0){
			$js=_js::r('Documento guardado correctamente.','"docEntry":"'.$docEntry.'"'); $cmt=true;
			a_sql::transaction($cmt);
			Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$docEntry,'dateC'=>1));
		}
	}
	echo $js;
}

else if(_0s::$router=='PUT inv/statusOpen'){
	if($js=Doc::status(array('docEntry'=>$___D['docEntry'],'serieType'=>$serieType,'verify_open'=>'Y'))){}
	else{
		$q=a_sql::query('SELECT whsId,itemId,itemSzId,quantity onOrder,buyFactor FROM gvt_por1 WHERE docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo lineas del documento.',2=>'El documento no tiene lineas registradas.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			a_sql::transaction();
			$upd=a_sql::query('UPDATE gvt_opor SET docStatus=\'O\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error abriendo documento: '));
			if(a_sql::$err){ die(a_sql::$errNoText); }
			$Liv=array();
			while($L=$q->fetch_assoc()){ $Liv[]=$L; }
			_ADMS::_lb('src/Invt');
			Invt::onOrderCommi_put($Liv,array('isPurc'=>'Y'));
			if(Invt::$err){ $js=Invt::$errText; }
			else{
				$js=_js::r('Documento abierto correctamente.');
			Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'docStatus'=>'O'));
				a_sql::transaction(true);
			}
		}
	}
	echo $js;
}
else if(_0s::$router=='PUT inv/statusCancel'){
	$___D['serieType']=$serieType; 
	a_sql::transaction();
	$js=Doc::statusCancel($___D,array('D'=>'Y'));
	if(!Doc::$ok){}
	else{ $errs=0;
		/*solo reversar onOrder si estaba abierto */
		if(Doc::$D['docStatus']=='O'){
			$q=a_sql::query('SELECT whsId,itemId,itemSzId,quantity onOrder,buyFactor FROM gvt_por1 WHERE docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo lineas del documento.',2=>'El documento no tiene lineas registradas.'));
			if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; }
			$Liv=array(); //negativo para restar
			while($L=$q->fetch_assoc()){ $L['onOrder']*=-1; $Liv[]=$L; }
			_ADMS::_lb('src/Invt'); 
			Invt::onOrderCommi_put($Liv,array('isPurc'=>'Y'));
			if(Invt::$err){ $js=Invt::$errText; $errs++; }
		}
		if($errs==0){
			$js=_js::r('Documento anulado correctamente.');
			a_sql::transaction(true);
		}
	}
	echo $js;
}
?>