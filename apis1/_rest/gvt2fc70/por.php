<?php
$serieType='gvtPor';
Doc::$owh=false;
if(_0s::$router=='GET por'){
	$___D['fromA']='A.docEntry,A.docStatus,A.cardId,A.cardName,A.docType,A.docDate,A.docTotal,A.curr,A.docTotalME,A.userId,A.dateC 
	FROM gvt_opor A';
	echo Doc::get($___D);
}
else if(_0s::$router=='GET por/tb99'){
	echo Doc::tb99($serieType,$___D);
}
else if(_0s::$router=='POST por'){ //a_ses::hashKey('ivt.wht.basic');
	if($js=_js::ise($___D['docDate'],'Se debe definir la fecha.')){}
	else if($js=_js::ise($___D['cardId'],'Se debe definir ID del socio de negocios.','numeric>0')){}
	else if($js=_js::ise($___D['cardName'],'Se debe definir socio de negocios.')){}
	else if(!_js::textLimit($___D['lineMemo'],200)){ $js=_js::e(3,'Los detalles no pueden exceder 200 caracteres.'); }
	else if(!is_array($___D['L']) || count($___D['L'])==0){ die(_js::e(3,'No se han enviado lineas a guardar.')); }
	else{
		$Ld=$___D['L']; unset($___D['L']);
		$errs=0; $nl=0;
		$_docTotalLine=$_docTotalList=0;
		foreach($Ld as $n => $L){ $nl++;
			$ln='Linea '.$nl.': '; 
			if($js=_js::ise($L['itemId'],$ln.'Se debe definir el ID del artículo.')){ $errs++; break; }
			else if($js=_js::ise($L['itemSzId'],$ln.'Se debe definir el ID de la talla del artículo.')){ $errs++; break; }
			else if($js=_js::ise($L['whsId'],$ln.'Se debe definir la bodega de ingreso.')){ $errs++; break; }
			else if($js=_js::ise($L['price'],$ln.'Se debe definir el precio del artículo.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'Se debe definir la cantidad.','numeric>0')){ $errs++; break; }
			else{
				$Ld[$n]['lineNum']=$nl;
				$Ld[$n]=Doc::lineTotal($L,array('discPf'=>$___D['discPf'],'curr'=>$___D['curr']));
				$_docTotalList+=$Ld[$n]['lineTotalList'];
				$_docTotalLine+=$Ld[$n]['priceLine'];
			}
		}
		$___D['docTotalList']=$_docTotalList;
		$___D['docTotalLine']=$_docTotalLine;
		$___D=Doc::docTotal($___D);
		if($errs==0){ unset($___D['undefined']);
			a_sql::transaction(); $cmt=false;
			$ins=a_sql::insert($___D,array('table'=>'gvt_opor','qDo'=>'insert','kui'=>'uid_dateC'));
			if($ins['err']){ $js=_js::e(3,'Error guardando documento: '.$ins['text']); $errs++; }
			else{ $Liv=array();
			$docEntry=($ins['insertId'])?$ins['insertId']:$___D['docEntry'];
			foreach($Ld as $n=>$L){
				$L['docEntry']=$docEntry;
				$L['openQty']=$L['quantity'];
				$ins2=a_sql::insert($L,array('table'=>'gvt_por1','qDo'=>'insert'));
				if($ins2['err']){ $js=_js::e(3,'Error guardando linea: '.$ins2['text'],$jsA); $errs++; break; }
				else{
					$Liv[]=array('itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'whsId'=>$L['whsId'],'onOrder'=>$L['quantity'],'buyFactor'=>$L['buyFactor']);
				}
			}
			}
		}
		if($errs==0){
			$js=_js::r('Documento guardado correctamente.','"docEntry":"'.$docEntry.'"'); $cmt=true;
			a_sql::transaction($cmt);
			Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$docEntry,'dateC'=>1));
		}
	}
	echo $js;
}
else if(_0s::$router=='GET por/view'){ //a_ses::hashKey('.basic');
	if($js=_js::ise($___D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	echo Doc::getOne(array('docEntry'=>$___D['docEntry'],'fromA'=>'C.cardCode, C.licTradType,C.licTradNum,A.* 
	FROM gvt_opor A 
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)',
	'fromB'=>'I.itemCode, I.itemName,I.buyUdm, B.* FROM gvt_por1 B 
	LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)'));
}
else if(_0s::$router=='POST por/receive'){ //pendiente
	if($js=_js::ise($___D['docEntry'],'Se debe definir Id de documento.')){}
	else if(!is_array($___D['L']) || count($___D['L'])==0){ die(_js::e(3,'No se han enviado lineas a guardar.')); }
	else{
		$Ld=$___D['L']; unset($___D['L']);
		$errs=0; $nl=0;
		$_docTotalLine=$_docTotalList=0;
		foreach($Ld as $n => $L){ $nl++;
			$ln='Linea '.$nl.': '; 
			if($js=_js::ise($L['id'],$ln.'Se debe definir el ID de linea a recibir.')){ $errs++; break; }
			else if($js=_js::ise($L['itemId'],$ln.'Se debe definir el ID del artículo.')){ $errs++; break; }
			else if($js=_js::ise($L['itemSzId'],$ln.'Se debe definir el ID de la talla del artículo.')){ $errs++; break; }
			else if($js=_js::ise($L['whsId'],$ln.'Se debe definir la bodega de ingreso.')){ $errs++; break; }
			else if($js=_js::ise($L['price'],$ln.'Se debe definir el precio del artículo.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'Se debe definir la cantidad.','numeric>0')){ $errs++; break; }
			else{
				$Ld[$n]['lineNum']=$nl;
				$Ld[$n]=Doc::lineTotal($L,array('discPf'=>$___D['discPf'],'curr'=>$___D['curr']));
				$_docTotalList+=$Ld[$n]['lineTotalList'];
				$_docTotalLine+=$Ld[$n]['priceLine'];
			}
		}
		$___D['docTotalList']=$_docTotalList;
		$___D['docTotalLine']=$_docTotalLine;
		$___D=Doc::docTotal($___D);
		if($errs==0){ unset($___D['undefined']);
			a_sql::transaction(); $cmt=false;
			$ins=a_sql::insert($___D,array('table'=>'gvt_opor','qDo'=>'insert','kui'=>'uid_dateC'));
			if($ins['err']){ $js=_js::e(3,'Error guardando documento: '.$ins['text']); $errs++; }
			else{ $Liv=array();
			$docEntry=($ins['insertId'])?$ins['insertId']:$___D['docEntry'];
			foreach($Ld as $n=>$L){
				$L['docEntry']=$docEntry;
				$L['openQty']=$L['quantity'];
				$ins2=a_sql::insert($L,array('table'=>'gvt_por1','qDo'=>'insert'));
				if($ins2['err']){ $js=_js::e(3,'Error guardando linea: '.$ins2['text'],$jsA); $errs++; break; }
				else{
					$Liv[]=array('itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'whsId'=>$L['whsId'],'onOrder'=>$L['quantity'],'buyFactor'=>$L['buyFactor']);
				}
			}
			}
		}
		if($errs==0){
			$js=_js::r('Documento guardado correctamente.','"docEntry":"'.$docEntry.'"'); $cmt=true;
			a_sql::transaction($cmt);
			Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$docEntry,'dateC'=>1));
		}
	}
	echo $js;
}

else if(_0s::$router=='PUT por/statusOpen'){
	if($js=Doc::status(array('docEntry'=>$___D['docEntry'],'serieType'=>$serieType,'verify_open'=>'Y'))){}
	else{
		$q=a_sql::query('SELECT whsId,itemId,itemSzId,quantity onOrder,buyFactor FROM gvt_por1 WHERE docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo lineas del documento.',2=>'El documento no tiene lineas registradas.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			a_sql::transaction();
			$upd=a_sql::query('UPDATE gvt_opor SET docStatus=\'O\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error abriendo documento: '));
			if(a_sql::$err){ die(a_sql::$errNoText); }
			$Liv=array();
			while($L=$q->fetch_assoc()){ $Liv[]=$L; }
			_ADMS::_lb('src/Invt');
			Invt::onOrderCommi_put($Liv,array('isPurc'=>'Y'));
			if(Invt::$err){ $js=Invt::$errText; }
			else{
				$js=_js::r('Documento abierto correctamente.');
			Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'docStatus'=>'O'));
				a_sql::transaction(true);
			}
		}
	}
	echo $js;
}
else if(_0s::$router=='PUT por/statusCancel'){
	$___D['serieType']=$serieType; 
	a_sql::transaction();
	$js=Doc::statusCancel($___D,array('D'=>'Y'));
	if(!Doc::$ok){}
	else{ $errs=0;
		/*solo reversar onOrder si estaba abierto */
		if(Doc::$D['docStatus']=='O'){
			$q=a_sql::query('SELECT whsId,itemId,itemSzId,quantity onOrder,buyFactor FROM gvt_por1 WHERE docEntry=\''.$___D['docEntry'].'\' ',array(1=>'Error obteniendo lineas del documento.',2=>'El documento no tiene lineas registradas.'));
			if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; }
			$Liv=array(); //negativo para restar
			while($L=$q->fetch_assoc()){ $L['onOrder']*=-1; $Liv[]=$L; }
			_ADMS::_lb('src/Invt'); 
			Invt::onOrderCommi_put($Liv,array('isPurc'=>'Y'));
			if(Invt::$err){ $js=Invt::$errText; $errs++; }
		}
		if($errs==0){
			$js=_js::r('Documento anulado correctamente.');
			a_sql::transaction(true);
		}
	}
	echo $js;
}
?>