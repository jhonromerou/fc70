<?php
//if(a_ses::$userId!=1){ die(_js::e(3,'Temporalmente fuera de servicio.')); }
//$R = _fread_tab::get($_FILES['file'],array('lineIni'=>3,'lineEnd'=>1000,'K'=>array('docEntry','slpId','docType','ref1','whsId','cardId','cardName','docDate','dueDate','addrMerch','addrInv','discDef','lineMemo','lineNum','itemId','itemSzId','price','quantity','priceList','disc','discSum','priceLine','lineTotal','lineTotalList')));
$Liv=array();
if($R['errNo']){ $js = _js::e($R); }
else{
	a_sql::transaction(); $comit=false;
	$grTypeId=3;
	$Doc=array();
	$uniK=array();
	$Kc=array();//clientes
	$Kitem=array();//Items
	$kU=false;
	$K1=array();//doc-line
	$K2=array();//doc-item
	$linea=2;
	foreach($_J['L'] as $ln => $Da){ $linea++;
		$lnt = 'Linea '.$linea.': '; $a = ' Actual: ';
		$lineTotal++; $kd=$Da['docEntry'];
		if(!array_key_exists($kd,$Doc)){
		$Doc[$kd]=array('docEntry'=>$Da['docEntry'],
		'docStatus'=>'O','dateC'=>date('Y-m-d H:i:s'),'userId'=>a_ses::$userId,
		'slpId'=>$Da['slpId'],'docType'=>$Da['docType'],'ref1'=>$Da['ref1'],'whsId'=>$Da['whsId'],
		'cardId'=>$Da['cardId'],'cardName'=>$Da['cardName'],'docDate'=>$Da['docDate'],'dueDate'=>$Da['dueDate'],
		'lineMemo'=>$Da['lineMemo'],
		'cityCode'=>$Da['cityCode'],'countyCode'=>$Da['countyCode'],'addrMerch'=>$Da['addrMerch']
		,'discDef'=>$Da['discDef'],'docTotalLine'=>0,'baseAmnt'=>0,'discTotal'=>0,'discSum'=>0,
		'docTotal'=>0,'L'=>array());
		}
		$itemCode=$Da['itemId'];
		$k1=$Da['docEntry'].$Da['lineNum'];;
		$k2=$Da['docEntry'].$Da['itemId'].$Da['itemSzId'];
		//calculos de lineas
		$Da['discSum']=($Da['priceList']-$Da['price'])*$Da['quantity'];
		$Da['priceLine']=$Da['price']*$Da['quantity'];
		$Da['lineTotal']=$Da['price']*$Da['quantity'];
		$Da['lineTotalList']=$Da['priceList']*$Da['quantity'];
		$Da['disc']= round(($Da['discSum']/$Da['lineTotalList'])*100,2);
		if(array_key_exists($k1,$K1)){ $js=_js::e(3,$lnt.'Linea de documento repetida.'); $errs++; break; }
		else if(array_key_exists($k2,$K2)){ $js=_js::e(3,$lnt.'Un documento no puede tener el mismo articulo 2 veces.'); $errs++; break; }
		else if($js=_js::ise($Da['docEntry'],$lnt.'Se debe definir el Id del nuevo documento.','numeric>0')){ $errs++; break; }
		else if($js=_js::ise($Da['slpId'],$lnt.'Se debe definir el Id del responsable de ventas.','numeric>0')){ $errs++; break; }
		else if($js=_js::ise($Da['docType'],$lnt.'Se debe definir el tipo de documento.')){ $errs++; break; }
		else if($js=_js::ise($Da['whsId'],$lnt.'Se debe definir el ID de la bodega','numeric>0')){ $errs++; break; }
		else if($js=_js::ise($Da['cardId'],$lnt.'Se debe definir el código del cliente')){ $errs++; break; }
		else if($js=_js::ise($Da['cardName'],$lnt.'Se debe definir el nombre del cliente')){ $errs++; break; }
		else if($js=_js::ise($Da['docDate'],$lnt.'Se debe definir la fecha del documento.')){ $errs++; break; }
		else if($js=_js::ise($Da['dueDate'],$lnt.'Se debe definir la fecha de vencimiento del documento.')){ $errs++; break; }
		else if($js=_js::ise($Da['cityCode'],$lnt.'Se debe definir codigo municipio.')){ $errs++; break; }
		else if($js=_js::ise($Da['countyCode'],$lnt.'Se debe definir codigo departamento.')){ $errs++; break; }
		else if($js=_js::ise($Da['addrMerch'],$lnt.'Se debe definir la dirección de entrega.')){ $errs++; break; }
		else if($js=_js::ise($Da['lineNum'],$lnt.'Se debe definir el número de linea.','numeric>0')){ $errs++; break; }
		else if($js=_js::ise($Da['itemId'],$lnt.'Se debe definir el Id del articulo.')){ $errs++; break; }
		else if($js=_js::ise($Da['itemSzId'],$lnt.'Se debe definir el Id de la talla.','numeric>0')){ $errs++; break; }
		else if($js=_js::ise($Da['price'],$lnt.'Se debe definir el precio unitario.','numeric>0')){ $errs++; break; }
		else if($js=_js::ise($Da['quantity'],$lnt.'Se debe definir la cantidad.','numeric>0')){ $errs++; break; }
		else if($js=_js::ise($Da['priceList'],$lnt.'Se debe definir el precio de lista ('.$Da['priceList'].').','numeric>0')){ $errs++; break; }
		else if($js=_js::ise($Da['priceLine'],$lnt.'Se debe definir el precio de la linea.','numeric>0')){ $errs++; break; }
		else if($js=_js::ise($Da['lineTotal'],$lnt.'Se debe definir el precio total de la linea.','numeric>0')){ $errs++; break; }
		else if($js=_js::ise($Da['lineTotalList'],$lnt.'Se debe definir el precio total de la linea en precio de lista.','numeric>0')){ $errs++; break; }
		else{
			$K1[$k1]=$k1;
			$K2[$k2]=$k2; $kc=$Da['cardId'];
			if(!array_key_exists($kc,$Kc)){
				$Kc[$kc]=a_sql::fetch('SELECT cardId,cardName FROM par_ocrd WHERE cardCode=\''.$Da['cardId'].'\' LIMIT 1 ',array(1=>$lnt.'Error obteniendo información de socio de negocios',2=>$lnt.'El socio de negocios no existe.'));
				if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
			}
			$Doc[$kd]['cardId']=$Kc[$kc]['cardId'];
			$Doc[$kd]['cardName']=$Kc[$kc]['cardName'];
			if(!array_key_exists($itemCode,$Kitem)){
				$Kitem[$itemCode]=a_sql::fetch('SELECT itemId,sellItem FROM itm_oitm WHERE itemCode=\''.$itemCode.'\' LIMIT 1 ',array(1=>$lnt.'Error obteniendo información de articulo',2=>$lnt.'El articulo no existe.'));
				if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
				if($Kitem[$itemCode]['sellItem']=='Ns'){ $errs++; $js=_js::e(3,$lnt.'El articulo no está marcado como de venta.'); break; }
			}
			$ex=$Kitem[$itemCode];
			$kU=$Da['docEntry'].'-'.$ex['itemId'].'-'.$Da['itemSzId'];
			/* articulos con mismo codigo evitar */
			if($kU && array_key_exists($kU,$uniK)){ $js=_js::e(3,$lnt.' Indice Repetido. Ya definido en linea '.$uniK[$kU].' ('.$kU.' || '.$kUA.').'); $errs++; break; }
			$kUA=$kU;
			$uniK[$kU]=$ln;
			$Doc[$kd]['docTotalLine']+=$Da['priceLine'];
			$Doc[$kd]['docTotalList']+=$Da['lineTotalList'];
			$Doc[$kd]['discSum']+=$Da['discSum'];
			$Doc[$kd]['L'][]=array('docEntry'=>$Da['docEntry'],'lineNum'=>$Da['lineNum'],'itemId'=>$ex['itemId'],'itemSzId'=>$Da['itemSzId'],'price'=>$Da['price'],'quantity'=>$Da['quantity'],'openQty'=>$Da['quantity'],'priceList'=>$Da['priceList'],'disc'=>$Da['disc'],'discSum'=>$Da['discSum'],'priceLine'=>$Da['priceLine'],'lineTotal'=>$Da['lineTotal'],'lineTotalList'=>$Da['lineTotalList'],
			'ku'=>'Ln-'.$ln.': '.$Da['docEntry'].'-'.$ex['itemId'].'-'.$Da['itemSzId']);;
			$kx=$Da['whsId'];
			$kx2=$ex['itemId'].$Da['itemSzId'];
				if(!array_key_exists($kx,$Liv)){ $Liv[$kx]=array(); }
				if(!array_key_exists($kx2,$Liv[$kx])){ $Liv[$kx][$kx2]=array('itemId'=>$ex['itemId'],'itemSzId'=>$Da['itemSzId'],'isCommited'=>0); }
				$Liv[$kx][$kx2]['isCommited'] +=$Da['quantity'];
		}
	}
	//if($errs==0){ die(_js::r('Validacion correcta')); } die($js);
	$nums=0; $nn=0;
	$LV=array();
	if($errs==0){
		foreach($Doc as $docEntry => $Dx){
			if($nums==0){ $n1=$docEntry; }
			$n2=$docEntry;
			$Dx['docEntry']=$docEntry;
			$L=$Dx['L']; unset($Dx['L']);
			$Dx['discTotal']=$Dx['discSum']/$Dx['docTotalList']*100;
			$Dx['docTotal']=$Dx['docTotalLine'];
			$Dx['baseAmnt']=$Dx['docTotalLine'];
			unset($Dx['docTotalList']);
			$ins=a_sql::insert($Dx,array('table'=>'gvt_opvt','qDo'=>'insert'));
			if($ins['err']){ $js=_js::e(3,'Error generando encabezado de documento: '.$ins['text']); $errs++; break; }
			else{
				foreach($L as $nl=>$Lx){
					$ku=$Lx['ku']; unset($Lx['ku']);
					$ins2=a_sql::insert($Lx,array('table'=>'gvt_pvt1','qDo'=>'insert'));
					if($ins2['err']){ $js=_js::e(3,'Error generando lineas de documento: '.$ins2['text'].' (Rows to: '.$ku.')'); $errs++; break 2; }
					$nn++;
				}
			}
			$nums++;
		}
	}
	if($errs==0){ /* marcar como recibidos */
		_ADMS::_app('Invt');
		foreach($Liv as $whsId => $Li){
			Invt::onOrderCommi_put($Li,array('whsId'=>$whsId));
			if(Invt::$err){ $js=Invt::$errText; $errs++; break; }
		}
	}
	if($errs==0){ a_sql::transaction(true); $js=_js::r('Se registraron '.$nums.' documentos.'); }
	}
echo $js;
?>