<?php
$serieType='opvt';
//if en otros
$serieType='opvt';

function itwOnOrder($whsId=0){
	$q=a_sql::query('SELECT W.itemId,W.itemSzId,SUM(W.onHand) onHand
	FROM pep_oitw W
	JOIN wma_owfa WFA ON (WFA.wfaId=W.wfaId)
	LEFT JOIN itm_oitm I ON (I.itemId=W.itemId)
	WHERE WFA.wfaClass=\'N\' 
	GROUP BY W.itemId,W.itemSzId
	');
	while($L=$q->fetch_assoc()){
		$x=a_sql::query('UPDATE ivt_oitw SET onOrder=\''.$L['onHand'].'\' WHERE itemId=\''.$L['itemId'].'\' AND itemSzId=\''.$L['itemSzId'].'\' AND whsId=\''.$whsId.'\' LIMIT 1');
	}
}

if(_0s::$router=='GET pvt'){ a_ses::hashKey('sellOrd.basic');
	_ADMS::lib('iDoc');
	if(!_js::ise($_GET['ref1'])){ $_GET['A.ref1(E_igual)']=$_GET['ref1']; }
	unset($_GET['ref1']);
	$_GET['fromA']='A.docEntry,A.docStatus,A.cardId,A.cardName,A.docType,A.docDate,A.dueDate,A.docTotal,A.curr,A.docTotalME,A.slpId,A.userId,A.ref1,A.whsId,A.dateC,A.dlvStatus,A.cartStatus
	FROM gvt_opvt A';
	echo iDoc::get($_GET,array('permsBy'=>'slps'));
}
else if(_0s::$router=='PUT pvt/form'){ a_ses::hashKey('sellOrd.edit');
	_ADMS::_lb('com/_2d');
	_ADMS::lib('iDoc');
	$docEntry=$c['docEntry'];
	$noEsLib=!($___D['docType']=='LB');
	$editab=!_js::ise($___D['docEntry'],'','numeric>0');
	//if(!$editab){ die(_js::e(3,'Creación de pedidos deshabilitado temporalmente, intente más tarde.')); }
	$isSave=(!_js::ise($docEntry,''));
	if(! $isSave && ! $noEsLib) {
		$___D['cartStatus'] = 'O';
	}
	$daysDifTod=_2d::relTime(date('Y-m-d'),$___D['docDate']);
	$daysDif=_2d::relTime($___D['docDate'],$___D['dueDate']);
	if($editab && iDoc::vStatus(array('tbk'=>'gvt_opvt','docEntry'=>$___D['docEntry'],'D'=>'Y'))){ die(_err::$errText); };
	if($js!=''){ die(substr($js,0,-1).',"docEntry":"'.$docEntry.'"}'); }
	if($editab && !(iDoc::$D['docStatus']=='D' || iDoc::$D['docStatus']=='S')){ die(_js::e(3,'El documento debe estar en borrador para poder modificarlo.')); }
	if($js=_js::ise($___D['docType'],'El tipo de documento debe estar definido.')){ die($js); }
	else if($js=_js::ise($___D['docDate'],'La fecha del documento debe estar definida.','Y-m-d')){ die($js);}
	else if($js=_js::ise($___D['dueDate'],'La fecha de entrega debe estar definida.','Y-m-d')){ die($js);}
	//else if(!$isSave && $daysDifTod<0){ die(_js::e(3,'La fecha del documento no puede ser anterior a la fecha actual.')); }
	else if($daysDif<0){ die(_js::e(3,'La fecha de entrega no puede ser menor a la fecha del documento.')); }
	//if($noEsLib && _0s::jSocVerif('>',$daysDif,'gvtPvt_minDaysToDeliv')){ die(_js::e(3,'La fecha de entrega no puede ser menor a '._0s::$jSoc['gvtPvt_minDaysToDeliv'].' dias.')); }
	else if($noEsLib && $js=_js::ise($___D['ref1'],'Se debe definir la orden de compra.')){}
	else if($noEsLib && !_js::textLimit($___D['ref1'],20)){ $js= _js::e(3,'La longuitud del texto de la orden de compra no puede exceder 20 caracteres.'); }
	else if($noEsLib && $js=_js::ise($___D['cardId'],'Se debe definir un contacto.')){ die($js); }
	else if($js=_js::ise($___D['cardName'],'Se debe definir un contacto.')){ die($js); }
	else if($js=_js::ise($___D['slpId'],'Seleccione un empleado de Ventas.')){ die($js); }
	else if($js=_js::ise($___D['countyCode'],'Seleccione un departamento.')){ die($js); }
	else if($js=_js::ise($___D['cityCode'],'Seleccione una ciudad.')){ die($js); }
	else if(!_js::textLimit($___D['addrMerch'])){ $js= _js::e(3,'La dirección de mercancía no puede exceder los 255 caracteres.'); }
	//else if(!_js::textLimit($___D['addrInv'])){ $js= _js::e(3,'La dirección de facturación no puede exceder los 255 caracteres.'); }
	else if(!_js::textLimit($___D['lineMemo'],500)){ $js= _js::e(3,'Los detalles no pueden exceder los 500 caracteres.'); }
	else if($js=_js::ise($___D['curr'],'Defina la moneda del documento.')){}
	else if($___D['discPf']>100){ $js=_js::e(3,'El descuento no puede ser mayor al 100%'); }
	else if(!is_array($___D['L'])){ $js=_js::e(3,'No se han enviado lineas para el documento.'); }
	else{
		if($noEsLib){
			_0s::jSocGet('gvtPvt_OCUnique');
			if(_0s::$jSoc['gvtPvt_OCUnique']=='Y'){
				$q=a_sql::fetch('SELECT docEntry FROM gvt_opvt WHERE docEntry!=\''.$___D['docEntry'].'\' AND canceled=\'N\' AND cardId=\''.$___D['cardId'].'\' AND ref1=\''.$___D['ref1'].'\' LIMIT 1',array(1=>'Error verificando existencia de orden de compra.'));
				if(a_sql::$err){ die(a_sql::$errNoText); }
				else if(a_sql::$errNo==-1){ die(_js::e(3,'La orden de compra ya está registrada en el documento #'.$q['docEntry'].'.')); }
			}
		};
		unset($___D['textSearch']);
		$curDif=($___D['curr']!=_0s::$currDefault);
		$___D['rate']=3000;
		$Ld=$___D['L']; unset($___D['L']);
		$errs=0; $Di=array();
		$lineTotal=0; $totalPriceList=0; $nl=0;
		$_docTotalLine=$_docTotalList=0;
		_ADMS::_lb('itm'); $n=0; $Isave=array();
		if(is_array($Ld)) foreach($Ld as $kn=>$L){ $n++;
			$totaln=0; $ki=$L['itemId'].$L['itemSzId'];
			$ln ='Linea '.$n.': '; $priceList=0;$itemId_='';
			if(array_key_exists($ki,$Isave)){ $js=_js::e($ln,'Ya existe una linea con este mismo artículo, Linea: '.$Isave[$ki]); }
			else if($js=_js::ise($L['itemId'],$ln.'No se ha encontrado el ID del artículo.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'Defina una cantidad mayor a 0.','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['price'],$ln.'Se debe definir el precio','numeric>0')){ $errs++; break; }
			else{
				$L['lineNum']=$n; $L['openQty']=$L['quantity'];
				$L[0]='i'; $L[1]='gvt_pvt1'; $L['_unik']='id';
				if($L['delete']){ $L['delete']='Y'; $Di[$nl]=$L; }
				else{
					$Di[$nl]=Doc::lineTotal($L,array('discPf'=>$___D['discPf'],'curr'=>$___D['curr']));
					$_docTotalList+=$Di[$nl]['lineTotalList'];
					$_docTotalLine+=$Di[$nl]['priceLine'];
				}
				$nl++;
			}
			$Isave[$ki]=$n;
		}
		$___D['docTotalList']=$_docTotalList; unset($Ld);
		$___D['docTotalLine']=$_docTotalLine;
		$___D=Doc::docTotal($___D);
			if($errs==0){
			$_br=(_V::_g('pvt_maxDisc_BR')+60);
			$_po=(_V::_g('pvt_maxDisc_PO')+60);
			if($___D['docType']=='BR' && $_br && $___D['discTotal']>$_br){ $js=_js::e(3,'El descuento para los productos de Baja Rotación no puede ser mayor a '.$_br.'%, Desc. Total: '.$___D['discTotal'].'%.'); $errs++; }
			else if($___D['docType']=='PO' && $_po && $___D['discTotal']>$_po){ $js=_js::e(3,'El descuento para los productos Obsoletos no puede ser mayor a '.$_po.'%, Desc. Total: '.$___D['discTotal'].'%.'); $errs++; }
			else if($___D['docType']!='BR' && $___D['docType']!='PO'){
				$kx='gvtPvt_maxDiscDef'; _0s::jSocGet($kx);
				$discMax=(_0s::$jSoc[$kx]=='Y' && $___D['discTotal']>$___D['discDef']);
				if($discMax){
					$js=_js::e(3,'El descuento total '.$___D['discTotal'].', no puede ser mayor al descuento autorizado para el cliente: '.$___D['discDef'].'.'); $errs++;
				}
			}
		}
		a_sql::transaction(); $cmt=false;
		if($errs==0){
		$ins=a_sql::uniRow($___D,array('tbk'=>'gvt_opvt','qk'=>'ud','wh_change'=>'docEntry=\''.$___D['docEntry'].'\' LIMIT 1'));
		if(a_sql::$err){ $js=_js::e(1,'Error guardando documento: '.a_sql::$errText); }
		else{
			$docEntry=($ins['insertId'])?$ins['insertId']:$___D['docEntry'];
			$addJs='"docEntry":"'.$docEntry.'"';
			if($ins['insertId']){
				iDoc::logPost(array('tbk'=>'gvt_doc99','serieType'=>'opvt','docEntry'=>$docEntry,'dateC'=>1));
			}
			a_sql::multiQuery($Di,0,array('docEntry'=>$docEntry));
			if(_err::$err){ $js=_err::$errText; $errs=1; }
			if($errs==0){ $cmt=true;
				$js=_js::r('Información guardada correctamente.',$addJs);
			}
		}
		}
		a_sql::transaction($cmt);
	}
	echo $js;
}
else if(_0s::$router=='GET pvt/form'){ a_ses::hashKey('sellOrd.basic');
	if($js=_js::ise($___D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	$q=a_sql::fetch('SELECT * FROM gvt_opvt WHERE docEntry=\''.$___D['docEntry'].'\'  LIMIT 1',array(1=>'Error obteniendo documento: ',2=>'No se encontró el documento '.$___D['docEntry'].'.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else if($js=a_ses::nis_slp($q['slpId'])){ die($js); }
	else{ $Mx=$q; $Mx['L']=Doc::lineGet(array('fromB'=>'I.itemCode,I.itemName,I.sellUdm,B.* FROM gvt_pvt1 B LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)','docEntry'=>$___D['docEntry'])); }
	echo _js::enc($Mx,'NO_PAGER');
}
else if(_0s::$router=='GET pvt/view'){ a_ses::hashKey('sellOrd.basic');
	_ADMS::lib('iDoc');
	$pedido = iDoc::getOne(array('r'=>'Mx',
		'docEntry'=>$_GET['docEntry'],'fromA'=>'C.cardCode, C.licTradType,C.licTradNum,C.phone1,C.invDayClose,A.*
	FROM gvt_opvt A
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)',
	'fromB'=>'I.itemCode, I.itemName,I.sellUdm, B.* FROM gvt_pvt1 B LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)'));

	if($js=a_ses::nis_slp($pedido['slpId'])){ die($js); }
	echo _js::enc($pedido,'NO_PAGER');;
}
else if(_0s::$router=='GET pvt/info'){ a_ses::hashKey('sellOrd.basic');
	_ADMS::lib('iDoc');
	echo iDoc::getOne(array('docEntry'=>$___D['docEntry'],'fromA'=>'A.* FROM gvt_opvt A'));
}
else if(_0s::$router=='GET pvt/openQty'){ a_ses::hashKey('sellOrd.basic');
	_ADMS::lib('iDoc');
	echo iDoc::getOne(array('docEntry'=>$_GET['docEntry'],'fromA'=>'ocrd.cardCode, ocrd.licTradType,ocrd.licTradNum,A.docEntry,A.cardId,A.cardName,A.docStatus,A.docType,A.userName,A.docDate,A.dueDate,A.ref1,A.slpId,A.curr,A.rate,A.docTotal,A.docTotalLine,A.docTotalME,A.discPf,A.discDef,A.addrMerch,A.addrInv,A.cityCode,A.countyCode,A.lineMemo
	FROM gvt_opvt A
	LEFT JOIN par_ocrd ocrd ON (ocrd.cardId=A.cardId)','fromB'=>'I.itemCode, I.itemName,I.sellUdm,B.lineNum,B.itemId,B.itemSzId,B.quantity,B.openQty,B.price,B.priceME,B.disc,B.priceList,B.cost FROM gvt_pvt1 B LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)'));
}
else if(_0s::$router=='GET pvt/tb99'){ a_ses::hashKey('sellOrd.basic');
	_ADMS::lib('iDoc');
	echo iDoc::getOne(array('fromA'=>'A.docDate FROM gvt_opvt A','fromB'=>'* FROM gvt_doc99 B','whB'=>'AND B.serieType=\''.$serieType.'\'','docEntry'=>$_GET['docEntry']));
}
/* status */
else if(_0s::$router==('PUT pvt/statusSend')){ a_ses::hashKey('sellOrd.edit');
	_ADMS::lib('iDoc');
	if(iDoc::vStatus(array('tbk'=>'gvt_opvt','docEntry'=>$___D['docEntry'],'D'=>'Y'))){ $js=_err::$errText; }
	else if(iDoc::$D['docStatus']!='D'){ $js=_js::e(3,'Solo un pedido en estado borrador puede ser enviado.'); }
	else{
		if(_0s::jSocVerif('=','Y','gvtPvt_requireOCAttachment')){
			$q=a_sql::fetch('SELECT fileId FROM gtd_ffc1 WHERE tt=\''.$serieType.'\' AND tr=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo relación de archivos del documento',2=>'No se ha relacionado la orden de compra al pedido.'));
			if(a_sql::$err){ die(a_sql::$errNoText); }
		}
		$upd=a_sql::query('UPDATE gvt_opvt SET docStatus=\'S\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array('Error actualizando a enviado: '));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $js=_js::r('Estado actualizado correctamente.');
			iDoc::logPost(array('tbk'=>'gvt_doc99','serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'docStatus'=>'S'));
		}
	}
	echo $js;
}
else if(_0s::$router=='PUT pvt/statusOpen'){
	_ADMS::lib('iDoc');
	a_sql::transaction(); $cmt=false;
	if($js=_js::ise($___D['whsId'],'Se debe definir la bodega.','numeric>0')){}
	else if(iDoc::vStatus(array('tbk'=>'gvt_opvt','docEntry'=>$___D['docEntry'],'fie'=>'whsId,canceled','D'=>'Y'))){ echo (_err::$errText); }
	else if(iDoc::$D['docStatus']!='S'){ die(_js::e(3,'Solo un documento enviado puede ser recibido.')); }
	else{
		$errs=0;
		$upd=a_sql::query('UPDATE gvt_opvt SET docStatus=\'O\',whsId=\''.$___D['whsId'].'\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error actualizando estado del documento: '));
		if(a_sql::$err){ $js=a_sql::$errNoText; $errs=1; }
		else{
			_ADMS::mApps('ivt/Ivt');
			Ivt::putFromDoc(array('tbk'=>'gvt_opvt','tbk1'=>'gvt_pvt1','docEntry'=>$___D['docEntry'],'isCommited'=>'+','whsId'=>$___D['whsId'],'whGet'=>'  '),array('handInv'=>'Y','numFactor'=>1));
			if(_err::$err){ $js=_err::$errText; $errs=1; }
		}
		if($errs==0){ $js=_js::r('Documento abierto correctamente.'); $cmt=true;
			iDoc::logPost(array('tbk'=>'gvt_doc99','serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'docStatus'=>'O'));
		}
		a_sql::transaction($cmt);
	}
	echo $js;
}
else if(_0s::$router=='PUT pvt/statusCancel'){ a_ses::hashKey('sellOrd.status.cancel');
	_ADMS::lib('iDoc');
	if($js=_js::ise($___D['lineMemo'],'Debe definir el motivo de anulación.')){}
	else if(!_js::textLimit($___D['lineMemo'],100)){ $js=_js::e(3,'Los detalles no puede exceder los 100 caracteres.'); }
	else if(iDoc::vStatus(array('tbk'=>'gvt_opvt','docEntry'=>$___D['docEntry'],'fie'=>'whsId,canceled','D'=>'Y'))){ die(_err::$errText); }
	else{
		$q=a_sql::fetch('SELECT D.docStatus dlvStatus FROM gvt_odlv D WHERE D.canceled=\'N\' AND D.tt=\''.$serieType.'\' AND D.tr=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo información de entregas relacionadas.'));
		if(a_sql::$errNo==1){ $js=a_sql::$errNoText; }
		else if($q['dlvStatus']!=''){
			$js=_js::e(3,'El documento tiene entregas registradas, no se puede modificar. Revise las entregas e intente de nuevo.');
		}
		else{
			a_sql::transaction(); $cmt=false; $errs=0;
			$upd=a_sql::query('UPDATE gvt_opvt SET docStatus=\'N\', canceled=\'Y\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error actualizando estado del documento: '));
			if(a_sql::$errNo==1){ $js=a_sql::$errNoText; }
			else if(iDoc::$D['docStatus']=='D' || iDoc::$D['docStatus']=='S'){ $cmt=true; }
			else if(iDoc::$D['docStatus']=='O' && iDoc::$D['whsId']==0){ $js=_js::e(3,'No hay bodega definida para el pedido.'); }
			else if(iDoc::$D['docStatus']=='O' ){
				_ADMS::_app('Ivt');
				Ivt::putFromDoc(array('tbk'=>'gvt_opvt','tbk1'=>'gvt_pvt1','docEntry'=>$___D['docEntry'],'isCommited'=>'-','whsId'=>iDoc::$D['whsId'],'whGet'=>'  '),array('handInv'=>'Y','numFactor'=>1));
				if(_err::$err){ $js=_err::$errText; $errs=1; }
			}
			if($errs==0){ $js=_js::r('Documento anulado correctamente.'); $cmt=true;
				iDoc::logPost(array('tbk'=>'gvt_doc99','serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'docStatus'=>'N','lineMemo'=>$___D['lineMemo']));
			}
			a_sql::transaction($cmt);
		}
	}
	echo $js;
}
else if(_0s::$router=='PUT pvt/statusClose'){ a_ses::hashKey('sellOrd.status.handClose');
	_ADMS::lib('iDoc');
	if($js=_js::ise($___D['lineMemo'],'Debe definir el motivo de cierre.')){}
	else if(!_js::textLimit($___D['lineMemo'],100)){ $js=_js::e(3,'Los detalles no puede exceder los 100 caracteres.'); }
	else if(iDoc::vStatus(array('tbk'=>'gvt_opvt','docEntry'=>$___D['docEntry'],'fie'=>'whsId','D'=>'Y'))){ die(_err::$errText); }
	else if(iDoc::$D['docStatus']!='O'){ $js=_js::e(3,'Solo un pedido en estado abierto, puede ser cerrado'); }
	else{
		a_sql::transaction(); $cmt=false; $errs=0;
		$upd=a_sql::query('UPDATE gvt_opvt SET docStatus=\'C\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error actualizando estado del documento: '));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			if(iDoc::$D['docStatus']=='O'){
				_ADMS::mApps('ivt/Ivt');
					Ivt::putFromDoc(array('qtyFie'=>'openQty','tbk'=>'gvt_opvt','tbk1'=>'gvt_pvt1','docEntry'=>$___D['docEntry'],'isCommited'=>'-','whsId'=>iDoc::$D['whsId'],
					'whGet'=>' AND B.openQty>0 '),
					array('handInv'=>'Y','numFactor'=>1));
				if(_err::$err){ $js=_err::$errText; $errs=1; }
			}
		}
		if($errs==0){ $js=_js::r('Documento anulado correctamente.'); $cmt=true;
				iDoc::logPost(array('tbk'=>'gvt_doc99','serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'docStatus'=>'C'));
		}
		a_sql::transaction($cmt);
	}
	echo $js;
}
else if(_0s::$router=='PUT pvt/statusDraw'){ a_ses::hashKey('sellOrd.status.openToDraw');
	_ADMS::lib('iDoc');
	if($js=_js::ise($___D['lineMemo'],'Debe definir el motivo.')){}
	else if(!_js::textLimit($___D['lineMemo'],100)){ $js=_js::e(3,'Los detalles no puede exceder los 100 caracteres.'); }
	else if(iDoc::vStatus(array('tbk'=>'gvt_opvt','docEntry'=>$___D['docEntry'],'fie'=>'whsId','D'=>'Y'))){ die(_err::$errText); }
	else if(iDoc::$D['docStatus']!='O'){ $js=_js::e(3,'Solo un documento en estado abierto, puede definirse como borrador.'); }
	else{
		$q=a_sql::fetch('SELECT D.docStatus dlvStatus FROM gvt_odlv D WHERE D.canceled=\'N\' AND D.tt=\''.$serieType.'\' AND D.tr=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo información de entregas relacionadas.'));
		if(a_sql::$errNo==1){ $js=a_sql::$errNoText; }
		else if($q['dlvStatus']!=''){
			$js=_js::e(3,'El documento tiene entregas registradas, no se puede modificar. Revise las entregas e intente de nuevo.');
		}
		else{
			$status='D';
			a_sql::transaction(); $cmt=false; $errs=0;
			$upd=a_sql::query('UPDATE gvt_opvt SET docStatus=\''.$status.'\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error actualizando estado del documento: '));
			if(a_sql::$err){ $js=a_sql::$errNoText; }
			else{
				if(iDoc::$D['docStatus']=='O'){
					_ADMS::_app('Ivt');
						Ivt::putFromDoc(array('tbk'=>'gvt_opvt','tbk1'=>'gvt_pvt1','docEntry'=>$___D['docEntry'],'isCommited'=>'-','whsId'=>iDoc::$D['whsId'],'whGet'=>'  '),array('handInv'=>'Y','numFactor'=>1));
					if(_err::$err){ $js=_err::$errText; $errs=1; }
				}
				if($errs==0){ $js=_js::r('Documento en borrador correctamente.'); $cmt=true;
						iDoc::logPost(array('tbk'=>'gvt_doc99','serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'docStatus'=>'C'));
				}
			}
			a_sql::transaction($cmt);
		}
	}
	echo $js;
}
else if(_0s::$router==('PUT pvt/cartStatus')){ a_ses::hashKey('sellOrd.status.cartStatus');
	if($js=Doc::status(array('closeOmit'=>'Y' , 'serieType'=>$serieType,'docEntry'=>$___D['docEntry']))){ die($js);}
	else if($js=_js::ise($___D['fieK'],'Debe definir el estado.')){}
	else if($js=_js::ise($___D['lineMemo'],'Especifique detalles')){}
	else if(!_js::textLimit($___D['lineMemo'],100)){ $js=_js::e(3,'Los detalles no puede exceder los 100 caracteres.'); }
	else{
		$upd=a_sql::query('UPDATE gvt_opvt SET cartStatus=\''.$___D['fieK'].'\' WHERE docEntry=\''.$___D['docEntry'].'\' LIMIT 1',array(1=>'Error actualizando estado del documento ('.$___D['fieK'].'): '));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $js=_js::r('Estado actualizado correctamente.');
			Doc::log_post(array('serieType'=>$serieType,'docEntry'=>$___D['docEntry'],'cartStatus'=>$___D['fieK'],'lineMemo'=>$___D['lineMemo']));
		}
	}
	echo $js;
}

/* */
else if(_0s::$router==('GET pvt/opens')){a_ses::hashKey('sellOrd.openQty');
	_ADMS::lib('iDoc');
	if($_GET['A_docStatus(E_igual)']==''){ $_GET['A.docStatus(E_igual)']='O'; }
	$_GET['fie']='A.whsId,A.addrMerch,A.addrInv,A.lineMemo';
	if(!$_GET['orderBy']){ $___D['orderBy']= 'docEntryAsc'; }
	$_GET['fromA']='A.* FROM gvt_opvt A ';
	echo iDoc::get($_GET);
}
/* personali */
else if(_0s::$router=='GET pvt/liq'){ a_ses::hashKey('sellOrd.basic');
	if($js=_js::ise($___D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	echo Doc::getOne(array('docEntry'=>$___D['docEntry'],'fromA'=>'ocrd.licTradType,ocrd.licTradNum,A.cardName,A.docDate,A.dueDate,A.docStatus,A.docEntry,A.ref1,A.dateC FROM gvt_opvt A JOIN par_ocrd ocrd ON (ocrd.cardId=A.cardId)',
	'fromB'=>'I.itemCode, I.itemName,SUM(B.quantity) quantity, B.lineTotal FROM gvt_pvt1 B LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)','whB'=>'GROUP BY I.itemCode,I.itemName,B.lineTotal'));
}

else if(_0s::$router=='GET pvt/rep/openQty'){ a_ses::hashKey('gvtPvt.repOpenQty');
	_ADMS::_lb('sql/filter');
	$tipoEstado=$___D['tipoEstado']; unset($___D['tipoEstado']);
	$whStat=($tipoEstado=='A')?'A.docStatus IN(\'O\',\'C\') ':'A.docStatus=\'O\' AND B.openQty>0';
	$whStat=($tipoEstado=='C')?'A.docStatus=\'C\'':$whStat;
	$whStat=($tipoEstado=='S')?'A.docStatus=\'S\'':$whStat;
	$_V=_V::_gA('pvt_lineMemoAtReport,pvt_barCode');
	$viewType=$___D['viewType'];
	if($viewType=='EP'){$___D['viewType2']='doc'; }
	$gbMemo=($___D['viewType2']=='doc')?',A.lineMemo':'';
	$gbMemo.=($_V['pvt_barCode'])?',BC.barCode':'';
	$gb='A.whsId,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm,W1.onHand,W1.isCommited';
	if($___D['viewType2']=='card'){ $gb='C.grId,A.cardName,A.docEntry,A.slpId,'.$gb.',C.invDayClose'; }
	else if($___D['viewType2']=='doc'){ $gb='C.grId,A.cardName,A.docEntry,A.docDate,A.dueDate,A.slpId,A.ref1,'.$gb.',C.invDayClose,A.cartStatus'; }
	$gb=$gb.$gbMemo;
	unset($___D['viewType2'],$___D['viewType']);
	$wh=a_sql_filtByT($___D);
	$lefBC='';
	if($_V['pvt_barCode']){
		$lefBC = 'LEFT JOIN itm_bar1 BC ON (BC.itemId=B.itemId AND BC.itemSzId=B.itemSzId AND BC.grTypeId=\''.$_V['pvt_barCode'].'\' ) ';
	}
	$qu='SELECT '.$gb.',SUM(quantity) quantity, SUM(B.openQty) openQty 
	FROM gvt_opvt A
LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
JOIN gvt_pvt1 B ON (B.docEntry=A.docEntry)
JOIN itm_oitm I ON (I.itemId=B.itemId)
LEFT JOIN ivt_oitw W1 ON (W1.whsId=A.whsId AND W1.itemId=B.itemId AND W1.itemSzId=B.itemSzId)
'.$lefBC.'
WHERE A.canceled=\'N\' AND '.$whStat.' '.$wh.' GROUP BY '.$gb.'';
	$q=a_sql::query($qu,array(1=>'Error obteniendo reporte de pendientes: ',2=>'No se encontraron resultados.'));
//if(a_ses::$userId==1){ echo $qu; }
if(a_sql::$err){ $js=a_sql::$errNoText; }
else{
	$Mx=$_V;
	$A=array();
	$Mx['_view']=$viewType;
	$Mx['L']=array();
	$Iw=array(); $n=0;
	$dateClose=date('Y-m-');
	while($L=$q->fetch_assoc()){
		$L['invDayClose']=$dateClose.$L['invDayClose'];
		$Mx['L'][]=$L;
	}
	$js=_js::enc($Mx,'NO_PAGER');
}
echo $js;
}
else if(_0s::$router=='GET pvt/rep/itemCanceled'){ a_ses::hashKey('gvtPvt.repitemCanceled');
	_ADMS::_lb('sql/filter');
	$wh=a_sql_filtByT($___D);
	$gb='A.docEntry,A.docStatus,A.dateC,A.docDate,A.dueDate,A.cardName,D99.userId, D99.lineMemo,D99.dateC';
	$q=a_sql::query('SELECT '.$gb.' dateCNull FROM gvt_opvt A
JOIN gvt_pvt1 B ON (B.docEntry=A.docEntry)
JOIN itm_oitm I ON (I.itemId=B.itemId)
LEFT JOIN gvt_doc99 D99 ON (D99.serieType=\''.$serieType.'\' AND D99.docEntry=A.docEntry AND D99.fiek=\'docStatus\' AND D99.fiev=\'N\')
WHERE 1 AND A.docStatus=\'N\' '.$wh.'
GROUP BY '.$gb.' '.a_sql::nextLimit(),array(1=>'Error obteniendo listado de documentos anulados.',2=>'No se encontraron resultados.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{ $Mx=array('L'=>array());
		while($L=$q->fetch_assoc()){
			$Mx['L'][]=$L;
		}
		$js=_js::enc($Mx);
	}
	echo $js;
}
else if(_0s::$router=='GET pvt/rep/stockPeP'){ a_ses::hashKey('gvtPvt.repOpenQty');
	_ADMS::lib('sql/filter');
	if($___D['I_itemCode(E_like3)']){
		$___D['I.itemCode(E_in)']=$___D['I_itemCode(E_like3)'];
	}unset($___D['I_itemCode(E_like3)']);
	$whsId=($___D['whsId']); unset($___D['whsId']);
	itwOnOrder($whsId);
	$gb='A.docEntry,A.docDate,A.dueDate,B.itemId,I.itemCode,I.itemName,B.itemSzId,W.onHand,W.isCommited,A.docDate,A.dueDate,A.cardName,A.slpId,W.onOrder,A.cartStatus';
	$fie=$gb.', SUM(B.openQty) \'openQty\'';
	$wh=a_sql_filtByT($___D);
	$qu='SELECT '.$fie.' FROM gvt_opvt A 
JOIN gvt_pvt1 B ON (B.docEntry=A.docEntry)
LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
LEFT JOIN ivt_oitw W ON (W.itemId=B.itemId AND W.itemSzId=B.itemSzId AND W.whsId=A.whsId)
WHERE A.docStatus=\'O\' AND B.openQty>0 AND A.whsId=\''.$whsId.'\' '.$wh.' GROUP BY '.$gb.'';
	$q=a_sql::query($qu,array(1=>'Error obteniendo reporte de pendientes: ',2=>'No se encontraron resultados.'));
//if(a_ses::$userId==1){ echo $qu; }
if(a_sql::$err){ $js=a_sql::$errNoText; }
else{
	$Mx=$_V;
	$A=array();
	$Mx['L']=array();
	$Iw=array(); $n=0;
	$dateClose=date('Y-m-');
	while($L=$q->fetch_assoc()){
		$Mx['L'][]=$L;
	}
	$js=_js::enc2($Mx);
}
echo $js;
}

?>
