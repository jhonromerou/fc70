<?php
if($js=_js::ise($___D['docEntrys'],'No se ha definido números de documentos')){ die($js); }
	else{
		$Mx=array('L'=>array());
		$gb='A.docEntry,A.tt,A.tr,A.cardId,A.cardName,B.detail,I.itemId,I.itemCode,I.itemName,G1.itemSize';
		$q2=a_sql::query('SELECT '.$gb.',SUM(B.quantity) quantity
		FROM '._0s::$Tb['gvt_odlv'].' A 
		JOIN '._0s::$Tb['gvt_dlv1'].' B ON (B.docEntry=A.docEntry)
		LEFT JOIN '._0s::$Tb['itm_oitm'].' I ON (I.itemId=B.itemId)
		LEFT JOIN '._0s::$Tb['itm_grs1'].' G1 ON (G1.itemSzId=B.itemSzId) 
		WHERE B.docEntry IN ('.$___D['docEntrys'].')
		GROUP BY '.$gb.' ORDER BY A.docEntry,B.detail,I.itemCode,G1.itemSize LIMIT 10000
		',array(1=>'Error obteniendo lineas del documento: ',2=>'El documento no tiene lineas.'));
		if(a_sql::$err){ $Mx['L']= json_decode(a_sql::$errNoText,1); }
		else{
			$A=array(); $SV=array(); $n=0; $na=0; $refLast=''; $lastItem='';
			$lastBox='';
			while($L=$q2->fetch_assoc()){
				$k=$L['docEntry'].'_'.$L['detail']; 
				$k2=$L['itemId'].'_'.$L['detail'];
				if(!array_key_exists($k,$SV)){ $SV[$k]=$n;
					if($n>0){ $A[$na]['refsLine'] = substr($A[$na]['refsLine'],0,-2); }
					$A[$n]=array('pvt_docEntry'=>$L['tr'],'cardName'=>$L['cardName'],'boxText'=>$L['detail'],'refsLine'=>''); $n++;
				}
				$na=$SV[$k];
				if($refLast!=$k2){
					if($lastBox!='' && $lastBox==$L['detail']){
						$A[$na]['refsLine'] = substr($A[$na]['refsLine'],0,-2).'<br/>';
					}
					$A[$na]['refsLine'] .= 'Ref. '.$L['itemCode'].'. '.substr($L['itemName'],0,15).' ';
				}
				$A[$na]['refsLine'] .= 'T:'.$L['itemSize'].'/'.($L['quantity']*1).', ';
				$refLast=$k2; $lastItem=$L['itemId'];
				$lastBox=$L['detail'];
			}
			$Mx['L']=$A; unset($A);
		}
		$js=_js::enc2($Mx);
	}
	echo $js;
?>