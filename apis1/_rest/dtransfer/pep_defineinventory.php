<?php

require('wmaPeP_defineInventory.php');
	$R = _fread_tab::get($_FILES['file'],array('lineIni'=>3,'lineEnd'=>2100,
'K'=>array('almacen','fase','itemCode','talla','onHand'))
);
/*
DELETE FROM pep_oitw WHERE whsId IN(6,17,18,19,20,21,22,23,24)
DELETE FROM pep_oitw WHERE whsId IN(17,22,23,19,18,24,21,20)
DELETE FROM pep_oitw WHERE whsId IN(6,21,17,22,23,19,18,24,20)
*/
$ALM=array(
'troq'=>6,
'pgua'=>21,'pregu'=>21,'preparado guarnecida'=>21,
'prelineas'=>17,'strobell'=>17,
'alm2'=>22,'almacen2'=>22,
'alm3'=>23,'almacen3'=>23,
'vulc'=>19,
'inyc'=>18,'inyeccion'=>18,
'mesat'=>20,
'reproceso'=>24,'repro'=>24,'reproceso carlos julio'=>24
);
$Fas=array(
'corte'=>2,
'con strobell'=>10,'strobell'=>10,'strobel-virada'=>10,'h0001'=>10,
'sin terminar'=>11,'I0001'=>11,
'guarnecida'=>6,'guarnicion'=>6,'guarnecido'=>6,
'reproceso'=>15,
'muestras'=>16,
'terceras'=>18,'nonos'=>17,
'segunda'=>19,'segundas'=>19,
);
$Di2=array();
$IT=array();
if($R['errNo']){ $js = _js::e($R); }
else{
	
	$grTypeId=3;
	foreach($R['L'] as $ln => $Da){
		$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
		$lineTotal++;
		$Da['almacen']=strtolower($Da['almacen']);
		$Da['fase']=strtolower($Da['fase']);
		$Da['onHand']=str_replace(',','.',$Da['onHand']);
		if($js=_js::ise($Da['itemCode'],$lnt.'Se debe definir el código del producto.')){ die($js); }
		else if($js=_js::ise($Da['onHand'],$lnt.'Se debe definir la cantidad disponible')){ die($js); }
		else if(!array_key_exists($Da['almacen'],$ALM)){ die(_js::e(3,'El almacen '.$Da['almacen'].' no tiene ID definida.')); }
		else if(!array_key_exists($Da['fase'],$Fas)){ die(_js::e(3,'La fase '.$Da['fase'].' no tiene ID definida.')); }
		$itk=$Da['itemCode'];
		if(!array_key_exists($itk,$IT)){
			$q1=a_sql::fetch('SELECT I.itemId FROM '._0s::$Tb['itm_oitm'].' I WHERE I.itemCode=\''.$Da['itemCode'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo información del artículo.',2=>$lnt.'El artículo '.$Da['itemCode'].' no existe.'));
			if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
			$It[$itk]=$q1['itemId'];
		}
		$itemId=$It[$itk];
		$whsId=$ALM[$Da['almacen']];
		$wfaId=$Fas[$Da['fase']];
		$Di2[]=array('whsId'=>$whsId,'wfaId'=>$wfaId,'itemId'=>$itemId,'itemSzId'=>$Da['talla'],'onHand'=>$Da['onHand']);
		
	}
	if($errs==0){
		a_sql::transaction(); $comit=false;
		foreach($Di2 as $n =>$Di){
			$ins=a_sql::insert($Di,array('tbk'=>'pep_oitw','qDo'=>'insert','wh_change'=>'WHERE itemId=\''.$Di['itemId'].'\' AND whsId=\''.$Di['whsId'].'\' AND wfaId=\''.$Di['wfaId'].'\' LIMIT 1'));
			if($ins['err']){ $js=_js::e(3,$lnt.'Error actualizando definiendo cantidad disponible: '.$ins['text']); $errs++; break; }
		}
		if($errs==0){ $comit=true; $js=_js::r('Se actualizaron '.$lineTotal.' lineas.'); }
		a_sql::transaction($comit);
	}
}
echo $js;
?>