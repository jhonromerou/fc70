<?php
	$R = _fread_tab::get($_FILES['file'],array('lineIni'=>3,'lineEnd'=>500,
'K'=>array('itemCode','costo'))
);
if($R['errNo']){ $js = _js::e($R); }
else{
	a_sql::transaction(); $comit=false;
	$grTypeId=3;
	foreach($R['L'] as $ln => $Da){
		$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
		$lineTotal++;
		if($js=_js::ise($Da['itemCode'],$lnt.'Se debe definir el código del producto.')){ die($js); }
		else if($js=_js::ise($Da['costo'],$lnt.'Se debe definir el código de Siigo')){ die($js); }
		$q1=a_sql::fetch('SELECT I.itemId,I.itemType FROM '._0s::$Tb['itm_oitm'].' I JOIN '._0s::$Tb['itm_bar1'].' BC ON (BC.itemId=I.itemId) WHERE BC.grTypeId=\''.$grTypeId.'\' AND BC.barCode LIKE \'%'.$Da['itemCode'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo información del artículo.',2=>$lnt.'El código de barras ('.$Da['itemCode'].') no está relacionado a ningun artículo.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
		else if($q1['itemType']!='MP'){ $js=_js::e(3,$lnt.'El artículo no es del tipo materia prima.'); $errs++; break; }
		else{
			$Di=array('itemId'=>$q1['itemId'],'avgPrice'=>$Da['costo']);
			$ins=a_sql::insert($Di,array('tbk'=>'itm_oitm','qDo'=>'update','wh_change'=>'WHERE itemId=\''.$q1['itemId'].'\' LIMIT 1'));
			if($ins['err']){ $js=_js::e(3,$lnt.'Error actualizando '.$Di['itemId'].'-'.$Di['buyPrices'].': '.$ins['text']); $errs++; break; }
		}
	}
	if($errs==0){ $comit=true; $js=_js::r('Se actualizaron '.$lineTotal.' lineas.'); }
	a_sql::transaction($comit);
}
echo $js;
?>