<?php
	$R = JFread::get($_FILES['file'],array('lineIni'=>3,'lineEnd'=>500,
'K'=>array('itemCode','sellPrice'))
);
if($R['errNo']){ $js = _js::e($R); }
else{
	a_sql::transaction(); $comit=false;
	$grTypeId=3;
	foreach($R['L'] as $ln => $Da){
		$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
		$lineTotal++;
		if($js=_js::ise($Da['itemCode'],$lnt.'Se debe definir el código del producto.')){ die($js); }
		else if($js=_js::ise($Da['sellPrice'],$lnt.'Se debe definir el precio de venta')){ die($js); }
		$Da['sellPrice']=str_replace(',','.',$Da['sellPrice']);
		$q1=a_sql::fetch('SELECT I.itemId FROM '._0s::$Tb['itm_oitm'].' I WHERE I.itemCode=\''.$Da['itemCode'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo información del artículo.',2=>$lnt.'El artículo '.$Da['itemCode'].' no existe.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
		else{
			$Di=array('itemId'=>$q1['itemId'],'sellPrice'=>$Da['sellPrice']);
			$ins=a_sql::insert($Di,array('tbk'=>'itm_oitm','qDo'=>'update','wh_change'=>'WHERE itemId=\''.$q1['itemId'].'\' LIMIT 1'));
			if($ins['err']){ $js=_js::e(3,$lnt.'Error actualizando '.$Di['itemId'].'-'.$Di['sellPrice'].': '.$ins['text']); $errs++; break; }
		}
	}
	if($errs==0){ $comit=true; $js=_js::r('Se actualizaron '.$lineTotal.' lineas.'); }
	a_sql::transaction($comit);
}
echo $js;
?>