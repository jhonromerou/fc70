<?php

$R = _fread_tab::get($_FILES['file'],array('lineIni'=>3,'lineEnd'=>1000,
'K'=>array('addId','cardCode','cardType','grId',
'slpId','actived','cardName','cardNameC',
'address','RF_mmag',
'phone1','phone2','cellular','email','referFrom',
'discPf','creditLine',
'licTradType','licTradNum','RF_regTrib','RF_tipEnt','RF_actEco','RF_reqRet','RF_autoRet','RF_firstName','RF_firstName2','RF_lastName','RF_lastName2'))
);
if($R['errNo']){ $js = _js::e($R); }
else{
	$grTypeId=3; $CRDs=array(); $CRDe=array(); $CRNa=array();
	foreach($R['L'] as $ln => $Da){
		$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
		$lineTotal++;
		$ks=$Da['licTradNum'];
		if(array_key_exists($Da['cardCode'],$CRDs)){ $errs++; $js=_js::e(3,$lnt.'El código de socios ya está definido en la '.$CRDs[$Da['cardCode']].'.'); break; }
		else if($Da['licTradNum']!='' && $CRDe[$ks]==$Da['cardType']){ $js=_js::e(4,$lnt.'Ya existe un socio de negocios del mismo tipo registrado con ese documento en este archivo.'); $errs++; break; }
		else if(!preg_match('/^(C|S|L)$/',$Da['cardType'])){ $errs++; $js=_js::e(3,$lnt.'El tipo debe estar definido correctamente.'); break; }
		else if(!preg_match('/^(Y|N)$/',$Da['actived'])){ $errs++; $js=_js::e(3,$lnt.'Se debe definir correctamente el estado del socio'); break; }
		else if($js=_js::textLen($Da['addId'],20,$lnt.'El Id adicional no puede superar 20 caracteres.')){ $errs++; break; }
		else if($js=_js::textLen($Da['cardCode'],20,$lnt.'El código no puede superar 20 caracteres.')){ $errs++; break; }
		else if($js=_js::ise($Da['cardName'],$lnt.'Se debe definir el nombre del socio de negocios.')){ $errs++; break; }
		else if($js=_js::textLen($Da['cardName'],100,$lnt.'El nombre del cliente no puede exceder 100 caracteres')){ $errs++; break; }
		else if($Da['RF_mmag']!='' && $js=_js::ise($Da['RF_mmag'],$lnt.'La ciudad debe ser un número.')){ $errs++; break; }
		else{
			if($Da['cardCode']!=''){ $CRDs[$Da['cardCode']]='Linea '.$ln; }
			if($Da['licTradNum']!=''){ $CRDe[$ks]=$Da['cardType']; }
			$CRNa[$Da['cardName']]=1;
			$ex=a_sql::fetch('SELECT cardType,cardName,cardCode,licTradNum FROM '._0s::$Tb['par_ocrd'].' WHERE cardCode=\''.$Da['cardCode'].'\' OR licTradNum=\''.$Da['licTradNum'].'\' LIMIT 1 ',array(1=>'Error obteniendo información de socio de negocios'));
			if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
			else if(a_sql::$errNo=='-1'){
				$exis=' Tipo: '.$ex['cardType'].' ';
				if($ex['cardCode']==$Da['cardCode']){ $js=_js::e(4,$lnt.'Ya existe un socio de negocios con este código.'.$exis); $errs++; break; }
				else if($ex['cardName']==$Da['cardName']){ $js=_js::e(4,$lnt.'Ya existe un socio de negocios con ese nombre.'.$exis); $errs++; break; }
				else if($ex['cardType']==$Da['cardType'] && $ex['licTradNum']==$Da['licTradNum']){ $js=_js::e(4,$lnt.'Ya existe un socio de negocios del mismo tipo registrado con el número de documento.'.$exis); $errs++; break; }
			}
		}
	}
	if($errs==0){
		a_sql::transaction(); $comit=false;
		foreach($R['L'] as $ln => $Da){
			$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
			$ins=a_sql::insert($Da,array('tbk'=>'par_ocrd','qDo'=>'insert'));
			if($ins['err']){ $js=_js::e(3,$lnt.'Error creando socio de negocios: '.$ins['text']); $errs++; break; }
		}
		if($errs==0){ a_sql::transaction(true); $js=_js::r('Se definieron los socios de negocio correctamente.'); }
	}
}
echo $js;
/* JS para campos en sql
{"Fie":{
"addId":{"t":"Id adicional","d":"ID adicional"},
"cardCode":{"t":"Codigo Socio","d":"Debe ser un codigo unico si se define"},
"cardType":{"t":"* Tipo Socio","d":"C=Cliente, S=Proveedor, L=Lead"},
"grId":{"t":"Grupo","d":"ID del Grupo de socio"},
"slpId":{"t":"Responsable","d":"ID del responsable de Ventas"},
"actived":{"t":"* Activo?","d":"Socio activo, Y=Si, N=No"},
"cardName":{"t":"* Nombre Socio","d":"Nombre de Socio"},
"cardNameC":{"t":"Nombre Comercial de Socio","d":"Nombre Comercial de Socio"},
"address":{"t":"Direccion","d":"Direccion Principal"},
"RF_mmag":{"t":"Ciudad","d":"ID Ciudad principal."},
"phone1":{"t":"Telefono 1","d":"Telefono 1"},
"phone2":{"t":"Telefono 2","d":"Telefono 2"},
"cellular":{"t":"Celular","d":"Numero celular"},
"email":{"t":"correo electronico","d":"correo electronico"},
"referFrom":{"t":"Referido por","d":"Referido por"},
"discPf":{"t":"Descuento autorizado","d":"Descuento autorizado"},
"creditLine":{"t":"Cupo","d":"Cupo de credito"},
"licTradType":{"t":"Tipo Documento","d":"Tipo de Documento Registrado. nit=NIT, dni=Cedula Ciudadania, dne=Cedula Extranjeria, pp=Pasaporte"},
"licTradNum":{"t":"Numero Doc.","d":"Numero de Documento"},
"RF_regTrib":{"t":"Regimen","d":"Tipo de Regimen. RC=Regimen Comun, RS=Resimen Simplificado, RE=Regimen Extranjero, GC=Gran Contribuyente"},
"RF_tipEnt":{"t":"Tipo persona","d":"Tipo de persona. PN=Persona Natural, PJ=Persona Juridica"},
"RF_actEco":{"t":"ciiu","d":"ID de actividad economica, ciiu"},
"RF_reqRet":{"t":"Retencion","d":"¿Sujeto a Retencion?"},
"RF_autoRet":{"t":"Autoretenedor","d":"¿Es autoretenedor?"},
"RF_firstName":{"t":"Primer Nombre","d":"Primer Nombre Persona Natural"},
"RF_firstName2":{"t":"Segundo Nombre","d":"Segundo Nombre Personal Natural"},
"RF_lastName":{"t":"Primer Apellido","d":"Primer apellido persona natural"},
"RF_lastName2":{"t":"Segundo Apellido","d":"Segundo Apellido Persona Natural"}
}
}
*/
?>