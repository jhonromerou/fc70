<?php
$R = _fread_tab::get($_FILES['file'],array('lineIni'=>2,
'lineEnd'=>8000,
'K'=>array('itemCode','childNum','itemCodeChild','pieceName','quantity','isVari','variType','wfaCode'))
);
if($R['errNo']){ $js = _js::e($R); }
else{
	$lnR=array();//verificar lineas repetidas
	//a_sql::dbase(array('sql_db'=>'pr_app_ps'));
	$errs = 0; $lineTotal=0;
	$sepdec = $R['L'][3]['sepdec'];
	a_sql::transaction(); $comit=false;
	foreach($R['L'] as $ln => $Da){
	$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
	$Da['quantity']=str_replace(',','.',$Da['quantity']);
	//if($ln==10){ break; }
	$lineTotal++; $lna=$Da['itemCode'].'_'.$Da['childNum'];
		if(_js::ise($Da['childNum'],0,'numeric>0')){ $js=_js::e(3,$lnt.'No se ha definido la linea de posición para el artículo hijo.'); $errs++; break; }
		else if($lnR[$lna]){ $js=_js::e(3,$lnt.'El número de linea que determina la posición del artículo hijo no puede estar repetido ('.$lna.').'); $errs++; break; }
		else if($Da['itemCode']==''){ $js=_js::e(3,$lnt.'itemCode no ha sido definido.'); $errs++; break; }
		else if($Da['itemCodeChild']==''){ $js=_js::e(3,$lnt.'itemCode Hijo no ha sido definido.'); $errs++; break; }
		else if(_js::ise($Da['quantity'],0,'numeric>0')){ $js=_js::e(3,$lnt.'La cantidad debe ser un número y mayor a 0.'); $errs++; break; }
		else if($js=reviewItm($Da,array('lnt'=>$lnt,'wfaCode'=>'Y'))){ $errs++; break; }
		else{ $lnR[$lna]=$lna.'_'.$ln;
			$q1=a_sql::$Rtemp['q1']; $q2=a_sql::$Rtemp['q2']; $q3=a_sql::$Rtemp['q3'];
			a_sql::$Rtemp=array();
			$Di=array('itemId'=>$q1['itemId'],'lineNum'=>$Da['childNum'],'citemId'=>$q2['itemId'],'isVari'=>$Da['isVari'],'variType'=>$Da['variType'],'wfaId'=>$q3['wfaId'],'pieceName'=>$Da['pieceName'],'quantity'=>$Da['quantity']);
			if($js=_js::ise($Di['itemId'],'itemId no pudo ser definido')){ die($js); }
			if($js=_js::ise($Di['citemId'],'citemId  no pudo ser definido (hijo).')){ die($js); }
			if($js=_js::ise($Di['wfaId'],'wfaId  no pudo ser definido (fase).')){ die($js); }
			//0
			$ins=a_sql::insert($Di,array('tbk'=>'itm_itt1','qDo'=>'insert'));
			if($ins['err']){ $js=_js::e(3,$lnt.'Error guardando linea: '.$ins['text']); $errs++; break; }
		}
	}
	if($errs==0){ $js=_js::r('Proceso realizado correctamente. Se registraron '.$lineTotal.' cambios.'); $comit=true; }
	else{ $comit='rollback'; }
	a_sql::transaction($comit);
}
echo $js;
?>