<?php
/*Definir fases de un articulo */
$R = _fread_tab::get($_FILES['file'],array('lineIni'=>3,'lineEnd'=>1000,
'K'=>array('itemCode','lineNum','wfaId','faseText','cost','teoricTime','timePrep','timeOper','lineMemo'))
);
if($R['errNo']){ $js = _js::e($R); }
else{
	$Ds=array(); $wfaIdBef=0; $wfaNextId=''; $lNumAnt=0; $kAnt='';
	foreach($R['L'] as $ln => $Da){
		$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
		$lineTotal++; unset($Da['faseText']);
		$Da['lineType']='F'; 
		if($js=_js::ise($Da['lineNum'],'El número de linea debe ser un número mayor a 0.','numeric>0')){ die($js); }
		else if($js=_js::ise($Da['wfaId'],'Se debe definir el ID de la Fase','numeric>0')){ die($js); }
		else if($js=_js::ise($Da['cost'],'Se debe definir el costo de la fase.','numeric>0')){ die($js); }
		else{
			$k=$Da['itemCode']; $lNum=$Da['lineNum']*1;
			if(!array_key_exists($k,$Ds)){
				$q1=a_sql::fetch('SELECT I.itemId,I.prdItem,M.mpgId FROM '._0s::$Tb['itm_oitm'].' I LEFT JOIN '._0s::$Tb['wma_ompg'].' M ON (M.itemId=I.itemId) WHERE I.itemCode=\''.$Da['itemCode'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo artículo',2=>$lnt.'El artículo '.$Da['itemCode'].' no existe'));
				if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
				//else if($q1['prdItem']!='Y'){ $js=_js::e(3,$lnt.'El artículo ('.$Da['itemCode'].') debe ser de producción.'); $errs++; break; }
				else if($q1['mpgId']>0){ $js=_js::e(3,$lnt.'Ya hay definida una matriz de fases para el artículo '.$Da['itemCode'].'.'); $errs++; break; }
				$Ds[$k]=array('itemId'=>$q1['itemId'],'mpgName'=>'Ref. '.$Da['itemCode'],'cost'=>0,'faseTime'=>0,'L'=>array());
			}
			$Ds[$k]['cost']+=$Da['cost'];
			$Ds[$k]['faseTime']+=$Da['teoricTime'];
			if(!array_key_exists($lNum,$Ds[$k])){$Ds[$k]['L'][$lNum]=array(); }
			unset($Da['itemCode']);
			$Ds[$k]['L'][$lNum]=$Da;
			if($lNum==1){ $Ds[$k]['L'][$lNum]['wfaIdBef']=0; }
			else{ $Ds[$k]['L'][$lNum]['wfaIdBef']=$Ds[$k]['L'][$lNumAnt]['wfaId'] ; }
			if($lNum>1){
				if($kAnt!=$k){ /*ultima linea */
					$Ds[$k]['L'][$lNumAnt]['wfaIdNext']=999;
				}
				else{ $Ds[$k]['L'][$lNumAnt]['wfaIdNext']=$Da['wfaId'];
			}
			$kAnt=$k; $lNumAnt=$lNum;
		}
			$kAnt=$k; $lNumAnt=$lNum;
		}
	}
	if($errs==0){
	a_sql::transaction();
		foreach($Ds as $itemCode => $LN){
			$L=$LN['L']; unset($LN['L']);
			$ins=a_sql::insert($LN,array('tbk'=>'wma_ompg','qDo'=>'insert'));
			if($ins['errr']){ $js=_js::e(3,'Error guardando matriz base de '.$LN['itemCode'].': '.$ins['err']['error_sql']); $errs++; break; }
			else{
				foreach($L as $kl => $Di){
					$Di['mpgId']=$ins['insertId'];
					$ins2=a_sql::insert($Di,array('tbk'=>'wma_mpg1','qDo'=>'insert'));
					if($ins2['err']){ $js=_js::e(3,'Error guardando lineas de fase '.$LN['itemCode'].': '.$ins2['err']['error_sql']); $errs++; break 2; }
				}
			}
		}
	}
	if($errs==0){ $js=_js::r('Fases Definidas correctamente.'); a_sql::transaction(true); }
}
echo $js;
?>