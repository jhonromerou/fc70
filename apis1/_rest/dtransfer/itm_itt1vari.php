<?php
	$R = _fread_tab::get($_FILES['file'],array('lineIni'=>2,
'lineEnd'=>4000,
'K'=>array('itemCode','pieceName','itemSize','itemCodeChild','quantity'))
);
if($R['errNo']){ $js = _js::e($R); }
else{
	$schR=array();
	$lnR=array();//verificar lineas repetidas
	//a_sql::dbase(array('sql_db'=>'pr_app_ps'));
	$errs = 0; $lineTotal=0;
	$sepdec = $R['L'][3]['sepdec'];
	a_sql::transaction(); $comit=false;
	foreach($R['L'] as $ln => $Da){
	$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
	$lineTotal++;
	$lna=$Da['itemCode'].'_'.$Da['pieceName'].'_'.$Da['itemSize'];
		if(_js::ise($Da['pieceName'])){ $js=_js::e(3,$lnt.'No se ha definido el subesquema para el artículo hijo.'); $errs++; break; }
		else if($lnR[$lna]){ $js=_js::e(3,$lnt.'El artículo se encuentra repetido para la misma talla.'); $errs++; break; }
		else if($Da['itemCode']==''){ $js=_js::e(3,$lnt.'itemCode no ha sido definido.'); $errs++; break; }
		else if(_js::ise($Da['quantity'],0,'numeric>0')){ $js=_js::e(3,$lnt.'La cantidad debe ser un número y mayor a 0.'); $errs++; break; }
		else if($js=reviewItm($Da,array('lnt'=>$lnt,'pieceName'=>$Da['pieceName']))){ $errs++; break; }
		else{ $lnR[$lna]=$lna;
			$q1=a_sql::$Rtemp['q1']; $q2=a_sql::$Rtemp['q2']; $q4=a_sql::$Rtemp['q4'];
			$Di=array('itemId'=>$q1['itemId'],'fatherId'=>$q4['lineNum'],'lineNum'=>$q4['lineNum'],'citemId'=>$q2['itemId'],'itemSzId'=>$q4['itemSzId'],'isVari'=>'Y','variType'=>'N','wfaId'=>$q4['wfaId'],'pieceName'=>$Da['pieceName'],'quantity'=>$Da['quantity']);
			$ksch=$q1['itemId'].'_'.$Da['pieceName'];
			$ins=a_sql::insert($Di,array('tbk'=>'itm_itt1','qDo'=>'insert'));
			if($ins['err']){ $js=_js::e(3,$lnt.'Error guardando linea: '.$ins['text']); $errs++; break; }
			$schR[$ksch]=$q1['T'];
		}
	}
	if($errs==0){
		foreach($schR as $tk => $T){
			$sep=explode('_',$tk);
			$q=a_sql::fetch('SELECT itemId,citemId,lineNum fatherId,lineNum,quantity,wfaId FROM '._0s::$Tb['itm_itt1'].' WHERE itemId=\''.$sep[0].'\' AND pieceName=\''.$sep[1].'\' AND itemSzId=\'0\' AND variType!=\'N\' LIMIT 1',array(1=>'Error obteniendo lista de materiales con variantes base: '));
			if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
			else if(a_sql::$errNo==-1){
				foreach($T as $itemSzId){
					$q['isVari']='Y'; $q['variType']='N'; $q['itemSzId']=$itemSzId;
					$q['pieceName']=$sep[1];
					$ins2=a_sql::insert($q,array('qDo'=>'noUpdateIfExist','tbk'=>'itm_itt1','wh_change'=>'WHERE itemId=\''.$sep[0].'\' AND pieceName=\''.$sep[1].'\' AND itemSzId=\''.$itemSzId.'\' AND isVari=\'Y\' LIMIT 1'));
					if($ins2['err']){ $js=_js::e(3,'Error guardando información para tallas que aplican con la base: '.$ins2['text']); $errs++; break 2; }
				}
			}
		}
	}
	if($errs==0){ $js=_js::r('Proceso realizado correctamente. Se registraron '.$lineTotal.' cambios.'); $comit=true; }
	else{ $comit='rollback'; }
	a_sql::transaction($comit);
}
echo $js;
?>