<?php
	$R = _fread_tab::get($_FILES['file'],array('lineIni'=>3,'lineEnd'=>500,
'K'=>array('docEntry','itemCode','itemSize','quantity'))
);
if($R['errNo']){ $js = _js::e($R); }
else{
	a_sql::transaction(); $comit=false;
	$grTypeId=3;
	foreach($R['L'] as $ln => $Da){
		$lnt = 'Linea '.$ln.': '; $a = ' Actual: ';
		$lineTotal++;
		if($js=_js::ise($Da['docEntry'],$lnt.'Se debe definir el número de documento','numeric>0')){ die($js); }
		else if($js=_js::ise($Da['itemCode'],$lnt.'Se debe definir el código del producto.')){ die($js); }
		else if($js=_js::ise($Da['itemSize'],$lnt.'Se debe definir la talla del producto.')){ die($js); }
		else if($js=_js::ise($Da['quantity'],$lnt.'Se debe definir la cantidad y se run número mayor a 0.','numeric>0')){ die($js); }
		$q1=a_sql::fetch('SELECT I.itemId FROM '._0s::$Tb['itm_oitm'].' I WHERE I.itemCode=\''.$Da['itemCode'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo información del artículo.',2=>$lnt.'El código ('.$Da['itemCode'].') no existe.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
		else{
			$q2=a_sql::fetch('SELECT itemSzId FROM '._0s::$Tb['itm_grs1'].' WHERE itemSzId=\''.$Da['itemSize'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo información de la talla.',2=>$lnt.'La talla ('.$Da['itemSize'].') no existe.'));
			if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
			else{
				$Di=array('docEntry'=>$Da['docEntry'],'itemId'=>$q1['itemId'],'itemSzId'=>$q2['itemSzId'],'quantity'=>$Da['quantity']);
				$ins=a_sql::insert($Di,array('tbk'=>'ivt_cat1','qDo'=>'insert'));
				if($ins['err']){ $js=_js::e(3,$lnt.'Error registrando linea: '.$ins['text']); $errs++; break; }
			}
		}
	}
	if($errs==0){ $comit=true; $js=_js::r('Se actualizaron '.$lineTotal.' lineas.'); }
	a_sql::transaction($comit);
}
echo $js;
?>