<?php
function reviewItm($Da=array(),$P=array()){
	$r=false;
	$lnt=$P['lnt'];
	if($P['itemId']!='Y'){
		$q1=a_sql::fetch('SELECT itemId,itemType,prdItem,grsId FROM '._0s::$Tb['itm_oitm'].' WHERE itemCode=\''.$Da['itemCode'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo artículo',2=>$lnt.'El artículo '.$Da['itemCode'].' no existe'));
		if(a_sql::$err){ return a_sql::$errNoText; }
		else if($q1['itemType']!='P'){ return _js::e(3,$lnt.'Solo se pueden definir listas de materiales para los artículos modelo.'); }
		else if($q1['prdItem']!='Y'){ return _js::e(3,$lnt.'El artículo no está definido como un artículo de producción.');
		}
		else{
			$q1['T']=array();
			$q1_= a_sql::query('SELECT itemSzId FROM '._0s::$Tb['itm_grs2'].' WHERE grsId=\''.$q1['grsId'].'\' ',array(1=>$lnt.'Error obteniendo tallas del artículo: ',2=>$lnt.'El articulo no tiene tallas definidas.'));
			if(a_sql::$err){ return a_sql::$errNoText; }
			else while($L=$q1_->fetch_assoc()){
				$q1['T'][$L['itemSzId']]=$L['itemSzId'];
			}
		}
		a_sql::$Rtemp['q1']=$q1;
	}
	if($P['citemId']!='Y' && $Da['itemCodeChild']){
		$q2=a_sql::fetch('SELECT itemId,itemType,prdItem FROM '._0s::$Tb['itm_oitm'].' WHERE itemCode=\''.$Da['itemCodeChild'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo artículo hijo',2=>$lnt.'El artículo hijo '.$Da['itemCodeChild'].' no existe.'));
		if(a_sql::$err){ return a_sql::$errNoText; }
		else if($q2['itemType']!='MP'){ return _js::e(3,$lnt.'Solo se pueden relacionar artículos del tipo materia prima.'); }
		else if($q2['prdItem']=='Y'){ return _js::e(3,$lnt.'No se puede relacionar artículos de producción a otro.'); }
		a_sql::$Rtemp['q2']=$q2;
	}
	if($P['wfaCode']=='Y'){
		$q3=a_sql::fetch('SELECT wfaId FROM '._0s::$Tb['wma_owfa'].' WHERE wfaCode=\''.$Da['wfaCode'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo fase de consumo',2=>$lnt.'La fase de consumo ('.$Da['wfaCode'].') no existe.'));
		if(a_sql::$err){ return a_sql::$errNoText; }
		a_sql::$Rtemp['q3']=$q3;
	}
	if($P['pieceName']){
		$q4=a_sql::fetch('SELECT citemId,lineNum,quantity,wfaId FROM '._0s::$Tb['itm_itt1'].' WHERE isVari=\'N\' AND itemId=\''.$q1['itemId'].'\' AND pieceName=\''.$P['pieceName'].'\' LIMIT 1',array(1=>$lnt.'Error información de subesquema padre',2=>$lnt.'El subesquema ('.$P['pieceName'].') no se ha definido para el artículo '.$Da['itemCode'].'.'));
		if(a_sql::$err){ return a_sql::$errNoText; }
		$q5=a_sql::fetch('SELECT G1.itemSzId FROM '._0s::$Tb['itm_grs1'].' G1 JOIN '._0s::$Tb['itm_grs2'].' G2 ON (G2.itemSzId=G1.itemSzId) WHERE G1.itemSize=\''.$Da['itemSize'].'\' AND G2.grsId=\''.$q1['grsId'].'\' LIMIT 1',array(1=>$lnt.'Error obteniendo información de talla para el artículo',2=>$lnt.'La talla definida ('.$Da['itemSize'].') no existe para este artículo'));
		if(a_sql::$err){ return a_sql::$errNoText; }
		$q4['itemSzId']=$q5['itemSzId'];
		a_sql::$Rtemp['q4']=$q4;
	}
	return $r;
}
_ADMS::lib('JFread');
require_once('dtransfer/'._0s::$brouter.'.php');
?>