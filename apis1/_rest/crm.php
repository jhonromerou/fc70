<?php
_0s::uriReq('crd|cpr|nov|task|notes|deals|events|sysd');
_0s::$Tb['crm_onov']='crm_onov';
unset($___D['serieType'],$___D['textSearch']);

function sendCopyTask($P=array()){
	_ADMS::lib('_2d,xCurl,_File');
	$Dis=array('ocardCode'=>a_ses::$ocardCode,'accK'=>'crmTaskCopy','templateCode'=>'crmTaskCopy','mailTo'=>$P['mailTo'],'mailToName'=>$P['mailToName']);
	if(!array_key_exists('_Fi',$Dis)){ $Dis['_Fi']=array(); }
	$jsf=$Dis['_Fi']['jsData']=_File::tempFile(json_encode(array('userName'=>$P['userName'],'userNameFrom'=>$P['userNameFrom'],'title'=>$P['title'],'taskNotes'=>$P['shortDesc'],'prsName'=>$P['name'],'cardName'=>$P['cardName'],'endDate'=>_2d::f($P['endDate'].' '.$P['endDateAt'],'d M H:i'))),'datafile.js');
	$r=xCurl::post('http://api0.admsistems.com/xl/pubapps/emailSend',$Dis);
	_File::del($jsf);
}
function sendNtyAssg($P=array(),$qA=array()){
	//si hay asignado y es diferente a usuario actual
	if($P['userAssg'] && $qA['userAssg']!=$P['userAssg'] && $P['userAssg']!=a_ses::$userId){
		_ADMS::lib('_2d,xCurl,_File');
		$qA2=a_sql::fetch('SELECT U1.userName userNameFrom,U2.userName userName, U2.userEmail 
FROM crm_onov C 
LEFT JOIN a0_vs0_ousr U1 ON (U1.userId=C.userId)
LEFT JOIN a0_vs0_ousr U2 ON (U2.userId=C.userAssg)
WHERE gid=\''.$P['gid'].'\' LIMIT 1');
		$Dis=array('ocardCode'=>a_ses::$ocardCode,'accK'=>'emailTaskAssg','templateCode'=>'crmTaskAssg','mailTo'=>$qA2['userEmail']);
		if(!array_key_exists('_Fi',$Dis)){ $Dis['_Fi']=array(); }
	$jsf=$Dis['_Fi']['jsData']=_File::tempFile(json_encode(array('userName'=>$qA2['userName'],'userNameFrom'=>$qA2['userNameFrom'],'title'=>$P['title'],'taskNotes'=>$P['shortDesc'],'prsName'=>$P['name'],'cardName'=>$P['cardName'],'endDate'=>_2d::f($P['endDate'].' '.$P['endDateAt'],'d M H:i'))),'datafile.js');
		$r=xCurl::post('http://api0.admsistems.com/xl/pubapps/emailSend',$Dis,array('r'=>'txt'));
		_File::del($jsf);
		if(a_ses::$userId==1){
			//print_r($r);
		}
	}
}
function sendInvEvent($P=array()){
	_ADMS::lib('_2d,xCurl');
	if(!array_key_exists('_Fi',$P)){ $P['_Fi']=array(); }
	$jsf=$P['_Fi']['jsData']=_File::tempFile(json_encode($P['C']),'datafile.js');
	$Dis=array('ocardCode'=>a_ses::$ocardCode,'accK'=>'crmMailEventInv','templateCode'=>'crmEventInv','mailTo'=>$P['to']
	);
	if($P['_Fi']){ $Dis['_Fi']=$P['_Fi']; }
	$r=xCurl::post('http://api0.admsistems.com/xl/pubapps/emailSend',$Dis);
	if(a_ses::$userId==1){
		//print_r($r);
	}
	_File::del($jsf);
	return $r;
}

class crm{
static public function nov_gOne($P=array(),$fie='A.*'){
	$profile=$P['profile']; unset($P['profile']);
	if($profile){
		$fie .=',P.name,C.cardName';
		$leff='LEFT JOIN par_ocpr P ON (P.prsId=A.prsId)
		LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)';
	}
	$q=a_sql::fetch('SELECT '.$fie.' FROM crm_onov A '.$leff.' WHERE A.gid=\''.$P['gid'].'\' LIMIT 1',array(1=>'Error obteniendo información guardada',2=>'No se guardo..'));
	return $q;
}
static public function nov_post($P=array(),$P2=array()){
	$err1=($P2['err1'])?$P2['err1'].': ':'Error generando documento: ';
	$profile=$P['profile']; unset($P['profile']);
	if($P['calId']>0){ $P['onCal']='Y'; } else{ $P['onCal']='N'; }
	$P['doDateAt']=substr($P['doDate'],11,5);
	$P['endDateAt']=substr($P['endDate'],11,5);
	
	$ins=a_sql::insert($P,array('table'=>'crm_onov','kui'=>'uid_dateC','qDo'=>'insert','updater'=>'uid_dateC'));
	if($ins['err']){ $js=_js::e(3,$err1.$ins['err']['error_sql']); _err::err($js); }
	else{
		$P['gid']=$ins['insertId'];
		$P['profile']=$profile;
		$q=self::nov_gOne($P);
		if(!a_sql::$err){ $P=$q; }
		sendNtyAssg($P,array());
		return $P;
	}
}
static public function nov_put($P=array()){
	$qA=a_sql::fetch('SELECT C.userAssg FROM crm_onov C WHERE gid=\''.$P['gid'].'\' LIMIT 1');
	$profile=$P['profile']; unset($P['profile']);
	if($P['calId']>0){ $P['onCal']='Y'; } else{ $P['onCal']='N'; }
	$P['doDateAt']=substr($P['doDate'],11,5);
	$P['endDateAt']=substr($P['endDate'],11,5);
	$ins=a_sql::insert($P,array('table'=>'crm_onov','qDo'=>'update','updater'=>'uid_dateC','wh_change'=>'WHERE gid=\''.$P['gid'].'\' LIMIT 1'));
	if($ins['err']){ $js=_js::e(3,'Error actualizando novedad: '.$ins['err']['error_sql']);  _err::err($js); }
	else{
		$P['profile']=$profile;
		$q=self::nov_gOne($P);
		if(!a_sql::$err){ $P=$q; }
		sendNtyAssg($P,$qA);
		return $P;
	}
}
static public function nov_get($P=array(),$P2=array()){
	$obj=$P['obj']; unset($P['obj']);
	$M=array();
	$ordBy='';
	$wh='';
	if($P['whText']){ $wh .=' AND ('.$P['whText'].') '; };
	$errs=array();
	$errs[1]=($P2[1])?$P2[1]:'Error obteniendo listado';
	$errs[2]=($P2[2])?$P2[2]:'No se encontraron resultados';
	$profile=$P['profile']; unset($P['profile']);
	if($profile=='cpr'){/* Permisos con base de responsable ventas */
		$wh .= a_ses::ousp('slps',array('tbA'=>'C'));
		$P['prsId']=($P['prsId'])?$P['prsId']:-1;
		$P['wh']['A.prsId']=$P['prsId'];
		$M=a_sql::fetch('SELECT A.prsId,A.name,C.cardId,C.cardName FROM par_ocpr A 
		LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
		WHERE A.prsId=\''.$P['prsId'].'\' LIMIT 1');
	}
	else if($profile=='crd'){/* Permisos con base de responsable ventas */
		$wh .= a_ses::ousp('slps',array('tbA'=>'C'));
		$P['cardId']=($P['cardId'])?$P['cardId']:-1;
		$P['wh']['A.cardId']=$P['cardId'];
		$M=a_sql::fetch('SELECT C.cardId,C.cardName FROM par_ocrd C 
		WHERE C.cardId=\''.$P['cardId'].'\' LIMIT 1');
	}
	else if($profile=='task' || $profile=='event'){/* Permisos con base a yo o asignado */
		$wh .=a_ses::ousp('users2',array('tbA'=>'A'));
	}
	$wh .= a_sql_filtByT($P['wh']);
	$M['L']=array();
	$fie='A.gid,A.obj,A.title,A.oType,A.oClass,A.status,A.userId,A.dateC,A.userUpd,A.dateUpd';
	$fie.=($P['_fie'])?','.$P['_fie']:'';
	$le='';
	if($profile=='nov'){
		$fie.=',A.prsId,P.name,C.cardId,C.cardName';
		$le ='LEFT JOIN par_ocpr P ON (P.prsId=A.prsId) 
		LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)';
	}
	else if($P2['prsLeft']){
		$fie.=',A.prsId,P.name,C.cardId,C.cardName';
		$le ='LEFT JOIN par_ocpr P ON (P.prsId=A.prsId) LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)';
	}
	$JoinTb='';
	if($P2['Join']){
		$fie .=','.$P2['Join'][0];
		$JoinTb=$P2['Join'][1];
	}
	$qu='SELECT '.$fie.'
	FROM crm_onov A '.$le.' 
	'.$JoinTb.' 
	WHERE 1 '.$wh.' '.$ordBy.a_sql::nextLimit();
	$q=a_sql::query($qu,$errs);
	if(a_sql::$err){ $M['L']=json_decode(a_sql::$errNoText,1); }
	else{ 
		while($L=$q->fetch_assoc()){
			if($P['repeatKey']){
				$K=dateCicle::calc(array('f'=>$L['repeatKey'],'iniDate'=>$L['endDate']));
				$L['nextDate']=$K['nextDate'];
			}
			$M['L'][]=$L;
		}
	}
	$js=_js::enc($M);
	return $js;
}
static public function nov_one($P=array()){
	$fie='A.gid,A.obj,A.title,A.oType,A.oClass,A.prsId,P.name,C.cardId,C.cardName';
	$fie .=($P['_fie'])?','.$P['_fie']:'';
	if($P['__fields']){ $fie .=','.$P['__fields']; }
	$M=a_sql::fetch('SELECT '.$fie.'
	FROM crm_onov A
	LEFT JOIN par_ocpr P ON (P.prsId=A.prsId)
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
	WHERE gid=\''.$P['gid'].'\' LIMIT 1',array(1=>'Error obteniendo información.',2=>'La información de gid:'.$P['gid'].', no existe.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{
		$M['iL']=array();
		if($M['obj']=='event'){
			$q=a_sql::query('SELECT A.id,A.lineType,A.oid,A.email,P.name
			FROM crm_oaev A 
			LEFT JOIN par_ocpr P ON (P.prsId=oid)
			WHERE A.gid=\''.$P['gid'].'\' LIMIT 20',array(1=>'Error obteniendo invitados al evento'));
			if(a_sql::$err){ $M['iL']=json_decode(a_sql::$errNoText,1); }
			else if(a_sql::$errNo==-1){
				while($L=$q->fetch_assoc()){
					if($L['lineType']=='prs'){ $L['line1']=$L['name']; unset($L['name']); }
					$M['iL'][]=$L;
				}
			}
		}
		$js=_js::enc2($M);
	}
	return $js;
}
/* Events */
static public function events_invSave($Ls=array(),$P=array()){
	$errs=0;
	if($js=_js::ise($P['gid'],'Id de novedad no definido','numeric>0')){ $errs++;}
	else if(is_array($Ls)){
		foreach($Ls as $n => $L){ $fro=' ('.$L['id'].'-'.$L['email'].')';
			$revisa=(!array_key_exists('id',$L));
			if($revisa && ($L['lineType']!='free' && !$L['oid'])){ $js=_js::e(3,'Se debe definir oid para relacionar invitado.'.$fro); $errs++; break; }
			else if($revisa && $js=_js::ise($L['email'],'Email no definido correctamente.'.$fro)){ $errs++; break; }
			else{
				$Pd=array('table'=>'crm_oaev','qDo'=>'insert');
				if($L['delete']=='Y'){
					$Pd['qDo']='delete';
					$Pd['wh_change']='WHERE id=\''.$L['id'].'\' LIMIT 1';
					
				}
				else if($L['id']){ continue; }/* omitir cambios */
				$L['gid']=$P['gid'];
				$ins=a_sql::insert($L,$Pd);
				if($ins['err']){ $js=_js::e(3,'Error realizando invitación'); $errs++; break; }
			}
		}
	}
	if($errs){ _err::err($js); }
}
static public function events_invSend($D=array()){
	/* $D= es query de nov */
	$q=a_sql::query('SELECT id,email FROM crm_oaev WHERE gid=\''.$D['gid'].'\' AND inviteSend=\'N\' ',array('Error obteniendo correos para notificación.'));
	if(a_sql::$err){ _err::err($js); }
	else if(a_sql::$errNo==2){  }
	else{
		_ADMS::lib('_File');
		_ADMS::_lb('lb/_Mailer,sql/filter,com/_2d');
		$Dte=array('eventName'=>$D['title']);
		$Dte['eventDate']=_2d::dateLong($D['doDate'],$D['endDate']);
		$Dte['eventLocation']=$D['wLocation'];
		$Dte['ocardName']=a_ses::$ocardCode;
		_ADMS::lib('Mailing');
		/* por arreglar */
		$temp = _File::tempFile(Mailing::ics_create(array('date1'=>$D['doDate'],'date2'=>$D['endDate'],'title'=>$D['title'],'cn'=>'John Romero','mailto'=>'jromdev@gmail.com','location'=>$D['wLocation'])),'guardarencalendario'.a_ses::$userId.time().'.ics');
		while($L=$q->fetch_assoc()){
			#/*
			$s=sendInvEvent(array('to'=>$L['email'],'C'=>$Dte,'_Fi'=>array($temp)));
			if($s['errNo']){ _err::err('No se envió el correo a los destinatarios ('.$L['email'].'). '.$s['text'],3); break; }
			else{ $upd=a_sql::query('UPDATE crm_oaev SET inviteSend=\'Y\' WHERE id=\''.$L['id'].'\' LIMIT 1 '); }
			continue; #*/
			$Ds=array(); #reset cada linea
			$Ds['body']=Mailing::fromTemp(array('tCode'=>'crmEventInv'),$Dte,array('encodeHTML'=>'N'));;
			if(_err::$err){ break; }
			$vp=array('from'=>'info@admsistems.com','to'=>$L['email'],'subject'=>'Invitación: '.$D['title'],'html'=>$Ds['body'],'_Fi'=>array($temp));
			$Ds['asunt'] = $D['title'];
			$Ds['mailTo']=$L['email'];
			$Ds['fromName'] = 'Invitación a Evento';
			$Ds['fromEmail'] = 'einvitacion@admsistems.com';
			$s = _Mail::send($Ds,array('F'=>array($temp)));
			if($s['errNo']){ _err::err('No se envió el correo a los destinatarios ('.$L['email'].'). '.$s['text'],3); break; }
			else{ $upd=a_sql::query('UPDATE crm_oaev SET inviteSend=\'Y\' WHERE id=\''.$L['id'].'\' LIMIT 1 '); }
		}
		_File::del($temp);
	}
}
}
require(_0s::$uriReq);
?>