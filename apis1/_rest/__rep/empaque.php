<?php
$ALM=$Fas=array();

/* Definir bodegas y fases */
$R = [
	'FIE'=>array('periodo' => 'Mes', 'boxes' => 'Cajas', 'quantity_number' => 'Unidades Totales'),
	'L'=>array()
];
$date1 =$___D['FIE'][0]['A.docDate(E_mayIgual)'];
$date2 = $___D['FIE'][1]['A.docDate(E_menIgual)'];
$whsId = $___D['FIE'][2]['A.whsId'];;
$wh = '';
if($whsId) {
	$wh .= ' AND A.whsId="'.$whsId.'"';
}
$qA=a_sql::query('SELECT A.docDate,A.docEntry,B.detail,SUM(B.quantity) quantity
FROM gvt_odlv A JOIN gvt_dlv1 B ON (B.docEntry = A.docEntry)
WHERE A.docStatus="C" AND A.docDate BETWEEN "'.$date1.'" AND "'.$date2.'" '.$wh.'
GROUP BY A.docDate,docEntry,B.detail
',array(1=>'Error consultando la informacion',2=>'No se encontraron resultados'));
if(a_sql::$err){ die(a_sql::$errNoText); }
else{
	while($L=$qA->fetch_assoc()){
	$mes = substr($L['docDate'],0,7);
	if (! $R['L'][$mes]){
		$R['L'][$mes] = [
			'periodo' => $mes,
			'boxes' => 0,
			'quantity_number' => 0,
		];
	}
	$R['L'][$mes]['boxes']++;
	$R['L'][$mes]['quantity_number'] += $L['quantity']; 
}
	$js = json_encode($R);
}

echo $js;
?>