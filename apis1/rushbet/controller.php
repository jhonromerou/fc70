<?php
require('../../../_inicnf.php');
require '../phpbase2.php';
_ADMS::lib('_err,_js,a_sql');
header('Content-Type:text/html');

c::$Sql2['db']='apdep';
a_sql::dbase(c::$Sql2);
/* mysql */
class JADEP{
  static $VP=[];
  static $VS=[];
  static $TMcolorGoals=[
    17=>['s'=>'#2ECC71','c'=>'más de 16.5'],
    18=>['s'=>'#F1C40F','c'=>'más de 17.5'],
    19=>['s'=>'#E74C3C','c'=>'más de 18.5'],
    20=>['s'=>'#2874A6','c'=>'más de 19.5'],
  ];
  static public function TMcolorGoals($kb=0,$r=false){
    $ret='';
    if($kb>=20){ $k=20; }
    else if($kb>=19){ $k=19; }
    else if($kb>=18){ $k=18; }
    else if($kb>=17){ $k=17; }
    else if($kb>=16){ $k=16; }
    else if($kb>=15){ $k=15; }
    else if($kb>=14){ $k=14; }
    if(self::$TMcolorGoals[$k]){ $ret= self::$TMcolorGoals[$k]['s']; }
    if($r && $ret!=''){ return self::$TMcolorGoals[$k]; }
    return $ret; 
  }

  static public function VS($D=[]){
    self::$VS=a_sql::fetch('SELECT vsId,sport,playerA,playerB
    FROM g_vs 
    WHERE sport=\'TM\' AND (playerA=\''.$D['playerA'].'\' OR playerB=\''.$D['playerB'].'\') ',[1=>'Error verificando enfrentamiento']);
    if(a_sql::$err){ _err::err(a_sql::$errNoText); }
    else if(a_sql::$errNo==2){
      $D['dateUpd']=date('Y-m-d H:i:s');
      $vsID=a_sql::qInsert($D,['tbk'=>'g_vs']);
      if(a_sql::$err){ _err::err('Error generando enfrentamiento: '.a_sql::$errText,3); }
      else{ $D['vsId']=$vsID; self::$VS=$D;
      }
    }
    return self::$VS;
  }
  static public function posibStyle($por=0){ //
    $style='';
    if($por>=85){  $style= 'background-color:green'; }
    else if($por>50 && $por<=80){ $style= 'background-color:yellow'; }
    else if($por<=40){ $style= 'background-color:purple'; }
    else if($por<=50){ $style= 'background-color:red'; }
    return $style;
  }
  // apis
  static public function betsApi($urlPath='',$g='matches'){
    $urlPath=str_replace('https://es.betsapi.com/r/','',$urlPath);
    if($g=='matches'){ $url='https://es.betsapi.com/rh2h/'.$urlPath; }
    else if($g=='goals'){ $url='https://es.betsapi.com/r/'.$urlPath; }
    $ch = curl_init($url); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, true); 
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
  }
  

  static public function TMgoals($urlPath){
    //return '<h5>Resultados pausados</h5>';
    $response=limpiarNS(JADEP::betsApi($urlPath,'goals'));
    $start = stripos($response, '<h3 class="card-title">Pts</h3></div><table class="table">')*1;
    $end = stripos($response,'</table></div>');
    $body = substr($response,$start,$end-$start);
    $RV=['name'=>'Player','P1'=>0,'P2'=>0,'P3'=>0,'P4'=>0,'P5'=>0,'T'=>0,'PR'=>0];
    $TB=HTMLTable($body,$RV);
    $botRows='';
    $tb='<table class="table_zh">
    <thead><tr>
    <td>Jugador</td> <td>P1</td> <td>P2</td> <td>P3</td> <td>P4</td> <td>P5</td> <td>Total</td> <td>Promedio</td></tr>
    </thead>
    <tbody>';
    $T=['P1'=>0,'P2'=>0,'P3'=>0,'P4'=>0,'P5'=>0,'T'=>0,'PR'=>0];
    foreach($TB as $n=>$PL){
      $parts=0; $total=0;
      if($PL['P1']>0){ $parts += 1; $total +=$PL['P1']; $T['P1']+=$PL['P1']; }
      if($PL['P2']>0){ $parts += 1; $total +=$PL['P2']; $T['P2']+=$PL['P2']; }
      if($PL['P3']>0){ $parts += 1; $total +=$PL['P3']; $T['P3']+=$PL['P3']; }
      if($PL['P4']>0){ $parts += 1; $total +=$PL['P4']; $T['P4']+=$PL['P4']; }
      if($PL['P5']>0){ $parts += 1; $total +=$PL['P5']; $T['P5']+=$PL['P5']; }
      $prom=round($total/$parts);
      $T['T'] += $total;
      $T['PR'] += $prom;
      $tb .= '<tr>
      <td>'.$PL['name'].'</td>
      <td>'.$PL['P1'].'</td>
      <td>'.$PL['P2'].'</td>
      <td>'.$PL['P3'].'</td>
      <td>'.$PL['P4'].'</td>
      <td>'.$PL['P5'].'</td>
      <td>'.$total.'</td>
      <td>'.$prom.'</td>
      </tr>';
    }
    $tb .='<tr>
    <td>Totales--</td>';
    foreach($T as $k=>$goals){
      $kC=self::TMcolorGoals($goals,true);
      $sty=''; $txt='';
      if($kC && $kC['s']){ $sty=$kC['s'];
        $botRows .='<span class="badge" style="background-color:'.$sty.'">'.$kC['c'].'</span>';
      }
      $tb .='<td style="background-color:'.$sty.'">'.$goals.'</td>';
    }
    $tb .='</tr>';
    $tb .='</tbody></table>
    '.$botRows;
    return $tb;
  }
}

/* Resultados de tenis de mes */
JADEP::$VP['TM']=[
  '3-0'=>['c'=>'3-0'],
  '3-1'=>['c'=>'3-1'],
  '3-2'=>['c'=>'3-2']
];

/* end mysql */
function limpiarNS($txt='',$tags=false){
  $txt=preg_replace('/\n/','',$txt);
  $txt=preg_replace('/\s\s+/','',$txt);
  if($tags) return strip_tags($txt);
  return ($txt);
}

function HTMLTable($html='',$RV=[]){
  //THEAD
  $TB=[];
  //$TB[]=$RV;
  preg_match_all("/<tr.*?>(.*?)<\/[\s]*tr>/s", $html, $Trs);
  $n=-1;
  foreach($Trs[1] as $nx=>$tr){ $n++;
    if($n==0){ continue; } //saltar head
    preg_match_all("/<td.*?>(.*?)<\/[\s]*td>/s", $tr, $Tds);
    $TB[$n]=$RV;
    $n2=0; $parts=0;
    foreach($RV as $kn=>$val){
      if($Tds[1][$n2]){ 
        $TB[$n][$kn]=$Tds[1][$n2];
      }
      $n2++;
    }
  }
  return $TB;
}

function datos($html){
  preg_match_all("/<tr.*?>(.*?)<\/[\s]*tr>/s", $html, $Trs);
  $M=[
    '<>'=>[
      '>3.5'=>0,
      '>4.5'=>0,
      '<3.5'=>0,
      '<4.5'=>0,
    ],
    'M'=>[
      '3-0'=>0,
      '3-1'=>0,
      '3-2'=>0,
      '0-3'=>0,
      '1-3'=>0,
      '2-3'=>0,
    ],
    'games'=>count($Trs[1]),
    $_GET['L']=>0,
    $_GET['V']=>0,
    'TB'=>[]
  ];
  foreach($Trs[1] as $n=>$tr){
    preg_match_all("/<td.*?>(.*?)<\/[\s]*td>/s", $tr, $Tds);
    // [a, fecha, a vs b, W-L, 3-2]
    $gamers=strip_tags(str_replace("</span>v","</span>-vs-",limpiarNS($Tds[1][2])));
    $gamers=explode('-vs-',$gamers);
    $L_VN=$gamers[0];
    $L_VN2=$gamers[1];
    //marcador
    $score=limpiarNS($Tds[1][4],1);
    $sep=explode('-',$score);
    /* Gano L? */
    $winner=($sep[0]>$sep[1] )?$L_VN:$L_VN2;
    $divGame=limpiarNS($Tds[1][4]);
    $urlGame=preg_replace('/.*href\=\"\/r\/(.*)\".*\<\/a>/','$1',$divGame);
    $M['TB'][]=[
      'games'=>($sep[0]*1+$sep[1]*1),
      'date'=>$Tds[1][1],
      'L'=>$L_VN,
      'V'=>$L_VN2,
      'W'=>$winner,
      'score'=>$score,
      'scoreL'=>$sep[0],
      'scoreV'=>$sep[1],
      $gamers[0]=>$sep[0],
      $gamers[1]=>$sep[1],
      'eventId'=>preg_replace('/.*\/r\/(\d+)\/.*/','$1',$divGame),
      'urlGame'=>$urlGame
    ];
  }
  return $M;
}

ini_set('display_errors','on');
ini_set('error_reporting','E_ALL');
?>