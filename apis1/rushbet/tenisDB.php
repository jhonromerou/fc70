<?php
require 'controller.php';
if(!isset($_GET['vsId'])){
  $q=a_sql::query('SELECT * FROM g_vs ');
  $html ='<table class="table_zh">
  <thead> <tr> <td>Deporte</td> <td>Player A</td> <td>Player B</td> </tr> </thead>
  <tbody>';
  while($L=$q->fetch_assoc()){
    $html .= '<tr>
    <td>'.$L['sport'].'</td>
    <td><a href="?vsId='.$L['vsId'].'">'.$L['playerA'].'</a></td>
    <td>'.$L['playerB'].'</td>
    </tr>';
  }
  $html .='</tbody></table>';
  echo $html; $html='';
}
else{  //draw
  $VS=a_sql::fetch('SELECT A.vsId,A.playerA, A.playerB
  FROM g_vs A
   WHERE A.vsId=\''.$_GET['vsId'].'\' LIMIT 1',[1=>'Error obteniendo enfrentamiento ',2=>'El enfrentamiento no está registrado']);
  if(a_sql::$err){ die(a_sql::$errNoText); }
  $pa=$VS['playerA']; $pb=$VS['playerB'];
  $M=[
    'games'=>0,
    $pa=>0,
    $pb=>0,
    '<>'=>[
      '>3.5'=>0,
      '>4.5'=>0,
      '<3.5'=>0,
      '<4.5'=>0,
    ],
    'M'=>[
      '3-0'=>0,
      '3-1'=>0,
      '3-2'=>0,
      '0-3'=>0,
      '1-3'=>0,
      '2-3'=>0,
    ],
    'TB'=>[]
  ];
  $q=a_sql::query('SELECT B.*  FROM g_score B
  WHERE B.vsId=\''.$VS['vsId'].'\' LIMIT 30',[1=>'Error obteniendo historico de partidas',2=>'No se encontraron partidas']);
  if(a_sql::$err){ die(a_sql::$errNoText); }
  $lastGoals=0; $limitGoals=5;
  $tbGoals='';
  while($L=$q->fetch_assoc()){
    $M['games'] +=1;
    if($L['winner']=='A'){ $M[$pa] +=1; }
    else { $M[$pb] +=1; }
    $sep=explode('-',$L['score']);
    $kScore='x';
    if($sep[0]+$sep[1]==3){ $kScore='3-0'; }
    else if($sep[0]+$sep[1]==4){ $kScore='3-1'; }
    else if($sep[0]+$sep[1]==5){ $kScore='3-2'; }

    $M['M'][$kScore] +=1;
    // under 
    if($L['games']>=4){ $M['<>']['>3.5'] += 1; }
    if($L['games']>4){ $M['<>']['>4.5'] += 1; }
    if($L['games']<4){ $M['<>']['<3.5'] += 1; }
    if($L['games']<=4){ $M['<>']['<4.5'] += 1; }
    /* Partidas */
    if($lastGoals<=$limitGoals){
      $tbGoals .=JADEP::TMgoals($L['urlGame']);
      sleep(0.5);
    } $lastGoals++;

    $M['TB'][]=[
      'games'=>$L['games'],
      'date'=>$L['docDate'],
      'L'=>(($L['playerAis']=='L')?$VS['playerA']:$VS['playerB']),
      'V'=>(($L['playerAis']=='V')?$VS['playerA']:$VS['playerB']),
      'W'=>(($L['winner']=='A')?$VS['playerA']:$VS['playerB']),
      'Score'=>$L['score']
    ];
  }
  //echo '<pre>'.print_r($M,1).'</pre>';
echo '<link rel="stylesheet" href="http://gstatic1.admsistems.com/_css/resetv3.css" />
<link rel="stylesheet" href="http://gstatic1.admsistems.com/_css/adms_icon.css" />
<link rel="stylesheet" href="http://gstatic1.admsistems.com/_css/cadmin.css" />
<style>
body { padding:3px 3px 3px 18px;  }
.htitle {background-color:yellow; font-size:16px; padding:3px 0; margin:8px 0; }
._divTabla { display:table; }
._divTablaTr { display:table-cell; width:45%; padding-right:50px; border-right:12px solid red; }
.gamer-L{ color:#cd201f !important }
.gamer-V{ color:#45aaf2 !important }
.concepto { font-weight:bold;  background-color:#5eba00 !important; }
</style>
<ul><b>Indices</b>
  <li><a href="#game">Cuotas del Partido</a></li>
  <li><a href="#games">Total Juegos [>3.5 <4.5]</a></li>
  <li><a href="#scores">Resultado correcto 3-x</a></li>
  <li><a href="#handicaps">Handicaps</a></li>
  <li><a href="#goals">Total Puntos</a></li>
  <li><a href="#lastMatches">Ultimos Enfrentamientos</a></li>
  <li><a href="#1x2">Doble Oportunidad 1X, 1o2, X2</a></li>
</ul>

<h5 class="htitle"><a name="game">cuota Partido</a></h5>
<table class="table_zh">
<thead>
<tr>
  <td>Cuota</td> <td>%</td>
  <td class="gamer-L">'.$VS['playerA'].'</td>
  <td class="gamer-V">'.$VS['playerB'].'</td>
  <td>Cuota</td> <td>%</td>
</tr>
</thead>
<tbody>';
$posib=round($M[$pa]/$M['games']*100,1);
$cuota=($posib>0)?round(1/($posib/100),2):0;
$posib2=round($M[$pb]/$M['games']*100,2);
$cuota2=($posib2>0)?round(1/$posib2*100,1):0;
echo '
<tr>
  <td>'.$cuota.'</td>
  <td style="'.JADEP::posibStyle($posib).'">'.$posib.'%</td>
  <td>'.$M[$pa].'</td>
  <td>'.$M[$pb].'</td>
  <td>'.$cuota2.'</td>
  <td style="'.JADEP::posibStyle($posib2).'">'.$posib2.'%</td>
</tr>
</tbody>
</table>';

//over - under
$X_UNDER=[
  '<3.5'=>['c'=>'Menos de 3.5','desc'=>'3 Juegos'],
  '<4.5'=>['c'=>'Menos de 4.5','desc'=>'3-4 Juegos'],
  '>3.5'=>['c'=>'Más de 3.5','desc'=>'4-5 Juegos'],
  '>4.5'=>['c'=>'Más de 4.5','desc'=>'5 Juegos'],
];
echo '<h5 class="htitle">Totales</h5>
<div class="_divTabla">
<div class="_divTablaTr">
<h5 class="htitle"><a name="games">Juegos Totales</a></h5>
<table class="table_zh">
<thead>
<tr>
<td class="concepto">Concepto</td><td>-h</td> <td>%</td>  <td>Cuota</td>
</tr>
<tbody>';
  foreach($X_UNDER as $k=>$D){
    $posib=round($M['<>'][$k]/$M['games']*100,1);
    $cuota=($posib>0)?(round(1/($posib/100),2)):0;
    echo '<tr>
    <td class="concepto">'.$D['c'].'</td>
    <td>'.$D['desc'].'</td>
    <td style="'.JADEP::posibStyle($posib).'">'.$posib.'%</td>
    <td>'.$cuota.'</td>
    </tr>';
  }
echo '</tbody>
</table>
</div>';

echo '<div class="_divTablaTr">
<h5 class="htitle"><a name="scores">Resultado correcto</a></h5>
<div><b>% Base</b>:16.6%, <b>Cuota base</b>:6</div>
<table class="table_zh">
<thead>
<tr>
<td class="concepto">Concepto</td> <td>%</td> <td>-h</td> <td>Cuota</td>
</thead>
<tbody>';
foreach(JADEP::$VP['TM'] as $k=>$L){
  $posib=round(($M['M'][$k]/$M['games'])*100,1);
  $cuota=($posib>0)?round(1/$posib*100,2):0;
  echo '<tr>
  <td class="concepto">'.$L['c'].'</td> 
  <td style="'.JADEP::posibStyle($posib).'">'.$posib.'%</td>
  <td>'.$M['M'][$k].' de '.$M['games'].'</td>
  <td>'.$cuota.'</td>
</tr>';
}
echo '</tbody></table>
</div>
</div>';

echo '<h5 class="htitle">Puntacion Partidos</h5>
<div class="_divTabla">
<div class="_divTablaTr">'.$tbGoals.'</div>
</div>';

/* Proyecciones puntaje */
$xOver=[//Min resultado
  '>16.5'=>[11,6,17],
  '>17.5'=>[11,7,18],
  '>18.5'=>[11,8,19],
  '>19.5'=>[11,9,20]
  ];
  $xUnder=[
  '<15.5'=>[11,4,15],
  '<16.5'=>[11,5,16,],
  '<17.5'=>[11,6,17],
  '<18.5'=>[11,7,18],
  '<19.5'=>[11,8,19],
  ];

$color2=['#2874A6','#E74C3C','#F1C40F','#2ECC71','#633974','#DC7633','#85929E'];

$tbOver=$tbUnder='';
foreach($xOver as $co=>$P){
  $tbOver .= '<tr>
  <td>'.$co.'</td>
  <td style="background-color:yellow;">'.($P[0]+$P[1]).'</td>';
  for($pierde=$P[1]; $pierde<=9; $pierde++){
    $sty=JADEP::TMcolorGoals($P[2]);
    $tbOver .='<td style="background-color:'.$sty.'">'.$P[0].'-'.$pierde.'</td>';
    $nc++; if($nc>6){ $nc=0; }
  }
  //12-10, 13-10
  $tbOver .= '<td style="background-color:#EEE">-</td>';
  for($gana=12; $gana<=16; $gana++){
    $tbOver .='<td style="background-color:'.$color2[$nc].'">'.$gana.'-'.($gana-2).'</td>';
    $nc++; if($nc>6){ $nc=0; }
  }
  $tbOver .='</tr>';
}
foreach($xUnder as $co=>$P){
  $tbUnder .= '<tr>
  <td>'.$co.'</td>
  <td style="background-color:yellow;">'.($P[0]+$P[1]).'</td>';
  for($pierde=$P[1]; $pierde>=0; $pierde--){
    $tbUnder .= '<td style="background-color:'.$color2[$nc].'">'.$P[0].'-'.$pierde.'</td>';
    $nc++; if($nc>6){ $nc=0; }
  }
  $tbUnder .= '</tr>';
}
  
echo '
<h5 class="htitle">Proyeccion de Puntajes</h5>';
foreach(JADEP::$TMcolorGoals as $goals =>$X){
  echo '<span class="badge" style="background-color:'.$X['s'].'">(>'.$goals.') '.$X['c'].'</span>';
}
echo '<div class="_divTabla">
<div class="_divTablaTr"><h5>Over</h5>
  <table class="table_zh"><tbody>'.$tbOver.'</tbody></table>
  </div>
  <div class="_divTablaTr"><h5>Under</h5>
  <table class="table_zh"><tbody>'.$tbUnder.'</tbody></table>
  </div>
</div>
';
echo '<h4 class="htitle"><a name="lastMatches">Ultimos <b>'.$M['games'].' enfrentamientos</a></h4>
<table class="table_zh">
<thead>
<tr>
  <td>%</td> <td class="concepto">Concepto</td> <td>Juegos</td> <td>Marcador</td> <td>Ganador</td> <td>Local</td> <td>Visitante</td>
</tr>
</thead>
<tbody>'; 
foreach($M['TB'] as $n=>$L){
  $k=$L['Score'];
  $posib=round(($M['M'][$k]/$M['games'])*100,1);
  $clsW=($L['W']==$L['L'])?' class="gamer-L"' :' class="gamer-V"';
  $under='';
  if($L['games']==3){ $under= '(1) '.$X_UNDER['<3.5']['c']; }
  if($L['games']>=4){ $under .= ' (2) '.$X_UNDER['>3.5']['c']; }
  if($L['games']<5){ $under .= ' (3) '.$X_UNDER['<4.5']['c']; }
  if($L['games']==5){ $under .= ' (4) '.$X_UNDER['>4.5']['c']; }
  echo '<tr>
  <td style="'.JADEP::posibStyle($posib).'">'.$posib.'%</td>
  <td class="concepto">'.$under.'</td>
  <td '.$clsW.'>'.$L['games'].'</td>
  <td '.$clsW.'>'.$L['Score'].'</td>
  <td '.$clsW.'>'.$L['W'].'</td>
  <td class="gamer-L">'.$L['L'].'</td>
  <td class="gamer-V">'.$L['V'].'</td>
</tr>';
}
echo '</tbody>
</table>
</div>';
//echo '<textarea style="width:95%; height:95%;">'.print_r($M,1).'</textarea>';
//echo '<textarea style="width:95%; height:95%;">'.$body.'</textarea>';

}
?>