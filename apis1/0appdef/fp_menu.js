
$M.liReset(true);
$M.iniSet={
nty:'N',help:'N',menu:'L',
mliDel:['sysreports']};

$M.liEdit([
{k:'nomCla',t:'Contratos (Altas/Bajas)'}
]);

$M.liTable([
{folId:'xGen',folName:'Gente',ico:'fa fa-users',folColor:'#007eff'},
{fatherId:'xGen',folId:'xGenTurn',folName:'Control Planta',MLis:['nomNov.turnOpen','nomNov.turn','nomNov.her','xGenCyp'], _F:[
	{folId:'a',folName:'Ausentismo',MLis:['nomNov.lic','nomNov.inc','nomNov.vac','nomNov.sus']},
	{mas:['jsv.xGeCypCls']},
	{rep:['nomNovRep.turn','nomNovRep.nov','nomNovRep.novHer']}
]},

{fatherId:'xGen',folId:'nomCrd',folName:'Empleados',ico:'fa fa-users',MLis:['nomCrd','nomCpr','JForm.p.nomCfr'],
	_F:[
	{mas:['tb.owsu','jsv.nomWorkArea','jsv.nomWorkPosi']},
	{frm:['JForm.forms.nomCfr']},
]},
{fatherId:'xGen',folId:'xGen1',folName:'Diseño',doc:['xGen11.presu','xGenEva']},
{fatherId:'xGen',folId:'xGen3',folName:'Reclutamiento',
doc:[/*'xGen31.cae','JForm.xGen33rindu',*/'nomCla','JForm.p.xGen33Cdp'],
frm:['JForm.forms.xGen33Cdp'],
mas:[/*'jsv.xGeCae',*/'jsv.nomLeaveRea','jsv.nomCapCls']
},
{fatherId:'xGen',folId:'xGenMeet',folName:'Reuniones',
_F:[{doc:['xGenTor.gente','xGenTor.seg']}]
},

/*Terceros*/
{folId:'crd',folName:'Terceros',ico:'fa fa-handshake-o',folColor:'#7eFFFF'},
	{fatherId:'crd',MLis:['crd.c','JCPro']},
/* Inventarios */
{folId:'ivt',folName:'Inventarios',ico:'fa fa-cubes',folColor:'#eeb000'},
	{fatherId:'ivt',_F:[
	{folId:'a',folName:'Articulos',MLis:['itm.p','itm.ai','itmAf','itmSub','itmSub.gr']},
	{doc:['ivtIng','ivtEgr']},
	{rep:['ivtStock.p','ivtStock.pHistory']},
	{folId:'Dot',folName:'Dotación'},
	{mdlActive:'ivtGes',folId:'Bit',folName:'Gestión Lotes',ico:'fa fa-leaf',folColor:'#0F0'},
	{mas:['tb.itmOwhs','jsv.itmGr']}
	]},
	{fatherId:'ivtDot',_F:[
		{doc:['ivtDotDoe','ivtDotDoi','ivtDotDos','ivtDotDoa']},
		{rep:['ivtStock.ai','ivtStock.ai.history']},
		{mas:['jsv.ivtDotDoeCls','jsv.ivtDotDocCls']},
	]},
	{mdlActive:'ivtGes',fatherId:'ivtBit', MLis:['ivtBitL']},
	{mdlActive:'ivtGes',fatherId:'ivtBit',_F:[
		{doc:['ivtBitO','ivtBitE','ivtBitI','ivtBitD']},
		{rep:['ivtBit.stock','ivtBit.stock.history','ivtBitRep.down']},
		{mas:['jsv.ivtBitDType']},
	]},

/* Seguridad */
{folId:'xSeg',folName:'Seguridad',ico:'fa fa-shield',folColor:'orange',MLis:['nomAil']},
	{fatherId:'xSeg',_F:[
		{doc:['JForm.docs.xGenCkPop']},
		{frm:['JForm.forms.xGenCkPop']},
		{mas:['jsv.nomAilClass']}
	]},
/* Produccion */
{folId:'wma',folName:'Producción',ico:'iBg iBg_produccion'},
	{fatherId:'wma',_F:[
		{folId:'Wpt',folName:'Partes de Trabajo',ico:'fa fa-tags',MLis:['wmaWpt.tickets','wmaWpt']},
		{mas:['wmaWop','jsv.wmaWopGr']},
	]},

{folId:'JDrive',folName:'Nube',ico:'fa fa-cloud',MLis:['JDrive.box','JDrive.i.sgc','JDrive.i.cardE'],_F:[
	{mas:['JDrive.box.folders']}
]},
	
{folId:'cnf',folName:'Configuración',ico:'fa fa-cog'},
{fatherId:'cnf',MLis:['cnf.meusr','cnf.mecrd']},
{fatherId:'cnf',_F:[
	{folId:'User',folName:'Usuarios',MLis:['cnf.ousr','cnf.ousp','cnf.ousa','cnf.repAssg']}
	]
},
/* interfaces */
{mdlActive:'itf',folId:'itf',folName:'Interfaces',ico:'fa fa-rocket',folColor:'red'},
{mdlActive:'itf',fatherId:'itf',_F:[
	{mdlActive:'itf',folId:'DT',folName:'Importaciones',ico:'fa fa-upload'}
]},
{mdlActive:'itf',fatherId:'itfDT',_F:[
	{mdlActive:'itf',folId:'Wma',folName:'Producción',MLis:['itfDT.itmMO']}
]}

]);

$M.kauTable([
{fatherId:'xGen',rootFolder:'Gente', L:['xGenEva','xGen11.presu','xGen31.cae','xGen33.rindu','xGen33.cdp','xGenTor.gente','xGenTor.seg','xGenCyp','xGenCkPop']
},
{fatherId:'xSeg',rootFolder:'Seguridad',L:['nomAil']},
{fatherId:'nom',rootFolder:'Nómina',
	L:['nomCrd','nomCpr','nomCfr','nomCla'],
	F:[
		{folName:'Novedades',L:['nomNov.turnOpen','nomNov.turn','nomNov.reg','nomNovRep']}
	]
},
{fatherId:'crd',rootFolder:'Socios de Negocios',ico:'fa fa-handshake-o',
	L:['crd.c','crdProy']
},
{fatherId:'itm',rootFolder:'Articulos',
	L:['itm.p','itmAi','ivtStockAi','itmAf'],
},
{fatherId:'ivt',rootFolder:'Inventarios',
	L:['ivtIng','ivtEgr','ivtStock.p','ivtStock.history','ivtBitO','ivtBitE','ivtBitI','ivtBitD','ivtBit.stock','ivtDotDoe','ivtDotES','ivtDotDoa']
},
{fatherId:'wma',rootFolder:'Producción', L:['wma.supersu','wmaWpt']},
{fatherId:'JDrive',rootFolder:'Nube', L:['JDrive.sumaster','JDrive.box','JDrive.i.cardE']},

{fatherId:'sudo',rootFolder:'Administración',
	L:['sysd.sumaster','sysd.supersu','admin.perms','JForm.forms']
}
]);