/*
1. Copy JPush-sw.js on root web
2. Call new JPush({pkey, fPushBlock, btnPushSubs});
*/
function JdeviceType(){
 var nave=navigator.userAgent.toLowerCase();
 var R={dType:'',dTypeOs:-1};
 if(nave.match(/Android/i)){ mob=true; R.dTypeOs=10; }
 else if(nave.match(/iPhone/i)){ mob=true; R.dTypeOs=21; }
 else if(nave.match(/iPod/i)){ mob=true; R.dTypeOs=22; }
 else if(nave.match(/iPad/i)){ mob=true; R.dTypeOs=23; }
 else if(nave.match(/BlackBerry/i)){ mob=true; R.dTypeOs=51; }
 else{
  mob=false; R.dTypeOs=9;
  if(nave.indexOf('chrome')){ R.dTypeOs=1; }
  else if(nave.indexOf('firefox')){ R.dTypeOs=2; }
  else if(nave.indexOf('opera')){ R.dTypeOs=3; }
 }
 if(mob){ R.dType='Mob'; }
 else{ R.dType='Nav'; }
 return R;
}
function JPush(P){
 var mData={nave:JdeviceType()}
 this.pkey=P.pkey;
 this.name=(P.sw)?P.sw:'./JPush-sw2.js';
 var sW=null;
 var isSubscribed=false;
 if('serviceWorker' in navigator && 'PushManager' in window){
  navigator.serviceWorker.register(this.name)
  .then(function(swReg){
   sW = swReg;
   console.log('Service Worker is registered');
  })
  .catch(function(error){
   console.error('Service Worker Error', error);
  });
 }
 else{ console.error('Push messaging is not supported'); }
 this.subscribe=function(Px){
  sW.pushManager.subscribe({
   userVisibleOnly: true,
   applicationServerKey: urlB64ToUint8Array(this.pkey)
  })
  .then(function(subscription) {
   console.log('User is subscribed:');
   if(Px.func){ mData.subs=subscription.toJSON();
    Px.func(mData);
   }
  })
  .catch(function(err) {
   console.log('Failed to subscribe the user: ', err);
  });
 }
 this.getStatus=function(Px){ /* verificar  */
  sW.pushManager.getSubscription()
  .then(function(subscription){
   isSubscribed = !(subscription === null);
   if(isSubscribed){ console.log('User IS subscribed yet.');
    mData.subs=subscription.toJSON();
    if(Px.fStatus){ Px.fStatus(mData); }
   }
   else{ console.log('User is NOT subscribed yet.'); }
   if(Notification.permission === 'denied'){
    console.log('Push Messaging Blocked.');
    if(Px.fPushBlock){ Px.fPushBlock(subscription); }
    return;
   }

  });
 }
 /* conf*/
}

function urlB64ToUint8Array(base64String) {
 const padding = '='.repeat((4 - base64String.length % 4) % 4);
 const base64 = (base64String + padding)
 .replace(/\-/g, '+')
 .replace(/_/g, '/');

 const rawData = window.atob(base64);
 const outputArray = new Uint8Array(rawData.length);
 for (let i = 0; i < rawData.length; ++i) {
   outputArray[i] = rawData.charCodeAt(i);
 }
 return outputArray;
}

/* Example */
function JPushIni(P){
 var JSW1=new JPush(P);
 if(P.btnPushSubs){
  var btn=document.querySelector(P.btnPushSubs);
  btn.addEventListener('click',(r)=>{ JSW1.getStatus(); });
 }
 if(P.btnStatus===0){
  var btn2=document.querySelector(P.btnStatus);
  btn2.addEventListener('click',(r)=>{ JSW1.getStatus(); });
 }
 return JSW1;
}
