
$Doc.a['gvtInv']={a:'gvtInv.view'};

Api.PTR = {o:'/sa/com1/itm/o', whs:'/sa/com1/itm/whs', wtd:'/sa/com1/itm/wtd'};

$M.s['bussPartner'] = {t:'Socios de Negocios',L:['crd.c','crd.s']}
$M.s['a03.form']={t:'Formularios', L:['a03.visit']};

$M.li['gvtInv.get'] ={t:'Ventas', func:function(){
	$M.Ht.ini({fieldset:true, func_filt:'gvtInv', topCont:$1.T.btnNew({fa:'fa_doc',textNode:' Nueva Factura de Venta', func:function(){ $M.to('gvtInv.form'); }}),
	func_pageAndCont:function(){ Gvt.Inv.get({}); } });
}};

$M.li['a03.visit']={t:'Visitas / Asesorias', func:function(){
	$M.Ht.ini({fieldset:true, btnNew:$1.T.btnNew({fa:'fa_doc',textNode:' Nuevo Registro de Visita', func:function(){ $M.to('a03.visit.form'); }}),
	func_pageAndCont:function(){ A03.Visit.get({}); } });
}};
$M.li['a03.visit.form']={t:'Visitas / Asesorias', func:function(){
$M.Ht.ini({func:function(){ $M.to('a03.visit.form'); }});
}};

A03={};

A03.Visit = {
form:function(P){
	var wrap = $1.t('div'); var Pa=$M.read();
	$Api.get({f:'/s/a03/forms.asesorias/A:o.byId', loadVerif:!Pa.fvaId, inputs:'fvaId='+P.fvaId,
	func:function(Jq){ 
		var D = Jq;
	var divLine = $1.t('div',{'class':'divLine'});
	var addNodes = $1.t('input',{type:'hidden','class':'jsFields searchChange_cardId',name:'cardId',value:D.cardId});
	var custTo = $1.T.divLinewx({divLine:true, wxn:'wrapx1', Label:{textNode:'Cliente'}, I:{tag:'input',type:'text','class':'jsFields searchChange_cardName',name:'cardName',readonly:'readonly',value:D.cardName},
		btnLeft:{func:function(){ $3.crd({A:'C'},wrap); } },
		addNodes:addNodes
		});
	divLine.appendChild(custTo);
	D.doTime = (D.doTime) ? D.doTime : '';
	D.endTime = (D.endTime) ? D.endTime : '';
	D.fiObjetivos = (D.fiObjetivos) ? D.fiObjetivos : '';
	D.fiActividades = (D.fiActividades) ? D.fiActividades : '';
	D.fiCompEmpresa = (D.fiCompEmpresa) ? D.fiCompEmpresa : '';
	D.fiCompAsesor = (D.fiCompAsesor) ? D.fiCompAsesor : '';
	D.fiConclusion = (D.fiConclusion) ? D.fiConclusion : '';
	wrap.appendChild(divLine);
	var divLine = $1.t('div',{'class':'divLine'});
	var wxn = $1.t('div',{'class':'wrapx2'});
	wxn.appendChild($1.t('label',{textNode:'Fecha'}));
	wxn.appendChild($1.t('input',{type:'date','class':'jsFields',name:'doDate',value:D.doDate}));
	divLine.appendChild(wxn);
	var wxn = $1.t('div',{'class':'wrapx4'});
	wxn.appendChild($1.t('label',{textNode:'Hora Entrada'}));
	var hourAt = $1.t('input',{type:'text','class':'jsFields',name:'doTime',value:D.doTime});
	hourAt.onfocus = hourAt.onclick = function(){ ADMS_Date.Sel.hour(this); }
	wxn.appendChild(hourAt);
	divLine.appendChild(wxn);
	var wxn = $1.t('div',{'class':'wrapx4'});
	wxn.appendChild($1.t('label',{textNode:'Hora Salida'}));
	var hourAt = $1.t('input',{type:'text','class':'jsFields',name:'endTime',value:D.endTime});
	hourAt.onfocus = hourAt.onclick = function(){ ADMS_Date.Sel.hour(this); }
	wxn.appendChild(hourAt);
	divLine.appendChild(wxn);
	wrap.appendChild(divLine);
	var divLine = $1.t('div',{'class':'divLine'});
	var wxn = $1.t('div',{'class':'wrapx1'});
	wxn.appendChild($1.t('label',{textNode:'1. Objetivos'}));
	wxn.appendChild($1.t('textarea',{'class':'input jsFields',name:'fiObjetivos',style:'font-size:12px; height:90px;',textNode:D.fiObjetivos}));
	divLine.appendChild(wxn);
	wrap.appendChild(divLine);
	var divLine = $1.t('div',{'class':'divLine'});
	var wxn = $1.t('div',{'class':'wrapx1'});
	wxn.appendChild($1.t('label',{textNode:'2. Actividades Realizadas'}));
	wxn.appendChild($1.t('textarea',{'class':'input jsFields',name:'fiActividades',style:'font-size:12px; height:90px;',textNode:D.fiActividades}));
	divLine.appendChild(wxn);
	wrap.appendChild(divLine);
	var divLine = $1.t('div',{'class':'divLine'});
	var wxn = $1.t('div',{'class':'wrapx2'});
	wxn.appendChild($1.t('label',{textNode:'3.1 Compromiso Empresa'}));
	wxn.appendChild($1.t('textarea',{'class':'input jsFields',name:'fiCompEmpresa',style:'font-size:12px; height:90px;',textNode:D.fiCompEmpresa}));
	divLine.appendChild(wxn);
	var wxn = $1.t('div',{'class':'wrapx2'});
	wxn.appendChild($1.t('label',{textNode:'3.2 Compromiso Asesor'}));
	wxn.appendChild($1.t('textarea',{'class':'input jsFields',name:'fiCompAsesor',style:'font-size:12px; height:90px;',textNode:D.fiCompAsesor}));
	divLine.appendChild(wxn);
	wrap.appendChild(divLine);
	var divLine = $1.t('div',{'class':'divLine'});
	var wxn = $1.t('div',{'class':'wrapx1'});
	wxn.appendChild($1.t('label',{textNode:'4. Conclusiones y Recomendaciones'}));
	wxn.appendChild($1.t('textarea',{'class':'input jsFields',name:'fiConclusion',style:'font-size:12px; height:90px;',textNode:D.fiConclusion}));
	divLine.appendChild(wxn);
	wrap.appendChild(divLine);
	var resp = $1.t('div');
	var p = $1.t('p');
	var iSend = $1.t('input',{type:'button','class':'ui_button',value:'Guardar Información'});
	p.appendChild(resp);
	p.appendChild(iSend);
	iSend.onclick = function(){
		var vPost = 'fvaId='+D.fvaId+'&'+ps_DOM.gImp.byName(wrap);
		$ps_DB.get({file:'PUT /s/a03/forms.asesorias/A:o',inputs:vPost,
		func:function(Jq2){
			if(Jq2.errNo){ $ps_DB.response(resp,Jq2); }
			else{ ps_DOM.delet(wrapBk); A03.Asesorias.get(); }
		}
		});
	}
	
	wrap.appendChild(p);
	}});
	var wrapBk = $1.Win.open(wrap,{winTitle:'Formulario de Visita',intMaxWidth:'1024',winId:'formuVisitaDCP',winSize:'medium'});
	ps_DOM.body.appendChild(wrapBk);
},
get:function(Po){
	var vPost = $1.G.filter();;
	var cont = $M.Ht.cont;
	$Api.get({f:'/s/a03/forms.asesorias/A:o', inputs:vPost, func:function(Jq){
	if(Jq.errNo){ $Api.resp(cont,Jq); }
	else{
		var tb = $1.T.table(['','No.','Entrada','Salida',{textNode:'Hrs.',title:'Duración en Horas'},'Creado por','Responsable','Cliente']);
		var tBody = $1.t('tbody');
		for(var i in Jq.DATA){ var D = Jq.DATA[i];
			var tr1 = $1.t('tr');
			var td = $1.t('td');
			var edit = $1.t('input',{type:'button','class':'iBg iBg_edit', D:D});
			edit.onclick = function(){ A03.Asesorias.put(this.D); }
			td.appendChild(edit);
			var edit = $1.t('input',{type:'button','class':'iBg iBg_email', D:D});
			edit.onclick = function(){ A03.Asesorias.sendEmail(this.D); }
			td.appendChild(edit);
			var edit = $1.t('input',{type:'button','class':'iBg iBg_print', D:D});
			edit.onclick = function(){ A03.Asesorias.print(this.D); }
			td.appendChild(edit);
			tr1.appendChild(td);
			var td = $1.t('td');
			var aView = $1.t('a',{textNode:D.fvaId, D:D});
			aView.onclick = function(){ ADMS_DCP.Asesorias.viewById(this.D); }
			td.appendChild(aView);
			tr1.appendChild(td);
			tr1.appendChild($1.t('td',{textNode:$2d.f(D.doDate,'Y-m-d H:iam')}));
			tr1.appendChild($1.t('td',{textNode:$2d.f(D.endDate,'Y-m-d H:iam')}));
			tr1.appendChild($1.t('td',{textNode:D.timeDuration}));
			tr1.appendChild($1.t('td',{textNode:D.userName}));
			tr1.appendChild($1.t('td',{textNode:D.userAssgName}));
			var td = $1.t('td');
			td.appendChild($M.Ht.a({objType:'bussPartner',objRef:D.cardId, textNode:D.cardName}));
			tr1.appendChild(td);
			tBody.appendChild(tr1);
		}
		tb.appendChild(tBody);
		cont.appendChild(tb);
	}
	}});
},
print:function(P){
	var wrap = $1.t('div',{'class':'pagePrint_letter'});
	$ps_DB.get({file:'GET /s/a03/forms.asesorias/A:o.byId', inputs:'fvaId='+P.fvaId, loade:wrap,
	func:function(Jq){ var D = Jq;
		D.doTime = (D.doTime) ? D.doTime : '';
		D.endTime = (D.endTime) ? D.endTime : '';
		D.fiObjetivos = (D.fiObjetivos) ? D.fiObjetivos : '';
		D.fiActividades = (D.fiActividades) ? D.fiActividades : '';
		D.fiCompEmpresa = (D.fiCompEmpresa) ? D.fiCompEmpresa : '';
		D.fiCompAsesor = (D.fiCompAsesor) ? D.fiCompAsesor : '';
		D.fiConclusion = (D.fiConclusion) ? D.fiConclusion : '';
		var divLogo = $1.t('div',{style:'height:4rem; text-align:right;'});
		divLogo.appendChild($1.t('img',{src:$0s.Soc.logo}));
		wrap.appendChild(divLogo);
		var ffLine = $1.T.ffLine({ffLine:1,t:'Cliente:', v:D.cardName});
		wrap.appendChild(ffLine);
		var ffLine = $1.T.ffLine({ffLine:1,t:'Fecha:',w:'ffx3', v:D.doDate});
		ffLine.appendChild($1.T.ffLine({t:'Entrada:',w:'ffx3', v:D.doTime}));
		ffLine.appendChild($1.T.ffLine({t:'Entrada:',w:'ffx3', v:D.endTime}));
		wrap.appendChild(ffLine);
		var ffLine = $1.T.ffLine({ffLine:1,t:'Realizado por:', w:'ffx2', v:D.userName});
		ffLine.appendChild($1.T.ffLine({t:'Responsable:',w:'ffx2', v:D.userAssgName}));
		wrap.appendChild(ffLine);
		var ffLine = $1.T.ffLine({ffLine:1,t:'1. Objetivos'});
		wrap.appendChild(ffLine);
		wrap.appendChild($1.t('div',{'class':'textarea',textNode:D.fiObjetivos}));
		var ffLine = $1.T.ffLine({ffLine:1,t:'2. Actividades'});
		wrap.appendChild(ffLine);
		wrap.appendChild($1.t('div',{'class':'textarea',textNode:D.fiActividades}));
		var ffLine = $1.T.ffLine({ffLine:1,t:'3.1 Compromisos Empresa'});
		wrap.appendChild(ffLine);
		wrap.appendChild($1.t('div',{'class':'textarea',textNode:D.fiCompEmpresa}));
		var ffLine = $1.T.ffLine({ffLine:1,t:'3.2 Compromisos Asesor'});
		wrap.appendChild(ffLine);
		wrap.appendChild($1.t('div',{'class':'textarea',textNode:D.fiCompAsesor}));
		var ffLine = $1.T.ffLine({ffLine:1,t:'4. Conclusiones y Recomendaciones'});
		wrap.appendChild(ffLine);
		wrap.appendChild($1.t('div',{'class':'textarea',textNode:D.fiConclusion}));
		var divBot = $1.t('div',{'class':'pagePrint_footer',textNode:'Impreso por '+$0s.userName+' el '+$2d.f(null,'Y-m-d H:iam')+' by ADM Systems'});
		wrap.appendChild(divBot);
	}});
	var wrapBk = $1.Win.print(wrap,{winTitle:'Formulario de Visita',intMaxWidth:'1024',winId:'formuVisitaDCP',winSize:'medium'});
	ps_DOM.body.appendChild(wrapBk);
},
sendEmail:function(D){
	var wrap = $1.t('div');
	$ps_DB.get({file:'GET /s/a03/forms.asesorias/A:sendEmail', inputs:'fvaId='+D.fvaId,
		func:function(Jq2){
			if(Jq2.errNo){ $ps_DB.response(wrap,Jq2); }
			else{
			var D = Jq2;
			var divLine = $1.T.divLinewx({divLine:true,wxn:'wrapx2', Label:{textNode:'Enviar a'}, I:{tag:'select',sel:{'class':'jsFields',name:'sendTo'},opts:Jq2.Emails} });
			divLine.appendChild($1.T.divLinewx({wxn:'wrapx2', Label:{textNode:'Con copia A:'}, I:{tag:'input',type:'text','class':'jsFields',name:'ccto',placeholder:'Digite unos o varios correos'} }));
			wrap.appendChild(divLine);
			var divLine = $1.t('div',{'class':'divLine'});
			var wnx = $1.t('div',{'class':'wrapx1'});
			wnx.appendChild($1.t('label',{textNode:'Cuerpo del Mensaje'}));
			wnx.appendChild($1.t('textarea',{'class':'jsFields',name:'body', textNode:'Soporte del formulario de Visita Realizada por el Asesor '+D.userName+'.'}));
			divLine.appendChild(wnx);
			wrap.appendChild(divLine);
			var p = $1.t('p');
			var resp = $1.t('div');
			var iSend = $1.t('input',{type:'submit',value:'Enviar al Correo'});
			iSend.onclick = function(){
				$ps_DB.get({file:'POST /s/a03/forms.asesorias/A:sendEmail', inputs:'fvaId='+D.fvaId+'&cardName='+D.cardName+'&'+ps_DOM.gImp.byName(wrap), loaderFull:true,
				func:function(Jq2){
						$ps_DB.response(resp,Jq2);
				}
				});
			}
			p.appendChild(iSend);
			p.appendChild(resp);
			wrap.appendChild(p);
			}
		}
	});
	var wrapBk = ps_DOM.Tag.Win.bkFixed(wrap,{winTitle:'Enviar Formulario de Visita al Cliente'});
	ps_DOM.body.appendChild(wrapBk);
}
}
