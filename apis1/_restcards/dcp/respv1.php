<?php
$D = $_POST;
$today = date('Y-m-d');
$GLOBALS['logImg'] = ($ADMS_socKey == 's1') ? 'https://firebasestorage.googleapis.com/v0/b/teams-1.appspot.com/o/_cards%2Flogos%2Flogodyd.png?alt=media&token=0bb35814-d507-4f15-aec9-74c3a7f9fc7c' : 'https://firebasestorage.googleapis.com/v0/b/teams-1.appspot.com/o/_cards%2Flogos%2Flogodcpper.jpg?alt=media&token=cfc615be-7822-4832-9a43-0e328ae0a0b2';

function resp_qu($D=array(),$P=array()){
	$date1 = $D['FIE'][0]['RT1.dueDate(E_mayIgual)'];
	$date2 = $D['FIE'][1]['RT1.dueDate(E_menIgual)'];
	$days = 30;
	$mes = 86400*$days;
	$notDays = 15;
	$yearDays = 396;
	$dif = (strtotime($date2)-strtotime($date1));
	if($P['view']=='calendar' && $dif>$mes){
		echo _js::e(3,'En la vista calendario no se pueden exceder los '.$days.' días.'); die();
	}
	else if($P['view']=='viewDCP' && $dif>(86400*$yearDays)){
		echo _js::e(3,'Ninguna vista pueden exceder los '.$yearDays.' días (13 meses).'); die();
	}
	else if($P['view']=='notify' && $dif>(86400*$notDays)){
		echo _js::e(3,'El rango de fechas no puede exceder '.$notDays.' días.'); die();
	}
	$fiAdd = '';
	if($P['view']=='viewDCP'){//printer
		$fiAdd .= ',F1.peoRel';
		$lefAdd = 'LEFT JOIN '._0s::$Tb['_par_ficha'].' F1 ON (F1.cardId = C.cardId)';
	}
	_ADMS::_lb('sql/filter');
	require_once('dian_dv.php');
	$groupId = $D['groupId']; unset($D['groupId']);
	$wh = a_sql_filtByT($D,array('FIE_EXTs'=>array(3=>1)));
	$_nit = 'IF((LOCATE(\'-\',C.licTradNum)>0),SUBSTR(C.licTradNum,1,LENGTH(C.licTradNum)-2),C.licTradNum)';
	if(_0s::$bussPartnerPrivacity=='Public_Soc'){}
	else{ $whU = 'AND '.a_ses::U_access(0,array('ret'=>'userAssg','tbAlias'=>'C')); }
	$twhere = 'AND (C.ocardId=\''.a_ses::$ocardId.'\' AND C.osocId=\''.a_ses::$ocardSocId.'\' '.$whU.' AND C.active=\'Y\') ';
	$twhere .= $wh.' ';
	$inerGr = ($groupId!='')?'INNER JOIN '._0s::$Tb['par_cgr1'].' G1 ON (G1.groupId=\''.$groupId.'\' AND G1.cardId=C.cardId)' : '';;
	$qt = ('SELECT RT2.respStatus, RA.year, RA.cardId, C.licTradNum, GR0.grName, RT0.*, '.$_nit.' nit, RT1.dueDate, C.cardName, C.licTradNum, C.RF_regTrib, C.RF_tipEnt, C.userAssg, C.userAssgName,
	IF(RT0.gdvCode =\'0-9\',SUBSTRING('.$_nit.',-1),SUBSTRING('.$_nit.',-2)) dvSys '.$fiAdd.'
	FROM 
	'._0s::$Tb['par_ocrd'].' C
	'.$inerGr.'
	'.$lefAdd.'
	INNER JOIN '._0s::$Tb['gt1_grt1'].' RA ON (RA.cardId = C.cardId)
	INNER JOIN '._0s::$Tb['gt1_ogrt'].' GR0 ON (GR0.grId = RA.grId)
	INNER JOIN '._0s::$Tb['gt1_ortr'].' RT0 ON (RT0.grId = RA.grId AND RT0.respYear = RA.year 
	AND ((C.RF_regTrib =\'GC\' AND RT0.respTo=\'GC\') 
	OR (C.RF_regTrib!=\'GC\' AND RT0.respTo=C.RF_tipEnt)
	OR (RT0.respTo=\'general\')
	)
	)
	INNER JOIN '._0s::$Tb['gt1_rtr1'].' RT1 ON (RT1.respId = RT0.respId AND (RT1.verif =\'uni\' OR
	RT1.verif = IF(RT0.gdvCode =\'0-9\',SUBSTRING('.$_nit.',-1),SUBSTRING('.$_nit.',-2)) 
	)) 
	LEFT JOIN '._0s::$Tb['gt1_rtr2'].' RT2 ON (RT2.respId = RT0.respId AND RT2.cardId = RA.cardId) 
	WHERE 1 '.$twhere.'
	GROUP BY RT0.respId, RT1.respId, RA.cardId
	ORDER BY RT1.dueDate ASC 
	');
	//echo $qt;
	$q = a_sql::query($qt);
	return $q;
}

function mailTem_user($R=array(),$D2=array()){
	$itdCss = 'style="border:0.0625rem solid #000; padding:0.25rem;';
	$html = '<img src="'.$GLOBALS['logImg'].'" style="height:55px;"/>
	<h4>Señor/a, '.$D2['cardName'].'</h4>
<p>Esta es una notificación de sus próximos vencimientos.</p>
<table style="border-collapse:collapse"> <tr> <td '.$tdCss.'>Fecha</td> <td title="Días que falta a la fecha de la notificación">Faltan</td> <td '.$tdCss.'>Responsabilidad</td> </tr>';
	foreach($R as $k =>$L){
		$tdCss = $itdCss;
		$tdCss .= ($L['daysToDue']<4)?'background-color:#FF0;"' :'"';
		$html .= '<tr> <td '.$tdCss.'>'.$L['dueDate'].'</td> <td '.$tdCss.'>'.$L['daysToDue'].'</td> <td '.$tdCss.'>'.$L['respName'].'</td></tr>';
	}
	$html .= '</table>
	<br/><br/>
	<div>Esto es un mensaje automático, si tiene alguna duda o la información no es clara, comuniquese con su delegado: <a href="mailto:'.$D2['userEmail'].'">'.$D2['userAssgName'].'</a></div>
	<br/><br/><br/><br/>
	<div style="font-size:0.75rem;">Reporte generado por ADM Systems</div>';
	return $html;
}

if(_0s::$router=='GET resp'){
	_ADMS::_lb('sql/filter');
	$wh = a_sql_filtByT($___D);
	$qu = a_sql::query('SELECT R0.*, GR0.grName FROM '._0s::$Tb['gt1_ortr'].' R0 INNER JOIN '._0s::$Tb['gt1_ogrt'].' GR0 ON (GR0.grId = R0.grId) WHERE 1 '.$wh.' ORDER BY R0.respYear DESC, GR0.grName ASC LIMIT 200',array(1=>'Error obteniendo responsabilidades.',2=>'No se encontraron responsabilidades'));
	if(a_sql::$err){ $js = a_sql::$errNoText; }
	else{
		$M=array('L'=>array());
		while($F = $qu->fetch_assoc()){
			$M['L'][]=$F;
		}
		$js = _js::enc($M);
	}
	echo $js;
}
else if(_0s::$router=='GET resp/one'){
	if($js=_js::ise($___D['respId'],'Se debe definir ID de responsabilidad')){ die($js); }
	$M = a_sql::fetch('SELECT * FROM '._0s::$Tb['gt1_ortr'].' R0 WHERE R0.respId=\''.$___D['respId'].'\' LIMIT 1',array(1=>'Error obteniendo información de la responsabilidad',2=>'No se encontró la responsabilidad.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{
		$qu = a_sql::query('SELECT subGr,dueDate FROM '._0s::$Tb['gt1_rtr1'].' WHERE respId=\''.$___D['respId'].'\'',array(1=>'Error obteniendo información de  los vencimientos de la responsabilidad',2=>'No se encontraron vencimientos para la responsabilidad.'));
		$M['L']=array();
		if(a_sql::$err){ $M['L']=json_decode(a_sql::$errNoText,1); }
		else{
		while($L = $qu->fetch_assoc()){
			$M['L'][$L['subGr']]=$L['dueDate'];
		}
		}
		$js =_js::enc($M,'just');
	}
	echo $js;
}
else if(_0s::$router=='PUT resp'){
	$respId = $___D['respId'];
	if($js=_js::ise($___D['respName'],'Se debe definir el nombre de la responsabilidad')){}
	else if($js=_js::ise($___D['grId'],'Se debe definir el grupo.')){}
	else if($js=_js::ise($___D['respYear'],'Se debe definir el año de la responsabilidad.')){}
	else if($js=_js::ise($___D['gdvCode'],'Se debe definir el grupo de digitos.')){}
	else if($js=_js::ise($___D['subGr'],'No se recibieron lineas para definir las fechas','array')){}
	else{
		$qu = a_sql::fetch('SELECT locked FROM '._0s::$Tb['gt1_ortr'].' WHERE respId=\''.$respId.'\' LIMIT 1');
		if($qu['locked'] == 'Y' && a_ses::$user!='supersu'){ die(_js::e(3,'La responsabilidad está bloqueada, solo un su-administrador puede modificar esa información.'));  }
		$subGrs = $___D['subGr']; unset($___D['subGr']);
		$err = 0;
		_ADMS::_lb('com/_2d');
		require('dian_dv.php');#llamo aqui,xk esta es en _0s y no cuenta
		foreach($subGrs as $subGr => $D2){
			if($D2['dueDate'] == ''){ unset($subGrs[$subGr]); }
			else{
				if(_2d::is0($D2['dueDate'])){ $js = _js::e(3,'La fecha es incorrecta.'); $err++; break; }
			}
		}
		if($err == 0){
			$ins = a_sql::insert($___D,array('tbk'=>'gt1_ortr','wh_change'=>'WHERE respId=\''.$respId.'\' LIMIT 1'));
			if($ins['err']){ $js = _js::e(1,$ins['text']); }
			else{
				$respId = ($ins['insertId']) ? $ins['insertId'] : $respId;
				$jsA='"respId":"'.$respId.'"';
				$js = _js::r('Responsabilidad Actualizada correctamente',$jsA);
				a_sql::query('DELETE FROM '._0s::$Tb['gt1_rtr1'].' WHERE respId=\''.$respId.'\' ');
				foreach($subGrs as $subGr => $D2){
					$D2['respId'] = $respId; $D2['gdvCode'] = $___D['gdvCode']; $D2['subGr'] = $subGr;
					foreach($DIAN_VERIF[$___D['gdvCode']] as $verif => $subGrDef){
						if($subGr == $subGrDef){
							$D2['verif'] = $verif;
							$ins2 = a_sql::insert($D2,array('qDo'=>'insert','tbk'=>'gt1_rtr1'));
							if($ins2['err']){ $js=_js::e(3,'Linea '.$subGr.': no se guardo correctamente: '.$ins2['text'],$jsA); break; }
						}
					}
				}
			}
		}
	}
	echo $js;
}

/*  view */
else if(_0s::$router=='GET resp/viewDCP'){
	$q = resp_qu($___D,array('view'=>'viewDCP'));
	if(a_sql::$errNo == 1){ $js = _js::e(1,'Error obteniendo responsabilidades: '.$q['error_sql']);}
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron responsabilidades.');}
	else{
	$Mx = array('Card'=>array());
	$Us = array();
	while($L = $q->fetch_assoc()){
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$kr = $L['cardId'];
		$subGr = $DIAN_VERIF[$k][$num];
		$L['verif'] = $L['dvSys'];
		$L['subGr'] = $subGr;
		$Ym = date('Y-m',strtotime($L['dueDate']));
		$Mx['Ym'][$Ym] = $Ym;
		if(!array_key_exists($kr,$Mx['Card'])){
			$Mx['Card'][$kr] = array('cardId'=>$L['cardId'],'cardName'=>$L['cardName'],'licTradNum'=>$L['licTradNum'],'userAssg'=>$L['userAssg'],'userAssgName'=>$L['userAssgName'],'RF_regTrib'=>$L['RF_regTrib'],'RF_tipEnt'=>$L['RF_tipEnt']);
			if($L['peoRel']){
			$Mx['Card'][$kr]['peoRel'] = _js::dec($L['peoRel']);
			}
		}
		$Mx['Card'][$kr]['Ym'][$Ym][] = array('dueDate'=>$L['dueDate'],'respStatus'=>$L['respStatus'],'respId'=>$L['respId'],'respName'=>$L['respName'],'respYear'=>$L['respYear'],'grName'=>$L['grName']);
	}
	$js = json_encode($Mx);
	}
	echo $js;
}


else if($ADMS_KEY == 'View'){
if($ADMS_KEYo[1] == 'list'){
	$q = resp_qu($D);
	if(a_sql::$errNo == 1){ $js = _js::e(1,'Error obteniendo responsabilidades: '.$q['error_sql']);}
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron responsabilidades.');}
	else{
	$Mx = array('Resp'=>array());
	while($L = $q->fetch_assoc()){
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$ye = substr($L['duedate'],0,4);
		$kr = $ye.'-'.$L['respId'];
		$subGr = $DIAN_VERIF[$k][$num];
		$L['verif'] = $L['dvSys'];
		$L['subGr'] = $subGr;
		if(!array_key_exists($kr,$Mx['Resp'])){
			$Mx['Resp'][$kr] = array('respName'=>$L['respName'],'gdvCode'=>$L['gdvCode'],'respTo'=>$L['respTo'],'grName'=>$L['grName']);
		}
		unset($L['respName'],$L['gdvCode'],$L['gc'],$L['pj'],$L['pn'],$L['si']);
		$Mx['Resp'][$kr]['L'][] = $L;
	}
		$js = json_encode($Mx);
	}
	echo $js;
}
else if($ADMS_KEYo[1] == 'calendar'){
		$q = resp_qu($D,array('view'=>'calendar'));
	if(a_sql::$errNo == 1){ $js = _js::e(1,'Error obteniendo responsabilidades: '.$q['error_sql']);}
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron responsabilidades.');}
	else{
	$Mx = array('Resp'=>array());
	_ADMS::_lb('com/_2d');
	$lastW ='';
	while($L = $q->fetch_assoc()){
		$timeDue = $L['dueDate'];
		$w = (_2d::g('z',$timeDue));
		$L['yzId'] = _2d::g('Y',$timeDue).'_'.$w;//+1 por javascript
		$lastW = $w;
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$kr = $L['yzId'].'_'.$L['respId'];
		$verif = ($k == '0-9')? substr($nit,-1)*1 : substr($nit,-2);
		$L['verif'] = $verif;
		$L['subGr'] = $DIAN_VERIF[$k][$verif];
		if(!array_key_exists($kr,$Mx['Resp'])){
			$Mx['Resp'][$kr] = array('respName'=>$L['respName'],'gdvCode'=>$L['gdvCode'],'gc'=>$L['gc'],'pj'=>$L['pj'],'pn'=>$L['pn'],'si'=>$L['si']);
		}
		$Mx['Resp'][$kr]['yzId'] = $L['yzId'];
		$Mx['Resp'][$kr]['total'] += 1;
		unset($L['gdvCode'],$L['gc'],$L['pj'],$L['pn'],$L['si']);
		$Mx['Resp'][$kr]['L'][] = $L;
	}
		$js = json_encode($Mx);
	}
	echo $js;
}
else if($ADMS_KEYo[1] == 'byCard'){
	$q = resp_qu($D);
	if(a_sql::$errNo == 1){ $js = _js::e(1,'Error obteniendo responsabilidades: '.$q['error_sql']);}
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron responsabilidades.');}
	else{
	$Mx = array('Card'=>array());
	$Us = array();
	while($L = $q->fetch_assoc()){
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$kr = $L['cardId'];
		$subGr = $DIAN_VERIF[$k][$num];
		$L['verif'] = $L['dvSys'];
		$L['subGr'] = $subGr;
		if(!array_key_exists($kr,$Mx['Card'])){
			$Mx['Card'][$kr] = array('cardId'=>$L['cardId'],'cardName'=>$L['cardName'],'licTradNum'=>$L['licTradNum'],'userAssg'=>$L['userAssg'],'userAssgName'=>$L['userAssgName'],'RF_regTrib'=>$L['RF_regTrib'],'RF_tipEnt'=>$L['RF_tipEnt']);
		}
		$Us[$L['userAssg']] = $L['userAssg'];
		$Mx['Card'][$kr]['L'][] = array('dueDate'=>$L['dueDate'],'respStatus'=>$L['respStatus'],'respId'=>$L['respId'],'respName'=>$L['respName'],'respYear'=>$L['respYear'],'grName'=>$L['grName']);
	}
	$js = json_encode($Mx);
	}
	echo $js;
}

}
else if($ADMS_KEY == 'Anex'){
if($ADMS_MET =='GET'){
	_ADMS::_lb('sql/filter');
	$qu = a_sql::query('SELECT * FROM '._0s::$Tb['gt1_rtr3'].' R3  WHERE R3.respId=\''.$D['respId'].'\' LIMIT 100');
	if(a_sql::$errNo == 1){ $js = _js::e(1,$qu['error_sql']); }
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron anexos.'); }
	else{
		while($F = $qu->fetch_assoc()){
			$js .= a_sql::JSON($F,'  ').','; 
		}
		$js = '{"Data":['.substr($js,0,-1).'
]
}';
	}
	echo $js;
}
else if($ADMS_MET =='PUT'){
	$respId = $D['respId'];
	if(!a_ses::$isSuper){ echo _js::e(4,'Su usuario no tiene permisos para modificar está información (supersu)'); return ''; }
	if(_js::isse($respId)){ $js = _js::e(3,'No se encontró relación a ninguna responsabilidad. '.$respId); }
	else{
		$err = 0;
		foreach($D['L'] as $ln => $D2){
			$b1 = _js::isse($D2['lineMemo']);
			$b2 = _js::isse($D2['linkTo']);
			if($b1 && $b2){ unset($D['L'][$ln]); }
			else if(!$b1 && $b2){ $js = _js::e(3,'#'.$ln.': El enlace debe estar definido.'); $err++; break; }
			else if($b1 && !$b2){ $js = _js::e(3,'#'.$ln.': Especifique los detalles'); $err++; break; }
		}
		if($err == 0){
			$ro = 0;
			foreach($D['L'] as $ln => $D2){
				$D2['respId'] = $respId; $D2['lineNum'] = $ln;
				$ins = a_sql::insert($D2,array('table'=>_0s::$Tb['gt1_rtr3'],'wh_change'=>'WHERE respId=\''.$respId.'\' AND lineNum=\''.$ln.'\' LIMIT 1'));
				if($ins['err']){ $js = _js::e(1,$ins['err']['error_sql']); $err++; break; }
				if(!$D2['delete']){ $ro++; }
			}
			if($err == 0){
				$js = _js::r('Se relacionaron '.$ro.' anexos a la responsabilidad.');
			}
		}
	}
	echo $js;
}
}
else if($ADMS_KEY == 'status'){
if($ADMS_MET =='GET'){
	$qH = a_sql::fetch('SELECT * FROM '._0s::$Tb['gt1_rtr2'].' R2  WHERE R2.respId=\''.$D['respId'].'\' AND R2.cardId=\''.$D['cardId'].'\'LIMIT 1');
	$qu = a_sql::query('SELECT * FROM '._0s::$Tb['gt1_rtr99'].' R99  WHERE R99.respId=\''.$D['respId'].'\' AND R99.cardId=\''.$D['cardId'].'\'LIMIT 30');
	if(a_sql::$errNo == 1){ $js = _js::e(1,$qu['error_sql']); }
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron actualizaciones en el estado.'); }
	else{
		$js0 = a_sql::JSON($qH);
		while($F = $qu->fetch_assoc()){
			$js .= a_sql::JSON($F,'  ').','; 
		}
		$js = '{
	"H":'.$js0.',
 "L":['.substr($js,0,-1).'
]
}';
	}
	echo $js;
}
else if($ADMS_MET =='PUT'){
	if(_js::isse($D['respId'])){ $js = _js::e(3,'No se encontró relación a ninguna responsabilidad. '.$D['respId']); }
	else if(_js::isse($D['cardId'])){ $js = _js::e(3,'No se encontró relación a ningún cliente. '.$D['cardId']); }
	else if(_js::isse($D['respStatus'])){ $js = _js::e(3,'Se debe definir un estado.'); }
	else{
		$ins = a_sql::insert($D,array('u_dateC'=>1,'table'=>_0s::$Tb['gt1_rtr2'],'wh_change'=>'WHERE cardId=\''.$D['cardId'].'\' AND respId=\''.$D['respId'].'\' LIMIT 1'));
		if($ins['err']){ $js = _js::e(1,$ins['err']['error_sql']); }
		else{
			$ins = a_sql::insert($D,array('POST'=>1,'u_dateC'=>1,'table'=>_0s::$Tb['gt1_rtr99']));
			if($ins['err']){ $js = _js::e(1,$ins['err']['error_sql']); }
			else{ $js = _js::r('Estado actualizado correctamente.'); }
		}
	}
	echo $js;
}
}
else if($ADMS_KEY == 'Not'){
if($ADMS_KEYo[1] == 'byCard'){ echo 'ALERTJSON';
	if(!_js::isse($D['emailNotifTo'])){
		_ADMS::_lb('com/_ocrd_emails'); $emailIns = $D['emailNotifTo']; unset($D['emailNotifTo']);
	}
	$q = resp_qu($D,array('allCard'=>true,'view'=>'notify'));
	if(a_sql::$errNo == 1){ $js = _js::e(1,'Error obteniendo responsabilidades: '.$q['error_sql']);}
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron responsabilidades.');}
	else{
	$Mx = array('Card'=>array());
	$Us = array();
	$today= date('Y-m-d');
	$todaytime = strtotime($today);
	$rows = 0;
	$n=0;
	$D['daysBef'] = 2; 
	$Mx = array('Card'=>array());
	while($L = $q->fetch_assoc()){
		//if($n==5){ break; }$n++;
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$kr = $L['cardId'];
		$subGr = $DIAN_VERIF[$k][$num];
		$L['verif'] = $L['dvSys'];
		$L['subGr'] = $subGr;
		$notDate = ($D['daysBef'])? date('Y-m-d',strtotime($L['dueDate'])-(86400*$D['daysBef'])) : $L['dueDate'];
		/* no enviar si ya vencio o notifico hoy mismo */
		if($L['dueDate']<$today){ continue; }
		$lastNot = a_sql::fetch('SELECT notDate  FROM '._0s::$Tb['gt1_onot'].' WHERE respId=\''.$L['respId'].'\' AND cardId=\''.$L['cardId'].'\' ORDER BY notDate DESC LIMIT 1 ');
		if(a_sql::$errNo==-1 && $lastNot['notDate']==$today){ continue; }
		$lastNotDate = ($lastNot['notDate']!='')?$lastNot['notDate']:'';
		$L['cardName'] = $L['cardName'];
		$RS = 'RS';
		if(!array_key_exists($kr,$Mx['Card'])){
			$Mx['Card'][$kr] = array('cardId'=>$L['cardId'],'cardName'=>$L['cardName'],'licTradNum'=>$L['licTradNum'],'userAssg'=>$L['userAssg'],'userAssgName'=>$L['userAssgName'],'RF_regTrib'=>$L['RF_regTrib'],'RF_tipEnt'=>$L['RF_tipEnt']);
			if($emailIns!=''){
				if(preg_match('/^userAssg/',$emailIns)){
					$userAssg = a_ses::U_getInfo($L['userAssg'],'userEmail');
					if(array_key_exists('userEmail',$userAssg)){
						$Mx['Card'][$kr]['emailTo'] = $userAssg['userEmail'].',';
						$Mx['Card'][$kr]['userEmail'] = $userAssg['userEmail'];
					}
				}
				$emails = _ocrd_emails::get($L['cardId'],array('r'=>'plain','wh'=>array('workerType(E_in)'=>$emailIns)));
				if($emails['emails']){
					$Mx['Card'][$kr]['emailTo'] .= $emails['emails'];
				}
			}
			//$Mx['Card'][$kr]['emailTo'] = 'jromdev@gmail.com';
		}
		$daysToDue = (strtotime($L['dueDate'])-$todaytime)/86400;
		$Rs = '&RS['.$L['respId'].']';
		$Mx['Card'][$kr]['Lsend'] .= $Rs.'[daysToDue]='.$daysToDue.$Rs.'[notDate]='.$today.$Rs.'[dueDate]='.$L['dueDate'];
		$Mx['Card'][$kr]['L'][] = array('daysToDue'=>$daysToDue,'lastNotDate'=>$lastNotDate,'dueDate'=>$L['dueDate'],'respStatus'=>$L['respStatus'],'respId'=>$L['respId'],'respName'=>$L['respName']);
	}
	$js = json_encode($Mx);
	}
	echo $js;
}
else if($ADMS_KEYo[1] == 'send'){
	if(count($D['RS'])==0){ $js = _js::e(3,'No se encontraron notificaciones para enviar.'); }
	else{
		foreach($D['RS'] as $k=>$V){ $respIds .= $k.','; }
		$respIds = substr($respIds,0,-1);
		$q = a_sql::query('SELECT R0.respId, R0.respName FROM '._0s::$Tb['gt1_ortr'].' R0 WHERE R0.respId IN ('.$respIds.') LIMIT 600');
		if(a_sql::$errNo==1){ $js = _js::e(1,'Error obteniendo responsabilidades para notificación: '.$q['error_sql']); }
		else if(a_sql::$errNo==2){ $js = _js::e(2,'No se encontraron responsabilidades para notificar.'); }
		else{
			$U = array();
			while($L=$q->fetch_assoc()){
				$ku = $D['userAssg'];
			if(!array_key_exists($ku,$U)){
				$U[$ku] = array('cardName'=>$D['cardName'],'mailTo'=>$D['mailTo']);
			}
			$kc = $D['cardId'].'-'.$L['respId'];
			$tR = $D['RS'][$L['respId']];
			$U[$ku]['R'][$kc] = array('daysToDue'=>$tR['daysToDue'],'dueDate'=>$tR['dueDate'],'notDate'=>$tR['notDate'],'respId'=>$L['respId'],'respName'=>$L['respName'],'respStatus'=>$L['respStatus']);
			}
		}
		if(count($U)>0){
		_ADMS::_lb('lb/_Mailer');
		foreach($U as $userId =>$L2){
		$D['body'] = mailTem_user($L2['R'],$D);
		$D['fromName'] = 'DYD Consultores y Asesores S.A.S.'; $D['userAssgName'];
		$D['fromEmail'] = 'dydplatform@admsystems.co'; $D['userEmail'];
		$D['replyTo'] = $D['userAssgName'].' <'.$D['userEmail'].'>';
		$s = _Mail::send($D);
		if($s['errNo']){ $js = _js::e(3,'No se envió el correo a los destinatarios ('.$D['cardName'].'). '.$s['text']); }
		else{
			foreach($L2['R'] as $respId=>$L3){
				$upd = a_sql::insert(array('cardId'=>$D['cardId'],'respId'=>$L3['respId'],'notDate'=>$L3['notDate'],'dueDate'=>$L3['dueDate']),array('POST'=>1,'table'=>_0s::$Tb['gt1_onot']));
			}
			$js = _js::r('Correo enviado correctamente.','"cardId":"'.$D['cardId'].'"');
			}
		}
		}
	}
	echo $js;
}

}



/* Version empresa*/
else if($ADMS_KEY == 'Pyme'){
if($ADMS_KEYo[1] == 'list'){
	$q = resp_qu($D);
	if(a_sql::$errNo == 1){ $js = _js::e(1,'Error obteniendo responsabilidades: '.$q['error_sql']);}
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron responsabilidades.');}
	else{
	$Mx = array('Resp'=>array());
	while($L = $q->fetch_assoc()){
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$kr = $L['respId'];
		$subGr = $DIAN_VERIF[$k][$num];
		$L['verif'] = $L['dvSys'];
		$L['subGr'] = $subGr;
		$js .= a_sql::JSON($L).',';
	}
		$js = '{"L":['.substr($js,0,-1).']}';
	}
	echo $js;
}
else if($ADMS_KEYo[1] == 'month'){
	$q = resp_qu($D,array('view'=>'calendar'));
	if(a_sql::$errNo == 1){ $js = _js::e(1,'Error obteniendo responsabilidades: '.$q['error_sql']);}
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron responsabilidades.'); }
	else{
	$Mx = array('Resp'=>array());
	_ADMS::_lb('com/_2d');
	$lastW ='';
	while($L = $q->fetch_assoc()){
		$timeDue = $L['dueDate'];
		$w = (_2d::g('z',$timeDue));
		$L['yzId'] = _2d::g('Y',$timeDue).'_'.$w;//+1 por javascript
		$lastW = $w;
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$kr = $L['yzId'].'_'.$L['respId'];
		$verif = ($k == '0-9')? substr($nit,-1)*1 : substr($nit,-2);
		$L['verif'] = $verif;
		$L['subGr'] = $DIAN_VERIF[$k][$verif];
		$js .= a_sql::JSON($L).',';
	}
		$js = '{"L":['.substr($js,0,-1).']}';
	}
	echo $js;
}

}


?>