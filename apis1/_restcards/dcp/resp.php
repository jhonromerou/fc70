<?php
/*
update par_ocrd SET licTradNumExt=(
IF((LOCATE('-',licTradNum)>0),
SUBSTR(licTradNum,LENGTH(licTradNum),LENGTH(licTradNum))
,'')) LIMIT 1000;

update par_ocrd SET licTradNum=REPLACE(licTradNum,CONCAT('-',licTradNumExt),'') LIMIT 10000; 
*/
$D = $_POST; $oTy='card';
$today = date('Y-m-d');
$GLOBALS['logImg'] = ($ADMS_socKey == 's1') ? 'https://firebasestorage.googleapis.com/v0/b/teams-1.appspot.com/o/_cards%2Flogos%2Flogodyd.png?alt=media&token=0bb35814-d507-4f15-aec9-74c3a7f9fc7c' : 'https://firebasestorage.googleapis.com/v0/b/teams-1.appspot.com/o/_cards%2Flogos%2Flogodcpper.jpg?alt=media&token=cfc615be-7822-4832-9a43-0e328ae0a0b2';

_ADMS::_lb('sql/filter');
require_once('dian_dv2.php');

function mailTem_user($R=array(),$D2=array()){
	$thCss = 'style="border:0.0625rem solid #000; padding:0.25rem;"';
	$tdCss = 'style="border:0.0625rem solid #000; padding:0.25rem;"';
	$html = '<img src="http://static1.admsistems.com/_logos/logodyd.png" style="height:55px;"/>
	<h4>Señor/a, '.$D2['cardName'].'</h4>
<p>Esta es una notificación de sus próximos vencimientos.</p>
<table style="border-collapse:collapse"><tr>
<td '.$tdCss.'>Fecha</td>
<td '.$tdCss.'>Grupo Resp.</td>
<td '.$tdCss.'>Responsabilidad</td>
</tr>';
	foreach($R as $k =>$L){
		$html .= '<tr>
		<td '.$tdCss.'>'.$L['dueDate'].'</td>
		<td '.$tdCss.'>'.$L['grName'].'</td>
		<td '.$tdCss.'>'.$L['respName'].'</td>
		</tr>';
	}
	$html .= '</table>
	<br/><br/>
	<div>Esto es un mensaje automático, si tiene alguna duda o la información no es clara, comuniquese con su delegado: '.$D2['slpName'].' <<a href="mailto:'.$D2['slpEmail'].'">'.$D2['slpEmail'].'</a>></div>
	<br/><br/><br/><br/>
	<div style="font-size:0.75rem;">Realizado desde <a href="http://admsistems.com">www.admsistems.com</a></div>';
	return $html;
}
function mailTem_Pdf($D2=array()){
	$thCss = 'style="border:0.0625rem solid #000; padding:0.25rem;"';
	$tdCss = 'style="border:0.0625rem solid #000; padding:0.25rem;"';
	$html = '<img src="http://static1.admsistems.com/_logos/logodyd.png" style="height:55px;"/>
	<h4>Estimado '.$D2['cardName'].'</h4>
<p>Esta es una notificación de sus próximos vencimientos. Encontrará adjunto un PDF con la información de sus vencimientos.</p>';
	$html .= '
	<br/><br/>
	<div>Esto es un mensaje automático, si tiene alguna duda o la información no es clara, comuniquese con su delegado: '.$D2['slpName'].' <<a href="mailto:'.$D2['slpEmail'].'">'.$D2['slpEmail'].'</a>></div>
	<br/><br/><br/><br/>
	<div style="font-size:0.75rem;">Realizado desde <a href="http://admsistems.com">www.admsistems.com</a></div>';
	return $html;
}


if(_0s::$router=='GET resp'){
	_ADMS::_lb('sql/filter');
	$wh = a_sql_filtByT($___D);
	$qu = a_sql::query('SELECT R0.*, GR0.grName FROM gt1_ortr R0 INNER JOIN gt1_ogrt GR0 ON (GR0.grId = R0.grId) WHERE 1 '.$wh.' ORDER BY R0.respYear DESC, GR0.grName ASC LIMIT 200',array(1=>'Error obteniendo responsabilidades.',2=>'No se encontraron responsabilidades'));
	if(a_sql::$err){ $js = a_sql::$errNoText; }
	else{
		$M=array('L'=>array());
		while($F = $qu->fetch_assoc()){
			$M['L'][]=$F;
		}
		$js = _js::enc($M);
	}
	echo $js;
}
else if(_0s::$router=='GET resp/one'){
	if($js=_js::ise($___D['respId'],'Se debe definir ID de responsabilidad')){ die($js); }
	$M = a_sql::fetch('SELECT * FROM gt1_ortr R0 WHERE R0.respId=\''.$___D['respId'].'\' LIMIT 1',array(1=>'Error obteniendo información de la responsabilidad',2=>'No se encontró la responsabilidad.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{
		$qu = a_sql::query('SELECT subGr,dueDate FROM gt1_rtr1 WHERE respId=\''.$___D['respId'].'\'',array(1=>'Error obteniendo información de  los vencimientos de la responsabilidad',2=>'No se encontraron vencimientos para la responsabilidad.'));
		$M['L']=array();
		if(a_sql::$err){ $M['L']=json_decode(a_sql::$errNoText,1); }
		else{
		while($L = $qu->fetch_assoc()){
			$M['L'][$L['subGr']]=$L['dueDate'];
		}
		}
		$js =_js::enc($M,'just');
	}
	echo $js;
}
else if(_0s::$router=='PUT resp'){
	$respId = $___D['respId'];
	if($js=_js::ise($___D['respName'],'Se debe definir el nombre de la responsabilidad')){}
	else if($js=_js::ise($___D['grId'],'Se debe definir el grupo.')){}
	else if($js=_js::ise($___D['respYear'],'Se debe definir el año de la responsabilidad.')){}
	else if($js=_js::ise($___D['gdvCode'],'Se debe definir el grupo de digitos.')){}
	else if($js=_js::ise($___D['subGr'],'No se recibieron lineas para definir las fechas','array')){}
	else{
		//die('ALERTJSON '.print_r($DIAN_VERIF[$___D['gdvCode']],1));
		$qu = a_sql::fetch('SELECT locked FROM gt1_ortr WHERE respId=\''.$respId.'\' LIMIT 1');
		if($qu['locked'] == 'Y' && a_ses::$user!='supersu'){ die(_js::e(3,'La responsabilidad está bloqueada, solo un su-administrador puede modificar esa información.'));  }
		$subGrs = $___D['subGr']; unset($___D['subGr']);
		$err = 0;
		_ADMS::_lb('com/_2d');
		require('dian_dv2.php');#llamo aqui,xk esta es en _0s y no cuenta
		foreach($subGrs as $subGr => $D2){
			if($D2['dueDate'] == ''){ unset($subGrs[$subGr]); }
			else{
				if(_2d::is0($D2['dueDate'])){ $js = _js::e(3,'La fecha es incorrecta.'); $err++; break; }
			}
		}
		if($err == 0){
			$ins = a_sql::insert($___D,array('tbk'=>'gt1_ortr','wh_change'=>'WHERE respId=\''.$respId.'\' LIMIT 1'));
			if($ins['err']){ $js = _js::e(1,$ins['text']); }
			else{
				$respId = ($ins['insertId']) ? $ins['insertId'] : $respId;
				$jsA='"respId":"'.$respId.'"';
				$js = _js::r('Responsabilidad Actualizada correctamente',$jsA);
				a_sql::query('DELETE FROM gt1_rtr1 WHERE respId=\''.$respId.'\' ');
				$nx=0;
				foreach($subGrs as $subGr => $D2){
					$D2['respId'] = $respId; $D2['gdvCode'] = $___D['gdvCode'];
					$D2['subGr'] = $subGr;
					foreach($DIAN_VERIF[$___D['gdvCode']] as $verif => $subGrDef){
						if($subGr == $subGrDef){
							$D2['verif'] = $verif;
							$ins2 = a_sql::insert($D2,array('qDo'=>'insert','tbk'=>'gt1_rtr1'));
							if($ins2['err']){ $js=_js::e(3,'Linea '.$subGr.': no se guardo correctamente: '.$ins2['text'],$jsA); break; }
						}
					}
				}
			}
		}
	}
	echo $js;
}

/*  view */
else if(_0s::$router=='GET resp/viewDCP'){
	$q = pTri::resp_qu($___D,array('view'=>'viewDCP'));
		if(pTri::$err){ die(pTri::$errText); }
	$Mx = array('Card'=>array());
	$Us = array();
	_ADMS::_lb('src/cpr');
	while($L = $q->fetch_assoc()){
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$kr = $L['cardId'];
		$subGr = $DIAN_VERIF[$k][$num];
		$L['verif'] = $L['dvSys'];
		$L['subGr'] = $subGr;
		$Ym = date('Y-m',strtotime($L['dueDate']));
		$Mx['Ym'][$Ym] = $Ym;
		if(!array_key_exists($kr,$Mx['Card'])){
			$Mx['Card'][$kr] = array('cardId'=>$L['cardId'],'cardName'=>$L['cardName'],'licTradNum'=>$L['licTradNum'],'slpId'=>$L['slpId'],'RF_regTrib'=>$L['RF_regTrib'],'RF_tipEnt'=>$L['RF_tipEnt']);
			$peoRel=Cpr::get(array('tr'=>$L['cardId'],'k'=>'position','f'=>'A.position,A.name,A.email','wh'=>'A.position IN (\'gerente\',\'contador\')'));
			if(!Cpr::$err){
				$Mx['Card'][$kr]['peoRel'] = $peoRel;
			}
			else{ $Mx['Card'][$kr]['peoRel'] = Cpr::$errText; }
		}
		$Mx['Card'][$kr]['Ym'][$Ym][] = array('dueDate'=>$L['dueDate'],'respStatus'=>$L['respStatus'],'respId'=>$L['respId'],'respName'=>$L['respName'],'respYear'=>$L['respYear'],'grName'=>$L['grName']);
	}
	$js = json_encode($Mx);
	echo $js;
}
else if(_0s::$router=='GET resp/list'){
	$q = pTri::resp_qu($___D);
	if(pTri::$err){ die(pTri::$errText); }
	$Mx = array('Resp'=>array());
	while($L = $q->fetch_assoc()){
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$ye = substr($L['dueDate'],0,4);
		$kr = $ye.'-'.$L['respId'];
		$subGr = $DIAN_VERIF[$k][$num];
		$L['verif'] = $L['dvSys'];
		$L['subGr'] = $subGr;
		if(!array_key_exists($kr,$Mx['Resp'])){
			$Mx['Resp'][$kr] = array('respName'=>$L['respName'],'gdvCode'=>$L['gdvCode'],'respTo'=>$L['respTo'],'grName'=>$L['grName']);
		}
		unset($L['respName'],$L['gdvCode'],$L['gc'],$L['pj'],$L['pn'],$L['si']);
		$Mx['Resp'][$kr]['L'][] = $L;
	}
		$js = json_encode($Mx);
	echo $js;
}
else if(_0s::$router=='GET resp/calendar'){
	$q = pTri::resp_qu($___D,array('view'=>'calendar'));
	if(pTri::$err){ die(pTri::$errText); }
	else{
	$Mx = array('Resp'=>array());
	_ADMS::_lb('com/_2d');
	$lastW ='';
	while($L = $q->fetch_assoc()){
		$timeDue = $L['dueDate'];
		$w = (_2d::g('z',$timeDue));
		$L['yzId'] = _2d::g('Y',$timeDue).'_'.$w;//+1 por javascript
		$lastW = $w;
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$kr = $L['yzId'].'_'.$L['respId'];
		$verif = ($k == '0-9')? substr($nit,-1)*1 : substr($nit,-2);
		$L['verif'] = $verif;
		$L['subGr'] = $DIAN_VERIF[$k][$verif];
		if(!array_key_exists($kr,$Mx['Resp'])){
			$Mx['Resp'][$kr] = array('respName'=>$L['respName'],'gdvCode'=>$L['gdvCode'],'gc'=>$L['gc'],'pj'=>$L['pj'],'pn'=>$L['pn'],'si'=>$L['si']);
		}
		$Mx['Resp'][$kr]['yzId'] = $L['yzId'];
		$Mx['Resp'][$kr]['total'] += 1;
		unset($L['gdvCode'],$L['gc'],$L['pj'],$L['pn'],$L['si']);
		$Mx['Resp'][$kr]['L'][] = $L;
	}
		$js = json_encode($Mx);
	}
	echo $js;
}
else if(_0s::$router=='GET resp/byCard'){
	$q = pTri::resp_qu($___D);
		if(pTri::$err){ die(pTri::$errText); }
	$Mx = array('Card'=>array());
	$Us = array();
	while($L = $q->fetch_assoc()){
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$kr = $L['cardId'];
		$subGr = $DIAN_VERIF[$k][$num];
		$L['verif'] = $L['dvSys'];
		$L['subGr'] = $subGr;
		if(!array_key_exists($kr,$Mx['Card'])){
			$Mx['Card'][$kr] = array('cardId'=>$L['cardId'],'cardName'=>$L['cardName'],'licTradNum'=>$L['licTradNum'],'userAssg'=>$L['userAssg'],'userAssgName'=>$L['userAssgName'],'RF_regTrib'=>$L['RF_regTrib'],'RF_tipEnt'=>$L['RF_tipEnt']);
		}
		$Us[$L['userAssg']] = $L['userAssg'];
		$Mx['Card'][$kr]['L'][] = array('dueDate'=>$L['dueDate'],'respStatus'=>$L['respStatus'],'respId'=>$L['respId'],'respName'=>$L['respName'],'respYear'=>$L['respYear'],'grName'=>$L['grName']);
	}
	$js = json_encode($Mx);
	echo $js;
}

else if(_0s::$router=='GET resp/status'){
	if($js=_js::ise($___D['respId'],'Se debe definir ID de responsabilidad')){ die($js); }
	else if($js=_js::ise($___D['cardId'],'Se debe definir ID del cliente')){ die($js); }
	$M = a_sql::fetch('SELECT * FROM gt1_rtr2 R2  WHERE R2.respId=\''.$___D['respId'].'\' AND R2.cardId=\''.$___D['cardId'].'\'LIMIT 1',array(1=>'Error consultado estado de responsabilidad: ',2=>'No se encontraron actualizaciones en el estado.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{
		$qu = a_sql::query('SELECT * FROM '._0s::$Tb['gt1_rtr99'].' R99  WHERE R99.respId=\''.$___D['respId'].'\' AND R99.cardId=\''.$___D['cardId'].'\'LIMIT 30',array(1=>'Error consultado estados de responsabilidad.',2=>'No se encontraron actualizaciones en el estado.'));
		$M['L']=array();
		if(a_sql::$err){ $$M['L']=json_decode(a_sql::$errNoText); }
		while($F = $qu->fetch_assoc()){
			$M['L'][]=$F;
		}
		$js=_js::enc($M,'just');
	}
	echo $js;
}
else if(_0s::$router=='PUT resp/status'){
	if($js=_js::ise($___D['respId'],'No se encontró relación a ninguna responsabilidad. '.$___D['respId'])){}
	else if($js=_js::ise($___D['cardId'],'No se encontró relación a ningún cliente. ')){}
	else if($js=_js::ise($___D['respStatus'],'Se debe definir un estado.')){}
	else{
		$ins = a_sql::insert($___D,array('kui'=>'uid_dateC','tbk'=>'gt1_rtr2','wh_change'=>'WHERE cardId=\''.$___D['cardId'].'\' AND respId=\''.$___D['respId'].'\' LIMIT 1'));
		if($ins['err']){ $js = _js::e(1,'Error actualizando estado: '.$ins['text']); }
		else{
			$ins = a_sql::insert($___D,array('qDo'=>'insert','kui'=>'uid_dateC','tbk'=>'gt1_rtr99'));
			if($ins['err']){ $js = _js::e(1,'Error guardando registro histórico: '.$ins['text']); }
			else{ $js = _js::r('Estado actualizado correctamente.'); }
		}
	}
	echo $js;
}


else if($ADMS_KEY == 'Anex'){
if($ADMS_MET =='GET'){
	_ADMS::_lb('sql/filter');
	$qu = a_sql::query('SELECT * FROM '._0s::$Tb['gt1_rtr3'].' R3  WHERE R3.respId=\''.$D['respId'].'\' LIMIT 100');
	if(a_sql::$errNo == 1){ $js = _js::e(1,$qu['error_sql']); }
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron anexos.'); }
	else{
		while($F = $qu->fetch_assoc()){
			$js .= a_sql::JSON($F,'  ').','; 
		}
		$js = '{"Data":['.substr($js,0,-1).'
]
}';
	}
	echo $js;
}
else if($ADMS_MET =='PUT'){
	$respId = $D['respId'];
	if(!a_ses::$isSuper){ echo _js::e(4,'Su usuario no tiene permisos para modificar está información (supersu)'); return ''; }
	if(_js::isse($respId)){ $js = _js::e(3,'No se encontró relación a ninguna responsabilidad. '.$respId); }
	else{
		$err = 0;
		foreach($D['L'] as $ln => $D2){
			$b1 = _js::isse($D2['lineMemo']);
			$b2 = _js::isse($D2['linkTo']);
			if($b1 && $b2){ unset($D['L'][$ln]); }
			else if(!$b1 && $b2){ $js = _js::e(3,'#'.$ln.': El enlace debe estar definido.'); $err++; break; }
			else if($b1 && !$b2){ $js = _js::e(3,'#'.$ln.': Especifique los detalles'); $err++; break; }
		}
		if($err == 0){
			$ro = 0;
			foreach($D['L'] as $ln => $D2){
				$D2['respId'] = $respId; $D2['lineNum'] = $ln;
				$ins = a_sql::insert($D2,array('table'=>_0s::$Tb['gt1_rtr3'],'wh_change'=>'WHERE respId=\''.$respId.'\' AND lineNum=\''.$ln.'\' LIMIT 1'));
				if($ins['err']){ $js = _js::e(1,$ins['err']['error_sql']); $err++; break; }
				if(!$D2['delete']){ $ro++; }
			}
			if($err == 0){
				$js = _js::r('Se relacionaron '.$ro.' anexos a la responsabilidad.');
			}
		}
	}
	echo $js;
}
}
else if($ADMS_KEY == 'Not'){
if($ADMS_KEYo[1] == 'byCard'){
	if(!_js::isse($D['emailNotifTo'])){
		_ADMS::_lb('com/_ocrd_emails'); $emailIns = $D['emailNotifTo']; unset($D['emailNotifTo']);
	}
	$q = pTri::resp_qu($D,array('allCard'=>true,'view'=>'notify'));
	if(a_sql::$errNo == 1){ $js = _js::e(1,'Error obteniendo responsabilidades: '.$q['error_sql']);}
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron responsabilidades.');}
	else{
	$Mx = array('Card'=>array());
	$Us = array();
	$today= date('Y-m-d');
	$todaytime = strtotime($today);
	$rows = 0;
	$n=0;
	$D['daysBef'] = 2; 
	$Mx = array('Card'=>array());
	while($L = $q->fetch_assoc()){
		//if($n==5){ break; }$n++;
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$kr = $L['cardId'];
		$subGr = $DIAN_VERIF[$k][$num];
		$L['verif'] = $L['dvSys'];
		$L['subGr'] = $subGr;
		$notDate = ($D['daysBef'])? date('Y-m-d',strtotime($L['dueDate'])-(86400*$D['daysBef'])) : $L['dueDate'];
		/* no enviar si ya vencio o notifico hoy mismo */
		if($L['dueDate']<$today){ continue; }
		$lastNot = a_sql::fetch('SELECT notDate  FROM '._0s::$Tb['gt1_onot'].' WHERE respId=\''.$L['respId'].'\' AND cardId=\''.$L['cardId'].'\' ORDER BY notDate DESC LIMIT 1 ');
		if(a_sql::$errNo==-1 && $lastNot['notDate']==$today){ continue; }
		$lastNotDate = ($lastNot['notDate']!='')?$lastNot['notDate']:'';
		$L['cardName'] = $L['cardName'];
		$RS = 'RS';
		if(!array_key_exists($kr,$Mx['Card'])){
			$Mx['Card'][$kr] = array('cardId'=>$L['cardId'],'cardName'=>$L['cardName'],'licTradNum'=>$L['licTradNum'],'userAssg'=>$L['userAssg'],'userAssgName'=>$L['userAssgName'],'RF_regTrib'=>$L['RF_regTrib'],'RF_tipEnt'=>$L['RF_tipEnt']);
			if($emailIns!=''){
				if(preg_match('/^userAssg/',$emailIns)){
					$userAssg = a_ses::U_getInfo($L['userAssg'],'userEmail');
					if(array_key_exists('userEmail',$userAssg)){
						$Mx['Card'][$kr]['emailTo'] = $userAssg['userEmail'].',';
						$Mx['Card'][$kr]['userEmail'] = $userAssg['userEmail'];
					}
				}
				$emails = _ocrd_emails::get($L['cardId'],array('r'=>'plain','wh'=>array('workerType(E_in)'=>$emailIns)));
				if($emails['emails']){
					$Mx['Card'][$kr]['emailTo'] .= $emails['emails'];
				}
			}
			//$Mx['Card'][$kr]['emailTo'] = 'jromdev@gmail.com';
		}
		$daysToDue = (strtotime($L['dueDate'])-$todaytime)/86400;
		$Rs = '&RS['.$L['respId'].']';
		$Mx['Card'][$kr]['Lsend'] .= $Rs.'[daysToDue]='.$daysToDue.$Rs.'[notDate]='.$today.$Rs.'[dueDate]='.$L['dueDate'];
		$Mx['Card'][$kr]['L'][] = array('daysToDue'=>$daysToDue,'lastNotDate'=>$lastNotDate,'dueDate'=>$L['dueDate'],'respStatus'=>$L['respStatus'],'respId'=>$L['respId'],'respName'=>$L['respName']);
	}
	$js = json_encode($Mx);
	}
	echo $js;
}
else if($ADMS_KEYo[1] == 'send'){
	if(count($D['RS'])==0){ $js = _js::e(3,'No se encontraron notificaciones para enviar.'); }
	else{
		foreach($D['RS'] as $k=>$V){ $respIds .= $k.','; }
		$respIds = substr($respIds,0,-1);
		$q = a_sql::query('SELECT R0.respId, R0.respName FROM gt1_ortr R0 WHERE R0.respId IN ('.$respIds.') LIMIT 600');
		if(a_sql::$errNo==1){ $js = _js::e(1,'Error obteniendo responsabilidades para notificación: '.$q['error_sql']); }
		else if(a_sql::$errNo==2){ $js = _js::e(2,'No se encontraron responsabilidades para notificar.'); }
		else{
			$U = array();
			while($L=$q->fetch_assoc()){
				$ku = $D['userAssg'];
			if(!array_key_exists($ku,$U)){
				$U[$ku] = array('cardName'=>$D['cardName'],'mailTo'=>$D['mailTo']);
			}
			$kc = $D['cardId'].'-'.$L['respId'];
			$tR = $D['RS'][$L['respId']];
			$U[$ku]['R'][$kc] = array('daysToDue'=>$tR['daysToDue'],'dueDate'=>$tR['dueDate'],'notDate'=>$tR['notDate'],'respId'=>$L['respId'],'respName'=>$L['respName'],'respStatus'=>$L['respStatus']);
			}
		}
		if(count($U)>0){
		_ADMS::_lb('lb/_Mailer');
		foreach($U as $userId =>$L2){
		$D['body'] = mailTem_user($L2['R'],$D);
		$D['fromName'] = 'DYD Consultores y Asesores S.A.S.'; $D['userAssgName'];
		$D['fromEmail'] = 'dydplatform@admsystems.co'; $D['userEmail'];
		$D['replyTo'] = $D['userAssgName'].' <'.$D['userEmail'].'>';
		$s = _Mail::send($D);
		if($s['errNo']){ $js = _js::e(3,'No se envió el correo a los destinatarios ('.$D['cardName'].'). '.$s['text']); }
		else{
			foreach($L2['R'] as $respId=>$L3){
				$upd = a_sql::insert(array('cardId'=>$D['cardId'],'respId'=>$L3['respId'],'notDate'=>$L3['notDate'],'dueDate'=>$L3['dueDate']),array('POST'=>1,'table'=>_0s::$Tb['gt1_onot']));
			}
			$js = _js::r('Correo enviado correctamente.','"cardId":"'.$D['cardId'].'"');
			}
		}
		}
	}
	echo $js;
}

}



/* Version empresa*/
else if($ADMS_KEY == 'Pyme'){
if($ADMS_KEYo[1] == 'list'){
	$q = pTri::resp_qu($D);
	if(a_sql::$errNo == 1){ $js = _js::e(1,'Error obteniendo responsabilidades: '.$q['error_sql']);}
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron responsabilidades.');}
	else{
	$Mx = array('Resp'=>array());
	while($L = $q->fetch_assoc()){
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$kr = $L['respId'];
		$subGr = $DIAN_VERIF[$k][$num];
		$L['verif'] = $L['dvSys'];
		$L['subGr'] = $subGr;
		$js .= a_sql::JSON($L).',';
	}
		$js = '{"L":['.substr($js,0,-1).']}';
	}
	echo $js;
}
else if($ADMS_KEYo[1] == 'month'){
	$q = pTri::resp_qu($D,array('view'=>'calendar'));
	if(a_sql::$errNo == 1){ $js = _js::e(1,'Error obteniendo responsabilidades: '.$q['error_sql']);}
	else if(a_sql::$errNo == 2){ $js = _js::e(2,'No se encontraron responsabilidades.'); }
	else{
	$Mx = array('Resp'=>array());
	_ADMS::_lb('com/_2d');
	$lastW ='';
	while($L = $q->fetch_assoc()){
		$timeDue = $L['dueDate'];
		$w = (_2d::g('z',$timeDue));
		$L['yzId'] = _2d::g('Y',$timeDue).'_'.$w;//+1 por javascript
		$lastW = $w;
		$nit = $L['nit'];
		$k = $L['gdvCode'];
		$kr = $L['yzId'].'_'.$L['respId'];
		$verif = ($k == '0-9')? substr($nit,-1)*1 : substr($nit,-2);
		$L['verif'] = $verif;
		$L['subGr'] = $DIAN_VERIF[$k][$verif];
		$js .= a_sql::JSON($L).',';
	}
		$js = '{"L":['.substr($js,0,-1).']}';
	}
	echo $js;
}

}


?>