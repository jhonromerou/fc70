<?php
if(_0s::$router== 'GET rt/assg'){
	if($js=_js::ise($___D['year'],'Se debe seleccionar un año para continuar.')){}
	else if($js=_js::ise($___D['cardId'],'Se debe definir ID de cliente.')){}
	else{
	$year = $___D['year']; $cardId = $___D['cardId'];
	$errNo_ = array('1'=>'Error en RT: ','2'=>'No se encontraron resultados.');
	$qu = a_sql::query('SELECT A.*, cA.grPerId asignado FROM '._0s::$Tb['gt1_ogrt'].' A
LEFT JOIN '._0s::$Tb['gt1_grt1'].' cA ON (cA.grId=A.grId AND cA.year=\''.$year.'\' AND cA.cardId=\''.$cardId.'\')
 ORDER BY A.grPrim ASC, A.grName ASC ', array('errNo'=>$errNo_));
	if(is_array($qu) && $qu['errNoText']){ $js = $qu['errNoText']; }
	else{
		$M=array('L'=>array());
			while($L = $qu->fetch_assoc()){
			if(is_numeric($L['asignado'])){ $L['checked'] = 'Y'; }
			$M['L'][]=$L;
		}
		$js = _js::enc($M,'just');
	}
	}
	echo $js;
}

else if(_0s::$router== 'PUT rt/assg'){
	if($js=_js::ise($___D['year'],'Se debe seleccionar un año para continuar.')){}
	else if($js=_js::ise($___D['cardId'],'No se encontró el cliente al que se le asignarán los grupos.')){}
	else{
		$year = $___D['year']; $cardId = $___D['cardId'];
		$D2 = array('year'=>$year,'cardId'=>$cardId);
		$wh1 = 'WHERE year=\''.$year.'\' AND cardId=\''.$cardId.'\' ';
		$ln = 0;
		foreach($___D['GR'] as $grId => $chk){ unset($D2['delete']);
			if($chk == 'N'){ $D2['delete'] = 'Y'; $ln--; }
			$D2['grId'] = $grId;
			$ins = a_sql::insert($D2,array('tbk'=>'gt1_grt1', 'wh_change'=>$wh1.' AND grId=\''.$grId.'\' LIMIT 1'));
			if($ins['err']){ $js = _js::e(1,$ins['err']['error_sql']); break; }
			else{ $ln++; }
		}
		if($ln>0){ $js = _js::r('Año ('.$year.'): Se asignaron '.$ln.' responsabilidades al cliente.'); }
		else{ $js = _js::r('Año ('.$year.'): Actualizado, pero ninguna responsabilidad definida para el contacto.'); }
	}
	echo $js;
}

else if($ADMS_KEY == 'vYear'){
if($ADMS_MET =='GET'){
	if(_js::isse($D['year'])){ echo _js::e(3,'Se debe seleccionar un año para continuar.'); return ''; }
	else if(_js::isse($D['cardId'])){ echo _js::e(3,'No se encontró el cliente al que se le asignarán los grupos.'); return ''; }
	$year = $D['year']; $cardId = $D['cardId'];
	$errNo_ = array('1'=>'Error en RT: ','2'=>'No se encontraron resultados.');
	$qu = a_sql::query('SELECT * FROM '._0s::$Tb['par_03_vtr'].' WHERE year=\''.$year.'\' AND cardId=\''.$cardId.'\' ', array('errNo'=>$errNo_));
	if(is_array($qu) && $qu['errNoText']){ $js = $qu['errNoText']; }
	else{
		while($L = $qu->fetch_assoc()){
			$js .= '"'.$L['varCode'].'":"'.$L['varValue'].'",';
		}
		$js = '{'.substr($js,0,-1).'}';
	}
	echo $js;
}
else if($ADMS_MET =='PUT'){
	if(_js::isse($D['year'])){ $js = _js::e(3,'Se debe seleccionar un año para continuar.'); }
	else if(_js::isse($D['cardId'])){ $js = _js::e(3,'No se encontró el cliente al que se le asignarán las variables.'); }
	else{
		$year = $D['year']; $cardId = $D['cardId'];
		$D2 = array('year'=>$year,'cardId'=>$cardId);
		$wh1 = 'WHERE year=\''.$year.'\' AND cardId=\''.$cardId.'\' ';
		$ln = 0;
		foreach($D['Va'] as $varCode => $Val){
			$D2['varCode'] = $varCode;
			$D2['varValue'] = $Val; unset($D2['delete']);
			if($Val==''){ $D2['delete'] = 'Y'; $ln--; }
			$ins = a_sql::insert($D2,array('table'=>_0s::$Tb['par_03_vtr'], 'wh_change'=>$wh1.' AND varCode=\''.$varCode.'\' LIMIT 1'));
			if($ins['err']){ $js = _js::e(1,$ins['err']['error_sql']); break; }
			else{ $ln++; }
		}
		if($ln>0){ $js = _js::r('Año ('.$year.'): Se asignaron '.$ln.' variables al cliente.'); }
		else{ $js = _js::e(3,'Año ('.$year.'): Ningun dato almacenado.'); }
	}
	echo $js;
}
}

?>
