<?php
function revDoc($_J){
	if(_js::iseErr($_J['demandante'],'Se debe definir el demandante')){}
	else if(_js::iseErr($_J['demandado'],'Se debe definir el demandado')){}
	else if(_js::iseErr($_J['dateEjecu'],'Se debe definir la fecha de Ejecutoria')){}
	else if(_js::iseErr($_J['datePagoAprox'],'Se debe definir la fecha aproximada de pago')){}
	else if(_js::iseErr($_J['salarioYear'],'Se debe definir la fecha aproximada de pago')){}
	else if(_js::iseErr($_J['tipoInt'],'Se debe definir el Tipo de Interes')){}
}
function revLine($_J,$ln){
	if(_js::iseErr($_J['prsName'],$ln.'Se debe definir el nombre de la persona')){}
	else if(_js::iseErr($_J['quantity'],$ln.'Se debe definir la cantidad de salarios','numeric>0')){}
}

require('lib.php');

if(_0s::$router=='GET dse'){ //a_ses::hashKey('geo.remi.read');
	_ADMS::lib('iDoc');
	$_GET['fromA']='A.* FROM dsp_odse A ';
	echo iDoc::get($_GET);
}
else if(_0s::$router=='GET dse/proy'){ //a_ses::hashKey('geo.remi.read');
	echo a_sql::queryL('SELECT A.* FROM dsp_dse2 A WHERE A.docEntry=\''.$_GET['docEntry'].'\' ORDER BY A.numPer ASC');
}

else if(_0s::$router=='GET dse/form'){ //a_ses::hashKey('geo.remi.read');
	_ADMS::lib('iDoc');
	$_GET['fromA']='A.* FROM dsp_odse A ';
	$_GET['fromB']='B.* FROM dsp_dse1 B';
	echo iDoc::getOne($_GET);
}
else if(_0s::$router=='GET dse/view'){ //a_ses::hashKey('geo.remi.read');
	_ADMS::lib('iDoc');
	$_GET['fromA']='A.* FROM dsp_odse A ';
	$_GET['fromB']='B.* FROM dsp_dse1 B';
	echo iDoc::getOne($_GET);
}
else if(_0s::$router=='POST dse'){
	revDoc($_J);
	if(_err::$err){ $js=_err::$errText; }
	else{
		$L=$_J['L']; unset($_J['L']);
		a_sql::transaction(); $cmt=false;
		$docEntry=a_sql::qInsert($_J,array('tbk'=>'dsp_odse','qk'=>'ud'));
		if(a_sql::$err){ $errs=1; $js=_js::e(3,'Error guardando información: '.a_sql::$errText); }
		else{
			$qI=array();
			if(is_array($L)){ $nl=1;
				foreach($L as $X){
					revLine($X,'Linea '.$nl.': '); $nl++;
					if(_err::$err){ $js=_err::$errText; break; }
					$X[0]='i';
					$X[1]='dsp_dse1';
					$qI[]=$X;
				}
				a_sql::multiQuery($qI,0,array('docEntry'=>$docEntry));
				if(_err::$err){ $errs=1; $js=_err::$errText; }
			}
			
		}
		if($errs==0){ $cmt=true;
			$js=_js::r('Sentencia Creada correctamente.','"docEntry":"'.$docEntry.'"');
		}
		a_sql::transaction($cmt);
	}
	echo $js;
}
else if(_0s::$router=='PUT dse'){
	if(_js::iseErr($_J['docEntry'],'Se debe definir Id de documento','numeric>0')){ die(_err::$errText); }
	a_sql::transaction(); $cmt=false;
	if($_J['updSimple']=='Y'){
		$_J=a_sql::fetch('SELECT * FROM dsp_odse WHERE docEntry=\''.$_J['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo sentencia para actualización rápida.',2=>'La sentencia no existe para actualización rapida.'));
		if(a_sql::$err){ $js=a_sql::$errNoTexT; $errs=1; }
		else{
			$_J['L']=array();
			$q=a_sql::query('SELECT * FROM dsp_dse1 WHERE docEntry=\''.$_J['docEntry'].'\' ',array(1=>'Error obteniendo personas de sentencia para actualización rápida.',2=>'La sentencia no tiene registradas personas para actualización rapida.'));
			if(a_sql::$err){ $js=a_sql::$errNoTexT; $errs=1; }
			else{ while($L=$q->fetch_assoc()){ $_J['L'][]=$L; } }
		}
	}
	revDoc($_J); $errs=0;
	if(_err::$err){ $js=_err::$errText; $errs=1; }
	if($errs==0){
		$qf=a_sql::query('DELETE FROM dsp_dse2 WHERE docEntry=\''.$_J['docEntry'].'\' ',array(1=>$ln.'Error eliminando datos de calculo de intereses.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; $errs=1; }
	}
	if($errs==0){
		$docEntry=$_J['docEntry'];
		$L=$_J['L']; unset($_J['L']);
		$_J=setPeriodosDia($_J);
		if(_err::$err){ $js=_err::$errText; $errs=1;}
		else{
			$L2=$_J['L2']; unset($_J['L2']);
			a_sql::qUpdate($_J,array('tbk'=>'dsp_odse','wh_change'=>'docEntry=\''.$docEntry.'\' LIMIT 1'));
			if(a_sql::$err){ $errs=1; $js=_js::e(3,'Error guardando información: '.a_sql::$errText); }
			else{
				$qI=array(); $salarios=0;
				foreach($L as $X){ $salarios+=$X['quantity']; }
				$balBase=$_J['valorSentencia']/$salarios;
				$intBase=$_J['intTotal']/$salarios;
				if(is_array($L)){ $nl=1;
					foreach($L as $X){ $ln='Linea '.$nl.': ';
						revLine($X,$ln); $nl++;
						if(_err::$err){ $js=_err::$errText; $errs=1; break; }
						$X[0]='i';
						$X[1]='dsp_dse1';
						$X['docEntry']=$docEntry;
						$X['_unik']='id';
						$X['balParcial']=$balBase*$X['quantity'];
						$X['priceLine']=$intBase*$X['quantity'];
						$qI[]=$X;
					}
					if($errs==0){
						$qI[]=array('u','dsp_odse','salarioNums'=>$salarios,'_wh'=>'docEntry=\''.$docEntry.'\' LIMIT 1');
						a_sql::multiQuery($qI);
						if(_err::$err){ $errs=1; $js=_err::$errText; }
					}
					if($errs==0){
						a_sql::multiQuery($L2);
						if(_err::$err){ $errs=1; $js=_err::$errText; }
					}
				}
				
			}
			if($errs==0){ $cmt=true;
				$js=_js::r('Información guardada correctamente.','"docEntry":"'.$docEntry.'"');
			}
		}
	}
	a_sql::transaction($cmt);
	echo $js;
}

else if(_0s::$router=='GET dse/put'){
	$_J=a_sql::fetch('SELECT * FROM dsp_odse WHERE docEntry='.$_GET['docEntry'].' LIMIT 1');
	if(_js::iseErr($_J['docEntry'],'Se debe definir Id de documento','numeric>0')){ die(_err::$errText); }
	$errs=0;
	revDoc($_J);
	a_sql::transaction(); $cmt=false;
	if(_err::$err){ $js=_err::$errText; $errs=1; }
	else{
		$qf=a_sql::query('DELETE FROM dsp_dse2 WHERE docEntry=\''.$_J['docEntry'].'\' ',array(1=>$ln.'Error eliminando datos de calculo de intereses.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; $errs=1; }
	}
	if($errs==0){
		$docEntry=$_J['docEntry'];
		$L=$_J['L']; unset($_J['L']);
		$_J=setPeriodosDia($_J);
		if(_err::$err){ $js=_err::$errText; $errs=1;}
		else{
			$L2=$_J['L2']; unset($_J['L2']);
			a_sql::qUpdate($_J,array('tbk'=>'dsp_odse','wh_change'=>'docEntry=\''.$docEntry.'\' LIMIT 1'));
			if(a_sql::$err){ $errs=1; $js=_js::e(3,'Error guardando información: '.a_sql::$errText); }
			else{
				$qI=array(); $salarios=0;
				if(is_array($L)){ $nl=1;
					foreach($L as $X){ $ln='Linea '.$nl.': ';
						revLine($X,$ln); $nl++;
						if(_err::$err){ $js=_err::$errText; $errs=1; break; }
						$X[0]='i';
						$X[1]='dsp_dse1';
						$X['docEntry']=$docEntry;
						$X['_unik']='id';
						$salarios+=$X['quantity'];
						$qI[]=$X;
					}
					if($errs==0){
						$qI[]=array('u','dsp_odse','salarioNums'=>$salarios,'_wh'=>'docEntry=\''.$docEntry.'\' LIMIT 1');
						a_sql::multiQuery($qI);
						if(_err::$err){ $errs=1; $js=_err::$errText; }
					}
					if($errs==0){
						a_sql::multiQuery($L2);
						if(_err::$err){ $errs=1; $js=_err::$errText; }
					}
				}
				
			}
			if($errs==0){ $cmt=true;
				$js=_js::r('Información guardada correctamente.','"docEntry":"'.$docEntry.'"');
			}
		}
	}
	a_sql::transaction($cmt);
	echo $js;
}


else if(_0s::$router=='PUT dse3'){
	if(_js::iseErr($_J['docEntry'],'Se debe definir Id de documento','numeric>0')){ die(_err::$errText); }
	revDoc($_J);
	if(_err::$err){ $js=_err::$errText; }
	$Mx=setPeriodos($_J,$_J['L']);
	print_r($Mx);
	echo $js;
}

else if(_0s::$router=='GET dse/sorForm'){ //a_ses::hashKey('geo.remi.read');
	_ADMS::lib('iDoc');
	$_GET['fromA']='A.* FROM dsp_odse A ';
	echo iDoc::getOne($_GET);
}
else if(_0s::$router=='GET dse/sorView'){ //a_ses::hashKey('geo.remi.read');
	_ADMS::lib('iDoc');
	$_GET['fromA']='A.* FROM dsp_odse A ';
	$_GET['fromB']='B.* FROM dsp_dse1 B';
	echo iDoc::getOne($_GET);
}
else if(_0s::$router=='PUT dse/sorForm'){
	if(_js::iseErr($_J['docEntry'],'Se debe definir Id de documento','numeric>0')){ die(_err::$errText); }
	$errs=0;
	a_sql::transaction(); $cmt=false;
	if(_err::$err){ $js=_err::$errText; $errs=1; }
	if($errs==0){
		$docEntry=$_J['docEntry'];
		$L=$_J['L']; unset($_J['L']);
		if(_err::$err){ $js=_err::$errText; $errs=1;}
		else{
			$L2=$_J['L2']; unset($_J['L2']);
			a_sql::qUpdate($_J,array('tbk'=>'dsp_odse','wh_change'=>'docEntry=\''.$docEntry.'\' LIMIT 1'));
			if(a_sql::$err){ $errs=1; $js=_js::e(3,'Error guardando información: '.a_sql::$errText); }
			if($errs==0){ $cmt=true;
				$js=_js::r('Información guardada correctamente.','"docEntry":"'.$docEntry.'"');
			}
		}
	}
	a_sql::transaction($cmt);
	echo $js;
}



?>