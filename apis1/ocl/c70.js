$V.cvtCondicGen='Tiempo de entrega: \nPedido Mínimo: 20 Pares';
$V.cvtDefPayGrText='';
$V.ordTypePE['LB']='Libre';
$V.crdAddrWin='Y';
Api.Gvt.b70='/v/gvt2fc70/';
$TXT.itemSize='Talla';
$Doc.a['ivtMov'].docT='Cambios';
$M.li['ivt.mov'].t='Cambios';
$M.li['ivt.mov.form'].t='Cambios';

$M.iniSet={
nty:'N',help:'N',menu:'L'};

$oB.push($V.docTT,[
{k:'gvtPor',v:'Orden de Compra'},{k:'gvtPdn',v:'Entrada por Compra'},
]);

$M.liTable([
{folId:'crd',folName:'Terceros',ico:'fa fa-handshake-o',folColor:'purple',MLis:['crd.c','crd.s'],_F:[
	{mas:['jsv.parGrC','tb.oslp','jsv.parCprPos','jsv.parDpto']}
]},
{mdlActive:'crdNov',fatherId:'crd',folId:'crdNov',folName:'Seguimiento',ico:'fa fa-heartbeat',folColor:'var(--blue)',
	MLis:['crdNov.agenda','crdNov'],_F:[
	{mas:['jsv.crdNovType','jsv.crdNovOri','jsv.crdNovPrio']},
	{rep:['crdRep.nov', 'crdRep.novTask']}
]},

{folId:'ivt',folName:'Inventario',ico:'fa fa-truck',MLis:['itmProfile','ivtStock.mmr','barcode.stickers'],_F:[
	{folId:'itm',folName:'Artículos',MLis:['itm.p','itm.mp','itm.se','itmSub','itmSub.gr','ipc','ipc.mpDiff']},
	{folId:'doc',folName:'Operaciones de Stock',MLis:['ivtWht','ivtIng','ivtEgr','ivt.cat','ivt.mov','ivt.cph','ivtAwh']},
	{folId:'rep',folName:'Reportes',ico:'fa fa-bolt',MLis:['ivtStock.p','ivtStock.withPeP','ivtStock.mp','ivtStock.pHistory','ivtStock.mpHistory','ivtRep.ivtBal','ivtRep.kardex','ivtRep.rota']},
	]},
{folId:'gvtSell',folName:'Ventas',ico:'fa fa-tags',folColor:'blue',MLis:['gvtOcvt','sellOrd.get','sellOrd.formLib','sellOrd.p','sellDlv.get','gvtRdn.get'],_F:[
		{folId:'tools',folName:'Herramientas',ico:'fa fa-magic',MLis:['sellDlv.labelPacking']},
		{rep:['gvtRep.ger','gvtPvt.repOpenQty','gvtPvt.repStockPeP','gvtPvt.repItemCanceled','gvtRdn.repStatus','gvtRep.dlv1']}
	]},
	{folId:'gvtPur',folName:'Compras',ico:'fa fa-shopping-cart',folColor:'red',MLis:['crd.s'],_F:[
		{doc:['gvtPor','gvtPdn','gvtPin']}
	]},
{mdlActive:'sgc',folId:'sgc',folName:'S.G.C',ico:'fa fa-line-chart',
	MLis:['sgcAcm','sgcPqr'],
	_F:[
		{mas:['jsv.sgcAcmClass','jsv.sgcWp','jsv.sgcPqrClass','jsv.sgcPqrClassL']},
		{mdlActive:'sgc',rep:['sgcRep.acm','sgcRep.pqr']},
	]},
{folId:'wma',folName:'Produccion',ico:'iBg iBg_produccion',_F:[
		{folId:'pdp',folName:'Planificación',MLis:['wmaPdp','wmaPdp.consol','wmaPdp.consolGroup','wmaPdp.auxCumpProd','wmaPdp.corteProg']},
		{folId:'odp',folName:'Orden Producción',MLis:['wma3.oodp','wmaOdp.docHistory','wma3.odp.tbFase']},
		{folId:'gfa',folName:'Gestión de Fases',MLis:['wmaDdf','wma3.dcf']},
		{folId:'bom',folName:'Escandallos',MLis:['wmaMpg','wmaBom']},
		{folId:'mrp',folName:'MRP',MLis:['wmaMrp.fromPdp','wmaMrp.fromPep','wmaMrp.fromOdp']},
		{rep:['wmaRep.bom','pepRep.dcfValue','pepRep.handAt','pepRep.stockValue']}
	]},

{folId:'repro',folName:'Reproceso', folColor:'red', ico:'fa fa-puzzle-piece',MLis:['reproDoc'], _F:[
	{rep:['reproRep.doc']},
	{mas:['jsv.reproDocParte', 'jsv.reproDocCausa', 'jsv.reproDocSolucion']},
]
},
{folId:'pep',folName:'Prod. en Proceso',ico:'fa fa-cubes',folColor:'orange',MLis:['pepWhs','pepWhs.history','pep.lopCat','pepWht','pepIng','pepEgr','pepAwh','pepMov'], _F:[
		{rep:['pepRep.handAt','pepRep.stockValue']}
	]},
{folId:'sys',folName:'Sistema',ico:'fa fa-user-secret',folColor:'green'},
{fatherId:'sys',folId:'itf',folName:'Interfaces',ico:'fa fa-database',_F:[
	{folId:'DT',folName:'Siigo',ico:'ico_xls',MLis:['itfST.siigo.ingProd','itfST.siigo.ivtEgr','itfST.siigo.gvtPor',,'itfST.siigo.gvtPin']},
	{folId:'mas',folName:'Importaciones',
		MLis:['itfDT.c70.pvtCreate','itfDT.dlvNews','itfDT.crdF','itfDT.ivtAwh','itfDT.pepAwh']
	},
]},
{fatherId:'sys',folId:'mast',folName:'Catalogos',ico:'fa fa-key',folColor:'red',_F:[
		{folId:'ivt',folName:'Inventarios',MLis:['jsv.itmGr','tb.itmOwhs','tb.itmOitp']},
		{folId:'gvt',folName:'Ventas',MLis:['tb.oslp']},
		{folId:'gfi',folName:'Contabilidad',MLis:['gfiPdc','tb.gfiOpym','tb.gfiOcdc','gfiItmGr']},
		{folId:'wma',folName:'Produccion',MLis:['wmaFas','wmaWop','wmaIsv','wmaMaq','wmaCif','jsv.wmaWopGr','sysd.massData.wmaWfa']},
	]},
{fatherId:'sys',folId:'cnf',folName:'Configuración',ico:'fa fa-cog',MLis:['cnf.meusr','cnf.mecrd'],_F:[
	{folId:'cnfUser',folName:'Usuarios',MLis:['cnf.ousr','cnf.ousp','cnf.ousa','cnf.repAssg']}
	]},
]);
/*
$M.sAdd([
{L:[{}]},
{fatherId:'cnf'},
{fatherId:'cnf',L:[,
{fatherId:'cnfUser'},
]);
*/
$M.kauTable([
{fatherId:'crd',rootFolder:'Socios de Negocios', ico:'fa-institution',
L:['crd.c','crd.s','crdNov','crdNov.sup']},
{fatherId:'itm',rootFolder:'Articulos',
	L:['itm.p','itm.mp','itm.se','itm.bc','ipc','ipc.mpDiff'],
},
{fatherId:'repro',rootFolder:'Reproceso',
	L:['reproDoc', 'reproRep'],
},
{fatherId:'sgc',rootFolder:'SGC',ico:'fa fa-line-chart',L:['sgcAcm','sgcAcm.sup','sgcPqr','sgcPqr.sup']},
{fatherId:'adm',rootFolder:'Administración',
 L:['sysd.sumaster','sysd.supersu',
 'gfi.suadmin',
,'admin.reports','itfDT.pepAwh']
},
{fatherId:'rep2',rootFolder:'Reportes 2',
 L:['gvtRep.dlv1','gvtRep.ger']
}
]);

$M.add([
{liA:"itf.siigo", rootFolder:"Siigo",
L:[
	{t:"Interface a Siigo", k:"itf.siigo.templates"},
]
},
{liA:"gvtPur", rootFolder:"Compras",
	L:[
		{t:"Orden de Compra", k:"gvtPor"},
		{t:"Remisión de Compra", k:"gvtPdn"},
		{t:"Factura de Compra", k:"gvtPin"},
	]
},
{liA:'wma',folder:'Planificación',
	L:[
	{t:"MRP",k:"wmaMrp"},
	{t:'Planificación Producción (Crear / Ver)',k:'wma3.opdp.basic'},
	{t:'Planificación Producción (Modificar)',k:'wma3.opdp.edit'},
	{t:'Cerrar Planificación Producción',k:'wma3.opdp.statusClose'},
	{t:'Consolidado Planificación',k:'wma3.opdp.basic'},
	{t:'Orden de Producción (Crear, Ver)',k:'wma3.oodp.basic'},
	{t:'Ord. Producción - Modificar Lineas',k:'wma3Odp.form2'},
	{t:'Anular Orden de Producción',k:'wma3.oodp.statusCancel'},
	{t:'Auxiliar Cumplimiento Producción',k:'wma3.opdp.auxCumpProd'},
	{t:'Corte para Programación',k:'wma3.opdp.corteProg'},
	]
},
/* fases */
{liA:'wma',folder:'Gestión de Fases',
	L:[
{k:'wma3.ddf.read',t:'Documentos de Fase - Visualización'},{k:'wma3.ddf.write',t:'Documentos de Fase (Escritura)'},
{k:'wma3.dcf.read',t:'Documentos Cierre - Lectura'},{k:'wma3.dcf.write',t:'Documentos Cierre - Escritura'},{k:'wma3.dcf.statusC',t:'Documentos Cierre - Anular'},{k:'wma3.dcf.emision',t:'Documentos Cierre - Consumos'}
]
},
]);

$Tpt.T['c70_gvtCvtTemplate']=function(Jr,topWrap){
	$1.T.btnFa({fa:'fa_print',textNode:' Imprimir', func:function(){ $1.Win.print(cont,{margin:'N'}); }},topWrap);
	var cont=$1.t('div',{id:'c70_gvtCvtTemplate'},topWrap);
	var bg7='#006AAE';
	var div=$1.t('div',{'class':'page_fullContent justPrint',style:''},cont);
	$1.t('img',{src:'http://static1.admsistems.com/shareImg/cx70/gvtcvtp1.jpg'},div);
	var div=$1.t('div',{'class':'page_fullContent justPrint'},cont);
	$1.t('img',{src:'http://static1.admsistems.com/shareImg/cx70/gvtcvtp2.jpg'},div);
	//var div=$1.t('div',{'class':'page_fullContent justPrint',style:''},cont);
	var top1=$1.t('div',{'class':'justPrint',style:'width:100%; height:128px; position:relative; marginBottom:14px;'},cont);
	$1.t('img',{src:'gvtcvttop2.jpg',style:'maxHeight:max-content'},top1);
	var tnumb=$1.t('div',{style:'position:absolute;bottom:32; right:40px; font-weight:bold; border:1px solid #424246; color:#424246; padding:4px 8px; fontSize:20px;',textNode:Jr.docEntry},top1);
	//fecha
	var tf=(''+Jr.docDate);
	var textCont=$1.t('div',{'class':'page_textContent'},cont);
	var tbr=$1.t('div',{style:'float:right;'},textCont);
	var sty='display:inline-block; border:0.0625rem solid #000; padding:0.5rem 0.25rem;';
	$1.t('div',{style:sty,textNode:'Fecha '},tbr);
	$1.t('div',{style:sty,textNode:tf.substr(8,2)},tbr);
	$1.t('div',{style:sty,textNode:tf.substr(5,2)},tbr);
	$1.t('div',{style:sty,textNode:tf.substr(0,4)},tbr);
	cont.appendChild($1.t('clear'));
	//datos
	var styH1='backgroundColor:'+bg7+'; color:#FFF; width:8rem;';
	var tb=$1.t('table',{'class':'table_simple table_x100'},textCont);
	var tBody=$1.t('tbody',0,tb);
	var tr=$1.t('tr',0,tBody);
	$1.t('td',{textNode:Jr.cardName,rowspan:4,style:'width:16rem; fontWeight:bold; textAlign:center; verticalAlign:middle; border:none;'},tr);
	var styNbg='border:none; backgroundColor:#FFF;';
	$1.t('td',{textNode:'Contacto',style:styH1},tr);
	$1.t('td',{textNode:Jr.prsCnt,colspan:3,style:styNbg},tr);
	var tr=$1.t('tr',0,tBody);
	$1.t('td',{textNode:'Teléfono',style:styH1},tr);
	$1.t('td',{textNode:Jr.phone,style:styNbg},tr);
	var tr=$1.t('tr',0,tBody);
	$1.t('td',{textNode:'Dirección',style:styH1},tr);
	$1.t('td',{textNode:Jr.address,colspan:3,style:styNbg},tr);
	var tr=$1.t('tr',0,tBody);
	$1.t('td',{textNode:'Correo',style:styH1},tr);
	$1.t('td',{textNode:Jr.email,style:styNbg},tr);
	var h6=$1.t('div',{style:'padding:0.25rem;'},textCont);
	$1.t('b',{textNode:'Asunto'},h6);
	$1.t('span',{textNode:': En conformidad con sus requerimientos, nuestra oferta de productos es'},h6);
	//lineas
	var tb=$1.t('table',{'class':'table_zh table_x100',style:'fontSize:14px;'},textCont);
	var trH=$1.t('tr',0,$1.t('thead',0,tb));
	var styCol='backgroundColor:'+bg7+'; color:#FFF; textAlign:center;'
	$1.t('td',{textNode:'',style:styCol},trH);
	$1.t('td',{textNode:'Referencia',style:styCol},trH);
	$1.t('td',{textNode:'Descripción',style:styCol},trH);
	$1.t('td',{textNode:'Precio',style:styCol},trH);
	$1.t('td',{textNode:'Cant.',style:styCol},trH);
	$1.t('td',{textNode:'Total',style:styCol},trH);
	var tB=$1.t('tbody',0,tb);
	var vAl='verticalAlign:middle;';
	for(var i in Jr.L){ L=Jr.L[i];
		var tr=$1.t('tr',0,tB);
		var td=$1.t('td',{style:'width:140px'},tr);
		$1.t('img',{src:Itm.Txt.imgSrc(L),ttitle:L.src1,style:'width:130px; display:inline-block;'+vAl},td);
		var td=$1.t('td',{style:'width:280px; word-break:break-word;'+vAl},tr);
		$1.t('b',{textNode:' Cod: '+L.itemCode+' '},td);
		$1.t('span',{textNode:' '+L.itemName},td);
		$1.t('td',{node:$1.t('div',{textNode:L.lineMemo,'class':'pre'}),style:vAl},tr);
		$1.t('td',{textNode:$Str.money(L.price),'class':'textNoWrap',style:vAl},tr);
		$1.t('td',{textNode:L.quantity*1,'class':'textNoWrap '+tbCal.trQty,style:'textAlign:center;'+vAl},tr);
		$1.t('td',{textNode:$Str.money(L.priceLine),'class':'textNoWrap',style:vAl},tr);
	}
	var tf=$1.t('tfoot',0,tb);
	var tr=$1.t('tr',{'class':tbCal._row},tf);
	$1.t('td',{textNode:'Total',colspan:3},tr);
	$1.t('td',{'class':tbCal.tbQty},tr);
	$1.t('td',{textNode:$Str.money(Jr.docTotal)},tr);
	tbCal.docTotal(tb,{trsCont:true});
	var bott=$1.t('div',{'class':'tptBottom'},textCont);
	return {bott:bott};
	//
}
