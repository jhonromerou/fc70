$M.iniSet = {
	nty: 'N', help: 'N', menu: 'L',
}
	$M.sAdd([
		{ L: [{ folId: 'crd', folName: 'Terceros', ico: 'fa fa-institution' }] },
		{ fatherId: 'crd', MLis: ['crd.c','cpr' ]},
	]);

	/* $M.sAdd([
 {topFolder:{folId:'crm',folName:'Organizador',ico:'fa fa-briefcase',folColor:'#3ccc47'}},
 {fatherId:'crm',MLis:['crmAgenda','crmTask','crmNov','crmEvents','crmNote']},
 ]); */

	$M.sAdd([
		{ L: [{ folId: 'geDoc', folName: 'Documentos', ico: 'fa fa-file', folColor: '#DD0' }] },
		{ fatherId: 'geDoc', MLis: ['geDoc.cards', 'geDoc.pwork'] }
	]);


	Api._dcp = '/1c/dcp/';
	Api._dcp = '/1c/dcp/';
	if(!$V.docSerieType){ $V.docSerieType = {}; }
if (!_Fi) { _Fi = {}; }
$Xls_extDef = 'xlsx';
$V.SN = { S: 'Si', N: 'No' };
$M.go({ ini: 'gtrib.control' });
$y.sysScrip([{ src: Api._dcp + 'vars/gdv' }]);

$V.crdGroup = ['Ninguno', 'Revisoria Fiscal', 'Asesoria Contable', 'Asesoria Tributaria', 'Auditoria'];
$Mdl.GeDoc_oTy = [{ folId: 'respCard', folName: 'Responsabilidades' }];
$V.RF_regTrib = [{ k: 'RC', v: 'Responsables de IVA' }, { k: 'RS', v: 'No Responsables IVA' }, { v: 'RE', v: 'Regimen Especial' }, { k: 'GC', v: 'Gran Contribuyente' }];
$js.del({ 'winClass': '_financiero' }, $Mdl.Cnf.crdFichaPut);
$Mdl.Cnf.crdReqCode = 'N';
//$Doc.a['fquP.audB']={a:'fquAudB',kl:'cardId'};

$ccH['dcpCal'] = ['#7FFF00', '#00FA9A', '#66CDAA', '#5F9EA0', '#F5DEB3', '#A52A2A',
	'#007BB6', '#B81621', '#222222', '#EE4056', '#CC181E', '#7FFF00',
	'#00FA9A', '#66CDAA', '#5F9EA0', '#F5DEB3', '#A52A2A'
];
$V.crdInfoOn = 'N'; //no mostrar info en cliente
$V.crdCprPosi = [{ k: 'repLeg', v: 'Rep. Legal' }, { k: 'repLeg2', v: 'Rep. Legal Primer Suplente' }, { k: 'repLeg3', v: 'Rep. Legal Segundo Suplente' }, { k: 'gerente', v: 'Gerente' }, { k: 'contador', v: 'Contador' }, { k: 'revFiscal', v: 'Revisoría Fiscal' }, { k: 'revFiscalSup', v: 'Revisoría Fiscal Suplente' }];

$TbV['crdDtyClass'] = { req: 'Requerimiento', cvt: 'Cotización', queja: 'Queja', otro: 'Otro' };
$TbV['crdDtyPrio'] = { none: 'N/A', normal: 'Normal', high: 'Inmediata', medium: 'Media', low: 'Baja' };

$oB.pus('crd', $Opts, [
	{ textNode: 'Asignar Responsabilidad', mTo: 'rt.assg', mPars: ['cardId'] },
	{ textNode: 'Ficha Control Cliente', 'class': 'iBg_docs', mTo: 'dcp.fichaCard', mPars: ['cardId'] }
]);

$M.sAdd([
	{
		L: [
			{ folId: 'gtrib', folName: 'Responsabilidades',ico:'fa fa-briefcase',folColor:'blue'},
			{ folId: 'formsDcp', folName: 'Formularios' },
			{ folId: 'dcpgeDocGCal', MLis: ['dcp.geDocGCal'] },
		]
	},
	{ fatherId: 'gtrib', MLis: ['gtrib.master', 'gtrib.control', 'gtrib.notif'] },
	{ fatherId: 'formsDcp', MLis: ['forms.visita', 'dcp.cnc', 'dcpFormsPpe'] }
]);

$M.add([
	{
		liA: 'socios', rootFolder: 'Terceros',
		L: [{ k: 'crd.c.basic', t: 'Clientes' },
		{ k: 'cpr.basic', t: 'Contactos' },
		{ k: 'rtAssg', t: 'Asignar Responsabilidades' },
		{ k: 'dcpFicha', t: 'Ficha Cliente' },
		{ k: 'crdDry.basic', t: 'Diario de Socio' }]
	},
	{
		liA: 'gtrib', rootFolder: 'Responsabilidades',
		L: [{ k: 'gtribMaster', t: 'Maestro de Responsabilidades' }, { k: 'gtribControl', t: 'Control de Responsabilidades' }]
	},
	{
		liA: 'forms_', rootFolder: 'Formularios', L: [{ k: 'formsVisita', t: 'Visitas / Asesorias' }, { k: 'dcp.cnc', t: 'Correspondencia' },
		{ k: 'dcpFormsPpe', t: 'Permisos Personal' }, { k: 'dcpFormsPpe.status', t: 'Permisos Personal / Autorización' }
		]
	}
]);


/* migrad */
var A03 = {
	api: { fic: '/sa/03/fic' },

	target: { respId: 'respCard' }
};
A03.Asesorias = {//oki
	put: function (P) {
		var wrap = $1.t('div');
		$Api.get({
			f: Api._dcp + 'forms/asesoria', loadVerif: !P.docEntry, inputs: 'docEntry=' + P.docEntry,
			func: function (Jq) {
				if (P.docEntry) {
					$Api.JS.addF({ name: 'docEntry', value: P.docEntry }, wrap);
				}
				var D = Jq; var jsF = $Api.JS.cls;
				var sea = $1.lTag({ tag: 'crd', D: D, value: ((D.cardName) ? D.cardName : ''), cont: wrap, 'class': jsF });
				//$crd.sea({cardId:Jq.cardId,cardName:Jq.cardName},wrap);
				var divLine = $1.T.divL({ divLine: 1, wxn: 'wrapx1', L: 'Cliente', Inode: sea }, wrap);
				D.doTime = (D.doTime) ? D.doTime : '';
				D.endTime = (D.endTime) ? D.endTime : '';
				D.fiObjetivos = (D.fiObjetivos) ? D.fiObjetivos : '';
				D.fiActividades = (D.fiActividades) ? D.fiActividades : '';
				D.fiCompEmpresa = (D.fiCompEmpresa) ? D.fiCompEmpresa : '';
				D.fiCompAsesor = (D.fiCompAsesor) ? D.fiCompAsesor : '';
				D.fiConclusion = (D.fiConclusion) ? D.fiConclusion : '';
				wrap.appendChild(divLine);
				var divLine = $1.T.divL({ divLine: 1, wxn: 'wrapx2', L: 'Fecha', I: { tag: 'input', type: 'date', 'class': jsF, name: 'docDate', value: $2d.f(D.docDate, 'Y-m-d') } }, wrap);
				var wxn = $1.t('div', { 'class': 'wrapx4' });
				wxn.appendChild($1.t('label', { textNode: 'Hora Entrada' }));
				var hourAt = $1.t('input', { type: 'text', 'class': jsF, name: 'doTime', value: $2d.f(D.docDate, 'H:i') });
				hourAt.onfocus = hourAt.onclick = function () { ADMS_Date.Sel.hour(this); }
				wxn.appendChild(hourAt);
				divLine.appendChild(wxn);
				var wxn = $1.t('div', { 'class': 'wrapx4' });
				wxn.appendChild($1.t('label', { textNode: 'Hora Salida' }));
				var hourAt = $1.t('input', { type: 'text', 'class': jsF, name: 'endTime', value: $2d.f(D.endDate, 'H:i') });
				hourAt.onfocus = hourAt.onclick = function () { ADMS_Date.Sel.hour(this); }
				wxn.appendChild(hourAt);
				divLine.appendChild(wxn);
				wrap.appendChild(divLine);
				var divLine = $1.t('div', { 'class': 'divLine' });
				var wxn = $1.t('div', { 'class': 'wrapx1' });
				wxn.appendChild($1.t('label', { textNode: '1. Objetivos' }));
				wxn.appendChild($1.t('textarea', { 'class': 'input ' + jsF, name: 'fiObjetivos', style: 'font-size:12px; height:90px;', textNode: D.fiObjetivos }));
				divLine.appendChild(wxn);
				wrap.appendChild(divLine);
				var divLine = $1.t('div', { 'class': 'divLine' });
				var wxn = $1.t('div', { 'class': 'wrapx1' });
				wxn.appendChild($1.t('label', { textNode: '2. Actividades Realizadas' }));
				wxn.appendChild($1.t('textarea', { 'class': 'input ' + jsF, name: 'fiActividades', style: 'font-size:12px; height:90px;', textNode: D.fiActividades }));
				divLine.appendChild(wxn);
				wrap.appendChild(divLine);
				var divLine = $1.t('div', { 'class': 'divLine' });
				var wxn = $1.t('div', { 'class': 'wrapx2' });
				wxn.appendChild($1.t('label', { textNode: '3.1 Compromiso Empresa' }));
				wxn.appendChild($1.t('textarea', { 'class': 'input ' + jsF, name: 'fiCompEmpresa', style: 'font-size:12px; height:90px;', textNode: D.fiCompEmpresa }));
				divLine.appendChild(wxn);
				var wxn = $1.t('div', { 'class': 'wrapx2' });
				wxn.appendChild($1.t('label', { textNode: '3.2 Compromiso Asesor' }));
				wxn.appendChild($1.t('textarea', { 'class': 'input ' + jsF, name: 'fiCompAsesor', style: 'font-size:12px; height:90px;', textNode: D.fiCompAsesor }));
				divLine.appendChild(wxn);
				wrap.appendChild(divLine);
				var divLine = $1.t('div', { 'class': 'divLine' });
				var wxn = $1.t('div', { 'class': 'wrapx1' });
				wxn.appendChild($1.t('label', { textNode: '4. Conclusiones y Recomendaciones' }));
				wxn.appendChild($1.t('textarea', { 'class': 'input ' + jsF, name: 'fiConclusion', style: 'font-size:12px; height:90px;', textNode: D.fiConclusion }));
				divLine.appendChild(wxn);
				wrap.appendChild(divLine);
				var resp = $1.t('div', 0, wrap);
				$Api.send({
					PUT: Api._dcp + 'forms/asesoria', loade: resp, jsBody: wrap, func: function (Jr2) {
						$Api.resp(resp, Jr2);
					}
				}, wrap);
			}
		});
		var wrapBk = $1.Win.open(wrap, { winTitle: 'Formulario de Visita', intMaxWidth: '1024', winId: 'formuVisitaDCP', winSize: 'medium', onBody: 1 });
	},
	get: function (Po) {
		var cont = $M.Ht.cont;
		var vPost = $1.G.filter();
		$Api.get({
			f: Api._dcp + 'forms/asesorias', loade: cont, inputs: vPost, func: function (Jq) {
				if (Jq.errNo) { $Api.resp(cont, Jq); }
				else {
					var tb = $1.t('table', { 'class': 'table_zh table_1024' });
					var tHead = $1.t('thead');
					var tr0 = $1.t('tr');
					tr0.appendChild($1.t('td', { 'textNode': '' }));
					tr0.appendChild($1.t('td', { 'textNode': 'No.' }));
					tr0.appendChild($1.t('td', { 'textNode': 'Entrada' }));
					tr0.appendChild($1.t('td', { 'textNode': 'Salida' }));
					tr0.appendChild($1.t('td', { 'textNode': 'Hrs.', title: 'Duración en Horas' }));
					tr0.appendChild($1.t('td', { 'textNode': 'Creado Por' }));
					tr0.appendChild($1.t('td', { 'textNode': 'Responsable' }));
					tr0.appendChild($1.t('td', { 'textNode': 'Cliente' }));
					$1.t('td', { textNode: '' }, tr0);
					tHead.appendChild(tr0);
					tb.appendChild(tHead);
					var tBody = $1.t('tbody');
					for (var i in Jq.L) {
						var D = Jq.L[i];
						var tr1 = $1.t('tr');
						var td = $1.t('td');
						var edit = $1.t('input', { type: 'button', 'class': 'iBg iBg_edit', D: D });
						edit.onclick = function () { A03.Asesorias.put(this.D); }
						td.appendChild(edit);
						var edit = $1.t('input', { type: 'button', 'class': 'iBg iBg_email', D: D });
						edit.onclick = function () { A03.Asesorias.sendEmail(this.D); }
						td.appendChild(edit);
						td.appendChild(edit);
						tr1.appendChild(td);
						var td = $1.t('td', 0, tr1);
						var aView = $1.t('a', { textNode: D.docEntry, href: $M.to('forms.visitaView', 'docEntry:' + D.docEntry, 'r') }, td);
						tr1.appendChild($1.t('td', { textNode: $2d.f(D.docDate, 'Y-m-d H:iam') }));
						tr1.appendChild($1.t('td', { textNode: $2d.f(D.endDate, 'Y-m-d H:iam') }));
						tr1.appendChild($1.t('td', { textNode: D.timeDuration }));
						tr1.appendChild($1.t('td', { textNode: $Doc.by('userDate', D) }));
						tr1.appendChild($1.t('td', { textNode: _g(D.slpId, $Tb.oslp) }));
						var td = $1.t('td', 0, tr1);
						$Doc.href('crd', D, td);
						var td = $1.t('td', 0, tr1);
						Attach.openWin({ tt: 'dcpVisit', tr: D.docEntry, title: 'Visita: ' + D.docEntry }, td);
						tBody.appendChild(tr1);
					}
					tb.appendChild(tBody);
					tb = $1.T.tbExport(tb, { ext: 'xlsx', fileName: 'Reporte de Visitas', print: 'Y' });
					cont.appendChild(tb);
				}
			}
		});
	},
	view: function (P) {
		var cont = $M.Ht.cont; var Pa = $M.read();
		$Api.get({
			f: Api._dcp + 'forms/asesoria', inputs: 'docEntry=' + Pa.docEntry, loade: cont, func: function (Jq) {
				var D = Jq;
				var divTop = $1.t('div', { style: 'marginBottom:0.5rem;', 'class': 'no-print' }, cont);
				var btnPrint = $1.T.btnFa({ fa: 'fa_print', textNode: ' Imprimir', func: function () { $1.Win.print(wrap); } }, divTop);
				var wrap = $1.t('div', { 'class': 'pagePrint_letter' }, cont);
				D.doTime = (D.doTime) ? D.doTime : '';
				D.endTime = (D.endTime) ? D.endTime : '';
				D.fiObjetivos = (D.fiObjetivos) ? D.fiObjetivos : '';
				D.fiActividades = (D.fiActividades) ? D.fiActividades : '';
				D.fiCompEmpresa = (D.fiCompEmpresa) ? D.fiCompEmpresa : '';
				D.fiCompAsesor = (D.fiCompAsesor) ? D.fiCompAsesor : '';
				D.fiConclusion = (D.fiConclusion) ? D.fiConclusion : '';
				var divLogo = $1.t('div', { style: 'height:4rem; text-align:right;' });
				divLogo.appendChild($1.t('img', { src: $Soc.logo }));
				wrap.appendChild(divLogo);
				var ffLine = $1.T.ffLine({ ffLine: 1, t: 'Cliente:', v: D.cardName });
				wrap.appendChild(ffLine);
				var ffLine = $1.T.ffLine({ ffLine: 1, t: 'Fecha:', w: 'ffx3', v: $2d.f(D.docDate, 'Y-m-d') });
				ffLine.appendChild($1.T.ffLine({ t: 'Entrada:', w: 'ffx3', v: $2d.f(D.docDate, 'H:iam') }));
				ffLine.appendChild($1.T.ffLine({ t: 'Salida:', w: 'ffx3', v: $2d.f(D.endDate, 'H:iam') }));
				wrap.appendChild(ffLine);
				var ffLine = $1.T.ffLine({ ffLine: 1, t: 'Realizado por:', w: 'ffx2', v: $Tb._g('ousr', D.userId) });
				ffLine.appendChild($1.T.ffLine({ t: 'Responsable:', w: 'ffx2', v: $Tb._g('oslp', D.slpId) }));
				wrap.appendChild(ffLine);
				var ffLine = $1.T.ffLine({ ffLine: 1, t: '1. Objetivos' });
				wrap.appendChild(ffLine);
				wrap.appendChild($1.t('div', { 'class': 'textarea', textNode: D.fiObjetivos }));
				var ffLine = $1.T.ffLine({ ffLine: 1, t: '2. Actividades' });
				wrap.appendChild(ffLine);
				wrap.appendChild($1.t('div', { 'class': 'textarea', textNode: D.fiActividades }));
				var ffLine = $1.T.ffLine({ ffLine: 1, t: '3.1 Compromisos Empresa' });
				wrap.appendChild(ffLine);
				wrap.appendChild($1.t('div', { 'class': 'textarea', textNode: D.fiCompEmpresa }));
				var ffLine = $1.T.ffLine({ ffLine: 1, t: '3.2 Compromisos Asesor' });
				wrap.appendChild(ffLine);
				wrap.appendChild($1.t('div', { 'class': 'textarea', textNode: D.fiCompAsesor }));
				var ffLine = $1.T.ffLine({ ffLine: 1, t: '4. Conclusiones y Recomendaciones' });
				wrap.appendChild(ffLine);
				wrap.appendChild($1.t('div', { 'class': 'textarea', textNode: D.fiConclusion }));
				var divBot = $1.t('div', { 'class': 'pagePrint_footer', textNode: $Soc.softFrom });
				wrap.appendChild(divBot);
			}
		});
	},
	sendEmail: function (D) {
		var wrap = $1.t('div');
		$Api.get({
			f: Api._dcp + 'forms/asesorias.sendEmail', inputs: 'docEntry=' + D.docEntry,
			func: function (Jq2) {
				if (Jq2.errNo) { $Api.put(wrap, Jq2); }
				else {
					var D = Jq2;
					if (Jq2.L && Jq2.L.errNo) {
						$Api.put(wrap, Jq2.L);
					}
					var divL = $1.T.divL({ divLine: true, wxn: 'wrapx2', L: 'Enviar a', I: { tag: 'select', sel: { 'class': 'jsFields', name: 'sendTo' }, opts: Jq2.Emails } }, wrap);
					$1.T.divL({ wxn: 'wrapx2', L: 'Con copia A:', I: { tag: 'input', type: 'text', 'class': 'jsFields', name: 'ccto', placeholder: 'Digite unos o varios correos' } }, divL);
					var divL = $1.T.divL({ divLine: true, wxn: 'wrapx1', L: 'Cuerpo de mensaje', I: { tag: 'textarea', 'class': 'jsFields', name: 'body', textNode: 'Soporte del formulario de Visita Realizada por el Asesor ' + $Tb._g('ousr', D.userId) + '.' } }, wrap);
					var resp = $1.t('div', 0, wrap);
					$Api.send({
						POST: Api._dcp + 'forms/asesorias.sendEmail', loade: resp, getInputs: function (T) { return 'docEntry=' + D.docEntry + '&cardName=' + D.cardName + '&' + $1.G.inputs(); }, func: function (Jr2) {
							$Api.resp(resp, Jr2);
							//if(!Jr2.errNo){ $1.delet(T); }
						}
					}, wrap);
				}
			}
		});
		var wrapBk = $1.Win.open(wrap, { winTitle: 'Enviar Formulario de Visita al Cliente', onBody: 1 });
	}
}

A03.RT = {
	assg: function () {
		var wrap = $M.Ht.cont; Pa = $M.read();
		var vcardId = 'cardId=' + Pa.cardId + '&';
		var ty = new Date().getFullYear();
		var yini = 2018; yend = (ty > yini) ? ty + 2 : yini + 2;
		var opts = {}; while (yini <= yend) { y2 = yini; opts[y2] = y2; yini++; }
		var divLine = $1.T.divL({ divLine: 1, wxn: 'wrapx4', L: { textNode: 'Año para Asignar' }, I: { tag: 'select', sel: { 'class': '__year jsFields', name: 'year' }, opts: opts, selected: ty } }, wrap);
		var wList = $1.t('div', 0, wrap);
		var resp = $1.t('div', 0, wrap);
		$Api.send({
			PUT: Api._dcp + 'rt/assg', loade: resp, getInputs: function () { return vcardId + $1.G.inputs(wrap); }, func: function (Jr2) {
				$Api.resp(resp, Jr2);
			}
		}, wrap);
		function upd(wList) {
			$Api.get({
				f: Api._dcp + 'rt/assg', loade: wList, inputs: vcardId + $1.G.inputs(wrap), func: function (Jq) {
					if (Jq.errNo) { $Api.resp(wList, Jq); }
					var len = $js.length(Jq.GRT);
					var tb = $1.T.table(['', 'Grupo', 'Familia', 'Periodicidad', '', '', 'Grupo', 'Familia', 'Periodicidad']);
					wList.appendChild(tb);
					var tBody = $1.t('tbody', 0, tb);
					var n = 1;
					for (var i in Jq.L) {
						var L = Jq.L[i];
						if (n == 1 || n % 2) { var tr = $1.t('tr', 0, tBody); }
						var td = $1.t('td', { title: L.checked }, tr);
						var ck = $1.T.check({ 'class': 'jsFields __' + L.grCode, name: 'GR[' + L.grId + ']', YN: true, checked: (L.checked == 'Y') });
						ck.childNodes[0].grCode = L.grCode;
						ck.childNodes[0].onclick = function () {
							var ts = $1.q('.__' + this.grCode, wrap, 'all');
							for (var n = 0; n < ts.length; n++) {
								if (ts[n] != this) { (ts[n]).checked = false; }
							}
						}
						td.appendChild(ck);
						tr.appendChild($1.t('td', L.grName));
						tr.appendChild($1.t('td', L.grPrim));
						tr.appendChild($1.t('td', L.grPeriod));
						if (n == 1) {
							tr.appendChild($1.t('td', { rowspan: (len / 2), style: 'backgroundColor:#CCC; width:2rem;' }));
						}
						n++;
					}
				}
			});
		}
		upd(wList);
		$1.q('.__year').onchange = function () { upd(wList); }
	}
	,
	vYear: function (P) {/* no usado */
		var vcardId = 'cardId=' + P.cardId + '&';
		var wrap = $1.t('div'); var ty = new Date().getFullYear();
		var yini = 2018; yend = (ty > yini) ? ty + 2 : yini + 2;
		var opts = {}; while (yini <= yend) { y2 = yini; opts[y2] = y2; yini++; }
		var divLine = $1.T.divL({ divLine: 1, wxn: 'wrapx4', L: { textNode: 'Año para Asignar' }, I: { tag: 'select', sel: { 'class': '__year jsFields', name: 'year' }, opts: opts, selected: ty } });
		wrap.appendChild(divLine);
		var wList = $1.t('div');
		var tb = $1.T.table(['', 'Valor', 'Variable']); wList.appendChild(tb);
		var tBody = $1.t('tbody'); tb.appendChild(tBody);
		function upd() {
			$ps_DB.get({
				f: 'GET ' + A03.RT.api + 'vYear', loade: tBody, inputs: vcardId + $1.G.inputs(wrap), func: function (Jq) {
					var ni = 1;
					for (var v in A03.Vs.vYear) {
						var tr = $1.t('tr'); tBody.appendChild(tr);
						var td = $1.t('td', ni); ni++; tr.appendChild(td);
						var td = $1.t('td'); tr.appendChild(td);
						val = (Jq[v]) ? Jq[v] : '';
						var inp = $1.t('input', { type: 'number', inputmode: 'numeric', min: 0, 'class': 'jsFields', name: 'Va[' + v + ']', value: val, style: 'width:9rem;' });
						if (A03.Vs.vYear_ext && A03.Vs.vYear_ext[v]) {
							var tinp = A03.Vs.vYear_ext[v];
							if (tinp.opts) {
								var inp = $1.T.sel({ sel: { 'class': 'jsFields', name: 'Va[' + v + ']', style: 'width:9rem;' }, opts: tinp.opts, selected: val });
							}
						}
						td.appendChild(inp);
						var td = $1.t('td', A03.Vs.vYear[v]); tr.appendChild(td);
					}
				}
			});
		}
		var resp = $1.t('div');
		var btnSend = $1.T.btnSend({
			value: 'Actualizar Variables', func: function () {
				$ps_DB.get({
					f: 'PUT ' + A03.RT.api + 'vYear', loade: resp, inputs: vcardId + $1.G.inputs(wrap), func: function (Jq) {
						$ps_DB.response(resp, Jq);
					}
				});
			}
		});
		wrap.appendChild(wList); wrap.appendChild(resp);
		wrap.appendChild(btnSend);
		upd();
		$1.Win.open(wrap, { onBody: 1, winSize: 'full', winTitle: 'Definir Variables para Cliente' });
		$1.q('.__year').onchange = function () { upd(); }
	}
	,
}

A03.GT = {
	Resp: {
		get: function (Po) {
			var cont = $M.Ht.cont;
			var vPost = $1.G.inputs($1.q('#wrapFormGT'), 'jsFiltVars');
			$Api.get({
				f: Api._dcp + 'resp', inputs: vPost, loade: cont,
				func: function (Jq) {
					if (Jq.errNo) { $Api.resp(cont, Jq); }
					else {
						var tb = $1.T.table(['#', '', 'respId', 'Nombre', 'Grupo', 'Año', 'gdv', 'Aplica Para', 'L']);
						var tBody = $1.t('tbody');
						for (var i in Jq.L) {
							var D = Jq.L[i];
							var tr1 = $1.t('tr');
							var td = $1.t('td');
							var edit = $1.t('input', { type: 'button', 'class': 'iBg iBg_edit', textNode: ' Modificar', D: D });
							edit.onclick = function () {
								var Di = this.D; delete (Di.copy);
								A03.GT.Resp.put(Di);
							}
							td.appendChild(edit);
							td.appendChild($1.t('span', ' | '));
							var clon = $1.t('button', { 'class': 'fa faBtn fa_clone', textNode: ' Copiar', D: D });
							clon.onclick = function () {
								var Di = this.D;
								$1.Win.confirm({
									text: 'Se cargará la información la responsabilidad, pero al momento de Guardar se creará una nueva.', func: function () {
										Di.copy = 'Y';
										A03.GT.Resp.put(Di);
									}
								});
							}
							td.appendChild(clon);
							if ($0s.user == 'supersu') {
								td.appendChild($1.t('span', ' | '));
								var aux = $1.t('button', { 'class': 'fa faBtn fa_doc', textNode: ' Anexos', D: D });
								aux.onclick = function () { A03.GT.Resp.Anex.put(this.D); }
								td.appendChild(aux);
							}
							tr1.appendChild(td);
							tr1.appendChild($1.t('td', '-'));
							tr1.appendChild($1.t('td', { textNode: D.respId }));
							tr1.appendChild($1.t('td', { textNode: D.respName }));
							tr1.appendChild($1.t('td', { textNode: D.grName }));
							tr1.appendChild($1.t('td', { textNode: D.respYear }));
							tr1.appendChild($1.t('td', { textNode: D.gdvCode }));
							var td = $1.t('td');
							td.appendChild($1.t('span', { 'class': 'spanLine', textNode: A03.Vs.respTo[D.respTo] }));
							tr1.appendChild(td);
							tr1.appendChild($1.t('td', { textNode: D.locked }));
							tBody.appendChild(tr1);
						}
						tb.appendChild(tBody);
						cont.appendChild(tb);
					}
				}
			});
		},
		put: function (P) {
			var wrap = $1.t('div'); var D = {};
			$Api.get({
				f: Api._dcp + 'resp/one', loadVerif: !P.respId, inputs: 'respId=' + P.respId, func: function (Jq) {
					var D = Jq;
					if (P.copy) {
						var wrapCoText = $1.t('div', { 'class': '__copyText' }, wrap);
						D.respId = 'newS';
						$Api.resp(wrapCoText, { errNo: 3, text: 'Copiando... Edite la información y al guardar se genera una nueva responsabilidad' });
					}
					var tid = $1.t('input', { type: 'hidden', 'class': 'jsFields', name: 'respId', value: D.respId }, wrap);
					var divL = $1.T.divL({ divLine: true, wxn: 'wrapx4_1', Label: { textNode: 'Nombre Responsabilidad' }, I: { tag: 'input', type: 'text', 'class': 'jsFields', name: 'respName', value: D.respName } }, wrap);
					$1.T.divL({ wxn: 'wrapx4', Label: { textNode: 'Año' }, I: { tag: 'input', type: 'text', 'class': 'jsFields', name: 'respYear', value: D.respYear } }, divL);
					var divL = $1.T.divL({ divLine: true, wxn: 'wrapx4', Label: { textNode: 'Grupo' }, I: { tag: 'select', sel: { 'class': 'jsFields', name: 'grId' }, selected: D.grId, opts: $Tb.GRT } }, wrap);
					$1.T.divL({ wxn: 'wrapx4', Label: { textNode: 'Grupo Digitos' }, I: { tag: 'select', sel: { 'class': 'jsFields grupoDigitos', name: 'gdvCode' }, selected: D.gdvCode, opts: $Tb.GDV } }, divL);
					$1.T.divL({ wxn: 'wrapx2', Label: { textNode: 'Aplica Para' }, I: { tag: 'select', sel: { 'class': 'jsFields', name: 'respTo' }, selected: D.respTo, opts: A03.Vs.respTo } }, divL);
					var sel = $1.q('.grupoDigitos', divL);
					var divL = $1.T.divL({ divLine: true, wxn: 'wrapx1', Label: { textNode: 'Detalles' }, I: { tag: 'textarea', 'class': 'jsFields', name: 'details', value: D.details } }, wrap);
					var wrapApply = $1.t('p', 0, wrap);
					var tb = $1.T.table(['#', 'Vencimiento']);
					var tBody = $1.t('tbody', 0, tb);
					wrap.appendChild(tb);
					var div = $1.t('div', 0, wrap);
					var resp = $1.t('div', 0, wrap);
					$Api.send({
						PUT: Api._dcp + 'resp', loade: resp, inputsFrom: wrap, func: function (Jr2) {
							$Api.resp(resp, Jr2);
							if (!Jr2.errNo) {
								tid.value = Jr2.respId; $1.delet($1.q('.__copyText'));
							}
						}
					}, wrap);

					A03.GT.Resp.gdvReload(sel, tBody, D);
				}
			});
			var bk = $1.Win.open(wrap, { winTitle: 'Definir Responsabilidad', winId: 'respTribNameDefine', winSize: 'medium', onBody: 1 });
		},
		gdvReload: function (sel, tBody, D) {
			//var gd = $1.q('.grupoDigitos');
			var gd = sel;
			function update(k) {
				var V = ($Tb.GDV[k]) ? $Tb.GDV[k].subGr : {};
				$1.clear(tBody);
				var n = 0;
				for (var sb in V) {
					var iName = 'subGr[' + sb + ']';
					var tr = $1.t('tr', 0, tBody);
					var td = $1.t('td', { textNode: V[sb] }, tr);
					var td = $1.t('td', 0, tr);
					var dueDate = (D.L && D.L[sb]) ? D.L[sb] : '';
					$1.t('input', { type: 'date', 'class': 'jsFields in_x100', name: iName + '[dueDate]', value: dueDate }, td);
				}
			}
			gd.onchange = function () { update(this.value); }
			update(D.gdvCode);
		},
	}
}
A03.GT.F = {//Seguimiento
	open: function (P) {
		var wrap = $1.t('div');
		var h4 = $1.t('h4', { 'class': 'head1', textNode: P.respName });
		wrap.appendChild(h4);
		var lin = $1.t('div'); wrap.appendChild(lin);
		lin.appendChild($1.t('b', 'Cliente:'));
		lin.appendChild($1.t('span', ' ' + P.cardName));
		var lin = $1.t('div'); wrap.appendChild(lin);
		lin.appendChild($1.t('b', 'Vence:'));
		lin.appendChild($1.t('span', ' ' + $2d.f(P.dueDate, 'mmm d')));
		var wrapMenu = $1.t('div');
		P.cardId = (P.cardId) ? P.cardId * 1 : '';
		var targetType = (P.respId + '-' + P.cardId);
		var objCt = { targetType: A03.target.respId, targetRef: targetType, FR: { bussPartner: P.cardId } };
		var obj5a2 = { targetType: A03.target.respId, targetRef: P.respId, relType: 'card', relRef: P.cardId };
		var Lis = [
			{ li: { textNode: 'Comentarios', 'class': 'fa fa_comment', winClass: 'win5c' } },
			{ li: { textNode: 'Archivos', 'class': 'fa fa_fileUpd', winClass: 'win5f', func: $3.F.$5f, funcPars: objCt } },
			{ li: { textNode: 'CheckList', 'class': 'fa fa_checklist', winClass: 'win5ck_R', func: $3.F.$5ck, funcPars: obj5a2 } }
		];
		var me = ps_DOM.Menu.inLine(Lis, wrap);
		wrapMenu.appendChild(me);
		wrap.appendChild(wrapMenu);
		wrapMenu.appendChild($3.F.$5c(objCt, 'noLoad'));
		wrapMenu.appendChild($3.F.$5f(objCt, 'noLoad'));
		wrapMenu.appendChild($3.F.$5ck(obj5a2, 'noLoad'));
		var bk = $1.Win.open(wrap, { winTitle: 'Seguimiento Responsabilidad (v.1)', winSize: 'medium' });
		ps_DOM.body.appendChild(bk);
	}
}

A03.Aud = {
	opts: function (P) {
		var L = P.L; var Jr = P.Jr;
		var Li = []; var n = 0;
		Li[n] = { ico: 'fa fa_pencil', textNode: ' Modificar', href: $M.to('fquP.audB.form', 'docEntry:' + L.docEntry, 'r') }; n++;
		Li = $Opts.add('fquPAudB', Li);
		return Li = { L: L, Li: Li, textNode: P.textNode };
	},
	get: function () {
		var cont = $M.Ht.cont;
		$Api.get({
			f: Api._dcp + 'aud/base', inputs: $1.G.filter(), loade: cont, func: function (Jr) {
				if (Jr.errNo) { $Api.resp(cont, Jr); }
				else {
					var tb = $1.T.table(['', 'N°', 'Año', 'Cliente', 'Actualizado', 'Creado']); cont.appendChild(tb);
					var tBody = $1.t('tbody', 0, tb);
					for (var i in Jr.L) {
						L = Jr.L[i];
						var tr = $1.t('tr', 0, tBody);
						var td = $1.t('td', 0, tr);
						var menu = $1.Menu.winLiRel(A03.Aud.opts({ L: L })); td.appendChild(menu);
						$1.t('td', { textNode: L.docEntry }, tr);
						$1.t('td', { textNode: L.docYear }, tr);
						$1.t('td', { textNode: L.cardName }, tr);
						$1.t('td', { textNode: $Doc.by('userDate', { userId: L.userUpd, dateC: L.dateUpd }) }, tr);
						$1.t('td', { textNode: $Doc.by('userDate', L) }, tr);
					}
				}
			}
		});
	},
	form: function () {
		var Pa = $M.read(); var cont = $M.Ht.cont;
		var vPost = (Pa.docEntry) ? 'docEntry=' + Pa.docEntry : '';
		$Api.get({
			f: Api._dcp + 'aud/base/form', inputs: vPost, loade: cont, func: function (Jr) {
				var P2 = {};
				var sea = $crd.sea({ cardId: Jr.cardId, cardName: Jr.cardName }, cont);
				var divL = $1.T.divL({ divLine: 1, wxn: 'wrapx4', L: 'Cliente', Inode: sea }, cont);
				$1.T.divL({ wxn: 'wrapx4', L: 'Año del Documento', I: { tag: 'input', type: 'number', inputmode: 'numeric', 'class': 'jsFields', name: 'docYear', value: Jr.docYear } }, divL);
				var tbr = $1.T.fieldset(Fqu.Tpt.base({ L: Jr.L }, null, P2));
				cont.appendChild(tbr);
				var inp = $1.t('input', { type: 'hidden', name: 'docEntry', 'class': 'jsFields', value: Pa.docEntry }, cont);
				var resp = $1.t('div', 0, cont);
				var bs = {
					textNode: 'Guardar Información', POST: Api._dcp + 'aud/base/form', getInputs: function () { return vPost + '&formId=' + Jr.formId + '&' + $1.G.inputs(cont); }, loade: resp, func: function (Jr2) {
						$Api.resp(resp, Jr2);
						if (Jr2.docEntry) { inp.value = Jr2.docEntry; }
					}
				};
				if (Pa.docEntry) { bs.PUT = bs.POST; delete (bs.POST); }
				$Api.send(bs, cont);
			}
		});
	}
}

var GT = {};
GT.Resp = {
	View: {
		load: function () {
			var v = $1.q('#gtRespViewType'); v = (v) ? v : {};
			switch (v.value) {
				default: GT.Resp.View.list(); break;
				case 'calendar': GT.Resp.View.calendar(); break;
				case 'byCard': GT.Resp.View.byCard(); break;
				case 'viewDCP': GT.Resp.View.DCP(); break;
				case 'cardTb': GT.Resp.View.cardTb(); break;
			}
		},
		calendar: function () {
			var cont = $M.Ht.cont;
			$Api.get({
				f: Api._dcp + 'resp/calendar', inputs: $1.G.filter(), loade: cont, func: function (Jq) {
					if (Jq.errNo) { $ps_DB.response(cont, Jq); }
					else {
						var date1 = $1.q('#_goDate1').value;
						var date2 = $1.q('#_goDate2').value;
						var Days = $2d.Vs.Days;
						var tb = $1.t('table', { 'class': 'table_zh', style: 'width:83rem;' });
						var tHead = $1.t('thead'); tb.appendChild(tHead);
						var tr0 = $1.t('tr'); tHead.appendChild(tr0);
						tr0.appendChild($1.t('td', { textNode: '#', title: 'No. Semana del Año' }));
						for (var dN in Days) {
							var k = Days[dN].k;
							if (dN == 'su' || dN == 's') { continue; }
							tr0.appendChild($1.t('td', { 'textNode': Days[dN].t, style: 'width:10rem;', 'class': 'weekDay_' + k }));
						}
						var tBody = $1.t('tbody'); tb.appendChild(tBody);
						var Wd = $2d.Draw.Wd({ sundays: 'N', date1: date1, date2: date2 });
						var nii = 1;
						for (var w in Wd) {
							var tr = $1.t('tr'); tBody.appendChild(tr);
							var wT = (Wd[w].wy > 52) ? 1 : Wd[w].wy;
							tr.appendChild($1.t('td', { textNode: wT }));;
							for (var dN in Days) {
								if (dN == 'su' || dN == 's') { continue; }
								var tD = Days[dN]; var k = tD.k;
								var Wd_w_k = (Wd[w][k]) ? Wd[w][k] : {};
								yzId = Wd_w_k.yzId;
								var text = (Wd_w_k.date) ? $2d.f(Wd_w_k.date, 'mmm d') : '';
								var td = $1.t('td', { 'class': 'yzId_' + yzId, style: 'width:16rem; height:10rem;' });
								td.appendChild($1.t('div', { textNode: text }));
								tr.appendChild(td);
							}
						}
						cont.appendChild(tb);
						for (var i in Jq.Resp) {
							var R = Jq.Resp[i];
							var tw = $1.q('.yzId_' + R.yzId, tb);
							if (tw && tw.tagName) {
								var div = $1.t('div', { 'class': 'gtRespViewCalend_wrap' });
								div.appendChild($1.t('span', { 'class': 'spanLine', textNode: R.total, title: R.yzId }));
								div.appendChild($1.t('b', { textNode: R.respName }));
								var divL = $1.t('div', { 'class': 'gtRespViewCalend_lis' });
								for (var l in R.L) {
									var L = R.L[l];
									var P2 = L; P2.resId = R.resId;
									var li = $1.t('li', { P: P2 });
									li.appendChild($1.t('button', { 'class': 'fa faBtn fa_doc' }));
									li.appendChild($1.t('textNode', L.cardName));
									li.onclick = function () { A03.GT.Resp.F.open(this.P); }
									divL.appendChild(li);
								}
								div.appendChild(divL);
								tw.appendChild(div);
							}
						}
					}
				}
			});
		},
		list: function () {
			var cont = $M.Ht.cont;
			$Api.get({
				f: Api._dcp + 'resp/list', inputs: $1.G.filter(), loade: cont, func: function (Jq) {
					if (Jq.errNo) { $Api.resp(cont, Jq); }
					else {
						var tb = $1.T.table(['', '#', 'Estado', 'Verif.', 'No. Doc.', 'Vencimiento', { textNode: 'Reg.', title: 'Regimen' }, { textNode: 'Tipo', title: 'Tipo Entidad' }, 'Cliente', 'Responsable']);
						var tBody = $1.t('tbody'); tb.appendChild(tBody);
						var n = 1;
						var bg = 'backgroundColor:#DDD;';
						for (var i in Jq.Resp) {
							var R = Jq.Resp[i]
							var tr = $1.t('tr'); tBody.appendChild(tr);
							var td = $1.t('td', { colspan: 10, style: bg }); tr.appendChild(td);
							td.appendChild($1.t('b', R.respName));
							td.appendChild($1.t('span', ' | '));
							td.appendChild($1.t('span', { 'class': 'spanLine', textNode: A03.Vs.respTo[R.respTo] }));
							if (R.L) {
								for (var l in R.L) {
									var L = R.L[l];
									var tr = $1.t('tr'); tBody.appendChild(tr);
									var respStatus = (L.respStatus) ? L.respStatus : 'Pendiente';
									var td = $1.t('td'); tr.appendChild(td);
									L.respName = R.respName;
									var btn = $1.t('button', { 'class': 'fa faBtn fa_doc', P: L });
									btn.onclick = function () { GT1.V.open(this.P); }
									td.appendChild(btn);
									tr.appendChild($1.t('td', n)); n++;
									tr.appendChild($1.t('td', respStatus));
									tr.appendChild($1.t('td', L.verif));
									tr.appendChild($1.t('td', L.licTradNum));
									tr.appendChild($1.t('td', L.dueDate));
									tr.appendChild($1.t('td', L.RF_regTrib));
									tr.appendChild($1.t('td', L.RF_tipEnt));
									tr.appendChild($1.t('td', L.cardName));
									tr.appendChild($1.t('td', $Tb._g('oslp', L.slpId)));
								}
							}
						}
						tb = $1.T.tbExport(tb);
						cont.appendChild(tb);
					}
				}
			});
		},
		byCard: function () {
			var cont = $M.Ht.cont;
			$Api.get({
				f: Api._dcp + 'resp/byCard', inputs: $1.G.filter(), loade: cont, func: function (Jq) {
					if (Jq.errNo) { $Api.resp(cont, Jq); }
					else {
						var tb = $1.T.table(['', '#', 'Estado', 'Vencimiento', 'Responsabilidad']);
						var tBody = $1.t('tbody'); tb.appendChild(tBody);
						var n = 1;
						var bg = 'backgroundColor:#DDD;';
						for (var i in Jq.Card) {
							var R = Jq.Card[i];
							var tr = $1.t('tr'); tBody.appendChild(tr);
							var td = $1.t('td', { colspan: 10, style: bg }); tr.appendChild(td);
							td.appendChild($1.t('b', R.cardName));
							td.appendChild($1.t('span', ' | '));
							td.appendChild($1.t('span', { 'class': 'spanLine', textNode: R.licTradNum }));
							td.appendChild($1.t('span', ' Resp: '));
							td.appendChild($1.t('span', { 'class': 'spanLine', textNode: $Tb._g('oslp', R.slpId) }));
							if (R.L) {
								var nl = 1;
								for (var l in R.L) {
									var L = R.L[l];
									var tr = $1.t('tr'); tBody.appendChild(tr);
									var respStatus = (L.respStatus) ? L.respStatus : 'Pendiente';
									var td = $1.t('td'); tr.appendChild(td);
									L.cardId = R.cardId;
									var btn = $1.t('button', { 'class': 'fa faBtn fa_doc', P: L });
									btn.onclick = function () { GT1.V.open(this.P); }
									td.appendChild(btn);
									tr.appendChild($1.t('td', n)); n++;
									tr.appendChild($1.t('td', respStatus));
									tr.appendChild($1.t('td', L.dueDate));
									tr.appendChild($1.t('td', L.respName));
								}
							}
						}
						tb = $1.T.tbExport(tb);
						cont.appendChild(tb);
					}
				}
			});
		},
		DCP: function () {
			var cont = $M.Ht.cont;
			var vPost = $1.G.filter();
			$Api.get({
				f: Api._dcp + 'resp/viewDCP', inputs: $1.G.filter(), loade: cont, func: function (Jq) {
					if (Jq.errNo) { $Api.resp(cont, Jq); }
					else {
						$1.q('body').style.overflowX = 'overlay';
						var tb = $1.T.table(['Empresa', 'Doc', 'Reg.', 'Tip. Ent']);
						var tr0 = $1.q('thead tr', tb);
						for (var Ym in Jq.Ym) {
							tr0.appendChild($1.t('td', $2d.f(Ym + '-01', 'mes')));
						}
						var tBody = $1.t('tbody'); tb.appendChild(tBody);
						var n = 1;
						var bg = 'backgroundColor:#DDD;';
						for (var i in Jq.Card) {
							var R = Jq.Card[i];
							var tr = $1.t('tr'); tBody.appendChild(tr);
							var tdc = $1.t('td'); tr.appendChild(tdc);
							var td = $1.t('div', { style: 'width:13rem;' }); tdc.appendChild(td);
							td.appendChild($1.t('b', R.cardName));
							td.appendChild($1.t('br'));
							td.appendChild($1.t('span', ' Resp: '));
							td.appendChild($1.t('span', { 'class': 'spanLine', textNode: $Tb._g('oslp', R.slpId) }));
							tr.appendChild($1.t('td', R.licTradNum));
							tr.appendChild($1.t('td', R.RF_regTrib));
							tr.appendChild($1.t('td', R.RF_tipEnt));
							for (var ym in Jq.Ym) {
								var td = $1.t('td'); tr.appendChild(td);
								if (R.Ym && R.Ym[ym]) {
									var nl = 1;
									for (var l in R.Ym[ym]) {
										var L = R.Ym[ym][l];
										var respStatus = (L.respStatus) ? L.respStatus : 'Pendiente';
										L.cardId = R.cardId;
										var li = $1.t('div', { style: 'width:11rem;' }); td.appendChild(li);
										var btn = $1.t('button', { 'class': 'fa faBtn fa_doc', P: L });
										btn.onclick = function () { GT1.V.open(this.P); }
										li.appendChild(btn);
										li.appendChild($1.t('span', '(' + (L.dueDate).substr(8, 2) + ') '));
										li.appendChild($1.t('span', ' ' + L.grName));
									}
								}
							}
						}
						tb = $1.T.tbExport(tb, { print: true });
						cont.appendChild(tb);
					}
				}
			});
		},
		cardTb: function () {
			var cont = $M.Ht.cont; var wrap = $1.t('div')
			$Api.get({
				f: Api._dcp + 'resp/viewDCP', inputs: $1.G.filter(), loade: cont, func: function (Jq) {
					if (Jq.errNo) { $ps_DB.response(cont, Jq); }
					else {
						var n = 1;
						var bg = 'backgroundColor:#DDD;';
						for (var i in Jq.Card) {
							var R = Jq.Card[i];
							var wrapC1 = $1.t('div', { style: 'padding:0.5rem; border:0.0625rem solid #CCC; margin-bottom:1rem; border-radius:0.25rem' }); wrap.appendChild(wrapC1);
							var wrapC = $1.t('div');
							wrapC1.appendChild($1.Win.print(wrapC, { btn: 1 }));
							wrapC1.appendChild(wrapC);
							var log = $1.t('div', { style: 'float:right; width:22rem; text-align:right;' });
							wrapC.appendChild(log);
							log.appendChild($1.t('img', { src: $Soc.logo, alt: 'Logo', style: 'width:22rem; height:3rem;' }));
							wrapC.appendChild($1.t('h1', { textNode: 'Calendario Tributario 2020' }));
							wrapC.appendChild($1.t('h4', { textNode: 'DyD Consultores y Asesores SAS y DCP Revisores, le informa.' }));
							wrapC.appendChild($1.t('h5', { textNode: 'Cliente: ' + R.cardName + '\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0NIT/CC:' + R.licTradNum, style: 'margin-bottom:0.5rem;' }));
							var peoR = (R.peoRel) ? R.peoRel : {};
							var gerente = (peoR.gerente) ? peoR.gerente.name : '';
							var contador = (peoR.contador) ? peoR.contador.name : '';
							wrapC.appendChild($1.t('li', 'Gerente: ' + gerente));
							wrapC.appendChild($1.t('li', 'Contador: ' + contador));
							wrapC.appendChild($1.t('li', 'Delegado Revisoría: ' + $Tb._g('oslp', R.slpId)));
							var tb = $1.T.table([{ colspan: 4 }]); wrapC.appendChild(tb);
							var tr0 = $1.q('thead tr', tb);
							var tBody = $1.t('tbody'); tb.appendChild(tBody);
							var tr = $1.t('tr'); var trn = 1; tBody.appendChild(tr);
							var n = 0;
							for (var ym in Jq.Ym) {
								var td = $1.t('td'); tr.appendChild(td);
								if (trn % 4 == 0) { tr = $1.t('tr'); tBody.appendChild(tr); } trn++;
								var bgc = $ccH._g('dcpCal', n); n++;
								td.appendChild($1.t('h5', { textNode: $2d.f(ym + '-01', 'mes'), style: 'text-align:center;' + bgc + ' font-size:0.75rem; padding:0.1rem 0;' }));
								if (R.Ym && R.Ym[ym]) {
									var nl = 1;
									for (var l in R.Ym[ym]) {
										var L = R.Ym[ym][l];
										var respStatus = (L.respStatus) ? L.respStatus : 'Pendiente';
										L.cardId = R.cardId;
										var li = $1.t('div', { style: 'width:13rem;' }); td.appendChild(li);
										var btn = $1.t('button', { 'class': 'fa faBtn fa_doc', P: L });
										btn.onclick = function () { GT1.V.open(this.P); }
										li.appendChild(btn);
										li.appendChild($1.t('span', '(' + (L.dueDate).substr(8, 2) + ') '));
										li.appendChild($1.t('span', ' ' + L.grName));
									}
								}
							}
							var inf = $1.t('div', { style: 'padding:0.5rem 0; text-align:center: font-size:0.9rem;' }, wrapC);
							$1.t('b', { textNode: 'Contacto: ' + $Soc.pbx + ' - ' + $Soc.mail }, inf);
							$1.t('div', { style: 'font-size:0.85rem;', textNode: '* El (número) hace referencia al día de vencimiento.' }, wrapC);
						}
						cont.appendChild(wrap);
					}
				}
			});
		}
		,
	}
	,
	W: {
		status: function (P, wrap) {
			var wrap = (wrap) ? wrap : $1.t('div');
			var vPost = 'respId=' + P.respId + '&cardId=' + P.cardId;
			$Api.get({
				f: Api._dcp + 'resp/status', loade: wrap, inputs: vPost, func: function (Jq) {
					var D = (Jq.H) ? Jq.H : {};
					var h4 = $1.t('h4', { 'class': 'head1', textNode: 'Seguimiento Responsabilidad' }); wrap.appendChild(h4);
					var divL = $1.T.divL({ divLine: 1, wxn: 'wrapx4', L: { textNode: 'Estado' }, I: { tag: 'select', sel: { 'class': 'jsFields', name: 'respStatus' }, opts: A03.Vs.respStatus, selected: D.respStatus, O: { vPost: 'respId=' + P.respId + '&cardId=' + P.cardId } } }, wrap);
					$1.T.divL({ wxn: 'wrapx4', L: { textNode: 'No. Formulario' }, I: { tag: 'input', type: 'text', 'class': 'jsFields', name: 'ref1', value: D.ref1 } }, divL);
					$1.T.divL({ wxn: 'wrapx4', L: { textNode: 'Recibo de Pago' }, I: { tag: 'input', type: 'text', 'class': 'jsFields', name: 'ref2', value: D.ref2 } }, divL);
					var divL = $1.T.divL({ divLine: 1, wxn: 'wrapx1', L: { textNode: 'Detalles' }, I: { tag: 'textarea', 'class': 'jsFields', name: 'lineMemo', value: D.lineMemo } }, wrap);
					var resp = $1.t('div', 0, wrap);
					$Api.send({
						textNode: 'Actualizar Estado', PUT: Api._dcp + 'resp/status', getInputs: function () { return vPost + '&' + $1.G.inputs(wrap); }, func: function (Jr2) {
							$Api.resp(resp, Jr2);
							if (!Jr2.errNo) { GT.Resp.W.status(P, wrap); }
						}
					}, wrap);
					var wrapList = $1.t('div', 0, wrap);
					if (Jq.errNo) { $Api.resp(wrapList, Jq); }
					else {
						var tb = $1.T.table(['#', 'Estado', 'Ref. 1', 'Ref. 2', 'Detalles', 'Realizado']);
						wrapList.appendChild(tb);
						var tBody = $1.t('tbody', 0, tb);
						var n = 1;
						for (var i in Jq.L) {
							var L = Jq.L[i];
							$1.t('td', { textNode: n }, tr); n++;
							var tr = $1.t('tr', 0, tBody);
							$1.t('td', { textNode: A03.Vs.respStatus[L.respStatus] }, tr);
							$1.t('td', { textNode: L.ref1 }, tr);
							$1.t('td', { textNode: L.ref2 }, tr);
							$1.t('td', { textNode: L.lineMemo }, tr);
							$1.t('td', { textNode: $Doc.by('userDate', L) }, tr);
						}
					}
					return wrap;
				}
			});
		}
	}
}

/* a03/ base.js */
var ADMS_DCP = {}

A03.Vs = {
	respStatus: { 'pendiente': 'Pendiente', 'Realizada': 'Presentada', 'problemaCliente': 'Presentada y no Pagada', 'pagada': 'Pagada', 'No Presentada': '--No Presentada' },
	respTo: { general: 'General - Todos', GC: 'Grandes Contribuyentes', PJ: 'Personas Jurídicas', PN: 'Personas Naturales', SI: 'Sucesiones Iliquidas' }
	,
	vYear: {
		patrimonio: 'Patrimonio bruto a diciembre 31',
		ingresos: 'Ingresos brutos a diciembre 31',
		consumoTC: 'Consumos con tarjeta de crédito',
		comprasCons: 'Compras y consumos totales del año',
		consigDepInv: 'Consignaciones, depósitos o inversiones',
		respIvaRC: 'Responsable IVA en el Regimen Común'
	}
	,
	vYear_ext: {
		respIvaRC: { opts: { Y: 'Sí', N: 'No' } }
	}
	,
	peoRel: {
		repLeg: { t: 'Representante Legal' }, repLeg2: { t: 'Rep. Legal Primer Suplente' },
		repLeg3: { t: 'Rep. Legal - Segundo Suplente' }, gerente: { t: 'Gerente' }, contador: { t: 'Contador' }, revFiscal: { t: 'Revisoría Fiscal' }
	},
	libOfi: {
		inv: { t: 'Inventarios y Balances' }, mayBal: { t: 'Mayor y Balance' }, cajaDiario: { t: 'Caja Diario' }, actJunDir: { t: 'Actas Junta Directiva' }, libAsamb: { t: 'Libro de Actas Asamblea accionista' }
	}
}

A03.GT.Resp.Anex = {
	get: function (P, wrapCont) {
		$ps_DB.get({
			file: 'GET ' + A03.GT.api + 'Anex', inputs: 'respId=' + P.respId, loade: wrapCont,
			func: function (Jq) {
				if (Jq.errNo) { $ps_DB.response(wrapCont, Jq); }
				else {
					var tb = $1.T.table(['#', 'Detalle']); wrapCont.appendChild(tb);
					var tBody = $1.t('tbody'); tb.appendChild(tBody);
					var n = 1;
					for (var i in Jq.Data) {
						var L = Jq.Data[i];
						var tr = $1.t('tr'); tBody.appendChild(tr);
						tr.appendChild($1.t('td', n)); n++;
						var td = $1.t('td'); tr.appendChild(td);
						td.appendChild($1.t('a', { href: L.linkTo, textNode: L.lineMemo, target: '_BLANK' }));
					}
				}
			}
		});
	}
	,
	put: function (P) {
		var wrap = $1.t('div'); var D = {};
		$ps_DB.get({
			file: 'GET ' + A03.GT.api + 'Anex', inputs: 'respId=' + P.respId, func: function (Jq) {
				wrap.appendChild($1.t('h5', { textNode: '(' + P.respYear + ') ' + P.respName }));
				var tb = $1.T.table(['', 'Detalle', 'Enlace']); wrap.appendChild(tb);
				var tBody = $1.t('tbody'); tb.appendChild(tBody);
				var ni = 1;
				for (var i in Jq.Data) {
					var L = Jq.Data[i];
					addLine(ni, L); ni++;
				}
				function addLine(ni, L) {
					var ln = 'L[' + ni + ']';
					var tr = $1.t('tr'); tBody.appendChild(tr);
					var td = $1.t('td'); tr.appendChild(td);
					var del = $1.t('input', { type: 'checkbox', name: ln + '[delete]', 'class': 'jsFields' }); td.appendChild(del);
					var td = $1.t('td'); tr.appendChild(td);
					var inp = $1.t('input', { type: 'text', name: ln + '[lineMemo]', 'class': 'jsFields', value: L.lineMemo }); td.appendChild(inp);
					var td = $1.t('td'); tr.appendChild(td);
					var inp = $1.t('input', { type: 'text', name: ln + '[linkTo]', 'class': 'jsFields', value: L.linkTo }); td.appendChild(inp);
				}
				addLine(ni, {}); ni++; addLine(ni, {}); ni++;
				var resp = $1.t('div');
				var iSend = $1.T.btnSend({
					value: 'Guardar Información', func: function () {
						$ps_DB.get({
							f: 'PUT ' + A03.GT.api + 'Anex', inputs: 'respId=' + P.respId + '&' + $1.G.inputs(wrap), loade: resp, func: function (Jq2) {
								$ps_DB.response(resp, Jq2);
							}
						});
					}
				});
				wrap.appendChild(resp); wrap.appendChild(iSend);
				$1.Win.open(wrap, { onBody: 1, winTitle: 'Anexos de Responsabilidad', winId: 'respAnex', winSize: 'medium' });
			}
		});

	}
}



var ADMS_DIAN = {
	VARS: {
		calendType: { General: "General", "Municipal": "Municipal" },
	}
	,
	Calend: {
		draw: function (type, SDATA, plain) {
			var cont = $1.t('div');
			if (type == '0-9') {
				for (var n = 10; n >= 1; n--) {
					var n1 = (n == 10) ? '0' : n;
					te = n1;
					var k = 'D' + n;
					var val = (SDATA && SDATA[k]) ? SDATA[k] : '';
					if (plain && val == '') { continue; }
					var spanBox = $1.t('div', { 'class': 'spanBox2' });
					spanBox.appendChild($1.t('span', { textNode: te, 'class': 'tTop' }));
					if (plain) { spanBox.appendChild($1.t('span', { textNode: val, 'class': 'tInput' })); }
					else {
						spanBox.appendChild($1.t('input', { type: 'number', inputmode: 'numeric', min: 1, max: 31, value: val, 'class': 'jsFields tInput', name: 'SDATA[' + k + ']' }));
					}
					cont.appendChild(spanBox);
				}
			}
			else if (type == '99-00') {
				for (var n = 100; n >= 1; n--) {
					var n1 = (n < 10) ? '0' + n : n;
					n1 = (n == 100) ? '00' : n1;
					var n2 = (n - 1 < 10) ? '0' + (n - 1) : n - 1;
					te = n2 + '-' + n1;
					var k = 'D' + n2 + 'D' + n1;
					n--;
					var val = (SDATA && SDATA[k]) ? SDATA[k] : '';
					if (plain && val == '') { continue; }
					var spanBox = $1.t('div', { 'class': 'spanBox2' });
					spanBox.appendChild($1.t('span', { textNode: te, 'class': 'tTop' }));
					if (plain) { spanBox.appendChild($1.t('span', { textNode: val, 'class': 'tInput' })); }
					else {
						spanBox.appendChild($1.t('input', { type: 'number', inputmode: 'numeric', min: 1, max: 31, value: val, 'class': 'jsFields tInput', name: 'SDATA[' + k + ']' }));
					}
					cont.appendChild(spanBox);
				}
			}
			else if (type == '96-00') {
				for (var n = 100; n >= 1; n--) {
					var n1 = (n < 10) ? '0' + n : n;
					n1 = (n == 100) ? '00' : n1;
					var n2 = (n - 1 < 10) ? '0' + (n - 1) : n - 1;
					var n3 = (n - 2 < 10) ? '0' + (n - 2) : n - 2;
					var n4 = (n - 3 < 10) ? '0' + (n - 3) : n - 3;
					var n5 = (n - 4 < 10) ? '0' + (n - 4) : n - 4;
					te = n5 + '-' + n1;
					var k = 'D' + n5 + 'D' + n4 + 'D' + n3 + 'D' + n2 + 'D' + n1;
					n--; n--; n--; n--;
					var val = (SDATA && SDATA[k]) ? SDATA[k] : '';
					if (plain && val == '') { continue; }
					var spanBox = $1.t('div', { 'class': 'spanBox2' });
					spanBox.appendChild($1.t('span', { textNode: te, 'class': 'tTop' }));
					if (plain) { spanBox.appendChild($1.t('span', { textNode: val, 'class': 'tInput' })); }
					else {
						spanBox.appendChild($1.t('input', { type: 'number', inputmode: 'numeric', min: 1, max: 31, value: val, 'class': 'jsFields tInput', name: 'SDATA[' + k + ']' }));
					}
					cont.appendChild(spanBox);
				}
			}
			else if (type == '91-00') {
				for (var n = 100; n >= 1; n--) {
					var n1 = (n < 10) ? '0' + n : n;
					n1 = (n == 100) ? '00' : n1;
					var n2 = (n - 1 < 10) ? '0' + (n - 1) : n - 1;
					var n3 = (n - 2 < 10) ? '0' + (n - 2) : n - 2;
					var n4 = (n - 3 < 10) ? '0' + (n - 3) : n - 3;
					var n5 = (n - 4 < 10) ? '0' + (n - 4) : n - 4;
					var n6 = (n - 5 < 10) ? '0' + (n - 5) : n - 5;
					var n7 = (n - 6 < 10) ? '0' + (n - 6) : n - 6;
					var n8 = (n - 7 < 10) ? '0' + (n - 7) : n - 7;
					var n9 = (n - 8 < 10) ? '0' + (n - 8) : n - 8;
					var n10 = (n - 9 < 10) ? '0' + (n - 9) : n - 9;
					te = n10 + '-' + n1;
					var k = 'D' + n10 + 'D' + n9 + 'D' + n8 + 'D' + n7 + 'D' + n6 + 'D' + n5 + 'D' + n4 + 'D' + n3 + 'D' + n2 + 'D' + n1;
					n--; n--; n--; n--; n--; n--; n--; n--; n--;
					var val = (SDATA && SDATA[k]) ? SDATA[k] : '';
					if (plain && val == '') { continue; }
					var spanBox = $1.t('div', { 'class': 'spanBox2' });
					spanBox.appendChild($1.t('span', { textNode: te, 'class': 'tTop' }));
					if (plain) { spanBox.appendChild($1.t('span', { textNode: val, 'class': 'tInput' })); }
					else {
						spanBox.appendChild($1.t('input', { type: 'number', inputmode: 'numeric', min: 1, max: 31, value: val, 'class': 'jsFields tInput', name: 'SDATA[' + k + ']' }));
					}
					cont.appendChild(spanBox);
				}
			}
			else if (type == '00-10') {
				for (var n = 0; n <= 99; n++) {
					var n1 = (n < 10 && n > 0) ? '0' + n : n; nP = n;
					n1 = (n == 0) ? '00' : n1;
					var n2 = (n + 1 < 10) ? '0' + (n + 1) : n + 1;
					var n3 = (n + 2 < 10) ? '0' + (n + 2) : n + 2;
					var n4 = (n + 3 < 10) ? '0' + (n + 3) : n + 3;
					var n5 = (n + 4 < 10) ? '0' + (n + 4) : n + 4;
					var n6 = (n + 5 < 10) ? '0' + (n + 5) : n + 5;
					var n7 = (n + 6 < 10) ? '0' + (n + 6) : n + 6;
					var n8 = (n + 7 < 10) ? '0' + (n + 7) : n + 7;
					var n9 = (n + 8 < 10) ? '0' + (n + 8) : n + 8;
					var n10 = (n + 9 < 10) ? '0' + (n + 9) : n + 9;
					if (nP == 0) {
						var n11 = (n + 10 < 10) ? '0' + (n + 10) : n + 10;
						te = n1 + '-' + n11;
						var k = 'D' + n11 + 'D' + n10 + 'D' + n9 + 'D' + n8 + 'D' + n7 + 'D' + n6 + 'D' + n5 + 'D' + n4 + 'D' + n3 + 'D' + n2 + 'D' + n1;
						n++;
					}
					else if (n1 == 91) {
						te = n1 + '-' + n9;
						var k = 'D' + n9 + 'D' + n8 + 'D' + n7 + 'D' + n6 + 'D' + n5 + 'D' + n4 + 'D' + n3 + 'D' + n2 + 'D' + n1;
					} else {
						te = n1 + '-' + n10;
						var k = 'D' + n10 + 'D' + n9 + 'D' + n8 + 'D' + n7 + 'D' + n6 + 'D' + n5 + 'D' + n4 + 'D' + n3 + 'D' + n2 + 'D' + n1;
					}
					n++; n++; n++; n++; n++; n++; n++; n++; n++;
					var val = (SDATA && SDATA[k]) ? SDATA[k] : '';
					if (plain && val == '') { continue; }
					var spanBox = $1.t('div', { 'class': 'spanBox2' });
					spanBox.appendChild($1.t('span', { textNode: te, 'class': 'tTop' }));
					if (plain) { spanBox.appendChild($1.t('span', { textNode: val, 'class': 'tInput' })); }
					else {
						spanBox.appendChild($1.t('input', { type: 'number', inputmode: 'numeric', min: 1, max: 31, value: val, 'class': 'jsFields tInput', name: 'SDATA[' + k + ']' }));
					}
					cont.appendChild(spanBox);
				}
			}

			return cont;
		}
		,
		getList: function (Jq) {
			if (Jq.reLoad == true) {
				$ps_DB.get({
					file: '/s/dian/calendList', inputs: '',
					func: function (Jq2) { ADMS_DIAN.Calend.getList(Jq2); }
				});
				return true;
			}
			var idList = 'dianCalendWrapList';
			var cont = document.getElementById(idList);
			ps_DOM.clear(cont);
			if (Jq.error) { $ps_DB.response(cont, Jq); return true; }
			var tb = $1.t('table', { 'class': 'table_zh table_x1024' });
			var tHead = $1.t('thead');
			var tr0 = $1.t('tr');
			tr0.appendChild($1.t('td', { textNode: '#', style: 'width:30px;' }));
			tr0.appendChild($1.t('td', { textNode: 'f(x)', style: 'width:60px;' }));
			tr0.appendChild($1.t('td', { textNode: 'Tipo', style: 'width:60px;' }));
			tr0.appendChild($1.t('td', { textNode: 'Año - Mes', style: 'width:60px;' }));
			tr0.appendChild($1.t('td', { 'textNode': 'Nombre' }));
			tr0.appendChild($1.t('td', { 'textNode': 'Dígito / Día' }));
			tHead.appendChild(tr0);
			tb.appendChild(tHead);
			var tBody = $1.t('tbody');
			var n = 1;
			for (var i in Jq.DATA) {
				var P = Jq.DATA[i];
				var tr1 = $1.t('tr');
				tr1.appendChild($1.t('td', { textNode: P.calId }));
				var tdE = $1.t('td', { style: 'position:relative; width:30px;' });
				var menu = $1.t('input', { type: 'button', 'class': 'btn iBg_menuList' });
				menu.DATA = P;
				menu.onclick = function () {
					var wrap = $1.t('ul', { 'class': 'ulList' });
					var li = $1.t('li');
					var ed = $1.t('input', { type: 'button', 'class': 'btnAddText iBg_edit', value: 'Modificar Calendario' });
					ed.DATA = this.DATA;
					ed.onclick = function () { ADMS_DIAN.Calend.createSimple(this.DATA); }
					li.appendChild(ed);
					wrap.appendChild(li);
					var li = $1.t('li');
					var addResp = $1.t('input', { type: 'button', 'class': 'btnAddText iBg_add', value: 'Añadir Responsabilidad' });
					addResp.D = { calId: this.DATA.calId, calendName: this.DATA.calendName, yearMonth: (this.DATA.calendYear + '-' + this.DATA.calendMonth) };
					addResp.onclick = function () { ADMS_DIAN.Resp.appenD(this.D); }
					li.appendChild(addResp);
					wrap.appendChild(li);
					var wrapBk = ps_DOM.Tag.Win.minRel(wrap, { winTitle: 'Opciones de Calendario' });
					this.parentNode.appendChild(wrapBk);

				}
				tdE.appendChild(menu);
				tr1.appendChild(tdE);
				tr1.appendChild($1.t('td', { textNode: P.calendType }));
				tr1.appendChild($1.t('td', { textNode: P.calendYear + '-' + P.calendMonth }));
				tr1.appendChild($1.t('td', { textNode: P.calendName }));
				var tdC = $1.t('td');
				var dr = ADMS_DIAN.Calend.draw(P.calendGroup, P.sdata, 'plain');
				tdC.appendChild(dr);
				tr1.appendChild(tdC);
				tBody.appendChild(tr1);
			}
			tb.appendChild(tBody);
			cont.appendChild(tb);
		}
		,
		createSimple: function (P) {
			var P = (P) ? P : {};
			var wrap = $1.t('div');
			P.calendYear = (P.calendYear) ? P.calendYear : (new Date()).getFullYear();
			P.calendMonth = (P.calendMonth) ? P.calendMonth : (new Date()).getFullYear();
			P.calendName = (P.calendName) ? P.calendName : '';
			var SDATA = (P.sdata) ? P.sdata : {};
			P.calId = (P.calId) ? P.calId : 'newS';
			wrap.appendChild($1.t('input', { type: 'hidden', 'class': 'jsFields', name: 'calId', value: P.calId }));
			var divLine = $1.t('div', { 'class': 'divLine' });
			var wrapxn = $1.t('div', { 'class': 'wrapx4' });
			wrapxn.appendChild($1.t('label', { 'textNode': 'Tipo' }));
			wrapxn.appendChild(ps_DOM.Tag.select({ sel: { 'class': 'jsFields', name: 'calendType' }, selected: P.calendType, opts: ADMS_DIAN.VARS.calendType }));
			divLine.appendChild(wrapxn);
			var wrapxn = $1.t('div', { 'class': 'wrapx4' });
			wrapxn.appendChild($1.t('label', { 'textNode': 'Año' }));
			wrapxn.appendChild($1.t('input', { type: 'number', inputmode: 'numeric', maxlength: 4, 'class': 'jsFields', name: 'calendYear', placeholder: 'Año', value: P.calendYear }));
			divLine.appendChild(wrapxn);
			wrap.appendChild(divLine);
			var wrapxn = $1.t('div', { 'class': 'wrapx4' });
			wrapxn.appendChild($1.t('label', { 'textNode': 'Mes' }));
			wrapxn.appendChild(ps_DOM.Tag.select({
				sel: { 'class': 'jsFields', name: 'calendMonth' }, selected: 'A' + P.calendMonth
				, opts: ADMS_Date.VARS.num00Months
			}));
			divLine.appendChild(wrapxn);
			wrap.appendChild(divLine);
			var divLine = $1.t('div', { 'class': 'divLine' });
			var wrapxn = $1.t('div', { 'class': 'wrapx1' });
			wrapxn.appendChild($1.t('label', { 'textNode': 'Detalle / Nombre' }));
			wrapxn.appendChild($1.t('input', { type: 'text', maxlength: 100, 'class': 'jsFields', name: 'calendName', placeholder: 'MES ABRIL 201x', value: P.calendName }));
			divLine.appendChild(wrapxn);
			wrap.appendChild(divLine);
			var tbCont = $1.t('div');
			if (P.calendGroup) {
				tbCont.appendChild(ADMS_DIAN.Calend.draw(P.calendGroup, SDATA));
			}
			var wck = $1.t('div', { 'class': 'ckLabel' });
			var c09 = $1.t('input', { type: 'radio', 'class': 'jsFields', name: 'calendGroup', id: 'calStruct1', value: '0-9' });
			c09.onclick = function () {
				ps_DOM.clear(tbCont);
				tbCont.appendChild(ADMS_DIAN.Calend.draw(this.value, SDATA));
			}
			if (P.calendGroup == '0-9') { c09.checked = true; }
			wck.appendChild(c09);
			wck.appendChild($1.t('label', { textNode: '0-9', 'for': 'calStruct1' }));
			var c09 = $1.t('input', { type: 'radio', 'class': 'jsFields', name: 'calendGroup', id: 'calStruct2', value: '99-00' });
			c09.onclick = function () {
				ps_DOM.clear(tbCont);
				tbCont.appendChild(ADMS_DIAN.Calend.draw(this.value, SDATA));
			}
			if (P.calendGroup == '99-00') { c09.checked = true; }
			wck.appendChild(c09);
			wck.appendChild($1.t('label', { textNode: '99-00', 'for': 'calStruct2' }));
			var c09 = $1.t('input', { type: 'radio', 'class': 'jsFields', name: 'calendGroup', id: 'calStruct3', value: '96-00' });
			c09.onclick = function () {
				ps_DOM.clear(tbCont);
				tbCont.appendChild(ADMS_DIAN.Calend.draw(this.value, SDATA));
			}
			if (P.calendGroup == '96-00') { c09.checked = true; }
			wck.appendChild(c09);
			wck.appendChild($1.t('label', { textNode: '96-00', 'for': 'calStruct3' }));
			wrap.appendChild(wck);

			var c09 = $1.t('input', { type: 'radio', 'class': 'jsFields', name: 'calendGroup', id: 'calStruct4', value: '91-00' });
			c09.onclick = function () {
				ps_DOM.clear(tbCont);
				tbCont.appendChild(ADMS_DIAN.Calend.draw(this.value, SDATA));
			}
			if (P.calendGroup == '91-00') { c09.checked = true; }
			wck.appendChild(c09);
			wck.appendChild($1.t('label', { textNode: '91-00', 'for': 'calStruct4' }));
			wrap.appendChild(wck);

			var c09 = $1.t('input', { type: 'radio', 'class': 'jsFields', name: 'calendGroup', id: 'calStruct5', value: '00-10' });
			c09.onclick = function () {
				ps_DOM.clear(tbCont);
				tbCont.appendChild(ADMS_DIAN.Calend.draw(this.value, SDATA));
			}
			if (P.calendGroup == '00-10') { c09.checked = true; }
			wck.appendChild(c09);
			wck.appendChild($1.t('label', { textNode: '00-10', 'for': 'calStruct5' }));
			wrap.appendChild(wck);
			wrap.appendChild($1.t('br'));
			wrap.appendChild($1.t('br'));
			wrap.appendChild(tbCont);
			var p = $1.t('p');
			var iResp = $1.t('div');
			var iSav = $1.t('input', { type: 'button', 'class': 'ui_button', value: 'Guardar' });
			iSav.onclick = function () {
				var vPost = ps_DOM.gImp.byName(wrap);
				$ps_DB.get({
					file: '/s/dian/calendSave', inputs: vPost, func: function (Jq) {
						if (Jq.errNo) { $ps_DB.response(iResp, Jq); }
						else { ps_DOM.delet(wrapBk); ADMS_DIAN.Calend.getList({ reLoad: true }); }
					}
				});
			}
			p.appendChild(iSav);
			p.appendChild(iResp);
			wrap.appendChild(p);
			var wrapBk = ps_DOM.Tag.Win.bkFixed(wrap, { winTitle: 'CALENDARIO DIGITO - DÍA' });
			ps_DOM.body.appendChild(wrapBk);
		}
	}
	,
	Resp: {
		groupAdd: function (Jq, Do) {
			var cardId = Do.cardId;
			if (Jq.reLoad == true) {
				var addPost = (Jq.addPost) ? Jq.addPost : '';
				addPost += '&cardId=' + cardId;
				$ps_DB.get({
					file: '/s/dian/groupList', ApiMethod: 'GET', inputs: addPost,
					func: function (Jq2) { ADMS_DIAN.Resp.groupAdd(Jq2, Do); }
				});
				return true;
			}
			var wrap = $1.t('div');
			for (var i in Jq) {
				var G = Jq[i];
				var groupW = $1.t('div', { style: 'border:1px solid #CCC; padding:4px; margin-bottom:4px;' });
				groupW.appendChild($1.t('h4', { textNode: G.groupName }));
				for (var pe in G.PER) {
					var lFor = 'gPer' + G.PER[pe]['code'];
					var per = $1.t('input', { type: 'checkbox', id: lFor, 'class': 'jsFields', name: 'TGROUPS[' + G.PER[pe]['code'] + ']' });
					if (G.PER[pe]['status'] == 'active') {
						per.setAttribute('checked', 'checked');
					}
					var lab = $1.t('label', { 'for': lFor, textNode: G.PER[pe]['text'] });
					groupW.appendChild(per);
					groupW.appendChild(lab);
				}
				wrap.appendChild(groupW);
			}
			var btns = $1.t('p');
			var resp = $1.t('div');
			var iSend = $1.t('input', { type: 'button', 'class': 'ui_button', value: 'Guardar Relación' });
			iSend.onclick = function () {
				var addP = '&cardId=' + cardId;
				$ps_DB.get({
					file: '/s/dian/group2customer', inputs: ps_DOM.gImp.byName(wrap) + addP,
					func: function (Jq2) {
						$ps_DB.response(resp, Jq2);
					}
				});
			}
			btns.appendChild(iSend);
			btns.appendChild(resp);
			wrap.appendChild(btns);
			var wrapBk = ps_DOM.Tag.Win.bkFixed(wrap, { winTitle: 'Definir Relación de Responsabilidades' });
			ps_DOM.body.appendChild(wrapBk);
		}
		,
		appenD: function (P) {
			var P = (P) ? P : {};
			P.respName = (P.respName) ? P.respName : '';
			P.respId = (P.respId) ? P.respId : 'newS';
			P.calId = (P.calId) ? P.calId : '';
			var wrap = $1.t('div');
			var divLine = $1.t('div', { 'class': 'divLine' });
			var wrapxn = $1.t('div', { 'class': 'wrapx3' });
			wrapxn.appendChild($1.t('label', { 'textNode': 'Periodicidad ' + P.respId }));
			var opts = { mensual: 'Mensual', bimestral: 'Bimestral', trimestral: 'Trimestral', cuatrimestral: 'Cuatrimestral', semestral: 'Semestral', anual: 'Anual' };
			wrapxn.appendChild(ps_DOM.Tag.select({ sel: { 'class': 'jsFields', name: 'respPer' }, selected: P.respPer, opts: opts }));
			divLine.appendChild(wrapxn);
			var wrapxn = $1.t('div', { 'class': 'wrapx3' });
			wrapxn.appendChild($1.t('label', { 'textNode': 'Aplica A' }));
			var opts = { 'general': 'General', GC: 'Gran Contribuyente', PJ: 'Persona Jurídica', PN: 'Persona Natural', DM: 'Demás Contribuyentes', 'RE': 'Regimen Extranjero' };
			wrapxn.appendChild(ps_DOM.Tag.select({ sel: { 'class': 'jsFields', name: 'tipEnt' }, selected: P.tipEnt, opts: opts }));
			divLine.appendChild(wrapxn);
			var wx1 = $1.t('div', { 'class': 'wrapx3' });
			wx1.appendChild($1.t('label', { 'textNode': 'Grupo Responsabilidad' }));
			var inp = $1.t('div', { 'class': 'input' });
			var CF = {
				file: '/s/dian/respGroupList',
				FIE: {
					groupId: {},
					groupName: { nameFrom: 'groupName', visible: 'Y', 'clasBtn': 'inBtn_user', placeholder: 'CREE, IVA, Activos...' }
				}
			};
			inp.appendChild($ps_DB.Search.v1(P, CF));
			wx1.appendChild(inp);
			divLine.appendChild(wx1);
			wrap.appendChild(divLine);
			var divLine = $1.t('div', { 'class': 'divLine' });
			var wrapxn = $1.t('div', { 'class': 'wrapx4_1' });
			wrapxn.appendChild($1.t('label', { 'textNode': 'Nombre' }));
			wrapxn.appendChild($1.t('input', { type: 'text', 'class': 'jsFields', name: 'respName', placeholder: 'IVA Bimestral', value: P.respName }));
			divLine.appendChild(wrapxn);
			wrap.appendChild(divLine);
			var divLine = $1.t('div', { 'class': 'divLine' });
			var wrapxn = $1.t('div', { 'class': 'wrapx1' });
			wrapxn.appendChild($1.t('label', { 'textNode': 'Detalles' }));
			wrapxn.appendChild($1.t('textarea', { 'class': 'jsFields', name: 'respDescrip', placeholder: 'IVA Bimestral', value: P.respName }));
			divLine.appendChild(wrapxn);
			wrap.appendChild(divLine);
			var p = $1.t('p');
			var iResp = $1.t('div');
			var iSav = $1.t('input', { type: 'button', 'class': 'ui_button', value: 'Guardar' });
			iSav.onclick = function () {
				var vPost = 'respId=' + P.respId + '&calId=' + P.calId + '&yearMonth=' + P.yearMonth + '&' + ps_DOM.gImp.byName(wrap);
				$ps_DB.get({
					file: '/s/dian/respSave', inputs: vPost, func: function (Jq) {
						if (Jq.ok) {
							if (P.respId == 'newS') { ps_DOM.clearInps(wrap); }
							ADMS_DIAN.Resp.getList({ reLoad: true, addPost: 'C.calId=' + P.calId }, wrapList);
						}
						$ps_DB.response(iResp, Jq);
					}
				});
			}
			p.appendChild(iSav);
			if (P.respId != 'newS') {
				var iNew = $1.t('input', { type: 'button', 'class': 'ui_button', value: 'Añadir Nuevo' });
				iNew.onclick = function () { ADMS_DIAN.Resp.appenD({ calId: P.calId, yearMonth: P.yearMonth }); }
				p.appendChild(iNew);
			}
			p.appendChild(iResp);
			wrap.appendChild(p);
			var wrapList = $1.t('div');
			ADMS_DIAN.Resp.getList({ reLoad: true, addPost: 'C.calId=' + P.calId }, wrapList);
			wrap.appendChild(wrapList);
			ps_DOM.delet('dianRespWrapForm');
			var wrapBk = ps_DOM.Tag.Win.bkFixed(wrap, { winTitle: P.calendName + ' - Responsabilidades ' });
			wrapBk.id = 'dianRespWrapForm';
			ps_DOM.body.appendChild(wrapBk);
		}
		,
		getList: function (Jq, cont) {
			if (Jq.reLoad == true) {
				var addPost = (Jq.addPost) ? Jq.addPost : '';
				$ps_DB.get({
					file: '/s/dian/respList', inputs: addPost,
					func: function (Jq2) { ADMS_DIAN.Resp.getList(Jq2, cont); }
				});
				return true;
			}
			var wrap = $1.t('div');
			if (Jq.error) { $ps_DB.response(wrap, Jq); }
			else {
				var tb = $1.t('table', { 'class': 'table_zh table_x100' });
				var tHead = $1.t('thead');
				var tr0 = $1.t('tr');
				tr0.appendChild($1.t('td', { textNode: '#', style: 'width:30px;' }));
				tr0.appendChild($1.t('td', { textNode: '', style: 'width:30px;' }));
				tr0.appendChild($1.t('td', { 'textNode': 'Grupo', style: 'width:50px;' }));
				tr0.appendChild($1.t('td', { 'textNode': 'Periodicidad', style: 'width:50px;' }));
				tr0.appendChild($1.t('td', { 'textNode': 'Aplica', style: 'width:50px;' }));
				tr0.appendChild($1.t('td', { 'textNode': 'Nombre' }));
				tr0.appendChild($1.t('td', { 'textNode': 'Detalles' }));
				tHead.appendChild(tr0);
				tb.appendChild(tHead);
				var tBody = $1.t('tbody');
				var n = 1;
				for (var i in Jq.DATA) {
					var P = Jq.DATA[i];
					var tr1 = $1.t('tr');
					tr1.appendChild($1.t('td', { textNode: n })); n++;
					var tdE = $1.t('td');
					var ed = $1.t('input', { type: 'button', 'class': 'btn iBg_edit' });
					ed.DATA = P;
					ed.onclick = function () { ADMS_DIAN.Resp.appenD(this.DATA); }
					tdE.appendChild(ed);
					tr1.appendChild(tdE);
					tr1.appendChild($1.t('td', { textNode: P.groupName }));
					tr1.appendChild($1.t('td', { textNode: P.respPer }));
					tr1.appendChild($1.t('td', { textNode: P.tipEnt }));
					tr1.appendChild($1.t('td', { textNode: P.respName }));
					tr1.appendChild($1.t('td', { textNode: P.respDescrip }));
					tBody.appendChild(tr1);
				}
				tb.appendChild(tBody);
				wrap.appendChild(tb);
			}
			if (cont) { ps_DOM.clear(cont); cont.appendChild(wrap); }
			else { ps_DOM.Tag.Win.bkFixed(wrap, { onBody: true, winTitle: 'Responsabilidades' }); }
		}
		,
		search2Add: function (D) {
			var D = (D) ? D : {};
			D.cardId = (D.cardId) ? D.cardId : '';
			var cont = $1.G.byId('dianRespSearch2Add');
			var divAssig = $1.t('div', { 'class': 'divLine' });
			cont.appendChild($1.t('input', { type: 'hidden', 'class': 'jsFields', name: 'cardId', value: D.cardId }));
			var wx1 = $1.t('div', { 'class': 'wrapx4_1' });
			wx1.appendChild($1.t('label', { 'textNode': 'Añadir Responsabilidad' }));
			var inp = $1.t('div', { 'class': 'input' });
			var CF = {
				file: '/s/dian/respSearch',
				FIE: {
					respId: {},
					respName: { nameFrom: 'respName', visible: 'Y', 'clasBtn': 'inBtn_user', placeholder: 'Digite...' }
				}
			};
			inp.appendChild($ps_DB.Search.v1(D, CF));
			wx1.appendChild(inp);
			divAssig.appendChild(wx1);
			var wx1 = $1.t('div', { 'class': 'wrapx4' });
			wx1.appendChild($1.t('label', { 'textNode': ' -' }));
			var iSave = $1.t('input', { type: 'button', 'class': 'ui_button input', value: 'Añadir' });
			iSave.onclick = function () {
				var vPost = ps_DOM.gImp.byName(cont);
				$ps_DB.get({
					file: '/s/dian/respAssg', inputs: vPost, func: function (Jq2) {
						if (Jq2.ok) { ps_DOM.clearInps(divAssig); ADMS_DIAN.Resp.assgSaved({ reLoad: true }); }
						else { ps_DOM.Tag.Win.message(Jq2); }
					}
				});
			}
			wx1.appendChild(iSave);
			divAssig.appendChild(wx1);
			cont.appendChild(divAssig);
		}
		,
		assgSaved: function (Jq, PD) {
			var PD = (PD) ? PD : {};
			var cont = $1.G.byId('dianRespAssgSaved');
			var formu = $1.G.byId('dianRespAssgSavedFormu');
			if (Jq.reLoad == true) {
				var vPost = ps_DOM.gImp.byName(formu, 'jsFiltVars');
				$ps_DB.get({
					file: '/s/dian/respAssgSaved', inputs: vPost, loade: cont,
					func: function (Jq2) { ADMS_DIAN.Resp.assgSaved(Jq2, PD); }
				});
				return true;
			}
			if (Jq.error) { $ps_DB.response(cont, Jq); }
			else if (Jq.DATA && !Jq.DATA[0]) {
				$ps_DB.response(cont, { text: 'No se encontraron resultados' });
			}
			else {
				var DaF = (Jq.Pagination) ? Jq.Pagination : {};
				DaF.func = function () { ADMS_DIAN.Resp.assgSaved({ reLoad: true });; }
				$ps_DB.getPager(DaF, cont);
				var tb = $1.t('table', { 'class': 'table_zh table_x600' });
				var tHead = $1.t('thead');
				var tr0 = $1.t('tr');
				tr0.appendChild($1.t('td', { textNode: '#', style: 'width:30px;' }));
				tr0.appendChild($1.t('td', { textNode: 'Semana', style: 'width:30px;' }));
				tr0.appendChild($1.t('td', { textNode: 'Estado', style: 'width:50px;' }));
				tr0.appendChild($1.t('td', { textNode: 'NIT', style: 'width:50px;' }));
				tr0.appendChild($1.t('td', { textNode: 'DV', style: 'width:50px;' }));
				tr0.appendChild($1.t('td', { 'textNode': 'Tip. Reg', style: 'width:50px;' }));
				tr0.appendChild($1.t('td', { 'textNode': 'Tip. Ent', style: 'width:50px;' }));
				tr0.appendChild($1.t('td', { 'textNode': 'Aplica a', style: 'width:50px;' }));
				tr0.appendChild($1.t('td', { textNode: 'Fecha', style: 'width:100px;' }));
				tr0.appendChild($1.t('td', { 'textNode': 'Grupo', style: 'width:60px;' }));
				tr0.appendChild($1.t('td', { 'textNode': 'Tipo', style: 'width:50px;' }));
				tr0.appendChild($1.t('td', { 'textNode': 'Periodicidad', style: 'width:50px;' }));
				tr0.appendChild($1.t('td', { 'textNode': 'Responsabilidad' }));
				if (PD.listCustomer == 'Y') {
					tr0.appendChild($1.t('td', { 'textNode': 'Cliente' }));
					tr0.appendChild($1.t('td', { 'textNode': 'Responsable' }));
				}
				tr0.appendChild($1.t('td', { 'textNode': 'Id' }));
				tHead.appendChild(tr0);
				tb.appendChild(tHead);
				var tBody = $1.t('tbody');
				var n = 1;
				for (var i in Jq.DATA) {
					var P = Jq.DATA[i];
					var tr1 = $1.t('tr');
					tr1.appendChild($1.t('td', { textNode: n })); n++;
					tr1.appendChild($1.t('td', { textNode: P.week }));;
					var rStat = $1.t('td', { style: 'position:relative;' });
					var iStat = $1.t('input', { type: 'button', 'class': 'btn iBg_menuList' });
					iStat.P = P;
					iStat.onclick = function () { ADMS_DCP.Resp.getMenu(this.P, this); }
					rStat.appendChild(iStat);
					rStat.appendChild($1.t('span', { textNode: ' ' + P.respStatus }));
					tr1.appendChild(rStat);
					tr1.appendChild($1.t('td', { textNode: P.licTradNum }));
					tr1.appendChild($1.t('td', { textNode: P.dv }));
					tr1.appendChild($1.t('td', { textNode: P.RF_regTrib }));
					tr1.appendChild($1.t('td', { textNode: P.RF_tipEnt }));
					tr1.appendChild($1.t('td', { textNode: P.tipEnt }));
					tr1.appendChild($1.t('td', { textNode: P.dueDate }));
					tr1.appendChild($1.t('td', { textNode: P.calendType }));
					tr1.appendChild($1.t('td', { textNode: P.groupName }));
					tr1.appendChild($1.t('td', { textNode: P.respPer }));
					tr1.appendChild($1.t('td', { textNode: P.respName }));
					if (PD.listCustomer == 'Y') {
						var toCard = $1.t('td');
						toCard.appendChild(ADMSys.Links.goTo(P, { linkType: 'customer' }));
						tr1.appendChild(toCard);
						tr1.appendChild($1.t('td', { textNode: P.userAssgName }));
					}
					tr1.appendChild($1.t('td', { textNode: P.respId }));
					tBody.appendChild(tr1);
				}
				tb.appendChild(tBody);
				var tb = ps_DOM.Tag.tbExport(tb);
				cont.appendChild(tb);
			}
		}
		,
		Form: {
			simple: function (P) {
				var P = (P) ? P : {};
				P.respStatus = (P.respStatus) ? P.respStatus : '';
				P.cardId = (P.cardId) ? P.cardId : '';
				var wrap = $1.t('div'); /* id=dianRespAssgSavedFormu*/
				var divLine = $1.t('div', { 'class': 'divLine' });
				var wx8 = $1.t('div', { 'class': 'wrapx8' });

				if (P.listCustomer == 'Y') {
					P.tbAlias2 = 'B';
					var divLine = $1.t('div', { 'class': 'divLine' });
					divLine.appendChild($1h.card(P, { wxn: 'wrapx2', readonly: 'N', wrap: divLine, _i1: 'B.cardId', _i2: 'B.cardName(E_like3)' }));
					wrap.appendChild(divLine);
					var D = P; D.tbAlias2 = 'B';
					divLine.appendChild($1h.userAssg(D, { wxn: 'wrapx2', divLine: false, L: { textNode: 'Responsable' } }));
					wrap.appendChild(divLine);
				}
				else {
					wrap.appendChild($1.t('input', { type: 'hidden', 'class': 'jsFiltVars', name: 'B.cardId', value: P.cardId }));
				}
				divLine.appendChild(wx8);
				var divLine = $1.t('div', { 'class': 'divLine' });
				var wx8 = $1.t('div', { 'class': 'wrapx4' });
				var date1 = ADMS_Date.weekBegin(ADMS_Date.get2('Y-m-d'));
				wx8.appendChild($1.t('label', { textNode: 'Fecha Inicio' }));
				wx8.appendChild($1.t('input', { type: 'date', 'class': 'input jsFiltVars', name: 'date1', value: date1 }));
				divLine.appendChild(wx8);
				var wx8 = $1.t('div', { 'class': 'wrapx4' });
				var date2 = ADMS_Date.weekEnd(ADMS_Date.get2('Y-m-d'));
				wx8.appendChild($1.t('label', { textNode: 'Fecha Fin' }));
				wx8.appendChild($1.t('input', { type: 'date', 'class': 'input jsFiltVars', name: 'date2', value: date2 }));
				divLine.appendChild(wx8);
				var wx8 = $1.t('div', { 'class': 'wrapx4' });
				wx8.appendChild($1.t('label', { textNode: 'Responsabilidad' }));
				wx8.appendChild($1.t('input', { type: 'numeric', min: 2016, inputmode: 'numeric', 'class': 'input jsFiltVars', name: 'C.respName(E_like3)' }));
				divLine.appendChild(wx8);
				var wx8 = $1.t('div', { 'class': 'wrapx4' });
				wx8.appendChild($1.t('label', { textNode: 'Estado' }));
				wx8.appendChild(ps_DOM.Tag.select({ sel: { 'class': 'jsFiltVars', name: 'STATUS[respStatus]' }, opts: ADMS_DCP.Resp.VARS.statusResp, selected: P.respStatus }));
				divLine.appendChild(wx8);
				wrap.appendChild(divLine);
				var p = $1.t('p');
				var iS = $1.t('input', { type: 'button', 'class': 'ui_button', value: 'Buscar' });
				iS.onclick = function () { ADMS_DIAN.Resp.assgSaved({ reLoad: true }, P); }
				p.appendChild(iS);
				wrap.appendChild(p);
				var field = $1.t('fieldset');
				field.appendChild($1.t('legend', { textNode: 'Formulario de Busqueda' }));
				field.appendChild(wrap);
				return field
			}
		}
	}

}

A03.Auxi = {
	api: '/sa/03/auxi/A:',
	formAssgResp: function () {
		var wrap = $M.Ht.cont;
		$ps_DB.get({
			f: 'GET ' + A03.Auxi.api + 'formAssgResp', loade: wrap, inputs: $1.G.inputs($1.q('#formWrap'), 'jsFiltVars'), func: function (Jq) {
				if (Jq.Cards.errNo) { $ps_DB.response(wrap, Jq.Cards); }
				else if (Jq.GRT.errNo) { $ps_DB.response(wrap, Jq.GRT); }
				else {
					var len = $js.length(Jq.GRT);
					var divVars = $1.T.ffLine({ ffLine: 1, w: 'ffxauto', t: 'Patrimonio:', v: '$__________' });
					divVars.insertBefore($1.t('span', { style: 'font-size:0.75rem;', textNode: 'Año 2017: ' }), divVars.firstChild);
					divVars.appendChild($1.T.ffLine({ w: 'ffxauto', t: 'Ingresos:', v: '$________' }));
					divVars.appendChild($1.T.ffLine({ w: 'ffxauto', t: 'Consumos TC:', v: '$________' }));
					divVars.appendChild($1.T.ffLine({ w: 'ffxauto', t: 'Compras y Consumos:', v: '$________' }));
					divVars.appendChild($1.T.ffLine({ w: 'ffxauto', t: 'Consign., Dep. o Inv.', v: '_$__________' }));
					var divVars2 = $1.T.ffLine({ ffLine: 1, w: 'ffxauto', t: 'Ciudad:', v: '__________________' });
					divVars2.appendChild($1.T.ffLine({ w: 'ffxauto', t: 'Teléfono/s:', v: '___________________' }));
					divVars2.appendChild($1.T.ffLine({ w: 'ffxauto', t: 'Email/s:', v: '_________________________' }));
					divVars2.appendChild($1.T.ffLine({ w: 'ffxauto', t: 'Firma:', v: '_________________________' }));
					var tb = $1.T.table(['', 'Grupo', 'Familia', 'Periodicidad', '', '', 'Grupo', 'Familia', 'Periodicidad']);
					var tBody = $1.t('tbody'); tb.appendChild(tBody);
					var n = 1;
					for (var i in Jq.GRT) {
						var L = Jq.GRT[i];
						if (n == 1 || n % 2) { var tr = $1.t('tr'); tBody.appendChild(tr); }
						var td = $1.t('td'); tr.appendChild(td);
						var ck = $1.T.ckLabel({ 'class': 'jsFields __' + L.grCode, name: 'GR[' + L.grId + ']', YN: true, checked: (L.checked == 'Y') });
						ck.childNodes[0].grCode = L.grCode;
						ck.childNodes[0].onclick = function () {
							var ts = $1.q('.__' + this.grCode, wrap, 'all');
							for (var n = 0; n < ts.length; n++) {
								if (ts[n] != this) { (ts[n]).checked = false; }
							}
						}
						td.appendChild(ck);
						tr.appendChild($1.t('td', L.grName));
						tr.appendChild($1.t('td', L.grPrim));
						tr.appendChild($1.t('td', L.grPeriod));
						if (n == 1) {
							tr.appendChild($1.t('td', { rowspan: (len / 2), style: 'backgroundColor:#CCC; width:2rem;' }));
						}
						n++;
					}
					for (var i in Jq.Cards) {
						var C = Jq.Cards[i];
						var ffLine = $1.T.ffLine({ ffLine: 1, w: 'ffxauto', t: 'Id:', v: C.cardId * 1 });
						ffLine.appendChild($1.T.ffLine({ w: 'ffxauto', t: 'Cliente:', v: C.cardName }));
						ffLine.appendChild($1.T.ffLine({ w: 'ffxauto', t: 'Responsable:', v: C.userAssgName }));
						ffLine.appendChild($1.T.ffLine({ w: 'ffxauto', t: 'Reg/Pers.:', v: '\u00A0\u00A0\u00A0\u00A0 / \u00A0\u00A0\u00A0\u00A0' }));
						ffLine.appendChild($1.T.ffLine({ w: 'ffxauto', t: 'Documento:', v: C.licTradNum }));
						var tb1 = tb.cloneNode(true);
						var vars = divVars.cloneNode(true);
						var vars2 = divVars2.cloneNode(true);
						wrap.appendChild(ffLine);
						wrap.appendChild(tb1);
						wrap.appendChild($1.t('br'))
						wrap.appendChild(vars); wrap.appendChild(vars2);
						wrap.appendChild($1.t('hr'));
						wrap.appendChild($1.t('br'));
						if (n > 1 && n % 2 == 0) {
							wrap.appendChild($1.t('div', { 'class': 'print_pagBreakBef' }));
						} n++;
					}
				}
			}
		});
	}
}

A03.Fic = {
	form: function () {
		var wrap = $M.Ht.cont; var jsF = 'jsFields';
		var Pa = $M.read();
		$Api.get({
			f: Api._dcp + 'ficha', loadVerif: !Pa.cardId, loade: wrap, inputs: 'cardId=' + Pa.cardId, func: function (Jq) {
				var H = (Jq) ? Jq : {};
				H.Addr = (H.Addr) ? H.Addr : {};
				H.vars = (H.vars) ? H.vars : {};
				H.peoAcc = (H.peoAcc) ? H.peoAcc : {};
				H.peoJunta = (H.peoJunta) ? H.peoJunta : {};
				var nij = 1;
				var ff = $1.T.ffLine({ ffLine: 1, t: 'Razón Social: ', w: 'ffx1', v: H.cardName });
				wrap.appendChild(ff);
				var ff = $1.T.ffLine({ ffLine: 1, w: 'ffx3', t: $V.licTradType[H.licTradType] + ':', v: H.licTradNum });
				ff.appendChild($1.T.ffLine({ t: 'Tipo Contribuyente:', w: 'ffx3', v: $V.RF_regTrib[H.RF_regTrib] }));
				ff.appendChild($1.T.ffLine({ t: 'Tipo Entidad:', w: 'ffx3', v: $V.RF_tipEnt[H.RF_tipEnt] }));
				wrap.appendChild(ff);

				var divL = $1.T.divL({ divLine: 1, wxn: 'wrapx4_1', L: { textNode: 'Constitución' }, I: { tag: 'input', type: 'text', 'class': jsF, name: 'constitucion', value: H.constitucion } });
				divL.appendChild($1.T.divL({ wxn: 'wrapx4', L: { textNode: 'Vigencia' }, I: { tag: 'input', type: 'date', 'class': jsF, name: 'constiDue', value: H.constiDue } }));
				wrap.appendChild(divL);
				var divL = $1.T.divL({ divLine: 1, wxn: 'wrapx1', L: { textNode: 'Matrícula de Cámara y Comercio' }, I: { tag: 'input', type: 'text', 'class': jsF, name: 'matrcyc', value: H.matrcyc } });
				wrap.appendChild(divL);
				var divL = $1.T.divL({ divLine: 1, wxn: 'wrapx4', L: { textNode: 'Actividad Principal' }, I: { tag: 'input', type: 'text', 'class': jsF, name: 'actPrinc', value: H.actPrinc } });
				divL.appendChild($1.T.divL({ wxn: 'wrapxauto', L: { textNode: 'Desde' }, I: { tag: 'input', type: 'date', 'class': jsF, name: 'actFrom', value: H.actFrom } }));
				divL.appendChild($1.T.divL({ wxn: 'wrapx4', L: { textNode: 'Actividad Secundaria' }, I: { tag: 'input', type: 'text', 'class': jsF, name: 'actSec', value: H.actSec } }));
				divL.appendChild($1.T.divL({ wxn: 'wrapx4', L: { textNode: 'Clave DIAN' }, I: { tag: 'input', type: 'text', 'class': jsF, name: 'vars[claveDian]', value: H.vars.claveDian } }));
				wrap.appendChild(divL);
				var divL = $1.T.divL({ divLine: 1, wxn: 'wrapx3', L: { textNode: 'Resolución de Facturación' }, I: { tag: 'input', type: 'text', 'class': jsF, name: 'resolFactu', placeholder: 'Papel(01), Resolución 1000xxx...', value: H.resolFactu } });
				divL.appendChild($1.T.divL({ wxn: 'wrapx3', L: { textNode: 'Ult. Factura Realizad', abrr: 20 }, I: { tag: 'input', type: 'text', 'class': jsF, name: 'resolAutorizada', value: H.resolAutorizada, placeholder: 'de la fra 75001 a la 81000' } }));
				divL.appendChild($1.T.divL({ wxn: 'wrapx3', L: { textNode: 'Ult. Actualización RUT' }, I: { tag: 'input', type: 'date', 'class': jsF, name: 'rutDateUpd', value: H.rutDateUpd } }));
				wrap.appendChild(divL);
				//junta --
				wrap.appendChild($1.t('h5', { 'class': 'head1', textNode: 'Junta Directiva' }));
				var tb = $1.T.table(['Tipo', 'Nombre', 'Doc.']); wrap.appendChild(tb);
				var tBody = $1.t('tbody', { 'class': '__tbJunta' }); tb.appendChild(tBody);
				for (var i in H.peoJunta) { trJunta(nij, H.peoJunta[i]); nij++; }
				var tr = $1.t('tr', 0, tBody);
				var td = $1.t('td', { colspan: 3 }, tr);
				var btnAdd = $1.T.btnFa({ fa: 'fa_plusCircle', textNode: ' Añadir Miembro', func: function () { trJunta(nij, {}, 'last'); nij++; } });
				td.appendChild(btnAdd);
				//accionistas --
				wrap.appendChild($1.t('h5', { 'class': 'head1', textNode: 'Accionistas' }));
				var divLine = $1.T.divL({ divLine: 1, wxn: 'wrapx4', L: { textNode: 'Valor Acción' }, I: { tag: 'input', type: 'number', inputmode: 'numeric', 'class': jsF + ' __valaccion', name: 'peoAcc[valorAccion]', value: H.peoAcc.valorAccion } });
				var vacc = $1.q('.__valaccion', divLine);
				vacc.onchange = vacc.onkeyup = function () { updAcc(); }
				delete (H.peoAcc.valorAccion);
				wrap.appendChild(divLine);
				var tb = $1.T.table(['Nombre', 'Doc.', { textNode: 'No. de acciones', style: 'width:3rem;' }, { textNode: 'V/R Suscripción', style: 'width:3rem;' }]); wrap.appendChild(tb);
				var tBody = $1.t('tbody', { 'class': '__tbAccion' }); tb.appendChild(tBody);
				for (var i in H.peoAcc) { trAcc(nij, H.peoAcc[i]); nij++; }
				var tr = $1.t('tr', { 'class': '__trTotales' }, tBody);
				var td = $1.t('td', { colspan: 2, textNode: 'Total', style: 'backgroundColor:#CCC;' }, tr);
				var td = $1.t('td', { 'class': '__noTotalAcc', style: 'backgroundColor:#CCC;' }, tr);
				var td = $1.t('td', { 'class': '__vrTotalAcc', style: 'backgroundColor:#CCC;' }, tr);
				var td = $1.t('td', { style: 'backgroundColor:#CCC;' }, tr);
				var tr = $1.t('tr', 0, tBody);
				var td = $1.t('td', { colspan: 5 }, tr);
				var btnAdd = $1.T.btnFa({ fa: 'fa_plusCircle', textNode: ' Añadir Accionista', func: function () { trAcc(nij, {}, 'last'); nij++; } });
				td.appendChild(btnAdd);
				var tb = $1.T.table(['Libro', 'Anuladas', 'Cantidad', 'Utilizadas', 'Últ. Actualización']); wrap.appendChild(tb);
				var tBody = $1.t('tbody', 0, tb);
				for (var i in A03.Vs.libOfi) {
					var Li = A03.Vs.libOfi[i];
					var tr = $1.t('tr'); tBody.appendChild(tr);
					var td = $1.t('td', Li.t); tr.appendChild(td);
					var nam = 'libOfi[' + i + ']';
					var tPe = (H.libOfi && H.libOfi[i]) ? H.libOfi[i] : {};
					var td = $1.t('td', 0, tr);
					td.appendChild($1.t('input', { type: 'text', 'class': jsF, name: nam + '[anuladas]', style: 'width:4rem;', value: tPe.anuladas }));
					var td = $1.t('td', 0, tr);
					td.appendChild($1.t('input', { type: 'text', 'class': jsF, name: nam + '[cantidad]', style: 'width:4rem;', value: tPe.cantidad }));
					var td = $1.t('td', 0, tr);
					td.appendChild($1.t('input', { type: 'text', 'class': jsF, name: nam + '[utilizadas]', style: 'width:4rem;', value: tPe.utilizadas }));
					var td = $1.t('td', 0, tr);
					td.appendChild($1.t('input', { type: 'text', 'class': jsF, name: nam + '[ultActualizacion]', style: 'width:4rem;', value: tPe.ultActualizacion }));
				}
				wrap.appendChild($1.T.divL({ divLine: 1, wxn: 'wrapx1', L: { textNode: 'Observaciones' }, I: { tag: 'textarea', 'class': jsF, name: 'lineMemo', value: H.lineMemo, placeHolder: 'Observaciones sobre el cliente...' } }));
				var resp = $1.t('div', 0, wrap);
				$Api.send({
					PUT: Api._dcp + 'ficha', getInputs: function () { return 'cardId=' + Pa.cardId + '&' + $1.G.inputs(wrap); }, func: function (Jr2) {
						$Api.resp(resp, Jr2);
					}
				}, wrap);

				function trJunta(ni, L, add) {
					var junType = { principal: 'Principal', suplente: 'Suplente', otro: 'Otros' };
					var tBody = $1.q('.__tbJunta');
					var jname = 'peoJunta[' + ni + ']';
					var tr = $1.t('tr');
					if (add) { tBody.insertBefore(tr, tBody.lastChild); } else { tBody.appendChild(tr); }
					var td = $1.t('td'); tr.appendChild(td);
					td.appendChild($1.T.sel({ sel: { 'class': jsF, name: jname + '[type]' }, opts: junType, selected: L.type }));
					var td = $1.t('td'); tr.appendChild(td);
					td.appendChild($1.t('input', { type: 'text', 'class': jsF, name: jname + '[name]', value: L.name }));
					var td = $1.t('td'); tr.appendChild(td);
					td.appendChild($1.t('input', { type: 'text', 'class': jsF, name: jname + '[cc]', value: L.cc }));
					var td = $1.t('td', 0, tr);
					$1.T.btnFa({ fa: 'fa_close', func: function (T) { $1.delet(T.parentNode.parentNode); } }, td);
				}
				function trAcc(ni, L, add) {
					var tBody = $1.q('.__tbAccion');
					var jname = 'peoAcc[' + ni + ']';
					var tr = $1.t('tr', { 'class': '__accline' });
					if (add) { tBody.insertBefore(tr, $1.q('.__trTotales', tBody)); } else { tBody.appendChild(tr); }
					var td = $1.t('td'); tr.appendChild(td);
					td.appendChild($1.t('input', { type: 'text', 'class': jsF, name: jname + '[name]', value: L.name }));
					var td = $1.t('td'); tr.appendChild(td);
					td.appendChild($1.t('input', { type: 'text', 'class': jsF, name: jname + '[cc]', value: L.cc }));
					var td = $1.t('td'); tr.appendChild(td);
					var noacc = $1.t('input', { type: 'number', inputmode: 'numeric', 'class': jsF + ' __noacc', name: jname + '[noacc]', value: L.noacc });
					noacc.onchange = noacc.onkeyup = function () {
						updAcc();
					}
					td.appendChild(noacc);
					var td = $1.t('td', { 'class': '__vreal' }); tr.appendChild(td);
					var td = $1.t('td', { 'class': '__partic' }); tr.appendChild(td);
					$1.T.btnFa({ fa: 'fa_close', func: function (T) { $1.delet(T.parentNode.parentNode); } }, td);
				}
				function updAcc() {
					var tb = $1.q('.__tbAccion');
					var trs = $1.q('tr.__accline', tb, 'all');
					var valacc = $1.q('.__valaccion').value;
					var vrTotal = 0; var accTotal = 0;
					for (var i = 0; i < trs.length; i++) {
						var acc = $1.q('.__noacc', trs[i]).value;
						var vr = acc * valacc;
						accTotal += acc * 1;
						$1.q('.__vreal', trs[i]).innerText = vr;
						vrTotal += vr;
					}
					for (var i = 0; i < trs.length; i++) {
						var vr = $1.q('.__vreal', trs[i]).innerText * 1;
						$1.q('.__partic', trs[i]).innerText = $js.toFixed(vr / vrTotal * 100, 0) + '%';
					}
					$1.q('.__noTotalAcc').innerText = accTotal;
					$1.q('.__vrTotalAcc').innerText = vrTotal;
				}
			}
		});
	}
}



GT.Not = {
	timeout: false, sends: 0, limitSend: 5,
	//wait on 5 sends
	load: function () {
		var cont = $1.q('#_wrapContent');
		$ps_DB.get({
			file: 'PUT ' + A03.GT.api + 'Not.load', inputs: $1.G.inputs($1.q('#_wrapForm'), 'jsFiltVars'), loade: cont, func: function (Jq) {
				$ps_DB.response(cont, Jq);
			}
		});
	}
	,
	byCard: function () {
		var cont = $M.Ht.cont;
		$ps_DB.get({
			file: 'GET ' + A03.GT.api + 'Not.byCard', inputs: $1.G.inputs($1.q('#_wrapForm'), 'jsFiltVars'), loade: cont, func: function (Jq) {
				if (Jq.errNo) { $ps_DB.response(cont, Jq); }
				else {
					var tb = $1.T.table(['#', 'Estado', 'Ult. Notificación', 'Días', 'Vencimiento', 'Responsabilidad']);
					var tBody = $1.t('tbody'); tb.appendChild(tBody);
					var n = 1;
					var bg = 'backgroundColor:#DDD;';
					for (var i in Jq.Card) {
						var R = Jq.Card[i];
						var tr = $1.t('tr', { 'class': '__cardLine_' + R.cardId }); tBody.appendChild(tr);
						var td = $1.t('td', { colspan: 10, style: bg }); tr.appendChild(td);
						td.appendChild($1.t('b', R.cardName));
						td.appendChild($1.t('span', ' | '));
						td.appendChild($1.t('span', { 'class': 'spanLine', textNode: R.licTradNum }));
						td.appendChild($1.t('span', ' Resp: '));
						td.appendChild($1.t('span', { 'class': 'spanLine', textNode: R.userAssgName }));
						td.appendChild($1.t('br'));
						var na = 'L[' + R.cardId + ']'; var jsF = 'jsFields2';
						var ck = $1.t('input', { type: 'checkbox', 'class': jsF + ' __cardId', name: 'send', O: { vPost: 'cardName=' + R.cardName + '&userAssgName=' + R.userAssgName + '&userEmail=' + R.userEmail + '&cardId=' + R.cardId } });
						ck.cardId = R.cardId;
						ck.Lsend = R.Lsend;
						td.appendChild(ck);
						td.appendChild($1.t('span', 'Notificar |'));
						var asu = $1.t('input', { type: 'text', style: 'width:80%;', 'class': jsF, name: 'asunt', placeholder: 'Asunto', value: 'Vencimientos Próximos' }); td.appendChild(asu);
						var emailTo = $1.t('input', { type: 'text', 'class': jsF, style: 'width:98%;', name: 'mailTo', 'placeholder': 'Emails a Notificar @', value: R.emailTo }); td.appendChild(emailTo);
						if (R.L) {
							var nl = 1;
							for (var l in R.L) {
								var L = R.L[l];
								var css = (L.daysToDue < 7) ? 'backgroundColor:#FF0;' : '';
								css = (L.daysToDue < 5) ? 'backgroundColor:#F00;' : css;
								css = (L.daysToDue < 3) ? 'backgroundColor:#F0F;' : css;
								var tr = $1.t('tr', { 'class': '__cardLine_' + R.cardId }); tBody.appendChild(tr);
								var respStatus = (L.respStatus) ? L.respStatus : 'Pendiente';
								tr.appendChild($1.t('td', n)); n++;
								tr.appendChild($1.t('td', respStatus));
								tr.appendChild($1.t('td', L.lastNotDate));
								tr.appendChild($1.t('td', { style: css, textNode: L.daysToDue }));
								tr.appendChild($1.t('td', { style: css, textNode: L.dueDate }));
								tr.appendChild($1.t('td', L.respName));
							}
						}
					}
					var btnSend = $1.T.btnSend({
						value: 'Enviar Notificaciones', func: function () {
							ni = 0; GT.Not.sends = 0;
							sendMail();
						}
					});
					cont.appendChild(tb);
					cont.appendChild(btnSend);
					//
					var ni = 0; var totalSend = 0; var vs0 = false;
					function sendMail() {
						clearTimeout(GT.Not.timeout);
						if (ni == 0) {
							vs0 = $1.q('.__cardId', cont, 'all');
							totalSend = vs0.length;
						}
						vs = vs0[ni];
						var vsSend = (vs && vs.checked && vs.style.display != 'none');
						if (vsSend) {
							GT.Not.sends++;
							if (GT.Not.sends > 0 && GT.Not.sends % GT.Not.limitSend == 0) {
								$0s.console.put('Limit send (' + GT.Not.sends + '), waiting...');
								GT.Not.timeout = setTimeout(sendMailRest, 3000, vs);
							}
							else { sendMailRest(vs); }
						}
						else {
							ni++;
							if (vs0[ni]) { sendMail(); }
						}
					}
					function sendMailRest(vs) {
						var vPost = $1.G.inputs(vs.parentNode, jsF) + vs.Lsend;
						$ps_DB.get({
							f: 'PUT ' + GT.api.gtrib + 'Not.send', loaderFull: 1, inputs: vPost, func: function (Jq2) {
								if (Jq2.errNo) {
									Jq2.text = 'Se presentó un error, haga clic para continuar con el siguiente mensaje: ' + Jq2.text;
									Jq2.func = function () { ni++; sendMail(); }
									$1.Win.message(Jq2);
								}
								else {
									var vs2 = $1.q('.__cardLine_' + Jq2.cardId, null, 'all');//trs
									for (var i2 = 0; i2 < vs2.length; i2++) {
										vs2[i2].style.display = 'none';
										vs.style.display = 'none';
										//vs.classList.remove('__cardId');
									}
									ni++; sendMail();
								}
							}
						});
					}
				}
			}
		});
	}
}

var GT1 = {
	V: {
		open: function (P) {
			var wrap = $1.t('div');
			var h4 = $1.t('h4', { 'class': 'head1', textNode: P.respName });
			wrap.appendChild(h4);
			var ffLine = $1.T.ffLine({ ffLine: 1, t: 'Grupo:', w: 'ffxauto', v: P.grName });
			ffLine.appendChild($1.T.ffLine({ t: 'Año:', w: 'ffxauto', v: P.respYear }));
			wrap.appendChild(ffLine);
			var ffLine = $1.T.ffLine({ ffLine: 1, t: 'Venc.:', w: 'ffxauto', v: $2d.f(P.dueDate, 'mmm d') });
			ffLine.appendChild($1.T.ffLine({ t: 'Aplica para:', w: 'ffxauto', v: P.respTo }));
			wrap.appendChild(ffLine);
			wrap.appendChild($1.t('br'));
			P.cardId = (P.cardId) ? P.cardId * 1 : '';
			var tt = 'respCard';;
			var targetRef = (P.respId + '-' + P.cardId);
			var Lis = [
				{ textNode: ' Estado', 'class': 'fa fa_prio_none', active: 'Y', winClass: 'winStatus' },
				{ textNode: 'Comentarios', 'class': 'fa fa_comment', winClass: 'win5c' },
				{
					textNode: 'Archivos', 'class': 'fa fa_fileUpd', winClass: 'win5f', func: function (T) {
						Attach.get(Wins.win5f.P);
					}
				}
			];
			var Wins = $1M.tabs(Lis, wrap, { w: { style: 'margin-top:0.5rem;' } });
			GT.Resp.W.status(P, Wins.winStatus);
			$5c.form({ tt: tt, tr: targetRef, getList: 'tt', wT: { 'class': 'win5c' } }, Wins.win5c);
			Attach.btnUp({ tt: tt, tr: targetRef, save: 'Y', fDraw: 'Y' }, Wins.win5f);
			wrap.appendChild($1.t('div', { style: 'margin-top:0.5rem; font-size:0.75rem;', textNode: 'Id: ' + P.respId + ', card:' + P.cardId }));
			var bk = $1.Win.open(wrap, { winTitle: 'Seguimiento Responsabilidad (v.2)', winSize: 'medium', onBody: 1 });
		}

	}
};

GT1.Resp = {
	View: {
		load: function () {
			var v = $1.q('#gtRespViewType'); v = (v) ? v : {};
			switch (v.value) {
				default: GT1.Resp.View.list(); break;
				case 'calendar': GT1.Resp.View.calendar(); break;
			}
		}
		,
		list: function () {
			var cont = $1.q('#_wrapContent');
			$ps_DB.get({
				file: 'GET ' + GT.api.gtrib + 'Pyme.list', inputs: $1.G.inputs($1.q('#_wrapForm'), 'jsFiltVars'), loade: cont, func: function (Jq) {
					if (Jq.errNo) { $ps_DB.response(cont, Jq); }
					else {
						var tb = $1.T.table(['', '#', 'Estado', 'Verif.', 'Vencimiento', 'Responsabilidad']);
						var tBody = $1.t('tbody'); tb.appendChild(tBody);
						var n = 1;
						var bg = 'backgroundColor:#DDD;';
						for (var l in Jq.L) {
							var L = Jq.L[l];
							var tr = $1.t('tr'); tBody.appendChild(tr);
							var respStatus = (L.respStatus) ? L.respStatus : 'Pendiente';
							var td = $1.t('td'); tr.appendChild(td);
							var btn = $1.t('button', { 'class': 'fa faBtn fa_doc', P: L });
							btn.onclick = function () { GT1.V.open(this.P); }
							td.appendChild(btn);
							tr.appendChild($1.t('td', n)); n++;
							tr.appendChild($1.t('td', respStatus));
							tr.appendChild($1.t('td', L.verif));
							tr.appendChild($1.t('td', $2d.f(L.dueDate, 'mmm d')));
							tr.appendChild($1.t('td', L.respName));
						}
						cont.appendChild(tb);
					}
				}
			});
		}
		,
		calendar: function () {
			var cont = $1.q('#_wrapContent');
			$ps_DB.get({
				file: 'GET ' + GT.api.gtrib + 'Pyme.month', inputs: $1.G.inputs($1.q('#_wrapForm'), 'jsFiltVars'), loade: cont, func: function (Jq) {
					if (Jq.errNo) { $ps_DB.response(cont, Jq); }
					else {
						var date1 = $1.q('#_goDate1').value;
						var date2 = $1.q('#_goDate2').value;
						var Days = $2d.Vs.Days;
						var tb = $1.t('table', { 'class': 'table_zh' });
						var tHead = $1.t('thead'); tb.appendChild(tHead);
						var tr0 = $1.t('tr'); tHead.appendChild(tr0);
						tr0.appendChild($1.t('td', { textNode: '#', title: 'No. Semana del Año' }));
						for (var dN in Days) {
							var k = Days[dN].k;
							tr0.appendChild($1.t('td', { 'textNode': Days[dN].t, style: 'width:10rem;', 'class': 'weekDay_' + k }));
						}
						var tBody = $1.t('tbody'); tb.appendChild(tBody);
						var Wd = $2d.Draw.Wd({ date1: date1, date2: date2 });
						var nii = 1;
						for (var w in Wd) {
							var tr = $1.t('tr'); tBody.appendChild(tr);
							var wT = (Wd[w].wy > 52) ? 1 : Wd[w].wy;
							tr.appendChild($1.t('td', { textNode: wT }));;
							for (var dN in Days) {
								var tD = Days[dN]; var k = tD.k;
								var Wd_w_k = (Wd[w][k]) ? Wd[w][k] : {};
								yzId = Wd_w_k.yzId;
								var text = (Wd_w_k.date) ? $2d.f(Wd_w_k.date, 'mmm d') : '';
								var td = $1.t('td', { 'class': 'yzId_' + yzId, style: 'width:16rem; height:10rem;' });
								td.appendChild($1.t('div', { textNode: text }));
								tr.appendChild(td);
							}
						}
						cont.appendChild(tb);
						for (var i in Jq.L) {
							var L = Jq.L[i];
							var tw = $1.q('.yzId_' + L.yzId, tb);
							if (tw && tw.tagName) {
								var btn = $1.t('button', { 'class': 'fa faBtn fa_doc', P: L });
								btn.onclick = function () { GT1.V.open(this.P); }
								var div = $1.t('div', { style: 'border:0.0625rem solid #CCC; margin-bottom:0.125rem;' });
								div.appendChild(btn);
								div.appendChild($1.t('b', { textNode: L.respName }));
								tw.appendChild(div);
							}
						}
					}
				}
			});
		}
	}
}


//$M//

$V.EmailNotifWorkers['userAssg'] = 'Responsable';
$V.EmailNotifWorkers['userAssg,repLeg,contador'] = 'Responsable, Representante y Contador';
$V.EmailNotifWorkers['userAssg,contador'] = 'Responsable y Contador';

_Fi['dsp.formVisit'] = function (wrap) {
	var tD = $2d.goRang({ rang: '2week' });
	var divL = $1.T.divL({ divLine: 1, wxn: 'wrapx8', L: 'Fecha Inicio', I: { tag: 'input', type: 'date', 'class': 'jsFiltVars', name: 'A.docDate(E_mayIgual)', value: tD.date1 } }, wrap);
	$1.T.divL({ wxn: 'wrapx8', L: 'Fecha Fin', I: { tag: 'input', type: 'date', 'class': 'jsFiltVars', id: '_goDate2', name: 'A.docDate(E_menIgual)', value: tD.date2 } }, divL);
	$1.T.divL({ wxn: 'wrapx8', L: 'Responsable', I: { tag: 'select', 'class': 'jsFiltVars', name: 'A.slpId', opts: $Tb.oslp } }, divL);
	var divL = $1.T.divL({ divLine: 1, wxn: 'wrapx4', L: 'Cliente', I: { tag: 'input', type: 'text', 'class': 'jsFiltVars', name: 'C.cardName(E_like3)' } }, wrap);
	$1.T.btnFa({ fa: 'faBtnCnt fa-search', textNode: 'Buscar', func: A03.Asesorias.get }, wrap);
}

_Fi['gtrib_not'] = function () {
	var wrap = $M.Ht.filt;
	var tD = $2d.goRang({ rang: '2week' });
	var divLine = $1.T.divL({ divLine: 1, wxn: 'wrapxauto _goDate1', L: { textNode: 'Fecha Inicio' }, I: { tag: 'input', type: 'date', 'class': 'jsFiltVars', id: '_goDate1', name: 'FIE[][RT1.dueDate(E_mayIgual)]', value: tD.date1 } });

	divLine.appendChild($1.T.divL({ wxn: 'wrapxauto _goDate2', L: { textNode: 'Fecha Fin' }, I: { tag: 'input', type: 'date', 'class': 'jsFiltVars', id: '_goDate2', name: 'FIE[][RT1.dueDate(E_menIgual)]', value: tD.date2 } }));
	var btnB = $1.t('button', { 'class': 'fa faBtn fa_arrowBack' });
	btnB.onclick = function () { upd('--'); }
	var btnN = $1.t('button', { 'class': 'fa faBtn fa_arrowNext' });
	btnN.onclick = function (act) { upd('++'); }
	var inte = '';
	function upd(act) {
		clearTimeout(inte);
		var date1 = $1.q('#_goDate1');
		var tD = $2d.goRang({ rang: '2week', movType: act, date1: date1.value });
		date1.value = tD.date1;
		$1.q('#_goDate2').value = tD.date2;
	}
	divLine.appendChild($1.T.divL({ wxn: 'wrapxauto', L: { textNode: 'Estado' }, I: { tag: 'select', sel: { 'class': 'jsFiltVars', name: 'FIE[][RT2.respStatus(E_igual)]' }, opts: A03.Vs.respStatus, noBlank: 1 } }));
	divLine.appendChild($1.T.divL({ wxn: 'wrapxauto', L: { textNode: 'Grupo' }, I: { tag: 'select', sel: { 'class': 'jsFiltVars', name: 'FIE[][RT0.grId(E_igual)]' }, opts: $Tb.GRT } }));
	wrap.appendChild(divLine);
	var divLine = $1.T.divL({ divLine: 1, wxn: 'wrapx3', subText: 'Configure los tipos de correo desde la ficha del socio', L: { textNode: 'Notificar a' }, I: { tag: 'select', sel: { 'class': 'jsFiltVars', name: 'emailNotifTo' }, opts: $V.EmailNotifWorkers } })
	divLine.appendChild($1.T.divL({ wxn: 'wrapx3', L: { textNode: 'Responsable' }, I: { tag: 'input', type: 'text', 'class': 'jsFiltVars', name: 'FIE[][C.userAssgName(E_like3)]', placeholder: 'Nombre del Responsable' } }));
	divLine.appendChild($1.T.divL({ wxn: 'wrapx3', L: { textNode: 'Nombre de Cliente' }, I: { tag: 'input', type: 'text', 'class': 'jsFiltVars', name: 'FIE[][C.cardName(E_like3)]', placeholder: 'Nombre del Cliente' } }));
	wrap.appendChild(divLine);
	var btnLoad = $1.T.btnSend({ textNode: ' Obtener Notificaciones para Enviar', func: function () { GT.Not.byCard({}); } });
	wrap.appendChild(btnLoad);
	var d1 = $1.q('._goDate1');
	d1.insertBefore(btnB, d1.childNodes[1]);
	var d1 = $1.q('._goDate2');
	d1.insertBefore(btnN, d1.childNodes[1]);
}
_Fi['gtrib_master'] = function () {
	var wrap = $M.Ht.filt;;
	var year = new Date().getFullYear();
	var divLine = $1.T.divL({ divLine: 1, wxn: 'wrapx8', L: { textNode: 'Año' }, I: { tag: 'input', type: 'number', inputmode: 'numeric', min: 2017, 'class': 'jsFiltVars', name: 'FIE[][R0.respYear(E_igual)]', value: year } });
	divLine.appendChild($1.T.divL({ wxn: 'wrapx8', L: { textNode: 'Grupo' }, I: { tag: 'select', sel: { 'class': 'jsFiltVars', name: 'FIE[][R0.grId(E_igual)]' }, opts: $Tb.GRT } }));
	divLine.appendChild($1.T.divL({ wxn: 'wrapx8', L: { textNode: 'Aplica' }, I: { tag: 'select', sel: { 'class': 'jsFiltVars', name: 'FIE[][R0.respTo(E_igual)]' }, opts: A03.Vs.respTo } }));
	divLine.appendChild($1.T.divL({ wxn: 'wrapx8', L: { textNode: 'Grupo Dig.' }, I: { tag: 'select', sel: { 'class': 'jsFiltVars', name: 'FIE[][R0.gdvCode(E_igual)]' }, opts: $Tb.GDV } }));
	wrap.appendChild(divLine);
	var btnSend = $1.T.btnSend({ textNode: 'Actualizar', func: A03.GT.Resp.get });
	wrap.appendChild(btnSend);

}
_Fi['gtrib_control'] = function () {
	var wrap = $M.Ht.filt;
	var tD = $2d.goRang({ rang: '2week' });
	var divLine = $1.T.divL({ divLine: 1, wxn: 'wrapx8', L: { textNode: 'Vista' }, I: { tag: 'select', sel: { id: 'gtRespViewType' }, opts: { viewDCP: 'Vista DCP', list: 'Listado', calendar: 'Calendario', byCard: 'Por Cliente', cardTb: 'Tabla Cliente' }, noBlank: 1 } });
	divLine.appendChild($1.T.divL({ wxn: 'wrapx8 _goDate1', L: { textNode: 'Fecha Inicio' }, I: { tag: 'input', type: 'date', 'class': 'jsFiltVars', id: '_goDate1', name: 'FIE[][RT1.dueDate(E_mayIgual)]', value: tD.date1 } }));
	divLine.appendChild($1.T.divL({ wxn: 'wrapx8 _goDate2', L: { textNode: 'Fecha Fin' }, I: { tag: 'input', type: 'date', 'class': 'jsFiltVars', id: '_goDate2', name: 'FIE[][RT1.dueDate(E_menIgual)]', value: tD.date2 } }));
	var btnB = $1.t('button', { 'class': 'fa faBtn fa_arrowBack' });
	btnB.onclick = function () { upd('--'); }
	var btnN = $1.t('button', { 'class': 'fa faBtn fa_arrowNext' });
	btnN.onclick = function (act) { upd('++'); }
	var inte = '';
	function upd(act) {
		clearTimeout(inte);
		var date1 = $1.q('#_goDate1');
		var tD = $2d.goRang({ rang: '2week', movType: act, date1: date1.value });
		date1.value = tD.date1;
		$1.q('#_goDate2').value = tD.date2;
		inte = setTimeout(function () { GT.Resp.View.load({}); }, 1000);
	}

	divLine.appendChild($1.T.divL({ wxn: 'wrapx8', L: { textNode: 'Estado' }, I: { tag: 'select', sel: { 'class': 'jsFiltVars', name: 'respStatus' }, opts: A03.Vs.respStatus, noBlank: 1 } }));
	divLine.appendChild($1.T.divL({ wxn: 'wrapx8', L: { textNode: 'Grupo' }, I: { tag: 'select', sel: { 'class': 'jsFiltVars', name: 'FIE[][RT0.grId(E_igual)]' }, opts: $Tb.GRT } }));
	wrap.appendChild(divLine);
	var divLine = $1.T.divL({ divLine: 1, wxn: 'wrapx3', L: { textNode: 'Nombre de Cliente' }, I: { tag: 'input', type: 'text', 'class': 'jsFiltVars', name: 'FIE[][C.cardName(E_like3)]' } });
	divLine.appendChild($1.T.divL({ wxn: 'wrapx3', L: { textNode: 'Responsable' }, I: { tag: 'select', sel: { 'class': 'jsFiltVars', name: 'FIE[][C.slpId(E_igual)]' }, opts: $Tb.oslp } }));
	var se = $o.Se.form({ jsFields: 'jsFiltVars', opts: 'bussPartnerGroups', putDefine: { id: 'groupId' } });
	//var divSe = $1.T.divL({ wxn: 'wrapx3', L: { textNode: 'Grupo Empresarial' }, Inode: se });
	//divLine.appendChild(divSe);
	wrap.appendChild(divLine);
	var d1 = $1.q('._goDate1');
	d1.insertBefore(btnB, d1.childNodes[1]);
	var d1 = $1.q('._goDate2');
	d1.insertBefore(btnN, d1.childNodes[1]);
	var btn = $1.T.btnSend({ textNode: 'Actualizar', func: GT.Resp.View.load });
	wrap.appendChild(btn);
}
_Fi['bp_docAux_gtrib'] = function () {
	var wrap = $M.Ht.filt;
	var tD = $2d.goRang({ rang: 'month' });
	var divLine = $1.T.divL({ divLine: 1, wxn: 'wrapx2', L: { textNode: 'Nombre de Cliente' }, I: { tag: 'input', type: 'text', 'class': 'jsFiltVars', name: 'FIE[0][A.cardName(E_like3)]' } });
	divLine.appendChild($1.T.divL({ wxn: 'wrapx2', L: { textNode: 'Responsable' }, I: { tag: 'input', type: 'text', 'class': 'jsFiltVars', name: 'FIE[1][A.userAssgName(E_like3)]' } }));
	var btn = $1.T.btnSend({ textNode: 'Obtener Formatos', func: A03.Auxi.formAssgResp });

	wrap.appendChild(divLine);
	wrap.appendChild(btn);
}
_Fi['fquP.audB'] = function (wrap) {
	var jsV = 'jsFiltVars';
	var divL = $1.T.divL({ divLine: 1, wxn: 'wrapx4', L: 'Nombre Cliente', I: { tag: 'input', type: 'text', name: 'C.cardName(E_like3)', 'class': jsV } }, wrap);
	$1.T.divL({ wxn: 'wrapx8', L: 'Año', I: { tag: 'input', type: 'number', name: 'FR.docYear(E_igual)', 'class': jsV } }, divL);
	$1.T.divL({ wxn: 'wrapx8', L: 'Fecha Inicio', subText: 'Creación', I: { tag: 'input', type: 'date', name: 'FR.dateC(E_mayIgual)(T_time)', 'class': jsV } }, divL);
	$1.T.divL({ wxn: 'wrapx8', L: 'Fecha Corte', I: { tag: 'input', type: 'date', name: 'FR.dateC(E_menIgual)(T_time)', 'class': jsV } }, divL);
	var btnSend = $1.T.btnSend({ textNode: 'Actualizar', func: A03.Aud.get });
	wrap.appendChild(btnSend);
};
_Fi['dcp.cnc'] = function (wrap) {
	var jsV = 'jsFiltVars';
	var divL = $1.T.divL({ divLine: 1, wxn: 'wrapx8', L: 'Fecha', subText: 'creado', I: { tag: 'input', type: 'date', name: 'A.dateC(E_mayIgual)', 'class': jsV } }, wrap);
	$1.T.divL({ wxn: 'wrapx8', L: 'Fecha Final', I: { tag: 'input', type: 'date', name: 'A.dateC(E_menIgual)', 'class': jsV } }, divL);
	$1.T.divL({ wxn: 'wrapx4', L: { textNode: 'Responsable' }, I: { tag: 'select', 'class': jsV, name: 'A.slpId', opts: $Tb.oslp } }, divL)
	$1.T.divL({ wxn: 'wrapx4', L: 'Nombre Cliente', I: { tag: 'input', type: 'text', name: 'C.cardName(E_like3)', 'class': jsV } }, divL);

	var btnSend = $1.T.btnSend({ textNode: 'Actualizar', func: $_DCP.CNC.get });
	wrap.appendChild(btnSend);
};

//$M.s.bussP.L.docAux = {t:'Documentos Auxiliares', L:{aux:{k:'bussPartner.docAux.gtrib'}}}

$M.li['gtrib.master'] = {
	t: 'Maestro de Responsabilidades', kau: 'gtribMaster', func: function () {
		var btnLoad = $1.T.btnNew({ textNode: 'Nueva Responsabilidad', func: A03.GT.Resp.put });
		$M.Ht.ini({ fieldset: true, f: _Fi.gtrib_master, btnNew: btnLoad, gyp: A03.GT.Resp.get });
	}
};

$M.li['gtrib.notif'] = {
	t: 'Gestor Notificiaciones Resp.', kau: 'gtribNotif', func: function () {
		$M.Ht.ini({ fieldset: true, func_filt: _Fi.gtrib_not, func_cont: null });
	}
};

$M.li['gtrib.control'] = {
	t: 'Control de Responsabilidades', kau: 'gtribControl', func: function () {
		$M.Ht.ini({ fieldset: true, f: _Fi.gtrib_control });
	}
};

$M.li['forms.visita'] = {
	t: 'Visitas / Asesorias', kau: 'formsVisita', func: function () {
		var mediumCont = $1.t('p');
		var btn = $1.T.btnFa({ fa: 'fa-docs', textNode: 'Nuevo', func: A03.Asesorias.put });
		$M.Ht.ini({ fieldset: 'Y', btnNew: btn, func_filt: 'dsp.formVisit', func_pageAndCont: A03.Asesorias.get });
	}
};
$M.li['forms.visitaView'] = {
	noTitle: 'Y', kau: 'formsVisita', func: function () {
		$M.Ht.ini({ func_cont: A03.Asesorias.view });
	}
};

$M.li['rt.assg'] = {
	t: 'Asignar Responsabilidades del año', kau: 'rtAssg', func: function () { $M.Ht.ini({ func_cont: A03.RT.assg }); }
}
$M.li['dcp.fichaCard'] = {
	t: 'Ficha de Cliente', kau: 'dcpFicha', func: function () { $M.Ht.ini({ func_cont: A03.Fic.form }); }
}

$M.li['bussPartner.docAux.gtrib'] = {
	t: 'Formato Físico Asignación Responsabilidades', func: function () {
		$M.Ht.ini({ fieldset: true, func_filt: _Fi.bp_docAux_gtrib, func_cont: null });
	}
};

$M.li['fquP.audB'] = {
	t: 'Auditoria Base', kau: 'fquP.audB', func: function () {
		var btn = $1.T.btnFa({ faBtn: 'fa_doc', textNode: 'Nuevo Documento', func: function () { $M.to('fquP.audB.form'); } });
		$M.Ht.ini({ btnNew: btn, fieldset: 'Y', func_filt: 'fquP.audB', func_pageAndCont: A03.Aud.get });
	}
};
$M.li['fquP.audB.form'] = {
	t: 'Auditoria Base', kau: 'fquP.audB', func: function () {
		$M.Ht.ini({ func_cont: A03.Aud.form });
	}
}

$M.li['dcp.geDocGCal'] = {
	t: 'Docs. Calidad', kau: 'geDoc.card', func: function () {
		$M.Ht.ini({
			func_cont: function () {
				GeDoc.wrapTemp();
				var Pa = $M.read();
				var cont = $1.q('.' + GeDoc.Cls.wrapFiles);
				var vPost = 'oTy=gCal';
				$Api.get({
					f: Api.GeDoc.a + 'oty', inputs: vPost, loade: cont, func: function (Jr) {
						if (Jr.errNo) { $Api.resp(cont, Jr); }
						else {
							$1.t('h5', { 'class': 'fa fa_folder', textNode: Jr.folName }, cont);
							var tb = $1.T.table(['Tipo', 'Nombre Archivo', 'Tamaño', 'Versión', 'Creado']);
							var tBody = $1.t('tbody', { 'class': 'geDoc_tableFiles' }, tb); cont.appendChild(tb);
							if (Jr.L && !Jr.L.errNo) { GeDoc.trLine(tBody, Jr.L, { view: 'free' }); }
							var upd = $1.t('div', { id: 'fileUpd' }, cont);
							if (GeDoc.perms('W', Jr)) {
								Attach.btnUp({
									func: function (Jr3) {
										if (!Jr3.errNo) {
											GeDoc.fileUp({ folId: Jr.folId, tt: 'gCal', tr: 1, L: Jr3 }, function (Jrr, o) {
												GeDoc.trLine(tBody, o, { view: 'free' });
											});
										}
									}
								}, upd);
							}
						}
					}
				});
			}
		});
	},

}

$M.li['dcp.cnc'] = {
	t: 'Correspondencia', kau: 'dcp.cnc', func: function () {
		var btn = $1.T.btnFa({ faBtn: 'fa_doc', textNode: 'Nuevo Documento', func: function () { $M.to('dcp.cnc.form'); } });
		$M.Ht.ini({ func_filt: 'dcp.cnc', btnNew: btn, func_pageAndCont: $_DCP.CNC.get });
	}
};
$M.li['dcp.cnc.form'] = {
	t: 'Correspondencia', kau: 'dcp.cnc', func: function () { $M.Ht.ini({ func_cont: $_DCP.CNC.form }); }

}

/* 2020 */
$M.liAdd('DcpLi', [
	{ _lineText: 'Permisos Personal' },
	{
		k: 'dcpFormsPpe', t: 'Permisos Personal', kau: 'dcpFormsPpe', func: function () {
			var btnA = $1.T.btnFa({ fa: 'faBtnCt fa_doc', textNode: 'Nuevo', func: function () { $M.to('dcpFormsPpe.form'); } });
			$M.Ht.ini({ fieldset: 'Y', func_filt: 'dcpFormsPpe', btnNew: btnA, func_pageAndCont: $_DCP.Ppe.get });
		}
	},
	{
		k: 'dcpFormsPpe.form', t: 'Permisos Personal (Form)', kau: 'dcpFormsPpe', func: function () {
			$M.Ht.ini({ func_cont: $_DCP.Ppe.form });
		}
	},
	{
		k: 'dcpFormsPpe.view', noTitle: 'Y', t: 'Permisos Personal (Doc)', kau: 'dcpFormsPpe', func: function () {
			$M.Ht.ini({ func_cont: $_DCP.Ppe.view });
		}
	}
]);

$_DCP = {};
$_DCP.CNC = {
	get: function () {
		var cont = $M.Ht.cont;
		$Api.get({
			f: Api._dcp + 'cnc', inputs: $1.G.filter(), loade: cont, func: function (Jr) {
				if (Jr.errNo) { return $Api.resp(cont, Jr); }
				var tb = $1.T.table(['', 'No', 'Fecha', 'Cliente', 'Responsable', 'Referencia Informe', 'Descripción']);
				var tBody = $1.t('tbody', 0, tb);
				for (var i in Jr.L) {
					var L = Jr.L[i];
					var tr = $1.t('tr', 0, tBody);
					var td = $1.t('td', 0, tr);
					$1.T.btnFa({ fa: 'fa-pencil', title: 'Modificar', P: L, func: function (P) { $Doc.to('dcp.cnc', '.form', P.P); } }, td);
					Attach.openWin({ tt: 'dcpCnc', tr: L.docEntry, title: 'Doc. Correspondencia: ' + L.docEntry }, td);
					$1.t('td', { textNode: L.docEntry }, tr);
					$1.t('td', { textNode: L.docDate }, tr);
					$1.t('td', { textNode: L.cardName }, tr);
					$1.t('td', { textNode: _g(L.slpId, $Tb.oslp) }, tr);
					$1.t('td', { textNode: L.refInf }, tr);
					$1.t('td', { textNode: L.lineMemo }, tr);
				}
				tb = $1.T.tbExport(tb, { fileName: 'Control Numeracion Correspondencia' });
				cont.appendChild(tb);
			}
		});
	},
	form: function (Pa) {
		var Pa = $M.read();
		var Pa = (Pa) ? Pa : {};
		var cont = $M.Ht.cont; var jsF = $Api.JS.cls;
		$Api.get({
			f: Api._dcp + 'cnc/form', inputs: 'docEntry=' + Pa.docEntry, loadVerif: !Pa.docEntry, loade: cont, func: function (Jr) {
				var Ds = {
					cont: cont, docEntry: Pa.docEntry, serieOpts: 'N', jsF: jsF,
					tbLines: null, POST: Api._dcp + 'cnc', func: function (Jr2) {
						$Api.resp(cont, Jr2);
					},
					Li: [
						{ fType: 'lTag', tag: 'crd', wxn: 'wrapx4', L: 'Cliente', req: 'Y', I: { 'class': jsF, fie: 'slpId', topPare: cont, D: Jr, value: Jr.cardName } },
						{ req: 'Y', wxn: 'wrapx8', L: 'Fecha', I: { tag: 'input', type: 'date', 'class': jsF, name: 'docDate', value: Jr.docDate } },
						{ fType: 'lTag', tag: 'slp', wxn: 'wrapx8', req: 'Y', L: 'Resp. Ventas', I: { 'class': jsF + ' ' + $Api.Sea.clsBox, k: 'slpId', name: 'slpId', selected: Jr.slpId } },
						{ divLine: 1, wxn: 'wrapx1', L: 'Referencia Informe', I: { tag: 'input', type: 'text', 'class': jsF, name: 'refInf', value: Jr.refInf } },
						{ divLine: 1, wxn: 'wrapx1', L: 'Detalles', I: { tag: 'textarea', 'class': jsF, name: 'lineMemo', textNode: Jr.lineMemo } }
					]
				};
				if (Pa.docEntry) {
					Ds.PUT = Ds.POST; delete (Ds.POST);
				}
				$Doc.form(Ds);
			}
		});
	}
}

$V.dcpPppStatus = [{ k: 'S', v: 'Pendiente' }, { k: 'O', v: 'Autorizado' }, { k: 'C', v: 'No Autorizado' }, { k: 'N', v: 'Anulado' }];
$_DCP.Ppe = {
	OLg: function (L) {
		var Li = []; var n = 0;
		Li.push({ ico: 'fa fa-eye', textNode: ' Visualizar', P: L, func: function (T) { $Doc.go('dcpFormsPpe', 'v', T.P, 1); } });
		if (L.docStatus == 'S') {
			Li.push({ k: 'edit', ico: 'fa fa-pencil', textNode: ' Modificar Documento', P: L, func: function (T) { $Doc.go('dcpFormsPpe', 'f', T.P, 1); } });
		}
		if (L.canceled == 'N') {
			Li.push({ k: 'statusZ', ico: 'fa fa_prio_low', textNode: ' Cambiar Estado', P: L, func: function (T) { $Doc.statusDefine({ docEntry: T.P.docEntry, Opts: $V.dcpPppStatus, reqMemo: 'N', api: Api._dcp + 'ppe/status', text: 'Se va a cambiar el estado del permiso.' }); } });
			Li.push({ k: 'statusC', ico: 'fa fa_prio_high', textNode: ' Anular', P: L, func: function (T) { $Doc.statusDefine({ docEntry: T.P.docEntry, reqMemo: 'N', api: Api._dcp + 'ppe/statusCancel', text: 'Se va a anular el permiso.' }); } });
		}
		return $Opts.add('gfiDcc', Li, L);;
	},
	opts: function (P, pare) {
		Li = { Li: $_DCP.Ppe.OLg(P.L), PB: P.L, textNode: P.textNode };
		var mnu = $1.Menu.winLiRel(Li);
		if (pare) { pare.appendChild(mnu); }
		return mnu;
	},
	get: function () {
		var cont = $M.Ht.cont;
		$Doc.tbList({
			api: Api._dcp + 'ppe', inputs: $1.G.filter(),
			fOpts: $_DCP.Ppe.opts, view: 'Y', docBy: 'userDate',
			tbSerie: 'dcpFormsPpe',
			TD: [
				{ H: 'Estado', k: 'docStatus', _V: 'dcpPppStatus' },
				{ H: 'Responsable', k: 'slpId', _gTb: 'oslp' },
				{ H: 'Fecha', k: 'docDate' },
				{ H: 'Hora Inicio', k: 'hIni' },
				{ H: 'Hora Fin', k: 'hEnd' },
				{ H: 'Duración', k: 'timeLong' },
				{ H: 'Motivo', k: 'lineMemo' }
			],
			tbExport: { ext: 'xlsx', fileName: 'Permisos' }
		}, cont);
	},
	form: function () {
		var cont = $M.Ht.cont; var Pa = $M.read(); var D = {};
		$Api.get({
			f: Api._dcp + 'ppe/form', inputs: 'docEntry=' + Pa.docEntry, loadVerif: !Pa.docEntry, loade: cont, func: function (Jr) {
				if (Jr) { D = Jr; }
				$Doc.form({
					tbSerie: 'dcpFormsPpe', cont: cont, docEdit: Pa.docEntry, POST: Api._dcp + 'ppe',
					tbH: {
						kTb: 'N',
						L: [
							{ L: 'Responsable', wxn: 'wrapx8', lTag: 'select', I: { name: 'slpId', opts: $Tb.oslp, selected: D.slpId } },
							{ L: 'Fecha', wxn: 'wrapx8', lTag: 'date', I: { name: 'docDate', value: D.docDate } },
							{ L: 'Hora Inicio', wxn: 'wrapx8', lTag: 'input', I: { name: 'hIni', value: D.hIni } },
							{ L: 'Hora Fin', wxn: 'wrapx8', lTag: 'input', I: { name: 'hEnd', value: D.hEnd } },
							{ L: 'Motivo Permiso', wxn: 'wrapx4', lTag: 'input', I: { name: 'lineMemo', value: D.lineMemo } }
						]
					}
				});
			}
		});
	},
	view: function () {
		var cont = $M.Ht.cont; var Pa = $M.read(); var jsF = $Api.JS.cls;
		$Api.get({
			f: Api._dcp + 'ppe/view', inputs: 'docEntry=' + Pa.docEntry, loade: cont, func: function (Jr) {
				var tP = {
					tbSerie: 'dcpFormsPpe', D: Jr,
					btnsTop: { ks: 'print,edit,statusZ,statusN,', icons: 'Y', Li: $_DCP.Ppe.OLg },
					THs: [
						{ docEntry: 'Y' }, { docTitle: 'Permiso Personal', cs: 5, ln: 1 }, { t: 'Estado', k: 'docStatus', _V: 'dcpPppStatus', ln: 1 },
						{ t: 'Fecha', k: 'docDate' }, { middleInfo: 'Y' }, { logo: 'Y' },
						{ t: 'Desde', k: 'hIni' }, { t: 'Hasta', k: 'hEnd' },
						{ t: 'Duración', k: 'timeLong' },
						{ t: 'Motivo', k: 'lineMemo', cs: 6, ln: 1 },
					]
				};
				$Doc.view(cont, tP);
			}
		});
	},
}