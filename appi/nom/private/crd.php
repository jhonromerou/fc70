<?php
JRoute::get('nom/crd',function($D){
	$D['from']='C.cardId,C.cardName,C.workSede,C.workArea,C.workPosi FROM nom_ocrd C';
	return a_sql::rPaging($D);
},[]);
JRoute::get('nom/crd/form',function($D){
	if(_js::ise($D['cardId'],'Se debe definir Id del empleado','numeric>0')){}
	else{
		$M=a_sql::fetch('SELECT C.*,C.licTradType,C.licTradNum,CC.cellular,CC.phone1,CC.phone2,CC.email,
		AD.addrId,AD.county,AD.city,AD.address,P.proName
		FROM nom_ocrd C
	LEFT JOIN par_ocrd CC ON (CC.cardId=C.cardId)
	LEFT JOIN par_opro P ON(P.proId=C.proId)
	LEFT JOIN app_addr AD ON (AD.tt=\'card\' AND AD.tr=C.cardId AND AD.lineNum=1)
	WHERE C.cardId=\''.$D['cardId'].'\' LIMIT 1',[1=>'Error obteniendo información del empleado',2=>'El empleado no existe']);
	if(a_sql::$err){ _err::err(a_sql::$errNoText); }
	else{ return _js::enc2($M); }
	}
	if(_err::$err){ return _err::$errText; }
},[]);
JRoute::put('nom/crd',function($D){
	if(_js::iseErr($D['name'],'Se debe definir nombre del empleado')){}
	else if(_js::iseErr($D['surName'],'Se debe definir apellido del empleado')){}
	else if(_js::iseErr($D['licTradType'],'El tipo de documento debe estar definido.')){}
	else if(_js::iseErr($D['licTradNum'],'El número de documento debe estar definido.')){}
	else if(_js::iseErr($D['workSede'],'Debe definir la sede','numeric>0')){}
	else if(_js::iseErr($D['workArea'],'Debe definir el área de trabajo','numeric>0')){}
	else if(_js::iseErr($D['workPosi'],'Debe definir el cargo del empleado','numeric>0')){}
	else{
		unset($D['bCode']);
		$xS=['licTradType','licTradNum','phone1','cellular','phone2','email'];
		$Ad=$D['Ad']; unset($D['Ad']);
		$Dc=array('cardName'=>$D['name'].' '.$D['surName']);
		foreach($xS as $k){ $Dc[$k]=$D[$k]; }
		$D['cardName']=$Dc['cardName'];
		if($D['cardId']>0){/*tercero existe */
			a_sql::uniRow($Dc,['tbk'=>'par_ocrd','wh_change'=>'cardId=\''.$D['cardId'].'\' LIMIT 1']);
			if(a_sql::$err){ _err::err('Error actualizando ficha de tercero (top). '.a_sql::$errText,3); }
			else{
				a_sql::qUpdate($D,['tbk'=>'nom_ocrd','wh_change'=>'cardId=\''.$D['cardId'].'\' LIMIT 1']);
				if(a_sql::$err){ _err::err('Error actualizando ficha de tercero. '.a_sql::$errText,3); }
			}
		}
		else{
			$D['bCode']=_js::numAlet(10,'1Z');
			$Dc['cardId']=a_sql::qInsert($D,['tbk'=>'nom_ocrd']);
			$Dc['cardType']='E';
			if(a_sql::$err){ _err::err('Error definiendo ficha de tercero. '.a_sql::$errText,3); }
			else if(1){
				a_sql::uniRow($Dc,['tbk'=>'par_ocrd','wh_change'=>'cardId=\''.$D['cardId'].'\' LIMIT 1']);
				if(a_sql::$err){ _err::err('Error definiendo ficha de tercero (top). '.a_sql::$errText,3); }
			}
		}
		if(!_err::$err){
			$Ad[0]['lineNum']=1;
			a_sql::uniRow($Ad[0],['tbk'=>'app_addr','wh_change'=>'addrId=\''.$Ad[0]['addrId'].'\' LIMIT 1']);
			if(a_sql::$err){ _err::err('Error definiendo dirección de tercero (top). '.a_sql::$errText,3); }
		}
	}
	if(_err::$err){ return _err::$errText; }
	else{ return _js::r('Información de empleado guardada correctamente',$D); }
},[]);

JRoute::get('nom/crd/sea','seaInp',[]);
?>