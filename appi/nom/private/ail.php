<?php
JRoute::get('nom/ail',function($D){
	$Px=fOCP::fg('owsu',['in'=>'C.workSede'],'nomBase');
	$D['from']='A.docEntry,A.docDate,A.docStatus,A.canceled,A.docType,A.docClass,C.cardId, C.cardName,A.userId,A.dateC,A.userUpd,A.dateUpd
	FROM nom_oail A 
	JOIN nom_ocrd C ON (C.cardId=A.cardId)
	'.$Px['l'];
	return a_sql::rPaging($D);
},[]);
JRoute::get('nom/ail/form',function($D){
	_ADMS::lib('iDoc');
	$D['fromA']='A.*, C.cardName 
		FROM nom_oail A 
		LEFT JOIN nom_ocrd C ON (C.cardId=A.cardId)';
	$D['fromB']='B.* FROM nom_ail1 B';
	return iDoc::getOne($D);
},[]);
JRoute::get('nom/ail/view',function($D){
	_ADMS::lib('iDoc');
	$D['fromA']='A.*,C.licTradType,C.licTradNum,CC.cardName 
		FROM nom_oail A 
		LEFT JOIN nom_ocrd CC ON (CC.cardId=A.cardId)
		LEFT JOIN par_ocrd C ON (C.cardId=CC.cardId)';
	$D['fromB']='B.* FROM nom_ail1 B';
	return iDoc::getOne($D);
},[]);
JRoute::post('nom/ail',function($D){
	$js=false;
	if(_js::iseErr($D['docDate'],'Se debe definir la fecha')){}
	else if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){}
	else if(_js::iseErr($D['docType'],'Se debe definir el tipo','numeric>0')){}
	else if(_js::iseErr($D['prp1'],'Se debe definir la Sede','numeric>0')){ }
	else{
		$Lx=$D['L']; unset($D['L']);
		$hayL=!_js::isArray($Lx);
		$D['docStatus']='C'; $faltan=0;
		if($hayL){ foreach($Lx as $n=>$L){
			$L[0]='i'; $L[1]='nom_ail1'; $L['_unik']='id';
			$Lx[$n]=$L;
			if($L['lineDue']=='' || $L['delete']=='Y'){ $faltan++; }
		}}
		else{ $faltan++; }//no hay nada, debe definirse algo
		if($faltan>0){ $D['docStatus']='O'; }
		a_sql::transaction(); $cmt=false;
		$D['docEntry']=a_sql::qInsert($D,['tbk'=>'nom_oail','qk'=>'ud','qku'=>'ud']);
		if(a_sql::$err){ _err::err('Error generando documento: '.a_sql::$errText,3); }
		if(!_err::$err && $hayL){
			a_sql::multiQuery($Lx,0,['docEntry'=>$D['docEntry']]);
		}
		if(!_err::$err){ $cmt=true;
			$js=_js::r('Información actualizada','"docEntry":"'.$D['docEntry'].'"');
		}
		a_sql::transaction($cmt);
	}
	_err::errDie();
	return $js;
},[]);
JRoute::put('nom/ail',function($D){
	$js=false;
	if(_js::iseErr($D['docEntry'],'Se debe definir ID a modificar','numeric>0')){}
	else if(_js::iseErr($D['docDate'],'Se debe definir la fecha')){}
	else if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){}
	else if(_js::iseErr($D['docType'],'Se debe definir el tipo','numeric>0')){}
	else if(_js::iseErr($D['prp1'],'Se debe definir la Sede','numeric>0')){ }
	else{
		$Lx=$D['L']; unset($D['L']);
		$hayL=!_js::isArray($Lx);
		$D['docStatus']='C'; $faltan=0;
		if($hayL){ foreach($Lx as $n=>$L){
			$L[0]='i'; $L[1]='nom_ail1'; $L['_unik']='id';
			$Lx[$n]=$L;
			if($L['lineDue']=='' || $L['delete']=='Y'){ $faltan++; }
		}}
		else{ $faltan++; }//no hay nada, debe definirse algo
		if($faltan>0){ $D['docStatus']='O'; }
		a_sql::transaction(); $cmt=false;
		a_sql::qUpdate($D,['tbk'=>'nom_oail','qku'=>'ud','wh_change'=>'docEntry=\''.$D['docEntry'].'\' LIMIT 1']);
		if(a_sql::$err){ _err::err('Error actualizando documento: '.a_sql::$errText,3); }
		if(!_err::$err && $hayL){
			a_sql::multiQuery($Lx,0,['docEntry'=>$D['docEntry']]);
		}
		if(!_err::$err){ $cmt=true;
			$js=_js::r('Información actualizada','"docEntry":"'.$D['docEntry'].'"');
		}
		a_sql::transaction($cmt);
	}
	_err::errDie();
	return $js;
},[]);

JRoute::put('nom/ail/statusCancel',function($D){
	_ADMS::lib('iDoc');
	$D=['docEntry'=>$D['docEntry'],'tbk'=>'nom_oail','t'=>'N'];
	return iDoc::putStatus($D);
},[]);
?>