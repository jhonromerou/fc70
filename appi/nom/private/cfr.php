<?php
JRoute::get('nom/cfr/emp',function($D){
	$D['from']='C.cardId,C.cardName,
	FF.fId,F.docEntry,F.docDate,
	FF2.fId fId2,F2.docEntry docEntry2,F2.docDate docDate2
	FROM nom_ocrd C
	LEFT JOIN nom_ocfr FF ON (FF.fId=1)
	LEFT JOIN nom_ocfrr F ON (F.fId=FF.fId AND F.cardId=C.cardId)
	LEFT JOIN nom_ocfr FF2 ON (FF2.fId=2)
	LEFT JOIN nom_ocfrr F2 ON (F2.fId=FF2.fId AND F2.cardId=C.cardId)
	';
	//$D['wh']='FF.grMdl=\'cfr1\'';
	$M['L']=a_sql::rPaging($D,true);
	return _js::enc2($M);
},[]);

/* respuestas */
JRoute::get('nom/cfr/ans',function($D){
	$M=a_sql::fetch('SELECT F.fId,C.cardId,F.fCode,F.fName,F.version,F.descrip,A.docDate,A.docEntry,C.cardName,A.lineMemo
	FROM nom_ocrd C
	JOIN nom_ocfr F ON (F.fId=\''.$D['fId'].'\')
	LEFT JOIN nom_ocfrr A ON (A.fId=F.fId AND A.cardId=C.cardId)
	WHERE C.cardId=\''.$D['cardId'].'\' AND F.fId=\''.$D['fId'].'\' LIMIT 1',[1=>'Error obtiendo datos del formulario.',2=>'El formulario base no existe.']);
	if(a_sql::$err){ return a_sql::$errNoText; }
	$M['L']=a_sql::fetchL('SELECT F1.*,B.aText 
	FROM nom_cfr1 F1 
	LEFT JOIN nom_cfr1r B ON (B.aid=F1.aid AND B.docEntry=\''.$M['docEntry'].'\')
	WHERE F1.fId=\''.$M['fId'].'\' ORDER BY F1.lineNum ASC',[1=>'Error obteniendo preguntas del formulario',2=>'El formulario no tiene preguntas registradas']);
	return _js::enc2($M);
},[]);
JRoute::post('nom/cfr/ans',function($D){
	$D['docDate']=date('Y-m-d');
	if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){ $js=_err::$errText; }
	else{
		a_sql::transaction(); $c=false;
		_ADMS::slib('JForm/JForm');
		$D=JForm::r_post($D,['tbk'=>'nom_ocfr','tbk1'=>'nom_cfr1']);
		if(_err::$err){ $js=_err::$errText; }
		else{ $c=true; $js=_js::r('Información actualizada correctamente.',$D);}
		a_sql::transaction($c);
	}
	return $js;
},[]);
JRoute::put('nom/cfr/ans',function($D){
	$D['docDate']=date('Y-m-d');
	if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){ $js=_err::$errText; }
	else{
		a_sql::transaction(); $c=false;
		_ADMS::slib('JForm/JForm');
		$D=JForm::r_put($D,['tbk'=>'nom_ocfr','tbk1'=>'nom_cfr1']);
		if(_err::$err){ $js=_err::$errText; }
		else{ $c=true; $js=_js::r('Información actualizada correctamente.',$D);}
		a_sql::transaction($c);
	}
	return $js;
},[]);

/* Plantillas */
JRoute::get('nom/cfr/forms',function($D){
	_ADMS::slib('JForm/JForm');
	return JForm::t_get($D,['tbk'=>'nom_ocfr']);
},[]);
JRoute::get('nom/cfr/form',function($D){
		_ADMS::slib('JForm/JForm');
	return JForm::t_getOne($D,['tbk'=>'nom_ocfr','tbk1'=>'nom_cfr1']);
},[]);
JRoute::post('nom/cfr/form',function($D){
		_ADMS::slib('JForm/JForm');
	$js=JForm::t_post($D,['tbk'=>'nom_ocfr','tbk1'=>'nom_cfr1']);
	return $js;
},[]);
JRoute::put('nom/cfr/form',function($D){
		_ADMS::slib('JForm/JForm');
	$js=JForm::t_put($D,['tbk'=>'nom_ocfr','tbk1'=>'nom_cfr1']);
	return $js;
},[]);

JRoute::get('nom/cfr/rep',function($D){
	$M=a_sql::fetch('SELECT F.fId,F.fCode,F.fName,F.version,F.descrip
	FROM nom_ocfr F 
	WHERE F.fId=\''.$D['fId'].'\' LIMIT 1',[1=>'Error obtiendo datos del formulario.',2=>'El formulario base no existe.']);
	if(a_sql::$err){ return a_sql::$errNoText; }
	$M['aL']=a_sql::fetchL('SELECT F1.aid,F1.lineNum,F1.lineType,F1.lOpts,F1.lineText FROM nom_cfr1 F1 
	WHERE F1.fId=\''.$D['fId'].'\' ORDER BY F1.lineNum ASC',[1=>'Error obteniendo preguntas del formulario',2=>'El formulario no tiene preguntas registradas']);
	if(a_sql::$err){ return a_sql::$errNoText; }
	$M['L']=a_sql::fetchL('SELECT A.docEntry,A.fId,B.aid,A.docDate,C.cardName,B.aText 
	FROM nom_cfr1r B
	JOIN nom_ocfrr A ON (A.docEntry=B.docEntry)
	LEFT JOIN nom_ocrd C ON (C.cardId=A.cardId)
	WHERE A.fId=\''.$D['fId'].'\' ',[1=>'Error obteniendo respuesta del formulario',2=>'El formulario no tiene respuestas registradas']);
	return _js::enc2($M);
},[]);

?>