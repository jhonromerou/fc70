<?php
class nomNovInc{
static public function get($D){
	_ADMS::libC('nom','nov');
	$D['lineType']='INC';
	return nomNov::get($D);
}

static public function form($D){
	$whB='N1.cardId=\''.$D['cardId'].'\' AND N1.periodo=\''.$D['periodo'].'\' AND N1.lineType=\'INC\'';
	$q=a_sql::query('SELECT N1.id,C.cardId,C.cardName,N1.periodo,N1.lineType2,N1.lineDate,N1.lineDue,N1.lineMemo
	FROM nom_nov2 N1
	LEFT JOIN nom_ocrd C ON (C.cardId=N1.cardId)
	WHERE '.$whB.' ',[1=>'Error obteniendo registros de incapacidades para el periodo',2=>'El empleado no tiene registro de incapacidades para el periodo']);
	if(a_sql::$err){ _err::err(a_sql::$errNoText); }
	else{
		$Mx=array('L'=>[]);
		while($L=$q->fetch_assoc()){
			$Mx['L'][]=$L;
		}
		return _js::enc2($Mx);
	}
	if(_err::$err){ return _err::$errText; }
}

static public function put($D,$met='PUT'){
	$js=false;
	if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){}
	else if(_js::iseErr($D['periodo'],'Se debe definir un periodo','numeric>0')){}
	else if(!is_array($D['L']) || count($D['L'])==0){ _err::err('No se enviaron lineas a definir.',3); }
	else{
		_ADMS::lib('_2d');
		_ADMS::libC('nom','nov,crd');
		a_sql::transaction(); $cmt=false;
		$xE=nomCrd::getData($D['cardId'],'C.salarioBal');
		if(!_err::$err){
			$whA='cardId=\''.$D['cardId'].'\' AND lineType=\'INC\' AND periodo=\''.$D['periodo'].'\'';
			$HER=0; /* totales */
			/* Revisar campos y otras novedades en el dia */
			$n=0;
			foreach($D['L'] as $nx=>$L){ $n++; 
				$ln='Linea '.$n.': ';
				if(_js::iseErr($L['lineType2'],$ln.'Se debe definir el tipo de incapacidad.')){ $errs=1; break; }
				else if(_js::iseErr($L['lineDate'],$ln.'Se debe definir el inicio de la incapacidad')){ $errs=1; break; }
				else if(_js::iseErr($L['lineDue'],$ln.'Se debe definir el fin de la incapacidad')){ $errs=1; break; }
				else if($L['lineDue']<$L['lineDate']){ _err::err($ln.'La fecha de inicio no puede ser menor a la de final.',3); $errs=1; break; }
				else if($e=_js::textMax($L['lineMemo'],100)){ _err::err($e); $errs=1; break; }
				else if($L['delete']=='Y'){ continue; }
				else{
					nomNov::verificarPer(['cardId'=>$D['cardId'],'id'=>$L['id'],'lineDate'=>$L['lineDate'],'lineDue'=>$L['lineDue']]);
					if(_err::$err){ $errs=1; break; }
					else{
						$D['L'][$nx]['quantity']=$L['quantity']=_2d::getDiff($L['lineDate'],$L['lineDue'])+1;
						$HER+=$L['quantity'];
					}
				}
			}
		}
		/* Almacenar datos */
		if($errs==0){ $n=1;
			$qI=array('cardId'=>$D['cardId'],'lineType'=>'INC','periodo'=>$D['periodo']);
			foreach($D['L'] as $xx=>$L){
				$qI['lineNum']=$n;
				$qI['lineDate']=$L['lineDate'];
				$qI['lineDue']=$L['lineDue'];
				$qI['lineType2']=$L['lineType2'];
				$qI['quantity']=$L['quantity'];
				$qI['salarioBal']=$xE['salarioBal'];
				$qI['lineMemo']=$L['lineMemo'];
				$qI['quantity2']=0;
				if($n==1){ /* dias y txt en linea 1 */
					$qI['quantity2']=$HER;
				}
				$n++; unset($qI['id']);
				if($L['id']>0){ $qI['id']=$L['id']; $qI['delete']=$L['delete']; }
				a_sql::uniRow($qI,
				['tbk'=>'nom_nov2','wh_change'=>'id=\''.$L['id'].'\' LIMIT 1']);
				if(a_sql::$err){ _err::err(a_sql::$errText,3); $errs++; break; }
			}
		}
		/* Actualizar Periodo */
		if($errs==0){
			nomNov::setPeriodo($qI,'INC');
			if(_err::$err){ $errs=1; }
		}
		if($errs==0){ $cmt=true;
			$js=_js::r('Novedades registradas',['cardId'=>$D['cardId'],'periodo'=>$D['periodo']]);
		}
		a_sql::transaction($cmt);
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
}
}
?>