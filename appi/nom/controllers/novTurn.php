<?php
class nomNovTurn{
/*Calcular horas y recargos */
	static public function horaFormat($h=0){
		if($h<0){ return 0; }
		if($h>0){ return round($h,2); }
		return $h;
	}
	static public function HDCalc($D,$R=array()){
		$R['_info']='';
		$R['openTurn']=$D['openTurn'];
		$R['closeTurn']=$D['closeTurn'];
		$hEnt=substr($D['timeEnt'],11,2)*1; //2020-01-03 22:30
		$hEntM=substr($D['timeEnt'],14,2)*1; //2020-01-03 (11-2)22:(14-2)30
		$hSal=substr($D['timeOut'],11,2)*1;
		$hSalM=substr($D['timeOut'],14,2)*1;
		//$hSal=substr($D['timeOut'],0,2)*1;; $hSalM=substr($D['timeOut'],2,2)*1;
		$minEnt=($hEntM>0)?(60-$hEntM)/60:0; //10:30, dan -0.5 en hora
		$minEnt2=0; //23:45, da 0.75
		$mismoDay=(substr($D['timeEnt'],0,10)==substr($D['timeOut'],0,10));
		if($minEnt>0) $minEnt2=1-$minEnt; //23:45, da 0.75
		$minSal=$hSalM/60;
		$hrMas= $minEnt+$minSal;
		$hNoc=21; $hNoc2=30; //entre 9pm y 6am
		$hSalNum=($hSal<=12)?$hSal+24:$hSal;// 4am es 28
		$hEntNum=($hEnt<=12)?$hEnt:$hEnt;// 4am es 28
		if($hEnt>$hNoc){ $hAlas12=24-$hEnt; }// 21:30, son 24-21=3 -0.5minEnt, 2.5h
		else{ $hAlas12=24-$hNoc; } // 22-24-(24-21)= -2-3=-7
		if($hAlas12<0){ $hAlas12=0; }
		$hAlTN=$hNoc-$hEnt; //3h de 6pm a 9pm
		if($hAlTN<0){ $hAlTN=0; }
		$diur=0; //8am=32, 2h son diurnas
		if($hSalNum>=$hNoc2){// 7:30, (31.5-30)=1.5h
			$diur=$hSalNum-$hNoc2+$minSal;
		}
		$hFinTC=6;
		//Turno
		$hrsTurn=_2d::getDiff($D['openTurn'],$D['closeTurn'],'h');
		$hFinTurn= $hEntNum+$minEnt2+$hrsTurn;
		$dx=_2d::getDiff($D['timeEnt'],$D['timeOut'],'h:i');
		if(is_numeric($dx[1])){ $dx[1]=round($dx[1],4); }
		if($hrsTurn){ $hrsTurn=round($hrsTurn,4); }
		$diffTurn=$R['hrsExtra']=$dx[1]-$hrsTurn;
		$R['hrsTurn']=$hrsTurn;
		$R['timeEnt']=$D['timeEnt'];
		$R['timeOut']=$D['timeOut'];
		$R['hextra']=$D['hextra'];
		$R['dominical']=$D['dominical'];
		$R['_info']='turno de '.$hEnt.'.'.$hEntM.' a '.$hFinTurn.'.'.$hEntM.' de '.$hrsTurn.'h, Salida: '.$hSalNum.'.'.$hSalM.', extras: '.$diffTurn.' >6am: '.$diur;
		$R['hrs']=$dx[0];
		$R['hrsNum']=$dx[1];
		if($hrsTurn>$R['hrsNum']){ $hrsTurn=$R['hrsNum']; }
		$ED=$EN=$EFN=$EFD=0;
		$RN=$FL=$FLN=0;
		$hayEX=($R['hrsNum']>$R['hrsTurn']);
		{/* 2. Horas rango */
		$HO=0; $HO2=0;
		$ND1=$ED1=$EN1=$RN1=0;
		$ND2=$ED2=$EN2=$RN2=0;
		$extraD1=($hNoc-$hFinTurn); //21-20, 1h ED1
		if($mismoDay){//haciendo extras en D1 >9am
			if($hEntNum<7){ /*entro a 12am, RN1 */
				if($hEntNum<6){ $RN1=6-$hEntNum-$minEnt2; }
				if($hFinTurn<6){ $EN1= 6-$hFinTurn; } //6-5.30 sali = 0.5h
			}
			if($hayEX){
				if($hSal>=$hNoc){ //sali despues de las 9, 21.5
					$ED1=$hNoc-$hFinTurn; //21 sali 8.25, 0.75
				}
				else{ $ED1=$hSal-$hFinTurn+$minSal; }
			}//9.5 - 6 = 3.5h
			/* horas en la noche */
			if($hSal>=$hNoc){
				$RN1 += $hSalNum-$hNoc+$minSal; // sali 22:15 - 22, 1.25h
				if($hSalNum>=$hFinTurn){
					$EN1 = ($hFinTurn>$hNoc)?$hSalNum-$hFinTurn+$minSal:$hSalNum-$hNoc+$minSal;
				}
				$RN1 -=$EN1;
			}
		}
		else{
			if($hEnt<7){ /*entro a 12am */
				if($hEnt<6){ $RN1=6-$hEntNum-$minEnt2; }
				if($hFinTurn<$hFinTC){ $EN1= $hFinTurn; $RN1 -=$EN1; } //sali 5.5 hice 5.5h
			}
			if($hSalNum>$hFinTurn){
				$ED1 = ($hNoc-$hFinTurn+$minEnt2);
			}
			
			if($ED1<0){ $ED1=0; } if($EN1<0){ $EN1=0; }
			if($hEntNum>=$hNoc){
				$RN1=24-$hEntNum-$minEnt2;
			}
			else if($hSalNum>=$hNoc){//sali desp 9pm
				if($hSalNum>=24){ $RN1=24-$hNoc; }
				else { $RN1=$hSalNum-$hNoc+$minSal; }
				if($hSalNum>=$hFinTurn){
					$en1=($hSalNum>=24)?24-$hFinTurn:$hSalNum-$hNoc+$minSal;
					if($en1>0){ $EN1+=$en1; $RN1 -=$en1; }
				}
			}
			//D
			if($hSalNum>=30){//sali despues de las 7
				$RN2=$hFinTC;; // 23:45	08:00	11:45
				if($hSalNum>$hFinTurn){ //terminaba a las 4.30, a las 6, 1.5
					if($hFinTurn<=30){ $EN2=30-$hFinTurn; }
					$RN2 -=$EN2;
				}
				if($hFinTurn>=30){ $ED2=$hSalNum-$hFinTurn+$minSal; }
				else{ $ED2=$hSal-$hFinTC+$minSal; }
			}
			else if($hSalNum>=24){//entre las 00 y las 6am
				// 29.5-24 = 5.5h rc
				if($hFinTurn>$hSalNum){ $RN2=$hSalNum-24+$minSal; }
				else{ $RN2=$hFinTurn-24;  }
				if($hSalNum>=$hFinTurn){ //sale 29.5, turno 28.5, 1 extras
					$EN2=$hSalNum-24+$minSal;
					$RN2 -=$EN2;
				}
			} //00 a 5.30
		}
		$ED1=self::horaFormat($ED1);
		$RN1=self::horaFormat($RN1);
		$EN1=self::horaFormat($EN1);
		$RN2=self::horaFormat($RN2);
		$EN2=self::horaFormat($EN2);
		$ED2=self::horaFormat($ED2);
		$HO=self::horaFormat($R['hrsNum']-$ED1-$EN1-$RN1-$RN2-$EN2-$ED2);
		$HO1=($mismoDay)?$HO:0;
		$HO2=($mismoDay)?0:$HO;
		//$R['txt'] .= '--> '." HO($HO), ED1($ED1), EN1($EN1), RN1($RN1), RN2($RN2), EN2($EN2), ED2($ED2)";
		}/* end 2.rango */
		{// calculo
		$extra=($D['hextra']=='Y');
		if($D['dominical']=='FH'){/* domingo, lunes normal */
			if($extra){
				if(($ED1>0 || $HO>0) && $hFinTurn<24){ $R['EFD']=$ED1+$HO; $HO2=0; }
				else if($ED1>0){ $R['EFD']=$ED1; }
				if($ED2>0 || $HO2>0){ $R['ED']=$ED2+$HO2; }
				if($RN1>0 || $EN1>0){ $R['EFN']=$RN1+$EN1; }
				if($EN2>0 || $RN2>0){ $R['EN']=$RN2+$EN2; }
			}else{
				if($HO1>0){ $R['FL']=$HO1; }
				if($HO>0 && $hEntNum<24){ $R['FL']=$HO; }
				if($ED1>0){ $R['EFD']=$ED1; }
				if($EN1>0){ $R['EFN']=$EN1; }
				if($RN1>0){ $R['FLN']=$RN1; }
				if($EN2>0){ $R['EN']=$EN2; }
				if($RN2>0){ $R['RN']=$RN2; }
				if($ED2>0){ $R['ED']=$ED2; }
			}
		}
		else if($D['dominical']=='HF'){/* sabado a domingo */
			if($extra){
				if($ED1>0 || $HO>0){ $R['ED']=$ED1+$HO; }
				if($ED2>0 || $HO1>0){ $R['EFD']=$ED2+$HO1; }
				if($RN1>0 || $EN1>0){ $R['EN']=$RN1+$EN1; }
				if($EN2>0 || $RN2>0){ $R['EFN']=$RN2+$EN2; }
			}else{
				if($HO1>0 && $hSalNum<24){ $R['FL']=$HO1; }
				if($ED1>0){ $R['ED']=$ED1; }
				if($EN1>0){ $R['EN']=$EN1; }
				if($RN1>0){ $R['RN']=$RN1; }
				if($EN2>0){ $R['EFN']=$EN2; }
				if($RN2>0){ $R['FLN']=$RN2; }
				if($ED2>0){ $R['EFD']=$ED2; }
			}
		}
		else if($D['dominical']=='FF'){/* domingo y lunes festivo */
			if($extra){
				if($HO>0 || $ED1>0 || $ED2>0){ $R['EFD']=$HO+$ED1+$ED2; }
				if($EN1>0 || $RN1>0 || $RN2>0 || $EN2>0){ $R['EFN']=$EN1+$RN1+$RN2+$EN2; }
			}else{
				if($HO>0){ $R['FL']=$HO; }
				if($ED1>0 || $ED2>0){ $R['EFD']=$ED1+$ED2; }
				if($EN1>0 || $EN2){ $R['EFN']=$EN1+$EN2; }
				if($RN1>0 || $RN2>0){ $R['FLN']=$RN1+$RN2; }
			}
		}
		else if($extra){
			if($ED1>0 || $ED2>0 || $HO>0){ $R['ED']=$HO+$ED1+$ED2; }
			if($EN1>0 || $EN2>0 || $RN1>0 || $RN2>0){ $R['EN']=$EN1+$EN2+$RN1+$RN2; }
		}
		else{
			if($ED1>0 || $ED2>0){ $R['ED']=$ED1+$ED2; }
			if($EN1>0 || $EN2){ $R['EN']=$EN1+$EN2; }
			if($RN1>0 || $RN2>0){ $R['RN']=$RN1+$RN2; }
		}
		}//end calculo
		//echo substr($R['timeEnt'],-5).' Fin '.substr($R['closeTurn'],-5).' a '.substr($R['timeOut'],-5)." HO($HO), ED1($ED1), EN1($EN1), RN1($RN1), RN2($RN2), EN2($EN2), ED2($ED2)<hr/>";
		unset($R['_info'],$R['txt']);
		return $R;
	}
	static public function setData($D,$P=array()){
		$nextDay=date('Y-m-d',strtotime($D['openDate'])+86400);
		if($D['openTurn']>$D['closeTurn']){//entro 22, salgo 07sale al otro dia
			$D['closeTurn']= $nextDay.' '.$D['closeTurn'];
		}
		else{ $D['closeTurn']=$D['openDate'].' '.$D['closeTurn']; }
		$D['openTurn']=$D['openDate'].' '.$D['openTurn'];
		if(($D['timeEnt'])>($D['timeOut'])){//sale al otro dia
			$D['timeOut']= $nextDay.' '.$D['timeOut'];
		}
		else{ $D['timeOut']=$D['openDate'].' '.$D['timeOut']; }
		$D['timeEnt']=$D['openDate'].' '.$D['timeEnt'];
		return $D;
	}
	static public function getBC($D,$P=array()){ //revisar, lo pase a nomBC.php
		if(_js::iseErr($D['lineCode'],'Se debe definir el código para buscar')){}
		else{
			$fie=($P['fie'])?','.$P['fie']:'';
			$wh=($P['wh'])?'AND '.$P['wh']:'';
			$whL=($P['whL'])?'AND '.$P['whL']:'';
			a_sql::transaction(); $cmt=false;
			$q=a_sql::fetch('SELECT C.cardId,C.cardName,N2.lineStatus,N2.openDate,N2.timeEnt'.$fie.'
			FROM nom_ocrd C
			LEFT JOIN nom_nov3 N2 ON (N2.cardId=C.cardId AND N2.lineStatus=\'O\' '.$whL.')
			WHERE C.bCode=\''.$D['lineCode'].'\' '.$wh.' LIMIT 2',[1=>'Error consultando información de empleado',2=>'El código {'.$D['lineCode'].'} no fue encontrado']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else{
				$q['_numRows']=a_sql::$numRows;
			}
			return $q;
		}
	}
	static public function getOne($D){
		$Px=fOCP::fg('owsu',['in'=>'C.workSede'],'nomBase');
		$qa=a_sql::fetch('SELECT N3.tid,C.cardId,C.cardName,N3.openDate,N3.timeEnt,N3.timeOut,N3.openTurn,N3.closeTurn,N3.dominical,N3.hextra
		FROM nom_nov3 N3
		JOIN nom_ocrd C ON (C.cardId=N3.cardId)
		'.$Px['l'].'
		WHERE N3.tid=\''.$D['tid'].'\' LIMIT 1',array(1=>'Error obteniendo registro de turno.',2=>'No se encontro el registro.'));
		if(a_sql::$err){ return a_sql::$errNoText; }
		else{ return _js::enc2($qa); }
	}
	static public function get($D){
		$Px=fOCP::fg('owsu',['in'=>'C.workSede'],'nomBase');
		if($D['salidaPend']){
			$D['wh']='N2.timeOut IS NULL';
		} unset($D['salidaPend']);
		$D['from']='N2.tid,N2.lineStatus,N2.tt,C.cardId,C.cardName,N2.timeEnt,N2.timeOut'.$Px['f'].'
		FROM nom_ocrd C
		JOIN nom_nov3 N2 ON (N2.cardId=C.cardId)
		'.$Px['l'];
		return a_sql::rPaging($D);
	}
	static public function open($D){
		$openDate=date('Y-m-d'); $js=false;
		if(_js::iseErr($D['openTurn'],'Se debe definir la hora inicial del turno. Formato hh:mm (24 horas)')){}
		else if(_js::iseErr($D['closeTurn'],'Se debe definir la hora final del turno. Formato hh:mm (24 horas)')){}
		else if(_js::iseErr($D['lineCode'],'Se debe definir el código para buscar')){}
		else{
			a_sql::transaction(); $cmt=false;
			$q=self::getBC($D,['whL'=>'N2.openDate=\''.$openDate.'\'']);
			if(_err::$err){ return _err::$errText; }
			else if($q['openDate'] && $q['lineStatus']=='O'){
				_err::err($q['cardName'].': tiene una entrada abierta '.$q['timeEnt'],3);
			}
			else{
				$timeEnt=date('H:i');
				$Di=['tt'=>'B','cardId'=>$q['cardId'],'openDate'=>$openDate,
				'openTurn'=>$D['openTurn'],'closeTurn'=>$D['closeTurn'],
				'timeEnt'=>$timeEnt,'dominical'=>$D['dominical'],'hextra'=>$D['hextra']];
				$Di=self::setData($Di);
				unset($Di['timeOut']);
				//die(print_r($Di));
				$tid=a_sql::qInsert($Di,
				['tbk'=>'nom_nov3']);
				if(a_sql::$err){ _err::err(a_sql::$errNoText); }
				else{
					$q['tid']=$tid;
					$q['timeEnt']=$timeEnt;
					$js=_js::r('Turno abierto correctamente',$q);
					$cmt=true;
				}
			}
			a_sql::transaction($cmt);
		}
		if(_err::$err){ return _err::$errText; }
		return $js;
	}
	static public function close($D){
		$js=false;
		a_sql::transaction(); $cmt=false;
		$q=self::getBC($D,['fie'=>'N2.cardId,N2.tid,N2.timeOut,N2.dominical,N2.hextra,N2.openDate,N2.openTurn,N2.closeTurn,N2.dominical,N2.hextra','whL'=>'N2.lineStatus=\'O\' ']);
		if(_err::$err){ return _err::$errText; }
		else if($q['lineStatus']==''){ _err::err($q['cardName'].' no tiene turno abierto.',3); }
		else if($q['_numRows']>1){ _err::err('No se puede cerrar porque se encontró más de 1 turno abierto, esto genera cálculos errados. Revise los turnos.',3); }
		else{
			_ADMS::lib('_2d');
			$q['timeOut']=date('Y-m-d H:i');
			$Di=array('tt'=>'B','cardId'=>$q['cardId'],'openDate'=>$q['openDate'],
				'openTurn'=>$q['openTurn'],'closeTurn'=>$q['closeTurn'],'dominical'=>$q['dominical'],'hextra'=>$q['hextra'],
				'timeEnt'=>substr($q['timeEnt'],11,5),
				'timeOut'=>substr($q['timeOut'],11,5),
				);
			$Di=self::setData($Di);
			$Di=self::HDCalc($Di,$Di);
			//echo 'ALERTJSON'; print_r($Di); die();
			$x=a_sql::qUpdate($Di,
				['tbk'=>'nom_nov3','wh_change'=>'tid=\''.$q['tid'].'\' LIMIT 1']);
			if(a_sql::$err){ _err::err(a_sql::$errText,3); }
			if(!_err::$err){
				self::setHRE($Di);
			}
			if(!_err::$err){
				$js=_js::r('Turno cerrado correctamente');
				$cmt=true;
			}
		}
		a_sql::transaction($cmt);
		if(_err::$err){ return _err::$errText; }
		return $js;
	}
	
	static public function post($D){
		$js=false;
		if(0){}
		else{ $js=self::save($D); }
		_err::errDie();
		return $js;
	}
	static public function put($D){
		$js=false;
		if(_js::iseErr($D['tid'],'Se debe definir ID del turno a modificar','numeric>0')){}
		else{ $js=self::save($D); }
		_err::errDie();
		return $js;
	}
	static public function save($D){
		if(_js::iseErr($D['cardId'],'Se debe definir el tercero','numeric>0')){}
		else if(_js::iseErr($D['openDate'],'Se debe definir fecha del turno')){}
		else if(_js::iseErr($D['timeEnt'],'Se debe definir la hora de entrada.')){}
		else if(_js::iseErr($D['timeOut'],'Se debe definir hora de salida.')){}
		else if(_js::iseErr($D['openTurn'],'Se debe definir la hora inicial del turno.')){}
		else if(_js::iseErr($D['closeTurn'],'Se debe definir hora final del turno.')){}
		else{
			$D['tt']='M'; //ya es manual
			a_sql::transaction(); $cmt=false;
			{
				_ADMS::lib('_2d');
				$Di=array('cardId'=>$D['cardId'],'openDate'=>$D['openDate'],
				'openTurn'=>$D['openTurn'],'closeTurn'=>$D['closeTurn'],
				'timeEnt'=>$D['timeEnt'],'timeOut'=>$D['timeOut'],'dominical'=>$D['dominical'],'hextra'=>$D['hextra']
				);
				$Di=self::setData($Di);
				$Di=self::HDCalc($Di,$Di);
				if($Di['hrsTurn']>18){ $errs=1;
					_err::err('La jornada excede las 18 horas. Revise los datos.',3);
				}
				//die(print_r($Di));
				if(!_err::$err && $D['preview']=='Y'){
					$Di['preview']='Y';
					a_sql::transaction(true); return _js::enc2($Di);
				}
				//echo 'ALERTJSON'; print_r($Di); die();
				if(!_err::$err){ self::setHRE($Di); }
				if(!_err::$err){
					a_sql::uniRow($Di,
						['tbk'=>'nom_nov3','qku'=>'d','wh_change'=>'tid=\''.$D['tid'].'\' LIMIT 1']);
					if(a_sql::$err){ _err::err(a_sql::$errText,3); }
					else{
						$js=_js::r('Turno cerrado correctamente');
						$cmt=true;
					}
				}
			}
			a_sql::transaction($cmt);
		}
		if(_err::$err){ return _err::$errText; }
		return $js;
	}

	static public function cancel($D){
		$js=false;
		if(_js::iseErr($D['tid'],'Se debe definir ID del turno a eliminar')){}
		else{
			a_sql::transaction(); $c=false;
			$qt=a_sql::fetch('SELECT cardId,openDate,lineStatus FROM nom_nov3 WHERE tid=\''.$D['tid'].'\' LIMIT 1',[1=>'Error obteniendo información de turno a eliminar',2=>'El registro no existe.']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else{
				a_sql::query('DELETE FROM nom_nov3 WHERE tid=\''.$D['tid'].'\' LIMIT 1',[1=>'Error eliminando turno']);
				if(a_sql::$err){ _err::err(a_sql::$errNoText); }
				else{
					a_sql::query('DELETE FROM nom_nov2 WHERE cardId=\''.$qt['cardId'].'\' AND lineDate=\''.$qt['openDate'].'\' AND lineType=\'HER\' LIMIT 50',[1=>'Error eliminando novedades relacionadas al turno']);
					if(a_sql::$err){ _err::err(a_sql::$errNoText); }
					else{ $c=true;
						$js=_js::r('Turno eliminado correctamente');
					}
				}
			}
			a_sql::transaction($c);
		}
		if(_err::$err){ return _err::$errText; }
		return $js;
	}
	/* Definir para crear nov2 de horas */
	static public function setHRE($D=array()){
		if(!_err::$err){
			$errs=0;//FL	RN	FLN	EFN	EN	EFD	ED
			$X_HE=array('RN'=>1.35,'FL'=>1.75,'FLN'=>2.1,
			'ED'=>1.25,'EN'=>1.75,'EFD'=>2,'EFN'=>2.5);
			$HER=0; /* totales */
			$qI=array('cardId'=>$D['cardId'],'lineDate'=>$D['openDate'],'lineDue'=>$D['openDate'],'lineType'=>'HER','periodo'=>$D['periodo']);
			$nl=1;
			$whB='cardId=\''.$D['cardId'].'\' AND lineDate=\''.$D['openDate'].'\' AND lineType=\'HER\'';
			foreach($X_HE as $k=>$fx){
				$qI['lineType2']=$k;
				$qI['numFactor']=$fx;
				$qI['quantity']=($D[$k]);
				unset($qI['delete']);
				if(!$D[$k] || $qI['quantity']=='' || $qI['quantity']<=0){
					$qI['delete']='Y';
				}
				else{ $qI['lineNum']=$nl; $nl++; }
				a_sql::uniRow($qI,
				['tbk'=>'nom_nov2','wh_change'=>$whB.' AND lineType2=\''.$k.'\' LIMIT 1']);
				if(a_sql::$err){ _err::err(a_sql::$errText,3); $errs++; break; }
			}
		}
	}
}
?>