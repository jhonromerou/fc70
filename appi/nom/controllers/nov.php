<?php
class nomNov{
static public function get($D,$P=array()){
	_ADMS::lib('_2d,sql/filter');
	$vType=($D['viewType']); 
	$wh=''; $fie='';
	if($vType=='1'){ $wh='AND N.lineNum=\'1\' ';}
	else{
		$fie=',N.lineDate,N.lineDue,N.quantity';
	}
	if($D['lineType']){ $wh .='AND N.lineType=\''.$D['lineType'].'\' '; }
	if($D['date1']!='' && $D['date2']!=''){ $wh .='AND '._2d::rangFields(['lineDate',$D['date1'],'lineDue',$D['date2']]).' '; }
	else if($D['date1']!=''){ $wh .='AND N.lineDate>=\''.$D['date1'].'\' '; }
	else if($D['date2']!=''){ $wh .='AND N.lineDue<=\''.$D['date2'].'\' '; }
	unset($D['viewType'],$D['date1'],$D['date2']);
	$wh .= a_sql_filtByT($D);
	return a_sql::pagingRows('SELECT N.id,C.cardId,C.cardName,N.periodo,N.quantity2'.$fie.'
	FROM nom_nov2 N
	LEFT JOIN nom_nov3 N3 ON (N3.cardId=N.cardId AND N3.openDate=N.lineDate)
	LEFT JOIN nom_ocrd C ON (C.cardId=N.cardId)
	WHERE 1 '.$wh.' ',
	[1=>'Error obteniendo registros de novedades',2=>'No se encontraron resultados']);
}
static public function verificarPer($D){
	$wf=_2d::rangFields(['A.lineDate',$D['lineDate'],'A.lineDue',$D['lineDue']]);
	$wh=($D['wh'])?'AND '.$D['wh']:'';
	//usar para lineType!=HER
	$qf=a_sql::fetch('SELECT A.id,A.lineType,A.lineDate,A.lineDue FROM nom_nov2 A
	WHERE A.cardId=\''.$D['cardId'].'\' AND ('.$wf.') '.$wh.'
	LIMIT 1
	',[1=>'Error verificando periodos para la novedad']);
	$aText=($D['aText'])?$D['aText']:'No se puede registrar la novedad.';
	if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
	else if(a_sql::$errNo==-1 && $qf['id']!=$D['id']){
		return _err::err($ln.'El empleado tiene registrada una novedad que inicia {'.$qf['lineDate'].'} y finalizada {'.$qf['lineDue'].'} de tipo {'.$qf['lineType'].'}: '.$aText,3);
	}
	else{ return false; }
}
static public function setPeriodo($P,$type=false){
	//HER,AUS,VAC,LIC,INC
	$D=['cardId'=>$P['cardId'],'periodo'=>$P['periodo']];
	$qD=a_sql::fetch('SELECT SUM(quantity) quantity FROM nom_nov2 WHERE cardId=\''.$D['cardId'].'\' AND periodo=\''.$D['periodo'].'\' AND lineType=\''.$type.'\' ',[1=>'Error calculando HER del periodo.']);
	if(a_sql::$err){ _err::err(a_sql::$errNoText); }
	else if(a_sql::$errNo==-1){ $D[$type]=$qD['quantity']; }
	if($type && !_err::$err){
		a_sql::uniRow($D,
		['tbk'=>'nom_nov1','wh_change'=>'cardId=\''.$D['cardId'].'\' AND periodo=\''.$D['periodo'].'\' LIMIT 1']);
		if(a_sql::$err){ _err::err(a_sql::$errText,3); }
	}
}
}
?>