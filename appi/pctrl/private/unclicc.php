<?php
a_sql::dbase(c::$Sql2);
JRoute::get('pctrl/unclicc',function($D){
	$Mx=['F'=>[],'M'=>[],'VD'=>[],'V'=>[]];
	$q=a_sql::query('SELECT K.k, K.kDesc
	FROM apla_okv K
	WHERE 1',[1=>'Error obteniendo datos']);
	if(a_sql::$err){ $Mx['V']=json_decode(a_sql::$errNoText,1); }
	else if(a_sql::$errNo==-1){ while($L=$q->fetch_assoc()){
			$Mx['V'][$L['k']]=$L;
		}
	}
	// Fijas
	$q=a_sql::query('SELECT A.k,A.maxUse,A.vUsaged,M.mdlCnf
	FROM ocard C
	JOIN loadMdl M ON (M.ocardId=C.ocardId)
	JOIN apla_opct A ON (A.ocardId=C.ocardId)
	LEFT JOIN apla_okv K ON (K.k=A.k)
	WHERE C.ocardCode=\''.a_ses::$ocardcode.'\' AND K.kType=\'F\' ',[1=>'Error obteniendo datos']);
	$n=0;
	if(a_sql::$err){ $Mx['F']=json_decode(a_sql::$errNoText,1); }
	else if(a_sql::$errNo==-1){ while($L=$q->fetch_assoc()){
			if($n==0){ $Mx['VD']=json_decode($L['mdlCnf'],1); $n=1; }
			unset($L['mdlCnf']);
			$Mx['F'][]=$L;
		}
	}
	// Mensuales
	$q=a_sql::query('SELECT A.k,A.maxUse,B.usaged
	FROM ocard C
	JOIN apla_opct A ON (A.ocardId=C.ocardId)
	LEFT JOIN apla_pct1 B ON (B.ocardId=C.ocardId and B.k=A.k AND B.periodo=\''.date('Y-m').'\')
	LEFT JOIN apla_okv K ON (K.k=A.k)
	WHERE C.ocardCode=\''.a_ses::$ocardcode.'\' AND K.kType=\'M\' ',[1=>'Error obteniendo consumo mensual']);
	if(a_sql::$err){ $Mx['M']=json_decode(a_sql::$errNoText,1); }
	else if(a_sql::$errNo==-1){ while($L=$q->fetch_assoc()){
			$Mx['M'][]=$L;
		}
	}
	return _js::enc2($Mx);
});

?>