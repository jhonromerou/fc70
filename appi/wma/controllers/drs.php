<?php
class wmaDrs extends JxDoc{
	static $serie='wmaDrs';
	static $tbk99='wma3_doc99';
	static $tbk='wma_odrs';
	static $tbk1='wma_drs1'; //materiales-costos
	static public function get($D=[]){
		$D['from']='A.docEntry,A.pdocEntry,A.serieId,A.docNum,A.dateC,A.userId,A.docDate,A.docStatus,A.wfaId,A.userUpd,A.dateUpd,A.lineMemo FROM '.self::$tbk.' A
		';
    return a_sql::rPaging($D,false);
  }
	static public function fieldsRequire(){
		return [
			['k'=>'docDate','ty'=>'date','iMsg'=>'Fecha'],
			['k'=>'wfaId','ty'=>'id','iMsg'=>'Fase'],
			['k'=>'cardId','ty'=>'id','iMsg'=>'Tercero'],
			['k'=>'whsId','ty'=>'id','iMsg'=>'Almacen proceso'],
			['k'=>'lineMemo','maxLen'=>200],
			['ty'=>'L','k'=>'L','iMsg'=>'Lineas documento','L'=>[
				['k'=>'itemId','ty'=>'id','iMsg'=>'Articulo'],
				['k'=>'itemSzId','ty'=>'id','iMsg'=>'Articulo (2)'],
				['k'=>'whsId','ty'=>'id','iMsg'=>'Almacen Origen'],
				['k'=>'quantity','ty'=>'>0','iMsg'=>'Cantidad'],
			]
			]
		];
	}
	static public function post($D=array()){
		self::formRequire($D);
		//self::nextID();
		a_sql::transaction(); $c=false;
		if(!_err::$err){ //registrar componentes y secundarios
			_ADMS::lib('JLog');
			_ADMS::libC('wma','ivtPep');
			$ivtPep=new ivtPep(['hands'=>true,'tt'=>self::$serie,'tr'=>$docEntry,'docDate'=>$D['docDate']]);
			$ivtPep->Livt[0]=[]; //Doc
			$Lx=$D['L']; unset($D['L'],$D['LS']);
			$D['whsId']=6;
			if(!_err::$err) foreach($Lx as $n=>$L){ //registrar entregas
				$L['wfaId']=$L['wfaIdBef'];
				//Registrar salida fase ant
				$cost=0;
				if($L['wfaId']>0){
					$ivtPep->getInfo($L); if(_err::$err){ break; }
					$ivtPep->handRevi(['outQty'=>$L['quantity']]); if(_err::$err){ break; }
					$ivtPep->handSett(['outQty'=>$L['quantity']]);
					$cost=$ivtPep->La['cost'];
				}
				$L['openQty']=$L['quantity'];
				$L[0]='i'; $L[1]=self::$tbk1; $L['docEntry']=$docEntry;
				$L['_err1']='Error registrando linea de documento';
				//$ivtPep->Livt[]=$L;
				//registrar ingreso al tercero
				$L['wfaId']=$D['wfaId']; $L['whsId']=$D['whsId'];
				$ivtPep->getInfo($L); if(_err::$err){ break; }
				$ivtPep->handRevi(['inQty'=>$L['quantity']]); if(_err::$err){ break; }
				$nD=['inQty'=>$L['quantity']];
				if($cost){ $nD['cost']=$cost; } //usar costo con el que sale
				$ivtPep->handSett($nD);
			}
		}
		if(!_err::$err){
			$D[0]='i'; $D[1]=self::$tbk; $D[2]='udUpd';
			$D['docEntry']=$docEntry; $D['_err1']='Error generando documento';
			$ivtPep->Livt[0]=$D;
			unset($ivtPep->Livt[0]);
			a_sql::multiQuery($ivtPep->Livt);
		}
		if(!_err::$err){ $c=true;
			$js=_js::e(3,'Documento generado correctamente','"docEntry":"'.$docEntry.'"');
			self::tb99P(['docEntry'=>$D['docEntry'],'dateC'=>1]);
      self::tbRel1P(['ott'=>$D['ott'],'otr'=>$D['otr'],'tr'=>$D['docEntry'],'serieId'=>$D['serieId'],'docNum'=>$D['docNum']]);
		}
		a_sql::transaction($c);
		_err::errDie();
		return $js;
	}

	static public function getOne($D){
    $M=a_sql::fetch('SELECT A.*, I.itemCode,I.itemName
    FROM '.self::$tbk.' A 
    JOIN itm_oitm I ON (I.itemId=A.itemId)
    WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo información de la orden',2=>'La orden no existe']);
    if(a_sql::$err){ return a_sql::$errNoText; }
    else{
      $M['MPtotal2']=$M['MOtotal2']=$M['MAtotal2']=$M['MPtotal2']=$M['SVtotal2']=$M['CIFtotal2']=$M['SEtotal2']=$M['PePtotal2']=0;
      $M['MPtotal3']=$M['MOtotal3']=$M['MAtotal3']=$M['MPtotal3']=$M['SVtotal3']=$M['CIFtotal3']=$M['SEtotal3']=$M['PePtotal3']=0;
      $M['LF']=[];
      /* Materiales */
      $q=a_sql::query('SELECT B.wfaId,I.itemCode,B.itemSzId itemSzId,I.itemName,B.price,B.quantity,I.udm,B.priceLine,B.lineType,B.priceLine2,B.reqQty,B.diffPrice,B.diffQty, B.priceLine3
      FROM '.self::$tbk1.' B
      LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
      WHERE B.docEntry=\''.$D['docEntry'].'\' ',[1=>'Error obteniendo materiales de las fases',2=>'Las fases no tiene materiales asignados']);
      if(a_sql::$err){ $M['LF']=json_decode(a_sql::$errNoText,1); }
      else{
        while($L=$q->fetch_assoc()){
					$k=$L['lineType'];
					$M[$k.'total2']=$L['priceLine2'];
					$M[$k.'total3']=$L['priceLine3'];
          $M['LF'][]=$L;
        }
      }
      $js=_js::enc2($M);
    }
    return $js;
  
  }
}
?>