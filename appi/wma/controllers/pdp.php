<?php
class wmaPdp{
static $serie='wmaPdp';
static $tbk='wma3_opdp';
static $tbk99='wma3_doc99';
static public function revDoc($D=array()){
	if(_js::iseErr($D['docDate'],'La fecha del documento debe estar definida.')){}
	else if(_js::iseErr($D['docType'],'Se debe definir el tipo de documento.')){}
	else if(!_js::textLimit($D['lineMemo'],500)){ _err::err('Los detalles no pueden exceder los 500 caracteres.',3); }
	else if(_js::isArray($D['L'])){ _err::err('No se han enviado lineas para el documento.',3); }
}
static public function getOne($D=[]){
	_ADMS::lib('iDoc');
	return iDoc::getOne(array('docEntry'=>$D['docEntry'],
	'fromA'=>'A.docEntry,A.serieId,A.docNum,A.dateC,A.userId,A.docDate,A.docType,A.dueDate,A.docStatus,A.lineMemo FROM wma3_opdp A',
	'fromB'=>'B.id,B.lineNum,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm,B.quantity,B.openQty FROM wma3_pdp1 B JOIN itm_oitm I ON (I.itemId=B.itemId)'));
}
static public function post($D=[],$trans='Y',$P2=array()){
	$docEntry=$D['docEntry'];
	self::revDoc($D);
	if(!_err::$err){
		$Ld=$D['L']; unset($D['L']); $errs=0; $n=1;
		foreach($Ld as $nk=>$L){//ln->T[xx] ->
			$totaln=0; $ln ='Linea '.$n.': ';
			if(_js::iseErr($L['itemId'],$ln.'No se ha encontrado el ID del artículo.','numeric>0')){ $errs=1; break; }
			else if(_js::iseErr($L['itemSzId'],$ln.'No se ha definido el ID de la talla.')){ $errs=1; break; }
			else if(_js::iseErr($L['quantity'],$ln.'La cantidad debe ser mayor a 0.','numeric>0')){ $errs=1; break; }
			else{
				$Ld[$nk]=$L;
				$Ld[$nk]['lineNum']=$n;
			}
			$n++;
		}
		if(!_err::$err){//Generar Encabezado
			_ADMS::lib('_2d,docSeries');
			if($trans!='N'){ a_sql::transaction(); $c=false; }
			if(!$D['docEntry'] && $P2['docSeries']!='N'){ $D=docSeries::nextNum($D,$D); }
			if($js=_err::$err){ return _err::$errText; }
			$ins=a_sql::uniRow($D,array('tbk'=>'wma3_opdp','qk'=>'ud','qku'=>'ud','wh_change'=>'docEntry=\''.$D['docEntry'].'\' LIMIT 1'));
			if(a_sql::$err){ _err::err('Error generando documento. '.a_sql::$errText,3); }
			else{
				$docEntry=($ins['insertId'])?$ins['insertId']:$D['docEntry'];
				$jsEntry='"docEntry":"'.$docEntry.'"';
				// Lineas
				foreach($Ld as $nk=>$L2){
					unset($L2['handInv']);
					$L2['docEntry']=$docEntry;
					$L2['openQty']=$L2['quantity'];
					$L2[0]='i'; $L2[1]='wma3_pdp1'; $L2['_unik']='id';
					//Revisar validez actualizacion
					if($L2['id']>0){
						$whId='WHERE id=\''.$L2['id'].'\' LIMIT 1';
						$vA=a_sql::fetch('SELECT progQty,quantity,openQty FROM wma3_pdp1 '.$whId,[1=>$ln.'Error verificando actualización',2=>$ln.'La linea a modificar no existe']);
						if(a_sql::$err){ _err::err(a_sql::$errNoText); break; }
						$dif=$L2['quantity']-$vA['quantity'];
						$nuevo = ($vA['openQty'])+$dif;
						if($nuevo<0){ _err::err($ln.'No se puede realizar la modificación, por que la cantidad pendiente quedaría negativa.',3); break; }
						else if($vA['progQty']>0 && $L2['delete']=='Y'){ _err::err($ln.'No se puede eliminar una linea con procedimientos iniciados.',3); break; }
						else{ $L2['openQty']=$nuevo; }
					}
					$Ld[$nk]=$L2;
				}
				if(!_err::$err){ //ingresar lineas
					a_sql::multiQuery($Ld);
				}
				if(!_err::$err){ $c=true;
					$js=_js::r('Información guardada correctamente.',$jsEntry);
					_ADMS::lib('JLog');
					if($D['docEntry']){
						JLog::post(array('tbk'=>self::$tbk99,'serieType'=>self::$serie,'docEntry'=>$docEntry,'dateUpd'=>1));
					}else{
						JLog::post(array('tbk'=>self::$tbk99,'serieType'=>self::$serie,'docEntry'=>$docEntry,'dateUpd'=>1));
					}
				}
				if($trans!='N'){ a_sql::transaction($c); }
			}
		}
	}
	_err::errDie();
	return $js;
}

static public function openQty($D=[]){
	$q=a_sql::fetch('SELECT SUM(B.openQty+B.progQty) openQty 
	FROM wma3_opdp A 
	JOIN wma3_pdp1 B ON (B.docEntry=A.docEntry) 
	WHERE A.docStatus=\'O\' AND (B.openQty+B.progQty)>0 AND B.itemId=\''.$D['itemId'].'\' AND B.itemSzId=\''.$D['itemSzId'].'\' ',array(1=>'Error obteniendo total de programación del artículo'));
	if(a_sql::$err){ return a_sql::$errNoText; }
	else if(a_sql::$errNo==2){ return 0; }
	else{ return (($q['openQty'])?$q['openQty']:0); }
}
static public function putStatusClose($D=array()){
	a_sql::transaction(); $cmt=false; $errs=0;
	_ADMS::lib('iDoc');
	$js=iDoc::putStatus(array('t'=>'C','tbk'=>self::$tbk,'serieType'=>self::$serie,'docEntry'=>$D['docEntry'],'log'=>self::$tbk99,'reqMemo'=>'Y','lineMemo'=>$D['lineMemo']));
	if(_err::$err){ return _err::$errText; }
	else{ $cmt=true; }
	a_sql::transaction($cmt);
	return $js;
}
static public function logGet($D=array()){
	_ADMS::lib('JLog');
	return JLog::get(array('tbk'=>self::$tbk99,'serieType'=>self::$serie,'docEntry'=>$D['docEntry']));
}
static public function logRel1($D=array()){
	_ADMS::lib('JLog');
	return JLog::rel1_get(array('ott'=>self::$serie,'otr'=>$D['docEntry']));
}
}
?>