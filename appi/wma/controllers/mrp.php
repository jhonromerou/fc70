<?php
class wmaMrp{
static $n=0;
static $MR0=array();//save mrp ver si repetie para MR[]
static $MR=array();//save mrp
static $MPonHand=array();
static $uniqSize=false;
static public function get($L0=array(),$P=array()){
	if(self::$uniqSize===false){
		$qf=a_sql::fetch('SELECT A.grsId, B.itemSzId
		FROM itm_ogrs A
		JOIN itm_grs2 B2 ON (B2.grsId=A.grsId)
		JOIN itm_grs1 B ON (B.itemSzId=B2.itemSzId)
		WHERE B.uniqSize=\'Y\' LIMIT 1');
		self::$uniqSize=$qf;
		if(!a_sql::$errNo==-1){ self::$uniqSize=array(); }
	}
	$wh=''; $gb='';
	$sume='SUM(T1.quantity) quantity ';
	if($P['sum']){ $sume .= ','.$P['sum']; }
	$gb .='I.itemId,I.itemCode,I.itemName,I.udm,I.itemType,I.buyUdm,I.buyOper,I.buyFactor,I.buyPrice,I.invPrice,I.minBuyOrd,T1.lineType,T1.wfaId';
	$lefx='';
	if($P['ivt']!='N'){
		$lefx='LEFT JOIN ivt_oitw W1 ON (W1.itemId=T1.citemId AND W1.itemSzId=T1.citemSzId AND W1.whsId=\''.$P['whsId'].'\')';
		$gb .=',W1.onHand,W1.onOrder';
	}
	if($P['gby']){ $gb .=','.$P['gby']; }
	$gb .=',I.grsId,C.cardName';
	$fie=$gb.',T1.buyPrice price';
	$gb .=',T1.buyPrice';
	if($P['join']){
		$lefx .="\n".$P['join'];
		if($P['joing']){ $gb .= ','.$P['joing']; }
		if($P['joinf']){ $fie .= ','.$P['joinf']; }
	}
	//$whType='AND T1.lineType IN(\'MP\',\'SE\')';
	if($P['whType']){ $whType='AND T1.lineType '.$P['whType']; } //pasar in o not in
	if($P['type']=='all'){ $whType=''; }
	if($P['wh']){ $wh .=' '.a_sql_filter($P['wh']); }
	//get query
	$fie = 'I1.itemId fitemId,I1.itemCode fitemCode,I1.itemName fitemName,'.$fie;
	$gb = 'I1.itemId,I1.itemCode,I1.itemName,'.$gb;
	$qt=''.$fie.',T1.citemSzId,I.defWhs whsId,'.$sume.'
	FROM itm_itt1 T1
	JOIN itm_oitm I1 ON (I1.itemId=T1.itemId) 
	JOIN itm_oitm I ON (I.itemId=T1.citemId) 
	LEFT JOIN par_ocrd C ON (C.cardId=I.cardId)';
	$q=a_sql::query('SELECT '.$qt.'
	'.$lefx.'
	WHERE 1 '.$whType.' AND T1.itemId=\''.$L0['itemId'].'\'
	AND (T1.itemSzId=\''.$L0['itemSzId'].'\' OR (T1.itemSzId=0 AND T1.variType=\'N\'))
	'.$wh.'
	GROUP BY T1.citemSzId,I.defWhs,'.$gb.'',array(1=>'Error obteniendo lista de materiales: '));
	//print_r($q);
	if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
	else if(a_sql::$errNo==-1){
		while($L=$q->fetch_assoc()){
			/* tallas unica o si es 0 bugs fix */
			if(preg_match('/^(SE|MO|MA|CIF|SV)$/',$L['lineType'])){
				$L['buyPrice']=$L['invPrice'];
				$L['buyUdm']=$L['udm'];
			}
			if(self::$uniqSize['grsId']==$L['grsId'] || self::$uniqSize['itemSzId']==$L['citemSzId'] || $L['citemSzId']==0){
				$L['citemSzId']=self::$uniqSize['itemSzId'];
			}
			$kr=$L['itemId'].$L['citemSzId'];
			if($P['gby1']){ $kr .= $L[$P['gby1']]; }
			$reqQty=$L['quantity']*$L0['reqQty'];
			unset($L['quantity']);
			$bc=$L['itemId'];
			/* descomponer lista, no se muestra el articulo */
			if($P['seView']=='tree' && ($L['itemType']=='SE' || $L['itemType']=='CIF')){
				self::get(array('itemId'=>$L['itemId'],'itemSzId'=>$L['citemSzId'],'reqQty'=>$reqQty,'whsId'=>$L['whsId'],'onHand'=>$L['onHand'],'onOrder'=>$L['onOrder'],'minBuyOrd'=>$L['minBuyOrd']),$P);
				continue;
			}
			if(!array_key_exists($kr,self::$MR0)){
				self::$MR0[$kr]=($P['fKey'])?$L[$P['fKey']]:self::$n;
				self::$n++;
			}
			$k=self::$MR0[$kr];
			if(!array_key_exists($k,self::$MR)){
				$L['reqQty']=0;
				self::$MR[$k]=$L;
			}
			self::$MR[$k]['reqQty']+=$reqQty;
		}
	}
	return false;
}

static public function usmSiigo($D){
	_ADMS::lib('_File');
	$fileName='pepmrp';
	JFile::fromPHP($fileName);
	$Dx=[];
	foreach(JFile::$PHP as $n=>$L){
		if($L['price']>0){}else{ continue; }
		if($n>20){ break; }
		$q=a_sql::fetch('SELECT C1.accCode accIvt
		FROM itm_oitm I
		JOIN itm_oiac GC ON GC.accGrId=I.accGrId
		LEFT JOIN gfi_opdc C1 ON C1.accId=GC.accIvt
		WHERE I.itemId=\''.$L['itemId'].'\' LIMIT 1');
		$Dx[]=['itemCode'=>$L['itemCode'],'itemName'=>$L['itemName'],
		'siiLine'=>substr($L['itemCode'],2,3),
		'siiGr'=>substr($L['itemCode'],5,4),
		'siiCode'=>substr($L['itemCode'],9,6),
		'accIvt'=>$q['accIvt']
		];
	}
	print_r($Dx);
}
static public function fromPdp($D){
	//return self::usmSiigo($D);
	_ADMS::lib('sql/filter'); _ADMS::lib('_File');
	$fileName='pepmrp';
	self::$MR=array();
	$viewType=$D['viewType']; $seView=$D['seView'];
	$whsId=$D['whsId'];
	$docEntrys=$D['A_docEntry(E_in)'];
	unset($D['docEntrys'],$D['seView'],$D['whsId'],$D['viewType']);
	$whI=array();
	$Kr=array('I_itemCode(E_like3)','T1_wfaId(E_in)','I_itemGr(E_igual)');
	foreach($Kr as $n => $k1){
		if($D[$k1]){ $whI[$k1]=$D[$k1]; unset($D[$k1]); }
	}
	$Pw=array('wh'=>$whI,'whsId'=>$whsId,'seView'=>$seView);
	$gb='B.itemId,B.itemSzId,I.itemCode,I.itemName';
	$fie= $gb.',SUM(B.openQty) reqQty';
	$wh ='';
	$TDA=[];
	if($viewType=='quantity'){ $fie =$gb.',SUM(B.quantity) reqQty'; }
	else{ $wh .= ' AND B.openQty>0 '; }
	$wh .=a_sql_filter($D);
	self::$MR=[];
	$q=a_sql::query('SELECT '.$fie.'
	FROM wma3_opdp A
	JOIN wma3_pdp1 B ON (B.docEntry=A.docEntry)
	LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
	WHERE A.docStatus!=\'N\' '.$wh.' GROUP BY '.$gb,array(1=>'Error obteniendo requerimiento materiales (opdp): ',2=>'No se encontraron resultados requerimiento materiales (opdp)'));
	if(a_sql::$err){ return a_sql::$errNoText; }
	while($L0=$q->fetch_assoc()){
		self::get($L0,$Pw);
		if(_err::$err){ return _err::$errText; }
	}
	//if($errs==0){ return JFile::toPHP($TDA,$fileName); }
	if($errs==0){ return _js::enc2(array('L'=>self::$MR)); }
}
static public function fromOdp($D){
	_ADMS::lib('sql/filter');
	self::$MR=array();
	$docEntrys=$D['docEntrys'];
	if(preg_match('/\,/',$docEntrys)){ $D['A.docEntry(E_in)']=$docEntrys; }
	else if(preg_match('/\-/',$docEntrys)){
		$sep=explode('-',$docEntrys);
		$D['A.docEntry(E_mayIgual)']=$sep[0];
		$D['A.docEntry(E_menIgual)']=$sep[1];
	}
	else{ $D['A.docEntry']=$docEntrys; }
	$seView=$D['seView'];
	$whsId=$D['whsId'];
	unset($D['docEntrys'],$D['seView'],$D['whsId']);
	$whI=array();
	$Kr=array('I_itemCode(E_like3)','T1_wfaId(E_in)','I_itemGr(E_igual)');
	foreach($Kr as $n => $k1){
		if($D[$k1]){ $whI[$k1]=$D[$k1]; unset($D[$k1]); }
	}
	$gb='B.itemId,B.itemSzId';
	$wh ='';
	$fie =$gb.',SUM(B.quantity) reqQty';
	$wh .=a_sql_filter($D);
	$q=a_sql::query('SELECT '.$fie.'
	FROM wma3_oodp A
	JOIN wma3_odp1 B ON (B.docEntry=A.docEntry)
	LEFT JOIN itm_oitm I ON (I.itemId=B.itemId)
	WHERE A.docStatus!=\'N\' '.$wh.' GROUP BY '.$gb,array(1=>'Error obteniendo requerimiento materiales (odp): ',2=>'No se encontraron resultados requerimiento materiales (odp)'));
	if(a_sql::$err){ return a_sql::$errNoText; }
	while($L0=$q->fetch_assoc()){
		self::get($L0,array('wh'=>$whI,'seView'=>$seView,'whsId'=>$whsId));
		if(_err::$err){ return _err::$errText; }
	}
	if($errs==0){ return _js::enc2(array('L'=>self::$MR)); }
}
static public function fromPep($D){
	_ADMS::lib('sql/filter');
	$PeP=array();
	$PePw=array();
	$seView=$D['seView'];
	$whsId=$D['whsId'];
	unset($D['docEntrys'],$D['seView'],$D['whsId']);
	$Pw=array('wh'=>array(),'whsId'=>$whsId);
	if($D['citemCode']){ $Pw['wh']['I.itemCode(E_like3)']=$D['citemCode']; }
	unset($D['citemCode']);
	self::$MR=array();
	$wh='WFA.wfaClass IN (\'N\')';
	$wh .=a_sql_filter($D);
	$gb='I.itemId,I.itemCode,B.wfaId,B.itemSzId,M1.lineNum';
	$q=a_sql::query('SELECT '.$gb.',SUM(B.onHand) reqQty
	FROM pep_oitw B
	LEFT JOIN wma_owfa WFA ON (WFA.wfaId=B.wfaId)
	LEFT JOIN wma_mpg1 M1 ON (M1.itemId=B.itemId AND M1.wfaId=B.wfaId)
	JOIN itm_oitm I ON (I.itemId=B.itemId)
	WHERE B.onHand>0 AND '.$wh.' GROUP BY '.$gb.'',array(1=>'Error obteniendo requerimiento materiales (opep): ',2=>'No se encontraron resultados requerimiento materiales (pep)'));
	if(a_sql::$err){ return a_sql::$errNoText; }
	while($L=$q->fetch_assoc()){
		if(!($L['lineNum']>0)){
		 return _err::err('error obteniendo siguiente fase del articulo '.$L['itemCode'],3);
		}
		$Pw['wh']['T1.wfaOrder(E_may)']=$L['lineNum'];
		self::get($L,$Pw);
		if(_err::$err){ return _err::$errText; }
	}
	if($errs==0){ return _js::enc2(array('L'=>self::$MR)); }
}
/**
 * Obtiene la lista de materiales de los productos por articulo
 */
static public function ficha($D){
	_ADMS::lib('sql/filter');
	$wh ='';
	if($D['priceDiff']=='Y'){
		$wh .= 'AND T1.buyPrice != I2.invPrice ';
	}
	unset($D['priceDiff']);
	$wh .=a_sql_filter($D);
	$gb='I.itemId fitemId,I.itemCode fitemCode,I.itemName fitemName,G1.itemSize fitemSize,
	I2.itemId,I2.itemCode,I2.itemName,G12.itemSize,I2.udm,I2.invPrice,
	T1.lineType,T1.wfaId,T1.quantity reqQty,T1.buyPrice price';
	$q=a_sql::query('SELECT '.$gb.'
	FROM itm_oitm I
	JOIN itm_grs2 G2 ON (G2.grsId=I.grsId)
	JOIN itm_grs1 G1 ON (G1.itemSzId=G2.itemSzId)
	JOIN itm_itt1 T1 ON (T1.itemId=I.itemId AND (
		T1.itemSzId=G1.itemSzId OR (T1.itemSzId=0 AND T1.variType=\'N\')
	))
	LEFT JOIN wma_owfa WFA ON (WFA.wfaId=T1.wfaId)
	JOIN itm_oitm I2 ON (I2.itemId=T1.citemId)
	LEFT JOIN itm_grs1 G12 ON (G12.itemSzId=T1.citemSzId)
	WHERE I.prdItem=\'Y\' '.$wh.' 
	ORDER BY I.itemCode,G1.itemSize,T1.lineNum ASC
	',array(1=>'Error obteniendo información de la ficha'));
	$Mx=[];
	if(a_sql::$err){ return a_sql::$errNoText; }
	while($L=$q->fetch_assoc()){
		$Mx[]=$L;
	}
	if($errs==0){ return $Mx; }
}
}
?>
