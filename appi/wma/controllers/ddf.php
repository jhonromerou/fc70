<?php
class wmaDdf{
static $serie='wmaDdf';
static $tbk='wma3_oddf';
static $tbk99='wma3_doc99';
static public function revDoc($D=array()){
	if(_js::iseErr($D['docDate'],'La fecha del documento debe estar definida.')){}
	else if(_js::iseErr($D['docType'],'Se debe definir el tipo de documento.')){}
	else if(!_js::textLimit($D['lineMemo'],500)){ _err::err('Los detalles no pueden exceder los 500 caracteres.',3); }
	else if(_js::isArray($D['L'])){ _err::err('No se han enviado lineas para el documento.',3); }
}
static public function getOne($D=[]){
	_ADMS::lib('iDoc');
	return iDoc::getOne(array('docEntry'=>$D['docEntry'],
	'fromA'=>'A.docEntry,A.serieId,A.docNum,A.dateC,A.userId,A.docDate,A.docType,A.dueDate,A.docStatus,A.lineMemo FROM wma3_opdp A',
	'fromB'=>'B.id,B.lineNum,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm,B.quantity,B.openQty FROM wma3_pdp1 B JOIN itm_oitm I ON (I.itemId=B.itemId)'));
}
static public function post($D=[],$trans='Y',$P2=array()){
	_ADMS::mApps('wma/wma3');
	wma3::i_wfaFS();
	if($trans!='N'){ a_sql::transaction(); $c=false; }
	$js=self::ddf_post($D);
	if(_err::$err){ $js=_err::$errText; }
	else{ $c=true;
		
	}
	if($trans!='N'){ a_sql::transaction($c); }
	return $js;
}

static public function ddf_post($D=array(),$P2=array()){
	$series=($P2['tt'])?$P2['tt']:'wmaDdf';
	$oTy=($P2['tr'])?$P2['tr']:0;
	if($P2['def_tt']){ $series=$P2['def_tr']; }
	if($P2['def_tr']){ $oTy=$P2['def_tr']; }
	//$systemFase=(array_key_exists('systemFase',$P2));
	//if($systemFase){ $P2['def_wfaBefId']=$P2['systemFase']; }
	$isE=($D['docType']=='E'); unset($D['cardName']);
	$whs0=($D['whsIdFrom']!='N999'); $js=false; $jsOk=false;
	if($js=_js::ise($D['docDate'],'Se debe definir la fecha del documento')){}
	else if($js=_js::ise($D['wfaId'],'Se debe definir la fase para el documento.','numeric>0')){}
	else if($js=_js::ise($D['whsId'],'Se debe definir el almacen para proceso.','numeric>0')){}
	else if($whs0 && $js=_js::ise($D['whsIdFrom'],'Se debe definir el almacen anterior.','numeric>0')){}
	else if($isE && $js=_js::ise($D['cardId'],'Se debe definir el tercero.','numeric>0')){}
	else if(_js::isArray($D['L'])){ $js=_js::e(3,'No se enviaron lineas para el documento.'); }
	else{ $errs=0;
		$L2=$D['L']; unset($D['L']);
			_ADMS::mApps('wma/PeP'); $Liv=array();
			$n=1; $odpNum=0;
		/* Revisar campos, inventario de fase, y estado de fase en odp */
		foreach($L2 as $k =>$L){
		$ln='Linea '.$n.': '; $n++;
			/* Definir fase anterior por defecto */
			if($P2['def_wfaBefId']){ $L['wfaIdBef']=$P2['def_wfaBefId']; }
			if($P2['def_tt']){ $L['tt']=$series; }
			if($P2['def_tr']){ $L['tr']=$oTy; }
			if($L['wfaIdBef']==0){ $L['wfaIdBef']=wma3::$V['wfaFS']; }
			$L2[$k]=$L;
			$wfaId0=($L['wfaIdBef']!=0);
			$diffWhs=($D['whsIdFrom']!=$D['whsId']);
			$revInv=($D['wfaId']!=$L['wfaIdBef']);
			$movBef=$revInv;
			$L2[$k]['wfaIdBef']=$L['wfaIdBef'];
			$isFase0=($L['wfaId']==wma3::$V['wfaFS']);
			$isOdp=($L['tt']=='wmaOdp');/* Si tiene origen en odp, revisar */
			if($isOdp && $js=_js::ise($L['tr'],$ln.'Se debe definir el número de la orden de producción.')){ $errs++; break; }
			//else if($js=_js::ise($L['wfaIdBef'],$ln.'Se debe definir la fase anterior del proceso. ('.$L['wfaIdBef'].')','numeric>0')){ $errs++; break; }
			else if($js=_js::ise($L['quantity'],$ln.'La cantidad debe ser mayor a 0.','numeric>0')){ $errs++; break; }
			else{
				/* SI ==>odp -revisar estado fase a realizar y de la anterior */
				if($isOdp){
					if(!$isFase0){
						wma3::odp_faseAnt(array('docEntry'=>$L['tr'],'wfaId'=>$D['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']),$ln);
						if(wma3::$err){ $errs++; $js=wma3::$errText; break; }
					}
					$qf=wma3::odp_fase(array('docEntry'=>$L['tr'],'wfaId'=>$D['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']),$ln);
					if(wma3::$err){ $errs++; $js=wma3::$errText; break; }
					$odpNum++;
				}
				/* revisar inventario si es necesario y no es fase0 */
				if($L['wfaIdBef']!=wma3::$V['wfaFS'] && $revInv){
					PeP::onHand($L,array('revD'=>true, 'whsId'=>$D['whsIdFrom'],'wfaId'=>$L['wfaIdBef'],'lnt'=>$ln));
					if(PeP::$err){ $js=PeP::$errText; $errs++; break; }
					$Liv[]=array('outQty'=>'Y','whsId'=>$D['whsIdFrom'],'wfaId'=>$L['wfaIdBef'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']);
				}
				/* ingresar si es diferente o si es fase0 */
				if($revInv || $L['wfaIdBef']==wma3::$V['wfaFS']){
					$Liv[]=array('inQty'=>'Y','whsId'=>$D['whsId'],'wfaId'=>$L['wfaIdBef'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'quantity'=>$L['quantity']);
				}
			}
		}
		/* SI ==> Actualizar estado de fase si es odp y liberar programado en pdp si fase0 */
		if($errs==0 && $odpNum>0){
			foreach($L2 as $k =>$L){ $ln='Linea '.$n.': '; $n++;
				if($L['tt']=='wmaOdp'){
					wma3::odp_fasePut(array('docEntry'=>$L['tr'],'wfaId'=>$D['wfaId'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'qSet'=>'openQty=openQty-'.$L['quantity'].', onProQty=onProQty+'.$L['quantity'].' '),array('revOdp'=>'Y'));
					if(wma3::$err){ $js=wma3::$errText; $errs++; break; }
					/*Actualizar pdp -prog si es fase0 */
					if(wma3::$D['pdp_docEntry'] && $L['wfaIdBef']==wma3::$V['wfaFS']){
						wma3::pdp_putLine(array('docEntry'=>wma3::$D['pdp_docEntry'],'itemId'=>$L['itemId'],'itemSzId'=>$L['itemSzId'],'qSet'=>'progQty=progQty-'.$L['quantity']));
							if(_err::$err){ $js=_err::$errText; $errs++; break; }
						}
				}
			}
		}
		/*Generar documento de fase*/
		if($errs==0){
			$docEntry=a_sql::qInsert($D,array('tbk'=>'wma3_oddf','qk'=>'ud','qku'=>'ud'));
			if(a_sql::$err){ $js=_js::e(3,'Error generando documento de fase: '.a_sql::$errText); $errs++; }
			else{
				foreach($L2 as $k =>$L){ $ln='Linea '.$n.': '; $n++;
					$L['docEntry']=$docEntry;
					$L['openQty']=$L['quantity'];
					$ins2=a_sql::insert($L,array('table'=>'wma3_ddf1','qDo'=>'insert'));
					if($ins2['err']){ $js=_js::e(3,'Error generando linea de documento de fase: '.$ins2['text']); $errs++; break; }
				}
			}
		}
		/*mover inventario */
		if($errs==0 && count($Liv)>0){
			PeP::onHand_put($Liv,array('docDate'=>$D['docDate'],'tt'=>$series,'tr'=>$docEntry));
			if(PeP::$err){ $js=PeP::$errText; $errs++;; }
		}
		if($errs==0){ $jsOk=_js::r('Documento generado correctamente.','"docEntry":"'.$docEntry.'"');
			self::$Ds=array('docEntry'=>$docEntry);
		}
	}
	if($js){ _err::err($js); }
	return $jsOk;
}
static public function ddf_put($L=array(),$ln=''){
	$ori =' on[wma3::ddf_put()]';
	if($js=_js::ise($L['nfId'],$ln.'Se debe definir ID de nota de fase.'.$ori,'numeric>0')){ _err::err($js); return false; }
	else if($js=_js::ise($L['qSet'],$ln.'qSet is undefined.'.$ori)){ _err::err($js); return false; }
	$qf=a_sql::query('UPDATE '._0s::$Tb['wma3_ddf1'].'
	SET '.$L['qSet'].'
	WHERE nfId=\''.$L['nfId'].'\' LIMIT 1',array(1=>$ln.'Error actualizado nota de fase.'.$ori));
	if(a_sql::$err){ _err::err(a_sql::$errNoText); return false; }
	else{ return true; }
}
static public function ddf_getLine($L=array(),$P=array()){
	$ln=$P['ln']; $errs=0;
	$qf=a_sql::fetch('SELECT A.docStatus,A.canceled,A.cardId,A.whsId,A.whsIdFrom,A.wfaId,WF.wfaName,WF1.wfaName wfaNameBef,B.*
	FROM wma3_ddf1 B
	JOIN wma3_oddf A ON (A.docEntry=B.docEntry)
	LEFT JOIN wma_owfa WF ON (WF.wfaId=A.wfaId)
	LEFT JOIN wma_owfa WF1 ON (WF1.wfaId=B.wfaIdBef)
	WHERE B.nfId=\''.$L['nfId'].'\' LIMIT 1',array(1=>$ln.'Error obteniendo información de la nota.',2=>$ln.'La nota de fase no existe.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; }
	//else if($qf['docEntry']!=$docEntry){ $js=_js::e(3,$ln.'La nota de fabricación no corresponde con el documento de fase #'.$qf['docEntry']); $errs++;  }
	else if($qf['canceled']=='Y'){ $js=_js::e(3,$ln.'El documento está anulado, no se puede recibir la linea.'); $errs++; }
	else if($qf['docStatus']=='C'){ $js=_js::e(3,$ln.'El documento está cerrado, no se puede recibir la linea.'); $errs++;  }
	else if($L['quantity']>$qf['openQty']){ $js=_js::e(3,$ln.'La cantidad a recibir ('.$L['quantity'].') es mayor a la cantidad pendiente ('.($qf['openQty']*1).') del documento.'); $errs++; }
	if($errs){ _err::err($js); }
	$qf['wfaIdBef']=($qf['wfaIdBef']==self::$V['wfaFS'])?self::$V['wfaFS']:$qf['wfaIdBef'];
	return $qf;
}


static public function putStatusClose($D=array()){
	a_sql::transaction(); $cmt=false; $errs=0;
	_ADMS::lib('iDoc');
	$js=iDoc::putStatus(array('t'=>'C','tbk'=>self::$tbk,'serieType'=>self::$serie,'docEntry'=>$D['docEntry'],'log'=>self::$tbk99,'reqMemo'=>'Y','lineMemo'=>$D['lineMemo']));
	if(_err::$err){ return _err::$errText; }
	else{ $cmt=true; }
	a_sql::transaction($cmt);
	return $js;
}
static public function logGet($D=array()){
	_ADMS::lib('JLog');
	return JLog::get(array('tbk'=>self::$tbk99,'serieType'=>self::$serie,'docEntry'=>$D['docEntry']));
}
}
?>