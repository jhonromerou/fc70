<?php
class ivtPep{
	public $La=[];
	public $Livt=[]; public $n=0; //linea n wtr es n+1
	public $tt=''; public $tr=''; public $docDate='';
	public $itk=''; //whsId-wfaId-itemId-itemSzId
	public $hands=false;// No guardar en handsAt, U=Guardar itk para evitar repetir consultas y acumular onHand+-
	public $handsAt=[]; //cada handSet, actualiza este campo
	public $qus=[0,0];
	static $canNegDef='N';
	static public function getAvgPrice($D=array()){//no usando en ivtDoc
		$qty=$D['inQty']-$D['outQty'];
		$vtotal=$D['stockValue']+($D['price']*$qty);
		$ntotal=$D['onHand']*1+$qty;/* + o - */
		if($ntotal==0){ $avgP=0; $vtotal=0; }
		else if($ntotal<0){ $avgP=0; $vtotal=0; }
		else{ $avgP=$vtotal/$ntotal; }
		/* (stockValue + ($)entry) / onHand + (q)entry */
		$D['stockValue']=$vtotal;
		$D['avgPrice']=$avgP;
		return $D;
	}
	function __construct($P){
		$this->tt=$P['tt'];
		$this->tr=$P['tr'];
		$this->docDate=$P['docDate'];
		$this->hands=($P['hands'])?$P['hands']:false;
	}
	public function getInfo($D=array(),$P=array()){
		$lnt=$P['lnt']; $ori=' on[ivtPep::getInf]';
		$this->itk=$D['whsId'].$D['wfaId'].$D['itemId'].$D['itemSzId'];
		if(array_key_exists($this->itk,$this->handsAt)){
			$this->handsAt[$this->itk]['_upd']='Y'; //importante para nuevo que tenga varias lineas
			$this->La=$this->handsAt[$this->itk];
			$this->qus[0]+=1;
			return true;
		}
		$this->qus[1]+=1;
		$revI=($P['revD']);
		$tb_oitw='pep_oitw'; $tb_wtr1='pep_wtr1';
		if($revI){ $ert=$lnt.'Error en consulta inventario en proceso. ';
			if($js=_js::ise($D['itemId'],$ert.'Se debe definir el artículo.')){
				self::$err=true; self::$errText=$js; return self::$err;
			}
			else if($js=_js::ise($D['itemSzId'],$ert.'Se debe definir la talla.'.$ori)){
				self::$err=true; self::$errText=$js; return self::$err;
			}
			else if($js=_js::ise($D['whsId'],$ert.'Se debe definir el almacen.'.$ori)){
				self::$err=true; self::$errText=$js; return self::$err;
			}
			else if($js=_js::ise($D['wfaId'],$ert.'Se debe definir la fase. ('.$D['wfaId'].')'.$ori)){
				self::$err=true; self::$errText=$js; return self::$err;
			}
		}
		$enS='whs:'.$D['whsId'].', wfa:'.$D['wfaId'].'';
		$errType=0;
		$qul='SELECT W1.id,W.whsId,WF.wfaId,I.itemId,G1.itemSzId,W1.onHand,W.whsCode,WF.wfaCode,I.itemCode,G1.itemSize,W1.avgPrice,W1.stockValue,IP.sumCost wfaCost,I.canNeg, I.costMet
		FROM itm_oitm I
		LEFT JOIN itm_grs1 G1 ON (G1.itemSzId=\''.$D['itemSzId'].'\')
		LEFT JOIN ivt_owhs W ON (W.whsId=\''.$D['whsId'].'\')
		LEFT JOIN wma_owfa WF ON (WF.wfaId=\''.$D['wfaId'].'\')
		LEFT JOIN itm_oipc IP ON (IP.itemId=I.itemId AND IP.itemSzId=G1.itemSzId AND IP.wfaId=WF.wfaId)
		LEFT JOIN pep_oitw W1 ON (W1.whsId=W.whsId AND W1.wfaId=WF.wfaId AND W1.itemId=I.itemId AND W1.itemSzId=G1.itemSzId)
		WHERE I.itemId=\''.$D['itemId'].'\' LIMIT 1';
		$this->La= a_sql::fetch($qul,array(1=>'Error obteniendo información de producto en proceso: '.$ori,2=>$lnt.'El articulo ('.$D['itemId'].') no existe.'.$enS.$ori));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else if(!($this->La['whsId']>0)){ _err::err($lnt.'El almacen no existe'.$ori,3); }
		else if(!($this->La['wfaId']>0)){ _err::err($lnt.'La fase de proceso no existe. ('.$D['wfaId'].')'.$ori,3); }
		else if(!($this->La['itemSzId']>0)){ _err::err($lnt.'El subproducto no existe'.$ori,3); }
		else{
			$this->La['invPrice']=$this->La['wfaCost']; //costo definido
			if($this->La['id']>0){ $this->La['_upd']='Y'; }
			if($this->La['avgPrice']<=0){ $this->La['avgPrice']=0; }
			if($this->La['stockValue']<=0){ $this->La['stockValue']=0; }
			//determinar tipo de costeo
			$this->La['costMet']='DOC'; //OJO! de momento todo entra segun costo documento
			{ $this->La['cost']=$this->La['avgPrice']; }
			$this->La['onHand0']=($this->La['onHand']=='')?0:$this->La['onHand']*1;
			unset($this->La['onHand']);
			$this->La['onHand']=$this->La['onHand0'];
			if($this->hands){ $this->handsAt[$this->itk]=$this->La; }
		}
	}
	public function handRevi($P2=array()){
		$_ori=' on[ivtPep::handRevi()]. (S:'.self::$canNegDef.', I:'.$this->La['canNeg'].')';
		if($this->hands){
			$this->La=$this->handsAt[$this->itk];
			if($P2['inQty']){ $this->handsAt[$this->itk]['onHand'] += $P2['inQty']; }
			if($P2['outQty']){ $this->handsAt[$this->itk]['onHand'] -= $P2['outQty']; }
		}
		if($P2['outQty']>0){
			$newHand = $this->La['onHand']-$P2['outQty'];
			if($newHand<0 && (self::$canNegDef=='N' || (self::$canNegDef=='item' && $this->La['canNeg']=='N'))){
				$itemName=$this->La['itemCode'].'-'.$this->La['itemSize'].') ';
				_err::err($itemName.'La cantidad ('.($P2['outQty']*1).') es mayor que la disponible ('.$this->La['onHand'].') para la fase  '.$this->La['wfaCode'].' en el almacen '.$this->La['whsCode'].$_ori,3);
			}
		}
	}
	public function handSett($L=array(),$P=array(),$La=false){
		$_ori=' on[ivtPep::handSett()]';
		$La=($La)?$La:$this->La;
		$inQty=$outQty=0;
		if($P['whsId']){ $L['whsId']=$P['whsId']; }
		if($L['inQty']){ $qty=$inQty=$L['inQty']; }
		if($L['outQty']){ $qty=$outQty=$L['outQty']; }
		if($qty>0){
			$numFactor=$L['numFactor']*1;
			if($numFactor<=0){ $numFactor=1; }
			$L['inQty']=$L['inQty']*$numFactor;
			$L['outQty']=$L['outQty']*$numFactor;
			$qty=$qty*$numFactor;
			$L['quantity']=$qty;
			$L['onHand']=$La['onHand'];
			$L['stockValue']=($La['stockValue'])?$La['stockValue']:0;
			//Obtener nuevo promedio de entrada/salida
			$L['price']=($L['cost'])?$L['cost']:$La['cost'];
			$L=self::getAvgPrice($L); unset($L['cost']);
			$newHand=$La['onHand']+$L['inQty']-$L['outQty'];
			$Li=array('i','pep_oitw','udU','whsId'=>$La['whsId'],'wfaId'=>$La['wfaId'],'itemId'=>$La['itemId'],'itemSzId'=>$La['itemSzId'],'onHand'=>$newHand,'avgPrice'=>$L['avgPrice'],'stockValue'=>$L['stockValue'],
				'_err1'=>'Error actualización cantidad en inventario.'.$_ori
			);
			if($La['_upd']=='Y'){ $Li[0]='u';
				$Li['_wh']='id=\''.$La['id'].'\' LIMIT 1';
				$Li['_wh']='whsId=\''.$La['whsId'].'\' AND wfaId=\''.$La['wfaId'].'\' AND itemId=\''.$La['itemId'].'\' AND itemSzId=\''.$La['itemSzId'].'\' LIMIT 1';
			}
			$this->n++; 
			$this->Livt[]=$Li; unset($Li);
			$this->Livt[]=['i','pep_wtr1','ud','whsId'=>$La['whsId'],'wfaId'=>$La['wfaId'],'itemId'=>$La['itemId'],'itemSzId'=>$La['itemSzId'],'quantity'=>$qty,'inQty'=>$L['inQty'],'outQty'=>$L['outQty'],
			'onHandAt'=>$newHand,'avgPrice'=>$L['avgPrice'],'stockValue'=>$L['stockValue'],
			'tt'=>$this->tt,'tr'=>$this->tr,'docDate'=>$this->docDate,
			'_err1'=>'Error registrando histórico.'.$_ori,
			'price'=>$L['price'],'priceLine'=>$L['price']*$qty,
			'sbPrice'=>$L['sbPrice'],'sbPriceLine'=>$L['sbPriceLine']
			];
			//if(a_ses::$userId==1){ print_r($this->La); }
		}
	}
}
?>