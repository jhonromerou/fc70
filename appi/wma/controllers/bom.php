<?php
class wmaBom{
	static public function fases($D=[]){
		if($D['wfaId']){ return self::byFase($D); }
		else{ return self::byAll($D); }
	}
	static public function byFase($D=[]){
		if(_js::iseErr($D['itemId'],'Se debe definir el articulo.','numeric>0')){}
		else if(_js::iseErr($D['itemSzId'],'Se debe definir el subproducto.','numeric>0')){}
		else if(_js::iseErr($D['wfaId'],'Se debe definir la fase.','numeric>0')){}
		else{
			$Mx=[];
			/*1. obtener fase anterior lineType=PeP */
			$qF=a_sql::fetch('SELECT I.itemId,I.itemCode,  I.itemName, M.wfaId, M.wfaIdBef, IC.sumCost
			FROM wma_mpg1 M
			JOIN itm_oitm I ON (I.itemId=M.itemId)
			LEFT JOIN itm_oipc IC ON (IC.itemId=M.itemId AND IC.itemSzId=\''.$D['itemSzId'].'\' AND IC.wfaId=M.wfaIdBef)
			WHERE M.itemId=\''.$D['itemId'].'\' AND M.wfaId=\''.$D['wfaId'].'\' LIMIT 1',[1=>'Error obteniendo fase anterior del proceso']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else{
				$Mx=['itemId'=>$D['itemId'],'itemSzId'=>$D['itemSzId'],'wfaName'=>$qF['wfaName'],'itemCode'=>$qF['itemCode'],'itemName'=>$qF['itemName']];
				$Mx['L']=array();
				if($qF['wfaIdBef']>0){
					$Mx['L'][]=['lineType'=>'PeP','itemId'=>$D['itemId'],'itemSzId'=>$D['itemSzId'],'wfaId'=>$qF['wfaIdBef'],'wfaName'=>$qF['wfaNameBef'],'baseQty'=>1,'price'=>$qF['sumCost'],'price2'=>$qF['sumCost']];
				}
			}
			/*2. obtener componentes MP,MO,MA,CIF */
			if(!_err::$err){
				$q=a_sql::query('SELECT A.citemId,A.citemSzId,A.lineType,A.whsId,A.buyPrice,A.quantity,I.itemCode,I.itemName,I.udm
				FROM itm_itt1 A
				JOIN itm_oitm I ON (I.itemId=A.citemId)
				WHERE A.itemId=\''.$D['itemId'].'\' AND A.wfaId=\''.$D['wfaId'].'\' AND ((A.itemSzId=0 AND A.variType=\'N\') OR (A.itemSzId=\''.$D['itemSzId'].'\' AND A.isVari=\'Y\')) ',[1=>'Error obteniendo lista de materiales para la fase']);
				if(a_sql::$err){ _err::err(a_sql::$errNoText); }
				else{ while($L=$q->fetch_assoc()){
					$Mx['L'][]=['lineType'=>$L['lineType'],'itemId'=>$L['citemId'],'itemSzId'=>$L['citemSzId'],'itemCode'=>$L['itemCode'],'itemName'=>$L['itemName'],'udm'=>$L['udm'],'whsId'=>$A['whsId'],'baseQty'=>$L['quantity'],'price'=>$L['buyPrice'],'price2'=>$L['buyPrice']];
				}}
			}
			/*3. obtener productos obtenidos  */
		}
		_err::errDie();
		return _js::enc2($Mx);
	}
	static public function byAll($D=[]){
		if(_js::iseErr($D['itemId'],'Se debe definir el articulo.','numeric>0')){}
		else if(_js::iseErr($D['itemSzId'],'Se debe definir el subproducto.','numeric>0')){}
		else{
			$Mx=['L'=>[],'LF'=>[]];
			/*1. obtener fase anterior lineType=PeP */
			$q=a_sql::query('SELECT M.wfaId
			FROM wma_mpg1 M
			WHERE M.itemId=\''.$D['itemId'].'\' ',[1=>'Error obteniendo fases del proceso',2=>'El articulo no tiene definido ninguna fase']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else while($L=$q->fetch_assoc()){
				$Mx['L'][]=['wfaId'=>$L['wfaId']];
			}
			/*2. obtener componentes MP,MO,MA,CIF */
			if(!_err::$err){
				$q=a_sql::query('SELECT A.wfaId,A.citemId,A.citemSzId,I.itemCode,I.itemName,A.buyPrice,A.quantity,I.udm,A.lineType,A.whsId
				FROM itm_itt1 A
				JOIN itm_oitm I ON (I.itemId=A.citemId)
				WHERE A.itemId=\''.$D['itemId'].'\' AND ((A.itemSzId=0 AND A.variType=\'N\') OR (A.itemSzId=\''.$D['itemSzId'].'\' AND A.isVari=\'Y\')) ',[1=>'Error obteniendo lista de materiales']);
				if(a_sql::$err){ _err::err(a_sql::$errNoText); }
				else if(a_sql::$errNo==-1){ while($L=$q->fetch_assoc()){
					$Mx['LF'][]=['wfaId'=>$L['wfaId'],'lineType'=>$L['lineType'],'itemId'=>$L['citemId'],'itemSzId'=>$L['citemSzId'],'itemCode'=>$L['itemCode'],'itemName'=>$L['itemName'],'udm'=>$L['udm'],'baseQty'=>$L['quantity'],'price'=>$L['buyPrice'],'whsId'=>$A['whsId']];
				}}
			}
			/*3. obtener productos obtenidos  */
		}
		_err::errDie();
		return _js::enc2($Mx);
	}
}
?>