<?php
JRoute::get('wma/wpt',function($D){
	$D['from']='A.docEntry,A.docStatus,A.docDate,I.itemCode moitemCode,I.itemName moitemName,I.itemGr,A.quantity,A.timeClose,A.okQty,A.rejQty,A.moudm,CC.workPosi,CC.cardName mocardName,CC.workPosi,A.dateC
	FROM wma_owpt A
	LEFT JOIN itm_oitm I ON (I.itemId=A.moitemId)
	LEFT JOIN nom_ocrd CC ON (CC.cardId=A.mocardId)';
	return a_sql::rPaging($D,false,[1=>'Error consultando registro',2=>'No se encontraron resultados']);
});

JRoute::get('wma/wpt/wopGr',function($D){ //obtener con base a grupo
	return a_sql::fetchL('SELECT I.itemId,I.itemCode,I.itemName,I.udm,I.invPrice FROM itm_oitm I WHERE I.itemType=\'MO\' AND I.itemGr=\''.$D['itemGr'].'\' LIMIT 200',['k'=>'L',1=>'Error obteniendo operaciones del grupo.',2=>'No se encontron operaciones en el grupo.'],true);
});

JRoute::get('wma/wpt/open',function($D){ //partes abiertos
	return a_sql::fetchL('SELECT A.docEntry,I.itemCode,I.itemName,A.moudm,I.invPrice,CC.workPosi,CC.cardName,CC.workPosi,A.dateC
	FROM wma_owpt A 
	LEFT JOIN itm_oitm I ON (I.itemId=A.moitemId)
	LEFT JOIN nom_ocrd CC ON (CC.cardId=A.mocardId)
	WHERE A.mocardId=\''.$D['cardId'].'\' AND A.docStatus=\'O\' LIMIT 50',['k'=>'L',1=>'Error consultando registro',2=>'El empleado no tiene registros abiertos'],true);
});
JRoute::post('wma/wpt/open',function($D){ //abir parte
	if(_js::iseErr($D['moitemId'],'La operación no se ha sido definida','numeric>0')){}
	else if(_js::iseErr($D['docDate'],'La fecha debe estar definida')){}
	else if(_js::iseErr($D['mocardId'],'Se debe definir el empleado','cardId>0')){}
	else{
		$qO=a_sql::fetch('SELECT docEntry FROM wma_owpt WHERE mocardId=\''.$D['mocardId'].'\' AND moitemId=\''.$D['moitemId'].'\' AND docStatus=\'O\' LIMIT 1',[1=>'Error verificando apertura de operación']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else if(a_sql::$errNo==-1){ _err::err('El empleado ya tiene un registro abierto para esta operación, debe cerrarlo primero.',3); }
		else{
			a_sql::qInsert($D,['tbk'=>'wma_owpt','qk'=>'ud']);
			if(a_sql::$err){ _err::err('Error abriendo proceso: '.a_sql::$errText,3); }
			else{ $js=_js::r('Proceso abierto correctamente.'); }
		}
	}
	_err::errDie();
	return $js;
});
JRoute::post('wma/wpt/close',function($D){ //cerrar partes
	if(_js::isArray($D['L'])){ _err::err('No se enviaron datos a guardar',3); }
	else{
		$ln=1; $dateC=date('Y-m-d H:i:s');
		foreach($D['L'] as $n=>$L){
			$isDel=($L['delete']=='Y');
			if($L['quantity'].$L['okQty'].$L['rejQty']=='' && $L['delete']!='Y'){
				unset($D['L'][$n]);
				continue; 
			}
			if(!$isDel && _js::iseErr($L['quantity'],$lnt.'La cantidad debe ser un número mayor a 0','if.num>0')){ break; }
			$lnt ='Linea '.$ln.': '; $ln++;
			$qO=a_sql::fetch('SELECT docEntry,docStatus,price FROM wma_owpt WHERE docEntry=\''.$L['docEntry'].'\' LIMIT 1',[1=>$lnt.'Error verificando apertura de operación',2=>$lnt.'El registro de operación no existe.']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); break; }
			else{
				$L['priceLine']=$qO['price']*$L['quantity'];
				if(1 || $qO['docStatus']=='O'){
					$L[0]='u'; $L[1]='wma_owpt'; $L['_wh']='docEntry=\''.$L['docEntry'].'\' LIMIT 1';
					$L['docStatus']='C'; $L['timeClose']=$dateC;
					$D['L'][$n]=$L;
				}
				else{ unset($D['L'][$n]); }
			}
		}
		a_sql::transaction(); $c=false;
		if(!_err::$err && count($D['L'])>0){
			a_sql::multiQuery($D['L']);
		}
		if(!_err::$err){ $c=true;
			$js=_js::r('Registros cerrados correctamente');
		}
		a_sql::transaction($c);
	}
	_err::errDie();
	return $js;
});

JRoute::get('wma/wpt/one',function($L){ //modificar 1
	if(_js::iseErr($L['docEntry'],'Se debe definir ID del documento','numeric>0')){}
	else{
		$qO=a_sql::fetch('SELECT A.docEntry,A.docDate,A.docStatus,CC.cardName,I.itemCode,I.itemGr,A.quantity,A.okQty,A.rejQty
		FROM wma_owpt A
		LEFT JOIN nom_ocrd CC ON (CC.cardId=A.mocardId)
		LEFT JOIN itm_oitm I ON (I.itemId=A.moitemId)
		WHERE A.docEntry=\''.$L['docEntry'].'\' LIMIT 1',[1=>$lnt.'Error obteniendo parte de trabajo',2=>$lnt.'El parte de trabajo no existe.']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			$js=_js::enc2($qO);
		}
	}
	_err::errDie();
	return $js;
});
JRoute::put('wma/wpt/one',function($L){ //modificar 1
	if(_js::iseErr($L['docEntry'],'Se debe definir ID del documento','numeric>0')){}
	else{
		a_sql::transaction(); $c=false;
		$qO=a_sql::fetch('SELECT docEntry,docStatus,price FROM wma_owpt WHERE docEntry=\''.$L['docEntry'].'\' LIMIT 1',[1=>$lnt.'Error verificando apertura de operación',2=>$lnt.'El registro de operación no existe.']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			$L['priceLine']=$qO['price']*$L['quantity'];
			a_sql::qUpdate($L,['tbk'=>'wma_owpt','wh_change'=>'docEntry=\''.$L['docEntry'].'\' LIMIT 1']);
			if(a_sql::$err){ _err::err('Error actualizando documento. '.a_sql::$errText,2); } 
		}
		if(!_err::$err){ $c=true;
			$js=_js::r('Registro actualizado correctamente');
		}
		a_sql::transaction($c);
	}
	_err::errDie();
	return $js;
});
JRoute::delete('wma/wpt/one',function($L){ //modificar 1
	if(_js::iseErr($L['docEntry'],'Se debe definir ID del documento','numeric>0')){}
	else{
		a_sql::transaction(); $c=false;
		$qO=a_sql::fetch('SELECT docEntry,docStatus,price FROM wma_owpt WHERE docEntry=\''.$L['docEntry'].'\' LIMIT 1',[1=>$lnt.'Error obteniendo el parte de trabajo']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			if(a_sql::$errNo==-1){
				$L['priceLine']=$qO['price']*$L['quantity'];
				a_sql::query('DELETE FROM wma_owpt WHERE docEntry=\''.$L['docEntry'].'\' LIMIT 1',[1=>'Error eliminando parte de trabajo']);
				if(a_sql::$err){ _err::err(a_sql::$errNoText); }
				else{ $js=_js::r('Parte eliminando correctamente'); }
			}
			else{ $js=_js::r('Parte eliminando correctamente (2)'); }
		}
		if(!_err::$err){ $c=true; }
		a_sql::transaction($c);
	}
	_err::errDie();
	return $js;
});

?>