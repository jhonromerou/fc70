<?php
JRoute::get('wma/lop',function($D){
	if($js=_js::ise($D['docEntry'],'Se debe definir ID de orden de producción.','numeric>0')){}
	else{
		$M=array('created'=>'N');
		$q=a_sql::query('SELECT B.lopId,I.itemCode,I.itemName,B.itemSzId,B.opTraz,B.quantity
		FROM wma3_lop1 B
		LEFT JOIN itm_oitm I ON (I.itemId=B.itemId) 
		WHERE B.docEntry=\''.$D['docEntry'].'\' LIMIT 99999',array(1=>'Error obteniendo lotes unitarios.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{
			if(a_sql::$errNo==-1){
				$M['created']='Y';
				while($L=$q->fetch_assoc()){ $M['L'][]=$L; }
			}
			$js=_js::enc2($M); unset($M);
		}
	}
	return $js;
});
JRoute::get('wma/lop/code',function($D){
	if($js=_js::ise($D['lopId'],'Se debe definir código de lote.')){}
	else{
		$q=a_sql::fetch('SELECT B.lopId,I.itemId,I.itemCode,I.itemName,B.itemSzId 
		FROM wma3_lop1 B
		LEFT JOIN itm_oitm I ON (I.itemId=B.itemId) 
		WHERE B.lopId=\''.$D['lopId'].'\' LIMIT 1',array(1=>'Error obteniendo código de lote.','El código no existe ['.$D['lopId'].'].'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $js=_js::enc2($q); }
	}
	return $js;
});
JRoute::post('wma/lop',function($D){
	if($js=_js::ise($D['docEntry'],'Se debe definir ID de orden de producción.','numeric>0')){}
	else if($js=_js::ise($D['var1'],'Se debe definir la distribución','numeric>0')){}
	else if($js=_js::ise($D['var3'],'Se debe definir repeticiones por unidad','numeric>0')){}
	else if($js=_err::iff($D['var3']>2,'Repeticiones no puede ser mayor a 2.')){}
	else{
		$q=a_sql::query('SELECT B.lopId FROM wma3_lop1 B WHERE B.docEntry=\''.$D['docEntry'].'\' LIMIT 1',array(1=>'Error obteniendo lotes unitarios.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		//else if(a_sql::$errNo==-1){ $js=_js::e(3,'Los lotes ya fueron definidos'); }
		else{
			$q=a_sql::query('SELECT B.itemId,B.itemSzId,B.quantity FROM wma3_odp1 B WHERE B.docEntry=\''.$D['docEntry'].'\' ',array(1=>'Error obteniendo lineas de orden de producción.',2=>'La orden de producción no tiene lineas registradas.'));
			$qus=array(); $n=1;
			_ADMS::mApps('wma/Odp');
			while($L2=$q->fetch_assoc()){
				//$qty=$L['quantity']; unset($L['quantity']);
				$L2['docEntry']=$D['docEntry'];
				$Rx=wmaOdp::loteDest($L2,$D);
				if(is_array($Rx)) foreach($Rx as $x3=> $L){
					$qty=$L['quantity']*$D['var3']*1;
					for($nx=1; $nx<=$qty*1; $nx++){
						$L['lopId']=$D['docEntry'].substr('0000'.$n,-5); $n++;
						$qus[]=$L;
					}
				}
			}
			a_sql::transaction(); $cmt=false;
			a_sql::multiInsert('wma3_lop1',$qus);
			if(_err::$err){ $js=_err::$errText; }
			else{ $js=_js::r('Códigos de lotes generados.'); $cmt=true; }
			a_sql::transaction($cmt);
		}
	}
	return $js;
});
JRoute::delete('wma/lop',function($D){
	if($js=_js::ise($D['docEntry'],'Se debe definir ID de orden de producción.','numeric>0')){}
	else{
		$q=a_sql::query('DELETE FROM wma3_lop1 WHERE docEntry=\''.$D['docEntry'].'\' LIMIT 99999',array(1=>'Error eliminando lotes unitarios.'));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $js=_js::r('Códigos eliminados correctamente.'); }
	}
	return $js;
});
?>