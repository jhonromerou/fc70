<?php
JRoute::get('wma/mpg',function($D){
	$D['from']='I.itemId,I.itemCode,I.itemName,M.updSubEsc,M.cost,M.costMP,M.costMO,M.costSV,M.costMA,M.cif,M.faseTime,M.faseUdm,M.dateUpd
	FROM itm_oitm I
	LEFT JOIN itm_oipc M ON (M.itemId=I.itemId AND M.isModel=\'Y\' AND M.itemSzId=0)';
	$D['wh']='I.prdItem=\'Y\'';
	return a_sql::rPaging($D,false,[1=>'Error obteniendo definición de fases para el artículo',2=>'No se encontraron resultados.']);
});
JRoute::get('wma/mpg/form',function($D){
	$Mx=a_sql::fetch('SELECT M.*,I.itemId,I.itemCode,I.itemName
	FROM itm_oitm I
	LEFT JOIN itm_oipc M ON (M.itemId=I.itemId AND M.isModel=\'Y\')
 WHERE I.itemId=\''.$D['itemId'].'\' AND I.prdItem=\'Y\' LIMIT 1',array(1=>'Error obteniendo definición de fases.',2=>'El artículo no existe o no es un artículo de producción.'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{
		$qf=a_sql::query('SELECT mpg1.*
		FROM wma_mpg1 mpg1
		WHERE mpg1.itemId=\''.$Mx['itemId'].'\' ORDER BY mpg1.lineNum ASC');
		if(a_sql::$errNo==1){ $Mx['L']=array('errNo'=>3,'text'=>'Error obteniendo lineas de fase: '.$qf['error_sql']); }
		else if(a_sql::$errNo==-1){
			while($L=$qf->fetch_assoc()){
				$Mx['L'][]=$L;
			}
		}
		$js =_js::enc2($Mx);
	}
	return $js;
});
JRoute::put('wma/mpg',function($D){
	if($js=_js::ise($D['itemId'],'Se debe definir el Id del artículo','numeric>0')){}
	else{
		$errs=0;;
		$D['faseTime']=0;
		$D['cost']=$D['costMP']=$D['costMO']=$D['cost']=$D['costMA']=$D['cif']=0;
		_ADMS::mApps('wma/wma3');
		$WFA1=wma3::mpg_faseOrder($D['L']); unset($D['L']);
		$op=array(); $upds=0; $nl=1;
		$qA=array();
		foreach($WFA1 as $n=>$L){ $ln='Linea '.$nl.': ';
			if($op[$L['wfaId']]){ $js=_js::e(3,$ln.'No se puede definir la misma fase más de 1 vez.'); $errs=1; break; }
			$op[$L['wfaId']]=$L['wfaId'];
			if($js=_js::ise($L['wfaId'],$ln.'Seleccione una fase.','numeric>0')){ $errs=1; break; }
			else if($ve && $L['cost']<0){ $js=_js::e(3,$ln.'El costo no puede ser negativo.'); $errs=1; break; }
			else{
				$L['lineNum']=$nl;
				if($L['delete']!='Y'){ $nl++; }
				if($L['delete']=='Y'){
					a_sql::query('SELECT id FROM itm_itt1 WHERE itemId=\''.$D['itemId'].'\' AND wfaId=\''.$L['wfaId'].'\' LIMIT 1',array(1=>$ln.'Error verificando lista de materiales de la fase.'));
					if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break;}
					else if(a_sql::$errNo==-1){ $js=_js::e(3,$ln.'No se puede eliminar una fase que tenga definido materiales. Primero elimine la lista de materiales para esta fase.'); $errs++; break; }
				}
				//definir matriz para guardaron
				$L[0]='i'; $L[1]='wma_mpg1'; $L['_unik']='id';
				$qA[]=$L;
			}
		}
		if($errs>0){ die($js); }
		a_sql::transaction(); $cmt=false;
		$D['dateUpd']=date('Y-m-d H:i:s');
		$D['isModel']='Y'; unset($D['active'],$D['descrip']);
		$ins=a_sql::uniRow($D,array('tbk'=>'itm_oipc','wh_change'=>'isModel=\'Y\' AND itemId=\''.$D['itemId'].'\' LIMIT 1','dateC'=>1));
		if(a_sql::$err){ die(_js::e(1,'Error guardando información: '.a_sql::$errText,$adJs)); }
		if(1){
			$adJs='"itemId":"'.$D['itemId'].'"';
			a_sql::multiQuery($qA,0,array('itemId'=>$D['itemId']));
			if(_err::$err){ $errs=1; $js=_err::$errText; }
			if($errs==0){ $cmt=true;
				$js=_js::r('Guardado correctamente.',$adJs);
			}
		}
		a_sql::transaction($cmt);
	}
	return $js;
});
JRoute::post('wma/mpg/updSubEsc',function($D){
	if($js=_js::ise($D['itemId'],'El id del articulo debe estar definido','numeric>0')){ die($js); }
	a_sql::transaction(); $cmt=false;
	_ADMS::mApps('wma/Lcost');
	wmaLcost::mpgSet2($D);
	if(_err::$err){ $js=_err::$errText; }
	else{
		wmaLcost::setUpdSubEsc('N',$D['itemId']);
		if(_err::$err){ $js=_err::$errText; $errs=1; }
	}
	if(!_err::$err){ $cmt=true;$js=_js::r('Escandallos de subproductos actualizados correctamente.'); }
	a_sql::transaction($cmt);
	return $js;
});
?>