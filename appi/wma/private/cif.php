<?php
JRoute::get('wma/cif',function($D){
	$D['from']='I.itemId,I.itemCode,I.itemName,I.udm,I.invPrice,I.itemGr,I.prdType,I.prdNum1,I.prdNum2,I.udm,I.buyPrice FROM itm_oitm I';
	$D['wh']='itemType=\'CIF\'';
	return a_sql::rPaging($D,false,[1=>'Error obteniendo resultados.',2=>'No se encontraron resultados.']);
});
JRoute::get('wma/cif/form',function($D){
	$q=a_sql::fetch('SELECT * FROM itm_oitm WHERE itemType=\'CIF\' AND itemId=\''.$D['itemId'].'\' LIMIT 1',array(1=>'Error obteniendo información de CIF.',2=>'No se encontró información.'));
	if(a_sql::$errNoText!=''){ $js=a_sql::$errNoText; }
	else{ $js =_js::enc2($q); }
	return $js;
});
JRoute::put('wma/cif/form',function($D){
	$itemId=$D['itemId']; unset($D['itemId']);
	if($js=_js::ise($D['itemCode'],'Se debe definir el códido para el CIF.')){}
	else if(!preg_match('/^([a-z0-9]{1,10})$/i',$D['itemCode'])){$js=_js::e(3,'El código no puede exceder los 10 caracteres y solo pueden ser letras y números.'); }
	else if($js=_js::ise($D['itemName'],'Se debe definir el nombre.')){}
	else if(!_js::textLimit($D['itemName'],50)){ $js=_js::e(3,'El nombre no puede exceder 50 caracteres.'); }
	else{
		$qe=a_sql::query('SELECT itemId FROM itm_oitm WHERE itemId!=\''.$itemId.'\' AND itemCode=\''.$D['itemCode'].'\' LIMIT 1',array(1=>'Error verificando que no exista el código.',-1=>'Ya existe un artículo con este código.'));
		if(a_sql::$err){ die(a_sql::$errNoText); }
		$D['itemType']='CIF';
		$ins=a_sql::uniRow($D,array('tbk'=>'itm_oitm','wh_change'=>'itemId=\''.$itemId.'\' LIMIT 1'));
		if(a_sql::$err){ $js=_js::e(1,'Error guardando información: '.a_sql::$errText,$adJs); }
		else{
			$itemId=($ins['insertId'])?$ins['insertId']:$itemId;
			$adJs='"itemId":"'.$itemId.'"';
			$js=_js::r('Guardado correctamente.',$adJs);
		}
	}
	return $js;
});
?>
