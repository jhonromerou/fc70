<?php
$serieType='wmaPdp'; unset($___D['serieType']);

JRoute::get('wma/pdp',function($D){
	$D['from']='A.docEntry,A.serieId,A.docNum,A.dateC,A.userId,A.docDate,A.docStatus,A.docType,A.dueDate,A.userUpd,A.dateUpd,A.lineMemo FROM wma3_opdp A';
	return a_sql::rPaging($D,false,[1=>'Error obteniendo listado',2=>'No se encontraron resultados.']);
});

JRoute::post('wma/pdp','post');
JRoute::put('wma/pdp','post');
JRoute::get('wma/pdp/view','getOne');
JRoute::get('wma/pdp/form','getOne');

JRoute::get('wma/pdp/consol',function($D){
	_ADMS::lib('sql/filter');
	$wh=a_sql_filter($D);
	$gb='B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm';
	return a_sql::fetchL('SELECT '.$gb.',SUM(B.quantity) quantity, SUM(B.openQty) openQty,SUM(B.progQty) progQty FROM wma3_opdp A
	JOIN wma3_pdp1 B ON (B.docEntry=A.docEntry)
	JOIN itm_oitm I ON (I.itemId=B.itemId)
	WHERE A.docStatus!=\'N\'  '.$wh.' GROUP BY '.$gb,['k'=>'L',1=>'Error obteniendo planificación de producción',2=>'No se encontraron resultados'],true);
});
JRoute::get('wma/pdp/consolGroup',function($D){
	_ADMS::lib('sql/filter');
	$wh=a_sql_filter($D);
	$gb='A.docType,B.itemId,B.itemSzId,I.itemCode,I.itemName,I.udm';
	return a_sql::fetchL('SELECT '.$gb.',SUM(B.quantity) quantity, SUM(B.openQty) openQty,SUM(B.progQty) progQty FROM wma3_opdp A
JOIN wma3_pdp1 B ON (B.docEntry=A.docEntry)
JOIN itm_oitm I ON (I.itemId=B.itemId)
WHERE A.docStatus=\'O\' AND (B.openQty+B.progQty) >0 '.$wh.' GROUP BY '.$gb,['k'=>'L',1=>'Error obteniendo planificación de producción',2=>'No se encontraron resultados'],true);
});
JRoute::get('wma/pdp/orders',function($D){
	if($js=_js::ise($D['docEntry'],'Se debe definir el número del documento.')){ die($js); }
$q=a_sql::fetch('SELECT A.docEntry,A.docStatus,A.docDate,A.dueDate,A.docType,A.lineMemo,A.dateC FROM wma3_opdp A WHERE A.docEntry='.$D['docEntry'].' LIMIT 1',array(1=>'Error obteniendo información del documento.',2=>'El documento no existe.'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else{
		$M=$q; unset($q);
		$q=a_sql::query('SELECT A.docEntry,A.docStatus,A.docDate,A.docStatus,B.itemId,B.itemSzId,B.quantity,I.itemCode,I.itemName,I.udm FROM wma3_oodp A
JOIN wma3_odp1 B ON (B.docEntry=A.docEntry)
JOIN itm_oitm I ON (I.itemId=B.itemId)
WHERE A.tr=\''.$D['docEntry'].'\' ',array(1=>'Error obteniendo ordenes de producción.',2=>'No existen ordenes de producción relacionadas'));
		$M['T']=array();
		$M['L']=array();
		if(a_sql::$err){ $M['L']= json_decode(a_sql::$errNoText,1); }
		else{ $k=0; $A=array(); $n=0;
			while($L=$q->fetch_assoc()){
				$kr=$L['docEntry']; $ta=$L['itemSzId'];
				if(!array_key_exists($kr,$A)){
					$A[$kr]=$k; $n=$A[$kr];
					$M['L'][$n]=array('docEntry'=>$L['docEntry'],'docStatus'=>$L['docStatus'],'docDate'=>$L['docDate'],'itemId'=>$L['itemId'],'itemCode'=>$L['itemCode'],'itemName'=>$L['itemName'],'udm'=>$L['udm'],'T'=>array()); $k++;
				}
				$n=$A[$kr];
				$M['T'][$ta]=$ta;
				if(!array_key_exists($ta,$M['L'][$n]['T'])){ $M['L'][$n]['T'][$ta]=0; }
				$M['L'][$n]['T'][$ta] += $L['quantity'];
			}
		}
		$js=_js::enc($M,'just');
	}
	return $js;
});

JRoute::put('wma/pdp/statusClose','putStatusClose');


JRoute::get('wma/pdp/tb99','logGet');
JRoute::get('wma/pdp/tbRel1','logRel1');

JRoute::get('wma/pdp/cortePlan',function($D){
	//if(a_ses::$userId!=1){ die(_js::e(3,'Reporte deshabilitado temporalmente.')); }
	_ADMS::lib('sql/filter');
	_ADMS::libC('wma','pep');
	$gb='A.docEntry, A.docDate, B.itemId,B.itemSzId, I.itemCode, I.itemName,W1.onHand';
	$wfaClassPdP=$D['wfaClassPdP']; unset($D['wfaClassPdP']);
	$wh=a_sql_filter($D);
	$q = a_sql::query('SELECT '.$gb.',SUM(B.openQty) openQty
FROM gvt_opvt A
JOIN gvt_pvt1 B ON (B.docEntry = A.docEntry)
JOIN itm_oitm I ON (I.itemId=B.itemId)
LEFT JOIN ivt_oitw W1 ON (W1.itemId=B.itemId AND W1.itemSzId=B.itemSzId AND W1.whsId=A.whsId)
WHERE A.docStatus=\'O\' AND I.prdItem=\'Y\' AND B.openQty>0 '.$wh.' GROUP BY '.$gb,array(1=>'Error obteniendo corte para programación',2=>'No se encontraron resultados.'));
	$M=array('_W'=>array(),
	//'errWarn'=>'Inventario de Producto en proceso no confiable, no se incluyen el producto en proceso.',
	//'winAlert'=>'Y',
	'L'=>array());
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else{
		$k=0; $kn=0; $It=array();
		while($L=$q->fetch_assoc()){
			$kr=$L['itemCode'].'-'.$L['itemSzId'];
			if(!array_key_exists($kr,$It)){ $It[$kr]=$kn; $k=$kn;
				$q2=wmaPdp::openQty($L);
				$pEp=wmaPep::onHand($L,array('wfaClass'=>$wfaClassPdP));
				//$pEp=array('onHand'=>'');
				if(_err::$err){ die(_err::$errText); }
				$M['L'][$k]=array('itemId'=>$L['itemId'],'itemCode'=>$L['itemCode'],'itemName'=>$L['itemName'],'itemSzId'=>$L['itemSzId'],'openQty'=>0,'onHand'=>$L['onHand'],
				'progr'=>$q2,
				'pEp'=>$pEp);
				unset($pEp);
				$kn++;
			}
			$k=$It[$kr];
			$M['L'][$k]['openQty'] += $L['openQty'];
		}
		$js=_js::enc2($M); unset($M);
	}
	return $js;
});
?>