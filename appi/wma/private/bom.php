<?php
/*
if($_GET['itt1_fatherId__'] && a_ses::$userId==1){
	$q=a_sql::query('SELECT id,itemId,lineNum FROM itm_itt1 WHERE isVari=\'N\' AND variType!=\'N\' ORDER BY lineNum');
while($L=$q->fetch_assoc()){
 $u=a_sql::query('UPDATE itm_itt1 SET fatherId=\''.$L['id'].'\' WHERE itemId=\''.$L['itemId'].'\' AND lineNum=\''.$L['lineNum'].'\' AND isVari=\'Y\' AND variType=\'N\' ');
}
}

if($_GET['itt1_contra__'] && a_ses::$userId==1){
	$n=1;
	$q=a_sql::query('SELECT id,itemId,lineNum,wfaId,pieceName,buyPrice,quantity,lineTotal FROM itm_itt1 WHERE citemId=231 AND itemSzId=0 ');
while($L=$q->fetch_assoc()){
		$ta=a_sql::query('SELECT T.itemSzId FROM itm_grs2 T JOIN itm_oitm I ON (I.grsId=T.grsId) WHERE I.itemId=\''.$L['itemId'].'\' ORDER BY itemSzId ');
		$ide=$L['id']; unset($L['id']);
		$L['fatherId']=$ide;
		$L['isVari']='Y';
		echo '#'.$n.': ';
		while($L2=$ta->fetch_assoc()){
			$L['itemSzId']=$L2['itemSzId'];
			$L['citemId']=($L2['itemSzId']<=39)?232:231;
			$ins2=a_sql::insert($L,array('tbk'=>'itm_itt1','qDo'=>'insert'));
		}$n++;
		a_sql::query('UPDATE itm_itt1 SET variType=\'tcode\' WHERE id=\''.$ide.'\' LIMIT 100 ');
}
}

*/
JRoute::get('wma/bom',function($D){
	$D['from']='I.itemId,I.itemCode,I.itemName,I.udm,I.itemGr,I.itemType FROM itm_oitm I';
	$D['wh']='I.prdItem=\'Y\'';
	return a_sql::rPaging($D,false,[1=>'Error obteniendo listado de artículo.',2=>'No se encontraron resultados.']);
});
JRoute::get('wma/bom/form',function($D){
	if($js=_js::ise($D['itemId'],'Se debe definir el Id del producto')){ die($js); }
	$Mx=a_sql::fetch('SELECT itemCode, itemName FROM itm_oitm WHERE itemId=\''.$D['itemId'].'\' LIMIT 1',array(1=>'Error obteniendo información del producto: ',2=>'El artículo no existe'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{
	#/*
	$Mx['_WFA']=a_sql::queryL('SELECT M1.wfaId k,WF.wfaName v,M1.lineNum wfaOrder 
	FROM wma_mpg1 M1 
	LEFT JOIN wma_owfa WF ON (WF.wfaId=M1.wfaId)
	WHERE M1.itemId=\''.$D['itemId'].'\' ORDER BY M1.lineNum ',
	array('L'=>'N','enc'=>'N',1=>'Error obteniendo fases del artículo: ',2=>'No se puede definir lista de materiales para un artículo sin fases'));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	#*/
		$q=a_sql::query('SELECT F.id,F.lineType,F.lineNum,F.citemId,F.citemSzId,F.citemSzId itemSzId,I.itemCode,I.itemName,I.udm,F.variType,F.pieceName,F.wfaOrder,F.wfaId,F.buyPrice,F.quantity,F.lineTotal 
		FROM itm_itt1 F 
		JOIN itm_oitm I ON (I.itemId=F.citemId) 
		WHERE F.itemId=\''.$D['itemId'].'\' AND F.isVari=\'N\' ORDER BY F.lineNum ASC LIMIT 300',array(1=>'Error obteniendo arból de materiales del artículo.',2=>'No se encontró ningun material definido para el artículo (1).'));
		if(a_sql::$err){ $Mx['L']=json_decode(a_sql::$errNoText,1); }
		else{$n=0;
			while($L=$q->fetch_assoc()){
				$Mx['L'][]=$L;
			}
		}
		$js=_js::enc2($Mx);
	}
	return $js;
});
JRoute::put('wma/bom/form',function($D){
	if($js=_js::ise($D['itemId'],'Se debe definir el Id del producto')){}
	else if(_js::isArray($D['L'])){ die(_js::e(3,'No se han enviado lineas a relacionar.')); }
	$errs=0; $lineNum=1;
	$qU=array();
	/* Revision de Lineas */
	foreach($D['L'] as $n => $D2){
		$ln='Linea '.$lineNum.': ';
		if($js=_js::ise($D2['citemId'],$ln.'Se debe definir el producto.')){ $errs++; break; }
		else if($js=_js::ise($D2['quantity'],$ln.'Se debe definir la cantidad de consumo.','numeric>0')){ $errs++; break; }
		else{
			$D2['wfaOrder']=$D['WFA'][$D2['wfaId']]['wfaOrder'];
			if($D2['id']>0){//revisar que no tenga variantes
				$qf=a_sql::fetch('SELECT Fa.id,Fa.variType,Fa.wfaId,Va.fatherId 
				FROM itm_itt1 Va 
				LEFT JOIN itm_itt1 Fa ON (Fa.id=Va.fatherId)
				WHERE Va.fatherId=\''.$D2['id'].'\' LIMIT 1',array(1=>'Error verificando si existen variantes.')); 
				if(a_sql::$err){ $js=a_sql::$errNoText; $errs++; break; }
				else if(a_sql::$errNo==-1 && $D2['delete']=='Y'){ $js=_js::e(3,$ln.'Existen variantes definidas para esta linea, debe elimininar primero las variantes.'); $errs++; break; }
				else if(a_sql::$errNo==-1 && $qf['variType']!=$D2['variType']){
					$js=_js::e(3,$ln.'Existen variantes definidas, debe eliminarlas para poder modificar el tipo de variante. '.$D2['id']); $errs++; break;
				}
				//Actualizar fases de variantes
				else if(a_sql::$errNo==-1 && $qf['wfaId']!=$D2['wfaId']){
					$qU[]=array('u','itm_itt1','wfaId'=>$D2['wfaId'],'lineNum'=>$lineNum,'_wh'=>'fatherId=\''.$D2['id'].'\' LIMIT 500');
				}
			}
			if($D2['itemId']=='undefined'){ unset($D2['undefined']); }
			$D2['itemId']=$D['itemId'];
			$D2['isVari']='N';
			$D2['lineNum']=$lineNum;
			$D2['lineTotal']=$D2['buyPrice']*$D2['quantity'];
			$D2[0]='i'; $D2[1]='itm_itt1'; $D2['_unik']='id';
			$D['L'][$n]=$D2;
		}
		$lineNum++;
	}
	a_sql::transaction(); $cmt=false;
	if($errs==0 && count($qU)>0){//actualizar fases de variantes si hay
		a_sql::multiQuery($qU);
		if(_err::$err){ $js=_err::$errText; $errs=1; }
	}
	if($errs==0){//definir itt1
		a_sql::multiQuery($D['L']);
		$affe=a_sql::$affected_rows;
		if(_err::$err){ $js=_err::$errText; $errs=1; }
		else{
			_ADMS::mApps('wma/Lcost');
			wmaLcost::setUpdSubEsc('Y',$D['itemId']);
			if(_err::$err){ $js=_err::$errText; $errs=1; }
		}
		if(!_err::$err){
			_ADMS::mapps('wma/Cost');
			wmaCost::defineModel(array('itemId'=>$D['itemId']));
			if(_err::$err){ $js=_err::$errText; $errs=1; }
			else{
				wmaCost::defineModelWfa(array('itemId'=>$D['itemId']));
				if(_err::$err){ $js=_err::$errText; $errs=1; }
				else{
					wmaCost::sumTo(array('itemId'=>$D['itemId']));
					if(_err::$err){ $js=_err::$errText; $errs=1; }
				}
			}
		}
	}
	if($errs==0){ $cmt=true;
		$js=_js::r('Se actualizó toda la información correctamente. ('.$upds.')');
	}
	a_sql::transaction($cmt);
	return $js;
});

JRoute::get('wma/bom/form2',function($D){
	if($js=_js::ise($D['itemId'],'Se debe definir el Id del producto')){ die($js); }
	$qi=a_sql::fetch('SELECT itemId,itemCode, itemName, grsId FROM itm_oitm WHERE itemId=\''.$D['itemId'].'\' LIMIT 1',array(1=>'Error obteniendo información del producto: ',2=>'El artículo no existe'));
	if(a_sql::$err){ $js=a_sql::$errNoText; }
	else{
		$Mx=$qi;
		$q=a_sql::query('SELECT F.citemId,F.citemSzId,F.id,F.lineNum,F.fatherId, I.itemCode, I.itemName, I.udm,F.lineNum, F.variType, F.pieceName,F.itemSzId,F.buyPrice,F.wfaId, F.quantity, F.lineTotal 
		FROM itm_itt1 F 
		JOIN itm_oitm I ON (I.itemId=F.citemId) 
		WHERE F.itemId=\''.$D['itemId'].'\' AND ((F.isVari=\'Y\' AND F.variType=\'N\') OR (F.isVari=\'N\' AND F.variType!=\'N\')) ORDER BY fatherId ASC LIMIT 1000',array(1=>'Error obteniendo arból de materiales del artículo.',2=>'No se encontró ningun material definido para el artículo.'));
		if(a_sql::$err){ $Mx['L']=json_decode(a_sql::$errNoText,1); }
		else{$n=0; $Mx['L']=array();
			while($L=$q->fetch_assoc()){
				$L['quantity']*=1; $L['itemId']=$D['itemId'];
				$k=$L['id']; #lineNum
				$k3=$L['itemSzId'];
				if($L['variType']!='N' && !array_key_exists($k,$Mx['L'])){
					$Mx['L'][$k] = $L;
				}
				$k2=$L['fatherId'];
				if($L['variType']!='Y' && $k2>0){ $k=$k2;
					if(!array_key_exists($k2,$Mx['L'])){ $Mx['L'][$k2]=array('TA'=>array());}
					$Mx['L'][$k]['TA'][$k3] = $L; 
				}
				
			}
		}
		$js=_js::enc2($Mx);
	}
	return $js;
});
JRoute::put('wma/bom/form2',function($D){
	if(_js::isArray($D['L'])){ die(_js::e(3,'No se han enviado lineas a guardar.')); }
	$errs=0;
	$nr=1;
	foreach($D['L'] as $n => $D2){
		$ln='Linea '.$D2['lineNum'].' (T: '.$D2['citemSzId'].')-'.$nr.': '; $nr++;
		if($D2['quantity']==''){ unset($D['L'][$n]); continue; }
		else if($js=_js::ise($D2['fatherId'],$ln.'No se ha encontrado el ID padre para la definir las variantes. ('.$D2['fatherId'].')')){ $errs=1; break; }
		else if($js=_js::ise($D2['citemId'],$ln.'No se ha definido el material para la variante.','numeric>0')){ $errs=1; break; }
		else if($D2['citemSzId'] && $js=_js::ise($D2['citemSzId'],$ln.'No se ha definido el material para la variante (2).','numeric>0')){ $errs=1; break; }
		else if($js=_js::ise($D2['quantity'],$ln.'Se debe definir una cantidad mayor a 0.','numeric>0')){ $errs=1; break; }
		else{
			$qf=a_sql::fetch('SELECT itemId,lineNum,lineType,pieceName, buyPrice,wfaId FROM itm_itt1 WHERE id=\''.$D2['fatherId'].'\' LIMIT 1',array(1=>'Error obteniendo línea padre: ',2=>$ln.'La linea padre no ha sido encontrada para definir la variante.'));
			if(a_sql::$err){ $js=a_sql::$errNoText; $errs=1; break; }
			else{
				$D['itemId']=$qf['itemId'];
				$D['L'][$n]['lineNum']=$qf['lineNum'];
				$D['L'][$n]['lineType']=$qf['lineType'];
				$D['L'][$n]['pieceName']=$qf['pieceName'];
				$D['L'][$n]['itemId']=$qf['itemId'];
				$D['L'][$n]['wfaId']=$qf['wfaId'];
				$D['L'][$n]['isVari']='Y';
				$D['L'][$n]['variType']='N';
				if($qf['variType']=='tcons'){ $D['L'][$n]['buyPrice']=$qf['buyPrice']; }
				$D['L'][$n]['lineTotal']=$D['L'][$n]['buyPrice']*$D2['quantity'];
				$D['L'][$n][0]='i';
				$D['L'][$n][1]='itm_itt1';
				$D['L'][$n]['_unik']='id';
			}
		}
	}
	$rAff=0; a_sql::transaction(); $cmt=false;
	if($errs==0 && count($D['L'])>0){
		a_sql::multiQuery($D['L']); $rAff=a_sql::$affected_rows;
		if(_err::$err){ $js=_err::$errText; $errs=1; }
		else if($rAff>0){
				_ADMS::mapps('wma/Cost,wma/Lcost');
			wmaCost::defineModel(array('itemId'=>$D['itemId']));
			if(_err::$err){ $js=_err::$errText; $errs=1; }
			else{
				wmaCost::defineModelWfa(array('itemId'=>$D['itemId']));
				if(_err::$err){ $js=_err::$errText; $errs=1; }
				else{
					wmaCost::sumTo(array('itemId'=>$D['itemId']));
					if(_err::$err){ $js=_err::$errText; $errs=1; }
				}
			}
			if($errs==0){
				wmaLcost::setUpdSubEsc('Y',$D['itemId']);
				if(_err::$err){ $js=_err::$errText; $errs=1; }
			}
		}
	}
	if($errs==0){ $cmt=true;
		$js=_js::r('Se actualizó toda la información correctamente ('.$rAff.').');
	}
	a_sql::transaction($cmt);
	return $js;
});

JRoute::get('wma/bom/fases',function($D){
	_ADMS::libC('wma','bom');
	return wmaBom::fases($D);
});
?>