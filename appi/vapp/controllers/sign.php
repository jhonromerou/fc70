<?php
class _udc{
const db='ucd_adm1.';
}
class vappSign{
static public function cardLogin($D){
 if(_js::iseErr($D['email'],'Debe definir el email.')){}
 else if(_js::iseErr($D['pass'],'Digite la contraseña.')){}
 else{
  $q=a_sql::fetch('SELECT ocardId,ocardCode FROM '._udc::db.'sel_ocrd WHERE email=\''.$D['email'].'\' AND pass=\''.$D['pass'].'\' LIMIT 1',array(1=>'Error iniciando sesión: ','El email o la contraseña no son correctos.'));
  if(a_sql::$err){ _err::err(a_sql::$errNoText); }
  else{
   a_sql::uniRow(['ocardId'=>$q['ocardId'],'lastLogin'=>date('Y-m-d H:i:s')],array('tbk'=>_udc::db.'sel_crd11','wh_change'=>'ocardId=\''.$q['ocardId'].'\' LIMIT 1'));
   _ADMS::lib('_jwt');
   _JWT::$pkey='miveappAbc'; _JWT::$due=false;
   //p=V, Vendedor
 		$D=array('SD'=>[
    'ocardtooken'=>_JWT::encode(array('p'=>'V','ocardId'=>$q['ocardId'],'ocardcode'=>$q['ocardCode']))
   ]);
   return _js::r('Inicio de sesión correcto',$D);
  }
 }
 _err::errDie();
}
static public function cardRegister($D){
 if(_js::iseErr($D['ocardName'],'Debe definir el nombre de su negocio')){}
 else if(_js::iseErr($D['ocardCode'],'Debe definir el usuario para su negocio.')){}
 else if(preg_match('/^\w$/',$D['ocardCode'])){ _err:err('El usuario del negocio solo puede tener letras y/o números'); }
 else if($js=_js::textMax($D['ocardCode'],20,'Usuario de negocio:')){ _err::err($js); }
 else if(_js::iseErr($D['email'],'Debe definir el email de registro.')){}
 else if($D['email']!==$D['emailr']){ _err::err('Verifique que los correos electrónicos coincidan',3); }
 else if($D['pass']!==$D['passr']){ _err::err('Verifique que las contraseñas coincidan',3); }
 else{
  $q=a_sql::fetch('SELECT ocardCode,email FROM '._udc::db.'sel_ocrd WHERE ocardCode=\''.$D['ocardCode'].'\' OR email=\''.$D['email'].'\' LIMIT 1',array(1=>'Error realizando su registro, intente de nuevo más tarde.'));
  if(a_sql::$err){ _err::err(a_sql::$errNoText); }
  else if($q['ocardCode']==$D['ocardCode']){ _err::err('Lo sentimos! alguien más está usando ya este usuario. Prueba con otro.',3); }
  else if($q['email']==$D['email']){ _err::err('El email ya está registrado, si está registrado intenta iniciar sesión',3); }
  else{
   unset($D['passr'],$D['emailr']);
   $ins=a_sql::qInsert($D,array('tbk'=>_udc::db.'sel_ocrd'));
   if(a_sql::$err){ _err::err('Error registrando usuario: '.a_sql::$errText,3); }
   else{
    _ADMS::lib('_Mailer');
  	 $Ds=array('asunt'=>'Bienvenido! '.$D['ocardName'],'to'=>$D['email']);
    $Ds['body']='<p>Estamos felices que te unas a la red, recuerda que puedes ingresar por <a href="http://app.unclicc.com/">app.unclicc.com</a></p>
    <p>Usuario: '.$D['email'].'<br/>
    contraseña: '.$D['pass'].'</p>
    <p><img src="https://launion.com.ar/wp-content/uploads/2017/08/image001-3-1-500x263.jpg" /></p>';
    $s = _Mail::send($Ds);
    if($s['errNo']){ }
    return _js::r('Usuario creado correctamente',$D);
   }
  }
 }
 _err::errDie();
}
static public function cardRestore($D){
 if(_js::iseErr($D['datoRecu'],'Digite el correo o usuario para recuperar su contraseña.')){}
 else{
  $q=a_sql::fetch('SELECT ocardCode,email,pass FROM '._udc::db.'sel_ocrd WHERE email=\''.$D['datoRecu'].'\' OR ocardCode=\''.$D['datoRecu'].'\' LIMIT 1',array(1=>'Error recuperando sus datos, intente de nuevo más tarde.',2=>'El correo o usuario no está asociado a ninguna cuenta. Verifica tus datos.'));
  if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
  else if(_js::iseErr($q['email'],'Su cuenta no tiene registrado ningun correo! comuniquese vía whatsapp o al correo info@admsistems.com para recuperar su información')){}
  else{
   _ADMS::lib('_Mailer');
 	 $Ds=array('asunt'=>'Recuperar datos de inicio','to'=>$q['email']);
   $Ds['body']='<p>Solicitaste recuperar tus datos para inicio de sesión, ingresa a <a href="http://app.unclicc.com/">app.unclicc.com</a></p>
   <p>Correo Registrado: '.$q['email'].'<br/>
   Usuario: '.$q['ocardCode'].'<br/>
   contraseña: '.$q['pass'].'</p>
   <p><img src="https://launion.com.ar/wp-content/uploads/2017/08/image001-3-1-500x263.jpg" /></p>';
   $s = _Mail::send($Ds);
   if($s['errNo']){ print_r($s); }
   return _js::r('Te enviamos un mensaje a '.$q['email'].' con tus datos de acceso.',$D);
  }
 }
 _err::errDie();
}
}
?>
