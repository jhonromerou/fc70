<?php
JRoute::get('sgc/acm','get');
JRoute::get('sgc/acm/tb99','tb99');

JRoute::get('sgc/acm/view','getOne');
JRoute::get('sgc/acm/form','getOne');
JRoute::post('sgc/acm','post');
JRoute::put('sgc/acm','put');

JRoute::put('sgc/acm/statusO','statusO');
JRoute::put('sgc/acm/statusC','statusC');

JRoute::put('sgc/acm/statusCancel','statusN');
?>