<?php
class sgcAcm extends JxDoc{
static $serie='sgcAcm';
static $AI='docEntry';
static $tbk='sgc_oacm';
static $tbk1='sgc_acm1';
static $tbk99='sgc_doc99';
static public function get($D){
	_ADMS::lib('iDoc,sql/filter');
  $D['from']='A.docEntry,A.docDate,A.dueDate,A.docStatus,A.canceled,A.docType,A.docClass,A.dateC,A.userId,A.dateUpd,A.userUpd,A.docTitle,A.dpto,A.lineNums
  FROM '.self::$tbk.' A';
	return a_sql::rPaging($D);
}
static public function getOne($D){
	if($js=_js::ise($D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	$M=a_sql::fetch('SELECT A.* 
	FROM '.self::$tbk.' A 
	WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'error obteniendo documento',2=>'El documento no existe']);
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else{
		$M['L']=a_sql::fetchL('SELECT B.*
		FROM '.self::$tbk1.' B 
		WHERE B.docEntry=\''.$D['docEntry'].'\' ORDER BY B.lineNum ASC',[1=>'error obteniendo lineas documento',2=>'El documento no tiene lineas registradas']);
	}
	return _js::enc2($M);
}
static public function fieldsRequire(){
  return [
		['k'=>'docType','ty'=>'R','iMsg'=>'Tipo'],
		['k'=>'docClass','ty'=>'id','iMsg'=>'Origen'],
    ['k'=>'docDate','ty'=>'date','iMsg'=>'Fecha'],
    ['k'=>'dueDate','ty'=>'date','iMsg'=>'Plazo'],
    ['k'=>'docTitle','maxLen'=>200,'iMsg'=>'Titulo'],
    ['k'=>'lineMemo','maxLen'=>1000,'iMsg'=>'Descripcion'],
    ['ty'=>'L','k'=>'L','req'=>'N','iMsg'=>'Plan de Accion','L'=>[
      ['k'=>'lineStatus','ty'=>'R','iMsg'=>'Estado'],
      ['k'=>'lineText','ty'=>'R','iMsg'=>'Actividad'],
      ['k'=>'lineDate','ty'=>'R','iMsg'=>'Fecha'],
    ]
    ]
  ];
}
static public function post($_J=array()){
  $ori=' on[gacJAcm()]';
  unset($_J['docEntry']);
	self::formRequire($_J);
	a_sql::transaction(); $c=false;
	if(!_err::$err){
    _ADMS::lib('docSeries,JLog');
		$_J['docEntry']=$docEntryN=self::nextID();
		if(!_err::$err){ $_J=self::nextNum($_J); }
  }
  $qI=[];
  if(!_err::$err){
    $Lx=$_J['L']; unset($_J['L']);
    $_J[0]='i'; $_J[1]=self::$tbk; $_J[2]='udUpd';
    $ln=0;
    if(!_js::isArray($Lx)){ foreach($Lx as $n=>$L){
			$ln++;
      $L[0]='i'; $L[1]=self::$tbk1; $L['docEntry']=$docEntryN;
			$L['lineNum']=$ln;
      $qI[]=$L;
		}}
		$_J['lineNums']=$ln;
		$qI[]=$_J;
    a_sql::multiQuery($qI);
  }
	if(!_err::$err){ $c=true; $js=_js::r('Accion registrada correctamente','"docEntry":"'.$docEntryN.'"');
		self::tb99P(['docEntry'=>$docEntryN,'dateC'=>1]);
	}
	a_sql::transaction($c);
	_err::errDie();
	return $js;
}
static public function put($_J=array()){
  $ori=' on[gvtPqr:post()]';
  $docEntryN=$_J['docEntry'];
	self::formRequire($_J);
	$qI=[];
	_ADMS::lib('iDoc,JLog');
	a_sql::transaction(); $c=false;
	if(iDoc::vStatus(array('tbk'=>self::$tbk,'docEntry'=>$_J['docEntry']))){}
  if(!_err::$err){
    $Lx=$_J['L']; unset($_J['L']);
		$_J[0]='u'; $_J[1]=self::$tbk; $_J[2]='udU';
		$_J['_wh']='docEntry=\''.$docEntryN.'\' LIMIT 1';
    $ln=0;
    if(!_js::isArray($Lx)){ foreach($Lx as $n=>$L){
			if($L['delete']!='Y'){ $ln++; }
			$L[0]='i'; $L[1]=self::$tbk1; $L['docEntry']=$docEntryN;
			$L['_unik']='id';
			$L['lineNum']=$ln;
      $qI[]=$L;
		}}
		$_J['lineNums']=$ln;
		$qI[]=$_J;
		//die(a_sql::toQuerys($qI));
    a_sql::multiQuery($qI);
  }
	if(!_err::$err){ $c=true; $js=_js::r('Accion actualizada correctamente','"docEntry":"'.$docEntryN.'"'); }
	a_sql::transaction($c);
	_err::errDie();
	return $js;
}
static public function statusC($D=array()){
	if(_js::iseErr($D['closeDate'],'Se debe definir fecha del cierre')){}
	else if($js=_js::textMax($D['lineMemoClose'],200,'Detalles de cierre')){ $js=_err::err($js); }
	else{
		a_sql::transaction(); $cmt=false;
		_ADMS::lib('iDoc,JLog');
		if(iDoc::vStatus(array('tbk'=>self::$tbk,'docEntry'=>$D['docEntry'],'D'=>'Y'))){ return _err::$errText; }
		else if(iDoc::$D['docStatus']!='O'){ return _err::err('Solo un documento abierto se puede cerrar .'.iDoc::$D['docStatus'].'.',3); }
		else{
			a_sql::query('UPDATE '.self::$tbk.' SET docStatus=\'C\',closeDate=\''.$D['closeDate'].'\',lineMemoClose=\''.$D['lineMemoClose'].'\' WHERE docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error actualizando estado del documento']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else{ $cmt=true;
				self::tb99P(['docEntry'=>$D['docEntry'],'docStatus'=>'C']);
				$js=_js::r('Estado actualizado correctamente');
			}

		}
		a_sql::transaction($cmt);
	}
	_err::errDie();
	return $js;
}
static public function statusN($_J=array()){
	_ADMS::lib('iDoc');
	$js=''; a_sql::transaction(); $cmt=false;
	$ori=' on[gvtRce::putCancel()]';
	iDoc::putStatus(array('log'=>self::$tbk99,'closeOmit'=>'Y','t'=>'N','tbk'=>self::$tbk,'docEntry'=>$_J['docEntry'],'serieType'=>self::$serie,'lineMemo'=>$_J['lineMemo']));
	if(_err::$err){ $js=_err::$errText; }
	else{ $cmt=true; $js=_js::r('Documento anulado correctamente.'); }
	a_sql::transaction($cmt);
	return $js;
}

}
?>