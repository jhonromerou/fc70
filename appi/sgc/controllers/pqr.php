<?php
class sgcPqr extends JxDoc{
static $serie='sgcPqr';
static $AI='docEntry';
static $tbk='sgc_opqr';
static $tbk1='sgc_pqr1';
static $tbk99='sgc_doc99';
static public function get($D){
	_ADMS::lib('iDoc,sql/filter');
  $D['from']='A.docEntry,A.docNum,A.serieId,A.docDate,A.docStatus,A.canceled,A.docType,A.docClass,A.dpto,A.slpId,A.dateC,A.userId,A.dateUpd,A.userUpd,C.cardName,A.docTitle
  FROM '.self::$tbk.' A
	JOIN par_ocrd C ON C.cardId=A.cardId';
	$D['permsBy']='slps';
	return a_sql::rPaging($D);
}
static public function getOne($D){
	if($js=_js::ise($D['docEntry'],'No se ha definido el número de documento.','numeric>0')){ die($js); }
	$M=a_sql::fetch('SELECT C.licTradType,C.licTradNum,A.*,C.cardName 
	FROM '.self::$tbk.' A 
	LEFT JOIN par_ocrd C ON (C.cardId=A.cardId)
	WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'error obteniendo documento',2=>'El documento no existe']);
	if(a_sql::$err){ die(a_sql::$errNoText); }
	else{
		$M['L']=a_sql::fetchL('SELECT B.id,B.itemId,B.itemSzId,B.quantity,B.lineClass,I.itemCode,I.itemName
		FROM '.self::$tbk1.' B 
		JOIN itm_oitm I ON (I.itemId=B.itemId)
		WHERE B.docEntry=\''.$D['docEntry'].'\'',[1=>'error obteniendo lineas documento',2=>'El documento no tiene lineas registradas']);
	}
	return _js::enc2($M);
}
static public function fieldsRequire(){
  return [
    ['k'=>'docDate','ty'=>'date','iMsg'=>'Fecha'],
    ['k'=>'cardId','ty'=>'id','iMsg'=>'Tercero'],
    ['k'=>'slpId','ty'=>'id','iMsg'=>'Responsable de Venta'],
    ['k'=>'docType','ty'=>'R','iMsg'=>'Tipo'],
    ['k'=>'docClass','ty'=>'id','iMsg'=>'Causa'],
    ['k'=>'docTitle','maxLen'=>200,'iMsg'=>'Asunto'],
    ['k'=>'docType2','ty'=>'R','iMsg'=>'Relación'],
    ['k'=>'lineMemo','maxLen'=>1000,'iMsg'=>'Descripcion'],
    ['ty'=>'L','k'=>'L','req'=>'N','iMsg'=>'Articulos','L'=>[
      ['k'=>'itemId','ty'=>'id','iMsg'=>'Articulo'],
      ['k'=>'itemSzId','ty'=>'id','iMsg'=>'Articulo (2)'],
      ['k'=>'quantity','ty'=>'>0','iMsg'=>'Cantidad'],
      ['k'=>'lineClass','ty'=>'id','iMsg'=>'Causa'],
    ]
    ]
  ];
}
static public function post($_J=array()){
  $ori=' on[gvtPqr:post()]';
  unset($_J['docEntry']);
	self::formRequire($_J);
	a_sql::transaction(); $c=false;
	if(!_err::$err){
    _ADMS::lib('docSeries,JLog');
    $_J['docEntry']=$docEntryN=self::nextID();
		if(!_err::$err){ $_J=self::nextNum($_J); }
  }
  $qI=[];
  if(!_err::$err){
    $Lx=$_J['L']; unset($_J['L']);
    $_J[0]='i'; $_J[1]=self::$tbk; $_J[2]='udUpd';
    $qI[]=$_J; $ln=1;
    if(!_js::isArray($Lx)){ foreach($Lx as $n=>$L){
      $L[0]='i'; $L[1]=self::$tbk1; $L['docEntry']=$docEntryN;
      $L['lineNum']=$ln; $ln++;
      $qI[]=$L;
    }}
    a_sql::multiQuery($qI);
  }
  if(!_err::$err){ $c=true; $js=_js::r('Documento creado correctamente','"docEntry":"'.$docEntryN.'"'); }
	a_sql::transaction($c);
	_err::errDie();
	return $js;
}
static public function put($_J=array()){
  $docEntryN=$_J['docEntry'];
	self::formRequire($_J);
	a_sql::transaction(); $c=false;
	$qI=[];
	_ADMS::lib('iDoc');
	if(iDoc::vStatus(array('tbk'=>self::$tbk,'docEntry'=>$_J['docEntry'],'D'=>'Y'))){}
	else if(iDoc::$D['docStatus']=='O'){ _err::err('No se puede modificar un documento abierto.',3); }
  if(!_err::$err){
    $Lx=$_J['L']; unset($_J['L']);
		$_J[0]='u'; $_J[1]=self::$tbk; $_J[2]='udU';
		$_J['_wh']='docEntry=\''.$docEntryN.'\' LIMIT 1';
    $qI[]=$_J; $ln=1;
    if(!_js::isArray($Lx)){ foreach($Lx as $n=>$L){
			$L[0]='i'; $L[1]=self::$tbk1; $L['docEntry']=$docEntryN;
			$L['_unik']='id';
      $L['lineNum']=$ln; $ln++;
      $qI[]=$L;
		}}
		//die(a_sql::toQuerys($qI));
    a_sql::multiQuery($qI);
  }
  if(!_err::$err){ $c=true; $js=_js::r('Documento actualizado correctamente','"docEntry":"'.$docEntryN.'"'); }
	a_sql::transaction($c);
	_err::errDie();
	return $js;
}
static public function statusO($D=array()){
	if(_js::iseErr($D['dpto'],'Se debe definir el departamento','numeric>0')){}
	else if($js=_js::textMax($D['lineMemoOpen'],200,'Detalles de apertura')){ $js=_err::err($js); }
	else{
		a_sql::transaction(); $cmt=false;
		_ADMS::lib('iDoc,JLog');
		$ori=' on[gvtPqr::statusOpen()]';
		if(iDoc::vStatus(array('tbk'=>self::$tbk,'docEntry'=>$D['docEntry'],'D'=>'Y'))){ return _err::$errText; }
		else if(iDoc::$D['docStatus']!='S'){ return _err::err('Solo una orden enviada se puede marcar como abierta .'.iDoc::$D['docStatus'].'.',3); }
		else{
			a_sql::query('UPDATE '.self::$tbk.' SET docStatus=\'O\',dpto=\''.$D['dpto'].'\',lineMemoOpen=\''.$D['lineMemoOpen'].'\' WHERE docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error actualizando estado del documento']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else{ $cmt=true;
				self::tb99P(['docEntry'=>$D['docEntry'],'docStatus'=>'O']);
				$js=_js::r('Estado actualizado correctamente');
			}

		}
		a_sql::transaction($cmt);
	}
	_err::errDie();
	return $js;
}
static public function statusC($D=array()){
	if(_js::iseErr($D['docAccep'],'Se debe definir estado de aceptación')){}
	else if($js=_js::textMax($D['lineMemoClose'],200,'Detalles de cierre')){ $js=_err::err($js); }
	else{
		a_sql::transaction(); $cmt=false;
		_ADMS::lib('iDoc,JLog');
		$ori=' on[gvtPqr::statusOpen()]';
		if(iDoc::vStatus(array('tbk'=>self::$tbk,'docEntry'=>$D['docEntry'],'D'=>'Y'))){ return _err::$errText; }
		else if(iDoc::$D['docStatus']!='O'){ return _err::err('Solo una orden abierta se puede cerrar .'.iDoc::$D['docStatus'].'.',3); }
		else{
			a_sql::query('UPDATE '.self::$tbk.' SET docStatus=\'C\',docAccep=\''.$D['docAccep'].'\',docCost=\''.$D['docCost'].'\',lineMemoClose=\''.$D['lineMemoClose'].'\' WHERE docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error actualizando estado del documento']);
			if(a_sql::$err){ _err::err(a_sql::$errNoText); }
			else{ $cmt=true;
				self::tb99P(['docEntry'=>$D['docEntry'],'docStatus'=>'C']);
				$js=_js::r('Estado actualizado correctamente');
			}

		}
		a_sql::transaction($cmt);
	}
	_err::errDie();
	return $js;
}
static public function statusN($_J=array()){
	$js=''; a_sql::transaction(); $cmt=false;
	$ori=' on[gvtRce::putCancel()]';
	Doc::putStatus(array('closeOmit'=>'Y','t'=>'N','tbk'=>self::$tbk,'docEntry'=>$_J['docEntry'],'serieType'=>self::$serie,'reqMemo'=>'Y','lineMemo'=>$_J['lineMemo']));
	if(_err::$err){ $js=_err::$errText; }
	else{
		_ADMS::mApps('gfi/Acc');
		gfiDac::putCancel(array('tt'=>self::$serie,'tr'=>$_J['docEntry']));
		if(_err::$err){ return _err::$errText; }
		$q=a_sql::query('SELECT A.canceled,B.lineType,B.acId,B.tt,B.tr,B.debBal
		FROM '.self::$tbk.' A 
		JOIN '.self::$tbk1.' B ON (B.docEntry=A.docEntry)
		WHERE A.docEntry=\''.$_J['docEntry'].'\'',array(1=>'Error consultando recibo de pago.',2=>'No se encontró información del pago a anular.'));
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{
			$errs=0;
			$qU=array();
			while($L=$q->fetch_assoc()){
				$qU[]=array('u','gfi_dac1','creBalDue='=>'creBalDue+'.$L['debBal'],'_wh'=>'acId=\''.$L['acId'].'\' LIMIT 1');
				if($L['tr']>0){//Revertir Doc
					$Q=self::revTT($L);
					if(_err::$err){ $errs=1; break; }
					$qU[]=array('u',$Q['tbk'],'balDue='=>'balDue+'.$L['debBal'],'_wh'=>'docEntry=\''.$L['tr'].'\' LIMIT 1');
				}
			}
			if($errs==0){/*actualizar */
				a_sql::multiQuery($qU);
				if(_err::$err){}
				else{ $cmt=true; $js=_js::r('Recibido anulado correctamente.');
					Doc::logPost(array('tbk'=>self::$tbk99,'serieType'=>self::$serie,'docEntry'=>$_J['docEntry'],'dateC'=>1));
				}
			}
		}
	}
	a_sql::transaction($cmt);
	return $js;
}

}
?>