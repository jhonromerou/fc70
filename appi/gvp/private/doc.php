<?php
JRoute::post('gvp/doc',function($D){
	if(_js::iseErr($D['crId'],'No hay terminal definido','numeric>0')){}
	else if(_js::iseErr($D['pcId'],'No hay operario de caja definido','numeric>0')){}
	else{
		$Lm=array();
		$noPays=($D['noPays']=='Y');
		/* Tipo pagos */
		$valorPagos=0;
		$qf2=a_sql::fetch('SELECT docSin,docRcv,whsId,fdpIdEFE,fdpIdTIB,fdpIdTCR,fdpIdTDE FROM gvp_ocre WHERE crId=\''.$D['crId'].'\' LIMIT 1',[1=>'Error obteniendo información de terminal post',2=>'El terminal pos no existe']);
		if(a_sql::$err){ return _err::err(a_sql::$errNoText); }
		else{
			$D['serieId']=$qf2['docSin'];
			$D['whsId']=$qf2['whsId'];
		}
		if($D['EFE']>0){ $Lm[]=['fdpId'=>$qf2['fdpIdEFE'],'creBal'=>$D['EFE']]; $valorPagos+=$D['EFE']; }
		if($D['TIB']>0){ $Lm[]=['fdpId'=>$qf2['fdpIdTIB'],'creBal'=>$D['TIB']]; $valorPagos+=$D['TIB'];}
		if($D['TCR']>0){ $Lm[]=['fdpId'=>$qf2['fdpIdTCR'],'creBal'=>$D['TCR']]; $valorPagos+=$D['TCR'];}
		if($D['TDE']>0){ $Lm[]=['fdpId'=>$qf2['fdpIdTDE'],'creBal'=>$D['TDE']]; $valorPagos+=$D['TDE'];}
		unset($D['EFE'],$D['TIB'],$D['TCR'],$D['TDE'],$D['CRE'],$D['noPays']);
		if(!$noPays && $valorPagos<=0){ return _err::err('Se debe definir pagos a aplicar',3); }
		if(!$noPays && $valorPagos>$D['docTotal']){ return _err::err('El valor a pagar no puede ser mayor que el valor de la factura',3); }
		/* verificar */
		$qf=a_sql::fetch('SELECT cardName,fdpId,pymId,cityCode,countyCode,address,phone1,email2,slpId FROM par_ocrd WHERE cardId=\''.$D['cardId'].'\' LIMIT 1',[1=>'Error obteniendo información del cliente',2=>'El cliente no existe']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		if(!_err::$err){
			$D['fdpId']=$qf['fdpId'];
			/* usar condicion definida */
			if($noPays && $D['pymId']>0){}
			else{ $D['pymId']=$qf['pymId']; }
			$D['cityCode']=$qf['cityCode']; $D['countyCode']=$qf['countyCode']; 
			$D['address']=$qf['address']; $D['phone1']=$qf['phone1']; $D['email']=$qf['email2'];
			$D['slpId']=$qf['slpId'];
			$D['cardName']=$qf['cardName'];
			$D['docDate']=$D['dueDate']=date('Y-m-d');
			require(c::$V['PATH_controllers'].'gvt/sin.php');
			require(c::$V['PATH_MAPPS'].'gvt/Rcv.php');
			a_sql::transaction(); $c=false;
			$D['reqCntData']='N'; //omitir datos direccion, email
			$R=gvtSin::post($D,false);
			if(!_err::$err){ $docEntry=$R['docEntry']; }
			if(!_err::$err && !$noPays){
				$ac=a_sql::fetch('SELECT acId FROM gfi_dac1 WHERE tt=\'gvtSin\' AND tr=\''.$R['docEntry'].'\' AND lineType=\'FV\' LIMIT 1',[1=>'Error verificando factura para aplicar pago',2=>'La factura no existe para aplicar pago']);
				if(a_sql::$err){ _err::err(a_sql::$errNoText); }
				else{ $R=['cardId'=>$R['cardId'],'cardName'=>$qf['cardName'],'docDate'=>$D['docDate'],'acId'=>$ac['acId'],'serieId'=>$qf2['docRcv']]; }
			}
			if(!_err::$err && !$noPays){
				$R['bal']=$valorPagos;
				$R['L']=$Lm; unset($Lm);
				$r=gvtRcv::postN($R,false);
			}
			if(!_err::$err){ $c=true;
				$js=_js::r('Factura generada correctamente','"docEntry":"'.$docEntry.'"');
			}
			a_sql::transaction($c);
		}
	}
	_err::errDie();
	return $js;
},[]);
?>