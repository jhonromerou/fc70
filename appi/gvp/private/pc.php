<?php
JRoute::get('gvp/pc',function($D){
	$D['from']='A.* FROM gvp_opca A';
	return a_sql::rPaging($D);
});
JRoute::get('gvp/pc/form',function($D){
	if(_js::iseErr($D['pcId'],'Se debe definir ID de la persona a modificar','numeric>0')){}
	else{
		$r=a_sql::fetch('SELECT * FROM gvp_opca WHERE pcId=\''.$D['pcId'].'\' LIMIT 1',[1=>'Error obteniendo información de la persona',2=>'La persona no existe']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{ return _js::enc2($r); }
	}
	_err::errDie();
	
},[]);
JRoute::put('gvp/pc',function($D){
	if(_js::iseErr($D['pcCode'],'Se debe definir un código para la persona')){}
	else if($js=_js::textMax($D['pcCode'],4,'Código ')){ _err::err($js); }
	else if(_js::iseErr($D['pcName'],'Se debe definir el nombre')){}
	else if($js=_js::textMax($D['pcName'],20,'Nombre ')){ _err::err($js); }
	else if(_js::iseErr($D['pcPass'],'Debe definir una clave para el cajero')){}
	else if($js=_js::textMax($D['pcPass'],8,'Contraseña ')){ _err::err($js); }
	else if($D['pcPass']!==$D['pcPass2']){ _err::err('La contraseña debe coincidir',3); }
	else{
		unset($D['pcPass2']);
		$R=a_sql::uniRow($D,['tbk'=>'gvp_opca','wh_change'=>'pcId=\''.$D['pcId'].'\' LIMIT 1']);
		if(!_err::$err){
			if($R['insertId']){ $D['pcId']=$R['insertId']; }
			return _js::r('Información guardada correctamente',$D);
		}
	}
	_err::errDie();
},[]);
JRoute::post('gvp/pc/login',function($D){
	if(_js::iseErr($D['pcCode'],'Debe digitar el codigo')){}
	else if(_js::iseErr($D['pcPass'],'Debe digitar la clave')){}
	else if(_js::iseErr($D['crId'],'Debe seleccionar la terminal','numeric>0')){}
	else{
		$qf=a_sql::fetch('SELECT pcId,pcName FROM gvp_opca WHERE pcCode=\''.$D['pcCode'].'\' AND pcPass = BINARY \''.$D['pcPass'].'\' LIMIT 1',[1=>'Error realizando login de cajero',2=>'La información de inicio no coincide']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{ $qf['crId']=$D['crId']; return _js::r('Iniciado',$qf); }
	}
	_err::errDie();
},[]);
?>