<?php
class plugsCmts{
static $objType = 'comment';
static $historyObjType = 'comment';
static $historyTargetType = 'commentData';
static public function getTotal($P=array()){
	$relType = $P['relType']; $relId = $P['relId'];
	$cList = a_sql::fetch('SELECT COUNT(id) totalLine FROM app_com1 
	WHERE relType=\''.$relType.'\' AND relId=\''.$relId.'\' ');
	if(a_sql::$errNo == 1){ $js = -1;}
	if(a_sql::$errNo == 2){ $js = 0;}
	else{ $js = $cList['totalLine']; }
	return $js;
}
static public function getById($P){
	$id = $P['commentId'];
	$C = a_sql::fetch('SELECT * FROM app_com1 WHERE id=\''.$P['commentId'].'\' '.a_ses::get_owh().' LIMIT 1',array('1_'=>'Error obteniendo comentario',2=>'El comentario no existe'));
	$js = '';
	if(a_sql::$errNo == 1){ $js= _js::e(1,$C['error_sql']); }
	else if(a_sql::$errNo == 1){ $js= _js::e(1,'Id '.$id.' no encontrada.');  }
	else{
		$C['deleted'] = ($C['userId'] == a_ses::$userId) ? 'Y' : 'N';
		$js = a_sql::JSON($C);
	}
	return $js;
}
static public function get($P=array()){
	if(_js::ise($P['tt']) || _js::ise($P['tr'])){ return _js::e(3,'No se ha definido el target para obtener los comentarios.'); }
	$fields = (array_key_exists('fields',$P)) ? 'id,'.$P['fields'] : 'id,tt,tr,tt2,tr2,dateC,userId,comment';
	unset($P['GPAGER'],$P['fields'],$P['orderBy'],$P['comment']);
	_ADMS::_lb('sql/filter');
	$canDelete='IF(userId=\''.a_ses::$userId.'\',\'Y\',\'N\') canDelete';
	$wh=a_sql_filter($P);
	return a_sql::fetchL('SELECT '.$fields.','.$canDelete.' FROM app_com1 WHERE 1 '.$wh.' ORDER BY dateC DESC LIMIT 50',
	['k'=>'L',1=>'Error obteniendo listado de comentarios: ',2=>'No se encontraron comentarios.'],true);
}
static public function post($P=array()){
	//$oTy = _o::$Ty['comment'];
	unset($P['id']);
	if(_js::ise($P['tt']) || _js::ise($P['tr'])){ return _js::e(3,'No se ha definido la relación del comentario. Target is undefined.'); }
	if(_js::iseErr($P['comment'],'El texto no puede estar en blanco.')){ }
	else if($js=_js::textMax($P['comment'],500,'Texto:')){ _err::err($js); }
	else{
		$comId=a_sql::qInsert($P,array('qk'=>'ud','tbk'=>'app_com1'));
		if(a_sql::$err){ $js = _js::e(1,'Error guardando comentario: '.a_sql::$errNoText); }
		else{
			$js = _js::r('Guardado Correctamente ('.$memberY.')','"id":"'.$comId.'"');
		}
	}
	_err::errDie();
	return $js;
}
static public function deleteById($P=array()){
	$id = $P['commentId'];
	if($js=_js::ise($id,'ID de comentario no definida.')){ die($js); }
	$gFile = a_sql::fetch('SELECT userId FROM app_com1 
	WHERE id=\''.$id.'\' LIMIT 1',array('err1_noText'=>1,1=>'No se pudo eliminar el comentario: '));
	if(a_sql::$err){ die(a_sql::$errNoText); }
	if(a_sql::$errNo=='-1' && $gFile['userId'] != a_ses::$userId){
		die(_js::e(4,'No tiene permisos para eliminar comentarios de otro usuario.'));
	}
	else{
		$cList = a_sql::query('DELETE FROM app_com1 WHERE id=\''.$id.'\' LIMIT 1',array(1=>'Error eliminando comentario: '));
		if(a_sql::$err){ $js=a_sql::$errNoText; }
		else{ $js = _js::r('Eliminado correctamente.','"id":"'.$id.'"'); }
	}
	return $js;
}
}
?>