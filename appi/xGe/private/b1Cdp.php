<?php
JRoute::get('xGe/b1Cdp/docs',function($D){
	$M=a_sql::fetch('SELECT F.fCode,F.fName,F.version,F.descrip FROM xge_ocdp F WHERE F.fId=\''.$D['fId'].'\' LIMIT 1',[1=>'Error obtiendo datos del formulario.',2=>'El formulario base no existe.']);
	if(a_sql::$err){ return a_sql::$errNoText; }
	$D['A.fId']=$D['fId']; unset($D['fId']);
	$D['from']='A.*,C.cardName FROM xge_ocdpr A
	LEFT JOIN nom_ocrd C ON (C.cardId=A.cardId)';
	$M['L']=a_sql::rPaging($D,true);
	return _js::enc2($M);
},[]);

/* respuestas */
JRoute::get('xGe/b1Cdp/ans',function($D){
	$M=a_sql::fetch('SELECT F.fId,F.fCode,F.fName,F.version,F.descrip,A.docDate,A.docEntry,A.timeTotal,A.qualNum,A.cardId,C.cardName
	FROM xge_ocdp F 
	LEFT JOIN xge_ocdpr A ON (A.fId=F.fId AND A.docEntry=\''.$D['docEntry'].'\')
	LEFT JOIN nom_ocrd C ON (C.cardId=A.cardId)
	WHERE F.fId=\''.$D['fId'].'\' LIMIT 1',[1=>'Error obtiendo datos del formulario.',2=>'El formulario base no existe.']);
	if(a_sql::$err){ return a_sql::$errNoText; }
	$M['L']=a_sql::fetchL('SELECT F1.*,B.aText FROM xge_cdp1 F1 
	LEFT JOIN xge_cdp1r B ON (B.aid=F1.aid AND B.docEntry=\''.$D['docEntry'].'\')
	WHERE F1.fId=\''.$D['fId'].'\' ORDER BY F1.lineNum ASC',[1=>'Error obteniendo preguntas del formulario',2=>'El formulario no tiene preguntas registradas']);
	return _js::enc2($M);
},[]);
JRoute::post('xGe/b1Cdp/ans',function($D){
	if(_js::iseErr($D['fId'],'Id de formulario base no definido','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){ $js=_err::$errText; }
	else if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){ $js=_err::$errText; }
	else{
		a_sql::transaction(); $c=false;
		_ADMS::slib('JForm/JForm');
		$D=JForm::r_post($D,['tbk'=>'xge_ocdp','tbk1'=>'xge_cdp1']);
		if(_err::$err){ $js=_err::$errText; }
		else{ $c=true; $js=_js::r('Información guardada correctamente.',$D);}
		a_sql::transaction($c);
	}
	return $js;
},[]);
JRoute::put('xGe/b1Cdp/ans',function($D){
	if(_js::iseErr($D['fId'],'Id de formulario base no definido','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){ $js=_err::$errText; }
	else if(_js::iseErr($D['cardId'],'Se debe definir el empleado','numeric>0')){ $js=_err::$errText; }
	else{
		a_sql::transaction(); $c=false;
		_ADMS::slib('JForm/JForm');
		$D=JForm::r_put($D,['tbk'=>'xge_ocdp','tbk1'=>'xge_cdp1']);
		if(_err::$err){ $js=_err::$errText; }
		else{ $c=true; $js=_js::r('Información actualizada correctamente.',$D);}
		a_sql::transaction($c);
	}
	return $js;
},[]);

/* qualify */
JRoute::get('xGe/b1Cdp/qua',function($D){
	$M=a_sql::fetch('SELECT F.fId,F.fCode,F.fName,F.version,F.descrip,A.docDate,A.docEntry,A.cardId,C.cardName,A.qualMemo
	FROM xge_ocdp F 
	LEFT JOIN xge_ocdpr A ON (A.fId=F.fId AND A.docEntry=\''.$D['docEntry'].'\')
	LEFT JOIN nom_ocrd C ON (C.cardId=A.cardId)
	WHERE F.fId=\''.$D['fId'].'\' LIMIT 1',[1=>'Error obtiendo datos del formulario.',2=>'El formulario base no existe.']);
	if(a_sql::$err){ return a_sql::$errNoText; }
	$M['L']=a_sql::fetchL('SELECT F1.*,B.aText,B.aPoints 
	FROM xge_cdp1 F1 
	LEFT JOIN xge_cdp1r B ON (B.aid=F1.aid AND B.docEntry=\''.$D['docEntry'].'\')
	WHERE F1.fId=\''.$D['fId'].'\' ORDER BY F1.lineNum ASC',[1=>'Error obteniendo preguntas del formulario',2=>'El formulario no tiene preguntas registradas']);
	return _js::enc2($M);
},[]);
JRoute::put('xGe/b1Cdp/qua',function($D){
	if(_js::iseErr($D['fId'],'Id de formulario base no definido','numeric>0')){ $js=_err::$errText; }
	else{
		a_sql::transaction(); $c=false;
		_ADMS::slib('JForm/JForm');
		$D=JForm::q_put($D,['tbk'=>'xge_ocdp','tbk1'=>'xge_cdp1']);
		if(_err::$err){ $js=_err::$errText; }
		else{ $c=true; $js=_js::r('Información actualizada correctamente.',$D);}
		a_sql::transaction($c);
	}
	return $js;
},[]);
/*p-*/
JRoute::get('xGe/b1Cdp/p',function($D){ /* Docs Planing */
	$Px=fOCP::fg('owsu',['in'=>'A.prp1'],'nomBase');
	$Px['tbk']='xge_ocdp';
	_ADMS::slib('JForm/JForm');
	return JForm::p_get($D,$Px);
},[]);
JRoute::get('xGe/b1Cdp/p/form',function($D){ /* Docs Planing */
	_ADMS::slib('JForm/JForm');
	return JForm::p_getOne($D,['tbk'=>'xge_ocdp','tbk1'=>'xge_cdp1','p1Fie'=>'C.cardName','p1Join'=>'LEFT JOIN nom_ocrd C ON (C.cardId=P1.tr)']);
},[]);
JRoute::post('xGe/b1Cdp/p',function($D){
	if(_js::iseErr($D['prp1'],'Se debe definir el publico objetivo','numeric>0')){ $js=_err::$errText; }
	else{
		a_sql::transaction(); $c=false;
		_ADMS::slib('JForm/JForm');
		$D=JForm::p_post($D,['tbk'=>'xge_ocdp','tbk1'=>'xge_cdp1','peoReq'=>'N']);
		if(_err::$err){ $js=_err::$errText; }
		else{ $c=true; $js=_js::r('Información guardada correctamente.',$D);}
		a_sql::transaction($c);
	}
	return $js;
},[]);
JRoute::put('xGe/b1Cdp/p',function($D){
	if(_js::iseErr($D['prp1'],'Se debe definir el publico objetivo','numeric>0')){ $js=_err::$errText; }
	else{
		a_sql::transaction(); $c=false;
		_ADMS::slib('JForm/JForm');
		$D=JForm::p_put($D,['tbk'=>'xge_ocdp','tbk1'=>'xge_cdp1','peoReq'=>'N']);
		if(_err::$err){ $js=_err::$errText; }
		else{ $c=true; $js=_js::r('Información guardada correctamente.',$D);}
		a_sql::transaction($c);
	}
	return $js;
},[]);

JRoute::get('xGe/b1Cdp/p/sign',function($D){
	_ADMS::slib('JForm/JForm');
	return JForm::p_signGet($D,['tbk'=>'xge_ocdp','tbk1'=>'xge_cdp1','p1Fie'=>'C.cardName lineText','p1Join'=>'LEFT JOIN nom_ocrd C ON (C.cardId=P1.tr)']);
},[]);
JRoute::put('xGe/b1Cdp/p/sign',function($D){
	if(_js::iseErr($D['bCode'],'No se ha enviado el código')){ return _err::$errText; }
	_ADMS::libC('nom','nomBc');
	$Dx=nomBc::byEmp(['lineCode'=>$D['bCode']]);
	if(_err::$err){ $js=_err::$errText; }
	else{
		$D['tt']='card'; $D['tr']=$Dx['cardId'];
		a_sql::transaction(); $c=false;
		_ADMS::slib('JForm/JForm'); unset($D['bCode']);
		$D=JForm::p_signPut($D,['tbk'=>'xge_ocdp','tbk'=>'xge_ocdp','tbk1'=>'xge_cdp1']);
		if(_err::$err){ $js=_err::$errText; }
		else{ $c=true;
		$D['lineText']=$Dx['cardName'];
		$js=_js::enc2(['L'=>[$D]]);
	}
		a_sql::transaction($c);
	}
	return $js;
},[]);


JRoute::get('xGe/b1Cdp/rep',function($D){
	$M=a_sql::fetch('SELECT F.fId,F.fCode,F.fName,F.version,F.descrip
	FROM xge_ocdp F 
	WHERE F.fId=\''.$D['fId'].'\' LIMIT 1',[1=>'Error obtiendo datos del formulario.',2=>'El formulario base no existe.']);
	if(a_sql::$err){ return a_sql::$errNoText; }
	$M['aL']=a_sql::fetchL('SELECT F1.aid,F1.lineNum,F1.lineType,F1.lOpts,F1.lineText FROM xge_cdp1 F1 
	WHERE F1.fId=\''.$D['fId'].'\' ORDER BY F1.lineNum ASC',[1=>'Error obteniendo preguntas del formulario',2=>'El formulario no tiene preguntas registradas']);
	if(a_sql::$err){ return a_sql::$errNoText; }
	$M['L']=a_sql::fetchL('SELECT A.docEntry,A.fId,B.aid,A.docDate,C.cardName,B.aText 
	FROM xge_cdp1r B
	JOIN xge_ocdpr A ON (A.docEntry=B.docEntry)
	LEFT JOIN nom_ocrd C ON (C.cardId=A.cardId)
	WHERE A.fId=\''.$D['fId'].'\' ',[1=>'Error obteniendo respuesta del formulario',2=>'El formulario no tiene respuestas registradas']);
	return _js::enc2($M);
},[]);

/* Plantillas */
JRoute::get('xGe/b1Cdp/forms/sea',function($D){
	_ADMS::slib('JForm/JForm');
	$D['grId']=1;
	return JForm::t_getSea($D,['tbk'=>'xge_ocdp']);
},[]);
JRoute::get('xGe/b1Cdp/forms',function($D){
	_ADMS::slib('JForm/JForm');
	$D['grId']=1;
	return JForm::t_get($D,['tbk'=>'xge_ocdp']);
},[]);
JRoute::get('xGe/b1Cdp/form',function($D){
	_ADMS::slib('JForm/JForm');
	return JForm::t_getOne($D,['tbk'=>'xge_ocdp','tbk1'=>'xge_cdp1']);
},[]);
JRoute::post('xGe/b1Cdp/form',function($D){
		_ADMS::slib('JForm/JForm');
		$D['grId']=1;
	$js=JForm::t_post($D,['tbk'=>'xge_ocdp','tbk1'=>'xge_cdp1']);
	return $js;
},[]);
JRoute::put('xGe/b1Cdp/form',function($D){
		_ADMS::slib('JForm/JForm');
		$D['grId']=1;
	$js=JForm::t_put($D,['tbk'=>'xge_ocdp','tbk1'=>'xge_cdp1']);
	return $js;
},[]);

?>