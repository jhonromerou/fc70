<?php
JRoute::get('xGe/torSeg',function($D){
	$D['from']='A.docEntry,A.dateC,A.docDate,A.docTitle,A.timeTotal,A.workSede FROM xge_otse A ';
	return a_sql::rPaging($D);
},[]);
JRoute::get('xGe/torSeg/form',function($D){
	$js=false;
	if(_js::iseErr($D['docEntry'],'Se debe definir ID para modificar','numeric>0')){}
	else{
		$M=a_sql::fetch('SELECT A.* FROM xge_otse A WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo documento',2=>'El documento no existe']);
		if(a_sql::$err){ return a_sql::$errNoText; }
		else{ return _js::enc2($M); }
	}
	if(_err::$err){ return _err::$errText; }
},[]);
JRoute::post('xGe/torSeg',function($D){
	$js=false;
	if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){}
	else if(_js::iseErr($D['workSede'],'Se debe definir la sede','numeric>0')){}
	else if(_js::iseErr($D['docTitle'],'Se debe definir el asunto')){}
	else if(_js::iseErr($D['timeTotal'],'El tiempo debe definirse','numeric>0')){}
	else{
		a_sql::transaction(); $c=false;
		$D['docEntry']=a_sql::qInsert($D,['tbk'=>'xge_otse']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{ $c=true;
			$js=_js::r('Información guardada correctamente','"docEntry":"'.$D['docEntry'].'"');
		}
		a_sql::transaction($c);
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
});
JRoute::put('xGe/torSeg',function($D){
	$js=false;
	if(_js::iseErr($D['docEntry'],'Se debe definir ID de documento','numeric>0')){}
	else if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){}
	else if(_js::iseErr($D['workSede'],'Se debe definir la sede','numeric>0')){}
	else if(_js::iseErr($D['docTitle'],'Se debe definir el asunto')){}
	else if(_js::iseErr($D['timeTotal'],'El tiempo debe definirse','numeric>0')){}
	else{
		a_sql::transaction(); $c=false;
		a_sql::qUpdate($D,['tbk'=>'xge_otse','wh_change'=>'docEntry=\''.$D['docEntry'].'\' LIMIT 1']);
		if(a_sql::$err){ _err::err(a_sql::$errNoText); }
		else{$c=true;
			$js=_js::r('Información guardada correctamente','"docEntry":"'.$D['docEntry'].'"');
		}
		a_sql::transaction($c);
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
});
?>