<?php
#plantillas
JRoute::get('xGe/ckPop/forms/sea',function($D){
	_ADMS::slib('JForm/JForm');
	return JForm::t_getSea($D,['tbk'=>'xge_ocpo']);
},[]);
JRoute::get('xGe/ckPop/forms',function($D){
	_ADMS::slib('JForm/JForm');
	return JForm::t_get($D,['tbk'=>'xge_ocpo']);
},[]);
JRoute::get('xGe/ckPop/form',function($D){
	_ADMS::slib('JForm/JForm');
	return JForm::t_getOne($D,['tbk'=>'xge_ocpo','tbk1'=>'xge_cpo1']);
},[]);
JRoute::post('xGe/ckPop/form',function($D){
		_ADMS::slib('JForm/JForm');
	$js=JForm::t_post($D,['tbk'=>'xge_ocpo','tbk1'=>'xge_cpo1']);
	return $js;
},[]);
JRoute::put('xGe/ckPop/form',function($D){
		_ADMS::slib('JForm/JForm');
		$D['grId']=1;
	$js=JForm::t_put($D,['tbk'=>'xge_ocpo','tbk1'=>'xge_cpo1']);
	return $js;
},[]);

JRoute::get('xGe/ckPop/docs',function($D){
	$Px=fOCP::fg('owsu',['in'=>'A.prp1'],'nomBase');
	$Px['tbk']='xge_ocpo';
	$Px['f']='I.itemId,I.itemName';
	$Px['l'] .= 'LEFT JOIN xge_ocpo F ON (F.fId=A.fId)
	LEFT JOIN itm_oitm I ON (I.itemId=A.tr)';
	_ADMS::slib('JForm/JForm');
	return JForm::r_docs($D,$Px);
},[]);
JRoute::get('xGe/ckPop/doc',function($D){
	_ADMS::slib('JForm/JForm');
	return JForm::r_doc($D,['tbk'=>'xge_ocpo','tbk1'=>'xge_cpo1',
	'f'=>'I.itemId,I.itemName',
	'l'=>'LEFT JOIN itm_oitm I ON (I.itemId=A.tr)']);
},[]);


/* respuestas */
JRoute::get('xGe/ckPop/ans',function($D){
	_ADMS::slib('JForm/JForm');
	return JForm::r_doc($D,['tbk'=>'xge_ocpo','tbk1'=>'xge_cpo1',
	'f'=>'I.itemId,I.itemName',
	'l'=>'LEFT JOIN itm_oitm I ON (I.itemId=A.tr)']);
},[]);
JRoute::post('xGe/ckPop/ans',function($D){
	if(_js::iseErr($D['fId'],'Id de formulario base no definido','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){ $js=_err::$errText; }
	else if(_js::iseErr($D['tt'],'La relación (tt) no ha sido definida')){ $js=_err::$errText; }
	else if(_js::iseErr($D['tr'],'Se debe definir el articulo','numeric>0')){ $js=_err::$errText; }
	else{
		a_sql::transaction(); $c=false;
		_ADMS::slib('JForm/JForm');
		$D=JForm::r_post($D,['tbk'=>'xge_ocpo','tbk1'=>'xge_cpo1']);
		if(_err::$err){ $js=_err::$errText; }
		else{ $c=true; $js=_js::r('Información guardada correctamente.',$D);}
		a_sql::transaction($c);
	}
	return $js;
},[]);
JRoute::put('xGe/ckPop/ans',function($D){
	if(_js::iseErr($D['fId'],'Id de formulario base no definido','numeric>0')){ $js=_err::$errText; }
	else if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){ $js=_err::$errText; }
	else if(_js::iseErr($D['tt'],'La relación (tt) no ha sido definida')){ $js=_err::$errText; }
	else if(_js::iseErr($D['tr'],'Se debe definir el articulo','numeric>0')){ $js=_err::$errText; }
	else{
		a_sql::transaction(); $c=false;
		_ADMS::slib('JForm/JForm');
		$D=JForm::r_put($D,['tbk'=>'xge_ocpo','tbk1'=>'xge_cpo1']);
		if(_err::$err){ $js=_err::$errText; }
		else{ $c=true; $js=_js::r('Información actualizada correctamente.',$D);}
		a_sql::transaction($c);
	}
	return $js;
},[]);

JRoute::put('xGe/ckPop/statusCancel',function($D){
	a_sql::transaction(); $cmt=false;
	_ADMS::lib('iDoc');
	iDoc::putStatus(array('closeOmit'=>'Y','t'=>'N','tbk'=>'xge_ocpor','docEntry'=>$D['docEntry'],'qku'=>'ud'));
	if(_err::$err){ return _err::$errText; }
	else{ $cmt=true; $js=_js::r('Documento anulado correctamente.'); }
		a_sql::transaction($cmt);
	return $js;
});

?>