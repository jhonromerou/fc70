<?php
JRoute::get('xGe/eva',function($D){
	$D['from']='* FROM xge_odpo A ';
	return a_sql::rPaging($D);
},[]);
JRoute::get('xGe/eva/form',function($D){
	$js=false;
	if(_js::iseErr($D['docEntry'],'Se debe definir ID para modificar','numeric>0')){}
	else{
		$M=a_sql::fetch('SELECT A.* FROM xge_odpo A WHERE A.docEntry=\''.$D['docEntry'].'\' LIMIT 1',[1=>'Error obteniendo documento',2=>'El documento no existe']);
		if(a_sql::$err){ return a_sql::$errNoText; }
		else{
			$M['L']=a_sql::fetchL('SELECT B.*
			FROM xge_dpo1 B
			WHERE B.docEntry=\''.$D['docEntry'].'\' ',[1=>'Error obteniendo lineas del documento']);
			if(a_sql::$err){ a_sql::$errNoText; }
		}
		return _js::enc2($M);
	}
	if(_err::$err){ return _err::$errText; }
},[]);
JRoute::post('xGe/eva',function($D){
	$js=false;
	if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){}
	else if(_js::iseErr($D['workSede'],'Se debe definir la sede para el documento','numeric>0')){}
	else{
		$D['docEntry']=a_sql::qInsert($D,['tbk'=>'xge_odpo']);
		if(a_sql::$err){ _err::err(a_sql::$errText,3); }
		else{
			$js=_js::r('Información guardada correctamente',['docEntry'=>$D['docEntry']]);
		}
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
});
JRoute::put('xGe/eva',function($D){
	$js=false;
	if(_js::iseErr($D['docDate'],'Se debe definir la fecha del documento')){}
	else if(_js::iseErr($D['workSede'],'Se debe definir la sede para el documento','numeric>0')){}
	else if(_js::isArray($D['L'],'No se enviaron puntuaciones')){}
	else{
		$Lx=$D['L']; unset($D['L']);
		$D['totalPoints']=0;
		foreach($Lx as $n=> $L){
			$L[0]='i'; $L[1]='xge_dpo1'; $L['_unik']='id';
			if(is_numeric($L['aPoints'])){
				$D['totalPoints']+=$L['aPoints'];
			}
			$Lx[$n]=$L;
		}
		a_sql::transaction(); $c=false;
		if($D['docEntry']>0){
			a_sql::qUpdate($D,['tbk'=>'xge_odpo','wh_change'=>'docEntry=\''.$D['docEntry'].'\' LIMIT 1']);
		}
		else{
			$D['docEntry']=a_sql::qInsert($D,['tbk'=>'xge_odpo']);
		}
		if(a_sql::$err){ _err::err(a_sql::$errText,3); }
		else{
			a_sql::multiQuery($Lx,null,['docEntry'=>$D['docEntry']]);
			if(!_err::$err){ $c=true;
				$js=_js::r('Información guardada correctamente',['docEntry'=>$D['docEntry']]);
			}
		}
		a_sql::transaction($c);
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
});

JRoute::get('xGe/eva/acc',function($D){
	$D['from']='* FROM xge_dpo2 A ';
	return a_sql::rPaging($D);
},[]);
JRoute::get('xGe/eva/acc/form',function($D){
	$js=false;
	if(_js::iseErr($D['id'],'Se debe definir ID para modificar','numeric>0')){}
	else{
		$M=a_sql::fetch('SELECT B.*
		FROM xge_dpo2 B
		WHERE B.id=\''.$D['id'].'\' ',[1=>'Error obteniendo accion de mejora',2=>'La accíón no existe']);
		if(a_sql::$err){ return a_sql::$errNoText; }
		return _js::enc2($M);
	}
	if(_err::$err){ return _err::$errText; }
},[]);
JRoute::put('xGe/eva/acc',function($D){
	$js=false;
	if(_js::iseErr($D['docEntry'],'Id de documento debe estar definido','numeric>0')){}
	else if(_js::iseErr($D['lineDate'],'Se debe definir la fecha.')){}
	else if(_js::iseErr($D['lineDate'],'Se debe definir el plazo')){}
	else if(_js::iseErr($D['lineTitle'],'Se debe definir el titulo de la acción')){}
	else if($D['lineStatus']=='C' && _js::iseErr($D['lineClose'],'Se debe definir la fecha del cierre.')){}
	else if($D['lineClose'] && $D['lineStatus']!='C'){ _err::err('No puede definir fecha de cierre si el estado no es cerrado.',3); }
	else{
		if($D['id']>0){
			$D['id']=a_sql::uniRow($D,['tbk'=>'xge_dpo2','wh_change'=>'id=\''.$D['id'].'\' LIMIT 1']);
		}
		else{
			$D['id']=a_sql::qInsert($D,['tbk'=>'xge_dpo2']);
		}
		if(a_sql::$err){ _err::err(a_sql::$errText,3); }
		else{
			$js=_js::r('Acción de mejora guardada correctamente',['id'=>$D['id']]);
		}
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
});

JRoute::get('xGe/eva/des',function($D){
	$D['from']='* FROM xge_dpo3 A ';
	return a_sql::rPaging($D);
},[]);
JRoute::get('xGe/eva/des/form',function($D){
	$js=false;
	if(_js::iseErr($D['id'],'Se debe definir ID para modificar','numeric>0')){}
	else{
		$M=a_sql::fetch('SELECT B.*
		FROM xge_dpo3 B
		WHERE B.id=\''.$D['id'].'\' ',[1=>'Error obteniendo hallazgo',2=>'El hallazgo no existe']);
		if(a_sql::$err){ return a_sql::$errNoText; }
		return _js::enc2($M);
	}
	if(_err::$err){ return _err::$errText; }
},[]);
JRoute::put('xGe/eva/des',function($D){
	$js=false;
	if(_js::iseErr($D['docEntry'],'Id de documento debe estar definido','numeric>0')){}
	else if(_js::iseErr($D['lineDate'],'Se debe definir la fecha.')){}
	else if(_js::iseErr($D['lineDate'],'Se debe definir el plazo')){}
	else if(_js::iseErr($D['lineTitle'],'Se debe definir el titulo del hallazgo')){}
	else if($D['lineStatus']=='C' && _js::iseErr($D['lineClose'],'Se debe definir la fecha de solución.')){}
	else if($D['lineClose'] && $D['lineStatus']!='C'){ _err::err('No puede definir fecha de solución si el estado no es cerrado.',3); }
	else{
		if($D['id']>0){
			$D['id']=a_sql::uniRow($D,['tbk'=>'xge_dpo3','wh_change'=>'id=\''.$D['id'].'\' LIMIT 1']);
		}
		else{
			$D['id']=a_sql::qInsert($D,['tbk'=>'xge_dpo3']);
		}
		if(a_sql::$err){ _err::err(a_sql::$errText,3); }
		else{
			$js=_js::r('Hallazgo guardado correctamente',['id'=>$D['id']]);
		}
	}
	if(_err::$err){ return _err::$errText; }
	return $js;
});

?>